<?
function mesure($lang,$size, $cm = true, $show = true)
{   
    global $dmn;
    $lang =  $dmn;

    $_size_help = '';
    $_delimiter = '';

    $_size_explode = explode('x',$size);
    if(isset($_size_explode[1]))
    {
        $size = $_size_explode[1];
        $_size_help = $_size_explode[0];
        $_size_help = str_replace(',', '.', $_size_help);
        $_delimiter = 'x';
    }
    else
    {
        $_size_explode = explode('-',$size);
        if(isset($_size_explode[1]))
        {
            $size = $_size_explode[1];
            $_size_help = $_size_explode[0];
            $_size_help = str_replace(',', '.', $_size_help);
            $_delimiter = '-';
        }
    }
    

    $size = str_replace(',', '.', $size);


    $data = '';
    switch($lang)
    {
        case 'UK':
            if ($cm)
            {
                $data = round(($size * 0.393700787),2) . ($show ? ' inch' : ''); 
                if(!empty($_size_help))
                {
                    $_size_help = round(($_size_help * 0.393700787),2); 
                }
            }
            else
            {
                $data = round(($size * 0.0393700787),2) . ($show ? ' inch' : '');
                if(!empty($_size_help))
                {
                    $_size_help = round(($_size_help * 0.0393700787),2); 
                }
            }
        break;
        default:
            if ($cm)
                $data = $size . ($show ? ' cm' : '');
            else
                $data = $size . ($show ? ' mm' : '');
        break;

    }

    return (!empty($_size_help) ? $_size_help . $_delimiter : ''). $data;
}

function opis($id,$l,$tyg4,$pyt,$rozm,$dataprom)
{  
    switch($l)
        {
        case 'CZ': $pole1="Cz1"; $pole2="Cz2"; $pole5="Cz5"; break;
        case 'EN': $pole1="En1"; $pole2="En2"; $pole5="En2"; break;
        default  : $pole1="Pl1"; $pole2="Pl2"; $pole5="Pl5"; break;
        }
    $query = "SELECT * FROM SKLADPROD WHERE IdProd='$id' order by Id";
	$result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");
    $nowyopis="";
    $opis3="";
    $opis4="";
    $bopis43=0;
	$proba333="";
	$pierscionek=0;
	$prevtyp=0;
   	while($line = mysql_fetch_array($result, MYSQL_ASSOC))
   	    {
   	    $query2 = "SELECT * FROM SKLADNIKI WHERE Id='".$line[Skladnik]."'";
	    $result2 = mysql_query ($query2) or die ("Zapytanie zakoñczone niepowodzeniem");
	    $line2 = mysql_fetch_array($result2, MYSQL_ASSOC);
	    $i=$line[Typ];
	    switch($line[Typ])
	        {
	        case 1: if($line2[Id]!=53) //staviori
	                    $nowyopis=$nowyopis."<font color=#C8B560><b>$line2[Pl1]</b></font><br>";
	                break;
	        case 2: $nowyopis=$nowyopis.$line2[$pole1].($rozm?" ".mesure($l,$rozm):"").".<br>";   //br
	                if($line2[Id]==34)
	                    $pierscionek=1;  
	                break;
	        case 3: $pole="$pole1";
	                $x=$line[Ilosc]%100;
	                if(($x<10 || $x>20) && ($x%10>=2 && $x%10<=4))
	                    $pole="$pole2";
	                else if($line[Ilosc]>1)
	                    $pole="$pole5";    	                
	                $opis3=$opis3.($line[Ilosc]?$line[Ilosc]." ":"").$line2[$pole];
	                if($line[Barwa2] && $line[Barwa2]!=462)
	                    $opis3=$opis3.", ".slownik($line[Barwa2],$l);
	                if($line[Szlif])
	                    $opis3=$opis3.", ".slownik($line[Szlif],$l);
	                if($line[Masa])
	                    $opis3=$opis3.", ".slownik(407,$l)." ".$line[Masa]." ct.";
                    //if($line[Barwa])
	                //    $opis3=$opis3.", ".slownik(408,$l)." ".$line[Barwa];
	                if($line[BarwaOd])
	                    {
	                    $query3 = "SELECT * FROM BARWY WHERE Id='".$line[BarwaOd]."'";
	                    $result3 = mysql_query ($query3) or die ("Zapytanie zakoñczone niepowodzeniem");
                	    $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
	                    $opis3=$opis3.", ".slownik(408,$l)." ".$line3[Nazwa];
	                    if($line[BarwaDo]>$line[BarwaOd])
	                        {
	                        $query3 = "SELECT * FROM BARWY WHERE Id='".$line[BarwaDo]."'";
	                        $result3 = mysql_query ($query3) or die ("Zapytanie zakoñczone niepowodzeniem");
                    	    $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
	                        $opis3=$opis3."-".$line3[Nazwa];
	                        }
	                    }
                    if($line[CzystoscOd])
	                    {
	                    $query3 = "SELECT * FROM CZYSTOSCI WHERE Id='".$line[CzystoscOd]."'";
	                    $result3 = mysql_query ($query3) or die ("Zapytanie zakoñczone niepowodzeniem");
                	    $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
	                    $opis3=$opis3.", ".slownik(209,$l)." ".$line3[Nazwa];
	                    if($line[CzystoscDo]>$line[CzystoscOd])
	                        {
	                        $query3 = "SELECT * FROM CZYSTOSCI WHERE Id='".$line[CzystoscDo]."'";
	                        $result3 = mysql_query ($query3) or die ("Zapytanie zakoñczone niepowodzeniem");
                    	    $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
	                        $opis3=$opis3."-".$line3[Nazwa];
	                        }
	                    }    
	                $opis3=$opis3.".<br>"; //br
	                if(!$bopis43)
	                    {
	                    $nowyopis=$nowyopis."[OPIS43]";
	                    $bopis43=1;
	                    }
	                break;
	        case 4: if($line[Kolor])
	                    {
	                    $kolor="";
	                    
	                    for($i=0; $i<strlen($line[Kolor]); $i++)
	                        {
	                        switch($line[Kolor][$i])
	                            {
	                            case 'z': $indslo=451;  break;
	                            case 'b': $indslo=452;  break;
	                            case 'c': $indslo=453;  break;
	                            case 'r': $indslo=454;  break;
	                            }
	                                
	                        $kolor=$kolor.slownik($indslo,$l).($i<strlen($line[Kolor])-1?", ":" ");
	                        }
	                    } 
	                $opis4=$opis4.$kolor.$line2[$pole1];
	                if($line[Proba])
	                    $opis4=$opis4." ".$line[Proba];
	                if($line[Proba]=="0,333")
	                    $proba333=slownik(210,$l);
	                $opis4=$opis4.".<br>"; //br
	                if(!$bopis43)
	                    {
	                    $nowyopis=$nowyopis."[OPIS43]";
	                    $bopis43=1;
	                    }    	        
	                break;
	        case 5: $nowyopis=$nowyopis.$line2[$pole1];
	                if($line[Wielkosc])
	                    $nowyopis=$nowyopis." ". mesure($l,$line[Wielkosc],false);
	                $nowyopis=$nowyopis.".<br>"; //br
	                break;
	        case 6: $font="";
	                if($line2[$pole2])
	                    $opis=$line2[$pole2];
	                else
	                    $opis=$line2[$pole1];
	                if(strstr($opis, "[orange]"))
	                    $font="</font>";
	                $opis=str_replace("[orange]", "<font color=orange>", $opis);
	                if($prevtyp!=6)
	                    $nowyopis=$nowyopis."<br>";
	                $nowyopis=$nowyopis.$opis.$font."<br>";
	                break;
	        case 7: if($line[Skladnik]==18)
	                $nowyopis=$nowyopis."<br>".slownik(630,$l);
	                break;        
	        }
	    $prevtyp=$line[Typ];
   	    }
   	
   	$dodatkowyopis="";
   	$tydzien="";
   	if($tyg4)
   	    switch($tyg4)
   	        {
   	        case 1: $tydzien=slownik(473,$l);   break;
   	        case 2: case 3: case 4:
   	                $tydzien=slownik(474,$l);   break;
   	        default: $tydzien=slownik(475,$l);   break;
   	        }
   	
   	if($pierscionek)
   	    {
   	    if($pyt)
   	        $dodatkowyopis=slownik(413,$l)."<br>";
   	    if(!$pyt && !$tyg4)
   	        $dodatkowyopis=slownik(404,$l)."<br>";        
   	    if($tyg4)
   	        $dodatkowyopis=slownik(411,$l)." $tyg4 $tydzien.<br>";
   	    }
   	else
   	    if($tyg4)
   	        $dodatkowyopis=slownik(412,$l)." $tyg4 $tydzien.<br>";
   	        
   	if($tyg4<0)            //katalog drukowany
   	    $dodatkowyopis="";
   	        
   	if($prevtyp!=6)
   	    $nowyopis=$nowyopis."<br>";
   	    
   	if($dataprom)
   	    $promdata="<font color=red>".slownik(357,$l)." ".$dataprom.".</font><br>"; 
   	    
   	$nowyopis=str_replace("[OPIS43]", $opis4.$opis3, $nowyopis);
   	   
	   $_desc = $nowyopis.$dodatkowyopis.$proba333.$promdata;
	   $_desc = str_replace('<br>','</h4><h4>',$_desc);
	   $_desc = '<h4>'.$_desc.'</h4>';
	   
   	return $_desc;
}   

function opis2($id,$l,$tyg4,$pyt,$rozm,$dataprom,$part)
{  
    switch($l)
        {
        case 'CZ': $pole1="Cz1"; $pole2="Cz2"; $pole5="Cz5"; break;
        case 'EN': $pole1="En1"; $pole2="En2"; $pole5="En2"; break;
        default  : $pole1="Pl1"; $pole2="Pl2"; $pole5="Pl5"; break;
        }
    $query = "SELECT * FROM SKLADPROD WHERE IdProd='$id' order by Id";
	$result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");
	$nowyopis="";
	$proba333="";
	$pierscionek=0;
	$prevtyp=0;
   	while($line = mysql_fetch_array($result, MYSQL_ASSOC))
   	    {
   	    if($line[Typ]!=$part) continue;
   	    
   	    if($nowyopis!="" && $part!=6)
   	        $nowyopis.="<br>";
   	    
   	    $query2 = "SELECT * FROM SKLADNIKI WHERE Id='".$line[Skladnik]."'";
	    $result2 = mysql_query ($query2) or die ("Zapytanie zakoñczone niepowodzeniem");
	    $line2 = mysql_fetch_array($result2, MYSQL_ASSOC);
	    
	    switch($line[Typ])
	        {
	        case 1: //$nowyopis=$nowyopis."<font color=#C8B560><b>$line2[Pl1]</b></font><br>";
	                break;
	        case 2: $nowyopis=$nowyopis.$line2[$pole1].($rozm ? " " . mesure($l,$rozm) :"").". ";
	                if($line2[Id]==34)
	                    $pierscionek=1;  
	                break;
	        case 3: $pole="$pole1";
	                $x=$line[Ilosc]%100;
	                if(($x<10 || $x>20) && ($x%10>=2 && $x%10<=4))
	                    $pole="$pole2";
	                else if($line[Ilosc]>1)
	                    $pole="$pole5";    	                
	                $nowyopis=$nowyopis.($line[Ilosc]?$line[Ilosc]." ":"").$line2[$pole];
	                if($line[Barwa2] && $line[Barwa2]!=462)
	                    $nowyopis=$nowyopis.", ".slownik($line[Barwa2],$l);
	                if($line[Szlif])
	                    $nowyopis=$nowyopis.", ".slownik($line[Szlif],$l);
	                if($line[Masa])
	                    $nowyopis=$nowyopis.", ".slownik(407,$l)." ".$line[Masa]." ct.";
                    if($line[BarwaOd])
	                    {
	                    $query3 = "SELECT * FROM BARWY WHERE Id='".$line[BarwaOd]."'";
	                    $result3 = mysql_query ($query3) or die ("Zapytanie zakoñczone niepowodzeniem");
                	    $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
	                    $nowyopis=$nowyopis.", ".slownik(408,$l)." ".$line3[Nazwa];
	                    if($line[BarwaDo]>$line[BarwaOd])
	                        {
	                        $query3 = "SELECT * FROM BARWY WHERE Id='".$line[BarwaDo]."'";
	                        $result3 = mysql_query ($query3) or die ("Zapytanie zakoñczone niepowodzeniem");
                    	    $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
	                        $nowyopis=$nowyopis."-".$line3[Nazwa];
	                        }
	                    }
                    if($line[CzystoscOd])
	                    {
	                    $query3 = "SELECT * FROM CZYSTOSCI WHERE Id='".$line[CzystoscOd]."'";
	                    $result3 = mysql_query ($query3) or die ("Zapytanie zakoñczone niepowodzeniem");
                	    $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
	                    $nowyopis=$nowyopis.", ".slownik(209,$l)." ".$line3[Nazwa];
	                    if($line[CzystoscDo]>$line[CzystoscOd])
	                        {
	                        $query3 = "SELECT * FROM CZYSTOSCI WHERE Id='".$line[CzystoscDo]."'";
	                        $result3 = mysql_query ($query3) or die ("Zapytanie zakoñczone niepowodzeniem");
                    	    $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
	                        $nowyopis=$nowyopis."-".$line3[Nazwa];
	                        }
	                    }    
	                $nowyopis=$nowyopis.". ";
	                break;
	        case 4: if($line[Kolor])
	                    {
	                    $kolor="";
	                    
	                    for($i=0; $i<strlen($line[Kolor]); $i++)
	                        {
	                        switch($line[Kolor][$i])
	                            {
	                            case 'z': $indslo=451;  break;
	                            case 'b': $indslo=452;  break;
	                            case 'c': $indslo=453;  break;
	                            case 'r': $indslo=454;  break;
	                            }
	                                
	                        $kolor=$kolor.slownik($indslo,$l).($i<strlen($line[Kolor])-1?", ":" ");
	                        }
	                    }  
	                $nowyopis=$nowyopis.$kolor.$line2[$pole1];
	                if($line[Proba])
	                    $nowyopis=$nowyopis." ".$line[Proba];
	                if($line[Proba]=="0,333")
	                    $proba333=slownik(210,$l);
	                $nowyopis=$nowyopis.". ";    	        
	                break;
	        case 5: $nowyopis=$nowyopis.$line2[$pole1];
	                if($line[Wielkosc])
	                    $nowyopis=$nowyopis." ".$line[Wielkosc]." mm";
	                $nowyopis=$nowyopis.". ";    	     
	                break;
	        case 6: $font="";
	                if(strstr($line2[$pole1], "[orange]"))
	                    $font="</font>";
	                $opis=str_replace("[orange]", "<font color=orange>", $line2[$pole1]);
	                //if($prevtyp!=6)
	                  //  $nowyopis=$nowyopis."<br>";
	                $nowyopis=$nowyopis.$opis.$font."<br>";
	                break;
	        }
	    $prevtyp=$line[Typ];
   	    }
   	
   	$dodatkowyopis="";
   	$tydzien="";
   	if($tyg4)
   	    switch($tyg4)
   	        {
   	        case 1: $tydzien=slownik(473,$l);   break;
   	        case 2: case 3: case 4:
   	                $tydzien=slownik(474,$l);   break;
   	        default: $tydzien=slownik(475,$l);   break;
   	        }
   	
   	if($pierscionek)
   	    {
   	    if($pyt)
   	        $dodatkowyopis=slownik(413,$l)."<br>";
   	    if(!$pyt && !$tyg4)
   	        $dodatkowyopis=slownik(404,$l)."<br>";        
   	    if($tyg4)
   	        $dodatkowyopis=slownik(411,$l)." $tyg4 $tydzien.<br>";
   	    }
   	else
   	    if($tyg4)
   	        $dodatkowyopis=slownik(412,$l)." $tyg4 $tydzien.<br>";
   	        
   	if($tyg4<0)            //katalog drukowany
   	    $dodatkowyopis="";
   	        
   	//if($prevtyp!=6)
   	  //  $nowyopis=$nowyopis."<br>";
   	    
   	if($dataprom)
   	    $promdata="<font color=red>".slownik(357,$l)." ".$dataprom.".</font><br>";
   	
   	if($part==6)        
   	    return $nowyopis.$dodatkowyopis.$proba333.$promdata;
   	return $nowyopis;
} 
?>
