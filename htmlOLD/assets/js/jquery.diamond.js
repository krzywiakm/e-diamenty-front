﻿$(document).ready(function () {

    // Barwa - color
    var $rangeColor = $("#color");

    $rangeColor.ionRangeSlider({
        type: "double",
        grid: true,
        values: ["D", "E", "F", "G", "H", "I", "J", "K", "L", "M"]
    });

    $rangeColor.on("change", function(){
        var $this = $(this),
            from = $('.filter-color .irs-from').html(),
            to = $('.filter-color .irs-to').html();

        $('#color_from').val(from);
        $('#color_to').val(to);
    });

    // Czystość - clarity
    var $rangeClarity = $("#clarity");

    $rangeClarity.ionRangeSlider({
        type: "double",
        grid: true,
        values: ["FL", "IF", "VVS2", "VVS1", "VS1", "VS2", "SI1", "SI2", "I1", "I2", "I3"]
    });

    $rangeClarity.on("change", function(){
        var $this = $(this),
            from = $('.filter-clarity .irs-from').html(),
            to = $('.filter-clarity .irs-to').html();

        $('#clarity_from').val(from);
        $('#clarity_to').val(to);
    });

    // Dostępność - Liczba dni
    var $rangeAvailability = $("#availability");

    $rangeAvailability.ionRangeSlider({
        type: "double",
        min: 1,
        max: 30,
        from: 5,
        to: 30
    });

    $rangeAvailability.on("change", function () {
        var $this = $(this),
            from = $this.data("from"),
            to = $this.data("to");

        $('#availability_from').val(from);
        $('#availability_to').val(to);
    });

    // Szlif
    var $rangeCut = $("#cut");
    $rangeCut.ionRangeSlider({
        type: "double",
        grid: true,
        force_edges: true,
        values: ["DOSKONAŁY", "BARDZO DOBRY", "DOBRY", "ZADOWALAJĄCY"]
    });
    $rangeCut.on("change", function () {
        var $this = $(this),
            from = $this.data("from"),
            to = $this.data("to");

        $('#cut_from').val(from);
        $('#cut_to').val(to);
    });


    // Polerowanie
    var $rangePolishing = $("#polishing");
    $rangePolishing.ionRangeSlider({
        type: "double",
        grid: true,
        force_edges: true,
        values: ["DOSKONAŁY", "BARDZO DOBRY", "DOBRY", "ZADOWALAJĄCY"]
    });

    $rangePolishing.on("change", function () {
        var $this = $(this),
            from = $this.data("from"),
            to = $this.data("to");

        $('#polishing_from').val(from);
        $('#polishing_to').val(to);
    });

    // Symetria
    var $rangeSymmetry = $("#symmetry");
    $rangeSymmetry.ionRangeSlider({
        type: "double",
        grid: true,
        force_edges: true,
        values: ["DOSKONAŁY", "BARDZO DOBRY", "DOBRY", "ZADOWALAJĄCY"]
    });

    $rangeSymmetry.on("change", function () {
        var $this = $(this),
            from = $this.data("from"),
            to = $this.data("to");

        $('#symmetry_from').val(from);
        $('#symmetry_to').val(to);
    });

    $('a.advanced-search').click( function(e){
        e.preventDefault();
        $(this).toggleClass('up down');
        $('#advanced-filtr').toggle(1000, function(){
            $(this).toggleClass('hide-advanced-filters show-advanced-filters')
        })
    });

    $('input[name="sort_shape"]').on('change', function() {
        var $this = $(this);
        if ($this.is(':checked')) {
            $this.parent('li').css('opacity', 1);
        } else {
            $this.parent('li').removeAttr('style');
        }
    });

    $('input[name="sort_colour"]').on('change', function() {
        var $this = $(this);
        var position = $this.data('position')*34 ;
        if ($this.is(':checked')) {
            $this.siblings('label').css('background-position', -position + 'px 0px');
        } else {
            $this.siblings('label').removeAttr('style');
        }
    });

    if(sessionStorage.length == 0){
        sessionStorage.setItem('observed', JSON.stringify([]));
        sessionStorage.setItem('compared', JSON.stringify([]));
        sessionStorage.setItem('cart', JSON.stringify([]));
    }

    console.log(sessionStorage);

    $('.compare .comp span').html(""+ (JSON.parse(sessionStorage.getItem('compared'))).length +"");
    $('#nav .watch span').html(""+ (JSON.parse(sessionStorage.getItem('observed'))).length +"");
    $('#nav .cart span').html(""+ (JSON.parse(sessionStorage.getItem('cart'))).length +"");


    //observed
    $(document).on("change", ".Table .watch input", function () {
        var id = $(this).parents('.TableRow').data('id');
        var observed = JSON.parse(sessionStorage.getItem('observed'));

        if($(this).is(":checked") ){
            observed.push(id);
        } else{
            observed.splice( $.inArray(id, observed), 1 );
        }

        sessionStorage.setItem('observed', JSON.stringify(observed));
        $('#nav .watch span').html(""+ (JSON.parse(sessionStorage.getItem('observed'))).length +"");

        console.log('observed', JSON.parse(sessionStorage.getItem('observed')));
    })


    //comparison
    $(document).on("change", ".Table .comparison input", function () {
        var id = $(this).parents('.TableRow').data('id');
        var compared = JSON.parse(sessionStorage.getItem('compared'));

        if($(this).is(":checked")){
            compared.push(id)
        } else{
            compared.splice( $.inArray(id, compared), 1 );
        }

        sessionStorage.setItem('compared', JSON.stringify(compared));
        $('.compare .comp span').html(""+ (JSON.parse(sessionStorage.getItem('compared'))).length +"");

        console.log('compared', JSON.parse(sessionStorage.getItem('compared')));
    })


    // cart
    $(document).on("click", ".Table .cart a", function (e) {
        e.preventDefault();
        var id = $(this).parents('.TableRow').data('id');
        var cart = JSON.parse(sessionStorage.getItem('cart'));

        if($.inArray(id, cart) ==  -1){
            cart.push(id)
            $(this).addClass('active');
        } else{
            cart.splice( $.inArray(id, cart), 1 );
            $(this).removeClass('active');
        }

        sessionStorage.setItem('cart', JSON.stringify(cart));
        $('#nav .cart span').html(""+ (JSON.parse(sessionStorage.getItem('cart'))).length +"");

        console.log('cart', JSON.parse(sessionStorage.getItem('cart')));
    })

    var s;
    var records = 20;
    var lock = 'false';
    var fetchResults = {

        sortData: {
            records: records,
            offset: 0,
            direction: "asc",
            orderBy: "shape"
        },


        init: function(){
            s = this.sortData;
            this.load();
            this.onChangeFilters();
            this.onScroll();
            this.onClear();
            this.onArrow();
        },

        load: function(){
            console.log( "LOAD") ;
            fetchResults.viewResults();
            s.offset = s.offset + s.records;
            $("#content-1").slimScroll({
                height: '400px',
                alwaysVisible: true,
                allowPageScroll: false
            });

        },

        onChangeFilters: function(){
            $('#filtr').on('change', function(e){
                console.log( "CHANGE") ;
                $.when($('.TableRow').remove()).then( function(){
                    s.offset = 0;
                    $(".slimScrollBar,.slimScrollRail").remove();
                    $(".slimScrollDiv").contents().unwrap();
                });

                fetchResults.load();

            })
        },

        onScroll: function(){

            $("#content-1").slimScroll().bind('slimscroll', function(e, pos){
                console.log("Reached " + pos);

                if(pos == "bottom"){
                    fetchResults.viewResults();
                    s.offset = s.offset + s.records;
                }
            });

        },

        onArrow: function(){
            $('.arrow').on('click', function(){
                console.log( "ARROW") ;

                var $this = $(this);

                $.when($('.TableRow').remove()).then( function(){
                    s.orderBy = $this.parent().attr('data-orderBy');
                    s.direction = $this.data('direction');
                    s.records = s.offset == 0 ? records : s.offset;
                    s.offset = 0;
                });

                fetchResults.load();

                s.offset = s.records;
                s.records = records;
            })
        },

        onClear: function(){
            $('a.reset-filters').click(function(e){
                e.preventDefault();
                console.log( "CLEAR") ;

                $rangePrice.data("ionRangeSlider").reset();
                $rangeMass.data("ionRangeSlider").reset();
                $rangeColor.data("ionRangeSlider").reset();
                $rangeClarity.data("ionRangeSlider").reset();
                $rangeAvailability.data("ionRangeSlider").reset();
                $rangeCut.data("ionRangeSlider").reset();
                $rangePolishing.data("ionRangeSlider").reset();
                $rangeSymmetry.data("ionRangeSlider").reset();

                $('#filtr input[type="checkbox"]:checked').click();


                $.when($('.TableRow').remove()).then( function(){
                    s.offset = 0;
                    s.direction = "asc";
                    s.orderBy = "shape";
                    s.records = records;
                });

                $(".slimScrollBar,.slimScrollRail").remove();
                $(".slimScrollDiv").contents().unwrap();

                fetchResults.load();

            })
        },

        viewResults: function () {
            if(lock == 'false'){
                lock = 'true';

            var filterParams = $('#filtr').serialize() + '&records=' + s.records + '&offset=' + s.offset + '&direction=' + s.direction + '&orderBy=' + s.orderBy;

            var table = $('.Table');

            $.get('webservice.php?' + filterParams, function (data) {
                $('.spinner').show();

                jQuery.each(data, function (k, v) {

                   // if(data.length < records){
                   //          s.offset = s.records;
                   //          console.log(s.offset, records,  data.length);
                   //      };

                    var tmpTr = $('<div data-id="' + v.id + '" class="TableRow">');
                    tmpTr.append('<div class="TableCell watch"><input type="checkbox" id="watch' + v.id + '" /><label for="watch' + v.id + '"><span></span></label></div>' +
                        '<div class="TableCell comparison"><input type="checkbox" id="comp' + v.id + '" value="' + v.id + '"><label for="comp' + v.id + '"><span></span></label></div>'
                    );

                    tmpTr.append('<div class="TableCell shape"><img src="'+v.image+'" alt="'+v.shape+'"/>' + v.shape + '</div>');

                    jQuery.each(v, function (kk, vv) {
                        switch (kk){
                            case 'id':
                            case 'image':
                            case 'shape':
                                break;
                            case 'price':
                                tmpTr.append('<div class="TableCell ' + kk + '">' + Number(vv).toLocaleString("pl-PL", { style: "currency", currency: "PLN" }) + ' PLN</div>')
                                break;
                            case 'availability':
                                tmpTr.append('<div class="TableCell ' + kk + '">' + vv + ' dni </div>')
                                break;
                            default:
                                tmpTr.append('<div class="TableCell ' + kk + '">' + vv + '</div>');

                        }
                    });

                    tmpTr.append('<div class="TableCell cart"><a href="#" data-id="' + v.id + '" title="Dodaj do koszyka">Dodaj do koszyka</a></div>')

                    table.append(tmpTr);
                })
            }, 'json').done(function () {

                var observed = JSON.parse(sessionStorage.getItem('observed'));
                var compared = JSON.parse(sessionStorage.getItem('compared'));


                $.each(observed, function (k, v) {
                    $('.Table').find('.TableRow[data-id="' + v + '"]').find('input[id="watch' + v + '"]').prop('checked', true);
                });

                $.each(compared, function (k, v) {
                    $('.Table').find('.TableRow[data-id="' + v + '"]').find('input[id="comp' + v + '"]').prop('checked', true);
                });

                setTimeout(function () {
                     $('.spinner').hide();
                     $('.TableRow').show();
                }, 500);

                lock = 'false';
            })

            }
        }
    };

    fetchResults.init();
});




