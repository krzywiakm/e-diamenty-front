$(document).ready(function(){

    // Zakres cen
    $rangePrice = $("#price");

    $rangePrice.ionRangeSlider({
        type: "double",
        min: 10,
        max: 1000000,
        from: 250000,
        to: 750000
    });

    $rangePrice.on("change", function () {
        var $this = $(this),
            from = $this.data("from"),
            to = $this.data("to");

        $('#price_from').val(from);
        $('#price_to').val(to);
    });
    
    // Masa
    $rangeMass = $("#mass");

    $rangeMass.ionRangeSlider({
        type: "double",
        min: 0,
        max: 5,
        from: 0.1,
        to: 2,
        step: 0.1,
    });

    $rangeMass.on("change", function () {
        var $this = $(this),
            from = $this.data("from"),
            to = $this.data("to");

        $('#mass_from').val(from);
        $('#mass_to').val(to);
    });

    if(localStorage.getItem('priceFrom')){

        if(localStorage.getItem('priceFrom') && localStorage.getItem('priceTo')){
            var sliderPrice = $rangePrice.data("ionRangeSlider");

            var priceFrom = localStorage.getItem('priceFrom');
            var priceTo = localStorage.getItem('priceTo');
            $('#price_from').val(priceFrom);
            $('#price_to').val(priceTo);
            sliderPrice.update({
                from: priceFrom,
                to: priceTo
            });
        }

        if(localStorage.getItem('massFrom') && localStorage.getItem('massTo')){
            var sliderMass = $rangeMass.data("ionRangeSlider");

            var massFrom = localStorage.getItem('massFrom');
            var massTo = localStorage.getItem('massTo');
            $('#mass_from').val(massFrom);
            $('#mass_to').val(massTo);
            sliderMass.update({
                from: massFrom,
                to: massTo
            });

        }

        localStorage.setItem('massFrom', $('#mass_from').val());
        localStorage.setItem('massTo', $('#mass_to').val());
    }

    window.onbeforeunload = function() {
        localStorage.setItem('massFrom', $('#mass_from').val());
        localStorage.setItem('massTo', $('#mass_to').val());
        localStorage.setItem('priceFrom', $('#price_from').val());
        localStorage.setItem('priceTo', $('#price_to').val());
    }
})
