$(document).ready(function () {

    console.log('observed', JSON.parse(sessionStorage.getItem('observed')));
    $('.result p span').html((JSON.parse(sessionStorage.getItem('observed'))).length);
    $('#nav .watch span').html("("+ (JSON.parse(sessionStorage.getItem('observed'))).length +")");
    $('#nav .cart span').html("("+ (JSON.parse(sessionStorage.getItem('cart'))).length +")");

    console.log('Storage', sessionStorage);
    console.log('observed', JSON.parse(sessionStorage.getItem('observed')));

    //observed
    $(document).on("click", ".Table .remove a", function (e) {
        e.preventDefault();
        var id = $(this).parents('.TableRow').data('id');
        var tableRow = $(this).parents('.TableRow');
        var observed = JSON.parse(sessionStorage.getItem('observed'));

        observed.splice( $.inArray(id, observed), 1 );
        tableRow.remove();


        sessionStorage.setItem('observed', JSON.stringify(observed));
        console.log('observed', JSON.parse(sessionStorage.getItem('observed')));

        $('#nav .watch span').html("("+ (JSON.parse(sessionStorage.getItem('observed'))).length +")");
        $('.result p span').html((JSON.parse(sessionStorage.getItem('observed'))).length);
    });


     $(document).on("click", ".result a.remove", function (e) {
         e.preventDefault();
         sessionStorage.setItem('observed', JSON.stringify([]));
         $('#nav .watch span').html("("+ (JSON.parse(sessionStorage.getItem('observed'))).length +")");
         $('.result p span').html((JSON.parse(sessionStorage.getItem('observed'))).length);
         $('.TableRow').remove();
    });

    // cart

    $(document).on("click", ".Table .cart a", function (e) {
        e.preventDefault();
        var id = $(this).parents('.TableRow').data('id');
        var cart = JSON.parse(sessionStorage.getItem('cart'));

        if($.inArray(id, cart) ==  -1){
            cart.push(id)
        } else{
            cart.splice( $.inArray(id, cart), 1 );
        }

        sessionStorage.setItem('cart', JSON.stringify(cart));
        $('#nav .cart span').html("("+ (JSON.parse(sessionStorage.getItem('cart'))).length +")");

        console.log('cart', JSON.parse(sessionStorage.getItem('cart')));
    });


    var s;
    var records = 20;
    var fetchResults = {

        sortData: {
            records: records,
            offset: 0,
            direction: "asc",
            orderBy: "shape"
        },


        init: function(){
            s = this.sortData;
            this.load();
            this.onArrow();
        },

        load: function(){
            console.log( "LOAD") ;
            fetchResults.viewResults();
            s.offset = s.offset + s.records;
        },

        onArrow: function(){
            $('.arrow').on('click', function(){
                console.log( "ARROW") ;

                var $this = $(this);

                $.when($('.TableRow').remove()).then( function(){
                    s.orderBy = $this.parent().attr('data-orderBy');
                    s.direction = $this.data('direction');
                    s.records = s.offset == 0 ? records : s.offset;
                    s.offset = 0;
                });

                fetchResults.load();

                s.offset = s.records;
                s.records = records;
            })
        },

        viewResults: function () {

            var observed = decodeURIComponent($.param( {ids: (sessionStorage.getItem('observed')).replace(/]|\[/g,'') } ));

            var filterParams = observed+'&records='+s.records+'&offset='+s.offset+'&direction='+s.direction+'&orderBy='+s.orderBy;

            var table = $('.Table');

            $.get('webservice.php?'+filterParams, function(data){

                jQuery.each(data, function(k,v){

                    var tmpTr = $('<div data-id="'+v.id+'" class="TableRow"/>');
                    tmpTr.append('<div class="TableCell remove"><a href="#" data-id="'+v.id+'" title="Usuń">X</a></div>');

                    tmpTr.append('<div class="TableCell shape"><img src="'+v.image+'" alt="'+v.shape+'"/>' + v.shape + '</div>');

                    jQuery.each(v, function(kk,vv){

                        return kk == 'id'|| kk == "shape" || kk == "image" ? '' :  tmpTr.append('<div class="TableCell '+kk+'">'+vv+'</div>');
                    });
                    tmpTr.append('<div class="TableCell cart"><a href="#" data-id="'+v.id+'" title="Dodaj do koszyka">Dodaj do koszyka</a></div>')

                    table.append(tmpTr);
                })
            }, 'json').done(function(){
                console.log('done!')
            })
        }
    }

    fetchResults.init();


});


