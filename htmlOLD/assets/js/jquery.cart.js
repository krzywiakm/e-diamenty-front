$(document).ready(function () {
    console.log('cart', JSON.parse(sessionStorage.getItem('cart')));
    $('.result p span').html((JSON.parse(sessionStorage.getItem('observed'))).length);
    $('#nav .watch span').html("("+ (JSON.parse(sessionStorage.getItem('observed'))).length +")");
    $('#nav .cart span').html("("+ (JSON.parse(sessionStorage.getItem('cart'))).length +")");


    console.log('Storage', sessionStorage,sessionStorage.getItem('cart'));
    console.log('cart', (JSON.parse(sessionStorage.getItem('cart'))).id);

    var cart = decodeURIComponent($.param( {ids: (sessionStorage.getItem('cart')).replace(/]|\[/g,'') } ));

    var filterParams = cart;
    var totalAmonut = 0;
    var totalDelivery = 0;

    $.get( "webservice.php?" + cart, function( data ) {

        var table = $('.cart-details');
        jQuery.each(data, function(k,v){

            window['item'+v.id] = counter();
            var price = Number(v.price);

            var tmpTr = $('<tr data-id="'+v.id+'" class="TableRow"/>');
            tmpTr.append('<td class="product-name">' +
                            '<img src="assets/images/cart/product.png" alt="product" class="alignleft" /> ' +
                            '<div class="product-detail"> ' +
                                '<h2>' +
                                    '<a href="#" title="#">0.25-Carat Round Diamond</a>' +
                                '</h2> ' +
                                '<p>This is Photoshops version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>' +
                            '</div> ' +
                        '</td> ' +
                        '<td class="product-availability">'+ v.availability+ '</td> ' +
                        '<td class="product-quantity"> ' +
                        '<div>'+
                            '<p data-id="'+v.id+'" class="quantity-message"></p>'+
                            '<input data-changeby ="minus" data-id="'+v.id+'" type="button" value="" class="qtyminus quantity" field="quantity" /> ' +
                            '<input data-id="'+v.id+'" type="text" name="quantity" value="'+window['item'+v.id].value()+'" class="qty quantity-value" /> ' +
                            '<input data-changeby="plus" data-id="'+v.id+'" type="button" value="" class="qtyplus quantity" field="quantity" /> ' +
                        '</div>'+
                        '</td> ' +
                        '<td data-id="'+v.id+'" class="product-price">' + price.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }) + ' </td> ' +
                        '<td data-id="'+v.id+'" class="product-value product-value-total"> ' + (price * window['item'+v.id].value()).toLocaleString("pl-PL", { style: "currency", currency: "PLN" }) + '</td> ' +
                        '<td class="product-remove"> <a href="#" title="Usuń produkt">X</a> ' + '</td>');

            table.append(tmpTr);
            totalAmonut += price * window['item'+v.id].value();
console.log(totalAmonut);
            totalDelivery = totalDelivery > Number(v.availability) ? totalDelivery : v.availability;

        });

        $('.price').html(totalAmonut.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));
        $('.day').html(totalDelivery);

    }, 'json');

    // Number((v.price).replace(/PLN/i, "")).toLocaleString("pl-PL", { style: "currency", currency: "PLN" })+


    $(document).on("click", ".quantity", function (e) {

        $('.quantity-message').html();

        var id = $(this).data('id');
        var price =   Number($('.product-price[data-id='+id+']').text().replace(/[^0-9\.]+/g,""))/100;
        var changeBy = $(this).data('changeby');

        var item = window['item'+id];

        if(changeBy == "plus"){

            $.get('webservice.php?id='+id+'quantity='+item.value(), function(data){
                data = 'success';
                if(data == 'success'){

                    item.increment();

                    $('.quantity-value[data-id='+id+']').val(item.value());
                    $('.product-value[data-id='+id+']').html((item.value() * price).toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));
                    totalAmonut+=price;
                    $('.price').html(totalAmonut.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));


                } else{
                    $('.quantity-message[data-id='+id+']').html("Nie wystarczająca ilość produktu");
                }
            })

        } else{

            if(item.value() == 0){
                return true

            } else{

                item.decrement();

                $('.quantity-value[data-id='+id+']').val(item.value());
                $('.product-value[data-id='+id+']').html((item.value() * price).toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));
                totalAmonut-=price;
                $('.price').html(totalAmonut.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));

            }

        }

    });


    var counter = function(){
        var privateCounter = 1;

        function changeBy(val){
            privateCounter +=val;
        }

        return {
            setValue: function(value){
                privateCounter = value;
            },

            increment: function() {
                changeBy(1);
            },
            decrement: function() {
                changeBy(-1);
            },
            value: function() {
                return privateCounter;
            }
        }
    }


    $(document).on("click", ".product-remove a", function (e) {
        e.preventDefault();
        var id = $(this).parents('.TableRow').data('id');
        var tableRow = $(this).parents('.TableRow');
        var cart = JSON.parse(sessionStorage.getItem('cart'));
        var delivery = Number(tableRow.find('.product-availability').text());
        var total = Number(tableRow.find('.product-value-total').text().replace(/[^0-9\.]+/g,""))/100;

        totalAmonut -=total;

        console.log(delivery, totalDelivery, totalAmonut, Number(totalAmonut));

        $('.price').html(totalAmonut.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));

        cart.splice( $.inArray(id, cart), 1 );
        tableRow.remove();

        if(totalDelivery == delivery){
            $('.product-availability').each(function(){
                totalDelivery = totalDelivery > Number($(this).text()) ? totalDelivery : Number($(this).text());
            });
        }

        $('.day').html(totalDelivery);

        sessionStorage.setItem('cart', JSON.stringify(cart));
        console.log('cart', JSON.parse(sessionStorage.getItem('cart')));

        $('#nav .cart span').html("("+ (JSON.parse(sessionStorage.getItem('cart'))).length +")");
    });

});