<?php
/**
 * Created by PhpStorm.
 * User: Wera
 * Date: 2016-04-12
 * Time: 19:00
 */

header('content-type:text/html;charset=utf-8');

$tab = array();

for($i=1; $i<=100; $i++){

    $shape = ["Round", "Oval", "Pear", "Marquise", "Cushion", "Emerald", "Princess", "Radiant", "Asscher"];
    $color = ["D", "E", "F", "G", "H", "I", "J", "K", "L", "M"];
    $clarity = ["FL", "IF", "VVS2", "VVS1", "VS1", "VS2", "SI1", "SI2", "I1", "I2", "I3"];
    $certificat = ["e-diamenty.pl", "GIA", "IGI", "HRD"];
    $options = ["Doskonały", "Bardzo dobry", "Dobry", "Zadowalający"];



    $tab[] =
         [
            "id"=> $i,
            "shape" => $shape[array_rand ($shape, 1)],
            "image" => ' assets/images/cart/diamond.png',
            "mass" => rand(0,5),
            "color" => $color[array_rand($color, 1)],
            "clarity" => $clarity[array_rand($clarity, 1)],
            "certificat" => $certificat[array_rand($certificat,1)],
            "availability" => rand(1,30),
            "symmetry" => $options[array_rand($options, 1)],
            "cut" => $options[array_rand($options, 1)],
            "polishing" => $options[array_rand($options, 1)],
            "price" => rand(10, 1000000)
        ];
};


$data = json_encode($tab, JSON_UNESCAPED_UNICODE);

file_put_contents('database.json',$data);