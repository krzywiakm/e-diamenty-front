<?php

if (!is_file('database.json'))
    die('file does not exist');

$data = file_get_contents('database.json');

$data = json_decode($data, true);

if(isset($_GET['ids'])){
    $ids = explode(',', $_GET['ids']);

   $data = array_filter($data, function($a) use ($ids){

        foreach ($ids as $id){
            if($a['id'] == $id)
            {
                return true;
            }

        }
    });

}

    //$_GET zabezpieczyć
    $offset = isset($_GET['offset']) ? $_GET['offset'] : "";
    $records = isset($_GET['records']) ? $_GET['records'] : "";
    $orderBy = isset($_GET['orderBy'])? $_GET['orderBy'] : "";
    $direction = isset($_GET['direction'])? $_GET['direction'] : "";

//    usort($data, function($a, $b) use($orderBy){
//        return strcmp($a[$orderBy], $b[$orderBy]);
//    });
//
//    $data = $direction == 'desc' ? array_reverse($data) : $data;

if(isset($_GET['shape'])){

    //rangeSlider filter mass
    $massFrom = $_GET['mass_from'];
    $massTo = $_GET['mass_to'];


    $data = array_filter($data, function($a) use ($massFrom, $massTo){
        if( $a['mass'] >= $massFrom && $a['mass'] <= $massTo ){
            return $a;
        }
    });

}

if(isset($_GET['offset'])){
    $data = array_slice($data, $offset, $records);
}


$data = json_encode($data, JSON_HEX_AMP);


die($data);

