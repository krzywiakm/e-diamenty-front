$(document).ready(function(){


    var massParamInit = new function() {
        this.name = 'mass';
        this.divider = 100;
        //database value, number format
        this.min = 0.01;
        this.max = 10;
    };

    var priceParamInit = new function() {
        this.name = 'price';
        this.divider = 1;
        //database value, number format
        this.min = 50;
        this.max = 13000000;
    };

    function Grid(paramInit){

        this.name = paramInit.name;
        this.divider = paramInit.divider;
        this.min = paramInit.min;
        this.max = paramInit.max;
        this.maxO = paramInit.max * paramInit.divider;
        this.minO = paramInit.min * paramInit.divider;
        this.values = []
    }

    Grid.prototype.recursiveGenerateGrid = function (digits) {

        var self = this;
        var compare = digits;
        var roundtoOne = Number(1 + '0'.repeat(compare - 1));
        var min = Math.ceil(this.minO/roundtoOne)*roundtoOne;
        var step;

        switch(this.name){
            case "mass": step = calculateStepforMass(digits, compare); break;
            case "price": step = calculateStepforPrice(digits, compare); break;
        }

        min = min < step ? step : min;
        console.log(step, min);
        for (i = min; i <= this.maxO; i += step) {
            compare = i.toString().length;
            if (digits != compare) {
                min = i;
                return this.recursiveGenerateGrid(compare) ;
            }
            this.values.push(i/this.divider)
        }
    };

    Grid.prototype.getValues = function(){
        this.values.indexOf(this.min) < 0 ? this.values.unshift(this.min)  : "";
        this.values.indexOf(this.max) < 0 ? this.values.push(this.max) : "";
        return this.values
    };

    function calculateStepforMass(digits, compare){
        return digits % 2 == 0 ? (digits < 3 ? Number(5 + '0'.repeat(compare - 1)/2) :  Number(1 + '0'.repeat(compare - 2)) ) : (digits < 4 ? Number(1 + '0'.repeat(compare - 1)) : Number(1 + '0'.repeat(compare - 2))) ;
    }

    function calculateStepforPrice(digits, compare){
        return digits % 2 == 0 ? (digits < 3 ? Number(5 + '0'.repeat(compare - 1)) :  (digits < 8 ) ? Number(5 + '0'.repeat(compare - 2)) : Number(5 + '0'.repeat(compare - 3)) ) : (digits < 4 ? Number(1 + '0'.repeat(compare - 1)) : Number(1 + '0'.repeat(compare - 2)) ) ;
    }

    var mass = new Grid(massParamInit);
    mass.recursiveGenerateGrid(mass.minO.toString().length);

    var price = new Grid(priceParamInit);
    price.recursiveGenerateGrid(price.minO.toString().length);

    var priceValues = price.getValues();
    var massValues = mass.getValues();

    console.log('mass.values', mass.getValues(),  massValues.indexOf(0.1),  massValues.indexOf(2));
    console.log('price.values', price.getValues(), priceValues.indexOf(50), priceValues.indexOf(750000));

    // Zakres cen
    $rangePrice = $("#price");

    $rangePrice.ionRangeSlider({
        type: "double",
        min: priceParamInit.min,
        max: priceParamInit.max,
        from: priceValues.indexOf(50),
        to: priceValues.indexOf(750000),
        values: priceValues
    });



    $rangePrice.on("change", function () {
        var $this = $(this),
            from = $this.data("from"),
            to = $this.data("to");

        $('#price_from').val(priceValues[from]);
        $('#price_to').val(priceValues[to]);
    });
    
    // Masa
    $rangeMass = $("#mass");

    $rangeMass.ionRangeSlider({
        type: "double",
        min: massParamInit.min,
        max: massParamInit.max,
        from: massValues.indexOf(0.1),
        to: massValues.indexOf(2),
        values: massValues
    });

    $rangeMass.on("change", function () {
        var $this = $(this),
            from = $this.data("from"),
            to = $this.data("to");

        $('#mass_from').val(massValues[from]);
        $('#mass_to').val(massValues[to]);
    });

    if(localStorage.getItem('priceFrom')){

        if(localStorage.getItem('priceFrom') && localStorage.getItem('priceTo')){
            var sliderPrice = $rangePrice.data("ionRangeSlider");

            var priceFrom = localStorage.getItem('priceFrom');
            var priceTo = localStorage.getItem('priceTo');
            $('#price_from').val(priceFrom);
            $('#price_to').val(priceTo);
            sliderPrice.update({
                from: priceValues.indexOf(Number(priceFrom)),
                to: priceValues.indexOf(Number(priceTo))
            });
        }

        if(localStorage.getItem('massFrom') && localStorage.getItem('massTo')){
            var sliderMass = $rangeMass.data("ionRangeSlider");

            var massFrom = localStorage.getItem('massFrom');
            var massTo = localStorage.getItem('massTo');
            $('#mass_from').val(massFrom);
            $('#mass_to').val(massTo);
            sliderMass.update({
                from: massValues.indexOf(Number(massFrom)),
                to: massValues.indexOf(Number(massTo))
            });

        }

        localStorage.setItem('massFrom', $('#mass_from').val());
        localStorage.setItem('massTo', $('#mass_to').val());
    }

    window.onbeforeunload = function() {
        localStorage.setItem('massFrom', $('#mass_from').val());
        localStorage.setItem('massTo', $('#mass_to').val());
        localStorage.setItem('priceFrom', $('#price_from').val());
        localStorage.setItem('priceTo', $('#price_to').val());
    };




    // localStorage.removeItem('priceTo');
    // localStorage.removeItem('priceFrom');
    // localStorage.removeItem('massTo');
    // localStorage.removeItem('massFrom');

});
