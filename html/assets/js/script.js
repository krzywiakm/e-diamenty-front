(function($) {
    
        $('.bx-cart').bxSlider({
            mode: 'vertical',
            controls: true,
            pager: false,
            auto: false,
            nextSelector: '#slider-next',
            prevSelector: '#slider-prev',
            minSlides: 2,
            maxSlides: 2,
            adaptiveHeight: true
        });

  $.fn.menumaker = function(options) {
      
      var cssmenu = $(this), settings = $.extend({
        title: "",
        format: "dropdown",
        breakpoint: 992,
        sticky: false
      }, options);

      return this.each(function() {
        cssmenu.find('li ul').parent().addClass('has-sub');
        if (settings.format != 'select') {
          cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
          $(this).find("#menu-button").on('click', function(){
            $(this).toggleClass('menu-opened');
            var mainmenu = $(this).next('ul');
            if (mainmenu.hasClass('open')) { 
              mainmenu.hide().removeClass('open');
            }
            else {
              mainmenu.show().addClass('open');
              if (settings.format === "dropdown") {
                mainmenu.find('ul').show();
              }
            }
          });

          multiTg = function() {
            cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
            cssmenu.find('.submenu-button').on('click', function() {
              $(this).toggleClass('submenu-opened');
              if ($(this).siblings('ul').hasClass('open')) {
                $(this).siblings('ul').removeClass('open').hide();
              }
              else {
                $(this).siblings('ul').addClass('open').show();
              }
            });
          };

          if (settings.format === 'multitoggle') multiTg();
          else cssmenu.addClass('dropdown');
        }

        else if (settings.format === 'select')
        {
          cssmenu.append('<select style="width: 100%"/>').addClass('select-list');
          var selectList = cssmenu.find('select');
          selectList.append('<option>' + settings.title + '</option>', {
                                                         "selected": "selected",
                                                         "value": ""});
          cssmenu.find('a').each(function() {
            var element = $(this), indentation = "";
            for (i = 1; i < element.parents('ul').length; i++)
            {
              indentation += '-';
            }
            selectList.append('<option value="' + $(this).attr('href') + '">' + indentation + element.text() + '</option');
          });
          selectList.on('change', function() {
            window.location = $(this).find("option:selected").val();
          });
        }

        if (settings.sticky === true) cssmenu.css('position', 'fixed');

        resizeFix = function() {
          if ($(window).width() > settings.breakpoint) {
            cssmenu.find('ul').show();
            cssmenu.removeClass('small-screen');
            if (settings.format === 'select') {
              cssmenu.find('select').hide();
            }
            else {
              cssmenu.find("#menu-button").removeClass("menu-opened");
            }
          }

          if ($(window).width() <= settings.breakpoint && !cssmenu.hasClass("small-screen")) {
            cssmenu.find('ul').hide().removeClass('open');
            cssmenu.addClass('small-screen');
            if (settings.format === 'select') {
              cssmenu.find('select').show();
            }
          }
        };
        resizeFix();
        return $(window).on('resize', resizeFix);

      });
  };
})(jQuery);

(function($){
    $(document).ready(function(){

        $('.choose').css('display', 'none');

        $('.slidedown').on({
            mouseover: function(){
                $(this).find('.choose').stop().slideDown();
            },

            mouseleave: function(){
                $(this).find('.choose').stop().slideUp();
            }
        });


        // responsive menu 
        $(window).load(function() {
            $("#cssmenu").menumaker({
                title: "",
                format: "dropdown"
            });
        });
        
        // add class to scroll
        $(window).on('scroll load', function() {
            var totalScroll = $(window).scrollTop();
            var headerHeight = $('.sticky').height();
            if (totalScroll >= 125) {
                $('.sticky').addClass('scroll');
                $('.static').addClass('noscroll');
            } else {
                $('.sticky').removeClass('scroll');
                $('.static').removeClass('noscroll');
            }
        });
        
        // start tooltip
    	$('.tooltip').tooltipster({
    		trigger: 'click',
            position: 'right',
            maxWidth: 350,
            theme: 'tooltipster-light',
            interactive: true,
            animation: 'grow',
            touchDevices: true
    	});
        
    	$(window).keypress(function() {
    		$('.tooltip').tooltipster('hide');
    	});

    });
})(jQuery);
