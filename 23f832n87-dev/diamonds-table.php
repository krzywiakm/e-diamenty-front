<? include 'api/func.php'; ?>
<div id="diamonds-table">
    <!-- start / filtr -->
    <div class="wrapper row">
        <div class="col-md-12">
            <form id="filtr">
                <div id="basic-filtr">
                    <div style="margin-bottom: 35px;" class="row filtr1">
                        <div class="col-md-4 col-sm-12 col-xs-12 single">
                            <label>
                                <?= slownik(64) ?> <span>(PLN)</span>
                                <i class="info tooltip" title="<?= slownik(69) ?>"></i>
                            </label>
                            <input id="price" />
                            <input type="text" id="price_from" name="price_from" value="1" class="input-filtr" />
                            <input type="text" id="price_to" name="price_to" value="<?= getHighestPrice(); ?>" class="input-filtr" />
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 single">
                            <label>
                                <?= slownik(56) ?> <span>(Carat)</span>
                                <i class="info tooltip" title="<?= slownik(70) ?>"></i>
                            </label>
                            <input id="mass" />
                            <input type="text" id="mass_from" name="mass_from" value="0.01" class="input-filtr" />
                            <input type="text" id="mass_to" name="mass_to" value="<?= getHighestMass(); ?>" class="input-filtr" />
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 filter-availability single mobile-advanced-filters">
                            <label>
                                <?= slownik(60) ?> <span>(Liczba dni)</span>
                                <i class="info tooltip" title="<?= slownik(73) ?>"></i>
                            </label>
                            <input id="availability" />
                            <input type="text" id="availability_from" name="availability_from" value="0" class="input-filtr" readonly />
                            <input type="text" id="availability_to" name="availability_to" value="30" class="input-filtr" readonly />
                        </div>
                    </div>
                </div>
            </form>
            <span class="catalog-scroll-down"></span>
        </div>
    </div>
    <!-- end / filtr -->
    
    <!-- end / table catalog -->
    <div class="wrapper row table">
        <div class="col-md-12">
            <div class="table-responsive">
                <!-- start / cube animate for search result -->
                <div class="spinner">
                     <div class="cube1"></div>
                     <div class="cube2"></div>
                </div>
                <div class="TableHeading">
                    <div data-orderBy="shape" class="TableHead head-shape"><?= slownik(55) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="mass" class="TableHead head-mass"><?= slownik(56) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="color" class="TableHead head-color"><?= slownik(57) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="clarity" class="TableHead head-clarity"><?= slownik(58) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="certificat" class="TableHead head-certificat"><?= slownik(59) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="availability" class="TableHead head-availability"><?= slownik(60) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="symmetry" class="TableHead head-symmetry"><?= slownik(61) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="cut" class="TableHead head-cut"><?= slownik(62) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="polishing" class="TableHead head-polishing"><?= slownik(63) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="price" class="TableHead head-price"><?= slownik(64) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                </div>
                <!-- end / cube animate for search result -->
                <div id="content-1">
                    <div class="Table">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end / table catalog -->
</div>