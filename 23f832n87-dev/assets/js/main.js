$(".section-img").click(function(e){
    $(".section-heading").fadeOut();
    $(".section-img").fadeOut();
    $(this).fadeIn();
    $(".page-content").fadeIn();
    $("#history-btn").hide();
    $("#back-btn").show();
});

$("#back-btn").click(function(e){
    $(".section-heading").fadeIn();
    $(".section-img").fadeIn();
    $(".page-content").fadeOut();
    $("#history-btn").show();
    $("#choose-diamond").fadeOut();
    $("#back-btn").hide();
});

$(".open-product-images").click(function(e){
    $(".product-images").fadeOut();
    $(".product-images").fadeIn();
});

$(".open-choose-diamond").click(function(e){
    $(".section-heading").fadeOut();
    $(".section-img").fadeOut();
    $(".page-content").fadeOut();
    $("#choose-diamond").fadeIn();
    $("#back-btn").show();
});
