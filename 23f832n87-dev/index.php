<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap/bootstrap-theme.min.css">
        <link rel="stylesheet" href="assets/css/ediamenty/component.css">
        <link rel="stylesheet" href="assets/css/ediamenty/grid.css">
        <link rel="stylesheet" href="assets/css/ediamenty/ion.rangeSlider.css">
        <link rel="stylesheet" href="assets/css/ediamenty/ion.rangeSlider.skinNice.css">
        <link rel="stylesheet" href="assets/css/ediamenty/jquery.bxslider.css">
        <link rel="stylesheet" href="assets/css/ediamenty/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="assets/css/ediamenty/normalize.css">
        <link rel="stylesheet" href="assets/css/ediamenty/perfect-scrollbar.min.css">
        <link rel="stylesheet" href="assets/css/ediamenty/responsive.css">
        <link rel="stylesheet" href="assets/css/ediamenty/style.css">
        <link rel="stylesheet" href="assets/css/ediamenty/tooltipster.css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin-ext" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/main.css">

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <nav class="navbar main-navbar" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <div class="col-xs-4">
                <ul class="nav navbar-nav full-box">
                    <li id="back-btn">
                        <a class="btn-yes" href="#">WSTECZ</a>
                    </li>
                    <li id="history-btn">
                        <a class="btn-yes" href="#">HISTORIA ZAMÓWIEŃ</a>
                    </li>
                </ul>
              </div>
              <div class="col-xs-4 logo-container">
                <img src="assets/images/logo.png" alt="">
              </div>
              <div class="col-xs-4">
                
              </div>
            </div><!--/.navbar-collapse -->
          </div>
        </nav>

        <div class="container-fluid full-box">
          <div class="row">
            <div class="col-xs-12">
              <h2 style="margin-top:40px;;margin-bottom:40px;" class="section-heading">WYBIERZ KOLEKCJĘ</h2>
              <img class="section-img" src="assets/images/metropolitan.png" alt="">
              <img class="section-img" src="assets/images/eternal.png" alt="">
              <img class="section-img" src="assets/images/stella.png" alt="">
            </div>
          </div>
        </div>

        <div class="container page-content">
          <div class="row">
            <div class="col-xs-12">
              <h2 style="text-align:center;">Wybierz metal</h2>
              <div class="row">
                <div style="text-align: center;margin-top: 20px;" class="col-xs-12">
                  <a class="btn-yes open-product-images" href="#">ŻÓŁTE ZŁOTO</a>
                  <a class="btn-yes open-product-images" href="#">BIAŁE ZŁOTO</a>
                  <a class="btn-yes open-product-images" href="#">PLATYNA</a>
                </div>
              </div>
            </div>
          </div>
          <div style="margin-top: 20px;" class="row product-images">
            <div class="col-xs-12">
              <h2 style="text-align:center;">Wybierz produkt</h2>
              <div class="row">
                <div style="text-align: center;margin-top: 20px;" class="col-xs-12">
                  <img class="clickable open-choose-diamond" src="assets/images/product1.png" alt="">
                  <img class="clickable open-choose-diamond" src="assets/images/product2.png" alt="">
                  <img class="clickable open-choose-diamond" src="assets/images/product3.png" alt="">
                  <img class="clickable open-choose-diamond" src="assets/images/product4.png" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="choose-diamond" class="container">
          <div class="row">
            <div class="col-xs-3">
              <img src="assets/images/product1.png" alt="">
            </div>
            <div style="padding-top: 30px;" class="col-xs-3">
              <h1>Eternal</h1>
            </div>
            <div style="padding-top: 30px;" class="col-xs-3">
              <div class="big-price">Cena: 5000 PLN</div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <?php include 'diamonds-table.php';?>
            </div>
          </div>
        </div>


        <script src="https://code.jquery.com/jquery-2.2.2.min.js" integrity="sha256-36cp2Co+/62rEAAYHLmRCPIych47CvdM+uTBJwSzWjI=" crossorigin="anonymous"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/ediamenty/ion.rangeSlider.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.7/jquery.slimscroll.min.js"></script>
        <script src="assets/js/ediamenty/jquery.sliderRangeStorage.js"></script>
        <script src="assets/js/ediamenty/jquery.diamond.js"></script>
        <script src="assets/js/ediamenty/jquery.bxslider.js"></script>

        <script src="assets/js/ediamenty/whcookies.js"></script>
      
        <script src="assets/js/main.js"></script>
    </body>
</html>
