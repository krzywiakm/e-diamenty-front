<?

if (!isset($_SESSION['inicjuj'])) {
  session_regenerate_id();
  $_SESSION['inicjuj'] = true;
  $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
  $_SESSION['czas'] = time();
  $_SESSION['KOSZYK'] = 0;
}

define("SESID", SESSION_NAME() . "=" . SESSION_ID());


# logowanie, wylogowanie itp
if ($_POST['loguj']) {
  $login  = htmlentities(substr($_POST["email"], 0, 255));
  $passwd = htmlentities(substr($_POST["haslo"], 0, 255));
  login($login, sha1($passwd));
}
    
if ($_POST['logout']) {
  logout();
  session_destroy();
}

?>
