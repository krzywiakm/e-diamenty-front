<?php
include "func.php";

$query = "SELECT KSZTALTY.Id, SLOWNIK.Pl, SLOWNIK.En
FROM KSZTALTY
INNER JOIN SLOWNIK
ON KSZTALTY.IdSlownik=SLOWNIK.Id;";
$result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");
$ksztalty = array();
while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
{
    $ksztalty[$line['Id']] = $line;
}

$query = "SELECT SYMETRIE.Id, SLOWNIK.Pl, SLOWNIK.En
FROM SYMETRIE
INNER JOIN SLOWNIK
ON SYMETRIE.IdSlownik=SLOWNIK.Id;";
$result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");
$symetrie = array();
while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
{
    $symetrie[$line['Id']] = $line;
}

$query = "SELECT SZLIFY.Id, SLOWNIK.Pl, SLOWNIK.En
FROM SZLIFY
INNER JOIN SLOWNIK
ON SZLIFY.IdSlownik=SLOWNIK.Id;";
$result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");
$szlify = array();
while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
{
    $szlify[$line['Id']] = $line;
}

$query = "SELECT POLEROWANIA.Id, SLOWNIK.Pl, SLOWNIK.En
FROM POLEROWANIA
INNER JOIN SLOWNIK
ON POLEROWANIA.IdSlownik=SLOWNIK.Id;";
$result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");
$polerowania = array();
while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
{
    $polerowania[$line['Id']] = $line;
}

$query = "SELECT * FROM BARWY";
$result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");
$barwy = array();
$barwy_nazwy = array();
while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
{
    $barwy[$line['Id']] = $line;
    $barwy_nazwy[$line['Nazwa']] = $line;
}

$query = "SELECT * FROM CZYSTOSCI";
$result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");
$czystosci = array();
$czystosci_nazwy = array();
while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
{
    $czystosci[$line['Id']] = $line;
    $czystosci_nazwy[$line['Nazwa']] = $line;
}

$query = "SELECT * FROM LABORATORIA";
$result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");
$certyfikaty = array();
$certyfikaty_nazwy = array();
while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
{
    $certyfikaty[$line['Id']] = $line;
    $certyfikaty_nazwy[$line['Nazwa']] = $line;
}

//$_GET zabezpieczyć
$offset = isset($_GET['offset']) ? $_GET['offset'] : "0";
$records = isset($_GET['records']) ? $_GET['records'] : "40";
$orderBy = isset($_GET['orderBy'])? $_GET['orderBy'] : "price";
$direction = isset($_GET['direction'])? $_GET['direction'] : "asc";
$mass_from = isset($_GET['mass_from'])? $_GET['mass_from'] : "0";
$mass_to = isset($_GET['mass_to'])? $_GET['mass_to'] : "10";
$price_from = isset($_GET['price_from'])? $_GET['price_from'] : "0";
$price_to = isset($_GET['price_to'])? $_GET['price_to'] : "750000";
$color_from = isset($_GET['color_from'])? $barwy_nazwy[$_GET['color_from']]["Id"] : "1";
$color_to = isset($_GET['color_to'])? $barwy_nazwy[$_GET['color_to']]["Id"] : "10";
$clarity_from = isset($_GET['clarity_from'])? $czystosci_nazwy[$_GET['clarity_from']]["Id"] : "2";
$clarity_to = isset($_GET['clarity_to'])? $czystosci_nazwy[$_GET['clarity_to']]["Id"] : "11";
$cut_from = isset($_GET['cut_from'])? $_GET['cut_from'] : "1";
$cut_to = isset($_GET['cut_to'])? $_GET['cut_to'] : "4";
$polishing_from = isset($_GET['polishing_from'])? $_GET['polishing_from'] : "1";
$polishing_to = isset($_GET['polishing_to'])? $_GET['polishing_to'] : "4";
$symmetry_from = isset($_GET['symmetry_from'])? $_GET['symmetry_from'] : "1";
$symmetry_to = isset($_GET['symmetry_to'])? $_GET['symmetry_to'] : "4";
$availability_from = isset($_GET['availability_from'])? $_GET['availability_from'] : "0";
$availability_to = isset($_GET['availability_to'])? $_GET['availability_to'] : "30";

if ($color_to == '10') {
    $color_to = '23';
}

$sort_color = array();
$params = explode('&', $_SERVER['QUERY_STRING']);
foreach ($params as $param) {
    $name = explode('=', $param)[0];
    $value = explode('=', $param)[1];
    $value = str_replace('s', '', $value);
    if ($name == 'sort_colour') {
        $sort_color[] = $value;
    }
}

if(count($sort_color) < 1) {
    $sort_color_in = "";
}
else {
    $sort_color_in = "(Kolor IN (" . implode(',', $sort_color) . ")) AND ";
}

$sort_shapes = array();
$params = explode('&', $_SERVER['QUERY_STRING']);
foreach ($params as $param) {
    $name = explode('=', $param)[0];
    $value = explode('=', $param)[1];
    $value = str_replace('s', '', $value);
    if ($name == 'sort_shape') {
        if ($value == '2') {
            $value = '9';
        }
        else if ($value == '5') {
            $value = '2';
        }
        else if ($value == '6') {
            $value = '11';
        }
        else if ($value == '7') {
            $value = '5';
        }
        else if ($value == '9') {
            $value = '6';
        }
        else if ($value == '11') {
            $value = '7';
        }
        $sort_shapes[] = $value;
    }
}

if(count($sort_shapes) < 1) {
    $sort_shapes_in = '';
}
else {
    $sort_shapes_in = "(Ksztalt IN (" . implode(',', $sort_shapes) . ")) AND ";
}

$cert_list = array();
$cert_e_diamenty = "";
if (isset($_GET['certificate_gia'])) {
    $cert_list[] = '1';
}
if (isset($_GET['certificate_igi'])) {
    $cert_list[] = '2';
}
if (isset($_GET['certificate_hrd'])) {
    $cert_list[] = '3';
}
if (isset($_GET['certificate_e_diamenty'])) {
    $cert_e_diamenty = " OR Laboratorium IS NULL";
}
if(count($cert_list) < 1) {
    $lab_in = '';
}
else {
    $lab_in = "(Laboratorium IN (" . implode(',', $cert_list) . ")" . $cert_e_diamenty . ") AND ";
}

if ($direction == 'asc') {
    $direction = 'ASC';
}
elseif ($direction == 'desc') {
    $direction = 'DESC';
}
else {
    $direction = 'ASC';
}

if ($orderBy == 'shape') {
   $orderBy = 'Ksztalt';
}
elseif ($orderBy == 'mass') {
    $orderBy = 'Masa';
}
elseif ($orderBy == 'color') {
    $orderBy = 'Barwa';
}
elseif ($orderBy == 'clarity') {
    $orderBy = 'Czystosc';
}
elseif ($orderBy == 'certificat') {
    $orderBy = 'Laboratorium';
}
elseif ($orderBy == 'availability') {
    $orderBy = 'Czas';
}
elseif ($orderBy == 'symmetry') {
    $orderBy = 'Symetria';
}
elseif ($orderBy == 'polishing') {
    $orderBy = 'Polerowanie';
}
elseif ($orderBy == 'cut') {
    $orderBy = 'Szlif';
}
elseif ($orderBy == 'price') {
    $orderBy = 'Brutto';
}
else {
   $orderBy = 'Id';
}

$query = "SELECT p.Id, p.DataDodania, p.Ksztalt, p.Masa, p.Barwa, p.Kolor, p.Czystosc, p.Sklep, p.StockNumber, p.Symetria, p.Polerowanie, p.Szlif, p.Laboratorium, p.Brutto, p.Pokaz, p.RodzajZestawienia, z.Czas FROM PRODUKTY AS p INNER JOIN ZESTRODZ AS z ON p.RodzajZestawienia=z.Nr " . 
"WHERE (Czas IS NOT NULL) AND (Brutto BETWEEN " . $price_from . " AND " . $price_to . ") AND " . 
"(Czas BETWEEN " . $availability_from . " AND " . $availability_to . ") AND " .
"(Masa BETWEEN " . $mass_from . " AND " . $mass_to . ") AND " .
"(Czystosc BETWEEN " . $clarity_from . " AND " . $clarity_to . ") AND " .
"(Barwa BETWEEN " . $color_from . " AND " . $color_to . ") AND " .
"((Szlif BETWEEN " . $cut_from . " AND " . $cut_to . ") OR Szlif IS NULL) AND " .
"((Polerowanie BETWEEN " . $polishing_from . " AND " . $polishing_to . ") OR Polerowanie IS NULL) AND " .
"((Symetria BETWEEN " . $symmetry_from . " AND " . $symmetry_to . ") OR Symetria IS NULL) AND " .
$lab_in .
$sort_shapes_in .
$sort_color_in .
"(Pokaz = '1' ) AND " .
"(Idealny = '0' ) " .
"ORDER BY " . $orderBy . ' ' . $direction . 
" LIMIT " . $offset . ", " . $records;

if(isset($_GET['ids'])){
    $query = "SELECT p.Id, p.DataDodania, p.Ksztalt, p.Masa, p.Barwa, p.Kolor, p.Czystosc, p.Sklep, p.StockNumber, p.Symetria, p.Polerowanie, p.Szlif, p.Laboratorium, p.Brutto, p.Pokaz, p.RodzajZestawienia, z.Czas FROM PRODUKTY AS p INNER JOIN ZESTRODZ AS z ON p.RodzajZestawienia=z.Nr " . 
    "WHERE p.Id IN (" . $_GET['ids'] . ")";

    if(isset($_GET['cart'])){
        if ($_GET['cart'] == true) {
            $_SESSION["KOSZYK"] = $_GET['ids'];
            $_SESSION["KOSZYK-QTY"] = $_GET['qty'];
        }
    }
}

$result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");

$data = array();
while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
{
    $product = array(
        "id" => $line['Id'],
        "shape" => $ksztalty[$line['Ksztalt']]['Pl'],
        "url" => BASE_URL . "/sklep/produkt/" . $line['Id'],
        "image" => getDiamondMiniPhotoUrl($line['Ksztalt']),
        "mass" => formatMass($line['Masa']),
        "color" => formatData($barwy[$line['Barwa']]['Nazwa']),
        "clarity" => formatData($czystosci[$line['Czystosc']]['Nazwa']),
        "certificat" => formatCert($certyfikaty[$line['Laboratorium']]['Nazwa']),
        "availability" => formatData($line['Czas']),
        "symmetry" => formatData($symetrie[$line['Symetria']]['Pl']),
        "cut" => formatData($szlify[$line['Szlif']]['Pl']),
        "polishing" => formatData($polerowania[$line['Polerowanie']]['Pl']),
        "price" => formatPrice($line['Brutto']),
        "shortDesc" => ""
    );
    if ($product["certificat"] == 'e-diamenty.pl') {
        $product["symmetry"] = formatData($symetrie[2]['Pl']);
        $product["cut"] = formatData($szlify[2]['Pl']);
        $product["polishing"] = formatData($polerowania[2]['Pl']);
    }

    if ($product["shape"] == 'Szmaragdowy') {
        $product["shape"] = 'Szmarag.';
    }

    if($product['availability'] == 0) { 
        $avail = 'Od ręki';
    } else { 
        $avail = $product['availability'] . ' dni';
    }
    $short_desc = 'Kształt: ' . $product['shape'] . ', ' .
    'Masa: ' . $product['mass'] . ', ' .
    'Barwa: ' . $product['color'] . ', ' .
    'Czystość: ' . $product['clarity'] . ', ' .
    'Certyfikat: ' . $product['certificat'] . ', ' .
    'Dostępność: ' . $avail . ', ' .
    'Szlif: ' . $product['cut'] . ', ' .
    'Symetria: ' . $product['symmetry'] . ', ' .
    'Polerowanie: ' . $product['polishing'];

    $product['shortDesc'] = $short_desc;

    $data[] = $product;
}


$data = array_filter($data, function($a) {
    $product_qty = getProductStockQuantity($a['id']);
    if( $product_qty > 0 || $a['certificat'] != 'e-diamenty.pl' ){
        if ($a['certificat'] == 'e-diamenty.pl') {
            return $a;
        }
        else if ( ($a['certificat'] != 'e-diamenty.pl') and (intval($a['availability']) == 0) and ($product_qty > 0) ) {
            return $a;
        }
        else if ( ($a['certificat'] != 'e-diamenty.pl') and (intval($a['availability']) > 0) ) {
            return $a;
        }
    }
});

$data = json_encode($data, JSON_HEX_AMP);


die($data);

