<? $page_name = basename(__FILE__, '.php'); ?>
<? include 'api/func.php'; ?>
<? include 'api/auth.php'; ?>
<? include 'api/yes.php'; ?>
<? 
if (isset($_GET["collection-id"])) {
    $collectionId = intval($_GET["collection-id"]);
    $kolekcja = getYesKolekcjeById($_GET["collection-id"]);
}
if (!isset($_POST["Zaliczka"])) {
    $_POST["Zaliczka"] = 0;
}
$price = calculateTheTotalPrice(intval($_GET['collection-id']), intval($_GET['metal-id']), intval($_GET['product-type']), intval($_GET['diamond1']), intval($_GET['diamond2']));
$transactionId = createTransaction(); 
$diamond1 = getProduct(intval($_GET['diamond1']));
$productImgUrl = productImgUrl($_GET["collection-id"], $_GET["metal-id"], $_GET["product-type"], floatval($diamond1['Masa']));
$czasRealizacji = getYesProductDeliveryTime($_GET["collection-id"], $_GET["metal-id"], $_GET["product-type"], dostepnosc($diamond1['RodzajZestawienia'])) . ' dni';
$orderProductFullName = getYesProductFullName(intval($_GET['collection-id']), intval($_GET['metal-id']), intval($_GET['product-type']), intval($_GET['diamond1']), intval($_GET['diamond2']));
?>
<? if ($transactionId != null) {
    header('Location: order-summary?id=' . $transactionId);
    exit();
} ?>
<? include 'includes/header.php'; ?>
<!-- start / main  -->
<section id="main" class="cart steps">
    <!-- start / cart - step 1 -->
    <div class="wrapper row">
        <div class="col-xs-12">
            <h1 style="margin-bottom: 18px;">Zamówienie</h1>
            <div class="row">
              <div class="col-xs-2">
                <img src="<?= $productImgUrl ?>" alt="">
              </div>
              <div class="col-xs-2">
                <h1 style="text-align: center;font-size: 20px;margin-top: 70px;"><?= $kolekcja['Nazwa'] ?></h1>
              </div>
              <div class="col-xs-2">
                <div style="font-weight: bold;font-size: 20px;margin-top: 66px;">Cena:<br><span><?= $price ?></span> PLN</div>
              </div>
              <div class="col-xs-2">
                  <div style="font-size: 20px;margin-top: 25px;">
                      <img src="<?= getDiamondMiniPhotoUrl($diamond1['Ksztalt'])?>" alt=""><br>
                      <?= getProductFullName(intval($_GET['diamond1'])); ?>
                      <? if ($_GET["product-type"] == '4') {
                          echo '<br>';
                          echo getProductFullName(intval($_GET['diamond2']));
                      }
                      ?>
                  </div>
              </div>
              <div class="col-xs-2">
                  <div style="font-size: 20px;margin-top: 66px;">
                      Czas realizacji:<br><?=$czasRealizacji?>
                  </div>
              </div>
            </div>
        </div>
        <div class="col-md-9 step-1">
            <div id="step-container">
                <ul style="padding-left: 0px;list-style: none;">
                    <li class="col-md-4 tab active">1. nabywca/odbiorca</li>
                </ul>
                <div id="step-1" class="panel-container">
                    <div class="col-md-12 name">
                        <h1 style="color:red; margin-bottom:20px;"><?= printMsg(); ?></h1>
                    </div>
                    <form id="create-transaction" method="post">
                        <input type="hidden" name="collection-id" value="<?= $_GET['collection-id'] ?>" />
                        <input type="hidden" name="metal-id" value="<?= $_GET['metal-id'] ?>" />
                        <input type="hidden" name="product-type" value="<?= $_GET['product-type'] ?>" />
                        <input type="hidden" name="diamond1" value="<?= $_GET['diamond1'] ?>" />
                        <input type="hidden" name="diamond2" value="<?= $_GET['diamond2'] ?>" />
                        <input type="hidden" name="product-full-name" value="<?= $orderProductFullName ?>" />
                        <input type="hidden" name="czas-realizacji" value="<?= $czasRealizacji ?>" />
                        <div class="col-md-6 left">
                            <div class="form-group">
                                <label>Zaliczka (PLN) <span class="required">*</span></label>
                                <input style="color: #888d9d;width: 100%;border: 1px solid #d9d9d9;padding: 9px;" type="number" name="Zaliczka" value="<?= $_POST['Zaliczka'] ?>" />
                            </div>
                          <? 
                          if ($_GET["product-type"] == '1' || $_GET["product-type"] == '2') {
                          ?>
                            <div class="form-group">
                                <label>Rozmiar pierścionka <span class="required">*</span></label>
                                <select name="rozmiar">
                                    <? for ($i=8; $i <= 20; $i++) { 
                                        echo "<option value='".$i."'>".$i."</option>";
                                    } ?>
                                </select>
                            </div>
                          <?
                          }
                          ?>
                        </div>
                        <div class="col-md-12 name">
                            <h2>Dane kupującego</h2>
                        </div>
                        <div class="col-md-6 left">
                            <div class="form-group">
                                <label>Imię <span class="required">*</span></label>
                                <input type="text" name="name" value="<?= $_POST['name'] ?>" />
                            </div>
                            <div class="form-group">
                                <label>Nazwisko <span class="required">*</span></label>
                                <input type="text" name="surname" value="<?= $_POST['surname'] ?>" />
                            </div>
                            <div class="form-group">
                                <label>Kraj</label>
                                <select name="kraj">
                                    <option>Polska</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ulica</label>
                                <input type="text" name="street" value="<?= $_POST['street'] ?>" />
                            </div>
                            <div class=" row form-group">
                                <div class="col-md-6">
                                    <label>Kod pocztowy</label>
                                    <input type="text" name="kod" value="<?= $_POST['kod'] ?>" />
                                </div>
                                <div class="col-md-6">
                                    <label>Miasto</label>
                                    <input type="text" name="miasto" value="<?= $_POST['miasto'] ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <p><span class="required">*</span> pola wymagane</p>
                            </div>
                        </div>
                        <div class="col-md-6 right">
                            <div class="form-group">
                                <label>E-mail<span class="required">*</span></label>
                                <input type="text" name="email" value="<?= $_POST['email'] ?>" />
                            </div>
                            <div class="form-group">
                                <label>Telefon<span class="required">*</span></label>
                                <input type="text" name="telefon" value="<?= $_POST['telefon'] ?>" />
                            </div>
                            <div class="form-group">
                                <label>Komentarz do zamówienia</label>
                                <textarea class="yes-textarea" name="komentarz" id="" rows="10"></textarea>
                            </div>
                        </div>
                        
                        <input type="hidden" name="purchase" value="Dalej" />
                    </form>
                </div>
                <div class="row actions">
                    <div class="col-md-6 col-sm-6 col-xs-12 continue"></div>
                    <div class="col-md-6 col-sm-6 col-xs-12 nextstep"><a href="#" onclick="event.preventDefault();document.getElementById('create-transaction').submit();" title="Przejdź dalej"><?= slownik(132) ?></a></div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mini-cart">

        </div>
    </div>
    <!-- end / cart - step 1 -->
</section>
<!-- end / main  -->
<?php include 'includes/footer.php';?>