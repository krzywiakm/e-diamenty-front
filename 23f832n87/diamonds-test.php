<? $page_name = basename(__FILE__, '.php'); ?>
<? include 'api/func.php'; ?>
<? include 'api/auth.php'; ?>
<? include 'api/yes.php'; ?>
<? include 'includes/header.php'; ?>
<?
if (isset($_GET["collection-id"])) {
    $collectionId = intval($_GET["collection-id"]);
    $kolekcja = getYesKolekcjeById($_GET["collection-id"]);
}
if (isset($_GET["collection-id"]) && isset($_GET["metal-id"]) && isset($_GET["product-type"])) {
    $productImgUrl = productImgUrl($_GET["collection-id"], $_GET["product-type"], $_GET["metal-id"]);
    $productImgUrlAllSizes = productImgUrlAllSizes($_GET["collection-id"], $_GET["product-type"], $_GET["metal-id"]);
}
?>
  <div id="choose-diamond" class="container">
    <div class="row">
      <div class="col-xs-3">
        <?
            foreach ($productImgUrlAllSizes as $key => $value) {
                ?>
                    <img class="img-size img-size-<?= $key ?> hide" src="assets/images/produkty/<?= $value ?>" alt="">
                <?
            }
        ?>
      </div>
      <script>
        $(document).ready(function () {
            $('.img-size').first().removeClass('hide');
        });
      </script>
      <div class="col-xs-4">
        <h1 style="text-align: center;font-size: 40px;margin-top: 110px;"><?= $kolekcja['Nazwa'] ?></h1>
      </div>
      <div class="col-xs-5">
        <div class="big-price">Cena: <span id="price-value">0</span> PLN</div>
      </div>
    </div>
    <div id="main" class="row catalog">
      <div class="col-xs-12">
        <div id="diamonds-table">
            <!-- start / filtr -->
            <div class="wrapper row">
                <div class="col-md-12">
                    <form id="filtr">
                        <div id="basic-filtr">
                            <div style="margin-bottom: 35px;" class="row filtr1">
                                <div class="col-md-4 col-sm-12 col-xs-12 single">
                                    <label>
                                        <?= slownik(64) ?> <span>(PLN)</span>
                                        <i class="info tooltip" title="<?= slownik(69) ?>"></i>
                                    </label>
                                    <input id="price" />
                                    <input type="text" id="price_from" name="price_from" value="1" class="input-filtr" />
                                    <input type="text" id="price_to" name="price_to" value="<?= getHighestPrice(); ?>" class="input-filtr" />
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 single">
                                    <label>
                                        <?= slownik(56) ?> <span>(Carat)</span>
                                        <i class="info tooltip" title="<?= slownik(70) ?>"></i>
                                    </label>
                                    <input id="mass" />
                                    <input type="text" id="mass_from" name="mass_from" value="0.3" class="input-filtr" />
                                    <input type="text" id="mass_to" name="mass_to" value="<?= getHighestMass(); ?>" class="input-filtr" />
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 filter-availability single mobile-advanced-filters">
                                    <label>
                                        <?= slownik(60) ?> <span>(Liczba dni)</span>
                                        <i class="info tooltip" title="<?= slownik(73) ?>"></i>
                                    </label>
                                    <input id="availability" />
                                    <input type="text" id="availability_from" name="availability_from" value="0" class="input-filtr" readonly />
                                    <input type="text" id="availability_to" name="availability_to" value="30" class="input-filtr" readonly />
                                </div>
                            </div>
                        </div>
                        <div id="advanced-filtr" class="hide-advanced-filters">
                            <div class="row filtr2">
                                <div class="col-md-4 col-sm-12 col-xs-12 filter-color single mobile-advanced-filters">
                                    <label>
                                        <?= slownik(57) ?> <span>(Color)</span>
                                        <i class="info tooltip" title="<?= slownik(71) ?>"></i>
                                    </label>
                                    <input id="color" />
                                    <input type="hidden" id="color_from" name="color_from" value="D" />
                                    <input type="hidden" id="color_to" name="color_to" value="M" />
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 filter-clarity single mobile-advanced-filters">
                                    <label>
                                        <?= slownik(58) ?> <span>(Clarity)</span>
                                        <i class="info tooltip" title="<?= slownik(72) ?>"></i>
                                    </label>
                                    <input id="clarity" />
                                    <input type="hidden" id="clarity_from" name="clarity_from" value="IF" />
                                    <input type="hidden" id="clarity_to" name="clarity_to" value="I3" />
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 single">
                                    <label style="margin-bottom: 24px;">
                                        <?= slownik(66) ?>
                                        <i class="info tooltip" title="<?= slownik(77) ?>"></i>
                                    </label>
                                    <ul class="list">
                                        <li>
                                            <input type="checkbox" name="certificate_gia" id="gia" value="true" /><label for="gia"><span></span>GIA</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" name="certificate_igi" id="igi" value="true" /><label for="igi"><span></span>IGI</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" name="certificate_hrd" id="hrd" value="true" /><label for="hrd"><span></span>HRD</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row filtr3">
                                <div class="col-md-4 col-sm-12 col-xs-12 filter-cut single">
                                    <label>
                                        <?= slownik(62) ?>
                                        <i class="info tooltip" title="<?= slownik(74) ?>"></i>
                                    </label>
                                    <input id="cut" />
                                    <input type="hidden" id="cut_from" name="cut_from" value="1" />
                                    <input type="hidden" id="cut_to" name="cut_to" value="4" />
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 filter-polishing single">
                                    <label>
                                        <?= slownik(65) ?>
                                        <i class="info tooltip" title="<?= slownik(75) ?>"></i>
                                    </label>
                                    <input id="polishing" />
                                    <input type="hidden" id="polishing_from" name="polishing_from" value="1" />
                                    <input type="hidden" id="polishing_to" name="polishing_to" value="4" />
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 filter-symmetry single">
                                    <label>
                                        <?= slownik(61) ?>
                                        <i class="info tooltip" title="<?= slownik(76) ?>"></i>
                                    </label>
                                    <input id="symmetry" />
                                    <input type="hidden" id="symmetry_from" name="symmetry_from" value="1" />
                                    <input type="hidden" id="symmetry_to" name="symmetry_to" value="4" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12 single">
                                <a href="#" title="Reset Filters" class="reset-filters"><?= slownik(54) ?></a>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-4 single">
                                <a href="#" title="Wyszukiwania zaawansowane" class="advanced-search down">Wyszukiwania zaawansowane</a>
                            </div>
                        </div>
                    </form>
                    <span class="catalog-scroll-down"></span>
                </div>
            </div>
            <!-- end / filtr -->
            
            <!-- end / table catalog -->
            <div class="wrapper row table">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <!-- start / cube animate for search result -->
                        <div class="spinner">
                             <div class="cube1"></div>
                             <div class="cube2"></div>
                        </div>
                        <div class="TableHeading">
                            <div class="TableHead head-compare" style="padding-top:15px;"><img src="assets/images/icons/add-to-cart-active.png" alt="Wybierz"></div>
                            <div data-orderBy="shape" class="TableHead head-shape"><?= slownik(55) ?><br>
                                <span data-direction="desc" class="arrow up"></span>
                                <span data-direction="asc" class="arrow down"></span>
                            </div>
                            <div data-orderBy="mass" class="TableHead head-mass"><?= slownik(56) ?><br>
                                <span data-direction="desc" class="arrow up"></span>
                                <span data-direction="asc" class="arrow down"></span>
                            </div>
                            <div data-orderBy="color" class="TableHead head-color"><?= slownik(57) ?><br>
                                <span data-direction="desc" class="arrow up"></span>
                                <span data-direction="asc" class="arrow down"></span>
                            </div>
                            <div data-orderBy="clarity" class="TableHead head-clarity"><?= slownik(58) ?><br>
                                <span data-direction="desc" class="arrow up"></span>
                                <span data-direction="asc" class="arrow down"></span>
                            </div>
                            <div data-orderBy="certificat" class="TableHead head-certificat"><?= slownik(59) ?><br>
                                <span data-direction="desc" class="arrow up"></span>
                                <span data-direction="asc" class="arrow down"></span>
                            </div>
                            <div data-orderBy="availability" class="TableHead head-availability"><?= slownik(60) ?><br>
                                <span data-direction="desc" class="arrow up"></span>
                                <span data-direction="asc" class="arrow down"></span>
                            </div>
                            <div data-orderBy="symmetry" class="TableHead head-symmetry"><?= slownik(61) ?><br>
                                <span data-direction="desc" class="arrow up"></span>
                                <span data-direction="asc" class="arrow down"></span>
                            </div>
                            <div data-orderBy="cut" class="TableHead head-cut"><?= slownik(62) ?><br>
                                <span data-direction="desc" class="arrow up"></span>
                                <span data-direction="asc" class="arrow down"></span>
                            </div>
                            <div data-orderBy="polishing" class="TableHead head-polishing"><?= slownik(63) ?><br>
                                <span data-direction="desc" class="arrow up"></span>
                                <span data-direction="asc" class="arrow down"></span>
                            </div>
                            <div data-orderBy="price" class="TableHead head-price"><?= slownik(64) ?><br>
                                <span data-direction="desc" class="arrow up"></span>
                                <span data-direction="asc" class="arrow down"></span>
                            </div>
                        </div>
                        <!-- end / cube animate for search result -->
                        <div id="content-1">
                            <div class="Table diamonds-table">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end / table catalog -->
        </div>
      </div>
    </div>
  </div>
<? include 'includes/footer.php'; ?>