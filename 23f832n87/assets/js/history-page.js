var baseURL = 'https://diamenty.yes.pl';
//baseURL = 'http://localhost/yes-e-diamenty/';

function orderHistoryBy(column, currentDirection) {
    var direction = 'asc';
    if (currentDirection == 'asc') {
      direction = 'desc';
    }
    else {
      direction = 'asc';
    }
    location.href = baseURL + '/history?sort-by='+column+'&sort-direction='+direction;
}