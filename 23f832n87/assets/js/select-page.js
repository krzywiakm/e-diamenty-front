var baseURL = 'https://diamenty.yes.pl';
//baseURL = 'http://localhost/yes-e-diamenty/';
var appState = {
    collectionId: 0,
    metalId: 0,
    products: {
        downloaded: false,
        data: []
    }
}

function chooseCollection(id) {
    appState.collectionId = id;
    showAvailableMetals();
}

function showAvailableMetals() {
    $('.metal').hide();
    for (var i = 0; i < appState.products.data.length; i++) {
        appState.products.data[i]
        if (appState.products.data[i]['IdKolekcja'] == appState.collectionId.toString()) {
            $('.metal' + appState.products.data[i]['IdMetal']).show();
        }
    }
}

function showAvailableProductTypes() {
    $('.productImg').hide();
    for (var i = 0; i < appState.products.data.length; i++) {
        appState.products.data[i]
        if (appState.products.data[i]['IdKolekcja'] == appState.collectionId.toString() && 
            appState.products.data[i]['IdMetal'] == appState.metalId.toString()) {
            console.log(appState.products.data[i]);
            $('.productImg' + appState.products.data[i]['Id']).show();
        }
    }
}

function chooseMetal(id) {
    appState.metalId = id;
    showAvailableProductTypes();
}

function openDiamondsList(productType) {
    location.href = baseURL + '/diamonds' + '?collection-id=' + appState.collectionId + '&metal-id=' + appState.metalId + '&product-type=' + productType;
}

$(".section-img").click(function(e){
    console.log("test");
    $(".section-heading").fadeOut();
    $(".section-img").fadeOut();
    $(this).fadeIn();
    $(".page-content").fadeIn();
    $("#history-btn").hide();
    $("#back-btn").show();
});

$("#back-btn").click(function(e){
    $(".section-heading").fadeIn();
    $(".section-img").fadeIn();
    $(".page-content").fadeOut();
    $("#history-btn").show();
    $("#choose-diamond").fadeOut();
    $("#back-btn").hide();
});

$(".open-product-images").click(function(e){
    $(".product-images").fadeOut();
    $(".product-images").fadeIn();
});

$(".open-choose-diamond").click(function(e){
    $(".section-heading").fadeOut();
    $(".section-img").fadeOut();
    $(".page-content").fadeOut();
    $("#choose-diamond").fadeIn();
    $("#back-btn").show();
});

$(document).ready(function () {
    $.get( baseURL + '/api/products.php', function( data ) {
        appState.products.data = JSON.parse(data);
        appState.products.downloaded = true;
    });
});
