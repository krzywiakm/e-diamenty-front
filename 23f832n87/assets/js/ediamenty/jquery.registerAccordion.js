$(document).ready(function () {
    
    $('.bxslider').bxSlider({
        controls: false,
        auto: false
    });

    $('#additional-form').hide();

    $(document).on('change', '#add', function() {
        if($(this).is(':checked')){
            $('#additional-form').show(1000);
        }
        else{
            $('#additional-form').hide(1000);
        }
    });

});

