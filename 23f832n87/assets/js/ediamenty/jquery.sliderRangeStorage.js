$(document).ready(function(){
    var BASE_URL = 'https://diamenty.yes.pl';
    //BASE_URL = 'http://localhost/yes-e-diamenty/';
    var input_check_delay = 1500;

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        input_check_delay = 3000;
    }

    $.get( BASE_URL + '/api/attributes-service.php', function( data ) {
        var data = JSON.parse(data);

        var massParamInit = new function() {
            this.name = 'mass';
            this.divider = 100;
            //database value, number format
            this.min = 0.3;
            this.max = data.max_mass;
        };

        var priceParamInit = new function() {
            this.name = 'price';
            this.divider = 1;
            //database value, number format
            this.min = 1;
            this.max = data.max_price;
        };



        function Grid(paramInit){

            this.name = paramInit.name;
            this.divider = paramInit.divider;
            this.min = paramInit.min;
            this.max = paramInit.max;
            this.maxO = paramInit.max * paramInit.divider;
            this.minO = paramInit.min * paramInit.divider;
            this.values = []
        }

        Grid.prototype.recursiveGenerateGrid = function (digits) {

            var self = this;
            var compare = digits;
            var roundtoOne = Number(1 + '0'.repeat(compare - 1));
            var min = Math.ceil(this.minO/roundtoOne)*roundtoOne;
            var step;

            switch(this.name){
                case "mass": step = calculateStepforMass(digits, compare); break;
                case "price": step = calculateStepforPrice(digits, compare); break;
            }

            min = min < step ? step : min;
            for (i = min; i <= this.maxO; i += step) {
                compare = i.toString().length;
                if (digits != compare) {
                    min = i;
                    return this.recursiveGenerateGrid(compare) ;
                }
                this.values.push(i/this.divider)
            }
        };

        Grid.prototype.getValues = function(){
            this.values.indexOf(this.min) < 0 ? this.values.unshift(this.min)  : "";
            this.values.indexOf(this.max) < 0 ? this.values.push(this.max) : "";
            return this.values
        };

        function calculateStepforMass(digits, compare){
            return digits % 2 == 0 ? (digits < 3 ? Number(5 + '0'.repeat(compare - 1)/2) :  Number(1 + '0'.repeat(compare - 2)) ) : (digits < 4 ? Number(1 + '0'.repeat(compare - 1)) : Number(1 + '0'.repeat(compare - 2))) ;
        }

        function calculateStepforPrice(digits, compare){
            return digits % 2 == 0 ? (digits < 3 ? Number(5 + '0'.repeat(compare - 1)) :  (digits < 8 ) ? Number(5 + '0'.repeat(compare - 2)) : Number(5 + '0'.repeat(compare - 3)) ) : (digits < 4 ? Number(1 + '0'.repeat(compare - 1)) : Number(1 + '0'.repeat(compare - 2)) ) ;
        }

        var mass = new Grid(massParamInit);
        mass.recursiveGenerateGrid(mass.minO.toString().length);

        var price = new Grid(priceParamInit);
        price.recursiveGenerateGrid(price.minO.toString().length);

        var priceValues = price.getValues();
        var massValues = mass.getValues();

        var keyup_lock = false;

        // Zakres cen
        $rangePrice = $("#price");

        $rangePrice.ionRangeSlider({
            type: "double",
            min: priceParamInit.min,
            max: priceParamInit.max,
            from: priceValues.indexOf(1),
            to: priceValues.indexOf(data.max_price),
            values: priceValues,
            onChange: function (data) {
                localStorage.setItem('serviceLock', 'true');
            },
            onFinish: function (data) {
                localStorage.setItem('serviceLock', 'false');
                $('#filtr').trigger('change');
            }
        });

        localStorage.setItem('priceMin', 1);
        localStorage.setItem('priceMax', data.max_price);

        $rangePrice.on("change", function () {
            var $this = $(this),
                from = $this.data("from"),
                to = $this.data("to");

            $('#price_from').val(priceValues[from]);
            $('#price_to').val(priceValues[to]);
        });


        $('input[name="price_from"]').keyup(function() {
            if (keyup_lock == false) {
                setTimeout(function(){
                    if ($('#price_from').val() == "") {

                    }
                    else if (!isNaN(parseInt($('#price_from').val()))) {
                        $('#price_from').val(parseInt($('#price_from').val()));
                    }
                    else {
                        $('#price_from').val(parseInt(1));
                    }
                    if (parseInt($('#price_from').val()) > data.max_price) {
                        $('#price_from').val(parseInt(data.max_price))
                    }
                    else if (parseInt($('#price_from').val()) < 1) {
                        $('#price_from').val(1);
                    }
                    localStorage.setItem('serviceLock', 'false');
                    var sliderPrice = $rangePrice.data("ionRangeSlider");
                    sliderPrice.update({
                        from: priceValues.indexOf(Number(findClosestInArray($('#price_from').val(), priceValues))),
                        to: priceValues.indexOf(Number(findClosestInArray($('#price_to').val(), priceValues)))
                    });
                    localStorage.setItem('serviceLock', 'false');
                    keyup_lock = false;
                }, input_check_delay);
                keyup_lock = true;
            }
        });

        $('input[name="price_to"]').keyup(function() {
            if (keyup_lock == false) {
                setTimeout(function(){
                    if ($('#price_to').val() == "") {

                    }
                    else if (!isNaN(parseInt($('#price_to').val()))) {
                        $('#price_to').val(parseInt($('#price_to').val()));
                    }
                    else {
                        $('#price_to').val(parseInt(1));
                    }
                    if (parseInt($('#price_to').val()) > data.max_price) {
                        $('#price_to').val(parseInt(data.max_price))
                    }
                    else if (parseInt($('#price_to').val()) < 1) {
                        $('#price_to').val(1);
                    }
                    localStorage.setItem('serviceLock', 'false');
                    var sliderPrice = $rangePrice.data("ionRangeSlider");
                    sliderPrice.update({
                        from: priceValues.indexOf(Number(findClosestInArray($('#price_from').val(), priceValues))),
                        to: priceValues.indexOf(Number(findClosestInArray($('#price_to').val(), priceValues)))
                    });
                    localStorage.setItem('serviceLock', 'false');
                    keyup_lock = false;
                }, input_check_delay);
                keyup_lock = true;
            }
        });
        
        // Masa
        $rangeMass = $("#mass");

        $rangeMass.ionRangeSlider({
            type: "double",
            min: massParamInit.min,
            max: massParamInit.max,
            from: massValues.indexOf(0.3),
            to: massValues.indexOf(data.max_mass),
            values: massValues,
            onChange: function (data) {
                localStorage.setItem('serviceLock', 'true');
                $('#mass_from').val(parseFloat($('#mass_from').val()).toFixed(2));
                $('#mass_to').val(parseFloat($('#mass_to').val()).toFixed(2));
            },
            onFinish: function (data) {
                localStorage.setItem('serviceLock', 'false');
                $('#filtr').trigger('change');
            }
        });

        localStorage.setItem('massMin', 0.3);
        localStorage.setItem('massMax', data.max_mass);

        $rangeMass.on("change", function () {
            var $this = $(this),
                from = $this.data("from"),
                to = $this.data("to");
            $('#mass_from').val(massValues[from]);
            $('#mass_to').val(massValues[to]);
        });


        $('input[name="mass_from"]').keyup(function() {
            var replace = $('#mass_from').val();
            replace = replace.replace(",", ".");
            $('#mass_from').val(replace);
            if (keyup_lock == false) {
                setTimeout(function(){
                    if ($('#mass_from').val() == "") {

                    }
                    else if (!isNaN(parseFloat($('#mass_from').val()).toFixed(2))) {
                        $('#mass_from').val(parseFloat($('#mass_from').val()).toFixed(2));
                    }
                    else {
                        $('#mass_from').val(parseFloat(0.01).toFixed(2));
                    }
                    if (parseFloat($('#mass_from').val()) > data.max_mass) {
                        $('#mass_from').val(parseFloat(data.max_mass).toFixed(2))
                    }
                    else if (parseFloat($('#mass_from').val()).toFixed(2) < 0.01) {
                        $('#mass_from').val(0.01);
                    }
                    localStorage.setItem('serviceLock', 'false');
                    var sliderMass = $rangeMass.data("ionRangeSlider");
                    sliderMass.update({
                        from: massValues.indexOf(Number(findClosestInArray(parseFloat($('#mass_from').val()).toFixed(2), massValues))),
                        to: massValues.indexOf(Number(findClosestInArray(parseFloat($('#mass_to').val()).toFixed(2), massValues)))
                    });
                    localStorage.setItem('serviceLock', 'false');
                    if (!isNaN(parseFloat($('#mass_from').val()).toFixed(2))) {
                        $('#mass_from').val(parseFloat($('#mass_from').val()).toFixed(2));
                    }
                    if (!isNaN(parseFloat($('#mass_to').val()).toFixed(2))) {
                        $('#mass_to').val(parseFloat($('#mass_to').val()).toFixed(2));
                    }
                    keyup_lock = false;
                }, input_check_delay);
                keyup_lock = true;
            }
        });

        $('input[name="mass_to"]').keyup(function() {
            var replace = $('#mass_to').val();
            replace = replace.replace(",", ".");
            $('#mass_to').val(replace);
            if (keyup_lock == false) {
                setTimeout(function(){
                    if ($('#mass_to').val() == "") {

                    }
                    else if (!isNaN(parseFloat($('#mass_to').val()).toFixed(2))) {
                        $('#mass_to').val(parseFloat($('#mass_to').val()).toFixed(2));
                    }
                    else {
                        $('#mass_to').val(parseFloat(0.01).toFixed(2));
                    }
                    if (parseFloat($('#mass_to').val()) > data.max_mass) {
                        $('#mass_to').val(parseFloat(data.max_mass).toFixed(2))
                    }
                    else if (parseFloat($('#mass_to').val()).toFixed(2) < 0.01) {
                        $('#mass_to').val(0.01);
                    }
                    localStorage.setItem('serviceLock', 'false');
                    var sliderMass = $rangeMass.data("ionRangeSlider");
                    sliderMass.update({
                        from: massValues.indexOf(Number(findClosestInArray(parseFloat($('#mass_from').val()).toFixed(2), massValues))),
                        to: massValues.indexOf(Number(findClosestInArray(parseFloat($('#mass_to').val()).toFixed(2), massValues)))
                    });
                    localStorage.setItem('serviceLock', 'false');
                    if (!isNaN(parseFloat($('#mass_from').val()).toFixed(2))) {
                        $('#mass_from').val(parseFloat($('#mass_from').val()).toFixed(2));
                    }
                    if (!isNaN(parseFloat($('#mass_to').val()).toFixed(2))) {
                        $('#mass_to').val(parseFloat($('#mass_to').val()).toFixed(2));
                    }
                    keyup_lock = false;
                }, input_check_delay);
                keyup_lock = true;
            }
        });

        if(localStorage.getItem('priceFrom')){

            if(localStorage.getItem('priceFrom') && localStorage.getItem('priceTo')){
                var sliderPrice = $rangePrice.data("ionRangeSlider");

                var priceFrom = localStorage.getItem('priceFrom');
                var priceTo = localStorage.getItem('priceTo');
                $('#price_from').val(priceFrom);
                $('#price_to').val(priceTo);
                sliderPrice.update({
                    from: priceValues.indexOf(Number(priceFrom)),
                    to: priceValues.indexOf(Number(priceTo))
                });
            }

            if(localStorage.getItem('massFrom') && localStorage.getItem('massTo')){
                var sliderMass = $rangeMass.data("ionRangeSlider");

                var massFrom = localStorage.getItem('massFrom');
                var massTo = localStorage.getItem('massTo');
                $('#mass_from').val(massFrom);
                $('#mass_to').val(massTo);
                sliderMass.update({
                    from: massValues.indexOf(Number(massFrom)),
                    to: massValues.indexOf(Number(massTo))
                });
                if (!isNaN(parseFloat($('#mass_from').val()).toFixed(2))) {
                    $('#mass_from').val(parseFloat($('#mass_from').val()).toFixed(2));
                }
                if (!isNaN(parseFloat($('#mass_to').val()).toFixed(2))) {
                    $('#mass_to').val(parseFloat($('#mass_to').val()).toFixed(2));
                }

            }

            localStorage.setItem('massFrom', $('#mass_from').val());
            localStorage.setItem('massTo', $('#mass_to').val());
        }

        window.onbeforeunload = function() {
            localStorage.setItem('massFrom', $('#mass_from').val());
            localStorage.setItem('massTo', $('#mass_to').val());
            localStorage.setItem('priceFrom', $('#price_from').val());
            localStorage.setItem('priceTo', $('#price_to').val());
        };


        localStorage.removeItem('priceTo');
        localStorage.removeItem('priceFrom');
        localStorage.removeItem('massTo');
        localStorage.removeItem('massFrom');

    });

    function findClosestInArray(goal, counts) {
        goal = Number(goal);
        var closest = counts.reduce(function (prev, curr) {
          return (Math.abs(curr - goal) < Math.abs(prev - goal) ? curr : prev);
        });

        return closest;
    }
});