$(".section-img").click(function(e){
    $(".section-heading").hide();
    $(".section-img").hide();
    $(this).show();
    $(".page-content").show();
    $("#history-btn").hide();
    $("#back-btn").show();
});

$("#back-btn").click(function(e){
    $(".section-heading").show();
    $(".section-img").show();
    $(".page-content").hide();
    $("#history-btn").show();
    $("#back-btn").hide();
});
