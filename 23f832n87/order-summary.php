<? $page_name = basename(__FILE__, '.php'); ?>
<? include 'api/func.php'; ?>
<? include 'api/auth.php'; ?>
<? include 'api/yes.php'; ?>
<? include 'includes/header.php'; ?>
<? 
$order_id = $_GET["id"];

if ($order_id) {
    $order = getYesShopOrderById($order_id);
    $price = $order['DoZaplaty'];
    $diamond1 = $order['Products'][0];
    $diamond2 = $order['Products'][1];
    if ($diamond1) {
      $diamond1Id = $diamond1['Id'];
    }
    else {
      $diamond1Id = 0;
    }
    if ($diamond2) {
      $diamond2Id = $diamond2['Id'];
    }
    else {
      $diamond2Id = 0;
    }
    $yesProduct = getYesProduktById($order['IdYesProdukt']);
    $orderProductFullName = getYesProductFullName($yesProduct['IdKolekcja'],$yesProduct['IdMetal'],$yesProduct['IdTypu'],$diamond1Id,$diamond2Id);
    $productImgUrl = productImgUrl($yesProduct['IdKolekcja'], $yesProduct['IdMetal'], $yesProduct['IdTypu'], floatval($diamond1['Masa']));
    $kolekcja = getYesKolekcjeById($yesProduct['IdKolekcja']);
    $czasRealizacji = getYesProductDeliveryTime($yesProduct['IdKolekcja'], $yesProduct['IdMetal'], $yesProduct['IdTypu'], dostepnosc($diamond1['RodzajZestawienia'])) . ' dni';
    $czasRealizacji = dostepnosc($diamond1['RodzajZestawienia']) . ' dni';
}
else {
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
    echo '<h1 style="text-align:center;">404 not found</h1>';
    die();
}
if ($order['Id'] == null) {
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
    echo '<h1 style="text-align:center;">404 not found</h1>';
    die();
}
?>

<!-- start / main  -->
<section id="main" class="cart steps">
    <!-- start / cart - step 1 -->
    <div class="wrapper row">
        <div class="col-xs-12">
            <h1 style="margin-bottom: 18px;">Zamówienie <?= $order['Id'] ?></h1>
            <div class="row">
              <div class="col-xs-2">
                <img src="<?= $productImgUrl ?>" alt="">
              </div>
              <div class="col-xs-2">
                <h1 style="text-align: center;font-size: 20px;margin-top: 70px;"><?= $kolekcja['Nazwa'] ?></h1>
              </div>
              <div class="col-xs-2">
                <div style="font-weight: bold;font-size: 20px;margin-top: 66px;">Cena:<br><span><?= $price ?></span> PLN</div>
              </div>
              <div class="col-xs-2">
                  <div style="font-size: 20px;margin-top: 25px;">
                      <img src="<?= getDiamondMiniPhotoUrl($diamond1['Ksztalt'])?>" alt=""><br>
                      <?= getProductFullName(intval($diamond1Id)); ?>
                      <? if ($yesProduct['IdTypu'] == '4') {
                          echo '<br>';
                          echo getProductFullName(intval($diamond2Id));
                      }
                      ?>
                  </div>
              </div>
              <div class="col-xs-2">
                  <div style="font-size: 20px;margin-top: 66px;">
                      Czas realizacji:<br><?=$czasRealizacji?>
                  </div>
              </div>
            </div>
        </div>
        <div class="col-md-9 step-1">
            <div id="step-container">
                <ul style="padding-left: 0px;list-style: none;">
                    <li class="col-md-4 tab active">1. nabywca/odbiorca</li>
                </ul>
                <div id="step-1" class="panel-container">
                    <div class="col-md-12 name">
                        <h1 style="color:red; margin-bottom:20px;"><?= printMsg(); ?></h1>
                    </div>
                    <div class="col-md-6 left">
                        <div class="form-group">
                            <label>Zaliczka (PLN)</label>
                            <div><?= $order['Zaliczka'] ?></div>
                        </div>
                          <? 
                          if ($yesProduct['IdTypu'] == '1' || $yesProduct['IdTypu'] == '2') {
                          ?>
                            <div class="form-group">
                                <label>Rozmiar pierścionka</label>
                                <div><?= $order['YesRozmiar'] ?></div>
                            </div>
                          <?
                          }
                          ?>
                    </div>
                    <div class="col-md-12 name">
                        <h2>Dane kupującego</h2>
                    </div>
                    <form id="cart-register" method="post">
                        <div class="col-md-6 left">
                            <div class="form-group">
                                <label>Imię</label>
                                <div><?= $order['user']['Imie'] ?></div>
                            </div>
                            <div class="form-group">
                                <label>Nazwisko</label>
                                <div><?= $order['user']['Nazwisko'] ?></div>
                            </div>
                            <div class="form-group">
                                <label>Kraj</label>
                                <div>Polska</div>
                            </div>
                            <div class="form-group">
                                <label>Ulica</label>
                                <div><?= $order['user']['Ulica'] ?></div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label>Kod pocztowy</label>
                                    <div><?= $order['user']['KodPocztowy'] ?></div>
                                </div>
                                <div class="col-md-6">
                                    <label>Miasto</label>
                                    <div><?= $order['user']['Miejscowosc'] ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 right">
                            <div class="form-group">
                                <label>E-mail</label>
                                <div><?= $order['user']['Email'] ?></div>
                            </div>
                            <div class="form-group">
                                <label>Telefon</label>
                                <div><?= $order['user']['Telefon'] ?></div>
                            </div>
                            <div class="form-group">
                                <label>Komentarz do zamówienia</label>
                                <div><?= $order['Komentarz']; ?></div>
                            </div>
                        </div>
                        
                        <input type="hidden" name="rejestruj" value="Dalej" />
                    </form>
                </div>
                <div class="row actions">
                    <div class="col-md-6 col-sm-6 col-xs-12 continue"></div>
                    <div class="col-md-6 col-sm-6 col-xs-12 nextstep"><a href="#" onclick="event.preventDefault();generatePDF();" title="Przejdź dalej"><?= slownik(130) ?></a></div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mini-cart">

        </div>
    </div>
    <!-- end / cart - step 1 -->
</section>
<!-- end / main  -->
<script>
    function generatePDF() {
        let createdAt = new Date().toLocaleDateString();
        var docDefinition = 
        { 
        content: [
          { text: 'Potwierdzenie przyjęcia zamówienia', alignment: 'center', style: 'mainHeader' },
          ' ',
          ' ',
          {
            columns: [
              {
                width: '*',
                text: 'Numer zamówienia:'
              },
              {
                width: '*',
                text: '<?= $order['Id'] ?>'
              }
            ]
          },
          {
            columns: [
              {
                width: '*',
                text: 'Data:'
              },
              {
                width: '*',
                text: createdAt
              }
            ]
          },
          {
            columns: [
              {
                width: '*',
                text: 'Klient:'
              },
              {
                width: '*',
                text: '<?= $order['user']['Imie'] .' '. $order['user']['Nazwisko'] ?>'
              }
            ]
          },
          {
            columns: [
              {
                width: '*',
                text: 'Adres:'
              },
              {
                width: '*',
                text: '<?= $order['user']['Ulica'] .' '. $order['user']['NrDomu'] .', '. $order['user']['KodPocztowy'] .' '. $order['user']['Miejscowosc'] ?>'
              }
            ]
          },
          {
            columns: [
              {
                width: '*',
                text: 'Kontakt:'
              },
              {
                width: '*',
                text: '<?= 'tel. ' . $order['user']['Telefon'] .', email: '. $order['user']['Email'] ?>'
              }
            ]
          },
          ' ',
          ' ',
          'Zamówiony produkt:',
          ' ',
          '<?=$orderProductFullName?>',
          ' ',
          <? 
          if ($yesProduct['IdTypu'] == '1' || $yesProduct['IdTypu'] == '2') {
          ?>
            'Rozmiar pierścionka: <?= $order['YesRozmiar'] ?>',
          <?
          }
          ?>
          ' ',
          ' ',
          { text: 'Potwierdzam przyjęcie zadatku w kwocie <?= $order['Zaliczka'] ?> PLN na poczet płatności za sprowadzony diament' },
          ' ',
          ' ',
          'YES biżuteria:',
          '<?= $_SESSION['store'] ?>',
          '<?= $_SESSION['store_address'] ?>',
        ],
        styles: {
         mainHeader: {
           fontSize: 20,
           bold: true
         },
         header: {
           fontSize: 14,
           bold: true
         }
        }
        };
        pdfMake.createPdf(docDefinition).download('potwierdzenie.pdf');
    }
</script>
<?php include 'includes/footer.php';?>