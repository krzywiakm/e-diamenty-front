<? $page_name = basename(__FILE__, '.php'); ?>
<? include 'api/func.php'; ?>
<? include 'api/auth.php'; ?>
<? include 'api/yes.php'; ?>
<? include 'includes/header.php'; ?>
<? 
$cert_id = $_GET["cert-id"];
$product_id = $_GET["id"];

if ($product_id) {
    $product = getProduct($product_id);
}
else if($cert_id) {
    $product = getProductByCert($cert_id); 
    $product_id = $product['Id'];
}
else {
    header('Location: ' . BASE_URL . '/views/sklep/katalog');
    die();
}
if ($product['Id'] == null) {
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
    header('Location: ' . BASE_URL . '/404');
}
if (isset($_GET["collection-id"]) && isset($_GET["metal-id"]) && isset($_GET["product-type"])) {
    $productImgUrl = productImgUrl($_GET["collection-id"], $_GET["metal-id"], $_GET["product-type"], floatval($product['Masa']));
    $price = calculateTheTotalPrice(intval($_GET['collection-id']), intval($_GET['metal-id']), intval($_GET['product-type']), intval($product_id));
    $productType = $_GET["product-type"];
}
?>

<!-- start / main  -->
<section id="main" class="product">
    <div class="wrapper row">
        <!-- start / product gallery  -->
        <div class="col-md-5 col-md-offset-1 images">
            <div class="thumbnail">
                <figure>
                    <img id="gallery-main-image" src="<?= $productImgUrl; ?>" alt="<?= ksztalt($product['Ksztalt']) . ' ' . formatMass($product['Masa']) ?> - e-diamenty.pl" />
                </figure>
            </div>
        </div>
        <!-- end / product gallery  -->
        
        <!-- start / product detail  -->
        <div class="col-md-5 detail">
            <div class="row">
                <div class="col-md-6 summary">
                    <h1><?= getProductFullName($product_id); ?></h1>
                    <div class="amount">
                        <p><?= $price; ?> <span>PLN</span></p>
                    </div>
                </div>
                <div class="col-md-6 form">
                <? if(isProductAvailable($product_id)) { ?>
                    <? if($productType != '4') { ?>
                        <div id="addtocart">
                            <button style="margin-top: 0px;" class="btn-yes product-to-cart" onclick="makeOrderById(<?= $product['Id'] ?>)">ZAMÓW</button>
                        </div>
                    <? } else { ?>
                    <h1 style="text-align: center;font-size: 20px;margin-top: 23px;"><span class="clickable" onclick="openDiamondsList(<?= $_GET["collection-id"] ?>,<?= $_GET["metal-id"] ?>,<?= $_GET["product-type"] ?>, <?= $product_id ?>)">Dobierz<br>drugi kamień</span></h1>
                    <? } ?>
                <? } else { ?>
                <h1 style="text-align: center;font-size: 20px;margin-top: 45px;">Produkt<br>niedostępny</h1>
                <? } ?>
                </div>
            </div>

            <div class="row meta">
                <div class="col-md-12">
                    <h2>Cechy produktu</h2>
                    <ul style="padding-left: 0px;">
                        <li>
                            <span class="value">Kształt</span>
                            <span class="key"><?= ksztalt($product['Ksztalt']); ?></span>
                        </li>
                        <li>
                            <span class="value">Masa</span>
                            <span class="key"><?= formatMass($product['Masa']); ?></span>
                        </li>
                        <li>
                            <span class="value">Barwa</span>
                            <span class="key"><?= barwa($product['Barwa']); ?></span>
                        </li>
                        <li>
                            <span class="value">Czystość</span>
                            <span class="key"><?= czystosc($product['Czystosc']); ?></span>
                        </li>
                        <li>
                            <span class="value">Certyfikat</span>
                            <span class="key"><?= formatCert(certyfikaty($product['Laboratorium'])); ?></span>
                        </li>
                        <li>
                            <span class="value">Dostępność</span>
                            <span class="key"><? if(dostepnosc($product['RodzajZestawienia']) == 0) { 
                                print 'Od ręki'; 
                            } else { 
                                print dostepnosc($product['RodzajZestawienia']) . ' dni'; 
                            } ?></span>
                        </li>
                        <li>
                            <span class="value">Cena</span>
                            <span class="key"><?= $price; ?> PLN</span>
                        </li>
                        <li>
                            <span class="value">Szlif</span>
                            <span class="key"><?= szlif($product['Szlif']); ?></span>
                        </li>
                        <li>
                            <span class="value">Symetria</span>
                            <span class="key"><?= symetria($product['Symetria']); ?></span>
                        </li>
                        <li>
                            <span class="value">Polerowanie</span>
                            <span class="key"><?= polerowanie($product['Polerowanie']); ?></span>
                        </li>
                        <li>
                            <span class="value">Fluorescencja</span>
                            <span class="key"><?= fluorescencja($product['Fluorescencja']); ?></span>
                        </li>
                        <li>
                            <span class="value">Numer certyfikatu</span>
                            <span class="key"><?= $product['Symbol']; ?></span>
                        </li>
                    </ul>
                    <? if(formatCert(certyfikaty($product['Laboratorium'])) == 'GIA') { ?>
                    <a target="_blank" href="http://www.gia.edu/cs/Satellite?pagename=GST%2FDispatcher&childpagename=GIA%2FPage%2FReportCheck&c=Page&cid=1355954554547&reportno=<?=$product['Symbol']?>" title="Zobacz certyfikat" class="rmore">Zobacz certyfikat</a>
                    <?}?>
                    <? if(formatCert(certyfikaty($product['Laboratorium'])) == 'HRD') { ?>
                    <a target="_blank" href="https://my.hrdantwerp.com/?id=34&record_number=<?=$product['Symbol']?>" title="Zobacz certyfikat" class="rmore">Zobacz certyfikat</a>
                    <?}?>
                    <? if(formatCert(certyfikaty($product['Laboratorium'])) == 'IGI') { ?>
                    <a target="_blank" href="http://www.igiworldwide.com/verify.php?r=<?=$product['Symbol']?>" title="Zobacz certyfikat" class="rmore">Zobacz certyfikat</a>
                    <?}?>
                </div>
            </div>
         </div>
         <!-- end / product detail  -->
    </div>
</section>
<!-- end / main  -->
<script>
    function openDiamondsList(collectionId, metalId, productTypeId, diamond1Id) {
        location.href = baseURL + '/diamonds' + '?collection-id=' + collectionId + '&metal-id=' + metalId + '&product-type=' + productTypeId + '&diamond1-id=' + diamond1Id;
    }
</script>

<? include 'includes/footer.php'; ?>
