<? $page_name = basename(__FILE__, '.php'); ?>
<? include 'api/func.php'; ?>
<? include 'api/auth.php'; ?>
<? include 'api/yes.php'; ?>
<? include 'includes/header.php';?>
<? $sortBy = isset($_GET['sort-by']) ? $_GET['sort-by'] : "Id"; ?>
<? $sortDirection = isset($_GET['sort-direction']) ? $_GET['sort-direction'] : "desc"; ?>

<? $orders = getCurrentYesShopOrders($sortBy, $sortDirection); ?>

<!-- start / main  -->
<section id="main" class="login order-history">
    <!-- start / order history table -->
    <div class="wrapper row table">
        <div class="col-md-12">
            <h2>Historia zamówień</h2>
            <div class="table-responsive">
                <div class="Table">
    				 <div class="TableHeading">
    				     <div onclick="orderHistoryBy('Id', '<?=$sortDirection?>')" class="TableHead head-number clickable">Numer</div>
                         <div onclick="orderHistoryBy('Nazwisko', '<?=$sortDirection?>')" class="TableHead head-number clickable">Nazwisko</div>
                         <div onclick="orderHistoryBy('DataZam', '<?=$sortDirection?>')" class="TableHead head-date clickable">Data</div>
                         <div onclick="orderHistoryBy('DataZam', '<?=$sortDirection?>')" class="TableHead head-time clickable">Czas</div>
                         <div onclick="orderHistoryBy('DoZaplaty', '<?=$sortDirection?>')" class="TableHead head-amount clickable">Kwota</div>
                         <div onclick="orderHistoryBy('Przyjete', '<?=$sortDirection?>')" class="TableHead head-check clickable">Przyjęto</div>
                         <div onclick="orderHistoryBy('Wykonane', '<?=$sortDirection?>')" class="TableHead head-shipment clickable">Towar wysłany</div>
                         <div class="TableHead head-detail"></div>
    				 </div>
                <? foreach ($orders as $order) { ?>
                     <div class="TableRow">
                        <div class="TableCell number"><?= $order['Id']; ?></div>
                        <div class="TableCell number"><?= $order['Nazwisko']; ?></div>
                        <div class="TableCell date"><?= $order['DataZam']; ?></div>
                        <div class="TableCell time"><?= $order['CzasZam']; ?></div>
                        <div class="TableCell amount"><?= $order['DoZaplaty']; ?> PLN</div>
                        <div class="TableCell check"><?= taknie($order['Przyjete']); ?></div>
                        <div class="TableCell shipment"><?= taknie($order['Wykonane']); ?></div>
                        <div class="TableCell detail"><a href="<?= BASE_URL ?>/order-summary?id=<?= $order['Id'] ?>">szczegóły</a></div>
                     </div>
                <? } ?>
                </div>
            </div>
            <span class="catalog-scroll-right">Przesuń w prawo</span>
        </div>
    </div>
    <!-- end / order history table -->
</section>
<!-- end / main  -->

<?php include 'includes/footer.php';?>
