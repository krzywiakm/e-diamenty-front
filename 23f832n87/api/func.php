<?
// ini_set('session.gc_maxlifetime', 604800); //1 week
// session_set_cookie_params(604800);
// session_start();

if (isset($_GET[LANG]))
    $_SESSION[LANG] = $_GET[LANG];
else
    $_GET[LANG] = $_SESSION[LANG];

#definicje stalych

#dev
// define("SHOP_TOKEN", "RuFre4ReQ2pR");
// define("RT", "/Applications/MAMP/htdocs/yes-e-diamenty/");
// define("BASE_DIR", "/Applications/MAMP/htdocs/yes-e-diamenty/");
// define("DATA", "/Applications/MAMP/htdocs/yes-e-diamenty/common/users.dat");
// define("BASE_URL", "http://localhost/yes-e-diamenty");
// define("FOTO_FOLDER", "http://dic.nazwa.pl/foto/");

#prod
define("SHOP_TOKEN", "RuFre4ReQ2pR");
define("RT", "/home/dic/ftp/");
define("BASE_DIR", "/home/dic/ftp/23f832n87/");
define("DATA", "/home/dic/ftp/common/users.dat");
define("BASE_URL", "//diamenty.yes.pl");
define("FOTO_FOLDER", "//dic.nazwa.pl/foto/");


function dblogin()
{
    // $logindb = "";
    // $passwdb = "";
    // $line    = file(DATA);
    // foreach ($line as $temp) {
    //     $str     = explode(",", $temp);
    //     $logindb = chop($str[0]);
    //     $passwdb = chop($str[1]);
    // }
    // $link = mysql_connect('sql.dic.nazwa.pl', $logindb, $passwdb) or die("Nie mo  na si—Ö po é—Ïczy—è: " . mysql_error());

    // dev start
    $logindb = "root";
    $passwdb = "root";
    $line    = file(DATA);
    foreach ($line as $temp) {
        $str     = explode(",", $temp);
        $logindb = chop($str[0]);
        $passwdb = chop($str[1]);
    }
    $link = mysql_connect('localhost', $logindb, $passwdb) or die("nie mozna sie polaczyc: " . mysql_error());
    // dev end
    
    mysql_select_db("dic") or die("Nie mozna wybra—è bazy danych");
    
    mysql_query('SET character_set_connection=utf8');
    
    mysql_query('SET character_set_client=utf8');
    
    mysql_query('SET character_set_results=utf8');
    
    mysql_query('set names utf8;');
    
    return $link;
}
dblogin();

function getAllDeliveryOptions()
{
    $query = "SELECT * FROM PRZESYLKI";
    
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $data = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $data[] = $line;
    }
    return $data;
}

function getHighestPrice()
{
    $query = "SELECT MAX(Brutto) FROM PRODUKTY";
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $line = mysql_fetch_array($result);
    return (float)ceil($line[0]);
}

function getLowestPrice()
{
    $query = "SELECT MIN(Brutto) FROM PRODUKTY";
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $line = mysql_fetch_array($result);
    return (float)ceil($line[0]);
}

function getHighestMass()
{
    $query = "SELECT MAX(Masa) FROM PRODUKTY";
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $line = mysql_fetch_array($result);
    return (float)$line[0];
}

function getLowestMass()
{
    // $query = "SELECT MIN(Masa) FROM PRODUKTY";
    // $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    // $line = mysql_fetch_array($result);
    return 0.3;
}

function getDiamondPhotoUrl($ksztalt_id, $kolor_id = null)
{
    if ($ksztalt_id != null) {
        $query = "SELECT Nazwa FROM KSZTALTY where Id=$ksztalt_id";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row_ksztalt = mysql_fetch_array($result, MYSQL_ASSOC);
    }
    
    if ($kolor_id != null) {
        $query = "SELECT Nazwa FROM KOLORY where Id=$kolor_id";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row_kolor = mysql_fetch_array($result, MYSQL_ASSOC);
    }
    else {
        $query = "SELECT Nazwa FROM KOLORY where Id=15";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row_kolor = mysql_fetch_array($result, MYSQL_ASSOC);
    }
    
    return FOTO_FOLDER . $row_ksztalt[Nazwa] . $row_kolor[Nazwa] . '.jpg';
}

function getDiamondMiniPhotoUrl($ksztalt_id, $kolor_id = null)
{
    if ($ksztalt_id != null) {
        $query = "SELECT Nazwa FROM KSZTALTY where Id=$ksztalt_id";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row_ksztalt = mysql_fetch_array($result, MYSQL_ASSOC);
    }
    
    if ($kolor_id != null) {
        $query = "SELECT Nazwa FROM KOLORY where Id=$kolor_id";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row_kolor = mysql_fetch_array($result, MYSQL_ASSOC);
    }
    else {
        $query = "SELECT Nazwa FROM KOLORY where Id=15";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row_kolor = mysql_fetch_array($result, MYSQL_ASSOC);
    }
    
    return FOTO_FOLDER . $row_ksztalt[Nazwa] . $row_kolor[Nazwa] . 'mini.jpg';
}

function getUserData($id)
{
    $query = "SELECT Imie, Nazwisko, Email, Ulica, Miejscowosc, Telefon, KodPocztowy FROM USERS WHERE Id=$id";
    
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $line = mysql_fetch_array($result);
    return $line;
}

function getNewestProducts($number = 4)
{
    $query = "SELECT Id, DataDodania, Ksztalt, Masa, Barwa, Kolor, Czystosc, Sklep, StockNumber, Symetria, Polerowanie, Szlif, Laboratorium, Brutto, Pokaz, RodzajZestawienia, Nowosc FROM PRODUKTY WHERE Nowosc=1 ORDER BY RAND() LIMIT $number";
    
    $result = mysql_query($query) or die("Zapytanie zakoñczone niepowodzeniem");
    $data = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $data[] = $line;
    }
    return $data;
}

function getProductStockQuantity($id)
{
    $query = "SELECT Ilosc FROM TOWAR WHERE IdProd = $id";
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $quantity = 0;
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        if (intval($line['Ilosc']) == 1) {
            $quantity++;
        }
    }
    return $quantity;
}

function getProductFromStock($id)
{
    $query = "SELECT * FROM TOWAR WHERE Id = $id";
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $line = mysql_fetch_array($result);
    return $line;
}

function formatData($value)
{
    if($value == null) {
        return 'Brak danych';
    }
    else {
        return $value;
    }
}

function formatPrice($value)
{
    if($value == null) {
        return 'Brak danych';
    }
    else {
        return $value;
    }
}

function formatCert($value)
{
    if($value == null) {
        return 'e-diamenty.pl';
    }
    else {
        return $value;
    }
}

function sendEmail($from, $topic, $msg)
{
    $to = 'kontakt@e-diamenty.pl';
    $headers = 'From: ' . $from . "\r\n"
    .'Content-Type: text/plain; charset=utf-8'."\r\n";
    return mail($to, '=?utf-8?B?'.base64_encode($topic).'?=', $msg, $headers);
}

function formatMass($value)
{
    $after_dot = explode('.', $value)[1];

    if($value == null) {
        return 'Brak danych';
    }
    else if((strlen($after_dot) == 3) && substr($after_dot, -1) != 0) {
        return number_format(round($value, 3), 3);
    }
    else {
        return number_format(round($value, 2), 2);
    }
}

function kropek($string)
{
    return strtr($string, ",", ".");
}

function taknie($zerojeden)
{
    if ($zerojeden)
        return 'tak';
    else
        return 'nie';
}

function platnoscTyp($id)
{
    $id = (int)$id;
    $platnosc = 'Brak';
    if ($id == 0) {
        $platnosc = 'Brak';
    }
    else if ($id == 1) {
        $platnosc = 'Przelew na konto';
    }
    else if ($id == 2) {
        $platnosc = 'Pobranie';
    }
    else if ($id == 3) {
        $platnosc = 'Gotówka przy odbiorze';
    }
    else if ($id == 4) {
        $platnosc = 'Przelewy24';
    }

    return $platnosc;
}

function slownik($i) //$l
{
    //if(!isset($l))
    $l = $_GET[LANG];
    switch ($l) {
        case "EN":
            $lfield = "En";
            break;
        case "CZ":
            $lfield = "Cz";
            break;
        case "CN":
            $lfield = "Cn";
            break;
        case "ES":
            $lfield = "Es";
            break;
        case "PT":
            $lfield = "Pt";
            break;
        case "RU":
            $lfield = "Ru";
            break;
        case "DE":
            $lfield = "De";
            break;
        case "IT":
            $lfield = "It";
            break;
        default:
            $lfield = "Pl";
            break;
    }
    
    $query = "SELECT $lfield FROM YESSLOWNIK where Id=$i";
    $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    return $row[$lfield] . ($_GET[LANG] == "QQQ" ? "(" . $i . ")" : "");
}

function ksztalt($i)
{
    if ($i != null) {
        $query = "SELECT IdSlownik FROM KSZTALTY where Id=$i";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        return slownik($row[IdSlownik]);
    } else {
        return "Brak danych";
    }
}

function barwa($i)
{
    if ($i != null) {
        $query = "SELECT * FROM BARWY where Id=$i";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        return $row["Nazwa"];
    } else {
        return "Brak danych";
    }
}

function dostepnosc($i)
{
    if ($i != null) {
        $query = "SELECT * FROM ZESTRODZ where Nr=$i";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        return $row["Czas"];
    } else {
        return "Brak danych";
    }
}

function czystosc($i)
{
    if ($i != null) {
        $query = "SELECT * FROM CZYSTOSCI where Id=$i";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        return $row["Nazwa"];
    } else {
        return "Brak danych";
    }
}

function szlif($i)
{
    if ($i != null) {
        $query = "SELECT IdSlownik FROM SZLIFY where Id=$i";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        return slownik($row[IdSlownik]);
    } else {
        return "Brak danych";
    }
}

function symetria($i)
{
    if ($i != null) {
        $query = "SELECT IdSlownik FROM SYMETRIE where Id=$i";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        return slownik($row[IdSlownik]);
    } else {
        return "Brak danych";
    }
}

function polerowanie($i)
{
    if ($i != null) {
        $query = "SELECT IdSlownik FROM POLEROWANIA where Id=$i";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        return slownik($row[IdSlownik]);
    } else {
        return "Brak danych";
    }
}

function fluorescencja($i)
{
    if ($i != null) {
        $query = "SELECT IdSlownik FROM FLUORESCENCJE where Id=$i";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        return slownik($row[IdSlownik]);
    } else {
        return "Brak danych";
    }
}

function certyfikaty($i)
{
    if ($i != null) {
        $query = "SELECT * FROM LABORATORIA where Id=$i";
        $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        return $row["Nazwa"];
    } else {
        return null;
    }
}

function getProductFullName($id)
{
    $product = getProduct($id);
    return ksztalt($product['Ksztalt']) . ' ' . formatMass($product['Masa']) . 'ct ' . barwa($product['Barwa']) . '/' . czystosc($product['Czystosc']);
}

function number_of_all_products()
{
    $counter = mysql_query("SELECT COUNT(*) FROM PRODUKTY" . " WHERE (Pokaz = '1' ) AND (Idealny = '0')");
    $num     = mysql_fetch_array($counter);
    return $num[0];
}

function getProduct($id)
{
    $query = "SELECT * FROM PRODUKTY where Id=$id";
    $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
    $row = mysql_fetch_array($result, MYSQL_ASSOC);

    if ($row['Laboratorium'] == null) {
        $row['Szlif'] = 2;
        $row['Symetria'] = 2;
        $row['Polerowanie'] = 2;
    }
    return $row;
}

function isProductAvailable($id)
{
    $available = False;
    $query = "SELECT p.Id, p.DataDodania, p.Ksztalt, p.Masa, p.Barwa, p.Kolor, p.Czystosc, p.Sklep, p.StockNumber, p.Symetria, p.Polerowanie, p.Szlif, p.Laboratorium, p.Brutto, p.Pokaz, p.RodzajZestawienia, z.Czas FROM PRODUKTY AS p INNER JOIN ZESTRODZ AS z ON p.RodzajZestawienia=z.Nr where p.Id=$id AND p.Pokaz='1'";
    $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
    $product = mysql_fetch_array($result, MYSQL_ASSOC);
    if ($product != false) {
        $product_qty = getProductStockQuantity($product['Id']);
        if( $product_qty > 0 || $product['Laboratorium'] != null ){
            if ($product['Laboratorium'] == null) {
                $available = True;
            }
            else if ( ($product['Laboratorium'] != null) and (intval($product['Czas']) == 0) and ($product_qty > 0) ) {
                $available = True;
            }
            else if (($product['Laboratorium'] != null) and intval($product['Czas']) > 0) {
                $available = True;
            }
        }
    }

    return $available;
}

function getProductGalleryLinks($id)
{
    $query = "SELECT * FROM GALERIA WHERE IdProd='$id'";
    $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");

    $data = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $data[] = $line['Url'];
    }
    return $data;
}

function getRandomProducts($number = 4)
{
    $query = "SELECT * FROM PRODUKTY WHERE " .
    "(DataDodania >= '2016-01-06 00:00:00' ) AND " .
    "(Pokaz = '1' ) AND " .
    "(Idealny = '0' ) " .
    "ORDER BY RAND() LIMIT $number";
    $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");

    $data = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        if ($line['Laboratorium'] == null) {
            $line['Szlif'] = 2;
            $line['Symetria'] = 2;
            $line['Polerowanie'] = 2;
        }
        $data[] = $line;
    }
    return $data;
}

function getProductBySymbol($symbol)
{
    $query = "SELECT * FROM PRODUKTY where Symbol='$symbol'";
    $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
    $row = mysql_fetch_array($result, MYSQL_ASSOC);

    if ($row['Laboratorium'] == null) {
        $row['Szlif'] = 2;
        $row['Symetria'] = 2;
        $row['Polerowanie'] = 2;
    }
    return $row;
}

function getProductByCert($cert_id)
{
    $query = "SELECT * FROM PRODUKTY where Symbol='$cert_id'";
    $result = mysql_query($query);
    if ($result == false) {
        header('Location: ' . BASE_URL . '/views/sklep/katalog');
    }
    else {
        $row = mysql_fetch_array($result, MYSQL_ASSOC);

        if ($row['Laboratorium'] == null) {
            $row['Szlif'] = 2;
            $row['Symetria'] = 2;
            $row['Polerowanie'] = 2;
        }
        return $row;
    }
}

function shortProductDescription($id)
{
    $product = getProduct($id);
    if(dostepnosc($product['RodzajZestawienia']) == 0) { 
        $avail = 'Od ręki';
    } else { 
        $avail = dostepnosc($product['RodzajZestawienia']) . ' dni';
    }
    $short_desc = 'Kształt: ' . ksztalt($product['Ksztalt']) . ', ' .
    'Masa: ' . formatMass($product['Masa']) . ', ' .
    'Barwa: ' . barwa($product['Barwa']) . ', ' .
    'Czystość: ' . czystosc($product['Czystosc']) . ', ' .
    'Certyfikat: ' . formatCert(certyfikaty($product['Laboratorium'])) . ', ' .
    'Dostępność: ' . $avail . ', ' .
    'Szlif: ' . szlif($product['Szlif']) . ', ' .
    'Symetria: ' . symetria($product['Symetria']) . ', ' .
    'Polerowanie: ' . polerowanie($product['Polerowanie']) . ', ' .
    'Fluorescencja: ' . fluorescencja($product['Fluorescencja']) . ', ' .
    'Numer certyfikatu: ' . $product['Symbol'];

    return $short_desc;
}

function auth()
{
    return ( ($_SESSION["USER_AUTH"] == True) && ($_SESSION["SHOP_TOKEN"] == SHOP_TOKEN) && ($_SESSION["USER_LOGIN"] != 'admin') && ($_SESSION["USER_LOGIN"] != 'dic') );
}


function currtime()
{
    $timeres  = mysql_query("SELECT CURTIME()");
    $timeline = mysql_fetch_array($timeres);
    return $timeline[0];
}

function currdate()
{
    $datares  = mysql_query("SELECT CURDATE()");
    $dataline = mysql_fetch_array($datares);
    return $dataline[0];
}

function markProductAsBought($product)
{
    $id = $product['Id'];
    $stockQty = getProductStockQuantity($id);
    if ($stockQty > 0) {
        $query = "SELECT * FROM TOWAR WHERE IdProd='$id' AND Ilosc='1' LIMIT 1";
        $result = mysql_query($query);
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        $stockId = $row['Id'];

        $update_query = "UPDATE TOWAR SET Ilosc='0' WHERE Id='$stockId' AND Ilosc='1' LIMIT 1";
        mysql_query($update_query);
        if ($row) {
            return $row;
        }
        else {
            return null;
        } 
    }
    else {
        $query = "UPDATE PRODUKTY SET Pokaz='0' WHERE Id='$id'";
        $row = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem #1");
        return null;
    }
}

function setTransactionP24sessionId($transactionId, $sessionId)
{
        $query = "UPDATE ZAMOWIENIA SET IdPlatnosci='" . $sessionId . "' WHERE Id='$transactionId'";
        mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
}

function proccessTransaction($id, $session_id)
{
    if ($_POST['purchase']) {
        if ($_POST['shippment']) {
            $shippment = $_POST['shippment'];
        }
        if ($_POST['payment']) {
            $payment = $_POST['payment'];
        }
        if ($_POST['invoice']) {
            $invoice = $_POST['invoice'];
        }
        if ($_POST['comment']) {
            $comment = $_POST['comment'];
        }
        if ($_POST['payment']) {
            $payment = $_POST['payment'];
        }
        if ($_POST['invoice']) {
            if ($_POST['invoice'] == 'invoice1') {
                $invoice = '1';
            }
            else {
                $invoice = '0';
            }
        }
        $user_id = $_SESSION["USER_ID"];
        $current_user = getUserData($user_id); 
        
        $shippment_cost = 0;
        $shippment_text = "";

        $delivery_options = getAllDeliveryOptions();

        foreach ($delivery_options as $option) {
            if ($shippment == $option['Id']) {
                $shippment_text = $option['Nazwa'];
                $shippment_cost = $option['Koszt1'];
            }
        }
        
        $query = "INSERT INTO ZAMOWIENIA(IdUser, DataZam, CzasZam, Faktura, Platnosc, IdPlatnosci) VALUES('" . $user_id . "', '" . currdate() . "', '" . currtime() . "', '" . $invoice . "', '" . $payment . "', '" . $session_id . "')";
        mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $zam_id = mysql_insert_id();
        $query  = "SELECT * FROM PRODUKTY " . "WHERE Id IN (" . $_SESSION["KOSZYK"] . ")";
        
        $result = mysql_query($query) or die("Zapytanie zakoñczone niepowodzeniem");
        
        $sum_brutto = 0;
        $sum_netto  = 0;
        
        $products = array();
        $lista_prod = "";
        $items_qty = json_decode($_SESSION["KOSZYK-QTY"], true);
        
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
            if ($items_qty[$line["Id"]] == null) {
                $qty = '1';
            }
            else {
                $qty = (string)$items_qty[$line["Id"]];
            }

            $netto = (double)$line["Netto"];
            $brutto = (double)$line["Brutto"];
            $netto = number_format($netto, 2, '.', '');
            $brutto = number_format($brutto, 2, '.', '');

            if (getProductStockQuantity($line['Id']) < 1 ) {
                $line["StockNumber"] = "";
            }

            $line['Qty'] = $qty;
            $products[] = $line;
            $lista_prod = $lista_prod . getProductFullName($line['Id']) . ' - ' . $qty . ' szt.' . '<br>';

            for ($i=0; $i < (int)$qty; $i++) { 
                $boughtProduct = markProductAsBought($line);
                if ($boughtProduct != null) {
                    $kodKresk = $boughtProduct['KodKresk'];
                }
                else {
                    $kodKresk = "";
                }
                $query = "INSERT INTO PRODUKTYZAM SET IdZam='$zam_id', Symbol='" . $line["Symbol"] . "', Rozmiar='" . $line["Masa"] . "', Ilosc='1', Brutto='" . $brutto . "', Netto='" . $netto . "', Typ='" . $line["Typ"] . "', RodzajZestawienia='" . $line["RodzajZestawienia"] . "', KodKresk='" . $kodKresk . "'";
                mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
                $sum_brutto += $brutto;
                $sum_netto += $netto;
            }
        }

        $do_zaplaty = $sum_brutto + $shippment_cost;
        
        $query = "UPDATE ZAMOWIENIA SET KwotaBrutto='" . $sum_brutto . "', KwotaNetto='" . $sum_netto . "', Przesylka='" . $shippment_text . "', Przesylka2='" . $shippment_text . "', KosztPrzesylki='" . $shippment_cost . "', Komentarz='" . $comment . "', Zatwierdzone='1', DoZaplaty='" . $do_zaplaty . "' WHERE Id='$zam_id'";
        mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        
        $query = "SELECT * FROM ZAMOWIENIA WHERE Id = '$zam_id'";
        $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $zamowienie = mysql_fetch_array($result, MYSQL_ASSOC);

        $zamowienie['Products'] = $products;

        if ($zamowienie) {

            if ($lang == "PL"){
                $lang = "";
            }


            $myFile    = BASE_DIR . "../common/mail7" . $lang . ".txt";

            if ((int)$payment == 1) {
                $myFile    = BASE_DIR . "../common/mail7" . $lang . ".txt";
            }
            else if ((int)$payment == 2) {
                $myFile    = BASE_DIR . "../common/mail8" . $lang . ".txt";
            }
            else if ((int)$payment == 3) {
                $myFile    = BASE_DIR . "../common/mail9" . $lang . ".txt";
            }
            else if ((int)$payment == 4) {
                $myFile    = BASE_DIR . "../common/mail6" . $lang . ".txt";
            }

            $fh        = fopen($myFile, 'r');
            $wiadomosc = fread($fh, filesize($myFile));
            fclose($fh);

            $zam_komentarz = iconv('UTF-8', 'ISO-8859-2//TRANSLIT', $zamowienie['Komentarz']);
            
            $wiadomosc = str_replace("[IMIE]", $current_user['Imie'], $wiadomosc);
            $wiadomosc = str_replace("[NAZWISKO]", $current_user['Nazwisko'], $wiadomosc);
            $wiadomosc = str_replace("[NUMERZAMOWIENIA]", $zamowienie['Id'], $wiadomosc);
            $wiadomosc = str_replace("[KOMENTARZ]", $zam_komentarz, $wiadomosc);
            $wiadomosc = str_replace("[PRZESYLKA]", $zamowienie['Przesylka'], $wiadomosc);
            $wiadomosc = str_replace("[LISTAPRODUKTOW]", $lista_prod, $wiadomosc);
            $wiadomosc = str_replace("[KWOTA]", $zamowienie['DoZaplaty'] . ' PLN', $wiadomosc);
            
            $out = mail($current_user['Email'], "[e-diamenty.pl] Potwierdzenie zakupu", $wiadomosc, "Content-type: text/html; charset=iso-8859-2\r\nFrom: kontakt@e-diamenty.pl");
        }
        
        return $zamowienie;

    } else if ($id != null) {
        $query = "SELECT * FROM ZAMOWIENIA WHERE Id = '$id'";
        $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $zamowienie = mysql_fetch_array($result, MYSQL_ASSOC);

        // $query = "UPDATE ZAMOWIENIA SET IdPlatnosci='" . $session_id . "' WHERE Id='$id'";
        // mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $query = "SELECT * FROM PRODUKTYZAM WHERE IdZam = '$id'";
        $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $products = array();
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
            $product = getProductBySymbol($line['Symbol']);
            $product['Qty'] = $line['Ilosc'];
            $products = addProductToArray($product, $products);
        }
        $zamowienie['Products'] = $products;
        
        return $zamowienie;

    } else {
        return null;
    }
}

function addProductToArray($new_product, $products)
{
    $exists = false;
    $position = 0;
    for ($i=0; $i < count($products); $i++) { 
        if ($products[$i]['Id'] == $new_product['Id']) {
            $exists = true;
            $position = $i;
        }
    }
    if ($exists == true) {
        $products[$position]['Qty'] = (int)$products[$position]['Qty'] + 1;
    }
    else {
        $products[] = $new_product;
    }

    return $products;
}

function setMsg($msg)
{
    $_SESSION["MSG"] = $msg;
}

function printMsg()
{
    $msg = $_SESSION["MSG"];
    $_SESSION["MSG"] = '';
    return $msg;
}

function setErrorMsg($msg)
{
    $_SESSION["ERROR_MSG"] = $msg;
}

function printErrorMsg()
{
    $msg = $_SESSION["ERROR_MSG"];
    $_SESSION["ERROR_MSG"] = '';
    return $msg;
}

function getNewsPost($id)
{
    $post = array();

    if ( (intval($id) <= getNumberOfAllPosts() ) and (intval($id) > 0) ) {
        $newsFile = BASE_DIR . "cms/news/".$id.".txt";
        $fh = fopen($newsFile, 'r');
        $post['id'] = $id;
        $post['title'] = fgets($fh);
        $post['date'] = fgets($fh);
        $post['img'] = fgets($fh);
        $post['body'] = fread($fh, filesize($newsFile));
        fclose($fh);
    }

    return $post;
}

function getNewsPostBySlug($slug)
{
    $newsDir = BASE_DIR . "cms/news/";

    if (is_dir($newsDir)){
      if ($dh = opendir($newsDir)){
        while (($file = readdir($dh)) !== false){
          $ext = pathinfo($file, PATHINFO_EXTENSION);
          if ($ext == 'txt') {
            $fh = fopen($newsDir . $file, 'r');
            $post = array();
            $post['id'] = basename($file, ".txt");
            $post['title'] = fgets($fh);
            $post['date'] = trim(fgets($fh));
            $post['img'] = fgets($fh);
            $post['body'] = fread($fh, filesize($newsDir . $file));
            fclose($fh);
            if (convertToSlug($post['title']) == $slug) {
                $postBySlug = $post;
            }
          }
        }
        closedir($dh);
      }
    }

    return $postBySlug;
}

function renderPageContent($page_name)
{
    $newsFile = BASE_DIR . "cms/pages/" . $page_name . ".txt";

    $fh = fopen($newsFile, 'r');
    $body = fread($fh, filesize($newsFile));
    fclose($fh);

    $body = str_replace('<?= BASE_URL ?>', BASE_URL ,$body);

    return $body;
}

function getPageShortDescription($page_name)
{
    $newsFile = BASE_DIR . "cms/pages/" . $page_name . ".txt";

    $fh = fopen($newsFile, 'r');
    $body = fread($fh, filesize($newsFile));
    fclose($fh);

    $body = str_replace('<?= BASE_URL ?>', BASE_URL ,$body);

    $paragraphs = explode('<p>', $body);

    $p1 = $paragraphs[1];
    $p2 = $paragraphs[2];

    $p1 = str_replace('<p>','',$p1);
    $p1 = str_replace('</p>','',$p1);

    $p2 = str_replace('<p>','',$p2);
    $p2 = str_replace('</p>','',$p2);
    
    if (strlen($p1) < 90) {
       $p1 = $p1 . ' ' . $p2;
       $p1 = substr($p1, 0, 86);
    }

    if (strlen($p1) > 90) {
       $p1 = substr($p1, 0, 86);
    }

    return $p1 . '...';
}

function getAllNewsPosts($page = 1)
{
    $newsDir = BASE_DIR . "cms/news/";
    $newsPosts = array();

    if (is_dir($newsDir)){
      if ($dh = opendir($newsDir)){
        while (($file = readdir($dh)) !== false){
          $ext = pathinfo($file, PATHINFO_EXTENSION);
          if ($ext == 'txt') {
            $fh = fopen($newsDir . $file, 'r');
            $post = array();
            $post['id'] = basename($file, ".txt");
            $post['title'] = fgets($fh);
            $post['date'] = trim(fgets($fh));
            $post['img'] = fgets($fh);
            $post['body'] = fread($fh, filesize($newsDir . $file));
            fclose($fh);
            $newsPosts[] = $post;
          }
        }
        closedir($dh);
      }
    }
    foreach ($newsPosts as $key => $row) {
        $dates[$key]  = strtotime(date(str_replace("/",".",$row['date'])));
    }
    // var_dump($dates);
    array_multisort($dates, SORT_DESC, $newsPosts);
    $offset = $page * 4 - 4;
    return array_slice($newsPosts,$offset,4);
}

function getNumberOfAllPosts()
{
    $newsDir = BASE_DIR . "cms/news/";
    $quantity = 0;

    if (is_dir($newsDir)){
      if ($dh = opendir($newsDir)){
        while (($file = readdir($dh)) !== false){
          $ext = pathinfo($file, PATHINFO_EXTENSION);
          if ($ext == 'txt') {
            $quantity++;
          }
        }
        closedir($dh);
      }
    }
    return $quantity;
}

function getCurrentUserOrders(){
    $userid = $_SESSION['USER_ID'];
    $query = "SELECT * FROM ZAMOWIENIA WHERE IdUser='" . $userid . "' order by Id DESC";
    $result = mysql_query($query) or die("Zapytanie zakończone niepowodzeniem");
    $data = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $data[] = $line;
    }
    return $data;
}

function updateUserData()
{
if ($_POST['updateData']) {
        
        if (strlen($_POST['Email']) < 1 ) {
            $error = 'Wypełnij wszystkie wymagane pola.';
        }
        
        if ($error == "") {
            $imie     = $_POST['name'];
            $nazwisko = $_POST['surname'];
            $email    = $_POST['Email'];
            $telefon  = $_POST['telefon'];
            $adres    = $_POST['street'];
            $kod      = $_POST['kod'];
            $miasto   = $_POST['miasto'];
            
            if (strlen($imie) > 30)
                $imie = substr($imie, 0, 40);
            if (strlen($nazwisko) > 30)
                $nazwisko = substr($nazwisko, 0, 40);
            if (strlen($email) > 40)
                $email = substr($email, 0, 60);
            if (strlen($telefon) > 15)
                $telefon = substr($telefon, 0, 15);
            
            if (!preg_match('|^[_a-z0-9.-]*[a-z0-9]@[_a-z0-9.-]*[a-z0-9].[a-z]{2,3}$|e', $email)) {
                $error = 'Niepoprawne dane.';
            }
        }
    }
}

function changeUserPassword()
{
    if ($_POST['zmienhaslo']) {
        $old_pass_hash = sha1($_POST['starehaslo']);

        $query = "SELECT * FROM USERS WHERE Email='" . $_SESSION["USER_EMAIL"] . "' and Haslo='" . $old_pass_hash . "' and Login like 'K%' and Active=1 order by Id desc";

        $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $ile    = mysql_num_rows($result);
        $line   = mysql_fetch_array($result);
        $userid = $line[Id];
        mysql_free_result($result);
        
        if ($ile < 1) {
            setErrorMsg('Niepoprawne hasło. Spróbuj ponownie.');
            return False;
        }
        else if ($_POST['nowehaslo'] != $_POST['nowehaslopowtorz']) {
            setErrorMsg('Hasło powtórzone niepoprawnie.');
            return False;
        }
        else if (strlen($_POST['nowehaslo']) < 6) {
            setErrorMsg('Hasło musi mieć minimum 6 znaków.');
            return False;
        }
        else {
            $query = "UPDATE USERS SET Haslo='" . sha1($_POST['nowehaslo']) . "' WHERE Id='$userid'";
            mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
            setMsg('Hasło poprawnie zmienione.');
            return True;
        }
    }
}

function register($easy_register = false)
{
    if ($_GET['bezrej'] == 'true') {
        $_SESSION['BezRej'] = true;
    }
    else if($_GET['bezrej'] == 'false') {
        $_SESSION['BezRej'] = false;
        $_SESSION["USER_AUTH"] = null;
        $_SESSION["USER_LOGIN"] = null;
        $_SESSION["USER_EMAIL"] = null;
        $_SESSION["USER_ID"] = null;
    }
    $error = "";
    if ($_POST['rejestruj']) {

        if ($_POST['rule1'] != 'on') {
            $error = slownik(106);
        }

        if ($easy_register == false) {
            if (strlen($_POST['name']) < 1 OR 
                strlen($_POST['surname']) < 1 OR
                strlen($_POST['street']) < 1 OR 
                strlen($_POST['kod']) < 1 OR 
                strlen($_POST['miasto']) < 1 OR 
                strlen($_POST['Email']) < 1 ) {
                $error = slownik(107);
            }
        } 
        else {
            if ( strlen($_POST['Email']) < 1 ) {
                $error = slownik(107);
            }
        }
        
        if ($error == "") {
            $imie     = $_POST['name'];
            $nazwisko = $_POST['surname'];
            $email    = $_POST['Email'];
            $telefon  = $_POST['telefon'];
            $adres    = $_POST['street'];
            $kod      = $_POST['kod'];
            $miasto   = $_POST['miasto'];
            //$inny=$_POST[inny];
            
            if (strlen($imie) > 30)
                $imie = substr($imie, 0, 40);
            if (strlen($nazwisko) > 30)
                $nazwisko = substr($nazwisko, 0, 40);
            if (strlen($email) > 40)
                $email = substr($email, 0, 60);
            if (strlen($telefon) > 15)
                $telefon = substr($telefon, 0, 15);
            
            if (!preg_match('|^[_a-z0-9.-]*[a-z0-9]@[_a-z0-9.-]*[a-z0-9].[a-z]{2,3}$|e', $email)) {
                $error = 'Niepoprawne dane.';
            }
        }
        
        if ($error == "") {
            $login = "K" . rand(10000, 99999);
            $haslo = substr(md5($login), 0, 8);
            $kraj  = $_POST['kraje'];
            
            $lang = $_GET[LANG];
            
            $query = "SELECT * from USERS where Email='$email' and Login like 'K%' and BezRejestracji=0";
            $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
            
            if (mysql_num_rows($result) and !$_SESSION['BezRej'])
                $error = 'E-mail istnieje już w bazie.';
            else {
                if ($_POST['chkAdres_dostawy']) {
                    $pola2 = ",Nazwa2,Ulica2,KodPocztowy2,Miejscowosc2,KrajKod2,Telefon2,NazwaFirmy2";
                    $dane2 = ",'$_POST[imieplus]','$_POST[ulicaplus]','$_POST[kodplus]','$_POST[miastoplus]','$_POST[krajplus]','$_POST[telefonplus]','$_POST[nazwafirmyplus]'";
                }
                if ($_POST['nazwafirmy']) {
                    $polaf = ",NazwaFirmy,NIP,Faktura";
                    $danef = ",'$_POST[nazwafirmy]','$_POST[nip]',1";
                }
                
                $spam = (isset($_POST['chkPromocje']) ? 1 : 0);

                if ($_SESSION['BezRej']) {
                    $bez_rej = 1;
                }
                else {
                    $bez_rej = 0;
                }
                
                $query = "INSERT INTO USERS(Login,Haslo,Imie,Nazwisko,Ulica,KodPocztowy,Miejscowosc,Telefon,Email,KrajKod,Lang,Waluta,DataDodania,Klient,Newsletter$kraj1$pola2$polaf, BezRejestracji) VALUES('" . $login . "','" . sha1($haslo) . "','" . $imie . "','" . $nazwisko . "','" . $adres . "','" . $kod . "','" . $miasto . "','" . $telefon . "','" . $email . "','" . $kraj . "','" . $lang . "','" . $waluta . "',now(),1,$spam$kraj2$dane2$danef, $bez_rej)";
                
                $result1 = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
                
                if ($_SESSION['BezRej']) {
                    $zaloguj = login($email, sha1($haslo), true, $login);
                    header('Location: ' . BASE_URL . '/views/sklep/koszyk-krok2');
                    die();
                }
                else {     
                    if ($result1) {
                        //$ok=1;            
                        if ($lang == "PL")
                            $lang = "";
                        $myFile    = BASE_DIR . "../common/mail5" . $lang . ".txt";
                        $fh        = fopen($myFile, 'r');
                        $wiadomosc = fread($fh, filesize($myFile));
                        fclose($fh);
                        
                        //$wiadomosc = str_replace("[LOGIN]", $login, $wiadomosc);        
                        $wiadomosc = str_replace("[HASLO]", $haslo, $wiadomosc);
                        $wiadomosc = str_replace("[IMIE]", $imie, $wiadomosc);
                        $wiadomosc = str_replace("[NAZWISKO]", $nazwisko, $wiadomosc);
                        
                        //$wiadomosc = "Twoje haslo do sklepu: ".$haslo;
                        
                        $out = mail($email, "[e-diamenty.pl] Rejestracja konta", $wiadomosc, "Content-type: text/html; charset=iso-8859-2\r\nFrom: kontakt@e-diamenty.pl");
                    }

                    header('Location: ' . BASE_URL . '/views/sklep/zaloguj');
                    setMsg('Dane do logowania zostały wysłane na twój adres email.');
                    die();
                }
            }
        }
        setMsg($error);
        if($error == "") {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

function loggedOnly()
{
    if (!auth()) {
        header('Location: ' . BASE_URL . '/views/sklep/zaloguj');
        die();
    }
}

# zaloguj user-a
function login($login, $passwd, $bezRejestracji = false, $system_login = "")
{
    $_SESSION["USER_AUTH"] = null;
    $_SESSION["USER_LOGIN"] = null;
    $_SESSION["USER_EMAIL"] = null;
    $_SESSION["USER_ID"] = null;

    if ($bezRejestracji != true) {
        $_SESSION['BezRej'] = false;
    }

    if ($system_login != "" && $_SESSION['BezRej'] == true) {
        $query = "SELECT * FROM USERS WHERE Login='" . $system_login . "' and Haslo='" . $passwd . "' and Login like 'K%' and Active=1 and BezRejestracji=1 order by Id desc";
    }
    else {
        $query = "SELECT * FROM USERS WHERE Email='" . $login . "' and Haslo='" . $passwd . "' and Login like 'K%' and Active=1 and BezRejestracji=0 order by Id desc";
    }

    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $ile    = mysql_num_rows($result);
    $line   = mysql_fetch_array($result);
    $userid = $line[Id];
    mysql_free_result($result);
    
    if ($ile < 1) {
        $login = htmlentities($login);
        setErrorMsg('Niepoprawne dane logowania. Spróbuj ponownie.');
        return False;
    } else {
        if (!strcmp($login, $line[Email]) && !strcmp($passwd, $line[Haslo])) {
            $_SESSION["USER_AUTH"]  = True;
            $_SESSION["USER_EMAIL"] = $login;
            $_SESSION["USER_ID"]    = $userid;
            $_SESSION["SHOP_TOKEN"] = SHOP_TOKEN;
            
            return True;
        } else {
            $login = htmlentities($login);
            return False;
        }
    }
}

function koszyk()
{
    if (auth()) {
        if ($_SESSION[BezRej])
            $query = "SELECT * FROM USERS WHERE Email='" . $_SESSION["USER_EMAIL"] . "' and Login like 'S%' and BezRejestracji=1 and Active=1 order by Id desc";
        else
            $query = "SELECT * FROM USERS WHERE Email='" . $_SESSION["USER_EMAIL"] . "' and Login like 'S%' and BezRejestracji=0 and Active=1 order by Id desc";
        //$query = "SELECT * FROM USERS WHERE Email='" . $_SESSION["USER_EMAIL"] . "' and Login like 'S%' order by Id desc";
        $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $line   = mysql_fetch_array($result);
        $userid = $line[Id];
        
        $query = "SELECT * FROM ZAMOWIENIA WHERE IdUser='" . $userid . "' and Zatwierdzone='false' order by Id";
        $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $ile = mysql_num_rows($result);
        
        if ($ile < 1) {
            return 0;
        }
        
        $zamline = mysql_fetch_array($result, MYSQL_ASSOC);
        
        $query = "SELECT count(*) ile FROM PRODUKTYZAM WHERE IdZam='" . $zamline[Id] . "'";
        $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $line = mysql_fetch_array($result);
        return $line[ile];
    } else
        return $_SESSION["KOSZYK"];
}

function koszyk2()
{
    if (auth()) {
        if ($_SESSION[BezRej])
            $query = "SELECT * FROM USERS WHERE Email='" . $_SESSION["USER_EMAIL"] . "' and Login like 'S%' and BezRejestracji=1 and Active=1 order by Id desc";
        else
            $query = "SELECT * FROM USERS WHERE Email='" . $_SESSION["USER_EMAIL"] . "' and Login like 'S%' and BezRejestracji=0 and Active=1 order by Id desc";
        //$query = "SELECT * FROM USERS WHERE Email='" . $_SESSION["USER_EMAIL"] . "' and Login like 'S%' order by Id desc";
        $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $line   = mysql_fetch_array($result);
        $userid = $line[Id];
        
        $query = "SELECT * FROM ZAMOWIENIA WHERE IdUser='" . $userid . "' and Zatwierdzone='false' order by Id";
        $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $ile = mysql_num_rows($result);
        
        if ($ile < 1) {
            return 0;
        }
        
        $zamline = mysql_fetch_array($result, MYSQL_ASSOC);
        
        $query = "SELECT sum(Brutto) brt FROM PRODUKTYZAM WHERE IdZam='" . $zamline[Id] . "'";
        $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $line = mysql_fetch_array($result);
        return $line[brt];
    } else {
        $kwota = 0;
        for ($i = 0; $i < $_SESSION["KOSZYK"]; $i++)
            $kwota += $_SESSION["BRUT" . $i];
        return $kwota;
    }
}

# wyloguj user-a
function logout()
{
    $_SESSION["USER_AUTH"]  = False;
    $_SESSION["USER_EMAIL"] = Null;
    $_SESSION["USER_LOGIN"] = Null;
    header('Location: ' . BASE_URL);
    die();
}

function lostPassword()
{
    if ($_POST['przypomnij']) {
        $email = $_POST['email'];
        $error = "";

        if (strlen($email) < 1) {
            $error = 'Niepoprawne dane.';
        }
        else if (!preg_match('|^[_a-z0-9.-]*[a-z0-9]@[_a-z0-9.-]*[a-z0-9].[a-z]{2,3}$|e', $email)) {
            $error = 'Niepoprawne dane.';
        }

        if ($error == "") {
            $query = "SELECT * FROM USERS WHERE Email='" . $email . "' and Login like 'K%' and Active=1 and BezRejestracji=0 order by Id desc";

            $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
            $ile    = mysql_num_rows($result);
            $line   = mysql_fetch_array($result);
            $userid = $line['Id'];
            $login = $line['Login'];
            $email = $line['Email'];
            mysql_free_result($result);

            $haslo = substr(md5($login), 0, 8);

            if ($ile < 1) {
                setErrorMsg("Niepoprawne dane.");
                header('Location: ' . BASE_URL . '/views/sklep/przypomnij-haslo');
                die();
            }
            else {

                $query = "UPDATE USERS SET Haslo='" . sha1($haslo) . "' WHERE Id='" . $userid . "'";
                
                $result1 = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");

                if ($result1) {
                    // if ($lang == "PL")
                    //     $lang = "";
                    // $myFile    = BASE_DIR . "../common/mail5" . $lang . ".txt";
                    // $fh        = fopen($myFile, 'r');
                    // $wiadomosc = fread($fh, filesize($myFile));
                    // fclose($fh);
                    
                    // $wiadomosc = str_replace("[LOGIN]", $login, $wiadomosc);        
                    // $wiadomosc = str_replace("[HASLO]", $haslo, $wiadomosc);
                    // $wiadomosc = str_replace("[IMIE]", $imie, $wiadomosc);
                    // $wiadomosc = str_replace("[NAZWISKO]", $nazwisko, $wiadomosc);

                    $wiadomosc = "Twoje dane do logowania to:<br>Login: " . $email . "<br>Hasło: " . $haslo;
                    
                    $out = mail($email, "[e-diamenty.pl] Przypomnij haslo", $wiadomosc, "Content-type: text/html; charset=utf-8\r\nFrom: kontakt@e-diamenty.pl");

                    header('Location: ' . BASE_URL . '/views/sklep/zaloguj');
                    setMsg('Dane do logowania zostały wysłane na twój adres email.');
                    die();
                }
                else {
                    setErrorMsg("Niepoprawne dane.");
                    header('Location: ' . BASE_URL . '/views/sklep/przypomnij-haslo');
                    die();
                }
            }
        }
        else {
            setErrorMsg($error);
            header('Location: ' . BASE_URL . '/views/sklep/przypomnij-haslo');
            die();
        }
    }
}

function kraje($lang, $wybr)
{
    $l = $lang;
    switch ($lang) {
        case "EN":
        case "CZ":
            break;
        default:
            $l = "";
    }
    $query = "SELECT * FROM KRAJE order by Priorytet, Nazwa$l";
    $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");
    
    $str = "";
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $str .= "<option value='$line[Kod]'" . ($wybr == $line[Kod] ? " selected" : "") . ">" . $line["Nazwa" . $l] . "</option>\n";
    }
    return $str;
}

/** 
 * Truncates text.
 *
 * Cuts a string to the length of $length and replaces the last characters
 * with the ending if the text is longer than length.
 *
 * @param string $text String to truncate.
 * @param integer $length Length of returned string, including ellipsis.
 * @param string $ending Ending to be appended to the trimmed string.
 * @param boolean $exact If false, $text will not be cut mid-word
 * @param boolean $considerHtml If true, HTML tags would be handled correctly
 * @return string Trimmed string.
 */
function truncate($text, $length = 100, $ending = '...', $exact = true, $considerHtml = true)
{
    if ($considerHtml) {
        // if the plain text is shorter than the maximum length, return the whole text
        if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
            return $text;
        }
        
        // splits all html-tags to scanable lines
        preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
        
        $total_length = strlen($ending);
        $open_tags    = array();
        $truncate     = '';
        
        foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output
            if (!empty($line_matchings[1])) {
                // if it ÈÖs an  È·empty element ÈÝ with or without xhtml-conform closing slash (f.e.)
                if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                    // do nothing
                    // if tag is a closing tag (f.e.)
                } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                    // delete tag from $open_tags list
                    $pos = array_search($tag_matchings[1], $open_tags);
                    if ($pos !== false) {
                        unset($open_tags[$pos]);
                    }
                    // if tag is an opening tag (f.e. )
                } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                    // add tag to the beginning of $open_tags list
                    array_unshift($open_tags, strtolower($tag_matchings[1]));
                }
                // add html-tag to $truncate ÈÖd text
                $truncate .= $line_matchings[1];
            }
            
            // calculate the length of the plain text part of the line; handle entities as one character
            $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
            if ($total_length + $content_length > $length) {
                // the number of characters which are left
                $left            = $length - $total_length;
                $entities_length = 0;
                // search for html entities
                if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                    // calculate the real length of all entities in the legal range
                    foreach ($entities[0] as $entity) {
                        if ($entity[1] + 1 - $entities_length <= $left) {
                            $left--;
                            $entities_length += strlen($entity[0]);
                        } else {
                            // no more characters left
                            break;
                        }
                    }
                }
                $truncate .= substr($line_matchings[2], 0, $left + $entities_length);
                // maximum lenght is reached, so get off the loop
                break;
            } else {
                $truncate .= $line_matchings[2];
                $total_length += $content_length;
            }
            
            // if the maximum length is reached, get off the loop
            if ($total_length >= $length) {
                break;
            }
        }
    } else {
        if (strlen($text) <= $length) {
            return $text;
        } else {
            $truncate = substr($text, 0, $length - strlen($ending));
        }
    }
    
    // if the words shouldn't be cut in the middle...
    if (!$exact) {
        // ...search the last occurance of a space...
        $spacepos = strrpos($truncate, ' ');
        if (isset($spacepos)) {
            // ...and cut the text in this position
            $truncate = substr($truncate, 0, $spacepos);
        }
    }
    
    // add the defined ending to the text
    $truncate .= $ending;
    
    if ($considerHtml) {
        // close all unclosed html-tags
        foreach ($open_tags as $tag) {
            $truncate .= '';
        }
    }
    
    return $truncate;
    
}

function html_cut($text, $max_length)
{
    $tags   = array();
    $result = "";
    
    $is_open          = false;
    $grab_open        = false;
    $is_close         = false;
    $in_double_quotes = false;
    $in_single_quotes = false;
    $tag              = "";
    
    $i        = 0;
    $stripped = 0;
    
    $stripped_text = strip_tags($text);
    
    while ($i < mb_strlen($text, 'utf-8') && $stripped < mb_strlen($stripped_text, 'utf-8') && $stripped < $max_length) {
        $symbol = mb_substr($text, $i, 1, 'utf-8');
        $result .= $symbol;
        
        switch ($symbol) {
            case '<':
                $is_open   = true;
                $grab_open = true;
                break;
            
            case '"':
                if ($in_double_quotes)
                    $in_double_quotes = false;
                else
                    $in_double_quotes = true;
                
                break;
            
            case "'":
                if ($in_single_quotes)
                    $in_single_quotes = false;
                else
                    $in_single_quotes = true;
                
                break;
            
            case '/':
                if ($is_open && !$in_double_quotes && !$in_single_quotes) {
                    $is_close  = true;
                    $is_open   = false;
                    $grab_open = false;
                }
                
                break;
            
            case ' ':
                if ($is_open)
                    $grab_open = false;
                else
                    $stripped++;
                
                break;
            
            case '>':
                if ($is_open) {
                    $is_open   = false;
                    $grab_open = false;
                    array_push($tags, $tag);
                    $tag = "";
                } elseif ($is_close) {
                    $is_close = false;
                    array_pop($tags);
                    $tag = "";
                }
                
                break;
            
            default:
                if ($grab_open || $is_close)
                    $tag .= $symbol;
                
                if (!$is_open && !$is_close)
                    $stripped++;
        }
        
        $i++;
    }
    
    while ($tags) {
        if (($tag = array_pop($tags)) != 'br') {
            $result .= "</$tag>";
        }
    }
    
    return $result;
}

function cmp_news_date($a, $b)
{
    return date($b['date']) - date($a['date']);
}

function remove_accent($str)
{
$a = array('À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ','Ā','ā','Ă','ă','Ą','ą','Ć','ć','Ĉ','ĉ','Ċ','ċ','Č','č','Ď','ď','Đ','đ','Ē','ē','Ĕ','ĕ','Ė','ė','Ę','ę','Ě','ě','Ĝ','ĝ','Ğ','ğ','Ġ','ġ','Ģ','ģ','Ĥ','ĥ','Ħ','ħ','Ĩ','ĩ','Ī','ī','Ĭ','ĭ','Į','į','İ','ı','Ĳ','ĳ','Ĵ','ĵ','Ķ','ķ','Ĺ','ĺ','Ļ','ļ','Ľ','ľ','Ŀ','ŀ','Ł','ł','Ń','ń','Ņ','ņ','Ň','ň','ŉ','Ō','ō','Ŏ','ŏ','Ő','ő','Œ','œ','Ŕ','ŕ','Ŗ','ŗ','Ř','ř','Ś','ś','Ŝ','ŝ','Ş','ş','Š','š','Ţ','ţ','Ť','ť','Ŧ','ŧ','Ũ','ũ','Ū','ū','Ŭ','ŭ','Ů','ů','Ű','ű','Ų','ų','Ŵ','ŵ','Ŷ','ŷ','Ÿ','Ź','ź','Ż','ż','Ž','ž','ſ','ƒ','Ơ','ơ','Ư','ư','Ǎ','ǎ','Ǐ','ǐ','Ǒ','ǒ','Ǔ','ǔ','Ǖ','ǖ','Ǘ','ǘ','Ǚ','ǚ','Ǜ','ǜ','Ǻ','ǻ','Ǽ','ǽ','Ǿ','ǿ');
$b = array('A','A','A','A','A','A','AE','C','E','E','E','E','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','ae','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','o','u','u','u','u','y','y','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','D','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','IJ','ij','J','j','K','k','L','l','L','l','L','l','L','l','l','l','N','n','N','n','N','n','n','O','o','O','o','O','o','OE','oe','R','r','R','r','R','r','S','s','S','s','S','s','S','s','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Y','Z','z','Z','z','Z','z','s','f','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','A','a','AE','ae','O','o');
return str_replace($a, $b, $str);
}

function convertToSlug($str)
{
return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), remove_accent($str)));
}
