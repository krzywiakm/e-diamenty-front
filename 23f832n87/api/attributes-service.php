<?php
include "func.php";



$data = array();

$data['min_price'] = getLowestPrice();
$data['max_price'] = getHighestPrice();
$data['min_mass'] = getLowestMass();
$data['max_mass'] = getHighestMass();

$data = json_encode($data, JSON_HEX_AMP);


die($data);