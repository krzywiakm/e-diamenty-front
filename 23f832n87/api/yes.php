<?

function yesKolekcje()
{
    $query = "SELECT * FROM YESKOLEKCJE";
    
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $data = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $data[] = $line;
    }
    return $data;
}

function getYesKolekcjeById($id)
{
    $query = "SELECT * FROM YESKOLEKCJE WHERE Id=$id";
    
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $line = mysql_fetch_array($result);
    return $line;
}

function yesMetale()
{
    $query = "SELECT * FROM YESMETALE";
    
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $data = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $data[] = $line;
    }
    return $data;
}

function getYesMetaleById($id)
{
    $query = "SELECT * FROM YESMETALE WHERE Id=$id";
    
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $line = mysql_fetch_array($result);
    return $line;
}

function yesProdukty()
{
    $query = "SELECT * FROM YESPRODUKTY";
    
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $data = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $data[] = $line;
    }
    return $data;
}

function getYesProduktByParams($kolekcjaId, $metalId, $typId)
{
    $kolekcjaId = intval($kolekcjaId);
    $metalId = intval($metalId);
    $typId = intval($typId);

    $query = "SELECT * FROM YESPRODUKTY WHERE IdKolekcja='$kolekcjaId' AND IdMetal='$metalId' AND IdTypu='$typId'";
    
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $line = mysql_fetch_array($result);
    return $line;
}

function getYesProduktById($id)
{
    $query = "SELECT * FROM YESPRODUKTY WHERE Id=$id";
    
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $line = mysql_fetch_array($result);
    return $line;
}

function yesTypy()
{
    $query = "SELECT * FROM YESTYPY";
    
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $data = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $data[] = $line;
    }
    return $data;
}

function getYesTypeById($id)
{
    $query = "SELECT * FROM YESTYPY WHERE Id=$id";
    
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $line = mysql_fetch_array($result);
    return $line;
}

function productImgUrl($kolekcjaId, $metalId, $productTypeId, $weight = 0.5) {
    $kolekcjaNazwa = getYesKolekcjeById($kolekcjaId)['Nazwa'];
    $typSymbol = getYesTypeById($productTypeId)['Symbol'];
    if ($metalId == 1) {
        $color = 'y';
    }
    else {
        $color = 'w';
    }
    $sizes = productImgUrlAllSizes($kolekcjaId, $productTypeId, $metalId);

    if ($weight < 0.5) {
        if ($sizes['030']) {
            $size = '030';
        }
        else if($sizes['050']) {
            $size = '050';
        }
    }
    else if ($weight < 0.7) {
        if ($sizes['050']) {
            $size = '050';
        }
        else if($sizes['070']) {
            $size = '070';
        }
    }
    else if ($weight < 1.0) {
        if ($sizes['070']) {
            $size = '070';
        }
    }
    else if ($weight < 1.5) {
        if ($sizes['100']) {
            $size = '100';
        }
    }
    else if ($weight < 2.0) {
        if ($sizes['150']) {
            $size = '150';
        }
        else if ($sizes['100']) {
            $size = '100';
        }
    }
    else if ($weight >= 2.0) {
        if ($sizes['200']) {
            $size = '200';
        }
        else if ($sizes['150']) {
            $size = '150';
        }
        else if ($sizes['100']) {
            $size = '100';
        }
    }
    if ($size == "") {
        $size = '050';
    }

    return 'assets/images/produkty/' . strtolower($kolekcjaNazwa) .'-'. strtolower($typSymbol) .'-'. $color .'-'. $size.'.jpg';
}

function productImgUrlAllSizes($kolekcjaId, $productTypeId, $metalId) {
    $kolekcjaNazwa = getYesKolekcjeById($kolekcjaId)['Nazwa'];
    $typSymbol = getYesTypeById($productTypeId)['Symbol'];
    if ($metalId == 1) {
        $color = 'y';
    }
    else {
        $color = 'w';
    }
    $imgsAll = array();
    if ($handle = opendir('assets/images/produkty')) {

        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                $imgName = strtolower($kolekcjaNazwa) .'-'. strtolower($typSymbol) .'-'. $color;
                if (substr($entry,0,strlen($imgName)) == $imgName) {
                    $size = substr(substr($entry,-7),0,3);
                    $filename = $entry;
                    $imgsAll[$size] = $filename;
                }
            }
        }
        closedir($handle);
    }
    return $imgsAll;
}

function getYesShopOrderById($id) {
    $sklepId = $_SESSION['YES_SHOP_ID'];
    $query = "SELECT * FROM ZAMOWIENIA WHERE Id=$id AND IdYesSklep=$sklepId";
    
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $order = mysql_fetch_array($result);
    $query = "SELECT * FROM PRODUKTYZAM WHERE IdZam = '$id'";
    $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
    $products = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $product = getProductBySymbol($line['Symbol']);
        $product['Qty'] = $line['Ilosc'];
        $products = addProductToArray($product, $products);
    }
    $order['Products'] = $products;

    if (isset($order['IdUser'])) {
        $query = "SELECT * FROM USERS WHERE Id=$order[IdUser]";
        
        $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
        $user = mysql_fetch_array($result);

        $order['user'] = $user;
        return $order;
    }
    else {
        return null;
    }
}

function getCurrentYesShopOrders($sortBy = "Id", $sortDirection = "desc") {
    $sort = $sortBy.' '.$sortDirection;
    $sklepId = $_SESSION['YES_SHOP_ID'];
    $query = "SELECT ZAMOWIENIA.Id, ZAMOWIENIA.IdUser, ZAMOWIENIA.IdYesSklep, ZAMOWIENIA.KwotaNetto, ZAMOWIENIA.KwotaBrutto, ZAMOWIENIA.DoZaplaty, ZAMOWIENIA.DataZam, ZAMOWIENIA.CzasZam, ZAMOWIENIA.Zatwierdzone, ZAMOWIENIA.Wykonane, ZAMOWIENIA.Zaplacone, ZAMOWIENIA.Przyjete, ZAMOWIENIA.Oczekujace, USERS.Login, USERS.Imie, USERS.Nazwisko, USERS.Email FROM ZAMOWIENIA INNER JOIN USERS ON ZAMOWIENIA.IdUser = USERS.Id WHERE ZAMOWIENIA.IdYesSklep='" . $sklepId . "' ORDER BY $sort";
    $result = mysql_query($query) or die("Zapytanie zakończone niepowodzeniem");
    $data = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $data[] = $line;
    }
    return $data;
}

function productTypeNameToSymbol($name) {
    if ($name == 'pierścionek') {
        return 'ring1';
    }
    else if ($name == 'pierścionek diamenty po bokach') {
        return 'ring2';
    }
    else if ($name == 'wisiorek') {
        return 'pendant1';
    }
    else if ($name == 'kolczyki') {
        return 'earrings1';
    }
    else {
        return null;
    }
}

function createTransaction() {
    if ($_POST['purchase']) {

        if (strlen($_POST['name']) > 0 && strlen($_POST['surname']) > 0 && strlen($_POST['email']) > 0 && strlen($_POST['telefon']) > 0) {
            //$current_user = getUserData($user_id);

            $login = "Y" . rand(10000, 99999);
            $haslo = substr(md5($login), 0, 8);
            $imie = $_POST['name'];
            $nazwisko = $_POST['surname'];
            $adres = $_POST['street'];
            $kod = $_POST['kod'];
            $miasto = $_POST['miasto'];
            $telefon = $_POST['telefon'];
            $email = $_POST['email'];
            $comment = $_POST['komentarz'];
            $product_full_name = $_POST['product-full-name'];
            $czas_realizacji = $_POST['czas-realizacji'];

            $kraj = 0;
            $lang = "PL";
            $waluta = "PLN";

            $query = "INSERT INTO USERS(Login,Haslo,Imie,Nazwisko,Ulica,KodPocztowy,Miejscowosc,Telefon,Email,KrajKod,Lang,Waluta,DataDodania,Klient, BezRejestracji) VALUES('" . $login . "','" . sha1($haslo) . "','" . $imie . "','" . $nazwisko . "','" . $adres . "','" . $kod . "','" . $miasto . "','" . $telefon . "','" . $email . "','" . $kraj . "','" . $lang . "','" . $waluta . "',now(),1,1)";
            
            $result = mysql_query($query) or die("Zapytanie zakonczone niepowodzeniem");

            $user_id = mysql_insert_id();
            
            $shippment_cost = 0;

            $storeId = $_SESSION['YES_SHOP_ID'];
            $yes_sklep_email = $_SESSION['store_email'];
            $yesProductId = getYesProduktByParams($_POST['collection-id'], $_POST['metal-id'], $_POST['product-type'])['Id'];
            $zaliczka = $_POST['Zaliczka'];
            if ($_POST['product-type'] == '1' || $_POST['product-type'] == '2') {
                $yesRozmiar = intval($_POST['rozmiar']);
            }
            else {
                $yesRozmiar = 0;
            }
            
            
            $query = "INSERT INTO ZAMOWIENIA(IdUser, IdYesSklep, IdYesProdukt, DataZam, CzasZam, Zaliczka, YesRozmiar) VALUES('" . $user_id . "', '" . $storeId . "', '" . $yesProductId . "', '" . currdate() . "', '" . currtime() . "', '" . $zaliczka . "', '" . $yesRozmiar . "')";
            mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
            $zam_id = mysql_insert_id();
            
            $cart_ids = $_POST['diamond1'];
            if ($_POST['product-type'] == '4') {
                $cart_ids = $_POST['diamond1'] . ', ' . $_POST['diamond2'];
            }

            $query  = "SELECT * FROM PRODUKTY " . "WHERE Id IN (" . $cart_ids . ")";
            
            $result = mysql_query($query) or die("Zapytanie zakoñczone niepowodzeniem");
            
            $products = array();
            $lista_prod = "";
            
            while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
                $qty = '1';
                $netto = (double)$line["Netto"];
                $brutto = (double)$line["Brutto"];
                $netto = number_format($netto, 2, '.', '');
                $brutto = number_format($brutto, 2, '.', '');

                if (getProductStockQuantity($line['Id']) < 1 ) {
                    $line["StockNumber"] = "";
                }

                $line['Qty'] = $qty;
                $products[] = $line;
                //$lista_prod = $lista_prod . getProductFullName($line['Id']) . ' - ' . $qty . ' szt.' . '<br>';

                $boughtProduct = markProductAsBought($line);
                if ($boughtProduct != null) {
                    $kodKresk = $boughtProduct['KodKresk'];
                }
                else {
                    $kodKresk = "";
                }

                $query = "INSERT INTO PRODUKTYZAM SET IdZam='$zam_id', Symbol='" . $line["Symbol"] . "', Rozmiar='" . $line["Masa"] . "', Ilosc='1', Brutto='" . $brutto . "', Netto='" . $netto . "', Typ='" . $line["Typ"] . "', RodzajZestawienia='" . $line["RodzajZestawienia"] . "', KodKresk='" . $kodKresk . "'";
                mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
            }

            $total_sums = calculateTheTotalPrice($_POST['collection-id'], $_POST['metal-id'], $_POST['product-type'], $_POST['diamond1'], $_POST['diamond2']);

            $total_sums = str_replace(" ", "", $total_sums);

            $sum_brutto = number_format($total_sums, 2, '.', '');
            $sum_netto = bruttoToNetto($sum_brutto, 0.23);

            $do_zaplaty = $sum_brutto;

            if ($yesRozmiar > 0) {
                $commentRozmiar = "<br>Rozmiar pierścionka: " . strval($yesRozmiar);
            }
            else {
                $commentRozmiar = "";
            }

            $comment_with_info = "Zamówiony produkt:<br>" . $product_full_name . $commentRozmiar . "<br>Potwierdzam przyjęcie zadatku w kwocie " . $zaliczka . " PLN" . " na poczet płatności za sprowadzony diament." . "<br><br>" . "Treść komentarza:<br>". $comment . "<br><br>YES biżuteria: " . $_SESSION['store'] . " " . $_SESSION['store_address'];
            
            $query = "UPDATE ZAMOWIENIA SET KwotaBrutto='" . $sum_brutto . "', KwotaNetto='" . $sum_netto . "',  KosztPrzesylki='" . $shippment_cost . "', Komentarz='" . $comment_with_info . "', Zatwierdzone='1', DoZaplaty='" . $do_zaplaty . "' WHERE Id='$zam_id'";
            mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");

            if ($zam_id) {
                $myFile    = BASE_DIR . "../common/yesmail1.txt";
                //$myFile    = BASE_DIR . "yesmail1.txt";


                $yes_salon_nazwa = $_SESSION['store'];
                $yes_salon_adres = $_SESSION['store_address'];

                $fh        = fopen($myFile, 'r');
                $wiadomosc = fread($fh, filesize($myFile));
                fclose($fh);

                $comment = iconv('UTF-8', 'ISO-8859-2//TRANSLIT', $comment);
                $product_full_name = iconv('UTF-8', 'ISO-8859-2//TRANSLIT', $product_full_name);
                $yes_salon_nazwa = iconv('UTF-8', 'ISO-8859-2//TRANSLIT', $yes_salon_nazwa);
                $yes_salon_adres = iconv('UTF-8', 'ISO-8859-2//TRANSLIT', $yes_salon_adres);
                
                $wiadomosc = str_replace("[IMIE]", $imie, $wiadomosc);
                $wiadomosc = str_replace("[NAZWISKO]", $nazwisko, $wiadomosc);
                $wiadomosc = str_replace("[NUMERZAMOWIENIA]", $zam_id, $wiadomosc);
                $wiadomosc = str_replace("[KOMENTARZ]", $comment, $wiadomosc);
                $wiadomosc = str_replace("[YESPRODUKT]", $product_full_name, $wiadomosc);
                $wiadomosc = str_replace("[ZALICZKA]", $zaliczka . ' PLN', $wiadomosc);
                $wiadomosc = str_replace("[KWOTA]", $do_zaplaty . ' PLN', $wiadomosc);
                $wiadomosc = str_replace("[CZASREALIZACJI]", $czas_realizacji, $wiadomosc);
                $wiadomosc = str_replace("[YESSALON]", $yes_salon_nazwa, $wiadomosc);
                $wiadomosc = str_replace("[YESSALONADRES]", $yes_salon_adres, $wiadomosc);

                //echo $wiadomosc;
                
                $out = mail($yes_sklep_email, "[diamenty.yes.pl] Potwierdzenie zakupu", $wiadomosc, "Content-type: text/html; charset=iso-8859-2\r\nFrom: kontakt@e-diamenty.pl");
                $out = mail("kontakt@e-diamenty.pl", "[diamenty.yes.pl] Potwierdzenie zakupu", $wiadomosc, "Content-type: text/html; charset=iso-8859-2\r\nFrom: kontakt@e-diamenty.pl");
            }

            return $zam_id;
        }
        else {
            setMsg('Wypełnij wszystkie wymagane pola.');
            return null;
        }
    }
    else {
        return null;
    }
}

function calculateTheTotalPrice($collectionId, $metalId, $productTypeId, $diamond1Id, $diamond2Id = 0) {
    if ($diamond1Id != 0) {
        $diamond1 = getProduct($diamond1Id);
        $price = $diamond1['Brutto'];
    }
    if ($diamond1Id != 0 && $diamond2Id != 0) {
        $diamond1 = getProduct($diamond1Id);
        $diamond2 = getProduct($diamond2Id);
        $price = $diamond1['Brutto'] + $diamond2['Brutto'];
    }

    if ($productTypeId == 1) {
        $price = ($price*1.96+2132.51)*1.23;
    }
    else if ($productTypeId == 2) {
        $price = ($price*1.96+2132.51)*1.23;
    }
    else if ($productTypeId == 3) {
        $price = ($price*1.96+1272.16)*1.23;
    }
    else if ($productTypeId == 4) {
        $price = ($price*1.96+2555.96)*1.23;
    }

    $yesProduct = getYesProduktByParams($collectionId, $metalId, $productTypeId);
    if ($yesProduct['DodatkowyKoszt']) {
        $price = $price + $yesProduct['DodatkowyKoszt'];
    }

    if ($yesProduct['ZakresMasaKoszt'] != "" && $yesProduct['ZakresMasaKoszt'] != null) {
        $zakresy = explode("-", $yesProduct['ZakresMasaKoszt']);
        for ($i=0; $i < count($zakresy); $i++) { 
            $parts = explode(":", $zakresy[$i]);
            $mass_from = $parts[0];
            $mass_to = $parts[1];
            $additionalCost = $parts[2];
            if (floatval($diamond1['Masa']) >= floatval($mass_from) && floatval($diamond1['Masa']) <= floatval($mass_to)) {
                $price = $price + $additionalCost;
            }
        }
    }

    $price = yesRoundPrice($price);
    $price = yesFormatPrice($price);

    return $price;
}

function yesRoundPrice($price) {
    $change_arr = array(
        "99" => array("0"=>-1, "1"=>-2, "2"=>0, "3"=>2, "4"=>1, "5"=>0, "6"=>-1, "7"=>2, "8"=>1, "9"=>0),
        "299" => array("0"=>-1, "1"=>-2, "2"=>-3, "3"=>2, "4"=>1, "5"=>0, "6"=>3, "7"=>2, "8"=>1, "9"=>0)
    );
    $price = intval($price);
    if ( $price <= 99 ) {
        $price = $price + ($change_arr["99"][strval($price)[1]]);
    }
    else if ($price <= 299) {
        $price = $price + ($change_arr["299"][strval($price)[2]]);
    }
    else if ($price <= 979) {
        $round_part = intval(substr(strval($price),1,3));
        if ($round_part <= 8) {
            $price = intval(strval((intval(strval($price)[0])-1)) . '99');
        }
        else if ($round_part >= 9 && $round_part <= 20) {
            $price = intval(strval($price)[0] . '15');
        }
        else if ($round_part >= 21 && $round_part <= 37) {
            $price = intval(strval($price)[0] . '29');
        }
        else if ($round_part >= 38 && $round_part <= 56) {
            $price = intval(strval($price)[0] . '49');
        }
        else if ($round_part >= 57 && $round_part <= 70) {
            $price = intval(strval($price)[0] . '65');
        }
        else if ($round_part >= 71 && $round_part <= 87) {
            $price = intval(strval($price)[0] . '79');
        }
        else if ($round_part >= 88) {
            $price = intval(strval($price)[0] . '99');
        }
    }
    else if ($price <= 999) {
        $round_part = intval(substr(strval($price),1,3));
        $price = intval(strval($price)[0] . '95');
    }
    else if ($price <= 1995) {
        $round_part = intval(substr(strval($price),2,4));
        if ($price <= 1025) {
            $price = "995";
        }
        else if ($round_part <= 25) {
            $price = intval(strval($price)[0].strval((intval(strval($price)[1])-1)) . '95');
        }
        else if ($round_part >= 26 && $round_part <= 66) {
            $price = intval(strval($price)[0].strval($price)[1] . '49');
        }
        else if ($round_part >= 67) {
            $price = intval(strval($price)[0].strval($price)[0] . '95');
        }
    }
    else if ($price > 1995) {
        $main_part_str = substr(strval($price),0,strlen(strval($price))-2);
        $round_part = intval(substr(strval($price),-2));
        if ($round_part <= 44) {
            $price = intval(strval((intval($main_part_str)-1)) . '95');
        }
        else if ($round_part >= 45) {
            $price = intval($main_part_str . '95');
        }
    }
    return $price;
}

function yesFormatPrice($price) {
    $price = strval($price);
    if (strlen($price) > 3 ) {
        if (strlen($price) % 3 == 0) {
            $new_price = "";
            for ($i=0; $i < strlen($price); $i++) { 
               $new_price = $new_price . $price[$i];
               if ((($i+1) % 3 == 0) && ($i != strlen($price)-1)) {
                   $new_price = $new_price . " ";
               }
            }
            $price = $new_price;
        }
        else if (strlen($price) % 3 == 1) {
            $new_price = $price[0] . " ";
            for ($i=1; $i < strlen($price); $i++) { 
               $new_price = $new_price . $price[$i];
               if (($i % 3 == 0) && ($i != strlen($price)-1)) {
                   $new_price = $new_price . " ";
               }
            }
            $price = $new_price;
        }
        else if (strlen($price) % 3 == 2) {
            $new_price = $price[0] . $price[1] . " ";
            for ($i=2; $i < strlen($price); $i++) { 
               $new_price = $new_price . $price[$i];
               if ((($i-1) % 3 == 0) && ($i != strlen($price)-1)) {
                   $new_price = $new_price . " ";
               }
            }
            $price = $new_price;
        }
    }
    return $price;
}

function calculateDiamondPriceFromTotal($collectionId, $metalId, $productTypeId, $totalPrice) {

    if ($productTypeId == 1) {
        $price = (($totalPrice/1.23)-2132.51)/1.96;
    }
    else if ($productTypeId == 2) {
        $price = (($totalPrice/1.23)-2132.51)/1.96;
    }
    else if ($productTypeId == 3) {
        $price = (($totalPrice/1.23)-1272.16)/1.96;
    }
    else if ($productTypeId == 4) {
        $price = (($totalPrice/1.23)-2555.96)/1.96;
    }

    if ($price < 1) {
        $price = 1;
    }

    return intval($price);
}

function getYesProductFullName($collectionId, $metalId, $productTypeId, $diamond1Id, $diamond2Id = 0) {
    $collectionName = getYesKolekcjeById($collectionId);
    $metalName = getYesMetaleById($metalId);
    $productTypeName = getYesTypeById($productTypeId);
    $orderPrice = calculateTheTotalPrice($collectionId, $metalId, $productTypeId, $diamond1Id, $diamond2Id);
    $diamond1 = getProduct($diamond1Id);
    $diamond1Lab = formatCert(certyfikaty($diamond1['Laboratorium']));

    $diamond2text = "";
    if ($diamond2Id > 0) {
        $diamond2 = getProduct($diamond2Id);
        $diamond2Lab = formatCert(certyfikaty($diamond2['Laboratorium']));
        $diamond2text = ' '.getProductFullName($diamond2Id).' Numer certyfikatu: '.$diamond2Lab.' '.$diamond2['Symbol'];
    }

    return $productTypeName['Nazwa'].' '.$collectionName['Nazwa'].' '.$metalName['Nazwa'].' '.getProductFullName($diamond1Id).' Numer certyfikatu: '.$diamond1Lab.' '.$diamond1['Symbol'].$diamond2text.' Cena: '.$orderPrice.' PLN';
}

function getYesProductDeliveryTime($collectionId, $metalId, $productTypeId, $deliveryTime) {
    $yesProduct = getYesProduktByParams($collectionId, $metalId, $productTypeId);
    if (intval($yesProduct['CzasRealizacji']) > intval($deliveryTime)) {
        return intval($yesProduct['CzasRealizacji']);
    }
    else {
        return intval($deliveryTime);
    }
}

// function regenerateSecretTokensForAllStores() {
//     $query = "SELECT * FROM YESSALONY";
//     $counter = 0;
    
//     $result = mysql_query($query) or die("Zapytanie zako\F1czone niepowodzeniem");
//     $data = array();
//     while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
//         $data[] = $line;
//         $counter = $counter + 1;
//         $password = generatePassword($length = 32);
//         $update_query = "UPDATE YESSALONY SET TokenLogowania='$password' WHERE Id='$line[Id]' LIMIT 1";
//         mysql_query($update_query);
//     }
//     echo $counter;
// }

function bruttoToNetto($brutto, $tax) {
  return number_format($brutto/(1+$tax), 2, '.', '');
}

function generatePassword($length = 8) {
    $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}





