<nav class="navbar main-navbar" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <div class="col-xs-4">
        <ul class="nav navbar-nav full-box">
          <? if ($page_name == 'select') { ?>
            <li id="back-btn">
                <a class="btn-yes" href="#"><?= slownik(129) ?></a>
            </li>
            <li id="history-btn">
                <a class="btn-yes" href="history"><?= slownik(123) ?></a>
            </li>
          <? } else if ($page_name == 'history') { ?>
            <li id="back-btn">
                <a class="btn-yes" href="select"><?= slownik(126) ?></a>
            </li>
          <? } else if ($page_name == 'order-summary') { ?>
            <li id="back-btn">
                <a class="btn-yes" href="history"><?= slownik(129) ?></a>
            </li>
          <? } else if ($page_name == 'diamond') { ?>
            <li id="back-btn">
                <a class="btn-yes" href="diamonds?collection-id=<?= $_GET['collection-id'] ?>&metal-id=<?= $_GET['metal-id'] ?>&product-type=<?= $_GET['product-type'] ?>&reset-filters=false"><?= slownik(129) ?></a>
            </li>
          <? } else { ?>
            <li id="back-btn">
                <a class="btn-yes" href="select"><?= slownik(129) ?></a>
            </li>
          <? } ?>
        </ul>
      </div>
      <div class="col-xs-4 logo-container">
        <a href="select"><img src="assets/images/logo.png" alt=""></a>
      </div>
      <div style="font-size: 12px;" class="col-xs-4">
        <? if ($page_name == 'diamonds') { ?>
        <ul class="nav navbar-nav full-box pull-right">
          <li><button class="btn-yes" onclick="makeOrder()"><?= slownik(133) ?></button></li>
        </ul>
        <? } else { ?>
          <?= slownik(124) ?>: <?= $_SESSION['store'] ?><br />
          <?= slownik(125) ?>: <?= $_SESSION['store_address'] ?><br />
          <?= slownik(134) ?>: <?= $_SESSION['store_email'] ?>
        <? } ?>
      </div>
    </div><!--/.navbar-collapse -->
  </div>
</nav>