			<script src="assets/js/vendor/bootstrap.min.js"></script>
<? if ($page_name == 'diamonds') { ?>
			<script src="assets/js/ediamenty/ion.rangeSlider.js"></script>
			<script src="assets/js/ediamenty/jquery.slimscroll.min.js"></script>
			<script src="assets/js/ediamenty/jquery.sliderRangeStorage.js"></script>
			<script src="assets/js/ediamenty/jquery.diamond.js"></script>
			<script src="assets/js/ediamenty/jquery.bxslider.js"></script>
			<script src="assets/js/diamonds-page.js"></script>
<? } else if ($page_name == 'diamond') { ?>
			<script src="assets/js/diamonds-page.js"></script>
<? } else if ($page_name == 'select') { ?>
			<script src="assets/js/select-page.js"></script>
<? } else if ($page_name == 'order-summary') { ?>
		 	<script src='assets/js/vendor/pdfmake.min.js'></script>
		 	<script src='assets/js/vendor/vfs_fonts.js'></script>
<? } else if ($page_name == 'history') { ?>
			<script src="assets/js/history-page.js"></script>
<? } ?>
    </body>
</html>