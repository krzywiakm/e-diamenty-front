<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title>YES diamenty</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
      <link rel="stylesheet" href="assets/css/bootstrap/bootstrap-theme.min.css">
      <link rel="stylesheet" href="assets/css/ediamenty/component.css">
      <link rel="stylesheet" href="assets/css/ediamenty/grid.css">
      <link rel="stylesheet" href="assets/css/ediamenty/ion.rangeSlider.css">
      <link rel="stylesheet" href="assets/css/ediamenty/ion.rangeSlider.skinNice.css">
      <link rel="stylesheet" href="assets/css/ediamenty/jquery.bxslider.css">
      <link rel="stylesheet" href="assets/css/ediamenty/jquery.mCustomScrollbar.css">
      <link rel="stylesheet" href="assets/css/ediamenty/normalize.css">
      <link rel="stylesheet" href="assets/css/ediamenty/perfect-scrollbar.min.css">
      <link rel="stylesheet" href="assets/css/ediamenty/responsive.css">
      <link rel="stylesheet" href="assets/css/ediamenty/style.css">
      <link rel="stylesheet" href="assets/css/ediamenty/tooltipster.css">
      <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin-ext" rel="stylesheet">

      <link rel="stylesheet" href="assets/css/main.css">

      <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
      <script src="assets/js/ediamenty/jquery-2.2.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <?php include 'includes/navbar.php';?>