<? $page_name = basename(__FILE__, '.php'); ?>
<? include 'api/func.php'; ?>
<? include 'api/auth.php'; ?>
<? include 'api/yes.php'; ?>
<? $produkty = yesProdukty(); ?>
<? $kolekcje = yesKolekcje(); ?>
<? $metale = yesMetale(); ?>
<? $typy = yesTypy(); ?>
<? 
$sortuj_kolekcje = array();
foreach ($kolekcje as $key => $row)
{
    $sortuj_kolekcje[$key] = $row['Sortuj'];
}
array_multisort($sortuj_kolekcje, SORT_ASC, $kolekcje);

$sortuj_produkty = array();
foreach ($produkty as $key => $row)
{
    $sortuj_produkty[$key] = $row['IdTypu'];
}
array_multisort($sortuj_produkty, SORT_ASC, $produkty);
?>
<? include 'includes/header.php'; ?>
<style>
  #back-btn {
    display: none;
  }
</style>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-12">
        <h2 style="margin-top:40px;margin-bottom:40px;" class="section-heading"><?= slownik(126) ?></h2>
        <? foreach ($kolekcje as $kolekcja) { ?>
          <img onclick="chooseCollection('<?= $kolekcja['Id'] ?>')" class="section-img" src="<?= $kolekcja['ImgUrl'] ?>" alt="<?= $kolekcja['Nazwa'] ?>">
        <? } ?>
      </div>
    </div>
  </div>

  <div class="container page-content">
    <div class="row">
      <div class="col-xs-12">
        <h2 style="text-align:center;"><?= slownik(127) ?></h2>
        <div class="row">
          <div style="text-align: center;margin-top: 20px;" class="col-xs-12">
          <? foreach ($metale as $metal) { ?>
            <button onclick="chooseMetal('<?= $metal['Id'] ?>')" class="btn-yes open-product-images metal metal<?= $metal['Id'] ?>"><?= $metal['Nazwa'] ?></button>
          <? } ?>
          </div>
        </div>
      </div>
    </div>
    <div style="margin-top: 20px;" class="row product-images">
      <div class="col-xs-12">
        <h2 style="text-align:center;"><?= slownik(128) ?></h2>
        <div class="row">
          <div id="product-images-container" style="text-align: center;margin-top: 20px;" class="col-xs-12">
            <? foreach ($produkty as $produkt) { ?>
              <img onclick="openDiamondsList('<?= $produkt['IdTypu'] ?>')" class="clickable productImg productImg<?= $produkt['Id'] ?>" src="<?= productImgUrl($produkt['IdKolekcja'], $produkt['IdMetal'], $produkt['IdTypu']) ?>" alt="">
            <? } ?>
<!--             <img onclick="openDiamondsList(2)" class="clickable" src="assets/images/product2.png" alt="">
            <img onclick="openDiamondsList(3)" class="clickable" src="assets/images/product3.png" alt="">
            <img onclick="openDiamondsList(4)" class="clickable" src="assets/images/product4.png" alt=""> -->
          </div>
        </div>
      </div>
    </div>
  </div>
<?php include 'includes/footer.php';?>
