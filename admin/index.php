<?php

  $starttime=microtime(1);
  include "session.php";
  include "../common/vat.php";

?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2">
<link rel="stylesheet" type="text/css" href="../common/style2.css">

<link href="jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet">
<link href="jQuery-ui-Slider-Pips-1.11.0/dist/jquery-ui-slider-pips.min.css" rel="stylesheet">

<link href="../common/icon.jpg" rel="SHORTCUT ICON">

<title>
  e-diamenty - strefa administratora
</title>

<body bgcolor=white onLoad="window.setTimeout('getSecs()',1)">
<? print "
<script>
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = \"; expires=\"+date.toGMTString();
	}
	else var expires = \"\";
	document.cookie = name+\"=\"+value+expires+\"; path=/\";
}

function readCookie(name) {
	var nameEQ = name + \"=\";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,\"\",-1);
}";

if(auth())
    print "createCookie('dicsessiontime','".$_SESSION["czas"]."');";
else
    print "createCookie('dicsessiontime','".($_SESSION["czas"]-900)."');";

print "
function initStopwatch()
{
        var myTime = new Date();
        var timeNow = myTime.getTime();
        var timeDiff = timeNow/1000 - readCookie('dicsessiontime');
        this.diffSecs = timeDiff;
        return(this.diffSecs);//900-
}
function getSecs()
{
        var mySecs = initStopwatch();
        var mins = Math.floor(mySecs/60);
        var sec = Math.floor(mySecs-60*mins);
        var h = Math.floor(mins/60);
        mins = Math.floor(mins-60*h);
        
        var zero=\"\"
        if(sec<10)
            zero=\"0\"
            
        var mzero=\"\"
        if(mins<10)
            mzero=\"0\"    
        
        document.clock.face.value=h+\":\"+mzero+mins+\":\"+zero+sec
        
        if(mySecs>-5)
            window.setTimeout('getSecs()',1000);
        else
            {
            document.clock.face.value=\"wylogowany\";            
            if(prevSecs-mySecs>3)
                location.assign(\"index.php?".$_SERVER['QUERY_STRING']."\");            
            else
                location.assign(\"index.php?".$_SERVER['QUERY_STRING']."&autologout=1\");            
            }
        prevSecs=mySecs;
}

function ConfirmBox() {
    var r = confirm('NA PEWNO?');
	return r;
}
</script>";?>

<table style="border: 0px solid white;" cellspacing=0 cellpadding=0 width=100%>

<!-- naglowek -->
<tr><td>
 <a href="../" target="_blank"><img src="../common/logo1.png" align=middle border=0 height=15></a></td>
 <td></td>
 <td align="center" valign="middle"><font size=4>Strefa administratora</font>
</td></tr>
<!-- koniec -->


<!-- lewa strona -->
<tr><td width=18% height=450 valign=top>
  <?
    include "left.php";
  ?>
</td>
<!-- koniec -->

<td width=2%></td>

<!-- prawa strona -->
<td valign=top>
  <?
  $page=$_POST[page];
  if($page=="")
	$page=$_GET[page];
  if($page=="") $page="link1";
  //if(!auth()) $page="logout";
  if(!file_exists($page.".php"))
  {
     include "default.php";
  }
  else
  {
    include_once $page.".php";
    if($page=="login" && $_SERVER['QUERY_STRING'] && $_GET[page]!="logout")
        print "<script>location.assign(\"index.php?".$_SERVER['QUERY_STRING']."\");</script>";
  }
?>
</td></tr>
<!-- koniec -->

</table>


<!-- stopka -->
<?
print "<small>&copy; by Antek &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>".round(microtime(1)-$starttime,3)." sec</i></small>";
?>
<!-- koniec -->
</body>
</html>
