$(document).ready(function()
{
  $('#globalBox').on('change', 'select.categorySelect', function(event) {
	 categorySelector($(this), '');
  });
});


function categorySelector(selectOBJ, ifFinalCategoryFoundFunction)
{
  var id_category = selectOBJ.val(); //id kategorii
  if (id_category <= 0)
  {
	 selectOBJ.nextAll().remove();
	 return;
  }

  $.ajax({
	 type: "POST",
	 async: true,
	 cache: false,
	 dataType: "json",
	 url: "./class/post.php?method=getCategorySelect",
	 data: ({id_category: id_category}),
	 success: function(rq)
	 {
		if (rq && rq.success == '1')
		{
		  selectOBJ.nextAll().remove();
		  selectOBJ.parent().append(rq.html);

		  if (parseInt(rq.id_category) > 0)
		  {
			 if (ifFinalCategoryFoundFunction != '')
				window[ifFinalCategoryFoundFunction](rq.id_category);
		  }
		}
		else
		{
		  alert('Error! [JS][categorySelector]');
		}
	 },
	 error: function(XMLHttpRequest, textStatus, errorThrown) {
		alert('Error! [AJAX][categorySelector][' + textStatus + ']');
	 }
  });
}