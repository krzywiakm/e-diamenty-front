<?php
function igicert($nrcert)
{
    
    $ch = curl_init();

    // set url
    curl_setopt($ch, CURLOPT_URL, "http://global.igiworldwide.com:8082/IGIOnlineReport.asmx/ReportDetail?Printno=$nrcert");

    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $output contains the output string
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch); 
    
    $output=str_replace("\":\"","\",\"",$output);
    $output=str_replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>
<string xmlns=\"http://tecogis.com/\">","",$output);
    $output=str_replace("</string>","",$output);
    $output=str_replace("[{","",$output);
    $output=str_replace("}]","",$output);
    
    //print "<pre>";
    //print $output;
    
    $params=str_getcsv($output);
    $par=array();
    
    for($i=0; $i<count($params); $i+=2)
        {
        $par[$params[$i]]=$params[$i+1];
        }
    
    //$tabs=array("KAMIENIE","KOLORY","BARWY","CZYSTOSCI","KSZTALTY","SYMETRIE",
    //    "SZLIFY","POLEROWANIA","FLUORESCENCJE","LABORATORIA");
        
    $tabs[KAMIENIE]=$par[DESCRIPTION];
    $tabs[KSZTALTY]=$par["SHAPE AND CUT"]=="ROUND BRILLIANT"?"ROUND":$par["SHAPE AND CUT"];
    $tabs[MASA]=str_replace(" Carat","",$par["CARAT WEIGHT"]);
    $tabs[BARWY]=$par["COLOR GRADE"];
    $tabs[CZYSTOSCI]=str_replace(" ","",$par["CLARITY GRADE"]);
    $tabs[SZLIFY]=$par["CUT GRADE"];
    $tabs[POLEROWANIA]=$par[POLISH];
    $tabs[SYMETRIE]=$par[SYMMETRY];
    $tabs[FLUORESCENCJE]=$par[FLUORESCENCE];
    $tabs[LABORATORIA]="IGI";

    //    print_r($params);
    //    print_r($par);
    //    print_r($tabs);
        
   return $tabs;     

}

//print igicert("101410430");//162567601
?>
