<?php 
/**************************************************
 *                  Kursy walut                   *
 **************************************************
 * Ostatnia modyfikacja: 18.01.2013 (wersja 5.0)  *
 * Autor: Jacek Kowalski (http://jacekk.info)     *
 *                                                *
 * Strona WWW: http://jacekk.info/scripts/kursy   *
 *                                                *
 * Utwór rozprowadzany na licencji                *
 * http://creativecommons.org/licenses/by-nc/2.5/ *
 **************************************************/

/* Kodowanie znaków UTF-8 */

/****************************************************
 * UWAGA! Po każdej zmianie tego pliku, należy      *
 * usunąć plik kursy_cache.txt z głównego folderu!  *
 ****************************************************/
 
/****************************************************
 * Ostatnia modyfikacja: 14.10.2015 (antek)         *
 * Aktualizacja cen produktów wg kursu USD          *
 ****************************************************/

$vatproc=23;
$vatmnoz=1.23;

function kropek($string)
{
	return strtr($string,",",".");
}

function convert($text) {
	// Zmień na żądane kodowanie znaków - puste pozostawia UTF-8
	$charset = '';
	
	if($charset && function_exists('iconv')) {
		return iconv('utf-8', $charset, $text);
	}
	elseif($charset && function_exists('recode_string')) {
		return recode_string('utf8...'.$charset, $text);
	}
	else
	{
		return $text;
	}
}

class kursy {
	/* zawartość arkusza XML z kursami */
	private $contents = '';
	
	function __construct($url, $cache = 'kursy_cache.txt',
					$lastupdate = '12:16 -1 day', $thisupdate = '12:16') {
		// Plik z cache:
		// $cache
		// Czy dane w cache w cache aktualne?
		$recent = TRUE;
		
		// Daty ostatnich aktualizacji
		$lastupdate = strtotime($lastupdate);
		$thisupdate = strtotime($thisupdate);
		
		// Sprawdzenie możliwości zapisania kursów
		if( ( !file_exists($cache) AND !is_writable(dirname($cache)) )
			OR ( file_exists($cache) AND !(is_writable($cache)) ) ) {
			// Plik cache "nie działa"
			$cache = '';
		}
		else
		{
			// Dane są aktualne?
			if(@filemtime($cache) < $lastupdate) {
				$recent = FALSE;
			}
			elseif(time() > $thisupdate && @filemtime($cache) < $thisupdate) {
				$recent = FALSE;
			}
		}
		
		// Nie istnieje możliwość zapisu w cache lub dane są nieaktualne
		if($cache == '' OR !$recent) {
			// Link do arkusza XML
			$this->contents = file_get_contents($url);
			if($this->contents == FALSE) {
				throw new Exception('Nie udało się pobrać kursów walut.');
			}
			
			// Można zapisać do cache'a
			if($cache != '') {
				// Zapamiętujemy arkusz
				file_put_contents($cache, $this->contents);
			}
		}
		else
		{
			// Ładujemy zapisane dane
			$this->contents = file_get_contents($cache);
		}
	}
	
	function znajdz($fields) {
		if(!is_array($fields)) {
			$fields = array($fields);
		}
		
		$last = libxml_use_internal_errors(TRUE);
		$info = new SimpleXMLElement($this->contents);
		libxml_use_internal_errors($last);
		
		/* tablica wypełniana kursami */
		$rates = array(
			'numer_tabeli' => (string)$info->numer_tabeli,
			'data_publikacji' => (string)$info->data_publikacji
		);
		
		foreach($info->pozycja as $v) {
			$kod = (string)$v->kod_waluty;
			$rates[$kod] = array(
				'nazwa' => convert((string)$v->nazwa_waluty),
				'ilosc' => (string)$v->przelicznik
			);
			foreach($fields as $field) {
				$rates[$kod][$field] = (string)$v->$field;
			};
		}
		
		return $rates;
	}
}

try {
    $time0=microtime(1);
	// adres do kursów, plik do cache'owania, poprzednia aktualizacja, najbliższa aktualizacja
	$kursy = new kursy('http://nbp.pl/kursy/xml/LastA.xml', '/home/dic/ftp/logs/kursy_cache.txt', '12:16 -1 day', '12:16');
	$waluta = $kursy->znajdz(array('kurs_sredni'));
	
    $waluty = array("USD");//("CZK","EUR","CNY","GBP","USD");
    
    $log=date('Y-m-d H:i:s')." ";
    
    foreach($waluty as $w)
        {
        $myFile = "/home/dic/ftp/common/kurs$w.txt";
        $k=kropek($waluta[$w]['kurs_sredni'])/kropek($waluta[$w]['ilosc']);
  		$fh = fopen($myFile, 'w') or die("can't open file");
	    fwrite($fh, $k);
	    fclose($fh);
	    $log.="$w: $k, ";
	    $queryKURSY[$w]="INSERT INTO KURSY(Data,Waluta,Kurs) values('".date('Y-m-d')."','$w','$k')";
	    }
	
	$kursUSD=kropek($waluta["USD"]['kurs_sredni'])/kropek($waluta["USD"]['ilosc']);

	$time1=microtime(1);
	
	$line = file("/home/dic/ftp/common/users.dat");
    foreach($line as $temp)
        {
        $str = explode(",", $temp);
        $logindb = chop($str[0]);
        $passwdb = chop($str[1]);
        }
    $link = mysql_connect('sql.dic.nazwa.pl', $logindb, $passwdb)
    or die ("Nie można się połączyć: " . mysql_error());
    mysql_select_db ("dic") or die ("Nie mozna wybrać bazy danych");    
	
	$query="UPDATE PRODUKTY p, GRUPYCEN g set p.Brutto=p.NettoUSD*g.Mnoznik*$kursUSD*$vatmnoz+g.KosztDodatkowy, p.Netto=round(p.NettoUSD*g.Mnoznik*$kursUSD*$vatmnoz+g.KosztDodatkowy,2)/$vatmnoz where p.GroupId=g.Id and g.Dolar=1";
	$result=mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
	
	if($result) $log.="prodCertUpd: ".mysql_affected_rows().", ";
	    
	$query="UPDATE PRODUKTY p, GRUPYCEN g set p.Brutto=p.Masa*g.Netto*g.Mnoznik*$kursUSD*$vatmnoz+g.KosztDodatkowy, p.Netto=round(p.Masa*g.Netto*g.Mnoznik*$kursUSD*$vatmnoz+g.KosztDodatkowy,2)/$vatmnoz where p.GroupId=g.Id and g.Dolar=0";
	$result=mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");  
	
	if($result) $log.="prodMasUpd: ".mysql_affected_rows().", ";
	
	$query = "UPDATE PRODUKTY p, PRODUKTYZAM pz SET pz.Brutto=p.Brutto, pz.Netto=p.Netto where pz.Symbol=p.Symbol and IdZam in (select Id from ZAMOWIENIA where Zatwierdzone=0)";
	$result=mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");  
	
	if($result) $log.="prodZamUpd: ".mysql_affected_rows().", ";
	
	foreach($waluty as $w)
	    $result=mysql_query ($queryKURSY[$w]) or die ("Zapytanie zakonczone niepowodzeniem");  
	  
    mysql_close($link);
    
    $time2=microtime(1);
    
    print "\nKursy walut z NBP:    ".round($time1-$time0,3)." sec";
    print "\nZmiany cen produktow: ".round($time2-$time1,3)." sec";
    print "\nRazem:                ".round($time2-$time0,3)." sec";

    $log.="czas: ".round($time2-$time0,3)." sec.\n";
    
    $files = fopen("/home/dic/ftp/logs/nbp.log", "a");
    flock($files, 2);
    fputs($files, $log);
    flock($files, 3);
    fclose($files);
/*	
	echo $waluta['USD']['ilosc'].' USD: '.$waluta['USD']['kurs_sredni'].'<br />
'.$waluta['EUR']['ilosc'].' EUR: '.$waluta['EUR']['kurs_sredni'].'
<hr />
Kursy by <a href="http://jacekk.info/" target="_blank">Jacek Kowalski</a>';
*/
}
catch(Exception $e) {
	echo 'Błąd:';
	// Aby pokazać błąd, odkomentuj poniższą linię:
	 var_dump($e);
}
?>
