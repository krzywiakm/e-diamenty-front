<?php

class allegro {

  protected $_session;
  protected $_client;
  protected $_local_version;
  public $_country;
  public $_login;
  private $_password;
  private $_api_key;
  public $_api_exception;
  private $conf;
  
  const mnoznik = 1.18;

  public function __construct() {
    /* $line = file("../common/users.dat");
      foreach($line as $temp)
      {
      $str = explode(",", $temp);
      $logindb = chop($str[0]);
      $passwdb = chop($str[1]);
      } */
    //	dnmk 1INfinity1
    mysql_connect('sql.dic.nazwa.pl', "dic", "Dic123456") or die("Brak po��czenia: " . mysql_error());
    mysql_select_db("dic") or die("Brak bazy");
  }

  public function utf2iso($str) {
    return iconv("UTF-8", "ISO-8859-2", $str);
  }

  public function currtime() {
    $timeres = mysql_query("SELECT CURTIME()");
    $timeline = mysql_fetch_array($timeres);
    return $timeline[0];
  }

  public function currdate() {
    $datares = mysql_query("SELECT CURDATE()");
    $dataline = mysql_fetch_array($datares);
    return $dataline[0];
  }

  public function allCon($country_id) {
    try {
      $this->_client = new SoapClient('https://webapi.allegro.pl/service.php?wsdl');
    } catch (SoapFault $SoapException) {
      $this->_api_exception = $SoapException->{'faultstring'};
      return false;
    }
    $this->_country = (int) $country_id;
  }

  public function zdejmijZAukcji($id, $typ, $rozmiar, $zam) {
    $go = "";
    $return['status'] = "BRAK";
    if ($typ > 0)
      $go = " AND rozmiar = " . $rozmiar;
    $qr = "SELECT id, aukcja_id, sztuk FROM allegro_aukcje WHERE status = 1 AND przedmiot_id = " . $id . $go;
    $re = mysql_query($qr);

    if (mysql_num_rows($re) > 0) {
      $aukcja = mysql_fetch_row($re);


      $qq = "SELECT id FROM allegro_deals WHERE item_id = " . $aukcja[1] . " AND zam_id = " . $zam;
      $qqr = mysql_query($qq);
      if (mysql_num_rows($qqr) == 0) {
        $sztuk = $aukcja[2] - 1;
        if ($sztuk > 0) {
          try {
            $request = array(
                'sessionHandle' => $this->_session,
                'itemId' => (float) $aukcja[1],
                'newItemQuantity' => $sztuk
            );
            $this->_client->doChangeQuantityItem($request);

            $query = 'UPDATE allegro_aukcje SET sztuk = ' . $sztuk . ' WHERE id = ' . $aukcja[0];
            mysql_query($query) or die("b��d zmiany stanu aukcji");
            $return['status'] = "OK";
          } catch (SoapFault $SoapException) {
            $return['status'] = false;
            $return['error'] = $SoapException;
          }
        } else {
          try {
            print_r($aukcja[1]);
            $request = array(
                'sessionHandle' => $this->_session,
                'finishItemId' => (float) $aukcja[1],
                'finishCancelAllBids' => 0,
                'finishCancelReason' => ''
            );
            $this->_client->doFinishItem($request);

            $query = 'UPDATE allegro_aukcje SET sztuk = 0, status = 0 WHERE id = ' . $aukcja[0];
            mysql_query($query) or die("b��d zmiany stanu aukcji");

            $return['status'] = "OK";
          } catch (SoapFault $SoapException) {
            $return['status'] = false;
            $return['error'] = $SoapException;
          }
        }
      }
    }
    return $return;
  }

  public function getTheme($przedmiot, $szablon, $zamowienie, $zamile, $rozmiarek=0) {
    $result = mysql_query("SELECT text FROM allegro_konfiguracja WHERE id = " . $szablon . " and nazwa = 'theme'");
    if (!$result) {
      echo 'Nie mo�na uruchomi� zapytania: ' . mysql_error();
      exit;
    }
    $row = mysql_fetch_row($result);

    $query = "SELECT Rodzaj FROM PRODUKTY where Id=" . $przedmiot . "";
    $result = mysql_query($query) or die("Zapytanie zako�czone niepowodzeniem");
    $line = mysql_fetch_array($result, MYSQL_ASSOC);
    $r = floor($line['Rodzaj'] / 100);
    $t = $line['Rodzaj'] - $r * 100;
	

    $query = "SELECT * FROM KATTYPY where NrRodz=" . $r . " and Nr=" . $t;
    $result = mysql_query($query) or die("Zapytanie zako�czone niepowodzeniem");
    $line = mysql_fetch_array($result, MYSQL_ASSOC);
    if ($line['Title'])
      $title = $line['Title'] . " ";

    $query = "SELECT p.Id, p.Symbol, p.Masa, p.Opis, IFNULL(s_k.Pl, k.Nazwa) Kamien, IFNULL(s_ks.Pl, ks.Nazwa) Ksztalt, ks.Nazwa KsztaltOryg, IFNULL(s_ko.Pl, ko.Nazwa) Kolor, ko.Nazwa KolorOryg, IFNULL(s_b.Pl, b.Nazwa) Barwa, IFNULL(s_c.Pl, c.Nazwa) Czystosc,
            IFNULL(s_s.Pl, s.Nazwa) Szlif, IFNULL(s_sy.Pl, sy.Nazwa) Symetria, IFNULL(s_po.Pl, po.Nazwa) Polerowanie, IFNULL(s_f.Pl, f.Nazwa) Fluorescencja, 
            IFNULL(s_l.Pl, l.Nazwa) Laboratorium, z.Czas FROM PRODUKTY p
            left join KAMIENIE k on p.Kamien=k.Id left join SLOWNIK s_k on k.IdSlownik=s_k.Id
            left join KSZTALTY ks on p.Ksztalt=ks.Id left join SLOWNIK s_ks on ks.IdSlownik=s_ks.Id
            left join KOLORY ko on p.Kolor=ko.Id left join SLOWNIK s_ko on ko.IdSlownik=s_ko.Id
            left join BARWY b on p.Barwa=b.Id left join SLOWNIK s_b on b.IdSlownik=s_b.Id
            left join CZYSTOSCI c on p.Czystosc=c.Id left join SLOWNIK s_c on c.IdSlownik=s_c.Id
            left join SZLIFY s on p.Szlif=s.Id left join SLOWNIK s_s on s.IdSlownik=s_s.Id
            left join SYMETRIE sy on p.Symetria=sy.Id left join SLOWNIK s_sy on sy.IdSlownik=s_sy.Id
            left join POLEROWANIA po on p.Polerowanie=po.Id left join SLOWNIK s_po on po.IdSlownik=s_po.Id
            left join FLUORESCENCJE f on p.Fluorescencja=f.Id left join SLOWNIK s_f on f.IdSlownik=s_f.Id
            left join LABORATORIA l on p.Laboratorium=l.Id left join SLOWNIK s_l on l.IdSlownik=s_l.Id
            left join ZESTRODZ z on p.RodzajZestawienia=z.Nr
            where p.Id=" . $przedmiot;
    $result = mysql_query($query) or die("Zapytanie zako�czone niepowodzeniem");
    $line = mysql_fetch_array($result, MYSQL_ASSOC);
    $title .= $line['Symbol'];
	$productArray = $line;

    $symbol = $line['Symbol'];
    $symbolalibaba = $line['SymbolAlibaba'];
    $line[Masa]=substr($line[Masa],-1)=="0"?substr($line[Masa],0,-1):$line[Masa];
    $masa = $line['Masa'];


    for ($i = 1; $i <= 6; $i++)
      $opis[$i] = opis2($line['Id'], "PL", 0, $line['Pytac'], $line['Rozmiar'], $line['DataProm'], $i);




    $foto = "";
    $id = $line['Id'];

    $urlscount = 0;
    $ytcount = 0;
    $galcount = 0;
    if (file_exists("../foto/" . $line[KsztaltOryg] . $line[KolorOryg] . ".jpg")) {
      $urls[0] = "../foto/" . $line[KsztaltOryg] . $line[KolorOryg] . ".jpg";
      $urlstype[0] = 0;
      $urlscount++;
    }

    $queryGal = "SELECT * from GALERIA where IdProd=" . $id . " order by Id";
    $resultGal = mysql_query($queryGal) or die("Zapytanie zako�czone niepowodzeniem");
    while ($lineGal = mysql_fetch_array($resultGal, MYSQL_ASSOC)) {
      //if(substr($lineGal[Url],0,3)=="../")
      //  $lineGal[Url]=substr($lineGal[Url],3);

      if ($lineGal[Typ] != 0 || file_exists($lineGal[Url])) {
        $urlstype[$urlscount] = $lineGal[Typ];
        if ($lineGal[Typ] == 1) {
          //if(!$ytcount)
          //    $urlstype[$urlscount]=11;
          $ytcount++;
        } else {
          //if(!$galcount)
          //    $urlstype[$urlscount]=10;
          $galcount++;
        }

        $urls[$urlscount++] = $lineGal[Typ] == 1 ? "http://www.youtube.com/v/" . $lineGal[Url] . "&autoplay=1&loop=1&fs=0&rel=0&showinfo=0" : $lineGal[Url];
      }
    }

    for ($j = 0; $j < $urlscount; $j++) {
      $rozm = "";
      //if ($j) {
        $mysock = getimagesize($urls[$j]);
        if ($mysock[0] > $mysock[1])
          $rozm = " width=250 vspace=" . round(($mysock[0] - $mysock[1]) / ($mysock[0] / 250) / 2);
        else
          $rozm = " height=250 hspace=" . round(($mysock[1] - $mysock[0]) / ($mysock[1] / 250) / 2);
      //}
      if ($urlstype[$j] == 0)
        $foto .= "<a href=\"http://dic.nazwa.pl/" . substr($urls[$j], 3) . "\" target=\"_blank\"><img src=\"http://dic.nazwa.pl/" . substr($urls[$j], 3) . "\"$rozm/></a>";
      if (!($j % 3))
        $foto .= "<br>";
    }
/*   
    if ($zamowienie == 0)
      $zam = "<p><span class='style4'><strong>DOST�PNY NA MAGAZYNIE</strong><p/><p>Termin realizacji oko�o 24h</p>";
    else if ($zamowienie == 1)
      $zam = "<p><span class='style4'><strong>DOST�PNY NA ZAM�WIENIE</strong><p/><p>Termin realizacji oko�o 1 tygodnia</p>";
    else if ($zamowienie == 4)
      $zam = "<p><span class='style4'><strong>DOST�PNY NA ZAM�WIENIE</strong><p/><p>Termin realizacji oko�o 4-5 tygodni</p>";
    else
      $zam = "<p><span class='style4'><strong>DOST�PNY NA ZAM�WIENIE</strong><p/><p>Termin realizacji oko�o " . $zamowienie . " tygodni</p>";
*/

    if (file_exists("../Galeria/gif/" . $line['SymbolYes2'] . ".gif"))
      $gif = '<a href="http://www.staviori.biz/orbitvu.php?fld=' . $line['SymbolYes2'] . '&width=466&height=460" target="_blank" rel="nofollow">
   <img src="http://www.staviori.biz/Galeria/gif/' . $line['SymbolYes2'] . '.gif" width="300" height="200" /></a>';
    else
      $gif = "";


    if ($opis[3] == null)
      $opis[3] = "brak";
      
  //    Kamie�: [KAMIEN] Kszta�t szlifu: Masa Barwa Czystosc Szlif Symetria Polerowanie Fluorescencja Certyfikat
//Dla masowych
//Kamie�: Kszta�t szlifu: Masa Barwa Czystosc Szlif Certyfikat: e-diamenty
//$fields=array("Symbol", "GroupId", "NettoUSD", "Netto", "Brutto", "RodzajZestawienia", "Masa", "Kamien", "Kolor", "Barwa", "Czystosc", "Ksztalt", "Symetria", "Szlif", "Polerowanie", "Fluorescencja", "Laboratorium", "Tymczasowy", "Pokaz");
    //$co = array("[TITLE]", "[SYMBOL]", "[SYMBOLALIBABA]", "[MASA]", "[MARKA]", "[NAZWA]", "[KAMIEN]", "[METAL]", "[WYMIAR]", "[OPIS]", "[OPIS2]", "[FOTO]", "[GIF]", "[ZAMOWIENIE]", "[ROZMIAR]");
    //$na = array($title, $symbol, $symbolalibaba, $masa, $opis[1], $opis[2], $opis[3], $opis[4], $opis[5], $opis[6], $productArray['Opis'], $foto, $gif, $zam, $rozmiarek);
    
    $urllab="";
    $buttoncert="";
    $btc="<img src='http://dic.nazwa.pl/Allegro/buttoncert.png'>";
    switch($line[Laboratorium])
        {
        case "IGI":
            $urllab="<a href=http://www.igiworldwide.com/verify.php?r=".$line[Symbol]." target=_blank>IGI</a>";
            $buttoncert="<a href=http://www.igiworldwide.com/verify.php?r=".$line[Symbol]." target=_blank>$btc</a>";
            break;
        case "GIA":
            $urllab="<a href=http://www.gia.edu/cs/Satellite?pagename=GST%2FDispatcher&childpagename=GIA%2FPage%2FReportCheck&c=Page&cid=1355954554547&reportno=".$line[Symbol]." target=_blank>GIA</a>";
            $buttoncert="<a href=http://www.gia.edu/cs/Satellite?pagename=GST%2FDispatcher&childpagename=GIA%2FPage%2FReportCheck&c=Page&cid=1355954554547&reportno=".$line[Symbol]." target=_blank>$btc</a>";
            break;
        case "HRD":
            $urllab="<a href=https://my.hrdantwerp.com?id=34&record_number=".$line[Symbol]." target=_blank>HRD</a>";
            $buttoncert="<a href=https://my.hrdantwerp.com?id=34&record_number=".$line[Symbol]." target=_blank>$btc</a>";
            /*
            $urllab2='<form action="https://my.hrdantwerp.com" type="post" id="findCertificate">	
						<input type="hidden" name="id" value="34">		
							<input type=hidden name="record_number" type="text" value="'.$line[Symbol].'" />
							<input value=hrd type="submit">
							<input type="hidden" name="L" value=""/>							
				</form>';
			*/
            break;
        }
        
    $zaliczka="";     
    if($zamile)   
        $zaliczka="Kamie� sprowadzany na indywidualne zam�wienie klienta. W celu realizacji zam�wienia konieczna jest przedp�ata 10% warto�ci towaru. Przedp�ata ta stanowi zadatek w rozumieniu przepis�w kodeksu cywilnego i nie podlega zwrotowi je�li klient zrezygnuje z zakupu.";
        
    $barwakolor="Barwa";
    if(!$line[Barwa])
        {
        $barwakolor="Kolor";
        $line[Barwa]=$line[Kolor];
        }

    $co = array("[KAMIEN]", "[KSZTALT]", "[MASA]", "[BARWA]", "[CZYSTOSC]", "[SZLIF]", "[SYMETRIA]", "[POLEROWANIE]", "[FLUORESCENCJA]", "[CERTYFIKAT]", "[OPIS]", "[CZAS]", "[FOTO]", "[GIF]", "[BUTTONCERT]", "[ZALICZKA]", "[BARWA-KOLOR]", "[NRCERTYFIKATU]");
    $na = array($line[Kamien], $line[Ksztalt], $masa, $line[Barwa], $line[Czystosc], $line[Szlif], $line[Symetria], $line[Polerowanie], $line[Fluorescencja], $urllab, $line[Opis], $zamowienie, $foto, $gif, $buttoncert, $zaliczka, $barwakolor, $line[Symbol]);
    
    foreach($na as $k => $v)
        if(!$v)
            $na[$k]="&nbsp;";
    
    $template = str_replace($co, $na, $row[0]);
    //$template = $row[0];
    return $template;
  }

  public function Login($login, $passwd, $api) {
    $this->_login = $login;
    $this->_password = hash('sha256', $passwd, true);
    $this->_password = base64_encode($this->_password);
    $this->_api_key = $api;

    $request = array(
        'sysvar' => 3,
        'countryId' => $this->_country,
        'webapiKey' => $this->_api_key
    );

    $version = $this->_client->doQuerySysStatus($request);

    if ($version === false || !isset($this->_country))
      return false;
    $this->_local_version = $version->verKey;

    $request = array(
        'userLogin' => $this->_login,
        'userHashPassword' => $this->_password,
        'countryCode' => $this->_country,
        'webapiKey' => $this->_api_key,
        'localVersion' => $this->_local_version
    );

    $this->_session = $this->_client->doLoginEnc($request)->sessionHandlePart;
  }

  public function checkCategory() {
    $query = 'SELECT nazwa, wartosc FROM `allegro_konfiguracja`';
    $result = mysql_query($query) or die("Pobranie kategorii B��D");

    while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
      $this->conf[$line['nazwa']] = $line['wartosc'];

    $ver = $this->getCatVer();

    if ($this->conf['categoryVersion'] != $ver) {
      $this->aktualizaujKategorie();
      $query = 'UPDATE allegro_konfiguracja SET wartosc = "' . $ver . '" WHERE nazwa = "categoryVersion"';
      $result = mysql_query($query) or die("Pobranie kategorii B��D");
      return "Aktualizacja kategorii przebieg�a pomy�lnie";
    }
  }

  public function getCatVer() {
    $request = array(
        'localVersion' => 0,
        'countryId' => 1,
        'webapiKey' => $this->_api_key
    );

    $cat = $this->_client->doGetCatsDataCount($request);
    return $cat->verStr;
  }

  public function getCategoryNames($id) {
    $request = array(
        'sessionId' => $this->_session,
        'categoryId' => $id
    );

    $cats = $this->_client->doGetCategoryPath($request);
    $arr = array();
    foreach ($cats->categoryPath->item as $cat) {
      $arr[] = utf2iso($cat->catName);
    }
    $arr = implode(" >> ", $arr);
    return $arr;
  }

  public function margin($level, $str = '+') {
    $margin = "";
    for ($i = 0; $i < $level; $i++) {
      $margin .= $str;
    }
    return $margin;
  }

  public function doGetShopCatsDataSELECT() {
    $res = $this->getShopCategories();
    $res = $this->doGetShopCatsDataTREE($res->shopCatsList->item);
    $res = $this->doGetShopCatsDataOPITONS($res);
    $category_html = '<input type="hidden" name="page" value="allegroitc">'
            . '<input type="hidden" name="id" value="' . $_GET['id'] . '">'
            . '<select name="catid">' . $res . '</select>'
            . '<input type="submit" value="OK">';
    return $category_html;
  }

  public function doGetShopCatsDataOPITONS($res, $level = 0) {
    $string = "";
    foreach ($res as $cat) {
      if (isset($_GET['catid']) && $cat['id'] == $_GET['catid'])
        $string .= '<option selected value="' . $cat['id'] . '">' . $this->margin($level) . utf2iso($cat['name']) . '</option>';
      else
        $string .= '<option value="' . $cat['id'] . '">' . $this->margin($level) . utf2iso($cat['name']) . '</option>';

      if (isset($cat['childs']))
        $string .= $this->doGetShopCatsDataOPITONS($cat['childs'], $level + 1);
    }
    return $string;
  }

  public function doGetShopCatsDataTREE($res, $parent_id = 0) {
    $cats = array();

    foreach ($res as $cat) {
      if ($cat->catParent == $parent_id) {
        $cats[$cat->catId] = array(
            'id' => $cat->catId,
            'name' => $cat->catName,
            'childs' => $this->doGetShopCatsDataTREE($res, $cat->catId)
        );
        if (count($cats[$cat->catId]['childs']) == 0)
          unset($cats[$cat->catId]['childs']);
      }
    }
    return $cats;
  }

  public function getShopCategories() {
    $request = array(
        'sessionHandle' => $this->_session
    );
    $cats = $this->_client->doGetShopCatsData($request);
    return $cats;
  }

  public function aktualizaujKategorie() {
    $query = 'DELETE FROM `allegro_categories`';
    $result = mysql_query($query) or die("Nie mo�na usun�� kategorii");
    $request = array(
        'countryId' => $this->_country,
        'localVersion' => 0,
        'webapiKey' => $this->_api_key
    );
    $kategorie = $this->_client->doGetCatsData($request);
    $query = 'INSERT INTO allegro_categories (`id_category`, `name`, `id_parent`, `country_id`, `position`) VALUES ';

    foreach ($kategorie->catsList->item as $kat) {
      $query .= '(' . (int) $kat->catId . ', 
                "' . utf2iso(htmlspecialchars($kat->catName)) . '", 
                ' . (int) $kat->catParent . ', 
                ' . (int) $this->_country . ', 
                ' . (int) $kat->catPosition . '
            ),';
    }
    $query = rtrim($query, ',');
    $query .= ';';

    $result = mysql_query($query) or die("Nie mo�na doda� nowych kategorii: " . mysql_error());
  }

  public function getServiceParentCategories($id_category, $country_id) {
    $query = 'SELECT * FROM `allegro_categories` WHERE `id_parent` = ' . (int) $id_category . ' AND `country_id` = ' . (int) $country_id . ' ORDER BY `position`';
    $result = mysql_query($query) or die("Pobranie kategorii B��D");

    while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
      $res[] = $line;

    return $res;
  }

  public function getFormFields($country_id, $cat) {
    $request = array(
        'countryId' => $this->_country,
        'categoryId' => $cat,
        'webapiKey' => $this->_api_key
    );
    return $this->_client->doGetSellFormFieldsForCategory($request);
  }

  public function getAllFids() {
    $request = array(
        'countryCode' => $this->_country,
        'localVersion' => 0,
        'webapiKey' => $this->_api_key
    );
    return $this->_client->doGetSellFormFieldsExt($request);
  }

  public function getProd($nr_kat, $zapytanie) {
    $tab = array();
    foreach ($nr_kat as $kat) {
      $tab[] = "sp.Skladnik = $kat";
    }
    $nr_kat = implode(" OR ", $tab);

    if ($zapytanie == 1)
      $query = 'SELECT apt.tytul, aa.aukcja_id, aa.status, aa.wystaw_pono, p.id, p.symbolyes, p.yesstanph, p.yesstandetal, p.yesstancentrala ,p.symbol, p.brutto, p.rozmiar, p.masa, p.czterytygodnie, p.rodzaj, sum(t.ilosc) as ile, t.rozmiar as rozm FROM PRODUKTY p '
              . 'LEFT JOIN TOWAR t ON p.Id = t.IdProd '
              . 'LEFT JOIN allegro_aukcje aa ON (aa.przedmiot_id = t.IdProd OR aa.przedmiot_id = p.id) AND (aa.rozmiar = t.rozmiar OR (aa.rozmiar = 0 AND p.rozmiar IS NULL)) AND aa.status = 1 '
              . 'LEFT JOIN SKLADPROD sp ON sp.IdProd = p.Id '
              . 'LEFT JOIN allegro_prod_tyt apt ON (apt.prod_id = t.IdProd OR apt.prod_id = p.id) AND (apt.rozmiar = t.rozmiar OR (apt.rozmiar = 0 AND p.rozmiar IS NULL)) '
              . 'WHERE sp.Typ = 2 AND (' . $nr_kat . ') '
              . 'GROUP BY p.Id, t.Rozmiar ORDER BY p.id';
    else if ($zapytanie == 2)
      $query = 'SELECT apt.tytul, aa.aukcja_id, aa.status, aa.wystaw_pono, pp.opis, pp.id as stareid, p.id, p.symbolyes, p.yesstanph, p.yesstandetal, p.yesstancentrala ,p.symbol, p.brutto, p.rozmiar as rozm, p.masa, p.czterytygodnie, p.rodzaj, sum(t.ilosc) as ile FROM PRODUKTY p '
              . 'LEFT JOIN PRODUKTY pp ON pp.Symbol = p.symbol '
              . 'LEFT JOIN TOWAR t ON p.Id = t.IdProd '
              . 'LEFT JOIN allegro_aukcje aa ON aa.przedmiot_id = p.Id AND (aa.rozmiar = p.rozmiar OR ( aa.rozmiar = 0 AND p.rozmiar IS NOT NULL ) OR ( aa.rozmiar = 0 AND p.rozmiar IS NULL )) AND aa.status = 1 '
              . 'LEFT JOIN SKLADPROD sp ON sp.IdProd = pp.Id '
              . 'LEFT JOIN allegro_prod_tyt apt ON apt.prod_id = pp.Id AND (apt.rozmiar = pp.rozmiar OR ( apt.rozmiar = 0 AND pp.rozmiar IS NOT NULL ) OR ( apt.rozmiar = 0 AND pp.rozmiar IS NULL )) AND aa.status = 1 '
              . 'WHERE sp.Typ = 2 AND (' . $nr_kat . ') '
              . 'GROUP BY p.Id, p.rozmiar ORDER BY pp.id';
    else if ($zapytanie == 3)
      $query = 'SELECT p.id, p.symbolyes, p.yesstanph, p.yesstandetal, p.yesstancentrala ,p.symbol, p.brutto, p.rozmiar, p.masa, p.czterytygodnie, p.rodzaj, t.rozmiar as rozm FROM PRODUKTY p '
              . 'LEFT JOIN TOWAR t ON p.Id = t.IdProd '
              . 'LEFT JOIN SKLADPROD sp ON sp.IdProd = p.Id '
              . ''
              . ''
              . 'WHERE sp.Typ = 2 AND (' . $nr_kat . ') '
              . 'GROUP BY p.Id, t.Rozmiar ORDER BY p.Id';
    else if ($zapytanie == 4)
      $query = 'SELECT apt.tytul, aa.aukcja_id, aa.status, aa.wystaw_pono, p.id, p.symbolyes, p.yesstanph, p.yesstandetal, p.yesstancentrala ,p.symbol, p.brutto, p.rozmiar, p.masa, p.czterytygodnie, p.rodzaj, sum(t.ilosc) as ile, t.rozmiar as rozm, k.Nazwa Kamien, IFNULL(s_ks.Pl, ks.Nazwa) Ksztalt, ks.Nazwa KsztaltOryg, IFNULL(s_ko.Pl, ko.Nazwa) Kolor, ko.Nazwa KolorOryg, b.Nazwa Barwa, c.Nazwa Czystosc,
            s.Nazwa Szlif, sy.Nazwa Symetria, po.Nazwa Polerowanie, f.Nazwa Fluorescencja, 
            l.Nazwa Laboratorium, z.Czas FROM PRODUKTY p '
              . 'LEFT JOIN TOWAR t ON p.Id = t.IdProd '
              . 'LEFT JOIN allegro_aukcje aa ON (aa.przedmiot_id = t.IdProd OR aa.przedmiot_id = p.id) AND (aa.rozmiar = t.rozmiar OR (aa.rozmiar = 0 AND p.rozmiar IS NULL)) AND aa.status = 1 '
              //. 'LEFT JOIN SKLADPROD sp ON sp.IdProd = p.Id '
              . 'LEFT JOIN allegro_prod_tyt apt ON (apt.prod_id = t.IdProd OR apt.prod_id = p.id) AND (apt.rozmiar = t.rozmiar OR (apt.rozmiar = 0 AND p.rozmiar IS NULL)) '
              .'left join KAMIENIE k on p.Kamien=k.Id
                left join KSZTALTY ks on p.Ksztalt=ks.Id left join SLOWNIK s_ks on ks.IdSlownik=s_ks.Id
                left join KOLORY ko on p.Kolor=ko.Id left join SLOWNIK s_ko on ko.IdSlownik=s_ko.Id
                left join BARWY b on p.Barwa=b.Id
                left join CZYSTOSCI c on p.Czystosc=c.Id
                left join SZLIFY s on p.Szlif=s.Id
                left join SYMETRIE sy on p.Symetria=sy.Id
                left join POLEROWANIA po on p.Polerowanie=po.Id
                left join FLUORESCENCJE f on p.Fluorescencja=f.Id
                left join LABORATORIA l on p.Laboratorium=l.Id 
                left join ZESTRODZ z on p.RodzajZestawienia=z.Nr '
              //. 'WHERE sp.Typ = 2 AND (' . $nr_kat . ') '
              . 'GROUP BY p.Id, t.Rozmiar ORDER BY p.id';
              
    /* else if($zapytanie == 4)
      $query = 'SELECT aa.aukcja_id, aa.status, aa.wystaw_pono, p.id, p.symbolyes, p.yesstanph, p.yesstandetal, p.yesstancentrala ,p.symbol, p.brutto, p.rozmiar, p.masa, p.czterytygodnie, p.rodzaj, sum(t.ilosc) as ile, t.rozmiar as rozm FROM PRODUKTY p '
      .'LEFT JOIN TOWAR t ON p.Id = t.IdProd '
      .'LEFT JOIN allegro_aukcje aa ON (aa.przedmiot_id = t.IdProd OR aa.przedmiot_id = p.id) AND (aa.rozmiar = t.rozmiar OR (aa.rozmiar = 0 AND p.rozmiar IS NULL)) AND aa.status = 1 '
      .'LEFT JOIN SKLADPROD sp ON sp.IdProd = p.Id WHERE sp.Typ = 2 AND ('.$nr_kat.') '
      .'GROUP BY p.Id, t.Id ORDER BY p.id'; */

	//echo $query;
    $result = mysql_query($query) or die("Pobranie kategorii B��D_" . mysql_error());

    while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
      {
      $line[masa]=substr($line[masa],-1)=="0"?substr($line[masa],0,-1):$line[masa];
      $line[brutto] = round(self::mnoznik*$line[brutto],2);
      $res[] = $line;
      }

    return $res;
  }

  public function aktualizujTytulAukcjiWDB($przedmiot, $rozmiar, $tytul) {
    $qr = 'SELECT id FROM allegro_prod_tyt WHERE prod_id = ' . $przedmiot . ' and rozmiar = ' . $rozmiar . ' LIMIT 1';
    $result = mysql_query($qr);
    if (!mysql_num_rows($result))
      $query = "INSERT INTO allegro_prod_tyt (`prod_id`, `rozmiar`, `tytul`) VALUES ($przedmiot, $rozmiar, '$tytul')";
    else
      $query = 'UPDATE allegro_prod_tyt SET tytul = "' . $tytul . '" WHERE prod_id = ' . $przedmiot . ' and rozmiar = ' . $rozmiar;

    mysql_query($query) or die(mysql_error());
  }

  public function getTest() {
    $query = 'SELECT przedmiot_id, rozmiar, tytul FROM `allegro_aukcje` where tytul != "" ORDER BY id';
    $result = mysql_query($query) or die("Pobranie KATTYPY B��D");


    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
      $res[$line['przedmiot_id']][$line['rozmiar']] = $line['tytul'];
    }


    foreach ($res as $przedmiot_id => $next) {
      foreach ($next as $rozmiar => $tytul) {
        $this->aktualizujTytulAukcjiWDB($przedmiot_id, $rozmiar, $tytul);
      }
    }

    /*
      $query = 'SELECT distinct IdProd from SKLADPROD WHERE Typ = 2 AND Skladnik = 34';
      $result = mysql_query($query) or die("Pobranie kategorii B��D");

      while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
      $res[] = $line;

      return $res; */
  }

  public function getMag() {
    $query = 'SELECT p.id, p.symbol, p.brutto, p.rozmiar, p.masa, p.rodzaj, sum(t.ilosc) as ile FROM PRODUKTY p LEFT JOIN TOWAR t ON p.Id = t.IdProd WHERE t.ilosc > 0 GROUP BY t.IDProd ORDER BY p.id';
    $result = mysql_query($query) or die("Pobranie kategorii B��D");

    while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
      {
      $line[masa]=substr($line[masa],-1)=="0"?substr($line[masa],0,-1):$line[masa];
      $line[brutto] = round(self::mnoznik*$line[brutto],2);
      $res[] = $line;
      }

    return $res;
  }

  public function getSkladniki() {
    $query = 'SELECT idprod, skladnik, kolor FROM `SKLADPROD`';
    $result = mysql_query($query) or die("Pobranie kategorii B��D");

    $res = array();

    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
      $res[$line['idprod']][] = $line['skladnik'];
      if ($line['skladnik'] == 19)
        $res[$line['idprod']]['kolor'] = $line['kolor'];
    }
    return $res;
  }

  public function getKatTypy() {
    $res = array();
    $query = 'SELECT NrRodz, Nr, title FROM `KATTYPY`';
    $result = mysql_query($query) or die("Pobranie KATTYPY B��D");

    while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
      $res[$line['NrRodz']][$line['Nr']] = $line['title'];

    return $res;
  }

  //public function wystawAukcjeTest($tablica, $id_przedmiotu, $zamowienie, $szablon, $rozmiarek)
  public function wystawAukcjeTest($tablica, $def) {

    $result = mysql_query("SELECT id FROM allegro_aukcje WHERE przedmiot_id = " . $def['realItemId'] . " AND rozmiar = " . $def['rozmiar'] . " AND status = 1 LIMIT 1");
    $num_rows = mysql_num_rows($result);

    if ($num_rows > 0)
      return false;

    //if($def['sztuk'] > 10)
    //  $def['sztuk'] = 10;

    $tablica[24]['string'] = $this->getTheme($def['przedmiotid'], $def['szablon'], $def['zamiletxt'], $def['zamile'], $def['rozmiar']);
    $tablica[2]['int'] = $def['allcatid']; //kategoria allegre
    $tablica[4]['int'] = 5; //na ile dni
    $tablica[5]['int'] = $def['sztuk']; //ile sztuk
    $tablica[8]['float'] = $def['cena']; //cena
    $tablica[9]['int'] = 1; //kraj
    $tablica[10]['int'] = 15; //wojewodztwo
    $tablica[11]['string'] = "Pozna�";
    $tablica[13]['int'] = 48; //48 - dodatkowe informacje i zagraniaca, 32- zagranica 
    $tablica[14]['int'] = 33; //faktury i przelew
    $tablica[15]['int'] = 2; // miniaturka

    if ($def['zamile'] == 0) {
      $tablica[340]['int'] = 24;
      $tablica[27]['string'] = $def['zamiletxt'];
   /* } else if ($def['zamile'] == 1) {
      $tablica[340]['int'] = 240;
      $tablica[27]['string'] = $def['zamiletxt'];*/
    } else {
      $tablica[340]['int'] = $def['zamile']*24;
      $tablica[27]['string'] = $def['zamiletxt'];
    }

    if (file_exists("../foto/" . $def['ksztaltoryg'] . $def['kolororyg'] . ".jpg")) {
      $image = file_get_contents("../foto/" . $def['ksztaltoryg'] . $def['kolororyg'] . ".jpg");
      $tablica[16]['image'] = $image;
    }

    if (0 && $tablica[8]['float'] < 200) // 0 && -> zablokowany if
    {
        /*$tablica[36]['float'] = 8.00; //paczka pocztowa ekonomiczna - pierwsza sztuka
        $tablica[38]['float'] = 10.00; //paczka pocztowa priorytetowa - pierwsza sztuka
        $tablica[136]['float'] = 8.00; //paczka pocztowa ekonomiczna - kolejna sztuka
        $tablica[138]['float'] = 10.00; //paczka pocztowa priorytetowa - kolejna sztuka
        $tablica[236]['int'] = 3; //paczka pocztowa ekonomiczna - pierwsza sztuka
        $tablica[238]['int'] = 3; //paczka pocztowa priorytetowa - pierwsza sztuka
*/
        //platnosc z gory
        //Paczka 24  //15.00 0.00 10
        
        $tablica[55]['float'] = 15.00;
        $tablica[155]['float'] = 0.00;
        $tablica[255]['int'] = 10;
        
        //Odbior osobisty po przedplacie //0.00 0.00 -
        //$tablica[35][] = 4;

        //Odbior w punkcie po przedplacie //0.00 0.00 -
        //Paczka 24 odbior w punkcie //15.00 0.00 10
        /*
        $tablica[57]['float'] = 15.00;
        $tablica[157]['float'] = 0.00;
        $tablica[257]['int'] = 10;
        */
        //platnosc przy odbiorze
        //Przesylka pobraniowa priorytetowa / Paczka24 pobranie  //20.00 0.00 10
        $tablica[42]['float'] = 20.00;
        $tablica[142]['float'] = 0.00;
        $tablica[242]['int'] = 10;
        //Odbior osobisty //0.00 0.00 -
        //Odbior w punkcie //0.00 0.00 -
        $tablica[35][] = 1;
        //Paczka 24 odbior w punkcie //20.00 0.00 10
        $tablica[58]['float'] = 20.00;
        $tablica[158]['float'] = 0.00;
        $tablica[258]['int'] = 10;

    } 
    
    if($tablica[8]['float'] <= 5000)
    {
        /*$tablica[36]['float'] = 0.00; //paczka pocztowa ekonomiczna - pierwsza sztuka
        $tablica[38]['float'] = 0.00; //paczka pocztowa priorytetowa - pierwsza sztuka
        $tablica[136]['float'] = 0.00; //paczka pocztowa ekonomiczna - kolejna sztuka
        $tablica[138]['float'] = 0.00; //paczka pocztowa priorytetowa - kolejna sztuka
        $tablica[236]['int'] = 3; //paczka pocztowa ekonomiczna - pierwsza sztuka
        $tablica[238]['int'] = 3; //paczka pocztowa priorytetowa - pierwsza sztuka
*/

        //platnosc z gory
        //Przesylka kurierska  //0.00 0.00 10
        $tablica[44]['float'] = 14.00;
        $tablica[144]['float'] = 0.00;
        $tablica[244]['int'] = 10;
        //Odbior osobisty po przedplacie //0.00 0.00 -
        //Odbior w punkcie po przedplacie //0.00 0.00 -
        $tablica[35][] = 4;
        //Paczka 24 odbior w punkcie //0.00 0.00 10
        /*
        $tablica[57]['float'] = 0.00;
        $tablica[157]['float'] = 0.00;
        $tablica[257]['int'] = 10;
*/
        //platnosc przy odbiorze
        //Przesylka kurierska pobraniowa  //20.00 0.00 10
        $tablica[45]['float'] = 18.00;
        $tablica[145]['float'] = 0.00;
        $tablica[245]['int'] = 10;
        //Odbior osobisty //0.00 0.00 -
        //Odbior w punkcie //0.00 0.00 -
        //$tablica[35][] = 1;
        //Paczka 24 odbior w punkcie //20.00 0.00 10
        /*
        $tablica[58]['float'] = 20.00;
        $tablica[158]['float'] = 0.00;
        $tablica[258]['int'] = 10;
        */
    }
    else
    {
        /*$tablica[36]['float'] = 0.00; //paczka pocztowa ekonomiczna - pierwsza sztuka
        $tablica[38]['float'] = 0.00; //paczka pocztowa priorytetowa - pierwsza sztuka
        $tablica[136]['float'] = 0.00; //paczka pocztowa ekonomiczna - kolejna sztuka
        $tablica[138]['float'] = 0.00; //paczka pocztowa priorytetowa - kolejna sztuka
        $tablica[236]['int'] = 3; //paczka pocztowa ekonomiczna - pierwsza sztuka
        $tablica[238]['int'] = 3; //paczka pocztowa priorytetowa - pierwsza sztuka
*/

        //platnosc z gory
        //Przesylka kurierska  //0.00 0.00 10
        $tablica[44]['float'] = 0.00;
        $tablica[144]['float'] = 0.00;
        $tablica[244]['int'] = 10;
        //Odbior osobisty po przedplacie //0.00 0.00 -
        //Odbior w punkcie po przedplacie //0.00 0.00 -
        $tablica[35][] = 4;
        //Paczka 24 odbior w punkcie //0.00 0.00 10
        /*
        $tablica[57]['float'] = 0.00;
        $tablica[157]['float'] = 0.00;
        $tablica[257]['int'] = 10;
*/
        //platnosc przy odbiorze
        //Przesylka kurierska pobraniowa  //20.00 0.00 10
        //$tablica[45]['float'] = 18.00;
        //$tablica[145]['float'] = 0.00;
        //$tablica[245]['int'] = 10;
        //Odbior osobisty //0.00 0.00 -
        //Odbior w punkcie //0.00 0.00 -
        //$tablica[35][] = 1;
        //Paczka 24 odbior w punkcie //20.00 0.00 10
        /*
        $tablica[58]['float'] = 20.00;
        $tablica[158]['float'] = 0.00;
        $tablica[258]['int'] = 10;
        */
    }

    $tablica[28]['int'] = 0; //Sztuki / Komplety / Pary
    $tablica[29]['int'] = 1; //format sprzedazy 0-kup teraz, 1-sklep
    //$tablica[31]['int'] = $def['catid']; //kategoria w sklepie
    $tablica[32]['string'] = "60-179";
    $tablica[33]['string'] = "15 1090 1362 0000 0001 3161 4944";//"95 1090 1362 0000 0001 2356 0079";
    $tablica[35]['int'] = 5; //odbiór osobisty przed i po wp³acie, darmowa opcja przsy³ki
    //$tablica[24452]['int'] = 1; // dostawa z polski

    $query = "SELECT max(id) from allegro_aukcje";
    $result = mysql_query($query) or die("Zapytanie zako�czone niepowodzeniem");
    $row = mysql_fetch_row($result);
    $localid = $row[0] + 1;


    $opcje = self::data2FID($tablica);
    $request = array(
        'sessionHandle' => $this->_session,
        'fields' => $opcje,
        'localId' => $localid,
        'afterSalesServiceConditions' => [
                'impliedWarranty' => '4cf00862-b20d-4f10-8f1e-d8fe67993a58',
                'returnPolicy'    => '22848381-e2b5-4eff-b82e-8b00bca9bfc6',
                'warranty'        => 'fc6a6e87-5e69-4976-b3f1-39948dc7ed69'
            ],
    );
    /*
    $request = array(
            'sessionHandle'               => $this->_session,
            'fields'                      => $opcje,
            'localId'                     => $localid,
            'afterSalesServiceConditions' => [
                'impliedWarranty' => '4cf00862-b20d-4f10-8f1e-d8fe67993a58',
                'returnPolicy'    => '22848381-e2b5-4eff-b82e-8b00bca9bfc6',
                'warranty'        => 'fc6a6e87-5e69-4976-b3f1-39948dc7ed69',
            ],
        );
        https://allegro.pl/afterSalesServiceConditions/impliedWarranty/update/4cf00862-b20d-4f10-8f1e-d8fe67993a58
        https://allegro.pl/afterSalesServiceConditions/returnPolicy/update/22848381-e2b5-4eff-b82e-8b00bca9bfc6
        https://allegro.pl/afterSalesServiceConditions/warranty/update/fc6a6e87-5e69-4976-b3f1-39948dc7ed69
    */
    /*
      $opcje = self::data2FID($tablica);
      $request = array(
      'sessionHandle' => $this->_session,
      'fields'        => $opcje
      ); */
    try {
      $status = $this->_client->doNewAuctionExt($request);
      $this->aktualizujTytulAukcjiWDB($def['przedmiotid'], $def['rozmiar'], $tablica[1]['string']);
      //$status = $this->_client->doCheckNewAuctionExt($request);
    } catch (SoapFault $SoapException) {
      $error = "ERROR: ";
      $error .= utf2iso($SoapException->{'faultstring'});
      $error .= "ID: " . $def['przedmiotid'] . " Rozmiar:" . $def['rozmiar'];
      echo $error . '<br>';
      echo '<pre>';
      print_r($opcje);
      echo '</pre>';
    }
    $error = "";
    $query = "INSERT INTO allegro_aukcje (aukcja_id ,przedmiot_id, rozmiar, kwota, sztuk, status, wystaw_pono, zamile, szablon, allcatid, tytul)
                VALUES (" . $status->itemId . ", " . $def['realItemId'] . ", " . $def['rozmiar'] . ", " . $tablica[8]['float'] . ", " . $tablica[5]['int'] . ", 1, "
            . "" . $def['wystpon'] . ", " . $def['zamile'] . ", " . $def['szablon'] . ", " . $def['allcatid'] . ", '" . $tablica[1]['string'] . "')";

    mysql_query($query) or $error = "Zapytanie zako�czone niepowodzeniem" . mysql_error();
    return $status;
  }

  public function doVerifyItem() {
    $request = array(
        'sessionHandle' => $this->_session,
        'localId' => 1
    );

    return $this->_client->doVerifyItem($request);
  }

  public function iso2utf($str) {
    return iconv("ISO-8859-2", "UTF-8", $str);
  }

  public function doChangeQuantityItem($item, $count) {
    $request = array(
        'sessionHandle' => $this->_session,
        'itemId' => $item,
        'newItemQuantity' => $count
    );

    return $this->_client->doChangeQuantityItem($request);
  }

  public static function data2FID($array) {
    if (!is_array($array) || !count($array))
      return array();

    $FIDs_array = array();
    foreach ($array as $fid => $data) {
      if (is_numeric($fid)) {
        $FID = self::getEmptyFID();

        $FID['fid'] = (int) $fid;

        if (isset($data['string'])) {
          $FID['fvalueString'] = (string) iso2utf($data['string']);
        } elseif (isset($data['int'])) {
          if (is_array($data['int'])) {// bit mask
            $FID['fvalueInt'] = (int) array_sum($data['int']);
          } else
            $FID['fvalueInt'] = (int) $data['int'];
        }
        elseif (isset($data['float'])) {
          $FID['fvalueFloat'] = (float) $data['float'];
        } elseif (isset($data['datetime'])) {
          $FID['fvalueDatetime'] = (int) $data['datetime'];
        } elseif (isset($data['image'])) {
          $FID['fvalueImage'] = $data['image'];
        }
        $FIDs_array[] = $FID;
      }
    }
    return $FIDs_array;
  }

  private static function getEmptyFID() {
    return array(
        'fid' => 0,
        'fvalueString' => '',
        'fvalueInt' => 0,
        'fvalueFloat' => 0,
        'fvalueImage' => 0,
        'fvalueDatetime' => 0,
        'fvalueDate' => '',
        'fvalueRangeInt' => array(
            'fvalueRangeIntMin' => 0,
            'fvalueRangeIntMax' => 0),
        'fvalueRangeFloat' => array(
            'fvalueRangeFloatMin' => 0,
            'fvalueRangeFloatMax' => 0),
        'fvalueRangeDate' => array(
            'fvalueRangeDateMin' => '',
            'fvalueRangeDateMax' => '')
    );
  }

  public function getCronConf() {
    $query = 'SELECT nazwa, wartosc FROM `allegro_konfiguracja`';
    $result = mysql_query($query) or die("Pobranie konfiguracji B��D");

    while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
      $this->conf[$line['nazwa']] = $line['wartosc'];
  }

  public function getCronConfVal($val) {
    return $this->conf[$val];
  }
  
  public function getMySellItems($nr) {
    $request = array(
        'sessionId' => $this->_session,
        'pageNumber' => $nr
    );  
    return $this->_client->doGetMySellItems($request);
  }
  
  public function getDziennikAukcji($rowid) {  
    $request = array(
        'sessionHandle' => $this->_session,
        'startingPoint' => (float) $rowid,
        'infoType' => 0
    );

    return $this->_client->doGetSiteJournal($request);//17154231612
  }
    
  public function getDziennikZdarzen($deal) {
    $request = array(
        'sessionId' => $this->_session,
        'journalStart' => (float) $deal
    );

    return $this->_client->doGetSiteJournalDeals($request);
  }

  public function setStatusAukcji($auckja, $status) {
    $query = 'UPDATE allegro_aukcje SET status = ' . $status . ' WHERE aukcja_id = ' . $auckja;
    $result = mysql_query($query) or die("Pobranie kategorii B��D");
  }

  public function setConf($name, $value) {
    $query = 'UPDATE allegro_konfiguracja SET wartosc = "' . $value . '" WHERE nazwa = "' . $name . '"';
    $result = mysql_query($query) or die("Aktualizacja konfiguracji b��d");
  }

  public function setPriceAuction($numer, $nowacena) {
    try {
      $tablica = array();
      $tablica[8]['float'] = $nowacena;
      $opcje = self::data2FID($tablica);
      $request = array(
          'sessionId' => $this->_session,
          'itemId' => (float) $numer,
          'fieldsToModify' => $opcje,
          'previewOnly' => 0
      );

      $this->_client->doChangeItemFields($request);

      $query = 'UPDATE allegro_aukcje SET kwota = ' . $nowacena . ' WHERE aukcja_id = ' . $numer;
      $result = mysql_query($query) or die("Pobranie kategorii B��D");
    } catch (SoapFault $SoapException) {
      return $SoapException;
    }
    return true;
  }

  public function checkPriceAuctions() {
    /*
      $query = 'SELECT aa.aukcja_id, aa.kwota, p.brutto, p.id, t.rozmiar FROM PRODUKTY p '
      .'LEFT JOIN TOWAR t ON p.Id = t.IdProd '
      .'LEFT JOIN allegro_aukcje aa ON (aa.przedmiot_id = t.IdProd OR aa.przedmiot_id = p.id) AND (aa.rozmiar = t.rozmiar OR (aa.rozmiar = 0 AND p.rozmiar IS NULL)) '
      .'WHERE aa.status = 1 '
      .'GROUP BY aa.aukcja_id ORDER BY p.id';

      $query = 'SELECT aa.aukcja_id, aa.kwota, p.brutto, p.id, t.rozmiar FROM PRODUKTY p '
      .'LEFT JOIN PRODUKTY pp ON pp.Symbol = p.symbol '
      .'LEFT JOIN TOWAR t ON p.Id = t.IdProd '
      .'LEFT JOIN allegro_aukcje aa ON aa.przedmiot_id = pp.Id AND (aa.rozmiar = pp.rozmiar OR ( aa.rozmiar = 0 AND pp.rozmiar IS NOT NULL )) AND aa.status = 1 '
      .'GROUP BY aa.aukcja_id ORDER BY aa.id';
     */
    $query = 'SELECT aa.aukcja_id, aa.kwota, p.brutto, p.id FROM allegro_aukcje aa '
            . 'LEFT JOIN PRODUKTY p ON aa.przedmiot_id = p.Id '
            . 'WHERE aa.status = 1 AND round(p.brutto*'.self::mnoznik.',2) <> aa.kwota '
            . 'GROUP BY aa.aukcja_id ORDER BY aa.id';

    $result = mysql_query($query) or die("Pobranie kategorii B��D" . mysql_error());

    while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
      {
      $line[brutto] = round(self::mnoznik*$line[brutto],2);
      $res[] = $line;
      }
      
    if (isset($res) && count($res) > 0)
      return $res;
    else
      return null;
    /*
      $query = 'SELECT aa.aukcja_id, aa.status, aa.wystaw_pono, pp.id as stareid, p.id, p.symbolyes, p.yesstanph, p.yesstandetal, p.yesstancentrala ,p.symbol, p.brutto, p.rozmiar as rozm, p.masa, p.czterytygodnie, p.rodzaj, sum(t.ilosc) as ile FROM PRODUKTY p '
      .'LEFT JOIN PRODUKTY pp ON pp.Symbol = p.symbol '
      .'LEFT JOIN TOWAR t ON p.Id = t.IdProd '
      .'LEFT JOIN allegro_aukcje aa ON aa.przedmiot_id = pp.Id AND (aa.rozmiar = pp.rozmiar OR ( aa.rozmiar = 0 AND pp.rozmiar IS NOT NULL )) AND aa.status = 1 '
      .'GROUP BY p.Id, p.rozmiar ORDER BY pp.id'; */
  }

  public function getClient($id_aukcji, $id_klienta) {
    $return = array('status' => true);
    try {
      $request = array(
          'sessionHandle' => $this->_session,
          'itemsArray' => array($id_aukcji)
      );
      $ret = $this->_client->doGetPostBuyData($request);
      foreach ($ret->itemsPostBuyData->item->usersPostBuyData as $item) {
        if (is_array($item)) {
          foreach ($item as $its) {
            if ($its->userData->userId == $id_klienta) {
              $tab['userLogin'] = $this->utf2iso($its->userData->userLogin);
              $tab['userFirstName'] = $this->utf2iso($its->userData->userFirstName);
              $tab['userLastName'] = $this->utf2iso($its->userData->userLastName);
              $tab['userMaidenName'] = $this->utf2iso($its->userData->userMaidenName);
              $tab['userCompany'] = $this->utf2iso($its->userData->userCompany);
              $tab['userCountryId'] = $its->userData->userCountryId;
              $tab['userStateId'] = $its->userData->userStateId;
              $tab['userPostcode'] = $its->userData->userPostcode;
              $tab['userCity'] = $this->utf2iso($its->userData->userCity);
              $tab['userAddress'] = $this->utf2iso($its->userData->userAddress);
              $tab['userEmail'] = $this->utf2iso($its->userData->userEmail);
              $tab['userPhone'] = $its->userData->userPhone;
              $tab['userPhone2'] = $this->utf2iso($its->userData->userPhone2);
              $tab['userRating'] = $its->userData->userRating;
              if ($its->userSentToData->userId != 0) {
                $tab['userFirstName_dowys'] = $this->utf2iso($its->userSentToData->userFirstName);
                $tab['userLastName_dowys'] = $this->utf2iso($its->userSentToData->userLastName);
                $tab['userCompany_dowys'] = $this->utf2iso($its->userSentToData->userCompany);
                $tab['userCountryId_dowys'] = $its->userSentToData->userCountryId;
                $tab['userPostcode_dowys'] = $its->userSentToData->userPostcode;
                $tab['userCity_dowys'] = $this->utf2iso($its->userSentToData->userCity);
                $tab['userAddress_dowys'] = $this->utf2iso($its->userSentToData->userAddress);
              }
              $return['user'] = $tab;
            }
          }
        } else {
          if ($item->userData->userId == $id_klienta) {
            $tab['userLogin'] = $this->utf2iso($item->userData->userLogin);
            $tab['userFirstName'] = $this->utf2iso($item->userData->userFirstName);
            $tab['userLastName'] = $this->utf2iso($item->userData->userLastName);
            $tab['userMaidenName'] = $this->utf2iso($item->userData->userMaidenName);
            $tab['userCompany'] = $this->utf2iso($item->userData->userCompany);
            $tab['userCountryId'] = $item->userData->userCountryId;
            $tab['userStateId'] = $item->userData->userStateId;
            $tab['userPostcode'] = $item->userData->userPostcode;
            $tab['userCity'] = $this->utf2iso($item->userData->userCity);
            $tab['userAddress'] = $this->utf2iso($item->userData->userAddress);
            $tab['userEmail'] = $this->utf2iso($item->userData->userEmail);
            $tab['userPhone'] = $item->userData->userPhone;
            $tab['userPhone2'] = $this->utf2iso($item->userData->userPhone2);
            $tab['userRating'] = $item->userData->userRating;
            if ($item->userSentToData->userId != 0) {
              $tab['userFirstName_dowys'] = $this->utf2iso($item->userSentToData->userFirstName);
              $tab['userLastName_dowys'] = $this->utf2iso($item->userSentToData->userLastName);
              $tab['userCompany_dowys'] = $this->utf2iso($item->userSentToData->userCompany);
              $tab['userCountryId_dowys'] = $item->userSentToData->userCountryId;
              $tab['userPostcode_dowys'] = $item->userSentToData->userPostcode;
              $tab['userCity_dowys'] = $this->utf2iso($item->userSentToData->userCity);
              $tab['userAddress_dowys'] = $this->utf2iso($item->userSentToData->userAddress);
            }
            $return['user'] = $tab;
          }
        }
      }
    } catch (SoapFault $SoapException) {
      $return['status'] = false;
      $return['error'] = $SoapException;
    }
    return $return;
  }

  public function getAukcjaFromItCoder($id) {
    $query = "SELECT przedmiot_id, rozmiar, kwota, sztuk, tytul FROM allegro_aukcje where aukcja_id = " . $id;
    return mysql_fetch_row(mysql_query($query));
  }

  public function getProductById($id) {
    $query = "SELECT symbol, rozmiar, brutto, netto, typ, rodzajzestawienia FROM PRODUKTY WHERE Id=" . $id;
    $result = mysql_query($query);
    $item = mysql_fetch_array($result);
    $item[brutto] = round(self::mnoznik*$item[brutto],2);
    return $item;
  }

  public function getFormularz($trans_id) {
    $request = array(
        'sessionId' => $this->_session,
        'transactionsIdsArray' => array($trans_id)
    );
    return $this->_client->doGetPostBuyFormsDataForSellers($request);
  }

  public function getSposobDostawy($id) {
    $request = array(
        'countryId' => 1,
        'webapiKey' => $this->_api_key
    );
    $return = array();
    $dost = $this->_client->doGetShipmentData($request);
    foreach ($dost->shipmentDataList->item as $dt) {
      if ($dt->shipmentId != $id)
        continue;
      $return['nazwa'] = $dt->shipmentName;
      if ($dt->shipmentType == 1)
        $return['typ'] = "P�atno�� z g�ry";
      else if ($dt->shipmentType == 2)
        $return['typ'] = "P�atno�� przy odbiorze";
      if ($dt->shipmentId == $id)
        break;
    }
    return $return;
  }

  public function dealAllegro($event) {
    $error = false;
    if ($event->dealEventType == 1) {
      $klient = $this->getClient($event->dealItemId, $event->dealBuyerId);
      if ($klient['status'] !== false) {
        $user = $klient['user'];
        $query2 = "SELECT id from USERS where Klient and Imie='" . $user['userFirstName'] . "' and Nazwisko='" . $user['userLastName'] . "' "
                . " and NickAllegro = '" . $user['userLogin'] . "' ";
        $result = mysql_query($query2);
        $ile = mysql_num_rows($result);
        $kor_ul = "";
        $kor_miesc = "";
        $kor_kod = "";
        if (isset($user['userFirstName_dowys'])) {
          $kor_ul = $user['userAddress_dowys'];
          $kor_miesc = $user['userCity_dowys'];
          $kor_kod = $user['userPostcode_dowys'];
          $dane_do_wys = "Dane kupuj�cego " . $user['userLogin'] . " (" . $user['userRating'] . "): <br>" . $user['userFirstName_dowys'] . " " . $user['userLastName_dowys'] . "<br>"
                  . "Ulica: " . $user['userAddress_dowys'] . "<br>"
                  . "" . $user['userPostcode_dowys'] . " " . $user['userCity_dowys'] . "<br>"
                  . $user['userPhone'] . ", " . $user['userPhone2'] . "<br>"
                  . "" . $user['userEmail'] . "<br>";
        } else {
          $dane_do_wys = "Dane kupuj�cego " . $user['userLogin'] . " (" . $user['userRating'] . "): <br>" . $user['userCompany'] . "<br>" . $user['userFirstName'] . " " . $user['userLastName'] . "<br>"
                  . "Ulica: " . $user['userAddress'] . "<br>"
                  . "" . $user['userPostcode'] . " " . $user['userCity'] . "<br>"
                  . $user['userPhone'] . ", " . $user['userPhone2'] . "<br>"
                  . "" . $user['userEmail'] . "<br>";
        }
        $aukcja = $this->getAukcjaFromItCoder($event->dealItemId);
        $sum = $event->dealQuantity * $aukcja[2];
        $zam = $aukcja[4] . " (" . $event->dealItemId . ")<br>"
                . "Sztuk: " . $event->dealQuantity . "<br>"
                . "Cena za sztuk�: " . $aukcja[2] . "  <br>"
                . "��czna kwota: " . $sum . "  <br>";
        $zam .= "<br>" . $dane_do_wys;

        if ($ile) { //Czy jest klient
          $row = mysql_fetch_row($result);
          $kl_id = $row[0];
        } else { //tworzenie klienta
          $login = "K" . rand(10000, 99999);
          $haslo = substr(md5($login), 0, 8);

          $query4 = "INSERT INTO USERS(Login,Haslo,Imie,Nazwisko,Ulica,KodPocztowy,Miejscowosc,Ulica2, KodPocztowy2,Miejscowosc2,"
                  . "Telefon,Komorka,Email,NazwaFirmy,Wojewodztwo,Kraj,Lang,Waluta,DataDodania,Klient,NickAllegro) "
                  . "VALUES('" . $login . "','" . sha1($haslo) . "','" . $user['userFirstName'] . "','" . $user['userLastName'] . "','" . $user['userAddress'] . "','" . $user['userPostcode'] . "','" . $user['userCity'] . "','" . $kor_ul . "','" . $kor_kod . "','" . $kor_miesc . "',"
                  . "'" . $user['userPhone'] . "','" . $user['userPhone2'] . "','" . $user['userEmail'] . "','" . $user['userCompany'] . "','" . $user['userStateId'] . "','0','PL','PLN',now(),1,'" . $user['userLogin'] . "')";
          $result = mysql_query($query4) or $error[] = mysql_error();
          $kl_id = mysql_insert_id();
        }//koniec tworzenia klienta

        $prod = $this->getProductById($aukcja[0]);
        $kwota_brutto = $prod['brutto'] * $event->dealQuantity;
        $kwota_netto = $prod['netto'] * $event->dealQuantity;

        $query3 = "INSERT INTO ZAMOWIENIA (IdUser,KwotaBrutto,KwotaNetto,DataZam,CzasZam,Komentarz,DoZaplaty,Zatwierdzone) values ($kl_id, $kwota_brutto, $kwota_netto, '" . $this->currdate() . "','" . $this->currtime() . "','$zam',$kwota_brutto,1)";
        $res = mysql_query($query3) or $error[] = mysql_error();
        if ($res) {
          $last_id = mysql_insert_id();
          $query = "INSERT INTO allegro_deals (id,item_id,transaction_id,buyer_id,quantity,status,time,zam_id)
          VALUES (" . $event->dealId . "," . $event->dealItemId . "," . $event->dealTransactionId . "," . $event->dealBuyerId . "," . $event->dealQuantity . ",1," . $event->dealEventTime . "," . $last_id . ")";
          mysql_query($query) or $error[] = mysql_error();
          /*
          if ($prod['typ'] == 0)
            $query6 = "INSERT INTO PRODUKTYZAM SET IdZam=$last_id, Symbol='" . $prod['symbol'] . "', Ilosc=" . $event->dealQuantity . ", Brutto='$kwota_brutto', Netto='" . $kwota_netto . "', Typ='" . $prod['typ'] . "', RodzajZestawienia='" . $prod['rodzajzestawienia'] . "'";
          else
            $query6 = "INSERT INTO PRODUKTYZAM SET IdZam=$last_id, Symbol='" . $prod['symbol'] . "', Rozmiar='" . $prod['rozmiar'] . "', Ilosc=" . $event->dealQuantity . ", Brutto='$kwota_brutto', Netto='" . $kwota_netto . "', Typ='" . $prod['typ'] . "', RodzajZestawienia='" . $prod['rodzajzestawienia'] . "'";
          */  
            
            
          if ($prod['typ'] == 0)
              $query6 = "INSERT INTO PRODUKTYZAM SET IdZam=$last_id, Symbol='" . $prod['symbol'] . "', Ilosc=1, Brutto='" . $prod['brutto'] . "', Netto='" . $prod['netto'] . "', Typ='" . $prod['typ'] . "', RodzajZestawienia='" . $prod['rodzajzestawienia'] . "'";
          else
              $query6 = "INSERT INTO PRODUKTYZAM SET IdZam=$last_id, Symbol='" . $prod['symbol'] . "', Rozmiar='" . $prod['rozmiar'] . "', Ilosc=1, Brutto='" . $prod['brutto'] . "', Netto='" . $prod['netto'] . "', Typ='" . $prod['typ'] . "', RodzajZestawienia='" . $prod['rodzajzestawienia'] . "'";
            
          for($i=0; $i<$event->dealQuantity; $i++)
              mysql_query($query6) or $error[] = mysql_error();
        }
      } else
        $error[] = $klient['error'];
    } else if ($event->dealEventType == 2) {// DEAL EVENT TYP 2
      //zrob tabele z transakcjami
      $qr = "SELECT zam_id FROM allegro_deals where id = " . $event->dealId;
      $deal = mysql_fetch_row(mysql_query($qr));

      $qr = "SELECT komentarz FROM ZAMOWIENIA where id = " . $deal[0];
      $zam = mysql_fetch_row(mysql_query($qr));

      $query = 'UPDATE allegro_deals SET transaction_id = ' . $event->dealTransactionId . ', status = 2 WHERE id = ' . $event->dealId;
      mysql_query($query) or $error[] = mysql_error();

      $form = $this->getFormularz($event->dealTransactionId)->postBuyFormData->item;
      $dost = $this->getSposobDostawy($form->postBuyFormShipmentId);
      /* 9:Przesy�ka kurierska
        10:Odbi�r osobisty
        11:Przesy�ka kurierska pobraniowa
        12:Przesy�ka elektroniczna (e-mail)
        13:Odbi�r osobisty po przedp�acie
        */
      $przesylkaId=0;
      switch($form->postBuyFormShipmentId)
        {
        case 9: $przesylkaId=1; break;
        case 11:$przesylkaId=2; break;
        case 10:
        case 13:$przesylkaId=3; break;
        }
      $f_p = "---Formularz pozakupowy---";
      $f_p .= "<br>" . $dost['typ'] . "<br>";
      if ($form->postBuyFormPayType == "collect_on_delivery")
        $f_p .= "P�ac� przy odbiorze";
      else if ($form->postBuyFormPayType == "wire_transfer")
        $f_p .= "Zwyk�y przelew - poza systemem PayU";
      else if ($form->postBuyFormPayType == "not_specified")
        $f_p .= "Brak w specyfikacji";
      else if ($form->postBuyFormPayType == "co")
        $f_p .= "Checkout PayU";
      else if ($form->postBuyFormPayType == "ai")
        $f_p .= "Raty PayU";
      else
        $f_p .= "P�atno�� PayU";
      $f_p .= "<br>";
      $koszt_dostawy = $form->postBuyFormPostageAmount;
      $f_p .= $dost['nazwa'] . " (" . $koszt_dostawy . " z�)<br>";
      if ($form->postBuyFormInvoiceOption == 1) {
        $fatkura = "Tak";
      } else {
        $faktura = "Nie";
      }
      $adr_dst = $form->postBuyFormShipmentAddress;

      $f_p .= "<br>Adres dostawy:<br>";
      if (!empty($adr_dst->postBuyFormAdrCompany))
        $f_p .= $this->utf2iso($adr_dst->postBuyFormAdrCompany) . "<br>";
      $f_p .= $this->utf2iso($adr_dst->postBuyFormAdrFullName) . "<br>";
      $f_p .= $this->utf2iso($adr_dst->postBuyFormAdrStreet) . "<br>";
      $f_p .= $adr_dst->postBuyFormAdrPostcode . " " . $this->utf2iso($adr_dst->postBuyFormAdrCity) . "<br>";
      $f_p .= $adr_dst->postBuyFormAdrPhone . "<br>";
      if (!empty($adr_dst->postBuyFormAdrNip))
        $f_p .= "NIP: " . $adr_dst->postBuyFormAdrNip . "<br>";
      if (!empty($form->postBuyFormMsgToSeller))
        $f_p .= "Wiadomo�� od kupuj�cego: <br>" . $this->utf2iso($form->postBuyFormMsgToSeller) . "<br>";
      $f_p .= "------------------------------";

      $new = $f_p . "<br>" . $zam[0];

      if($przesylkaId)
        {
        $qr = "SELECT DoZaplaty FROM ZAMOWIENIA where id = " . $deal[0];
        $zam_dozapl = mysql_fetch_row(mysql_query($qr));
        $qr = "SELECT Nazwa FROM PRZESYLKI where id = " . $przesylkaId;
        $przes_nazw = mysql_fetch_row(mysql_query($qr));
        $qrprzes = "UPDATE ZAMOWIENIA SET Przesylka='".$przes_nazw[0]."', Przesylka2='".$przes_nazw[0]."', KosztPrzesylki='".$koszt_dostawy."', DoZaplaty='".($zam_dozapl[0]+$koszt_dostawy)."' WHERE Id=".$deal[0];
        mysql_query($qrprzes) or $error[] = mysql_error();
        }

      $query = 'UPDATE ZAMOWIENIA SET komentarz = "' . $new . '" WHERE id = ' . $deal[0];
      mysql_query($query) or $error[] = mysql_error();

      //wype³niony formularz
    } else if ($event->dealEventType == 3) {
      $qr = "SELECT zam_id FROM allegro_deals where id = " . $event->dealId;
      $deal = mysql_fetch_row(mysql_query($qr));

      $qr = "SELECT komentarz FROM ZAMOWIENIA where id = " . $deal[0];
      $zam = mysql_fetch_row(mysql_query($qr));

      $query = 'UPDATE allegro_deals SET transaction_id = ' . $event->dealTransactionId . ', status = 3 WHERE id = ' . $event->dealId;
      mysql_query($query) or $error[] = mysql_error();

      $new = "<br><font color='red'>PONI�SZY FORMULARZ ANULOWANO</font><br>" . $zam[0];
      $query = 'UPDATE ZAMOWIENIA SET komentarz = "' . $new . '" WHERE id = ' . $deal[0];
      mysql_query($query) or $error[] = mysql_error();

      //anulowanie, powrót do danych standardowych
    } else if ($event->dealEventType == 4) {

      $qr = "SELECT zam_id FROM allegro_deals where id = " . $event->dealId;
      $deal = mysql_fetch_row(mysql_query($qr));

      $qr = "SELECT komentarz FROM ZAMOWIENIA where id = " . $deal[0];
      $zam = mysql_fetch_row(mysql_query($qr));

      $query = 'UPDATE allegro_deals SET transaction_id = ' . $event->dealTransactionId . ', status = 4 WHERE id = ' . $event->dealId;
      mysql_query($query) or $error[] = mysql_error();

      $new = "<br><font color='green'>P�ATNO�� PAYU ZAKO�CZONA</font><br>" . $zam[0];
      $query = 'UPDATE ZAMOWIENIA SET komentarz = "' . $new . '", Przedplata=1 WHERE id = ' . $deal[0];
      mysql_query($query) or $error[] = mysql_error();




      //op³acone
    }


    if ($error !== false)
      return $error;
    else
      return true;
  }

}
