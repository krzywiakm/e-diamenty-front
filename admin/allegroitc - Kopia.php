<?php

/*
  ini_set('display_errors', 'On');
  error_reporting(E_ALL); */

require_once 'class/allegro.php';

function utf2iso($str)
{
  return iconv("UTF-8", "ISO-8859-2", $str);
}

function iso2utf($str)
{
  return iconv("ISO-8859-2", "UTF-8", $str);
}

if(!auth())
{
  echo "<p class=error align=center>*** Brak dost�pu ***</p>";
}
else
{
  echo '<script type="text/javascript" src="./js/jquery.js"></script>';
  echo '<script type="text/javascript" src="./js/itcoder.js"></script>';
  echo '<a href="index.php?page=allegroitcs"> >>Edycja szablon�w aukcji<< </a><br><br>';
  $country = 1;
  $allegro = new allegro();
  $allegro->allCon($country);
  $allegro->Login("STAVIORI", "1dnmk1AudiRS6!", "048e8d8238");
  echo $allegro->checkCategory();

  //print_r($allegro->doVerifyItem());
  //print_r($allegro->doChangeQuantityItem(4069593725, 1));



  $mainCategories = $allegro->getServiceParentCategories(0, $country);
  echo '<table width="100%" cellspacing="0" cellpadding="0" id="globalBox"><tr>
       <td class="leftTableRow">Wybierz kategori�: </td>
       <td>
       <select class="categorySelect" name="options[]">
        <option value="0">-- Wybierz --&nbsp;</option>';
  foreach($mainCategories as $k => $category)
  {
    echo '<option value="'.$category['id_category'].'">'.$category['name'].' ('.$category['id_category'].')&nbsp;</option>';
  }
  echo '</select></td></tr></table>';

  if(isset($_GET['id']) && ($_GET['id'] == 123450 || $_GET['id'] == 110774))
    $nr_kat = 34;
  else
  {
    echo "Kategoria nie jest zdefiniowana";
    exit;
  }

  if(isset($_GET['id']))
  {
    $shopCats = $allegro->doGetShopCatsDataSELECT();
    echo '<form action="index.php" method="GET">';
    echo '<table><tr><td>Wybierz kategori� sklepu: </td><td>'.$shopCats.'</td></tr></table>';
    echo '</form>';

    if(!isset($_GET['catid']))
      exit;

    echo '<br>'.$allegro->getCategoryNames($_GET['id']).'<br><br>';

    echo '<form action="index.php" method="GET">';
    echo '<input type="hidden" name="page" value="allegroitc">';
    echo '<input type="hidden" name="id" value="'.$_GET['id'].'">';
    echo '<input type="hidden" name="catid" value="'.$_GET['catid'].'">';

    echo '<table><tr><td>Wybierz szablon: </td><td><select name="t">';
    $query = "SELECT id, wartosc from allegro_konfiguracja where nazwa = 'theme' order by Id";
    $result = mysql_query($query) or die("Zapytanie zako�czone niepowodzeniem");
    while($line = mysql_fetch_array($result, MYSQL_ASSOC))
    {
      $sr = "";
      if(isset($_GET['t']) && $_GET['t'] == $line['id'])
        $sr = "selected";
      echo '<option value="'.$line['id'].'" '.$sr.'>'.$line['wartosc'].'</option>';
    }
    echo '</select></td><td><input type="submit" value="OK"></td></tr></table>';
    echo '</form>';


    if(!isset($_GET['t']))
      exit;

    if(!isset($_GET['m']) || $_GET['m'] == 0)
      echo '<a href="index.php?page=allegroitc&id='.$_GET['id'].'&catid='.$_GET['catid'].'&t='.$_GET['t'].'&m=1">Poka� miniatury</a>';
    else if($_GET['m'] == 1)
      echo '<a href="index.php?page=allegroitc&id='.$_GET['id'].'&catid='.$_GET['catid'].'&t='.$_GET['t'].'&m=0">Ukryj miniatury</a>';


    if(isset($_POST['newAct']))
    {

      echo '<br>';
      if(!isset($_POST['rozmiar']))
        $rozmiarek = 0;
      else
        $rozmiarek = $_POST['rozmiar'];
      $status = $allegro->wystawAukcjeTest($_POST['pole'], $_POST['idprzedmiotu'], $_POST['zamile'], $_GET['t'], $rozmiarek);
      //stdClass Object ( [itemId] => 4069367790 [itemInfo] => 0,20 zł [itemIsAllegroStandard] => 0 ) 
      echo '<br><br>';
      if(isset($status->itemId))
        echo '<font color="green">Aukcja wystawiona prawid�owo ID: <b>'.$status->itemId.'</b></font>';

      //print_r($status);
      echo '<br><br><br>';
    }

    $forms = $allegro->getFormFields($country, $_GET['id']);
    
      foreach($forms->sellFormFieldsForCategory->sellFormFieldsList->item as $form)
      {
      echo $form->sellFormId.' '.$form->sellFormTitle.'<br>';
      print_r($form);
      echo '<br><br>';
      } 

    foreach($forms->sellFormFieldsForCategory->sellFormFieldsList->item as $form)
    {
      if($form->sellFormCat != 0)
      {
        $form->sellFormUnit = utf2iso($form->sellFormUnit);
        $form->sellFormTitle = utf2iso($form->sellFormTitle);
        $form->sellFormFieldDesc = utf2iso($form->sellFormFieldDesc);
        $form->sellFormDesc = utf2iso($form->sellFormDesc);
        //echo $form->sellFormId.' - '.$form->sellFormTitle.'<br>';
        if($form->sellFormResType == 1)
          $res = 'string';
        else if($form->sellFormResType == 2)
          $res = 'int';
        else if($form->sellFormResType == 3)
          $res = 'float';
        else
          $res = 'nieznane';

        if($form->sellFormType == 3)//float, input text
        {
          $d_p[$form->sellFormType][] = array(
              'id'      => $form->sellFormId,
              'input'   => 'text',
              'resType' => $res,
              'title'   => $form->sellFormTitle
          );
        }
        else if($form->sellFormType == 4) //rozwijana lista, przekazujemy inta do allegro
        {
          $lista = explode("|", $form->sellFormDesc);
          $d_p[$form->sellFormType][] = array(
              'id'      => $form->sellFormId,
              'input'   => 'combobox',
              'resType' => $res,
              'title'   => $form->sellFormTitle,
              'lista'   => $lista
          );
        }
        else if($form->sellFormType == 6) //checkbox, przekazujemy int
        {
          $lista = explode("|", $form->sellFormDesc);
          $numeracja = explode("|", $form->sellFormOptsValues);
          $d_p[$form->sellFormType][] = array(
              'id'        => $form->sellFormId,
              'input'     => 'checkbox',
              'resType'   => $res,
              'title'     => $form->sellFormTitle,
              'lista'     => $lista,
              'numeracja' => $numeracja
          );
        }
      }
    }
    $produkty = $allegro->getProd($nr_kat);
    $kattypy = $allegro->getKatTypy();
    $skladniki = $allegro->getSkladniki();
    //kamien
    $sk_kr_gor = array(172);
    $sk_perla = array(79, 119, 122, 165);
    $sk_bursztyn = array(33);
    $sk_szafiry = array(51, 74, 75, 81, 84);
    $sk_diamenty = array(90, 243, 38, 43, 64, 77);
    $sk_rubin = array(33);
    $sk_szmaragd = array(95);
    $sk_cyrkonia = array(100, 164, 190);
    $sk_krysztal = array(172);

    //material
    $sk_biale_zloto = array(76);
    $sk_zloto = array(19, 176, 207, 239);
    $sk_srebro = array(21, 210);
    $sk_stal = array(236);
    $sk_szklo = array(230);

    $prodts_zam = array();
    foreach($produkty as $p)
    {
      $zam = 0;
      $zam_ile = 0;
      $sztuk = 0;
      if($p['symbolyes'] == null && $p['czterytygodnie'] == 0 && $p['ile'] == 0)
        continue;
      $info = "";
      if($p['czterytygodnie'] > 0 && $p['symbolyes'] == null && $p['ile'] == 0)
      {
        if(in_array($p['id'], $prodts_zam))
          continue;

        $prodts_zam[] = $p['id'];
        $zam = 1;
        $zam_ile = $p['czterytygodnie'];
        $info = "<b>Na zam�wienie w ci�gu ".$p['czterytygodnie']." tygodni</b>";
        $sztuk = 2;
        $cf_wysylka = 504;
      }
      else if($p['symbolyes'] == null && $p['ile'] > 0)
      {
        $info = "<b>Od r�ki: ".$p['ile'].'</b>';
        $sztuk = $p['ile'];
        $cf_wysylka = 24;
      }
      else if($p['symbolyes'] != null)
      {
        $magazyn = $p['ile'] + $p['yesstancentrala'];
        if($magazyn > 0)
        {
          $info = "<b>Od r�ki: ".$magazyn.'</b>';
          $sztuk = $magazyn;
          $cf_wysylka = 24;
        }
        else if($magazyn == 0)
        {
          $st = $p['yesstanph'] + $p['yesstandetal'];
          if($st > 0)
          {
            $info = "<b>Na zam�wienie w ci�gu 1 tygodnia: ".$st."</b>";
            $zam_ile = 1;
            $sztuk = $st;
            $cf_wysylka = 168;
          }
          else
          //$info = "<b>BRAK WSZ�DZIE</b>"; 
            continue;
        }
      }

      $min = "";
      if(isset($_GET['m']) && $_GET['m'] == 1)
        if(file_exists("../katalog/foto/".$p['id']."mini.jpg"))
        {
          $min = "<img src='../katalog/foto/".$p['id']."mini.jpg'/>";
        }

      echo '<form action="index.php?page=allegroitc&id='.$_GET['id'].'&catid='.$_GET['catid'].'&t='.$_GET['t'].'" method="POST">';
      echo '<table width="100%">';




      if($p['aukcja_id'] != null && $p['status'] == 1)
        echo '<tr style="background-color: rgb(36, 228, 75);">';
      else
        echo '<tr style="background-color: rgb(193, 193, 241);">';




      $r = floor($p['rodzaj'] / 100);
      $t = $p['rodzaj'] - $r * 100;








      if($_GET['id'] == 110774)
        $tytul = $kattypy[$r][$t].' '.$p['symbol'].' r.'.$p['rozm'].' STAVIORI';
      else
        $tytul = $kattypy[$r][$t].' '.$p['symbol'].' STAVIORI';



      $ile = strlen($tytul);
      if($ile <= 50)
        $txt = 'Znak�w w tytule: '.$ile;
      else
        $txt = '<font color="red"><b>Znak�w w tytule: '.$ile.'</b></font>';

      echo '<td>'.$min.'<input type="text" size="50" name="pole[1][string]" value="'.$tytul.'"><br>'.$txt.'<br>';






      if($zam == 1)
        echo 'Rozmiar: ka�dy<br>';
      else
        echo 'Rozmiar: '.$p['rozm'].'<br>';








      echo 'Cena: '.$p['brutto'].' z�<br>';
      /* echo 'CzteryTyg: '.$p['czterytygodnie'].'<br>';
        echo 'Podr�czny: '.$p['ile'].'<br>';
        echo 'Centrala: '.$p['yesstancentrala'].'<br>';
        echo 'PH: '.$p['yesstanph'].'<br>';
        echo 'Detal: '.$p['yesstandetal'].'<br>';
        echo 'SymbolYes: '.$p['symbolyes'].'<br>'; */

      echo $info;
      echo '</td>';
      echo '<td>';

      foreach($d_p as $id => $f)
      {
        if($id == 3)
        {
          foreach($f as $t)
          {
            /* if($t['id'] == 22060)
              {
              if($p['rozmiar'] != null && $p['rozmiar'] != 0)
              echo $t['title'].': '.$p['rozmiar'].' cm<input type="hidden" name="pole['.$t['id'].']['.$t['resType'].']" value = "'.$p['rozmiar'].'"><br>';
              } */
            if($t['id'] == 22123)
            {
              if($p['masa'] != null && $p['masa'] != 0)
              {
                $masa = $p['masa'] / 100;
                echo $t['title'].': '.$masa.' kg<input value="'.(float) $masa.'" type="hidden" name="pole['.$t['id'].']['.$t['resType'].']"><br>';
              }
            }
            else
              echo $t['title'].': BRAK(3)<br>';
          }
        }
        if($id == 4) //combobox
        {
          foreach($f as $t)
          {
            if($t['id'] == 22064) //splot
            {
              $i = 0;
              echo $t['title'].': <select name="pole['.$t['id'].']['.$t['resType'].']">';
              foreach($t['lista'] as $nazwa)
              {
                if($i == 9)
                  echo '<option value="'.$i++.'" selected>'.$nazwa.'</option>';
                else
                  echo '<option value="'.$i++.'">'.$nazwa.'</option>';
              }
              echo '</select><br>';
            }
            else if($t['id'] == 22067 || $t['id'] == 22069)//stan
            {
              echo $t['title'].': Nowe<input value="1" type="hidden" name="pole['.$t['id'].']['.$t['resType'].']">';
              echo '<br>';
            }
            else if($t['id'] == 22051)//Rozmiar/�rednica wew. [mm] dla 123450
            {
              if(isset($p['rozm']) && $zam == 0)
              {
                echo $t['title'].': '.$t['lista'][$p['rozm']].'<input value="'.$p['rozm'].'" type="hidden" name="pole['.$t['id'].']['.$t['resType'].']">';
                echo '<br>';
              }
            }
            else
              echo $t['title'].': BRAK(4-'.$t['id'].')<br>';
          }
        }
      }

      echo '</td>';

      foreach($d_p as $id => $f)
      {
        if($id == 6) //checkbox
        {
          echo '<td>';
          foreach($f as $t)
          {
            if($t['id'] == 22052)//typ dla 123425
            {
              echo 'Typ: R�kodzie�o, Wyr�b jubilerski, Klasyczne<br>';
              echo '<input type="hidden" name="pole[22052][int]" value="19">';
            }
            else if($t['id'] == 22071 || $t['id'] == 22070)//materia� dla 123450
            {
              $sk = array();
              $im = 0;
              if(isset($skladniki[$p['id']]))
              {
                foreach($sk_biale_zloto as $sbz)
                  if(in_array($sbz, $skladniki[$p['id']]))
                  {
                    $sk[] = "Bia�e z�oto";
                    $im += 1;
                    break;
                  }
                foreach($sk_zloto as $s)
                  if(in_array($s, $skladniki[$p['id']]))
                  {
                    $sk[] = "Z�oto";
                    $im += 2;
                    break;
                  }
                foreach($sk_srebro as $s)
                  if(in_array($s, $skladniki[$p['id']]))
                  {
                    $sk[] = "Srebro";
                    $im += 4;
                    break;
                  }
                foreach($sk_stal as $s)
                  if(in_array($s, $skladniki[$p['id']]))
                  {
                    $sk[] = "Stal";
                    $im += 256;
                    break;
                  }
                foreach($sk_szklo as $s)
                  if(in_array($s, $skladniki[$p['id']]))
                  {
                    $sk[] = "Szk�o";
                    $im += 131072;
                    break;
                  }
              }
              if($im != 0)
              {
                $tyt = implode(", ", $sk);
                echo $t['title'].': '.$tyt.'<br>';
                echo '<input type="hidden" name="pole['.$t['id'].'][int]" value="'.$im.'">';
              }
            }
            else if($t['id'] == 22077 || $t['id'] == 22084 || $t['id'] == 22074)//kamie� 123425
            {
              $sk = array();
              $im = 0;
              if(isset($skladniki[$p['id']]))
              {
                foreach($sk_kr_gor as $s)
                  if(in_array($s, $skladniki[$p['id']]))
                  {
                    $sk[] = "Kryszta� g�rski";
                    $im += 1;
                    break;
                  }
                foreach($sk_perla as $s)
                  if(in_array($s, $skladniki[$p['id']]))
                  {
                    $sk[] = "Per�a";
                    $im += 2;
                    break;
                  }
                foreach($sk_bursztyn as $s)
                  if(in_array($s, $skladniki[$p['id']]))
                  {
                    $sk[] = "Bursztyn";
                    $im += 4;
                    break;
                  }
                foreach($sk_diamenty as $dm)
                  if(in_array($dm, $skladniki[$p['id']]))
                  {
                    $sk[] = "Brylant";
                    $im += 8;
                    break;
                  }
                foreach($sk_szafiry as $sz)
                  if(in_array($sz, $skladniki[$p['id']]))
                  {
                    $sk[] = "Szafir";
                    $im += 16;
                    break;
                  }
                foreach($sk_rubin as $sz)
                  if(in_array($sz, $skladniki[$p['id']]))
                  {
                    $sk[] = "Rubin";
                    $im += 32;
                    break;
                  }
                foreach($sk_szmaragd as $sz)
                  if(in_array($sz, $skladniki[$p['id']]))
                  {
                    $sk[] = "Szmaragd";
                    $im += 64;
                    break;
                  }
                foreach($sk_cyrkonia as $sz)
                  if(in_array($sz, $skladniki[$p['id']]))
                  {
                    $sk[] = "Cyrkonia";
                    $im += 128;
                    break;
                  }
              }

              if($im != 0)
              {
                $tyt = implode(", ", $sk);
                echo $t['title'].': '.$tyt.'<br>';
                echo '<input type="hidden" name="pole['.$t['id'].'][int]" value="'.$im.'">';
              }
            }
            else
              echo $t['title'].': BRAK(6-'.$t['id'].')<br>';
          }
          echo '</td>';
        }
      }

      echo '<td>'
      .'<input type="hidden" name="idprzedmiotu" value="'.$p['id'].'">'
      .'<input type="hidden" name="zamile" value="'.$zam_ile.'">'
      .'<input type="hidden" name="pole[2][int]" value="'.$_GET['id'].'">'//kategoria
      //. '<input type="hidden" name="pole[3][int]" value="5">'//data rozpoczecia
      .'<input type="hidden" name="pole[4][int]" value="5">'//na ile dni
      .'<input type="hidden" name="pole[5][int]" value="'.$sztuk.'">'//liczba sztuk
      .'<input type="hidden" name="pole[8][float]" value="'.$p['brutto'].'">'//cena
      .'<input type="hidden" name="pole[9][int]" value="1">'//kraj
      .'<input type="hidden" name="pole[10][int]" value="15">'//wojewodztwo
      .'<input type="hidden" name="pole[11][string]" value="Pozna�">'; //miejscowosc
      echo '<input value="'.$p['rozm'].'" type="hidden" name="rozmiar">';

      if($zam == 1)
      {
        echo '<input type="hidden" name="pole[13][int]" value="48">'; //48 - dodatkowe informacje i zagraniaca, 32- zagranica 
        echo '<input type="hidden" name="pole[27][string]" value="Produkt na zam�wienie.
                Termin realizacji okolo 4-5 tygodni.
                Wymagamy wp�aty zaliczki 30%.
                Nie ma mo�liwo�ci zwrotu towaru.">';
      }
      else
        echo '<input type="hidden" name="pole[13][int]" value="32">';

      echo '<input type="hidden" name="pole[15][int]" value="2">'; // miniaturka

      echo ''//opis przedmiotu
      .'<input type="hidden" name="pole[28][int]" value="0">'//Sztuki / Komplety / Pary
      .'<input type="hidden" name="pole[29][int]" value="1">'//format sprzedazy 0-kup teraz, 1-sklep
      .'<input type="hidden" name="pole[31][int]" value="'.$_GET['catid'].'">'//kategoria w sklepie
      .'<input type="hidden" name="pole[32][string]" value="60-179">'
      .'<input type="hidden" name="pole[33][string]" value="41 1140 2004 0000 3802 6522 2647">'
      .'<input type="hidden" name="pole[35][int]" value="5">'; //odbi�r osobisty przed i po wp�acie, darmowa opcja przsy�ki


      if($p['aukcja_id'] != null && $p['status'] == 1)
        echo '<center><a href="http://allegro.pl/show_item.php?item='.$p['aukcja_id'].'" target="_blank">Zobacz aukcj�</a></center><br><br>';
      else
        echo '<center><input type="submit" name="newAct" value="Wystaw aukcj�"></center><br><br>';



      echo '<center><a href="podglad.php?t='.$_GET['t'].'&id='.$p['id'].'&zamowienie='.$zam_ile.'" target="_blank">PODGL�D</a></center></td>';
      echo '</tr>';
      echo '</table>';
      echo '</form>';
    }
  }
}
