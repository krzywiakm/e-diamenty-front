<?php

class ValuesMapper
{

    private $shapes = array(
        'RD' => 'ROUND',
        'RBC' => 'ROUND',
        'BR' => 'ROUND',
        'OV' => 'OVAL',
        'PR' => 'PRINCESS',
        'MQ' => 'MARQUISE',
        'EM' => 'EMERALD',
        'CUSH' => 'CUSHION',
        'ASH' => 'ASSCHER',
        'HS' => 'HEART',
        'PS' => 'PEAR',
        'RAD' => 'RADIANT',
    );

    private $quality = array(
        'EX' => 'EXCELLENT',
        'VG' => 'VERY GOOD',
        'G' => 'GOOD',
        'GD' => 'GOOD',
        'F' => 'FAIR',
        'P' => 'POOR'
    );

    private $fluorescence = array(
        'N' => 'NONE',
        'NON' => 'NONE',
        'VSL' => 'VERY SLIGHT',
        'SL' => 'SLIGHT',
        'SLT' => 'SLIGHT',
        'F' => 'FAINT',
        'FNT' => 'FAINT',
        'M' => 'MEDIUM',
        'MED' => 'MEDIUM',
        'ST' => 'STRONG',
        'STR' => 'STRONG',
        'VST' => 'VERY STRONG',
        'VSTR' => 'VERY STRONG'
    );

    private $url = array(
        'N' => '',
        'NA' => '',
        'NULL' => '',
        'NON' => '',
        'NONE' => '',
        'DNA' => '',
        '' => '',
    );


    public function mapValue($header, $value)
    {
        switch ($header) {
            case 'SHAPE':
                return array_key_exists($value, $this->shapes) ? $this->shapes[$value] : $value;
            case 'CUT':
            case 'POLISH':
            case 'SYMMETRY':
                return array_key_exists($value, $this->quality) ? $this->quality[$value] : $value;
            case 'FLUORESCENCE':
                return array_key_exists($value, $this->fluorescence) ? $this->fluorescence[$value] : $value;
            case 'IMAGE URL':
                return array_key_exists($value, $this->url) ? $this->url[$value] : $value;
            default:
                return $value;
        }
    }
}
