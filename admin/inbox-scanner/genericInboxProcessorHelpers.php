<?php
/** @noinspection SpellCheckingInspection */
/** @noinspection PhpDeprecationInspection */

include "valuesMapper.php";

define('LOGS_PATH', "/home/dic/ftp/logs/");
define('DB_SERVER', 'sql.dic.nazwa.pl');
define('USERS_PATH', "/home/dic/ftp/common/users.dat");

// values for local
//define('DB_SERVER', '127.0.0.1:3306');
//define('USERS_PATH', "../../common/users.dat");
//define('LOGS_PATH', "../../logs/");

function connectToDatabase()
{
    $line = file(USERS_PATH);
    $login = '';
    $pass = '';
    foreach ($line as $temp) {
        $str = explode(",", $temp);
        $login = chop($str[0]);
        $pass = chop($str[1]);
    }
    $link = mysql_connect(DB_SERVER, $login, $pass)
    or die ("Nie można się połączyć: " . mysql_error());
    mysql_select_db("dic") or die ("Nie można wybrać bazy danych");
    return $link;
}

function connectToDatabaseImproved()
{
    $line = file(USERS_PATH);
    $login = '';
    $pass = '';
    foreach ($line as $temp) {
        $str = explode(",", $temp);
        $login = chop($str[0]);
        $pass = chop($str[1]);
    }
    $link = mysqli_connect(DB_SERVER, $login, $pass)
    or die ("Nie można się połączyć: " . mysqli_error($link));
    mysqli_select_db($link, "dic") or die ("Nie można wybrać bazy danych");
    return $link;
}

function readStonesFromFile($csvFilePath)
{
    $csv = file($csvFilePath);
    $stones = array();

    $lowPriceStonesCount = 0;
    $headers = getUppercasedValuesFromLine(array_shift($csv));
    $mapper = new ValuesMapper();

    foreach ($csv as $line) {
        $row = createEmptyRow();
        $fields = getUppercasedValuesFromLine($line);

        if (isEmpty($fields)) {
            continue;
        }

        for ($i = 0; $i < count($headers); $i++) {
            // correct fluorescence spelling or otherwise it might be
            // dismissed as unknown header
            if (isFluorescence($headers[$i])) {
                $headers[$i] = 'FLUORESCENCE';
            }
            // ingore fancy color and unknown headers
            if ($headers[$i] === 'FANCY COLOR' || !isset($row[$headers[$i]])) {
                continue;
            }


            $row[$headers[$i]] = $mapper->mapValue($headers[$i], $fields[$i]);
        }

        if ($row['TOTAL PRICE'] >= 200) {
            $stones[$row['CERTIFICATE NUMBER']] = $row;
        } else {
            $lowPriceStonesCount++;
        }
    }

    return array($stones, $lowPriceStonesCount);
}

function isEmpty($array)
{
    return count($array) === 0 || (count($array) === 1 && $array[0] === '');
}

function getCharacteristics()
{
    return array(
        'KAMIENIE',
        'KOLORY',
        'BARWY',
        'CZYSTOSCI',
        'KSZTALTY',
        'SYMETRIE',
        'SZLIFY',
        'POLEROWANIA',
        'FLUORESCENCJE',
        'LABORATORIA'
    );
}

function getFieldsArray()
{
    return array(
        'Symbol',
        'GroupId',
        'NettoUSD',
        'Netto',
        'Brutto',
        'RodzajZestawienia',
        'Masa',
        'Kamien',
//        'Kolor',
        'Barwa',
        'Czystosc',
        'Ksztalt',
        'Symetria',
        'Szlif',
        'Polerowania',
        'Fluorescencja',
        'Laboratorium',
        'StockNumber'
    );
}

function isFluorescence($name)
{
    return $name === 'FLUOROSCENCE' || $name === 'FLUORESCENCE' || $name === 'FLUORESENCE';
}

function createStatsArray($stonesCount, $lowPriceStonesCount)
{
    return array(
        'inFile' => $stonesCount,
        'inDatabase' => 0,
        'duplicates' => 0,
        'updated' => 0,
        'notUpdated' => 0,
        'prodZamUpd' => 0,
        'deleted' => 0,
        'notDeleted' => 0,
        'prodZamDel' => 0,
        'inserted' => 0,
        'notInserted' => 0,
        'lowPrice' => $lowPriceStonesCount
    );
}

function createEmptyRow()
{
    return array(
        'SELLER NAME' => '',
        'SHAPE' => '',
        'WEIGHT' => '',
        'COLOR' => '',
        'CLARITY' => '',
        'CUT' => '',
        'POLISH' => '',
        'SYMMETRY' => '',
        'FLUORESCENCE' => '',
        'LAB' => '',
        'CERTIFICATE NUMBER' => '',
        'STOCK NUMBER' => '',
        'TOTAL PRICE' => '',
        'IMAGE URL' => ''
    );
}

function getCheckProdTabs()
{
    return array(
//        'FANCY COLOR',
        'COLOR',
        'CLARITY',
        'SHAPE',
        'SYMMETRY',
        'CUT',
        'POLISH',
        'FLUORESCENCE',
        'LAB'
    );
}

function getCheckTabs()
{
    return array(
//        'KOLORY',
        'BARWY',
        'CZYSTOSCI',
        'KSZTALTY',
        'SYMETRIE',
        'SZLIFY',
        'POLEROWANIA',
        'FLUORESCENCJE',
        'LABORATORIA'
    );
}

function writeLogs($log, $statsArray, $unknownElements, $fileName)
{
    $log .= date('Y-m-d H:i:s') . " insert\n";
    $log .= "Statystyki: ";
    $separator = "";

    foreach ($statsArray as $key => $value) {
        $log .= $separator . "$key: $value";
        $separator = ", ";
    }

    $log .= ".\n";

    if (count($unknownElements)) {
        $separator = "";
        $log .= "Niezdefiniowane elementy w pliku: ";
        foreach ($unknownElements as $key => $value) {
            $log .= $separator . $key . ": ";
            $separator = "";
            foreach ($value as $key2 => $value2) {
                $log .= $separator . $key2 . (isset($value2['Seller Name']) ? " " . $value2['Seller Name'] : "") . "($value2[Count])";
                $separator = ", ";
            }
        }
        $log .= ".\n";
    }
    $log .= "\n";

    $files = fopen(LOGS_PATH . $fileName . ".log", "a");
    flock($files, 2);
    fputs($files, $log);
    flock($files, 3);
    fclose($files);
}

function trimAndUppercase($val)
{
    $trimmed = trim($val);

    // don't uppercase urls
    if (strpos($trimmed, 'http') === 0) {
        return $trimmed;
    }
    return strtoupper($trimmed);
}

function getUppercasedValuesFromLine($csvLine)
{
    return array_map("trimAndUppercase", explode(",", $csvLine));
}



function echoArray($arr)
{
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}
