<?php
// disabled create data source inspection
/** @noinspection DuplicatedCode */
/** @noinspection PhpDeprecationInspection */

error_reporting(E_ALL & ~E_NOTICE);
include "genericInboxProcessorHelpers.php";

define('INBOX_PATH', '/home/dic/ftp/inbox/');
//define('INBOX_PATH', '../../inbox/');

define('GROUP_ID', 'GroupId');
define('MULTIPLIER', 'Mnoznik');
define('ADDITIONAL_COST', 'KosztDodatkowy');
define('SUPPLIERS', 'DOSTAWCY');
define('TOTAL_PRICE', 'TOTAL PRICE');
define('STOCK_NUMBER', 'StockNumber');
define('NR', 'Nr');
define('SYMBOL', 'Symbol');

class GenericInboxProcessor
{
    private $vatMultiplier = 1.23;
    private $filePath;
    private $supplierId;
    private $inboxName;
    private $USDExchangeRate;
    private $unknownElements = array();

    public function __construct($supplierId, $inboxName, $USDExchangeRate)
    {
        $this->supplierId = $supplierId;
        $this->inboxName = $inboxName;
        $this->filePath = $this->createFilePath($inboxName);
        $this->USDExchangeRate = $USDExchangeRate;
    }

    public function processInbox()
    {
        echo $this->filePath;
//        $start = microtime(true);
        if ($this->filePath == null || !file_exists($this->filePath)) {
            echo "early return\n";
            return;
        }

        $log = date('Y-m-d H:i:s') . " START\n";
        list($stones, $lowPriceStonesCount) = readStonesFromFile($this->filePath);
//        return;
        $stonesCount = count($stones);
        $link = connectToDatabaseImproved();
//        $dbConnection = connectToDatabase();

        $stoneCharacteristics = getCharacteristics();

        foreach ($stoneCharacteristics as $characteristic) {
            $query = "SELECT * FROM $characteristic";
            $result = mysqli_query($link, $query) or die ('56 '.mysqli_error($link));
            while ($line = mysqli_fetch_array($result, MYSQL_ASSOC)) {
                $stoneCharacteristics[$characteristic][strtoupper($line['Nazwa'])] = $line['Id'];
            }
        }

        echo "stones read\n";

        $query = $this->createSuppliersQuery();
        $result = mysqli_query($link, $query) or die ('63 '.mysqli_error($link));
        while ($line = mysqli_fetch_array($result, MYSQL_ASSOC)) {
            $stoneCharacteristics[SUPPLIERS][$line[NR]] = array(
                NR => $line[NR],
                GROUP_ID => $line[GROUP_ID],
                MULTIPLIER => $line[MULTIPLIER],
                ADDITIONAL_COST => $line[ADDITIONAL_COST]
            );
        }

        $stats = createStatsArray($stonesCount, $lowPriceStonesCount);
        $flds = getFieldsArray();

        $log .= date('Y-m-d H:i:s') . " baza i opcje\n";

        $query = "SELECT * FROM PRODUKTY WHERE RodzajZestawienia = $this->supplierId";
        $result = mysqli_query($link, $query) or die ('79 '.mysqli_error($link));
        $symbolDel = "";
        $countDel = 0;
        $prodIdDel = array();
        $separator = "";
        $stockNumberDel = '';
//        $timestamp1 = microtime(true) - $start;

        echo "pre-while \n";

        while ($line = mysqli_fetch_array($result, MYSQL_ASSOC)) {
            $stats['inDatabase']++;
            if (isset($stones[$line[SYMBOL]])) {
                $nettoUSD = $stones[$line[SYMBOL]][TOTAL_PRICE];
                if (round($nettoUSD, 2) != $line['NettoUSD']) {
                    $query2 = $this->createUpdatePricesQuery($nettoUSD, $line[SYMBOL]);
                    $result2 = mysqli_query($link, $query2) or die ('92 '.$query2);
                    $stats['updated']++;
                } else {
                    $stats['notUpdated']++;
                }
                unset($stones[$line[SYMBOL]]);
            } else {
                $symbolDel .= $separator . "'" . $line['Symbol'] . "'";
                $stockNumberDel .= $separator . "'" . $line[SYMBOL] . "'";
                $separator = ",";
                $prodIdDel[] = $line['Id'];
                $countDel++;
            }
        }

        echo "post-while\n";
//        $timestamp2 = microtime(true) - $timestamp1;
        if ($stats['updated']) {
            $query =
                "UPDATE 
                    PRODUKTY p, 
                    PRODUKTYZAM pz 
                SET pz.Brutto = p.Brutto, 
                    pz.Netto = p.Netto 
                WHERE pz.Symbol = p.Symbol 
                    AND IdZam IN 
                        (SELECT Id FROM ZAMOWIENIA WHERE Zatwierdzone = 0)";

            $result = mysqli_query($link, $query) or die ('118 '.mysqli_error($link));

            $stats['prodZamUpd'] = mysqli_affected_rows($link);
        }

        echo "132\n";
//        $timestamp3 = microtime(true) - $timestamp2;

        $countAll = 0;
        if ($countDel) {
            $query =
                "DELETE FROM PRODUKTYZAM 
                    WHERE Symbol IN($symbolDel) 
                        AND IdZam IN 
                            (SELECT Id FROM ZAMOWIENIA WHERE Zatwierdzone = 0)";

            $result = mysqli_query($link, $query) or die ('132 '.mysqli_error($link));
            $stats['prodZamDel'] = mysqli_affected_rows($link);
            $query =
                "DELETE FROM PRODUKTY 
                    WHERE StockNumber IN($stockNumberDel) 
                        AND Symbol
                                NOT IN (SELECT Symbol FROM PRODUKTYZAM) 
                        AND Id 
                                NOT IN (SELECT IdProd FROM TOWAR)";

            $result = mysqli_query($link, $query) or die ('141 '.mysqli_error($link));
            $stats['deleted'] = mysqli_affected_rows($link);
            $stats['notDeleted'] = $countDel - $stats['deleted'];
        }

        echo "171\n";
        $log .= date('Y-m-d H:i:s') . " update i delete" . ($countAll ? " (allegro del: $countAll)" : "") . "\n";

//        $timestamp4 = microtime(true) - $timestamp3;


        $checktabs = getCheckTabs();
        $checkprod = getCheckProdTabs();

        $sliceLength = 1000;
        $stonesCount = count($stones);
        $sliceCount = ceil($stonesCount / $sliceLength);

        echo "pre-for\n";

        for ($i = 0; $i < $sliceCount; $i++) {
            $slice = array_slice($stones, $i * $sliceLength, $sliceLength);
            $q = "INSERT INTO PRODUKTY (Symbol, GroupId, NettoUSD, Netto, Brutto, RodzajZestawienia, Masa, Kamien, Barwa, Czystosc, Ksztalt, Symetria, Szlif, Polerowanie, Fluorescencja, Laboratorium, StockNumber)
                  VALUES ";
            foreach ($slice as $stone) {

                if (!$this->isValidStone($stone, $checktabs, $checkprod, $stoneCharacteristics)) {
                    $stats['notInserted']++;
                    continue;
                }

                $brutto = $this->calcStonePriceBrutto($stone, $stoneCharacteristics[SUPPLIERS]);
                $netto = round($brutto / $this->vatMultiplier, 2);
                $fluorescence = strlen($stoneCharacteristics['FLUORESCENCJE'][$stone['FLUORESCENCE']]) > 0 ? $stoneCharacteristics['FLUORESCENCJE'][$stone['FLUORESCENCE']] : "NULL";
                $values =
                    "'" . $stone['CERTIFICATE NUMBER'] . "', " .
                    $stoneCharacteristics[SUPPLIERS][$this->supplierId][GROUP_ID] . ", " .
                    $stone[TOTAL_PRICE] . ", " .
                    $netto . ", " .
                    $brutto . ", " .
                    $stoneCharacteristics[SUPPLIERS][$this->supplierId][NR] . ", " .
                    $stone['WEIGHT'] . ", " .
                    1 . ", " .
                    $stoneCharacteristics['BARWY'][$stone['COLOR']] . ", " .
                    $stoneCharacteristics['CZYSTOSCI'][$stone['CLARITY']] . ", " .
                    $stoneCharacteristics['KSZTALTY'][$stone['SHAPE']] . ", " .
                    $stoneCharacteristics['SYMETRIE'][$stone['SYMMETRY']] . ", " .
                    $stoneCharacteristics['SZLIFY'][$stone['CUT']] . ", " .
                    $stoneCharacteristics['POLEROWANIA'][$stone['POLISH']] . ", " .
                    "$fluorescence, " .
                    $stoneCharacteristics['LABORATORIA'][$stone['LAB']] . ", " .
                    "'" . $stone['STOCK NUMBER'] . "'"; // different than const on purpose
                if (!strpos($values, ', ,')) {
                    $q .= '(' . $values . '), ';
                }

            }
            $q = rtrim($q, ', ');

            if (endsWith($q, "VALUES")) {
                continue;
            }
            $q .= "ON DUPLICATE KEY UPDATE Kamien = 1;";
            $result = mysqli_query($link, $q) or die ('208 ' .  serialize($slice) . mysqli_error($link) . "\n<br>" . $q);
            if (!$result) {

                $stats['duplicates'] += count($slice);
                $log .= mysqli_error($link) . "\n";
            }

            $stats['inserted'] += count($slice);

        }
        echo "post-for\n";
//        $timestamp5 = microtime(true) - $timestamp4;

        $this->logChanges($log, $stats, $this->unknownElements);
        mysqli_close($link);

//        echo "$timestamp1 \n";
//        echo "$timestamp2 \n";
//        echo "$timestamp3 \n";
//        echo "$timestamp4 \n";
//        echo "$timestamp5 \n";
    }

    private function isValidStone($stone, $checktabs, $checkprod, $stoneCharacteristics)
    {
        for ($i = 0; $i < count($checktabs); $i++) {
            if ($stone[$checkprod[$i]] && !$stoneCharacteristics[$checktabs[$i]][$stone[$checkprod[$i]]]) { // true == is not okay
                $this->unknownElements[$checktabs[$i]][$stone[$checkprod[$i]]]['Count']++;
                return false;
            }
        }
        return true;
    }

    private function createInsertQuery($values, $flds)
    {
        $query = '';
        for ($i = 0; $i < count($flds); $i++) {
            if ($values[$i] == "") {
                continue;
            }
            $field = $flds[$i] === 'Polerowania' ? 'Polerowanie' : $flds[$i];
            $query .= ($i ? ", " : "") . $field . "='" . $values[$i] . "'";
        }
        return $query;
    }

    private function calcStonePriceBrutto($stone, $suppliers)
    {
        $supplierMultiplier = $suppliers[$this->supplierId][MULTIPLIER];
        $supplierAdditionalCost = $suppliers[$this->supplierId][ADDITIONAL_COST];
        return round($stone[TOTAL_PRICE] * $supplierMultiplier * $this->USDExchangeRate * $this->vatMultiplier + $supplierAdditionalCost, 2);
    }

    private function logChanges($log, $stats, $unknownElements)
    {
        writeLogs($log, $stats, $unknownElements, $this->inboxName);
    }

    private function createUpdatePricesQuery($nettoUSD, $stockNumber)
    {
        return
            "UPDATE 
                PRODUKTY p, 
                GRUPYCEN g 
            SET 
                p.NettoUSD = $nettoUSD, 
                p.Brutto = $nettoUSD * g.Mnoznik * $this->USDExchangeRate * $this->vatMultiplier + g.KosztDodatkowy, 
                p.Netto = $nettoUSD * g.Mnoznik * $this->USDExchangeRate + g.KosztDodatkowy / $this->vatMultiplier 
            WHERE 
                p.StockNumber = '$stockNumber' 
                AND p.GroupId = g.Id 
                AND g.Dolar = 1";
    }

    private function createSuppliersQuery()
    {
        return
            "SELECT
                z.Nr, 
                z.RapNetId,
                z.GroupId,
                g.Id,
                g.Mnoznik,
                g.KosztDodatkowy
            FROM ZESTRODZ z 
                LEFT JOIN
                    GRUPYCEN g
                    ON g.Id=z.GroupId 
            WHERE z.Nr = $this->supplierId
                AND z.GroupId
                AND g.Dolar";
    }

    private function createFilePath($dirName)
    {
        $supplierPath = INBOX_PATH . $dirName;

        if (!is_dir($supplierPath))
            return null;

        $files = glob($supplierPath . "/*.csv");
        if (count($files) <= 0)
            return null;

        $fileSize = 0;
        $filePath = '';

        foreach ($files as $file) {
            $size = filesize($file);

            if ($size > $fileSize) {
                $fileSize = $size;
                $filePath = $file;
            }
        }

        if (stripos($filePath, "sample.csv") !== false) {
            return null;
        }

        return $filePath;
    }
}

function endsWith( $haystack, $needle ) {
    $length = strlen( $needle );
    if( !$length ) {
        return true;
    }
    return substr( $haystack, -$length ) === $needle;
}

