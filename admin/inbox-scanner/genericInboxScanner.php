<?php
include "genericInboxProcessor.php";
ini_set('memory_limit', '2048M');
set_time_limit(3600);

//define('USD_RATE_PATH', "../../common/kursUSD.txt");
define('USD_RATE_PATH', "/home/dic/ftp/common/kursUSD.txt");

function getUSDExchangeRate()
{
    $fh = fopen(USD_RATE_PATH, 'r');
    $rate = fread($fh, filesize(USD_RATE_PATH));
    fclose($fh);
    return $rate;
}

// array of supplierId => inboxName
function getSuppliersArray()
{
    return array(
        1 => 'gutfreund',
        3 => 'igc',
        10 => 'beckdiamonds',
        13 => 'kpsangvi',
        15 => 'darshit',
        16 => 'apshaps',
        17 => 'amc',
        19 => 'aspeco',
        21 => 'meirowklink',
        23 => 'polirough',
        24 => 'rubinzonen',
        26 => 'supergems',
        27 => 'tristar',
    );
}

function scanSupplierInbox($supplierId)
{
    $usdRate = getUSDExchangeRate();
    $suppliers = getSuppliersArray();
    $supplierInboxName = $suppliers[$supplierId];
    $processor = new GenericInboxProcessor($supplierId, $supplierInboxName, $usdRate);
    $processor->processInbox();
}
