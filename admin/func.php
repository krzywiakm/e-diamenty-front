<?php
include "db.php";
#definicje stalych
define("DATA", "../common/users.dat");
define("SESID", SESSION_NAME() . "=" . SESSION_ID());

function beep($x)
{
    switch($x)
        {
        case 1: $f="../common/start.wav";   break;
        case 2: $f="../common/psycho.wav";  break;
        case 3: $f="../common/error.wav";   break;
        case 4: $f="../common/tada.wav";    break;
        }
    
    print "<EMBED src=\"$f\" autostart=true loop=false volume=100 hidden=true><NOEMBED><BGSOUND src=\"$f\"></NOEMBED>";
}

function get_url_contents($url)
    {
    $crl = curl_init();
    $timeout = 1;
    curl_setopt ($crl, CURLOPT_URL,$url);
    curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
    $ret = curl_exec($crl);
    $err = curl_errno($crl);
    curl_close($crl);
    if($err)
        $ret=$err;
    return $ret;
    }

function getUstawienia($pole)
{
    $query = "SELECT Wartosc FROM USTAWIENIA where Pole='$pole'";
	$result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
	$row = mysql_fetch_array($result, MYSQL_ASSOC);
	
	return $row[Wartosc];
}

function setUstawienia($pole,$wartosc)
{
    $query = "UPDATE USTAWIENIA set Wartosc='$wartosc' where Pole='$pole'";
	$result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
}

function kolortlaBlue($i)
{
	if($i%3==0)
		return "bgcolor='#0000BB'";
 	else if($i%3==2)
 		return "bgcolor='#0000FF'";
 	else
 		return "bgcolor='#0000DD'";
}

function kolortlaGreen($i)
{
	if($i%3==0)
		return "bgcolor='#00BB00'";
 	else if($i%3==2)
 		return "bgcolor='#00FF00'";
 	else
 		return "bgcolor='#00DD00'";
}

function kolortlaRed($i)
{
	if($i%3==0)
		return "bgcolor='#BB0000'";
 	else if($i%3==2)
 		return "bgcolor='#FF0000'";
 	else
 		return "bgcolor='#DD0000'";
}

function kolortla($i)
{
	if($i%3==0)
		return "bgcolor='#DDDDDD'";
 	else if($i%3==2)
 		return "bgcolor='#FFFFFF'";
 	else
 		return "bgcolor='#EEEEEE'";
}

function wyswietlpodkons($login, $level)
{
	$query = "SELECT * FROM USERS where not Klient and LoginPolec='".$login."' order by Nazwisko ";
	$result = mysql_query ($query) or die ("Zapytanie zako�czone niepowodzeniem");

	while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
 	 	{
		for($i=0; $i<$level; $i++)
			print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

   		print "<a href='index.php?page=lista&id=".$line[Id]."'>$line[Login], $line[Imie] $line[Nazwisko], $line[Miejscowosc]</a><br>";

     	wyswietlpodkons($line[Login], $level+1);
	  	}

	mysql_free_result($result);
}

function slownik($i,$l)
{       
    switch($l)
        {
        case "EN":  $lfield="En";   break;
        case "CZ":  $lfield="Cz";   break;
        case "CN":  $lfield="Cn";   break;
        default:    $lfield="Pl";   break;
        }
        
    $query = "SELECT $lfield FROM SLOWNIK where Id=$i";
	$result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
	$row = mysql_fetch_array($result, MYSQL_ASSOC);
	return $row[$lfield];
}

function slownik2($i)
{       
    $query = "SELECT Pl FROM SLOWNIK2 where Id=$i";
	$result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
	$row = mysql_fetch_array($result, MYSQL_ASSOC);
	return $row[Pl];
}

function slownik2i($i,$l)
{      
    switch($l)
        {
        case "EN":  $lfield="En";   break;
        case "CZ":  $lfield="Cz";   break;
        case "CN":  $lfield="Cn";   break;
        default:    $lfield="Pl";   break;
        }
         
    $query = "SELECT $lfield FROM SLOWNIK2 where Id=$i";
	$result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
	$row = mysql_fetch_array($result, MYSQL_ASSOC);
	return $row[$lfield];
}


include "../common/func.php";   //opis


function nicepriceCZK($k)
{
        $bruttoRound = ceil($k);
 	    
 	    if($bruttoRound<10000)
 	        $bruttoRound+=10-$bruttoRound%10;
 	    else
 	        $bruttoRound+=100-$bruttoRound%100;
 	        
 	    return $bruttoRound;
}

function prodlog($zmiana)
{    
    $zmianaPL = $zmiana;    
    $zmianaPL = str_replace("[D]", slownik2i(21,"PL"), $zmianaPL);
    $zmianaPL = str_replace("[P]", slownik2i(22,"PL"), $zmianaPL);   
    $zmianaPL = str_replace("[P2]", slownik2i(23,"PL"), $zmianaPL);   
    $zmianaPL = str_replace("[C]", slownik2i(24,"PL"), $zmianaPL);   
    $zmianaPL = str_replace("[K]", slownik2i(25,"PL"), $zmianaPL);   
    $zmianaPL = str_replace("[K2]", slownik2i(26,"PL"), $zmianaPL);   
    $zmianaPL = str_replace("[U]", slownik2i(27,"PL"), $zmianaPL);      
    $zmianaPL = str_replace("[4T]", slownik2i(28,"PL"), $zmianaPL);   
    $zmianaPL = str_replace("[4T2]", slownik2i(29,"PL"), $zmianaPL);
    
    $zmianaCZ = $zmiana;
    $zmianaCZ = str_replace("[D]", slownik2i(21,"CZ"), $zmianaCZ);
    $zmianaCZ = str_replace("[P]", slownik2i(22,"CZ"), $zmianaCZ);   
    $zmianaCZ = str_replace("[P2]", slownik2i(23,"CZ"), $zmianaCZ);   
    $zmianaCZ = str_replace("[C]", slownik2i(24,"CZ"), $zmianaCZ);   
    $zmianaCZ = str_replace("[K]", slownik2i(25,"CZ"), $zmianaCZ);   
    $zmianaCZ = str_replace("[K2]", slownik2i(26,"CZ"), $zmianaCZ);   
    $zmianaCZ = str_replace("[U]", slownik2i(27,"CZ"), $zmianaCZ);      
    $zmianaCZ = str_replace("[4T]", slownik2i(28,"CZ"), $zmianaCZ);   
    $zmianaCZ = str_replace("[4T2]", slownik2i(29,"CZ"), $zmianaCZ);
    
    $zmianaEN = $zmiana;
    $zmianaEN = str_replace("[D]", slownik2i(21,"EN"), $zmianaEN);
    $zmianaEN = str_replace("[P]", slownik2i(22,"EN"), $zmianaEN);   
    $zmianaEN = str_replace("[P2]", slownik2i(23,"EN"), $zmianaEN);   
    $zmianaEN = str_replace("[C]", slownik2i(24,"EN"), $zmianaEN);   
    $zmianaEN = str_replace("[K]", slownik2i(25,"EN"), $zmianaEN);   
    $zmianaEN = str_replace("[K2]", slownik2i(26,"EN"), $zmianaEN);   
    $zmianaEN = str_replace("[U]", slownik2i(27,"EN"), $zmianaEN);      
    $zmianaEN = str_replace("[4T]", slownik2i(28,"EN"), $zmianaEN);   
    $zmianaEN = str_replace("[4T2]", slownik2i(29,"EN"), $zmianaEN);
    
    $zmianaCN = $zmiana;
    $zmianaCN = str_replace("[D]", slownik2i(21,"CN"), $zmianaCN);
    $zmianaCN = str_replace("[P]", slownik2i(22,"CN"), $zmianaCN);   
    $zmianaCN = str_replace("[P2]", slownik2i(23,"CN"), $zmianaCN);   
    $zmianaCN = str_replace("[C]", slownik2i(24,"CN"), $zmianaCN);   
    $zmianaCN = str_replace("[K]", slownik2i(25,"CN"), $zmianaCN);   
    $zmianaCN = str_replace("[K2]", slownik2i(26,"CN"), $zmianaCN);   
    $zmianaCN = str_replace("[U]", slownik2i(27,"CN"), $zmianaCN);      
    $zmianaCN = str_replace("[4T]", slownik2i(28,"CN"), $zmianaCN);   
    $zmianaCN = str_replace("[4T2]", slownik2i(29,"CN"), $zmianaCN);
    
    $ilearg=func_num_args();
    $argtab=func_get_args();

    if($ilearg>1)
        {
        $zmianaPL = $zmianaPL." ".$argtab[2]." -> ".$argtab[1]." PLN";
        $zmianaEN = $zmianaEN." ".$argtab[2]." -> ".$argtab[1]." PLN";
        $zmianaCN = $zmianaCN." ".$argtab[2]." -> ".$argtab[1]." PLN";
        $zmianaCZ = $zmianaCZ." ".nicepriceCZK($argtab[2]/kurs("CZK"))." -> ".nicepriceCZK($argtab[1]/kurs("CZK"))." CZK";
        }
        
    $files = fopen("/home/dnmk/ftp/outbox/prod.log", "a");
    flock($files, 2);
    fputs($files, $zmianaPL."\n");
    flock($files, 3);
    fclose($files);
    
    $files = fopen("/home/dnmk/ftp/outbox/prodCZ.log", "a");
    flock($files, 2);
    fputs($files, $zmianaCZ."\n");
    flock($files, 3);
    fclose($files);
    
    $files = fopen("/home/dnmk/ftp/outbox/prodEN.log", "a");
    flock($files, 2);
    fputs($files, $zmianaEN."\n");
    flock($files, 3);
    fclose($files);
    
    $files = fopen("/home/dnmk/ftp/outbox/prodCN.log", "a");
    flock($files, 2);
    fputs($files, $zmianaCN."\n");
    flock($files, 3);
    fclose($files);
}

function kurs($w)
{
    if($w=="PLN")
        return "1";    
    
    $myFile = "../common/kurs$w.txt";
	$fh = fopen($myFile, 'r');
	$theData = fread($fh, filesize($myFile));
	fclose($fh);
	return $theData;
}

function pobierzKodKreskowy()
{
    $kod = getUstawienia("LastBarCode");
    $kod++;
    setUstawienia("LastBarCode",$kod);
    $skod="$kod";
    $suma=0;
    for($i=0; $i<12; $i++)
        $suma+=$skod[$i]*($i%2==1?3:1);
    $cyfra=10-$suma%10<10 ? 10-$suma%10 : 0;
    return $kod.$cyfra;
}

function pobierzNumerFak($rodz)
{
    $nr = getUstawienia("LastFakNr$rodz");
    $nr++;
    setUstawienia("LastFakNr$rodz",$nr);
    return "F$rodz/$nr/".getUstawienia("RokFak");
}

function pobierzNumerPar()
{
    $nr = getUstawienia("LastParNr");
    $nr++;
    setUstawienia("LastParNr",$nr);
    return "$nr/".getUstawienia("RokFak");
}

function getYesIndex($symbol)
{
//EI01000-I0000-DIW000-A08
    $query = "SELECT p.Id, p.Symbol, p.Brutto, p.NettoUSD, p.Masa, p.Idealny, k.Nazwa Kamien, ks.Nazwa Ksztalt, ko.Nazwa Kolor, b.Nazwa Barwa, c.Nazwa Czystosc,
            s.Nazwa Szlif, sy.Nazwa Symetria, po.Nazwa Polerowanie, f.Nazwa Fluorescencja, 
            l.Nazwa Laboratorium, z.Czas, z.Nazwa Dostawca FROM PRODUKTY p 
                left join KAMIENIE k on p.Kamien=k.Id
                left join KSZTALTY ks on p.Ksztalt=ks.Id
                left join KOLORY ko on p.Kolor=ko.Id
                left join BARWY b on p.Barwa=b.Id
                left join CZYSTOSCI c on p.Czystosc=c.Id
                left join SZLIFY s on p.Szlif=s.Id
                left join SYMETRIE sy on p.Symetria=sy.Id
                left join POLEROWANIA po on p.Polerowanie=po.Id
                left join FLUORESCENCJE f on p.Fluorescencja=f.Id
                left join LABORATORIA l on p.Laboratorium=l.Id 
                left join ZESTRODZ z on p.RodzajZestawienia=z.Nr
                where p.Symbol='$symbol'";
    $result = mysql_query ($query) or die ("Zapytanie zako�czone niepowodzeniem 4");
    $line = mysql_fetch_array($result, MYSQL_ASSOC);
    
    $idealny=$line[Idealny]?"14":"01";
    
    $czyst="?";
    
    if(!$line[Idealny])
        switch($line[Czystosc])
            {
            case "IF":   $czyst="A"; break;
            case "VVS1":
            case "VVS2": $czyst="B"; break;
            case "VS1":
            case "VS2":  $czyst="C"; break;
            case "SI1":
            case "SI2":  $czyst="D"; break;
            case "I1":   $czyst="E"; break;
            case "I2":   $czyst="F"; break;
            case "I3":   $czyst="G"; break;
            }
    else
        switch($line[Czystosc])
            {
            case "VS1":
            case "VS2":  $czyst="V"; break;
            case "SI1":
            case "SI2":  $czyst="Y"; break;
            }
            
    $m=$line[Masa];
    $masa="00";
    
    if($m==0.01)
        $masa="01";
    else if($m==0.02)
        $masa="02";
    else if($m>=0.03 && $m<=0.04)
        $masa="03";
    else if($m==0.05)
        $masa="05";
    else if($m==0.06)
        $masa="06";
    else if($m==0.07)
        $masa="07";
    else if($m>=0.08 && $m<=0.09)
        $masa="08";
    else if($m>=0.10 && $m<=0.13)
        $masa="10";
    else if($m>=0.14 && $m<=0.19)
        $masa="14";
    else if($m>=0.20 && $m<=0.24)
        $masa="20";
    else if($m>=0.25 && $m<=0.32)
        $masa="25";
    else if($m>=0.33 && $m<=0.39)
        $masa="33";
    else if($m>=0.40 && $m<=0.49)
        $masa="40";
    else if($m>=0.50 && $m<=0.59)
        $masa="50";
    else if($m>=0.60 && $m<=0.69)
        $masa="60";
    else if($m>=0.70 && $m<=0.74)
        $masa="70";
    else if($m>=0.75 && $m<=0.89)
        $masa="75";
    else if($m>=0.90 && $m<=0.99)
        $masa="90";
    else if($m>=1)
        $masa=chr(64+floor($m)).(floor(($m-floor($m)+0.0000001)*10));
    
    return "EI".$idealny."000-I0000-DIW000-".$czyst.$masa;
}

function kropek($string)
{
	return strtr($string,",",".");
}

function przecinek($string)
{
	return strtr($string,".",",");
}

function currency($kwota)
{
	$blad=0.001;
	if($kwota<0)
		$blad=-0.001;
 	$zloty = (int)($kwota+$blad);
 	$grosz = round(abs($kwota-$zloty)*100);
 	$string = $zloty.".".$grosz;
 	if($grosz<10)
 		$string = $zloty.".0".$grosz;
 	if($kwota<0 && $zloty==0)
 	    $string = "-".$string;
 	return $string;
}

function taknie($zerojeden)
{
	if($zerojeden)
		return "Tak";
	else
		return "Nie";
}

function woj($w)
{
	$ww='';
	switch ($w)
		{
		case '1': $ww='dolno�l�skie'; break;
		case '2': $ww='kujawsko-pomorskie'; break;
		case '3': $ww='lubelskie'; break;
		case '4': $ww='lubuskie'; break;
		case '5': $ww='��dzkie'; break;
		case '6': $ww='ma�opolskie'; break;
		case '7': $ww='mazowieckie'; break;
		case '8': $ww='opolskie'; break;
		case '9': $ww='podkarpackie'; break;
		case '10': $ww='podlaskie'; break;
		case '11': $ww='pomorskie'; break;
		case '12': $ww='�l�skie'; break;
		case '13': $ww='�wi�tokrzyskie'; break;
		case '14': $ww='warmi�sko-mazurskie'; break;
		case '15': $ww='wielkopolskie'; break;
		case '16': $ww='zachodniopomorskie'; break;
		}
	return $ww;
}

function woj2($w)
{
	$ww='';
	switch ($w)
		{
		case '1': $ww='D�'; break;
		case '2': $ww='KP'; break;
		case '3': $ww='LB'; break;
		case '4': $ww='LS'; break;
		case '5': $ww='�D'; break;
		case '6': $ww='MP'; break;
		case '7': $ww='MZ'; break;
		case '8': $ww='OP'; break;
		case '9': $ww='PK'; break;
		case '10': $ww='PL'; break;
		case '11': $ww='PM'; break;
		case '12': $ww='�L'; break;
		case '13': $ww='�W'; break;
		case '14': $ww='WM'; break;
		case '15': $ww='WP'; break;
		case '16': $ww='ZP'; break;
		}
	return $ww;
}

function kraj($w)
{
	$ww='';
	switch ($w)
		{
		case '0': $ww='Polska'; break;
		case '1': $ww='Czechy'; break;
		case '2': $ww='S�owacja'; break;
		case '3': $ww='Wielka Brytania'; break;
		case '99': $ww='inny'; break;
		}
	return $ww;
}

function tekst($string)
{
  //$string = strtr($string, "\xA5\x8C\x8F\xB9\x9C\x9F","\xA1\xA6\xAC\xB1\xB6\xBC");
  print "$string";
}

function currtime()
{
  $timeres = mysql_query("SELECT CURTIME()");
  $timeline = mysql_fetch_array($timeres);
  return $timeline[0];
}

function currdate()
{
  $datares = mysql_query("SELECT CURDATE()");
  $dataline = mysql_fetch_array($datares);
  return $dataline[0];
}

function dblogin()
{
$logindb = "";
$passwdb = "";
$line = file(DATA);
  foreach($line as $temp)
  {
    $str = explode(",", $temp);
    $logindb = chop($str[0]);
    $passwdb = chop($str[1]);
  }
$link = mysql_connect(DB, $logindb, $passwdb)
 or die ("Nie mo�na si� po��czy�: " . mysql_error());

mysql_select_db ("dic") or die ("Nie mozna wybra� bazy danych");

return $link;
}


# zaloguj user-a
function login($login, $passwd)
{
  $link = dblogin();

  $query = "SELECT * FROM ADMIN WHERE Login='" . $login . "' and Haslo='" . $passwd . "' AND (type = 'all' OR type = 'pl')";
  $result = mysql_query ($query) or die ("Zapytanie zako�czone niepowodzeniem");
  $ile = mysql_num_rows($result);
  $line = mysql_fetch_array($result);
  mysql_free_result($result);
  mysql_close($link);

  if($ile<1)
    {
    $login = htmlentities($login);
    return False;
    }
  else
    {
    if(!strcmp($login,$line[Login]) && !strcmp($passwd,$line[Haslo]))
    	{
    	$_SESSION["USER_AUTH"]  = True;
    	$_SESSION["USER_LOGIN"] = $_POST["login"];
        $_SESSION["USER_ROLE"] = $line[role];
        return True;
        }
    else
        {
        $login = htmlentities($login);
        return False;
        }
    }
}






# wyloguj user-a
function logout()
{
  $_SESSION["USER_AUTH"]  = False;
  $_SESSION["USER_LOGIN"] = Null;
  $_SESSION["USER_ROLE"] = false;
}


/*

# czy u�ytkownik istnieje
function user_exists($login)
{
  $line = file(DATA);
  foreach($line as $temp)
  {
    $str = explode(",", $temp);
    if(chop($str[0])==$login) return True;
  }
  return False;
}




# dodaje nowego u�ytkownika
function add_user($login, $passwd)
{
  $files = fopen(DATA, "a");
  flock($files, 2);
  fputs($files, $login . "," . $passwd . "\n");
  flock($files, 3);
  fclose($files);
}

*/


# sprawdza czy zalogowany
function auth()
{
    if($_SESSION["USER_AUTH"])
        {
        $page=$_POST[page];
        if($page=="")
	        $page=$_GET[page];
	        
        switch($_SESSION["USER_ROLE"])
            {
            case "admin":
                return true;
            case "employee":
                if(basename($_SERVER[PHP_SELF])=="index.php")
                    {
                    switch($page)
                        {
                        case "faktury": case "raport":
                        case "login": case "logout": case "haslo":
                            return true;
                        default:
                            return false;
                        }
                    }
                elseif(basename($_SERVER[PHP_SELF])=="drukfaktura.php")
                    return true;
                else 
                    return false;
            }
        }
    else 
        return false;
    
//  return ($_SESSION["USER_AUTH"] == True);
}



?>
