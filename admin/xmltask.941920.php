<?php

function kurs($w)
{
    if($w=="PLN")
        return "1";    
    
    $myFile = "/home/dnmk/ftp/common/kurs$w.txt";
	$fh = fopen($myFile, 'r');
	$theData = fread($fh, filesize($myFile));
	fclose($fh);
	return $theData;
}

function nicepriceCZK($k)
{
    $bruttoRound = ceil($k);
    
    if($bruttoRound<10000)
        $bruttoRound+=10-$bruttoRound%10;
    else
        $bruttoRound+=100-$bruttoRound%100;
        
    return $bruttoRound;
}

function nicepriceUSD($k)
{
    $bruttoRound = ceil($k);
    
    if($bruttoRound>3000)
        $bruttoRound+=100-$bruttoRound%100;
        
    return $bruttoRound;
}

function slownik($i,$l)
{       
    switch($l)
        {
        case "EN":  $lfield="En";   break;
        case "CZ":  $lfield="Cz";   break;
        case "CN":  $lfield="Cn";   break;
        default:    $lfield="Pl";   break;
        }
        
    $query = "SELECT $lfield FROM SLOWNIK where Id=$i";
	$result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
	$row = mysql_fetch_array($result, MYSQL_ASSOC);
	return $row[$lfield];
}

function createxml($lang,$waluta,$plik,$exclusive)
{    
    switch($lang)
        {            
        case "CZ":  $lpole="Cz1"; $lopis="OpisCZ"; break;
        case "EN":  $lpole="En1"; $lopis="OpisEN"; break;
        case "PL":  $lpole="Pl1"; $lopis="Opis";   break;
        }
    
    $tresc="<?xml version=\"1.0\" encoding=\"ISO-8859-2\"?>\n<katalog>\n";
    //$trescCSV="marka;model;dlugosc;nazwa;cena;cena_promocyjna;vat;kategoria;kamien;metal;opis;masa;dostepnosc;dostepnoscopis;foto\n";
    //$trescCSV="id;".slownik(478,$lang).";".slownik(479,$lang).";".slownik(481,$lang).";".slownik(648,$lang).";".slownik(482,$lang).";".slownik(483,$lang).";".slownik(484,$lang).";".slownik(480,$lang).";".slownik(486,$lang).";".slownik(487,$lang).";".slownik(488,$lang).";".slownik(489,$lang).";".slownik(490,$lang).";".slownik(646,$lang).";".slownik(491,$lang)."\n";
    $trescCSV="\"id\";\"".slownik(478,$lang)."\";\"".slownik(479,$lang)."\";\"".slownik(481,$lang)."\";\"".slownik(648,$lang)."\";\"".slownik(482,$lang)."\";\"".slownik(483,$lang)."\";\"".slownik(484,$lang)."\";\"".slownik(480,$lang)."\";\"kamen1\";\"kamen1_qty\";\"kamen1_carat\";\"kamen1_clarity\";\"kamen1_color\";\"kamen2\";\"kamen2_qty\";\"kamen2_carat\";\"kamen2_clarity\";\"kamen2_color\";\"".slownik(487,$lang)."\";\"Certificate\";\"Size\";\"".slownik(488,$lang)."\";\"".slownik(489,$lang)."\";\"".slownik(490,$lang)."\";\"".slownik(646,$lang)."\";\"".slownik(491,$lang)."\"\n";
    $query = "SELECT * FROM KATRODZAJE";
    $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
    $kategorie = array(array());
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
        {
        $kategorie["PL"][$line[Nr]]=$line[Nazwa];
        $kategorie["CZ"][$line[Nr]]=$line[NazwaCZ];
        $kategorie["EN"][$line[Nr]]=$line[NazwaEN];
        }
        
    $query = "SELECT * FROM KATTYPY where NrRodz<100";
    $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
    $podkategorie = array(array());
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
        {
        $i=$line[NrRodz]*100+$line[Nr];
        $podkategorie["PL"][$i]=$line[Nazwa];
        $podkategorie["CZ"][$i]=$line[NazwaCZ];
        $podkategorie["EN"][$i]=$line[NazwaEN];
        }    
        
    $query = "SELECT * FROM PRODUKTY WHERE Pokaz=1 and not Tymczasowy and not Akcesoria and ".($exclusive?"(Rodzaj>799 and Rodzaj<900)":"(Rodzaj<800 or Rodzaj>899)")." order by Rodzaj,Symbol,Rozmiar";
    //$query = "SELECT * FROM PRODUKTY WHERE Pokaz=1 and not Tymczasowy and not Akcesoria and ".($exclusive?"(Rodzaj>799 and Rodzaj<900)":"(Rodzaj<800)")." order by Rodzaj,Symbol,Rozmiar";
    $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
    $i=0;
    $prevFoto=0;
    $prevSymbol="";
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
        {
        if($line[Symbol]!=$prevSymbol)
            $id=$line[Id];
            
            
        $query2 = "SELECT * FROM SKLADPROD WHERE IdProd='$id' and Typ=4 and Proba like '%333' order by Id";
        $result2 = mysql_query ($query2) or die ("Zapytanie zakonczone niepowodzeniem");
        if($line2 = mysql_fetch_array($result2, MYSQL_ASSOC))
            {
            continue;
            }    
            
        $tresc=$tresc."\t<produkt>\n\t\t<id>$line[Id]</id>\n\t\t<".slownik(478,$lang).">Staviori</".slownik(478,$lang).">\n\t\t<".slownik(479,$lang).">$line[Symbol]</".slownik(479,$lang).">\n";
        $trescCSV.="\"$line[Id]\";\"Staviori\";\"$line[Symbol]\";\"";
        if($line[Typ]==1)
            {
            $tresc=$tresc."\t\t<".slownik(481,$lang).">$line[Rozmiar]</".slownik(481,$lang).">\n";
            $trescCSV.=$line[Rozmiar];
            }
        $trescCSV.="\";\"";
        
        //nazwa
        $query2 = "SELECT * FROM SKLADPROD WHERE IdProd='$id' and Typ=2 order by Id";
        $result2 = mysql_query ($query2) or die ("Zapytanie zakonczone niepowodzeniem");
        if($line2 = mysql_fetch_array($result2, MYSQL_ASSOC))
       	    {
       	    $query3 = "SELECT * FROM SKLADNIKI WHERE Id='".$line2[Skladnik]."'";
            $result3 = mysql_query ($query3) or die ("Zapytanie zakonczone niepowodzeniem");
            $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
            
            $tresc=$tresc."\t\t<".slownik(648,$lang).">".$line3[$lpole]." Staviori $line[Symbol]</".slownik(648,$lang).">\n";
            $trescCSV.=$line3[$lpole]." Staviori $line[Symbol]";
            }
        $trescCSV.="\";\"";
        
        switch($waluta)
            {
            case "EUR": $cena=round($line[BruttoH]/kurs("EUR"));         break;
            case "GBP": $cena=round($line[BruttoH]/kurs("GBP"));         break;
            case "CZK": $cena=nicepriceCZK($line[BruttoH]/kurs("CZK"));  break;
            case "USD": $cena=nicepriceUSD($line[BruttoH]/kurs("USD"));  break;
            default:    $cena=$line[BruttoH];
            }
        
        if($line[PromocjaH]>0)
            {
            switch($waluta)
                {
                case "EUR": $cena2=round($line[PromocjaH]/kurs("EUR"));         break;
                case "GBP": $cena2=round($line[PromocjaH]/kurs("GBP"));         break;
                case "CZK": $cena2=nicepriceCZK($line[PromocjaH]/kurs("CZK"));  break;
                case "USD": $cena2=nicepriceUSD($line[PromocjaH]/kurs("USD"));  break;
                default:    $cena2=$line[PromocjaH];
                }
            $tresc=$tresc."\t\t<".slownik(482,$lang).">$cena2</".slownik(482,$lang).">\n\t\t<".slownik(483,$lang).">$cena</".slownik(483,$lang).">\n";
            $trescCSV.=$cena2."\";\"".$cena."\";\"";
            }
        else    
            {
            $tresc=$tresc."\t\t<".slownik(482,$lang).">$cena</".slownik(482,$lang).">\n";
            $trescCSV.=$cena."\";\"\";\"";
            }
            
        $tresc=$tresc."\t\t<".slownik(484,$lang).">23%</".slownik(484,$lang).">\n";
        $trescCSV.="23%\";\"";
        
        $nrkat=(int)($line[Rodzaj]/100);
        $rodz=$line[Rodzaj];
        
        
            
        $query2 = "SELECT * FROM SKLADPROD WHERE IdProd='$id' and Typ=2 order by Id";
        $result2 = mysql_query ($query2) or die ("Zapytanie zakonczone niepowodzeniem");
        if($line2 = mysql_fetch_array($result2, MYSQL_ASSOC))
       	    {
       	    $query3 = "SELECT * FROM SKLADNIKI WHERE Id='".$line2[Skladnik]."'";
            $result3 = mysql_query ($query3) or die ("Zapytanie zakonczone niepowodzeniem");
            $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
            
            $tresc=$tresc."\t\t<".slownik(480,$lang).">".$line3[$lpole]."</".slownik(480,$lang).">\n";
            $trescCSV.=$line3[$lpole];
            }
        
        $trescCSV.="\";\"";
        
        $cert="No";
        $query2 = "SELECT * FROM SKLADPROD WHERE IdProd='$id' and Typ=3 order by Id";
        $result2 = mysql_query ($query2) or die ("Zapytanie zakonczone niepowodzeniem");
        $j=0;
        while($line2 = mysql_fetch_array($result2, MYSQL_ASSOC))
       	    {
       	    $query3 = "SELECT * FROM SKLADNIKI WHERE Id='".$line2[Skladnik]."'";
            $result3 = mysql_query ($query3) or die ("Zapytanie zakonczone niepowodzeniem");
            $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
            
            if($line2[Masa])
                $cert="Yes";
            
            $barwa="";
            $czystosc="";
            if($line2[BarwaOd])
                {
                $query4 = "SELECT * FROM BARWY WHERE Id='".$line2[BarwaOd]."'";
                $result4 = mysql_query ($query4) or die ("Zapytanie zakończone niepowodzeniem");
        	    $line4 = mysql_fetch_array($result4, MYSQL_ASSOC);
                $barwa=$line4[Nazwa];
                if($line2[BarwaDo]>$line2[BarwaOd])
                    {
                    $query4 = "SELECT * FROM BARWY WHERE Id='".$line2[BarwaDo]."'";
                    $result4 = mysql_query ($query4) or die ("Zapytanie zakończone niepowodzeniem");
            	    $line4 = mysql_fetch_array($result4, MYSQL_ASSOC);
                    $barwa=$barwa."-".$line4[Nazwa];
                    }
                }
            if($line2[CzystoscOd])
                {
                $query4 = "SELECT * FROM CZYSTOSCI WHERE Id='".$line2[CzystoscOd]."'";
                $result4 = mysql_query ($query4) or die ("Zapytanie zakończone niepowodzeniem");
        	    $line4 = mysql_fetch_array($result4, MYSQL_ASSOC);
                $czystosc=$line4[Nazwa];
                if($line2[CzystoscDo]>$line2[CzystoscOd])
                    {
                    $query4 = "SELECT * FROM CZYSTOSCI WHERE Id='".$line2[CzystoscDo]."'";
                    $result4 = mysql_query ($query4) or die ("Zapytanie zakończone niepowodzeniem");
            	    $line4 = mysql_fetch_array($result4, MYSQL_ASSOC);
                    $czystosc=$czystosc."-".$line4[Nazwa];
                    }
                }
            $tresc=$tresc."\t\t<".slownik(486,$lang).">".$line3[$lpole]."</".slownik(486,$lang).">\n";
            $trescCSV.=$line3[$lpole]."\";\"$line2[Ilosc]\";\"$line2[Masa]\";\"$czystosc\";\"$barwa\";\"";
            if(++$j>1) break;
            }
            
        while($j++<2)
            $trescCSV.="\";\"\";\"\";\"\";\"\";\"";
            
        //$trescCSV.=";";    ///kamen1;kamen1_qty;kamen1_carat;kamen1_clarity;kamen1_color
            
        $query2 = "SELECT * FROM SKLADPROD WHERE IdProd='$id' and Typ=4 order by Id";
        $result2 = mysql_query ($query2) or die ("Zapytanie zakonczone niepowodzeniem");
        if($line2 = mysql_fetch_array($result2, MYSQL_ASSOC))
       	    {
       	    $query3 = "SELECT * FROM SKLADNIKI WHERE Id='".$line2[Skladnik]."'";
            $result3 = mysql_query ($query3) or die ("Zapytanie zakonczone niepowodzeniem");
            $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
            
            $tresc=$tresc."\t\t<".slownik(487,$lang).">".$line3[$lpole]."</".slownik(487,$lang).">\n";
            $trescCSV.=$line3[$lpole];
            }        
            
        $trescCSV.="\";\"";        
        
        
        $trescCSV.="$cert\";\"";
        
        
        $query2 = "SELECT * FROM SKLADPROD WHERE IdProd='$id' and Typ=5 order by Id";
        $result2 = mysql_query ($query2) or die ("Zapytanie zakonczone niepowodzeniem");
        while($line2 = mysql_fetch_array($result2, MYSQL_ASSOC))
       	    {
       	    $query3 = "SELECT * FROM SKLADNIKI WHERE Id='".$line2[Skladnik]."'";
            $result3 = mysql_query ($query3) or die ("Zapytanie zakonczone niepowodzeniem");
            $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
            
            $tresc=$tresc."\t\t<".slownik(487,$lang).">".$line3[$lpole]."</".slownik(487,$lang).">\n";
            $trescCSV.=$line3[$lpole];
            if($line2[Wielkosc]) 
	            $trescCSV.=" ".$line2[Wielkosc]." mm";
	        $trescCSV.=". ";
            }        
        
        $trescCSV.="\";\"";
        
        //$tresc=$tresc."\t\t<kategoria>".$kategorie[$lang][$nrkat]."</kategoria>\n";
        //$tresc=$tresc."\t\t<podkategoria>".$podkategorie[$lang][$rodz]."</podkategoria>\n";
        
        $opis=opis($id,$lang,-1,$line[Pytac],$line[Rozmiar]);
        $opis=$opis." ".$line[$lopis];
        $opis=str_replace("<font color=#C8B560>", "", $opis); 
        $opis=str_replace("<font color=orange>", "", $opis);
        $opis=str_replace("</font>", "", $opis); 
        $opis=str_replace("<b>", "", $opis); 
        $opis=str_replace("</b>", "", $opis); 
        $opis=str_replace("<br>", " ", $opis); 
        $opis=str_replace("\n", " ", $opis);
        $opis=str_replace("\r", " ", $opis);
        
        $tresc=$tresc."\t\t<".slownik(488,$lang)."><![CDATA[$opis]]></".slownik(488,$lang).">\n";
        $trescCSV.=$opis."\";\"";
        
        $tresc=$tresc."\t\t<".slownik(489,$lang).">$line[Masa]</".slownik(489,$lang).">\n";
        $trescCSV.=$line[Masa]."\";\"";
        
        $queryStan2 = "SELECT count(*) c FROM TOWAR where IdProd=$line[Id] and Ilosc>0";
        $resultStan2 = mysql_query ($queryStan2) or die ("Zapytanie zakonczone niepowodzeniem");
        $lineStan2 = mysql_fetch_array($resultStan2, MYSQL_ASSOC);
        
        $queryStan3 = "SELECT count(*) c FROM PRODUKTYZAM where KodKresk is null and IdZam in (select Id from ZAMOWIENIA where Zatwierdzone and not Wykonane) and Symbol='$line[Symbol]'".($line[Typ]==1?" and Rozmiar=$line[Rozmiar]":"");
        $resultStan3 = mysql_query ($queryStan3) or die ("Zapytanie zakonczone niepowodzeniem");
        $lineStan3 = mysql_fetch_array($resultStan3, MYSQL_ASSOC);
        
        $ilosc = $lineStan2[c]+$line[YesStanCentrala]+$line[YesStanPH]+$line[YesStanDetal]-$lineStan3[c];
        
        if(!$line[Koniec] && $ilosc>0 && $lineStan2[c]+$line[YesStanCentrala]-$lineStan3[c]<1)
            $line[CzteryTygodnie]=1;
                    
        $line[Koniec]=($line[Koniec]||$ilosc<1)&&!$line[NigdyKoniec]?1:0;
        
        $dostepnosc="";
        $dostepnoscnr="";
        $tydzien="";
       	if($line[CzteryTygodnie])
       	    switch($line[CzteryTygodnie])
       	        {
       	        case 1: $tydzien=slownik(473,$lang);   break;
       	        case 2: case 3: case 4:
       	                $tydzien=slownik(474,$lang);   break;
       	        default: $tydzien=slownik(475,$lang);   break;
       	        }
        if($line[DataUsun]>0)
            {
            $dostepnosc=slownik(244,$lang);     //usuniety z oferty
            $dostepnoscnr="11";
            }
        else if(($line[CzteryTygodnie] && $line[Koniec]) || $line[CzteryTygodnie]==1)
            {
            $dostepnosc=slownik(214,$lang)." ".$line[CzteryTygodnie]." ".$tydzien;  //na zmowienie x tygodni
            $dostepnoscnr=$line[CzteryTygodnie];
            }
        else if($line[Koniec])
            {
            $dostepnosc=slownik(17,$lang);  //chwilowo brak
            $dostepnoscnr="10";
            }
        else
            {
            $dostepnosc=slownik(246,$lang); //dostepny
            $dostepnoscnr="0";
            }
            
        if($line[Typ]==2)
       	    {/*
       	    if($line[Pytac])
       	        $dostepnosc.=" ".slownik(211,$l);
       	    if(!$line[Pytac] && !$line[CzteryTygodnie])
       	        $dostepnosc.=" ".slownik(212,$l);
       	    if($line[CzteryTygodnie])
       	        $dostepnosc.=" ".slownik(213,$l)." ".$line[CzteryTygodnie]." ".$tydzien;
       	       */ 
       	        
       	    if($line[DataUsun]>0)
                {
                $dostepnosc=slownik(244,$lang);     //usuniety z oferty
                $dostepnoscnr="11";
                }
            else if(($line[CzteryTygodnie] && $line[Koniec]) || $line[CzteryTygodnie]==1)
                {
                $dostepnosc=slownik(213,$lang)." ".$line[CzteryTygodnie]." ".$tydzien;  //na zmowienie x tygodni
                $dostepnoscnr=$line[CzteryTygodnie];
                }
            else if($line[Koniec])
                {
                $dostepnosc=slownik(17,$lang);  //chwilowo brak
                $dostepnoscnr="10";
                }
            else
                {
                if($line[Pytac])
       	            {
       	            $dostepnosc=slownik(211,$lang);     //pytac o rozmiary
       	            $dostepnoscnr="9";
       	            }
       	        else
       	            {
       	            $dostepnosc=slownik(212,$lang);     //dostepne standardowe rozmiary
       	            $dostepnoscnr="0";
       	            }
                }//$dostepnosc=slownik(246,$lang);    
       	    }
            
        $tresc=$tresc."\t\t<".slownik(490,$lang).">$dostepnoscnr</".slownik(490,$lang).">\n";
        $tresc=$tresc."\t\t<".slownik(646,$lang).">$dostepnosc</".slownik(646,$lang).">\n";
        $trescCSV.=$dostepnoscnr."\";\"".$dostepnosc."\";\"";
        
        if(file_exists("/home/dnmk/ftp/katalog/foto/$line[Id].jpg"))
            {
            $tresc=$tresc."\t\t<".slownik(491,$lang).">http://staviori.biz/katalog/foto/$line[Id].jpg</".slownik(491,$lang).">\n";
            $trescCSV.="http://staviori.biz/katalog/foto/$line[Id].jpg";
            $prevFoto=$line[Id];
            $prevSymbol=$line[Symbol];
            }
        else if($line[Symbol]==$prevSymbol)
            {
            $tresc=$tresc."\t\t<".slownik(491,$lang).">http://staviori.biz/katalog/foto/$prevFoto.jpg</".slownik(491,$lang).">\n";
            $trescCSV.="http://staviori.biz/katalog/foto/$prevFoto.jpg";
            }
            
        $query2 = "SELECT * FROM GALERIA WHERE IdProd='".$line[Id]."' and Typ=0 order by Id";
	    $result2 = mysql_query ($query2) or die ("Zapytanie zakonczone niepowodzeniem");    
         while ($line2 = mysql_fetch_array($result2, MYSQL_ASSOC))
            {
            if(file_exists("/home/dnmk/ftp/".substr($line2[Url],3))) 
                $tresc=$tresc."\t\t<".slownik(491,$lang).">http://staviori.biz/".substr($line2[Url],3)."</".slownik(491,$lang).">\n";
            }  
        $tresc=$tresc."\t</produkt>\n";
        $trescCSV.="\"\n";
        //if(++$i>10)break;
        }
    $tresc=$tresc."</katalog>";
/*
    $myFile = "/home/dnmk/ftp/download/$plik.xml";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $tresc);
	fclose($fh);
	print "Gotowe, link: http://staviori.biz/download/$plik.xml ";		
*/
    $trescCSV=iconv("ISO-8859-2","UTF-8",$trescCSV);

	$myFile = "/home/dnmk/ftp/download/$plik.csv";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $trescCSV);
	fclose($fh);
	print "http://staviori.biz/download/$plik.csv ";
}
?>

