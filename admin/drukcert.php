<?php
include "session.php";

require('tfpdf/tfpdf.php');
class PDF extends tFPDF
{
function CellPl($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
    $this->Cell($w, $h, iconv("ISO-8859-2", "UTF-8", $txt), $border, $ln, $align, $fill, $link); 
    }
}

function opcja($tab,$id,$lang="PL")
{
$query = "SELECT Nazwa, IdSlownik FROM $tab WHERE Id='$id'";
$result = mysql_query ($query) or die ("Zapytanie zako�czone niepowodzeniem");
$line = mysql_fetch_array($result, MYSQL_ASSOC);
return $line[IdSlownik]?slownik($line[IdSlownik],$lang):$line[Nazwa];
}

function drukcert($kody,$lang="PL")
{
$w2=10;
$w=50;$h=10;$mt=80;$ml=23;$mr=23;$fs=11;

$w=$_GET[w]?$_GET[w]:$w;
$h=$_GET[h]?$_GET[h]:$h;
$mt=$_GET[mt]?$_GET[mt]:$mt;
$ml=$_GET[ml]?$_GET[ml]:$ml;
$mr=$_GET[mr]?$_GET[mr]:$mr;
$fs=$_GET[fs]?$_GET[fs]:$fs;

$pdf = new PDF();
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->SetFont('DejaVu','',$fs);

$pdf->SetTopMargin($mt); //mm
$pdf->SetLeftMargin($ml);
$pdf->SetRightMargin($mr);



//require_once('html2fpdf/html2fpdf.php'); 
//$pdf = new HTML2FPDF('P','mm','a4');
//$cm = $pdf->lMargin;

$link = dblogin(); 

$kodArr = explode(',', $kody);

foreach ($kodArr as $k => $kod) 
    {
    $pdf->AddPage();
    
    $querytow = "SELECT * FROM TOWAR WHERE KodKresk='$kod'";
    $resulttow = mysql_query ($querytow) or die ("Zapytanie zako�czone niepowodzeniem");
    $linetow = mysql_fetch_array($resulttow, MYSQL_ASSOC);

    $query = "SELECT * FROM PRODUKTY WHERE Id='$linetow[IdProd]'";
    $result = mysql_query ($query) or die ("Zapytanie zako�czone niepowodzeniem");
    $line = mysql_fetch_array($result, MYSQL_ASSOC);
   
    $pdf->CellPl($w,$h,"Numer certyfikatu:");
    $pdf->CellPl($w2,$h,$kod);
    $pdf->CellPl(0,$h,"Data: ".date("d.m.Y"),0,1,"R");
    $pdf->Ln();
    $pdf->CellPl($w,$h,"Kamie�:");
    $pdf->CellPl($w2,$h,opcja("KAMIENIE",$line[Kamien]),0,1);
    $pdf->CellPl($w,$h,"Rodzaj szlifu:");
    $pdf->CellPl($w2,$h,opcja("KSZTALTY",$line[Ksztalt]),0,1);
    $pdf->CellPl($w,$h,"Masa:");
    $pdf->CellPl($w2,$h,przecinek($line[Masa])." ct.",0,1);
    $pdf->CellPl($w,$h,"Barwa:");
    $pdf->CellPl($w2,$h,opcja("BARWY",$line[Barwa]),0,1);
    $pdf->CellPl($w,$h,"Czysto��:");
    $pdf->CellPl($w2,$h,opcja("CZYSTOSCI",$line[Czystosc]),0,1);
    $pdf->CellPl($w,$h,"Symetria:");
    $pdf->CellPl($w2,$h,$line[Symetria]?opcja("SYMETRIE",$line[Symetria]):slownik(50,$lang),0,1);
    $pdf->CellPl($w,$h,"Szlif:");
    $pdf->CellPl($w2,$h,$line[Szlif]?opcja("SZLIFY",$line[Szlif]):slownik(50,$lang),0,1);
    $pdf->CellPl($w,$h,"Polerowanie:");
    $pdf->CellPl($w2,$h,$line[Polerowanie]?opcja("POLEROWANIA",$line[Polerowanie]):slownik(50,$lang),0,1);
    }

$nazwa="cert.pdf";    
$download="D";//D//I//F

$pdf->Output();//($nazwa, $download);
	
mysql_close($link);
}

if(!auth())
{
  print "<p class=error align=center>*** Brak dost�pu ***</p>";
}
else
{
  if(isset($_GET[multidruk]))
    {
    $queryParams = array();
    parse_str($_SERVER['QUERY_STRING'], $params);
            
    foreach($params as $pName => $pValue)   
        if(substr($pName,0,2)=="ff")
            $queryParams[] = $pValue;
            
    $_GET[kod] = implode(',', $queryParams);
    }

  drukcert($_GET[kod]);
}
?>  
