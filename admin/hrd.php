<?php
function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();
   
    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }
   
    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}

function hrdcert($nrcert)
{
    $ch = curl_init();

    // set url
    curl_setopt($ch, CURLOPT_URL, "https://my.hrdantwerp.com/index.php?id=46&number=$nrcert&auth=CtBCNYwffc");

    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $output contains the output string
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch); 
    
    //print "<pre>";
    
    $xmlObj = simplexml_load_string($output);
    $arrXml = objectsIntoArray($xmlObj);
    
    //$tabs[KAMIENIE]=$params[5];
    $tabs[KSZTALTY]=$arrXml[Results][Shape][ShapeCode]=="brilliant"?"round":$arrXml[Results][Shape][ShapeCode];
    $tabs[MASA]=$arrXml[Results][Carat];
    $tabs[BARWY]=$arrXml[Results][ColourGrade][ColourCode];
    $tabs[CZYSTOSCI]=$arrXml[Results][ClarityGrade][ClarityCode]=="LC"?"IF":$arrXml[Results][ClarityGrade][ClarityCode];
    $tabs[SZLIFY]=$arrXml[Results][Cut][Proportions][ProportionsDescription];
    $tabs[POLEROWANIA]=$arrXml[Results][Cut][Polish][PolishDescription];
    $tabs[SYMETRIE]=$arrXml[Results][Cut][Symmetry][SymmetryDescription];
    $tabs[FLUORESCENCJE]=$arrXml[AdditionalInformation][Fluorescence][FluorescenceCode];
    $tabs[LABORATORIA]="HRD";
    
    //print_r($arrXml);
    //print_r($tabs);
    return $tabs;
}

//hrdcert(10014039013);
?>
