<?php

function utf2iso($str) {
  return iconv("UTF-8", "ISO-8859-2", $str);
}

function iso2utf($str) {
  return iconv("ISO-8859-2", "UTF-8", $str);
}

function katTypy() {
  $res = array();
  $query = 'SELECT NrRodz, Nr, title FROM `KATTYPY`';
  $result = mysql_query($query) or die("Pobranie KATTYPY BŁĄD");

  while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
  {
    $l = iso2utf ($line['title']);
    $res[$line['NrRodz']][$line['Nr']] = $l;
  }
  $res[4][5] = "Srebrny pierścionek";
  $res[2][4] = "Złoty wisiorek";
  $res[4][4] = "Srebrny wisiorek";
  $res[4][2] = "Srebrna bransoletka";
  $res[2][2] = "Złota bransoletka";
  $res[1][4] = "Bransoletka z brylantami";
  $res[8][4] = "Biżuteria Inwestycyjna Bransoletka";
  $res[4][1] = "Srebrny łańcuszek";
  $res[2][1] = "Złoty łańcuszek";
  $res[4][8] = "Srebrny naszyjnik";
  $res[8][5] = "Biżuteria Inwestycyjna Naszyjnik";
  $res[2][8] = "Złoty naszyjnik";
  $res[1][5] = "Naszyjnik z brylantami";
  $res[8][1] = "Pierścionek Exclusive";

  return $res;
}

