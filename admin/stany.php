<?php
$vatproc=23;
$vatmnoz=1.23;
function IsInt($str){ return (strspn($str, "0123456789") == strlen($str)); }
function kropek($string){ return strtr($string,",","."); }
function NicePrice($brutto)
    {    
    $bruttoRound = ceil($brutto);
    if($bruttoRound>=100 && $bruttoRound<=1000)
        {
        $konc=$bruttoRound%10;
        if($konc==0)
            $bruttoRound-=1;
        if($konc>0 && $konc<2)
            $bruttoRound+=2-$konc;
        if($konc>2 && $konc<5)
            $bruttoRound+=5-$konc;
        if($konc>5 && $konc<9)
            $bruttoRound+=9-$konc;
        }
    if($bruttoRound==1000 || $bruttoRound==999)
        $bruttoRound=995;
            
    if($bruttoRound>1000 && $bruttoRound<2000)
        {
        $konc=$bruttoRound%10;
        if($konc>0 && $konc<5)
            $bruttoRound+=5-$konc; 	            
        if($konc>5)
            $bruttoRound+=10-$konc;
        }
     	            
    if($bruttoRound>=2000 && $bruttoRound<=6000)
        {
        $konc=$bruttoRound%10;
        if($konc>0)
            $bruttoRound+=10-$konc; 
            
        if($bruttoRound%1000==0)
            $bruttoRound-=10;	            
        }
        
    if($bruttoRound>6000)
        {
        $konc=$bruttoRound%100;
        if($konc>0)
            $bruttoRound+=100-$konc; 
            
        if($bruttoRound%1000==0)
            $bruttoRound-=100;	            
        }   
    return $bruttoRound;
    }

function slownik2i($i,$l)
{      
    switch($l)
        {
        case "EN":  $lfield="En";   break;
        case "CZ":  $lfield="Cz";   break;
        default:    $lfield="Pl";   break;
        }         
    $query = "SELECT $lfield FROM SLOWNIK2 where Id=$i";
	$result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
	$row = mysql_fetch_array($result, MYSQL_ASSOC);
	return $row[$lfield];
}

function kurs($w)
{
    if($w=="PLN")
        return "1";    
    
    $myFile = "/home/dnmk/ftp/common/kurs$w.txt";
	$fh = fopen($myFile, 'r');
	$theData = fread($fh, filesize($myFile));
	fclose($fh);
	return $theData;
}

function nicepriceCZK($k)
{
        $bruttoRound = ceil($k);
 	    
 	    if($bruttoRound<10000)
 	        $bruttoRound+=10-$bruttoRound%10;
 	    else
 	        $bruttoRound+=100-$bruttoRound%100;
 	        
 	    return $bruttoRound;
}

function prodlog($zmiana)
{    
    $zmianaPL = $zmiana;    
    $zmianaPL = str_replace("[D]", slownik2i(21,"PL"), $zmianaPL);
    $zmianaPL = str_replace("[P]", slownik2i(22,"PL"), $zmianaPL);   
    $zmianaPL = str_replace("[P2]", slownik2i(23,"PL"), $zmianaPL);   
    $zmianaPL = str_replace("[C]", slownik2i(24,"PL"), $zmianaPL);   
    $zmianaPL = str_replace("[K]", slownik2i(25,"PL"), $zmianaPL);   
    $zmianaPL = str_replace("[K2]", slownik2i(26,"PL"), $zmianaPL);   
    $zmianaPL = str_replace("[U]", slownik2i(27,"PL"), $zmianaPL);      
    $zmianaPL = str_replace("[4T]", slownik2i(28,"PL"), $zmianaPL);   
    $zmianaPL = str_replace("[4T2]", slownik2i(29,"PL"), $zmianaPL);
    
    $zmianaCZ = $zmiana;
    $zmianaCZ = str_replace("[D]", slownik2i(21,"CZ"), $zmianaCZ);
    $zmianaCZ = str_replace("[P]", slownik2i(22,"CZ"), $zmianaCZ);   
    $zmianaCZ = str_replace("[P2]", slownik2i(23,"CZ"), $zmianaCZ);   
    $zmianaCZ = str_replace("[C]", slownik2i(24,"CZ"), $zmianaCZ);   
    $zmianaCZ = str_replace("[K]", slownik2i(25,"CZ"), $zmianaCZ);   
    $zmianaCZ = str_replace("[K2]", slownik2i(26,"CZ"), $zmianaCZ);   
    $zmianaCZ = str_replace("[U]", slownik2i(27,"CZ"), $zmianaCZ);      
    $zmianaCZ = str_replace("[4T]", slownik2i(28,"CZ"), $zmianaCZ);   
    $zmianaCZ = str_replace("[4T2]", slownik2i(29,"CZ"), $zmianaCZ);
    
    $zmianaEN = $zmiana;
    $zmianaEN = str_replace("[D]", slownik2i(21,"EN"), $zmianaEN);
    $zmianaEN = str_replace("[P]", slownik2i(22,"EN"), $zmianaEN);   
    $zmianaEN = str_replace("[P2]", slownik2i(23,"EN"), $zmianaEN);   
    $zmianaEN = str_replace("[C]", slownik2i(24,"EN"), $zmianaEN);   
    $zmianaEN = str_replace("[K]", slownik2i(25,"EN"), $zmianaEN);   
    $zmianaEN = str_replace("[K2]", slownik2i(26,"EN"), $zmianaEN);   
    $zmianaEN = str_replace("[U]", slownik2i(27,"EN"), $zmianaEN);      
    $zmianaEN = str_replace("[4T]", slownik2i(28,"EN"), $zmianaEN);   
    $zmianaEN = str_replace("[4T2]", slownik2i(29,"EN"), $zmianaEN);
    
    $ilearg=func_num_args();
    $argtab=func_get_args();

    if($ilearg>1)
        {
        $zmianaPL = $zmianaPL." ".$argtab[2]." -> ".$argtab[1]." PLN";
        $zmianaEN = $zmianaEN." ".$argtab[2]." -> ".$argtab[1]." PLN";
        $zmianaCZ = $zmianaCZ." ".nicepriceCZK($argtab[2]/kurs("CZK"))." -> ".nicepriceCZK($argtab[1]/kurs("CZK"))." CZK";
        }
        
    $files = fopen("/home/dnmk/ftp/outbox/prod.log", "a");
    flock($files, 2);
    fputs($files, $zmianaPL."\n");
    flock($files, 3);
    fclose($files);
    
    $files = fopen("/home/dnmk/ftp/outbox/prodCZ.log", "a");
    flock($files, 2);
    fputs($files, $zmianaCZ."\n");
    flock($files, 3);
    fclose($files);
    
    $files = fopen("/home/dnmk/ftp/outbox/prodEN.log", "a");
    flock($files, 2);
    fputs($files, $zmianaEN."\n");
    flock($files, 3);
    fclose($files);
}
    
$starttime=microtime(1);
    
// define some variables
$local_file = '/admin/stany.csv';
$server_file = 'kli/S07508_STANYINDEKSOW_INFINITY.CSV';
$ftp_server = '195.116.150.20';
$ftp_user_name = 'infinity';
$ftp_user_pass = 'vMqQNqN2';

// set up basic connection
$conn_id = ftp_connect($ftp_server);

// login with username and password
$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

ftp_pasv($conn_id, true);

//  get the last modified time
$time = ftp_mdtm($conn_id, $server_file);

$prevtime=file_get_contents("/home/dnmk/ftp/admin/stany.dat");

if($time>$prevtime)// >
    {
    file_put_contents("/home/dnmk/ftp/admin/stany.dat",$time);

    if ($time != -1) {
        // somefile.txt was last modified on: March 26 2003 14:16:41.
        echo date("d-m-y H:i:s").", yes file modified on: ".date("d-m-y H:i:s", $time);//"$server_file was last modified on: " . date("F d Y H:i:s.", $time);
    } else {
        echo "Couldn't get mdtime";
    }

    // try to download $server_file and save to $local_file
    /*if (ftp_get($conn_id, $local_file, $server_file, FTP_BINARY)) {
        echo "Successfully written to $local_file\n";
    } else {
        echo "There was a problem\n";
    }*/

    ob_start();
    $result = ftp_get($conn_id, "php://output", $server_file, FTP_BINARY);
    $data = ob_get_contents();
    ob_end_clean();
    
    $data=explode("\r\n",$data);
    foreach ($data as $key => $dat)
       $data[$key] = explode(";", $dat);
       
     ///polacz z baza 
    $line = file("/home/dnmk/ftp/common/users.dat");
    foreach($line as $temp)
        {
        $str = explode(",", $temp);
        $logindb = chop($str[0]);
        $passwdb = chop($str[1]);
        }
    $link = mysql_connect('sql.dnmk.nazwa.pl', $logindb, $passwdb)
            or die ("Nie można się połączyć: " . mysql_error());
    mysql_select_db ("dnmk") or die ("Nie mozna wybrać bazy danych");
    
    $count[0]=0;
    $count[1]=0;
    $count[2]=0;
    $count[3]=0;
    $count[4]=0;
    $count[5]=0;
    $count[6]=0;
    $querylog="";
  
    foreach ($data as $key => $dat)
        {
        if($key==0) //pomin naglowek
            continue;
        
        /*
        0-﻿INDEKS;
        1-CENA;
        2-WAGA_SR;
        3-STAN_PH;
        4-STAN_DETAL;
        5-STAN_CENTRALA
        */
        if($dat[0])     //ostatni wiersz pusty (dat[0] tez pusty)
            {
            //ustaw stany dla danego produktu
            //print $dat[0]."<br>";
            
            if(substr($dat[0],1,1)=="S")        //zmniejszanie stanów C o 5 dla srebra
                $dat[5]-=($dat[5]<5?$dat[5]:5);
            
            $query = "select * from PRODUKTY where SymbolYes='$dat[0]'";
	        $result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");
            if($line = mysql_fetch_array($result, MYSQL_ASSOC))
                {
                $sql="";
                if(IsInt($dat[3]) && $dat[3]!=$line[YesStanPH])
                    {
                    $sql.=($sql!=""?", ":"")."YesStanPH=$dat[3]";
                    $count[3]++;
                    }
                if(IsInt($dat[4]) && $dat[4]!=$line[YesStanDetal])
                    {
                    $sql.=($sql!=""?", ":"")."YesStanDetal=$dat[4]";
                    $count[4]++;
                    }
                if(IsInt($dat[5]) && $dat[5]!=$line[YesStanCentrala])
                    {                    
                    $sql.=($sql!=""?", ":"")."YesStanCentrala=$dat[5]";
                    $count[5]++;
                    }
                    
                if($dat[2]!=$line[Masa] && $dat[2]!="1" && $dat[2]!="0" && $dat[2]!="")
                    {
                    //$sql.=($sql!=""?", ":"")."Masa='$dat[2]'";    //update mas nieaktywny
                    $count[2]++;
                    }    
                    
                if($line[GroupId] && $dat[1]!="" && is_numeric(kropek($dat[1])) && kropek($dat[1])>0)
                    {
                    $query = "SELECT * FROM GRUPYCEN WHERE Id='".$line[GroupId]."'";
	                $result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");
                 	if($groupline = mysql_fetch_array($result, MYSQL_ASSOC))
                 	    {
                 	    $brutto = NicePrice(kropek($dat[1])*$groupline[Mnoznik]);
                 	    
                 	    if($line[Promocja]>0)
                 	        {
                 	        if(abs($brutto-$line[Promocja])/$line[Promocja]>0.03)         //$brutto!=$line[Promocja]
                 	            {
                 	            $sql.=($sql!=""?", ":"")."Promocja='$brutto'";
                 	            //if(!$line[Tymczasowy])
                                  //  prodlog("$line[Symbol]".($line[Typ]==1?" ($line[Rozmiar])":"").": [C]",$brutto,$line[Promocja]);
                                $count[1]++;    
                 	            }
             	            }
             	        else
             	            {
             	            if(abs($brutto-$line[Brutto])/$line[Brutto]>0.03)        //$brutto!=$line[Brutto]
                 	            {
                 	            $sql.=($sql!=""?", ":"")."Brutto='$brutto', Netto='".round($brutto/($vatmnoz),2)."'";
                 	            //if(!$line[Tymczasowy])
                                  //  prodlog("$line[Symbol]".($line[Typ]==1?" ($line[Rozmiar])":"").": [C]",$brutto,$line[Brutto]);
                                $count[1]++;    
                 	            }
             	            }
                 	    }
                 	//hurtownia
                 	$query = "SELECT * FROM GRUPYCENH WHERE Id='".$line[GroupId]."'";
	                $result = mysql_query ($query) or die ("Zapytanie zakoñczone niepowodzeniem");
                 	if($groupline = mysql_fetch_array($result, MYSQL_ASSOC))
                 	    {
                 	    $brutto = NicePrice(kropek($dat[1])*$groupline[Mnoznik]);
                 	    
                 	    if($line[PromocjaH]>0)
                 	        {
                 	        if(abs($brutto-$line[PromocjaH])/$line[PromocjaH]>0.03)         //$brutto!=$line[Promocja]
                 	            {
                 	            $sql.=($sql!=""?", ":"")."PromocjaH='$brutto'";
                 	            if(!$line[Tymczasowy])
                                    prodlog("$line[Symbol]".($line[Typ]==1?" ($line[Rozmiar])":"").": [C]",$brutto,$line[PromocjaH]);
                                $count[6]++;    
                 	            }
             	            }
             	        else
             	            {
             	            if(abs($brutto-$line[BruttoH])/$line[BruttoH]>0.03)        //$brutto!=$line[Brutto]
                 	            {
                 	            $sql.=($sql!=""?", ":"")."BruttoH='$brutto', NettoH='".round($brutto/($vatmnoz),2)."'";

                 	            if(!$line[Tymczasowy])
                                    prodlog("$line[Symbol]".($line[Typ]==1?" ($line[Rozmiar])":"").": [C]",$brutto,$line[BruttoH]);
                                $count[6]++;    
                 	            }
             	            }
                 	    }    
                 	//end-hurtownia    
                    }
                if($sql!="")
                    {
                    $query = "update PRODUKTY set $sql where SymbolYes='$dat[0]'";
                    //if($dat[2]!=$line[Masa])
                      //  print "$dat[0]: $line[Masa] -> $dat[2] ($line[SymbolYes2])<br>";
                    //print $query."\n";
                    $result = mysql_query ($query) or die ("Zapytanie zakończone niepowodzeniem");
                    $count[0]++;
                    $querylog.=$query.";\n";
                    }
                }
            }
        }
        
    mysql_close($link);
    
    print "\n\nPoprawione:\n - produkty: $count[0]\n - ceny:     $count[1]\n - ceny hurt:$count[6]\n - masy:     $count[2] (nieaktywne!)\n - stany PH: $count[3]\n - stany D:  $count[4]\n - stany C:  $count[5]"; 
    }
else
    print "There are no new updates";
    
// close the connection
ftp_close($conn_id);

$endtime=microtime(1);
print "\n\nTime: ".round($endtime-$starttime,3)." sec";

$files = fopen("/home/dnmk/ftp/admin/stany.log", "a");
flock($files, 2);
fputs($files, date("d-m-y H:i:s").", yes file modified on: ".date("d-m-y H:i:s", $time).", exec time: ".round($endtime-$starttime,3)." sec\n".$querylog."\n");
flock($files, 3);
fclose($files);
?>

