<?php
/*
$client = new SoapClient("ReportCheckWS");
//var_dump($client->__getFunctions()); 
//var_dump($client->__getTypes()); 

$ip=$_SERVER[SERVER_ADDR];
$input="<?xml version=\"1.0\" encoding=\"UTF8\"?>
	<REPORT_CHECK_REQUEST>
		<HEADER>
			<IP_ADDRESS>$ip</IP_ADDRESS>
		</HEADER>
		<BODY>
			<REPORT_DTLS>
				<REPORT_DTL>
					<REPORT_NO>1107771351</REPORT_NO>
					<REPORT_WEIGHT>0.41</REPORT_WEIGHT>
				</REPORT_DTL>
			</REPORT_DTLS>
		</BODY>
	</REPORT_CHECK_REQUEST>";
	
$input="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:q0=\"http://service.reportcheck.model.ngs.com/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soapenv:Body><q0:processRequest><arg0><?xml version=\"1.0\" encoding=\"UTF8\"?><REPORT_CHECK_REQUEST><HEADER><IP_ADDRESS>85.128.235.50</IP_ADDRESS></HEADER><BODY><REPORT_DTLS><REPORT_DTL><REPORT_NO>1107771351</REPORT_NO><REPORT_WEIGHT>0.41</REPORT_WEIGHT></REPORT_DTL></REPORT_DTLS></BODY></REPORT_CHECK_REQUEST></arg0></q0:processRequest></soapenv:Body></soapenv:Envelope>";
	
print "   $input    ";
var_dump($client->__soapCall("processRequest",array($input)));
var_dump($client->processRequest1(array($input)));
*/


function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
    $arrData = array();
   
    // if input is object, convert into array
    if (is_object($arrObjData)) {
        $arrObjData = get_object_vars($arrObjData);
    }
   
    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {
                $value = objectsIntoArray($value, $arrSkipIndices); // recursive call
            }
            if (in_array($index, $arrSkipIndices)) {
                continue;
            }
            $arrData[$index] = $value;
        }
    }
    return $arrData;
}

/*
 * SOAP call
 */
function post_soap_url($url, $data, $soap_action = '', $timeout = 200) {
    $ch = curl_init (); //initiate the curl session
    curl_setopt ( $ch, CURLOPT_URL, $url ); //set to url to post to
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 ); // return data in a variable
    curl_setopt ( $ch, CURLOPT_HEADER, 0 );
    curl_setopt ( $ch, CURLOPT_POST, 1 );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data ); // post the xml
    curl_setopt ( $ch, CURLOPT_TIMEOUT, $timeout ); // set timeout in seconds
    curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
    $header = array ("Content-Type: text/xml" );
    $header [] = "Content-Length: ".strlen($data);

    if (! is_null ( $soap_action ))
     $header [] = 'SOAPAction: "' . $soap_action . '"';

    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $header );
    $xmlResponse = curl_exec ( $ch );
    curl_close ( $ch );

    return $xmlResponse;
}

function vg($in)
{
    switch($in)
        {
        case "EX": return "Excellent";
        case "VG": return "Very Good";
        case "G": return "Good";
        case "F": return "Fair";
        default:   return $in;
        }
}

function fl($in)
{
    switch($in)
        {
        case "NON": return "None";
        //case "": return "Very Slight";
        //case "": return "Slight";
        case "FNT": return "Faint";
        case "MED": return "Medium";
        case "STG": return "Strong";
        case "VST": return "Very Strong";
        default:   return $in;
        }
}

function giacert($certificateNumber,$caratWeight)
{
    //$certificateNumber = '1107771351'; 
    //$caratWeight = '0.41';
    
    $xml = '<?xml version="1.0" encoding="UTF8"?><REPORT_CHECK_REQUEST><HEADER><IP_ADDRESS>85.128.235.50</IP_ADDRESS></HEADER><BODY><REPORT_DTLS><REPORT_DTL><REPORT_NO>' . $certificateNumber . '</REPORT_NO><REPORT_WEIGHT>' . $caratWeight . '</REPORT_WEIGHT></REPORT_DTL></REPORT_DTLS></BODY></REPORT_CHECK_REQUEST>';
    $request ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:q0="http://service.reportcheck.model.ngs.com/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><q0:processRequest><arg0>' . htmlentities($xml) . '</arg0></q0:processRequest></soapenv:Body></soapenv:Envelope>';
  
    //echo 'REQUEST:<br /><br />' . htmlentities($request) . '<br /><br />';
    
    $xmlResponse = post_soap_url ( "https://labws.gia.edu/ReportCheck/ReportCheckWS?WSDL", $request, "https://labws.gia.edu/ReportCheck/ReportCheckWS" );
    
    $xmlResponse = str_replace("&lt;","<",$xmlResponse);
    $xmlResponse = str_replace("&gt;",">",$xmlResponse);
    $xmlResponse = str_replace("<?xml version='1.0' encoding='UTF-8'?><S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\"><S:Body><ns2:processRequestResponse xmlns:ns2=\"http://service.reportcheck.model.ngs.com/\"><return>","",$xmlResponse);
    $xmlResponse = str_replace("</return></ns2:processRequestResponse></S:Body></S:Envelope>","",$xmlResponse);
    
    //echo 'RESPONSE:<br /><br />' . htmlentities($xmlResponse);
    
    $xmlObj = simplexml_load_string($xmlResponse);
    $arrXml = objectsIntoArray($xmlObj);
    
    //print "<pre>";
    //print_r($arrXml);
    if(!is_array($arrXml[REPORT_DTLS][REPORT_DTL][MESSAGE]))
        print "<p class=error align=center>".$arrXml[REPORT_DTLS][REPORT_DTL][MESSAGE]."</p>";
    if(!is_array($arrXml[ERROR_DTLS][ERROR_MSG]))
        print "<p class=error align=center>".$arrXml[ERROR_DTLS][ERROR_MSG]."</p>";
    
    $tabs[MASA]=$arrXml[REPORT_DTLS][REPORT_DTL][WEIGHT];
    $tabs[KSZTALTY]=$arrXml[REPORT_DTLS][REPORT_DTL][SHAPE]=="RBC"?"round":$arrXml[REPORT_DTLS][REPORT_DTL][SHAPE];
    $tabs[BARWY]=$arrXml[REPORT_DTLS][REPORT_DTL][COLOR];
    $tabs[CZYSTOSCI]=$arrXml[REPORT_DTLS][REPORT_DTL][CLARITY];
    $tabs[SZLIFY]=vg($arrXml[REPORT_DTLS][REPORT_DTL][FINAL_CUT]);
    $tabs[POLEROWANIA]=vg($arrXml[REPORT_DTLS][REPORT_DTL][POLISH]);
    $tabs[SYMETRIE]=vg($arrXml[REPORT_DTLS][REPORT_DTL][SYMMETRY]);
    $tabs[FLUORESCENCJE]=fl($arrXml[REPORT_DTLS][REPORT_DTL][FLUORESCENCE_INTENSITY]);
    $tabs[LABORATORIA]="GIA";

    //print_r($tabs);
    return $tabs;
}

//giacert(1107771351,0.41);
//giacert($_GET[n],$_GET[m]);
?>
