<?php
include "session.php";

function d2w( $digits )
{
$jednosci = Array( 'zero', 'jeden', 'dwa', 'trzy', 'cztery', 'pi��', 'sze��', 'siedem', 'osiem', 'dziewi��' );
$dziesiatki = Array( '', 'dziesi��', 'dwadzie�cia', 'trzydzie�ci', 'czterdzie�ci', 'pie�dziesi�t', 'sze��dziesi�t', 'siedemdziesi�t', 'osiemdziesi�t', 'dziewi��dziesi�t' );
$setki = Array( '', 'sto', 'dwie�cie', 'trzysta', 'czterysta', 'pi��set', 'sze��set', 'siedemset', 'osiemset', 'dziewi��set' );
$nastki = Array( 'dziesi��', 'jedena�cie', 'dwana�cie', 'trzyna�cie', 'czterna�cie', 'pi�tna�cie', 'szesna�cie', 'siedemna�cie', 'osiemna�cie', 'dziewi�tna�cie' );
$tysiace = Array( 'tysi�c', 'tysi�ce', 'tysi�cy' );
$miliony = Array( 'milion', 'miliony', 'milion�w' );
 
$digits = (string) $digits;
$digits = strrev( $digits );
$i = strlen( $digits );
 
$string = '';

if( $i > 8 && $digits[8] > 0 )
$string .= $setki[ $digits[8] ] . ' ';
if( $i > 7 && $digits[7] > 1 )
$string .= $dziesiatki[ $digits[7] ] . ' ';
elseif( $i > 6 && $digits[7] == 1 )
$string .= $nastki[$digits[6]] . ' ';
if( $i > 6 && $digits[6] > 0 && $digits[7] != 1 )
$string .= $jednosci[ $digits[6] ] . ' ';

$tmpStr = substr( strrev( $digits ), 0, -6 );
if( strlen( $tmpStr ) > 0 )
{
$tmpInt = (int) $tmpStr;
if( $tmpInt == 1 )
$string .= $miliony[0] . ' ';
elseif( ( $tmpInt % 10 > 1 && $tmpInt % 10 < 5 ) && ( $tmpInt%100 < 10 || $tmpInt%100 > 20 ) )
$string .= $miliony[1] . ' ';
else
$string .= $miliony[2] . ' ';
}
 
if( $i > 5 && $digits[5] > 0 )
$string .= $setki[ $digits[5] ] . ' ';
if( $i > 4 && $digits[4] > 1 )
$string .= $dziesiatki[ $digits[4] ] . ' ';
elseif( $i > 3 && $digits[4] == 1 )
$string .= $nastki[$digits[3]] . ' ';
if( $i > 3 && $digits[3] > 0 && $digits[4] != 1 )
$string .= $jednosci[ $digits[3] ] . ' ';
 
$tmpStr = substr( strrev( $digits ), 0, -3 );
if( strlen( $tmpStr ) > 0 )
{
$tmpInt = (int) $tmpStr;
if( $tmpInt == 1 )
$string .= $tysiace[0] . ' ';
elseif( ( $tmpInt % 10 > 1 && $tmpInt % 10 < 5 ) && ( $tmpInt%100 < 10 || $tmpInt%100 > 20 ) )
$string .= $tysiace[1] . ' ';
else
$string .= $tysiace[2] . ' ';
}
 
if( $i > 2 && $digits[2] > 0 )
$string .= $setki[$digits[2]] . ' ';
if( $i > 1 && $digits[1] > 1 )
$string .= $dziesiatki[$digits[1]] . ' ';
elseif( $i > 0 && $digits[1] == 1 )
$string .= $nastki[$digits[0]] . ' ';
if( $digits[0] > 0 && $digits[1] != 1 )
$string .= $jednosci[$digits[0]] . ' ';
 
return $string;
}

////////////////////////////////////////////////////////////////////////////////

function drukfaktura($ids,$cp,$email)
{
require_once('html2fpdf/html2fpdf.php'); 
$pdf = new HTML2FPDF('P','mm','a4');

$cm = $pdf->lMargin;

$link = dblogin(); 

$VAT23=23;

$cpst=$cp;
$cpen=$cp;
if($cp==2)
    {
    $cpst=0;
    $cpen=1;
    }
$idArr = explode(',', $ids);

foreach ($idArr as $k => $id) 
  for($copy=$cpst; $copy<=$cpen; $copy++)
    {
    $queryfak = "SELECT * FROM FAKTURY WHERE Id='$id'";
    $resultfak = mysql_query ($queryfak) or die ("Zapytanie zako�czone niepowodzeniem");
    $linefak = mysql_fetch_array($resultfak, MYSQL_ASSOC);

    $querypoz = "SELECT *, sum(Qty) Ilosc FROM FAKTURYPOZ WHERE IdFak='$id' group by Symbol, Rozmiar order by Symbol, Rozmiar";
    $resultpoz = mysql_query ($querypoz) or die ("Zapytanie zako�czone niepowodzeniem");
    
    $korekta = 0;
    if($linefak[Rodzaj]==3 || $linefak[Rodzaj]==4)
        {
        $korekta = 1;
        $queryfak2 = "SELECT * FROM FAKTURY WHERE Numer='$linefak[NumerFVlubPar]'";
        $resultfak2 = mysql_query ($queryfak2) or die ("Zapytanie zako�czone niepowodzeniem");
        $linefak2 = mysql_fetch_array($resultfak2, MYSQL_ASSOC);

        $querypoz2 = "SELECT *, sum(Qty) Ilosc FROM FAKTURYPOZ WHERE IdFak='$linefak2[Id]' group by Symbol, Rozmiar order by Symbol, Rozmiar";
        $resultpoz2 = mysql_query ($querypoz2) or die ("Zapytanie zako�czone niepowodzeniem");
        }
				
    $pdf->SetLeftMargin(1*$cm);
    $pdf->SetRightMargin(1*$cm);
    $pdf->SetTopMargin(5*$cm);
    $pdf->SetAutoPageBreak(false,1*$cm);

    $pdf->pgwidth = $pdf->fw - $pdf->lMargin - $pdf->rMargin ;

    ob_start();

    $ue=0;
    $tlumacz = array();

    switch ($linefak[Rodzaj])
        {
        case 1:
        case 3: $dokument="FAKTURA VAT";
                $nip = "<br><b>NIP:</b> $linefak[NIP]"; break;
        case 2: 
        case 4: $ue=1;
                $VAT23=0;
                $dokument="FAKTURA VAT WDT";
                $nip = "<br><b>NIPUE:</b> $linefak[NIP]"; 
                $tlumacz[1]=" <i>(seller)</i>";
                $tlumacz[2]=" <i>(buyer)</i>";
                $tlumacz[3]="<td align=right valign=top><font color=white>.</font><br><i>(invoice no.)<br>(date of issue)<br>(date of sale)</i></td>";
                $tlumacz[4]=" <i>(".($copy?"copy":"original").")</i>";
                $tlumacz[5]="<br><i>(No.)</i>";
                $tlumacz[6]="<br><i>(Item description)</i>";
                $tlumacz[7]="<br><i>(Quantity)</i>";
                $tlumacz[8]="<br><i>(Per)</i>";
                $tlumacz[9]="<br><i>(Price)</i>";
                $tlumacz[10]="<br><i>(Amount)</i>";
                $tlumacz[11]="<br><i>(VAT)</i>";
                $tlumacz[12]="<br><i>(VAT amount)</i>";
                $tlumacz[13]=" <i>(total)</i>";
                $tlumacz[14]="<br><i>(for payment)</i>";
                $tlumacz[15]="<br><i>(total for payment)</i>";
                $tlumacz[16]="<br><i>(in words)</i>";
                $tlumacz[17]="<br><i>(form of payment)</i>";
                if ($linefak[Rodzaj]==4)
                    {
                    $tlumacz[3]="<td align=right valign=top><font color=white>.</font><br><i>(invoice no.)<br>(date of issue)<br>(invoice no.)<br>(date of issue)<br>(date of sale)</i></td>";
                    $tlumacz[14]="<br><i>(for reimbursement)</i>";
                    $tlumacz[15]="<br><i>(total for reimbursement)</i>";
                    }
                                                        break;
        case 5:
                $nip = "";
                $dokument="FAKTURA VAT";    break;
        case 0: $nip = "";
                $dokument="PARAGON";    break;
        }
          
    print "<table width=100%><tr>$tlumacz[3]<td align=left valign=top><font color=white>.</font><br><b>$dokument".($korekta?" KOREKTA":"")." nr: $linefak[Numer]</b> <br>data wystawienia: ".substr($linefak[DataWyst],0,10).($korekta?"<br><b>DO: $dokument nr: $linefak2[Numer]</b> <br>data wystawienia: ".substr($linefak2[DataWyst],0,10)."<br>data sprzeda�y: $linefak2[DataSprz]":"<br>data sprzeda�y: $linefak[DataSprz]")."</td><td align=right valign=top><font color=white>.</font><br><b>".($copy?"KOPIA":"ORYGINA�")."</b>$tlumacz[4]</td></tr>";
    print "</table><br>";
        
    print "<table width=100%><tr><td valign=top><b>SPRZEDAWCA:</b>$tlumacz[1] <br>".getUstawienia("SprzedawcaFak")."</td><td valign=top><b>NABYWCA:</b>$tlumacz[2] <br>$linefak[Nabywca]<br><b>Adres:</b> $linefak[Adres]$nip</td></tr>";
    print "</table>";
    
    if($korekta)
        {
        print "<br><b>PRZED KOREKT�</b> ";
        print "<br><table width=100%><tr><td colspan=10 bgcolor=black height=1></td></tr><tr><td valign=top align=right width=10>Lp<br>$tlumacz[5]</td><td valign=top width=30%>Nazwa<br>$tlumacz[6]</td><td valign=top align=right>Ilo��<br>$tlumacz[7]</td><td valign=top>jm.<br>$tlumacz[8]</td><td valign=top align=right>Cena<br>netto$tlumacz[9]</td><td valign=top align=right>Cena<br>brutto$tlumacz[9]</td><td valign=top align=right>Wart.<br>netto$tlumacz[10]</td><td valign=top align=right>VAT<br>$tlumacz[11]</td><td valign=top align=right>Kwota<br>Vat$tlumacz[12]</td><td valign=top align=right>Wart.<br>brutto$tlumacz[10]</td></tr><tr><td colspan=10 bgcolor=black height=1></td></tr>";

        $i=0;
        $sumanetto=0.0;
        $sumaVAT=0.0;
        $sumabrutto=0.0;
        $sumailosc=0;
        $stawki = array();
        $stawkinetto = array();
        $stawkibrutto = array();
        while($linepoz2 = mysql_fetch_array($resultpoz2, MYSQL_ASSOC))
            {
            $i++;
            $zaliczki="";
            if($linepoz2[Symbol]=="ZALICZKA" && $i>1)
                {
                $linepoz2[Ilosc]=-$linepoz2[Ilosc];
                $sumailosc++;
                
                $queryfakZ = "SELECT * FROM FAKTURY WHERE Id='$linepoz[KodKresk]'";
                $resultfakZ = mysql_query ($queryfakZ) or die ("Zapytanie zako�czone niepowodzeniem");
                $linefakZ = mysql_fetch_array($resultfakZ, MYSQL_ASSOC);
                $wartnetto=$linepoz2[Netto]*$linepoz2[Ilosc];
                $wartbrutto=$linepoz2[Brutto]*$linepoz2[Ilosc];
                $kwotaVATZ=$wartbrutto-$wartnetto;
                $wartnetto=number_format(round($wartnetto,2),2,',',''); 
                $kwotaVATZ=number_format(round($kwotaVATZ,2),2,',','');
                $wartbrutto=number_format(round($wartbrutto,2),2,',','');
                $linepoz2Netto=number_format($linepoz2[Netto],2,',','');
                $linepoz2Brutto=number_format($linepoz2[Brutto],2,',','');
                $zaliczki="<tr><td colspan=10 bgcolor=black height=1></td></tr><tr><td colspan=2><b>ROZLICZONE ZALICZKI</b> </td></tr><tr><td colspan=2>$linefakZ[DataSprz] $linefakZ[Numer] $linepoz2[Symbol]</td><td align=right>$linepoz2[Ilosc]</td><td>$linepoz2[Jm]</td><td align=right>$linepoz2Netto</td><td align=right>$linepoz2Brutto</td><td align=right>$wartnetto</td><td align=right>$linepoz2[VAT]%</td><td align=right>$kwotaVATZ</td><td align=right>$wartbrutto</td></tr>";
                }
            $wartnetto=$linepoz2[Netto]*$linepoz2[Ilosc];
            //$kwotaVAT=($wartnetto*22.0)/100.0;
            $wartbrutto=$linepoz2[Brutto]*$linepoz2[Ilosc];
            $sumanetto+=$wartnetto;
            //$sumaVAT+=$kwotaVAT;
            $sumabrutto+=$wartbrutto;
            if(isset($stawki[$linepoz2[VAT]])) 
                {
                $stawkinetto[$linepoz2[VAT]]+=$wartnetto;
                $stawkibrutto[$linepoz2[VAT]]+=$wartbrutto;
                }
            else
                {
                $stawkinetto[$linepoz2[VAT]]=$wartnetto;
                $stawkibrutto[$linepoz2[VAT]]=$wartbrutto;
                $stawki[$linepoz2[VAT]]=1;
                }
            $sumailosc+=$linepoz2[Ilosc];  
            $wartnetto="";//number_format(round($wartnetto,2),2,',',''); 
            $kwotaVAT="";//number_format(round($kwotaVAT,2),2,',','');
            $wartbrutto=number_format(round($wartbrutto,2),2,',','');
            $linepoz2[Netto]=number_format($linepoz2[Netto],2,',','');
            $linepoz2[Brutto]=number_format($linepoz2[Brutto],2,',','');
            print "<tr><td align=right>$i</td><td>$linepoz2[Symbol]".($linepoz2[Typ]==1?" ($linepoz2[Rozmiar])":"")."</td><td align=right>$linepoz2[Ilosc]</td><td>$linepoz2[Jm]</td><td align=right>$linepoz2[Netto]</td><td align=right>$linepoz2[Brutto]</td><td align=right>$wartnetto</td><td align=right>$linepoz2[VAT]%</td><td align=right>$kwotaVAT</td><td align=right>$wartbrutto</td></tr>$zaliczki";
            }
               
        $podsumabrutto=number_format(round($sumabrutto,2),2,',','');    
        print "<tr><td colspan=10 bgcolor=black height=1></td></tr><tr><td colspan=8 align=right>podsuma:</td><td></td><td align=right valign=top>$podsumabrutto</td></tr>";
        $rabat=number_format($linefak2[Rabat],2,',','');    
        print "<tr><td colspan=8 align=right>rabat:</td><td></td><td align=right valign=top>-$rabat</td></tr>";
        
        $sumabrutto-=$linefak2[Rabat];
        $stawkibrutto[$VAT23]-=$linefak2[Rabat];
           
        //$sumanetto=$sumabrutto/1.23;   
        $sumabrutto=round($sumabrutto,2);
        $sumanetto=round($sumabrutto/(1+$VAT23/100),2); 
        $sumaVAT=number_format(round($sumabrutto-$sumanetto,2),2,',','');    
        $sumanetto=number_format(round($sumanetto,2),2,',',''); 
        $sumabrutto=number_format(round($sumabrutto,2),2,',','');    
        print "<tr><td colspan=10 bgcolor=black height=1></td></tr><tr><td></td><td><b>Razem:</b>$tlumacz[13]</td><td align=right><b>$sumailosc</b></td><td></td><td></td><td></td><td align=right><b>$sumanetto</b></td><td></td><td align=right><b>$sumaVAT</b></td><td align=right><b>$sumabrutto</b></td></tr><tr><td colspan=6></td><td colspan=4 bgcolor=black height=1></td></tr>";

        foreach ($stawki as $j => $value) 
            {
            $stawkibrutto[$j]=round($stawkibrutto[$j],2);
            $stawkinetto[$j]=round($stawkibrutto[$j]/($j/100+1),2);
            $sumaVATst=number_format(round($stawkibrutto[$j]-$stawkinetto[$j],2),2,',','');    
            $sumanettost=number_format(round($stawkinetto[$j],2),2,',',''); 
            $sumabruttost=number_format(round($stawkibrutto[$j],2),2,',','');  
            print "<tr><td colspan=6 align=right>w stawce $j%</td><td align=right>$sumanettost</td><td></td><td align=right>$sumaVATst</td><td align=right>$sumabruttost</td></tr>";
            }
                    
        print "<tr><td colspan=8 align=right>do zap�aty:$tlumacz[14]</td><td></td><td align=right valign=top>$sumabrutto</td></tr>";
        print "</table>";
        
        print "<br><b>KOREKTA</b> Podstawa korekty: zwrot";
        }
        
    print "<br><table width=100%><tr><td colspan=10 bgcolor=black height=1></td></tr><tr><td valign=top align=right width=10>Lp<br>$tlumacz[5]</td><td valign=top width=30%>Nazwa<br>$tlumacz[6]</td><td valign=top align=right>Ilo��<br>$tlumacz[7]</td><td valign=top>jm.<br>$tlumacz[8]</td><td valign=top align=right>Cena<br>netto$tlumacz[9]</td><td valign=top align=right>Cena<br>brutto$tlumacz[9]</td><td valign=top align=right>Wart.<br>netto$tlumacz[10]</td><td valign=top align=right>VAT<br>$tlumacz[11]</td><td valign=top align=right>Kwota<br>Vat$tlumacz[12]</td><td valign=top align=right>Wart.<br>brutto$tlumacz[10]</td></tr><tr><td colspan=10 bgcolor=black height=1></td></tr>";

    $i=0;
    $sumanetto=0.0;
    $sumaVAT=0.0;
    $sumabrutto=0.0;
    $sumailosc=0;
    $stawki = array();
    $stawkinetto = array();
    $stawkibrutto = array();
    while($linepoz = mysql_fetch_array($resultpoz, MYSQL_ASSOC))
        {
        $i++;
        $zaliczki="";
        if($linepoz[Symbol]=="ZALICZKA" && $i>1)
            {
            $linepoz[Ilosc]=-$linepoz[Ilosc];
            $sumailosc++;
            
            $queryfakZ = "SELECT * FROM FAKTURY WHERE Id='$linepoz[KodKresk]'";
            $resultfakZ = mysql_query ($queryfakZ) or die ("Zapytanie zako�czone niepowodzeniem");
            $linefakZ = mysql_fetch_array($resultfakZ, MYSQL_ASSOC);
            $wartnetto=$linepoz[Netto]*$linepoz[Ilosc];
            $wartbrutto=$linepoz[Brutto]*$linepoz[Ilosc];
            $kwotaVATZ=$wartbrutto-$wartnetto;
            $wartnetto=number_format(round($wartnetto,2),2,',',''); 
            $kwotaVATZ=number_format(round($kwotaVATZ,2),2,',','');
            $wartbrutto=number_format(round($wartbrutto,2),2,',','');
            $linepozNetto=number_format($linepoz[Netto],2,',','');
            $linepozBrutto=number_format($linepoz[Brutto],2,',','');
            $zaliczki="<tr><td colspan=10 bgcolor=black height=1></td></tr><tr><td colspan=2><b>ROZLICZONE ZALICZKI</b> </td></tr><tr><td colspan=2>$linefakZ[DataSprz] $linefakZ[Numer] $linepoz[Symbol]</td><td align=right>$linepoz[Ilosc]</td><td>$linepoz[Jm]</td><td align=right>$linepozNetto</td><td align=right>$linepozBrutto</td><td align=right>$wartnetto</td><td align=right>$linepoz[VAT]%</td><td align=right>$kwotaVATZ</td><td align=right>$wartbrutto</td></tr>";
            }
        $wartnetto=$linepoz[Netto]*$linepoz[Ilosc];
        //$kwotaVAT=($wartnetto*22.0)/100.0;
        $wartbrutto=$linepoz[Brutto]*$linepoz[Ilosc];
        $sumanetto+=$wartnetto;
        //$sumaVAT+=$kwotaVAT;
        $sumabrutto+=$wartbrutto;
        if(isset($stawki[$linepoz[VAT]])) 
            {
            $stawkinetto[$linepoz[VAT]]+=$wartnetto;
            $stawkibrutto[$linepoz[VAT]]+=$wartbrutto;
            }
        else
            {
            $stawkinetto[$linepoz[VAT]]=$wartnetto;
            $stawkibrutto[$linepoz[VAT]]=$wartbrutto;
            $stawki[$linepoz[VAT]]=1;
            }
        $sumailosc+=$linepoz[Ilosc];  
        $wartnetto="";//number_format(round($wartnetto,2),2,',',''); 
        $kwotaVAT="";//number_format(round($kwotaVAT,2),2,',','');
        $wartbrutto=number_format(round($wartbrutto,2),2,',','');
        $linepoz[Netto]=number_format($linepoz[Netto],2,',','');
        $linepoz[Brutto]=number_format($linepoz[Brutto],2,',','');
        print "<tr><td align=right>$i</td><td>$linepoz[Symbol]".($linepoz[Typ]==1?" ($linepoz[Rozmiar])":"")."</td><td align=right>$linepoz[Ilosc]</td><td>$linepoz[Jm]</td><td align=right>$linepoz[Netto]</td><td align=right>$linepoz[Brutto]</td><td align=right>$wartnetto</td><td align=right>$linepoz[VAT]%</td><td align=right>$kwotaVAT</td><td align=right>$wartbrutto</td></tr>$zaliczki";
        }
    $podsumabrutto=number_format(round($sumabrutto,2),2,',','');    
    print "<tr><td colspan=10 bgcolor=black height=1></td></tr><tr><td colspan=8 align=right>podsuma:</td><td></td><td align=right valign=top>$podsumabrutto</td></tr>";
    
    if($korekta)
        {
        $rabat=number_format($linefak2[Rabat]-$linefak[Rabat],2,',',''); 
        print "<tr><td colspan=8 align=right>korekta rabatu:</td><td></td><td align=right valign=top>-$rabat</td></tr>";
        $sumabrutto-=$linefak2[Rabat]-$linefak[Rabat];
        $stawkibrutto[$VAT23]-=$linefak2[Rabat]-$linefak[Rabat];
        }
    else
        {
        $rabat=number_format($linefak[Rabat],2,',','');     
        print "<tr><td colspan=8 align=right>rabat:</td><td></td><td align=right valign=top>-$rabat</td></tr>";
        $sumabrutto-=$linefak[Rabat];
        $stawkibrutto[$VAT23]-=$linefak[Rabat];
        }       
    
    $sumabrutto=round($sumabrutto,2);
    $sumanetto=round($sumabrutto/(1+$VAT23/100),2);
    $sumaVAT=number_format(round($sumabrutto-$sumanetto,2),2,',','');    
    $sumanetto=number_format(round($sumanetto,2),2,',',''); 
    $sumabrutto=number_format(round($sumabrutto,2),2,',','');    
    print "<tr><td colspan=10 bgcolor=black height=1></td></tr><tr><td></td><td><b>Razem:</b>$tlumacz[13]</td><td align=right><b>$sumailosc</b></td><td></td><td></td><td></td><td align=right><b>$sumanetto</b></td><td></td><td align=right><b>$sumaVAT</b></td><td align=right><b>$sumabrutto</b></td></tr><tr><td colspan=6></td><td colspan=4 bgcolor=black height=1></td></tr>";
    
    foreach ($stawki as $j => $value) 
        {
        $stawkibrutto[$j]=round($stawkibrutto[$j],2);
        $stawkinetto[$j]=round($stawkibrutto[$j]/($j/100+1),2);
        //$stawkinetto[$j]=$stawkibrutto[$j]/($j/100+1);
        $sumaVATst=number_format(round($stawkibrutto[$j]-$stawkinetto[$j],2),2,',','');    
        $sumanettost=number_format(round($stawkinetto[$j],2),2,',',''); 
        $sumabruttost=number_format(round($stawkibrutto[$j],2),2,',','');  
        print "<tr><td colspan=6 align=right>w stawce $j%</td><td align=right>$sumanettost</td><td></td><td align=right>$sumaVATst</td><td align=right>$sumabruttost</td></tr>";
        }
     
    if($korekta)
        {                
        print "<tr><td colspan=8 align=right>do zwrotu:$tlumacz[14]</td><td></td><td align=right valign=top>$sumabrutto</td></tr>";
        print "</table>";
        print "<b>Og�em do zwrotu:</b> $sumabrutto z�".$tlumacz[15]; 
        }
    else
        {        
        print "<tr><td colspan=8 align=right>do zap�aty:$tlumacz[14]</td><td></td><td align=right valign=top>$sumabrutto</td></tr>";
        print "</table>";
        print "<b>Og�em do zap�aty:</b> $sumabrutto z�".$tlumacz[15];
        }

    $kwotaArr = explode(',', $sumabrutto);
    $slownie = d2w($kwotaArr[0])." z� $kwotaArr[1]/100";
    print "<br><br><b>S�ownie:</b> $slownie".$tlumacz[16];
    
    $formaplat = "Got�wka";
    if($linefak[DataKredyt])
        $formaplat = "Przelew - termin p�atno�ci: $linefak[DataKredyt]";
    print "<br><br><b>Forma p�atno�ci:</b> $formaplat".$tlumacz[17];
    
    print "<br><br><br><br>".getUstawienia("KomentarzFak");
    
    print "<br><br><br><br><table><tr><td width=400 align=center>Imi�, nazwisko oraz podpis osoby<br>uprawnionej do wystawiania faktur VAT</td><td align=center>Imi�, nazwisko oraz podpis osoby<br>uprawnionej do odbioru faktur VAT</td></tr></table>";
    
    

    $htmlbuffer = ob_get_contents(); 
    ob_end_clean(); 
    $pdf->AddPage();						
    //$pdf->UseCSS(true);  
    //$htmlbuffer = iconv("UTF-8", "ISO8859-2", $htmlbuffer); 
    $pdf->WriteHTML($htmlbuffer); 
    }

$nazwa="faktura.pdf";    
$download="F";//I//D
if(isset($email))
    $download="F";

$pdf->Output($nazwa, $download);//($nazwa, $download);

if(isset($email))
	{
	require 'vendor/phpmailer/PHPMailerAutoload.php';
	
	$queryuser = "SELECT * FROM USERS WHERE Id='".$linefak[IdUser]."'";
    $resultuser = mysql_query ($queryuser) or die ("Zapytanie zako�czone niepowodzeniem");
    $userline = mysql_fetch_array($resultuser, MYSQL_ASSOC);
    
    $lang = $userline[Lang];
    if ($lang == "PL")
        $lang = "";
    $myFile = "../common/mail10$lang.txt";
    $fh = fopen($myFile, 'r');
    $wiadomosc = fread($fh, filesize($myFile));
    fclose($fh);
    
    $wiadomosc = str_replace("[IMIE]", $userline[Imie], $wiadomosc);
    $wiadomosc = str_replace("[NAZWISKO]", $userline[Nazwisko], $wiadomosc);
    $wiadomosc = str_replace("[NABYWCA]", $linefak[Nabywca], $wiadomosc);
    $wiadomosc = str_replace("[NRFAKTURY]", $linefak[Numer], $wiadomosc);
    $wiadomosc = str_replace("[NUMERZAMOWIENIA]", $linefak[IdZam], $wiadomosc);
    $wiadomosc = str_replace("[KWOTA]", $sumabrutto, $wiadomosc);
    
    $email=$userline[EmailFak];
	
	$mail = new PHPMailer(true);
	try
	    {
	    $mail->CharSet = 'ISO-8859-2';
	    $mail->setFrom('kontakt@e-diamenty.pl', 'e-diamenty');
	    $mail->addReplyTo('kontakt@e-diamenty.pl', 'e-diamenty');
	    $mail->addAddress($email);
			
	    $mail->Subject = "[e-diamenty] ".$linefak[Numer];
	    $mail->msgHTML(stripslashes($wiadomosc));
	    $mail->AltBody = stripslashes($wiadomosc);
	    $mail->AddAttachment($nazwa,$linefak[Numer].".pdf"); //AddAttachment( $file_to_attach , 'NameOfFile.pdf' );
	    $mail->send();
	    
	    print "<meta http-equiv=\"Refresh\" content=\"1; url=".$_SERVER['HTTP_REFERER']."\">";
		print "<head>
                <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-2\" />
                <title>OK</title>
                <body>
                
                <center><h1>Faktura wys�ana!</h1></center>

                </body>
                </html>";
        }
    catch (phpmailerException $e) 
        {
        print "<meta http-equiv=\"Refresh\" content=\"5; url=".$_SERVER['HTTP_REFERER']."\">";
		print "<head>
                <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-2\" />
                <title>B��d</title>
                <body>
                
                <center><h1>Faktura NIE wys�ana!</h1>";
        if(!$email)
            print "<h2>Nie zdefiniowano adresu e-mail do wysy�ania faktur!</h2>";
        print $e->errorMessage();
        print "</center></body>
                </html>";
        }

	unlink($nazwa);
	}
	
mysql_close($link);

if(!isset($email) && file_exists($nazwa))
    print "<script language=javascript>location.assign('$nazwa');</script>";
}

if(!auth())
{
  print "<p class=error align=center>*** Brak dost�pu ***</p>";
}
else
{
  if(isset($_GET[multidruk]))
    {
    $queryParams = array();
    parse_str($_SERVER['QUERY_STRING'], $params);
            
    foreach($params as $pName => $pValue)   
        if(substr($pName,0,2)=="ff")
            $queryParams[] = $pValue;
            
    $_GET[fakid] = implode(',', $queryParams);
    }

  drukfaktura($_GET[fakid],$_GET[fakcp],$_GET[email]);
}
?>  
