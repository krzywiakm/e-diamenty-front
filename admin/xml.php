<table border=1 width=100%>
<tr><th align=center>
<?
	tekst("KATALOG2XML");
?>
</th></tr>
</table>

<p>

<?
if(!auth())
{
  print "<p class=error align=center>*** Brak dostepu ***</p>";
}
else
{    
    if(!empty($_POST[generuj]))
        {
        $link = dblogin();
        
        $lang=$_POST[lang];
        $waluta=$_POST[waluta];
        
        switch($lang)
            {            
            case "CZ":  $lpole="Cz1"; $lopis="OpisCZ"; break;
            case "EN":  $lpole="En1"; $lopis="OpisEN"; break;
            case "PL":  $lpole="Pl1"; $lopis="Opis";   break;
            }
        
        $tresc="<?xml version=\"1.0\" encoding=\"ISO-8859-2\"?>\n<katalog>\n";
        //$trescCSV="marka;model;dlugosc;cena;cena_promocyjna;vat;kategoria;kamien;metal;opis;masa;dostepnosc;foto\n";
        $trescCSV=slownik(478,$lang).";".slownik(479,$lang).";".slownik(481,$lang).";".slownik(482,$lang).";".slownik(483,$lang).";".slownik(484,$lang).";".slownik(480,$lang).";".slownik(486,$lang).";".slownik(487,$lang).";".slownik(488,$lang).";".slownik(489,$lang).";".slownik(490,$lang).";".slownik(491,$lang)."\n";        
        $query = "SELECT * FROM KATRODZAJE";
        $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $kategorie = array(array());
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
            {
            $kategorie["PL"][$line[Nr]]=$line[Nazwa];
            $kategorie["CZ"][$line[Nr]]=$line[NazwaCZ];
            $kategorie["EN"][$line[Nr]]=$line[NazwaEN];
            }
            
        $query = "SELECT * FROM KATTYPY where NrRodz<100";
        $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $podkategorie = array(array());
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
            {
            $i=$line[NrRodz]*100+$line[Nr];
            $podkategorie["PL"][$i]=$line[Nazwa];
            $podkategorie["CZ"][$i]=$line[NazwaCZ];
            $podkategorie["EN"][$i]=$line[NazwaEN];
            }    
            
        $query = "SELECT * FROM PRODUKTY WHERE Pokaz=1 and not Tymczasowy and not Akcesoria and (Rodzaj<800 or Rodzaj>899) order by Symbol,Rozmiar";
        $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $i=0;
        $prevFoto=0;
        $prevSymbol="";
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
            {
            $tresc=$tresc."\t<produkt>\n\t\t<".slownik(478,$lang).">Staviori</".slownik(478,$lang).">\n\t\t<".slownik(479,$lang).">$line[Symbol]</".slownik(479,$lang).">\n";
            $trescCSV.="Staviori;$line[Symbol];";
            if($line[Typ]==1)
                {
                $tresc=$tresc."\t\t<".slownik(481,$lang).">$line[Rozmiar]</".slownik(481,$lang).">\n";
                $trescCSV.=$line[Rozmiar];
                }
            $trescCSV.=";";
            
            switch($waluta)
                {
                case "EUR": $cena=round($line[Brutto]/kurs("EUR"));         break;
                case "CZK": $cena=nicepriceCZK($line[Brutto]/kurs("CZK"));  break;
                default:    $cena=$line[Brutto];
                }
            
            if($line[Promocja]>0)
                {
                switch($waluta)
                    {
                    case "EUR": $cena2=round($line[Promocja]/kurs("EUR"));         break;
                    case "CZK": $cena2=nicepriceCZK($line[Promocja]/kurs("CZK"));  break;
                    default:    $cena2=$line[Promocja];
                    }
                $tresc=$tresc."\t\t<".slownik(482,$lang).">$cena2</".slownik(482,$lang).">\n\t\t<".slownik(483,$lang).">$cena</".slownik(483,$lang).">\n";
                $trescCSV.=$cena2.";".$cena.";";
                }
            else    
                {
                $tresc=$tresc."\t\t<".slownik(482,$lang).">$cena</".slownik(482,$lang).">\n";
                $trescCSV.=$cena.";;";
                }
                
            $tresc=$tresc."\t\t<".slownik(484,$lang).">23%</".slownik(484,$lang).">\n";
            $trescCSV.="23%;";
            
            $nrkat=(int)($line[Rodzaj]/100);
            $rodz=$line[Rodzaj];
            
            
            
            if($line[Symbol]!=$prevSymbol)
                $id=$line[Id];
                
            $query2 = "SELECT * FROM SKLADPROD WHERE IdProd='$id' and Typ=2 order by Id";
	        $result2 = mysql_query ($query2) or die ("Zapytanie zakonczone niepowodzeniem");
	        if($line2 = mysql_fetch_array($result2, MYSQL_ASSOC))
           	    {
           	    $query3 = "SELECT * FROM SKLADNIKI WHERE Id='".$line2[Skladnik]."'";
	            $result3 = mysql_query ($query3) or die ("Zapytanie zakonczone niepowodzeniem");
	            $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
	            
	            $tresc=$tresc."\t\t<".slownik(480,$lang).">".$line3[$lpole]."</".slownik(480,$lang).">\n";
	            $trescCSV.=$line3[$lpole];
	            }
	        
	        $trescCSV.=";";
	        
	        $query2 = "SELECT * FROM SKLADPROD WHERE IdProd='$id' and Typ=3 order by Id";
	        $result2 = mysql_query ($query2) or die ("Zapytanie zakonczone niepowodzeniem");
	        if($line2 = mysql_fetch_array($result2, MYSQL_ASSOC))
           	    {
           	    $query3 = "SELECT * FROM SKLADNIKI WHERE Id='".$line2[Skladnik]."'";
	            $result3 = mysql_query ($query3) or die ("Zapytanie zakonczone niepowodzeniem");
	            $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
	            
	            $tresc=$tresc."\t\t<".slownik(486,$lang).">".$line3[$lpole]."</".slownik(486,$lang).">\n";
	            $trescCSV.=$line3[$lpole];
	            }
	            
	        $trescCSV.=";";    
	            
	        $query2 = "SELECT * FROM SKLADPROD WHERE IdProd='$id' and Typ=4 order by Id";
	        $result2 = mysql_query ($query2) or die ("Zapytanie zakonczone niepowodzeniem");
	        if($line2 = mysql_fetch_array($result2, MYSQL_ASSOC))
           	    {
           	    $query3 = "SELECT * FROM SKLADNIKI WHERE Id='".$line2[Skladnik]."'";
	            $result3 = mysql_query ($query3) or die ("Zapytanie zakonczone niepowodzeniem");
	            $line3 = mysql_fetch_array($result3, MYSQL_ASSOC);
	            
	            $tresc=$tresc."\t\t<".slownik(487,$lang).">".$line3[$lpole]."</".slownik(487,$lang).">\n";
	            $trescCSV.=$line3[$lpole];
	            }        
	            
	        $trescCSV.=";";        
            //$tresc=$tresc."\t\t<kategoria>".$kategorie[$lang][$nrkat]."</kategoria>\n";
            //$tresc=$tresc."\t\t<podkategoria>".$podkategorie[$lang][$rodz]."</podkategoria>\n";
            
            $opis=opis($id,$lang,-1,$line[Pytac],$line[Rozmiar]);
            $opis=$opis." ".$line[$lopis];
            $opis=str_replace("<font color=#C8B560>", "", $opis); 
            $opis=str_replace("<font color=orange>", "", $opis);
            $opis=str_replace("</font>", "", $opis); 
            $opis=str_replace("<b>", "", $opis); 
            $opis=str_replace("</b>", "", $opis); 
            $opis=str_replace("<br>", " ", $opis); 
            
            $tresc=$tresc."\t\t<".slownik(488,$lang).">$opis</".slownik(488,$lang).">\n";
            $trescCSV.=$opis.";";
            
            $tresc=$tresc."\t\t<".slownik(489,$lang).">$line[Masa]</".slownik(489,$lang).">\n";
            $trescCSV.=$line[Masa].";";
            
            $queryStan2 = "SELECT count(*) c FROM TOWAR where IdProd=$line[Id] and Ilosc>0";
            $resultStan2 = mysql_query ($queryStan2) or die ("Zapytanie zakonczone niepowodzeniem");
            $lineStan2 = mysql_fetch_array($resultStan2, MYSQL_ASSOC);
            
            $queryStan3 = "SELECT count(*) c FROM PRODUKTYZAM where KodKresk is null and IdZam in (select Id from ZAMOWIENIA where Zatwierdzone and not Wykonane) and Symbol='$line[Symbol]'".($line[Typ]==1?" and Rozmiar=$line[Rozmiar]":"");
            $resultStan3 = mysql_query ($queryStan3) or die ("Zapytanie zakonczone niepowodzeniem");
            $lineStan3 = mysql_fetch_array($resultStan3, MYSQL_ASSOC);
            
            $ilosc = $lineStan2[c]+$line[YesStanCentrala]+$line[YesStanPH]+$line[YesStanDetal]-$lineStan3[c];
            
            if(!$line[Koniec] && $ilosc>0 && $lineStan2[c]+$line[YesStanCentrala]-$lineStan3[c]<1)
                $line[CzteryTygodnie]=1;
	                    
            $line[Koniec]=($line[Koniec]||$ilosc<1)&&!$line[NigdyKoniec]?1:0;
            
            $dostepnosc="";
            $tydzien="";
           	if($line[CzteryTygodnie])
           	    switch($line[CzteryTygodnie])
           	        {
           	        case 1: $tydzien=slownik(473,$lang);   break;
           	        case 2: case 3: case 4:
           	                $tydzien=slownik(474,$lang);   break;
           	        default: $tydzien=slownik(475,$lang);   break;
           	        }
            if($line[DataUsun]>0)
                $dostepnosc=slownik(244,$lang);
            else if($line[Koniec])
                $dostepnosc=slownik(17,$lang);
            else if($line[CzteryTygodnie])
                $dostepnosc=slownik(214,$lang)." ".$line[CzteryTygodnie]." ".$tydzien;
            else
                $dostepnosc=slownik(246,$lang);
                
            $tresc=$tresc."\t\t<".slownik(490,$lang).">$dostepnosc</".slownik(490,$lang).">\n";
            $trescCSV.=$dostepnosc.";";
            
            if(file_exists("../katalog/foto/$line[Id].jpg"))
                {
                $tresc=$tresc."\t\t<foto>http://staviori.biz/katalog/foto/$line[Id].jpg</foto>\n";
                $trescCSV.="http://staviori.biz/katalog/foto/$line[Id].jpg";
                $prevFoto=$line[Id];
                $prevSymbol=$line[Symbol];
                }
            else if($line[Symbol]==$prevSymbol)
                {
                $tresc=$tresc."\t\t<".slownik(491,$lang).">http://staviori.biz/katalog/foto/$prevFoto.jpg</".slownik(491,$lang).">\n";
                $trescCSV.="http://staviori.biz/katalog/foto/$prevFoto.jpg";
                }
                
            $query2 = "SELECT * FROM GALERIA WHERE IdProd='".$line[Id]."' and Typ=0 order by Id";
    	    $result2 = mysql_query ($query2) or die ("Zapytanie zakonczone niepowodzeniem");    
             while ($line2 = mysql_fetch_array($result2, MYSQL_ASSOC))
                { 
                $tresc=$tresc."\t\t<".slownik(491,$lang).">http://staviori.biz/".substr($line2[Url],3)."</".slownik(491,$lang).">\n";
                }  
            $tresc=$tresc."\t</produkt>\n";
            $trescCSV.="\n";
            //if(++$i>10)break;
            }
        $tresc=$tresc."</katalog>";
        mysql_close($link); 
        $myFile = "../download/$_POST[plik].xml";
  		$fh = fopen($myFile, 'w') or die("can't open file");
		fwrite($fh, $tresc);
		fclose($fh);
		print "Gotowe, link: <a href='$myFile'>http://staviori.biz/download/$_POST[plik].xml</a><br>";		
		$myFile = "../download/$_POST[plik].csv";
  		$fh = fopen($myFile, 'w') or die("can't open file");
		fwrite($fh, $trescCSV);
		fclose($fh);
		print "<a href='$myFile'>http://staviori.biz/download/$_POST[plik].csv</a><br><br><br>";
        }
    
    if(!empty($_POST[generujnrzest]))
        {
        $lang=$_POST[lang];
        switch($lang)
            {            
            case "CZ":  $lpole="Cz1"; $lopis="OpisCZ"; break;
            case "EN":  $lpole="En1"; $lopis="OpisEN"; break;
            case "PL":  $lpole="Pl1"; $lopis="Opis";   break;
            }
        $tresc="<?xml version=\"1.0\" encoding=\"ISO-8859-2\"?>\n<katalog>\n";
        $link = dblogin();
        $query = "SELECT * FROM KATRODZAJE";
        $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $kategorie = array(array());
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
            {
            $kategorie["PL"][$line[Nr]]=$line[Nazwa];
            $kategorie["CZ"][$line[Nr]]=$line[NazwaCZ];
            }
            
        $query = "SELECT * FROM KATTYPY where NrRodz<100";
        $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $podkategorie = array(array());
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
            {
            $i=$line[NrRodz]*100+$line[Nr];
            $podkategorie["PL"][$i]=$line[Nazwa];
            $podkategorie["CZ"][$i]=$line[NazwaCZ];
            }    
            
        $query = "SELECT * FROM PRODUKTY WHERE Pokaz=1 and not Tymczasowy and not Akcesoria and RodzajZestawienia='$_POST[nrzest]' order by Symbol";
        $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $i=0;
        $prevFoto=0;
        $prevSymbol="";
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
            {
            $tresc=$tresc."\t<produkt>\n\t\t<symbol>$line[Symbol]</symbol>\n";
            if($line[Typ]==1)
                $tresc=$tresc."\t\t<rozmiar>$line[Rozmiar]</rozmiar>\n";
            $tresc=$tresc."\t\t<cena>$line[Brutto]</cena>\n";
            
            $nrkat=(int)($line[Rodzaj]/100);
            $rodz=$line[Rodzaj];
            $tresc=$tresc."\t\t<kategoria>".$kategorie[$lang][$nrkat]."</kategoria>\n";
            $tresc=$tresc."\t\t<podkategoria>".$podkategorie[$lang][$rodz]."</podkategoria>\n";
            
            $opis=opis($line[Id],$lang,-1,$line[Pytac],$line[Rozmiar]);
            $opis=$opis." ".$line[$lopis];
            $opis=str_replace("<font color=#C8B560>", "", $opis); 
            $opis=str_replace("<font color=orange>", "", $opis);
            $opis=str_replace("</font>", "", $opis); 
            $opis=str_replace("<b>", "", $opis); 
            $opis=str_replace("</b>", "", $opis); 
            $opis=str_replace("<br>", " ", $opis); 
            
            $tresc=$tresc."\t\t<opis>$opis</opis>\n";
            
            $dostepnosc="";
            $tydzien="";
           	if($line[CzteryTygodnie])
           	    switch($line[CzteryTygodnie])
           	        {
           	        case 1: $tydzien=slownik(473,$l);   break;
           	        case 2: case 3: case 4:
           	                $tydzien=slownik(474,$l);   break;
           	        default: $tydzien=slownik(475,$l);   break;
           	        }
            if($line[DataUsun]>0)
                $dostepnosc=slownik(244,$lang);
            else if($line[Koniec])
                $dostepnosc=slownik(17,$lang);
            else if($line[CzteryTygodnie])
                $dostepnosc=slownik(214,$lang)." ".$line[CzteryTygodnie]." ".$tydzien;
            else
                $dostepnosc=slownik(246,$lang);
                
            $tresc=$tresc."\t\t<dostepnosc>$dostepnosc</dostepnosc>\n";
            
            if(file_exists("../katalog/foto/$line[Id].jpg"))
                {
                $tresc=$tresc."\t\t<foto>http://staviori.biz/katalog/foto/$line[Id].jpg</foto>\n";
                $prevFoto=$line[Id];
                $prevSymbol=$line[Symbol];
                }
            else if($line[Symbol]==$prevSymbol)
                $tresc=$tresc."\t\t<foto>http://staviori.biz/katalog/foto/$prevFoto.jpg</foto>\n";
                
            $tresc=$tresc."\t</produkt>\n";
            //if(++$i>10)break;
            }
        $tresc=$tresc."</katalog>";
        mysql_close($link); 
        $myFile = "../download/$_POST[plik].xml";
  		$fh = fopen($myFile, 'w') or die("can't open file");
		fwrite($fh, $tresc);
		fclose($fh);
		print "Gotowe, link: <a href='$myFile'>http://staviori.biz/download/$_POST[plik].xml</a><br><br><br>";
        }    
        
    if(!empty($_POST[ceneo]))
        {
        $lang=$_POST[lang];
        switch($lang)
            {            
            case "CZ":  $lpole="Cz1"; $lopis="OpisCZ"; break;
            case "EN":  $lpole="En1"; $lopis="OpisEN"; break;
            case "PL":  $lpole="Pl1"; $lopis="Opis";   break;
            }
        $tresc="<?xml version=\"1.0\" encoding=\"ISO-8859-2\"?>\n
                <offers xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1\">
                <group name=\"other\">\n";
                
                
        $link = dblogin();
        $query = "SELECT * FROM KATRODZAJE";
        $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $kategorie = array(array());
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
            {
            $kategorie["PL"][$line[Nr]]=$line[Nazwa];
            $kategorie["CZ"][$line[Nr]]=$line[NazwaCZ];
            }
            
        $query = "SELECT * FROM KATTYPY where NrRodz<100";
        $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $podkategorie = array(array());
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
            {
            $i=$line[NrRodz]*100+$line[Nr];
            $podkategorie["PL"][$i]=$line[Nazwa];
            $podkategorie["CZ"][$i]=$line[NazwaCZ];
            }    
            
        $query = "SELECT * FROM PRODUKTY WHERE Pokaz=1 and not Tymczasowy and not Akcesoria order by Symbol";
        $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $i=0;
        $prevFoto=0;
        $prevSymbol="";
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
            {
            $rozm = "";
            if($line[Typ]==1)
                $rozm=" $line[Rozmiar]cm";
            //$tresc=$tresc."\t<offer>\n\t\t<id>$line[Id]</id>\n\t\t<name><![CDATA[$line[Symbol]$rozm]]></name>\n";
            //if($line[Typ]==1)
              //  $tresc=$tresc."\t\t<rozmiar>$line[Rozmiar]</rozmiar>\n";
            //$tresc=$tresc."\t\t<price>$line[Brutto]</price>\n";
            
            $nrkat=(int)($line[Rodzaj]/100);
            $rodz=$line[Rodzaj];
            
            $excl="";
            if($nrkat==8)
                $excl="-exclusive";
                
            //$tresc=$tresc."\t\t<url><![CDATA[http://idiamonds.pl/wybrany$excl.php?id=$line[Id]]]></url>\n";
            $tresc=$tresc."<o id=\"$line[Id]\" url=\"http://staviori.pl/product.php?id=$line[Id]\" price=\"$line[Brutto]\">\n";
            if($line[Promocja]>0)
                $tresc=$tresc."\t\t<oldprice>$line[Promocja]</oldprice>\n";
            
            $tresc=$tresc."\t\t<cat><![CDATA[Bizuteria/".$kategorie[$lang][$nrkat]."/".$podkategorie[$lang][$rodz]."]]></cat>\n";
            //$tresc=$tresc."\t\t<podkategoria>".$podkategorie[$lang][$rodz]."</podkategoria>\n";
            $tresc=$tresc."\t\t<name><![CDATA[$line[Symbol]$rozm]]></name>\n";
            
            
            $opis=opis($line[Id],$lang,-1,$line[Pytac],$line[Rozmiar]);
            $opis=$opis." ".$line[$lopis];
            $opis=str_replace("<font color=#C8B560>", "", $opis); 
            $opis=str_replace("<font color=orange>", "", $opis);
            $opis=str_replace("</font>", "", $opis); 
            $opis=str_replace("<b>", "", $opis); 
            $opis=str_replace("</b>", "", $opis); 
            $opis=str_replace("<br>", " ", $opis); 
            
            $tresc=$tresc."\t\t<desc><![CDATA[$opis]]></desc>\n";
            
            $dostepnosc="";
            $tydzien="";
           	if($line[CzteryTygodnie])
           	    switch($line[CzteryTygodnie])
           	        {
           	        case 1: $tydzien=slownik(473,$l);   break;
           	        case 2: case 3: case 4:
           	                $tydzien=slownik(474,$l);   break;
           	        default: $tydzien=slownik(475,$l);   break;
           	        }
            if($line[DataUsun]>0)
                $dostepnosc=slownik(244,$lang);
            else if($line[Koniec])
                $dostepnosc=slownik(17,$lang);
            else if($line[CzteryTygodnie])
                $dostepnosc=slownik(214,$lang)." ".$line[CzteryTygodnie]." ".$tydzien;
            else
                $dostepnosc=slownik(246,$lang);
                
            //$tresc=$tresc."\t\t<dostepnosc>$dostepnosc</dostepnosc>\n";
            
            if(file_exists("../katalog/foto/$line[Id].jpg"))
                {
                $tresc=$tresc."\t\t<imgs><main url=\"http://staviori.biz/katalog/foto/$line[Id].jpg\"/></imgs>\n";
                $prevFoto=$line[Id];
                $prevSymbol=$line[Symbol];
                }
            else if($line[Symbol]==$prevSymbol)
                $tresc=$tresc."\t\t<imgs><main url=\"http://staviori.biz/katalog/foto/$prevFoto.jpg\"/></imgs>\n";
                
            $tresc=$tresc."\t</o>\n";
            //if(++$i>10)break;
            }
        $tresc=$tresc."</group>\n</offers>\n";
        mysql_close($link); 
        $myFile = "../sklep/xml/ceneo.xml";
  		$fh = fopen($myFile, 'w') or die("can't open file");
		fwrite($fh, $tresc);
		fclose($fh);
		print "Gotowe, link: <a href='$myFile'>http://staviori.pl/xml/ceneo.xml</a><br><br><br>";
        }    
        
    if(!empty($_POST[nokaut]))
        {
        $lang=$_POST[lang];
        switch($lang)
            {            
            case "CZ":  $lpole="Cz1"; $lopis="OpisCZ"; break;
            case "EN":  $lpole="En1"; $lopis="OpisEN"; break;
            case "PL":  $lpole="Pl1"; $lopis="Opis";   break;
            }
        $tresc="<?xml version=\"1.0\" encoding=\"ISO-8859-2\"?>\n
                <!DOCTYPE nokaut SYSTEM \"http://www.nokaut.pl/integracja/nokaut.dtd\">\n
                <nokaut>\n
                <offers>\n";
                
        $link = dblogin();
        $query = "SELECT * FROM KATRODZAJE";
        $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $kategorie = array(array());
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
            {
            $kategorie["PL"][$line[Nr]]=$line[Nazwa];
            $kategorie["CZ"][$line[Nr]]=$line[NazwaCZ];
            }
            
        $query = "SELECT * FROM KATTYPY where NrRodz<100";
        $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $podkategorie = array(array());
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
            {
            $i=$line[NrRodz]*100+$line[Nr];
            $podkategorie["PL"][$i]=$line[Nazwa];
            $podkategorie["CZ"][$i]=$line[NazwaCZ];
            }    
            
        $query = "SELECT * FROM PRODUKTY WHERE Pokaz=1 and not Tymczasowy and not Akcesoria order by Symbol";
        $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $i=0;
        $prevFoto=0;
        $prevSymbol="";
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
            {
            $rozm = "";
            if($line[Typ]==1)
                $rozm=" $line[Rozmiar]cm";
            $tresc=$tresc."\t<offer>\n\t\t<id>$line[Id]</id>\n\t\t<name><![CDATA[$line[Symbol]$rozm]]></name>\n";
            //if($line[Typ]==1)
              //  $tresc=$tresc."\t\t<rozmiar>$line[Rozmiar]</rozmiar>\n";
            $tresc=$tresc."\t\t<price>$line[Brutto]</price>\n";
            if($line[Promocja]>0)
                $tresc=$tresc."\t\t<oldprice>$line[Promocja]</oldprice>\n";
            
            $nrkat=(int)($line[Rodzaj]/100);
            $rodz=$line[Rodzaj];
            
            $excl="";
            if($nrkat==8)
                $excl="-exclusive";
                
            $tresc=$tresc."\t\t<url><![CDATA[http://staviori.pl/product.php?id=$line[Id]]]></url>\n";
            
            $tresc=$tresc."\t\t<category><![CDATA[Bizuteria/".$kategorie[$lang][$nrkat]."/".$podkategorie[$lang][$rodz]."]]></category>\n";
            //$tresc=$tresc."\t\t<podkategoria>".$podkategorie[$lang][$rodz]."</podkategoria>\n";
            
            $opis=opis($line[Id],$lang,-1,$line[Pytac],$line[Rozmiar]);
            $opis=$opis." ".$line[$lopis];
            $opis=str_replace("<font color=#C8B560>", "", $opis); 
            $opis=str_replace("<font color=orange>", "", $opis);
            $opis=str_replace("</font>", "", $opis); 
            $opis=str_replace("<b>", "", $opis); 
            $opis=str_replace("</b>", "", $opis); 
            $opis=str_replace("<br>", " ", $opis); 
            
            $tresc=$tresc."\t\t<description><![CDATA[$opis]]></description>\n";
            
            $dostepnosc="";
            $tydzien="";
           	if($line[CzteryTygodnie])
           	    switch($line[CzteryTygodnie])
           	        {
           	        case 1: $tydzien=slownik(473,$l);   break;
           	        case 2: case 3: case 4:
           	                $tydzien=slownik(474,$l);   break;
           	        default: $tydzien=slownik(475,$l);   break;
           	        }
            if($line[DataUsun]>0)
                $dostepnosc=slownik(244,$lang);
            else if($line[Koniec])
                $dostepnosc=slownik(17,$lang);
            else if($line[CzteryTygodnie])
                $dostepnosc=slownik(214,$lang)." ".$line[CzteryTygodnie]." ".$tydzien;
            else
                $dostepnosc=slownik(246,$lang);
                
            //$tresc=$tresc."\t\t<dostepnosc>$dostepnosc</dostepnosc>\n";
            
            if(file_exists("../katalog/foto/$line[Id].jpg"))
                {
                $tresc=$tresc."\t\t<image><![CDATA[http://staviori.biz/katalog/foto/$line[Id].jpg]]></image>\n";
                $prevFoto=$line[Id];
                $prevSymbol=$line[Symbol];
                }
            else if($line[Symbol]==$prevSymbol)
                $tresc=$tresc."\t\t<image><![CDATA[http://staviori.biz/katalog/foto/$prevFoto.jpg]]></image>\n";
                
            $tresc=$tresc."\t</offer>\n";
            //if(++$i>10)break;
            }
        $tresc=$tresc."</offers>\n</nokaut>";
        mysql_close($link); 
        $myFile = "../sklep/xml/nokaut.xml";
  		$fh = fopen($myFile, 'w') or die("can't open file");
		fwrite($fh, $tresc);
		fclose($fh);
		print "Gotowe, link: <a href='$myFile'>http://staviori.pl/xml/nokaut.xml</a><br><br><br>";
        }       
    
    
    print "<form method='post'>Lokalizacja: http://staviori.biz/download/<input type='text' name='plik' value='katalog'>.xml<br><br><input type='hidden' name='lang' value='PL'><input type='hidden' name='waluta' value='PLN'><input type='submit' name='generuj' value='Generuj plik xml'></form>
    <form method='post'>Lokalizacja: http://staviori.biz/download/<input type='text' name='plik' value='Q'>.xml<br><br>Rodzaj zestawienia:<input type='text' name='nrzest' value='4' size=2> <input type='hidden' name='lang' value='PL'><input type='submit' name='generujnrzest' value='Generuj plik xml'></form>
    <form method='post'>Lokalizacja: http://staviori.biz/download/<input type='text' name='plik' value='katalogCZ'>.xml<br><br><input type='hidden' name='lang' value='CZ'><input type='hidden' name='waluta' value='CZK'><input type='submit' name='generuj' value='Generuj plik xml CZ [CZK]'></form>
    <form method='post'>Lokalizacja: http://staviori.biz/download/<input type='text' name='plik' value='katalogEN'>.xml<br><br><input type='hidden' name='lang' value='EN'><input type='hidden' name='waluta' value='EUR'><input type='submit' name='generuj' value='Generuj plik xml EN [EUR]'></form>
    <form method='post'>Lokalizacja: http://staviori.pl/xml/ceneo.xml<br><br><input type='hidden' name='lang' value='PL'><input type='submit' name='ceneo' value='Generuj plik xml - ceneo'></form>
    <form method='post'>Lokalizacja: http://staviori.pl/xml/nokaut.xml<br><br><input type='hidden' name='lang' value='PL'><input type='submit' name='nokaut' value='Generuj plik xml - nokaut'></form>";
        
}
?>
