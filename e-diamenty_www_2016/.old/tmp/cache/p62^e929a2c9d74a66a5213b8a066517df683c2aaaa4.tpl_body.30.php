<?php /*%%SmartyHeaderCode:109132658755565953124dd8-87740475%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e929a2c9d74a66a5213b8a066517df683c2aaaa4' => 
    array (
      0 => 'tpl_body:30',
      1 => 1408477162,
      2 => 'tpl_body',
    ),
  ),
  'nocache_hash' => '109132658755565953124dd8-87740475',
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5617589797fab0_52238705',
  'has_nocache_code' => true,
  'cache_lifetime' => 3600,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5617589797fab0_52238705')) {function content_5617589797fab0_52238705($_smarty_tpl) {?>
<body>
<div id="top">
  <div id="top-inner"> <a href="index.php"><img src="graf/logo_e-diamenty_main_150x150.png" width="150" height="150" alt="e-diamenty" /></a>
    <ul id="top-face-twitter">
      <li class="top-face"> <a href="#"><!--aa--></a> </li>
      <li class="top-line"><a href="#"><!--aa--></a></li>
      <li class="top-twitter"><a href="#"><!--aa--></a></li>
    </ul>
  </div>
</div>
<div id="all">
  <nav id="main-menu"> <ul><li><a href="http://e-diamenty.pl/diamenty/"><span>Diamenty</span></a></li><li><a href="http://e-diamenty.pl/gemmologia/"><span>Gemmologia</span></a></li><li><a href="http://e-diamenty.pl/aktualnosci/"><span>Aktualności</span></a></li><li><a href="http://e-diamenty.pl/kontakt/"><span>Kontakt</span></a></li></ul> </nav>
  <?php CMS_Content_Block::smarty_internal_fetch_contentblock(array(),$_smarty_tpl); ?>
  <div id="content">  <a href="http://e-diamenty.pl/news/4/61/Grupa-Leviev-osiagnela-porozumienie-z-Angola/" title="Grupa Leviev osiągnęła porozumienie z Angolą"> Grupa Leviev osiągnęła porozumienie z Angolą<div class="archiwum-news"><article class="news-left"><h2>06/25/14</h2><p>Grupa Leviev osiągnęła porozumienie z Angolą, która pozwala sprzedawać firmie nieoszlifowane diamenty z kopalni Luminas na wolnym rynku, a nie jedynie do wybranych handlowców w Chinach i Dubaju; według osób wtajemniczonych cytowanych przez Bloomberg'a.</p> </article><article class="news-right"><h3>Grupa Leviev osiągnęła porozumienie z Angolą</h3><p>Raport zauważa, że Grupa Leviev skorzysta na sprzedaży surowych diamentów po cenach wolnorynkowych, podczas, gdy dotychczasowe porozumienie owocowało sprzedażą po obniżonych cenach. 
Wtajemniczeni uważają, że ten deal spowoduje, że ceny za nieoszlifowane diamenty będą wyższe o ok. 50% dla grupy Leviev. Ogólnie rzecz biorąc, firmy wydobywające diamenty operują w Angoli poprzez partnerstwo z Endiana i nieoszlifowane kamienie są sprzedawane przez Sodiam. Jednak Angola wyraziła zainteresowanie rozluźnieniem regulacji, które mogą zniechęcać do inwestycji w sektorze wydobywczym.
<br /><br />
Oficjalne dane dotyczące produkcji diamentów na rok 2013 nie został jeszcze opublikowane, jednak zgodnie z Kimberley Process za rok 2012 Angola była czwartym największym producentem diamentów o... </article><div class="clear"></div></div></a>  <a href="http://e-diamenty.pl/news/3/61/De-Beers-oglosi-wymagania-Sightholderow-w-lipcu/" title="De Beers ogłosi wymagania Sightholder&oacute;w w lipcu"> De Beers ogłosi wymagania Sightholderów w lipcu<div class="archiwum-news"><article class="news-left"><h2>06/24/14</h2><p>"De Beers zaprezentuje w lipcu swoje wymagania dotyczące wniosków Sightholderów na kolejny kontrakt na okres trzyletni", powiedziało w poniedziałek kierownictwo firmy w Izraelu. Podkreślili przy tym, że nacisk zostanie położony na zapewnienie większej przejrzystości i ładu... </article><article class="news-right"><h3>De Beers ogłosi wymagania Sightholderów w lipcu</h3><p>"Czuliśmy, że musimy być bardziej elastyczni i uprościć proces, ale nie bez zachowania pewnych rygorów"- powiedział Paul Rowley, wiceprezes De Beers ds. globalnej sprzedaży Sightholderów na spotkaniu z prasą handlową w Tel Awiwie. "Bardzo by nam zależało, by marki Sightholderów stały się coraz bardziej wytrzymałe. Poprzez nasze zasady dobrych praktyk i ładu finansowego szukamy większej przejrzystości i aby upewnić się, że Sightholderzy są na wystarczająco silnej pozycji by weszli z nami na nowy poziom".</p>

<p>Nowy okres kontraktów rozpoczyna się w kwietniu 2015r. i potrwa do marca 2018r. De Beers finalizuje obecnie swoje wymagania przed przedstawieniem ich Sightholderom przy następnej aukcji, która ma się odbyć między 14 a 18 lipca 2014r. Rowley powiedział, że zbieranie aplikacji... </article><div class="clear"></div></div></a>  <a href="http://e-diamenty.pl/news/2/61/swiatowa-Rada-Zlota-ma-na-celu-modernizacje-London-Gold-Fix/" title="Światowa Rada Złota ma na celu modernizację London Gold Fix"> Światowa Rada Złota ma na celu modernizację London Gold Fix<div class="archiwum-news"><article class="news-left"><h2>06/18/14</h2><p>Spotkanie Rady odbędzie się 7 lipca w Londynie aby badać sposoby reformowania i aktualizowania London Gold Fix oraz globalne tendencje dla tego surowca. Rada stwierdziła, że zamierza zaprosić przedstawicieli banków, rafinerii, funduszy inwestycyjnych, sponsorów produktów... </article><article class="news-right"><h3>Światowa Rada Złota ma na celu modernizację London Gold Fix</h3><p>Natalie Dempster, dyrektor zarządzający ds. Banków Centralnych i polityki publicznej dla Światowej Rady Złota powiedział, że proces fixingu został wynaleziony prawie sto lat temu, więc nie dziwi, że to musi się zmieniać, aby sprostać dzisiejszym wymaganiom rynku dla większej regulacji, przejrzystości i technologii. Modernizacja jest niezbędna w celu utrzymania zaufania w branży. Być może stanie się to przez reformę fixingu by dostosować go do zasad IOSCO albo może powstać nowy punkt odniesienia do cen.</p>
<p>Celem forum będzie zapewnienie, że pełny zakres analiz i perspektyw rynkowych jest dyskutowany, rozumiany i wywierany jest wpływ na ewentualne zmiany. </p>
<p>"Każda reforma, musi służyć potrzebom wszystkich uczestników rynku i spełniać dzisiejsze wymagania dotyczące... </article><div class="clear"></div></div></a>  <a href="http://e-diamenty.pl/de-beers-organizuje-aukcje-za-ponad-640-mln-dolarow/" title="De Beers organizuje aukcje za ponad 640 mln $"> De Beers organizuje aukcje za ponad 640 mln $<div class="archiwum-news"><article class="news-left"><h2>06/16/14</h2><p>De Beers zakończył czerwcową aukcje na poziomie 640 mln $, popyt na nieoszlifowane kamienie pozostał na stałym poziomie. Ceny i składy tzw. boxów pozostały w znacznym stopniu niezmienione od ostatniej aukcji, choć sightholderzy zgłosili kilka korekt dotyczących cen na... </article><article class="news-right"><h3>De Beers organizuje aukcje za ponad 640 mln $</h3><p>Podczas gdy popyt na kamienie nieoszlifowane był w miarę solidny, sightholderzy wyrażają nieustanne obawy związane ze sztywnymi marżami produkcji ze względu na utrzymujące się wysokie ceny surowca w stosunku do szlifowanych diamentów. Rapaport szacuje, że ceny nieoszlifowanych diamentów wzrosły pomiędzy 7% a 10% od początku roku. Dealerzy poinformowali, że kamienie były dalej sprzedawane na rynku wtórnym z ok. 5% marżą, z około połową towaru sprzedaną na kredyt, a połową za gotówkę.</p>
 </article><div class="clear"></div></div></a> 
    <div class="clear"></div>
  </div>
  <div class="clear"></div><footer id="stopka"> <div class="footer-img"> <img src="graf/logo_e-diamenty_small_33x22.png" width="33" height="22" alt="diament"> </div><div class="footer-text"><div style="float:left; margin-left:30px;"><p><a href="dlaczego-warto-kupowac-na-e-diamenty">Dlaczego warto kupować na e-diamenty.pl?</a></p><p><a href="aktualnosci/">Aktualności:</a> <a href="archiwum">archiwum</a></p></div><div style="float:left; margin-left:50px;"><p><a href="kontakt">Kontakt:</a> +48 61 095 654 | <a href="mailto:kontakt@e-diamenty.pl">kontakt@e-diamenty.pl</a></p><p><a href="regulamin">Regulamin:</a> <a href="regulamin/#regulamin-cookies">regulamin cookies</a></p></div></div> </footer>
</div>
</body>
</html><?php }} ?>
