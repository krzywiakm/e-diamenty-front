<?php /*%%SmartyHeaderCode:15311080505553425de0bea2-82935362%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4d9d4cc6f263abe5dbd4656bfc2d456ff8f859b4' => 
    array (
      0 => 'globalcontent:gemmologiaaside',
      1 => 1403564850,
      2 => 'globalcontent',
    ),
  ),
  'nocache_hash' => '15311080505553425de0bea2-82935362',
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_56178441640537_38626168',
  'has_nocache_code' => false,
  'cache_lifetime' => 3600,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56178441640537_38626168')) {function content_56178441640537_38626168($_smarty_tpl) {?><a href="gemmologia#czystosc">
<article class="news-right">
  <h2>Czystość (clarity):</h2>
  <p>Nieodłączna cechą kamieni szlachetnych są mikroskopijne inkluzje, zwane też „wrostkami”. Diamenty posiadają znamiona wewnętrzne (nazywane inkluzjami), jak również znamiona zewnętrzne (zwane skazami)...</p>
</article>
</a> <a href="gemmologia#masa">
<article class="news-right">
  <h2>Masa (carat):</h2>
  <p>Jednym z najważniejszych kryteriów przy wyborze diamentu jest jego masa, którą podaje się w karatach. Jeden karat (1 ct) to 0,2 grama...</p>
</article>
</a> <a href="gemmologia#barwa">
<article class="news-right">
  <h2>Barwa (color):</h2>
  <p>Zdecydowana większość wydobywanych diamentów jest zabarwiona. Wyróżnia się bardzo rzadkie diamenty o barwach fantazyjnych...</p>
</article>
</a> <a href="gemmologia#szlif">
<article class="news-right">
  <h2>Szlif (cut):</h2>
  <p>Pełny, klasyczny szlif brylantowy zawiera nie mniej niż 57 tzw. „faset”: 33 w koronie i 24 w podstawie...</p>
</article>
</a><?php }} ?>
