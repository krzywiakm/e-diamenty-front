<?php /*%%SmartyHeaderCode:19415908615553425dc89235-89974515%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ad83cfdd496e0d74150f5e8179bf0ea1e88e3ac2' => 
    array (
      0 => 'tpl_body:24',
      1 => 1408477610,
      2 => 'tpl_body',
    ),
  ),
  'nocache_hash' => '19415908615553425dc89235-89974515',
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5617844167f828_56156371',
  'has_nocache_code' => true,
  'cache_lifetime' => 3600,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5617844167f828_56156371')) {function content_5617844167f828_56156371($_smarty_tpl) {?>
<body>
<div id="top">
<div id="top-inner"> <a href="index.php"><img src="graf/logo_e-diamenty_main_150x150.png" width="150" height="150" alt="e-diamenty" /></a>
<ul id="top-face-twitter">
              <li class="top-face"> <a href="#"><!--aa--></a> </li>
              <li class="top-line"><a href="#"><!--aa--></a></li>
              <li class="top-twitter"><a href="#"><!--aa--></a></li>
</ul>
</div>
        </div>
<div id="all">
          <nav id="main-menu"> <ul><li><a href="http://e-diamenty.pl/diamenty/"><span>Diamenty</span></a></li><li><a href="http://e-diamenty.pl/gemmologia/"><span>Gemmologia</span></a></li><li><a href="http://e-diamenty.pl/aktualnosci/"><span>Aktualności</span></a></li><li><a href="http://e-diamenty.pl/kontakt/"><span>Kontakt</span></a></li></ul> </nav>
          <div id="content-left">


















<div id="da-slider" class="da-slider">
  <div class="da-slide"> <?php CMS_Content_Block::smarty_internal_fetch_contentblock(array(),$_smarty_tpl); ?>
    <h2>Porównaj ceny diamentów</h2>
    <p>Wybierz kształt, masę, zakres cen<br />
      znajdź idealne diamenty inwestycyjne.</p>
    <a href="diamenty" class="da-link">Zobacz więcej...</a>
    <div class="da-img"><img src="graf/slider_1_graf_410x280.png" alt="Wybierz kształt, masę, zakres cen znajdź idealne diamenty inwestycyjne." /></div>
  </div>
  <div class="da-slide">
    <h2>Dlaczego e-diamenty.pl?</h2>
    <p>e-diamenty.pl jest platformą, która pozwala pominąć pośredników w handlu diamentami. </p>
    <a href="dlaczego-warto-kupowac-na-e-diamenty" class="da-link">Zobacz więcej...</a>
    <div class="da-img"><img src="graf/slider_2_graf_410x280.png" alt="e-diamenty.pl jest platformą, która pozwala pominąć pośredników w handlu diamentami" /></div>
  </div>
  <nav class="da-arrows"> <span class="da-arrows-prev"></span> <span class="da-arrows-next"></span> </nav>
</div>











  </div>
          <aside id="content-right"><a href="gemmologia#czystosc">
<article class="news-right">
  <h2>Czystość (clarity):</h2>
  <p>Nieodłączna cechą kamieni szlachetnych są mikroskopijne inkluzje, zwane też „wrostkami”. Diamenty posiadają znamiona wewnętrzne (nazywane inkluzjami), jak również znamiona zewnętrzne (zwane skazami)...</p>
</article>
</a> <a href="gemmologia#masa">
<article class="news-right">
  <h2>Masa (carat):</h2>
  <p>Jednym z najważniejszych kryteriów przy wyborze diamentu jest jego masa, którą podaje się w karatach. Jeden karat (1 ct) to 0,2 grama...</p>
</article>
</a> <a href="gemmologia#barwa">
<article class="news-right">
  <h2>Barwa (color):</h2>
  <p>Zdecydowana większość wydobywanych diamentów jest zabarwiona. Wyróżnia się bardzo rzadkie diamenty o barwach fantazyjnych...</p>
</article>
</a> <a href="gemmologia#szlif">
<article class="news-right">
  <h2>Szlif (cut):</h2>
  <p>Pełny, klasyczny szlif brylantowy zawiera nie mniej niż 57 tzw. „faset”: 33 w koronie i 24 w podstawie...</p>
</article>
</a></aside>

<div class="clear"></div>
          <footer id="stopka"> <div class="footer-img"> <img src="graf/logo_e-diamenty_small_33x22.png" width="33" height="22" alt="diament"> </div><div class="footer-text"><div style="float:left; margin-left:30px;"><p><a href="dlaczego-warto-kupowac-na-e-diamenty">Dlaczego warto kupować na e-diamenty.pl?</a></p><p><a href="aktualnosci/">Aktualności:</a> <a href="archiwum">archiwum</a></p></div><div style="float:left; margin-left:50px;"><p><a href="kontakt">Kontakt:</a> +48 61 095 654 | <a href="mailto:kontakt@e-diamenty.pl">kontakt@e-diamenty.pl</a></p><p><a href="regulamin">Regulamin:</a> <a href="regulamin/#regulamin-cookies">regulamin cookies</a></p></div></div> </footer>
        </div>

</body>
</html><?php }} ?>
