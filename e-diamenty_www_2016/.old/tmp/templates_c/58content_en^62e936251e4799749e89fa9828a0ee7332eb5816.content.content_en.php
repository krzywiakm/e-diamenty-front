<?php /* Smarty version Smarty-3.1.16, created on 2015-05-13 14:38:57
         compiled from "content:content_en" */ ?>
<?php /*%%SmartyHeaderCode:1202164697555345e1467499-99926092%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '62e936251e4799749e89fa9828a0ee7332eb5816' => 
    array (
      0 => 'content:content_en',
      1 => 1404151785,
      2 => 'content',
    ),
  ),
  'nocache_hash' => '1202164697555345e1467499-99926092',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_555345e146b309_76779666',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_555345e146b309_76779666')) {function content_555345e146b309_76779666($_smarty_tpl) {?><div class="gemologia-full-news">
  <article class="news-left">
    <h1>Ocena wartości diamentu - 4C</h1>
    <h2 id="czystosc">Czystość:<br />
      <span class="smaller-sec-header">(Clarity)</span></h2>
  </article>
  <article class="news-right"> <img src="graf/Czystosc_580x580.jpg" width="580" height="580" alt="Czystość - gemmologia">
    <p>Nieodłączna cechą kamieni szlachetnych są mikroskopijne inkluzje, zwane też „wrostkami”. Diamenty posiadają znamiona wewnętrzne (nazywane inkluzjami), jak również znamiona zewnętrzne (zwane skazami), które czynią każdy kamień wyjątkowym i niepowtarzalnym.  Powstały w procesie formowania się kamieni. Oglądanie kamienia pod dziesięciokrotnym powiększeniem pozwala na określenie klasy czystości.Diament tym cenniejszy im mniej posiada inkluzji i skaz. Idealnie czyste brylanty są rzadkością. Szczególnym zainteresowaniem cieszą się te kamienie, których kryterium określającym czystość mieszczą się w przedziale SI – małe inkluzje. Jeżeli czystość oznaczona jest jako SI lub wyższa, „wrostki” zazwyczaj nie są widoczne gołym okiem. Wyróżnia się następujące stopnie czystości:<br />
      <br />
      <strong>IF</strong> - (InternallyFlawless) - czyste, wolne od znamion wewnętrznych w specjalistycznym badaniu wykazuje jedynie znamiona zewnętrzne. <br />
      <br />
      <strong>VVS</strong> - (VeryVery Small Inclusions) - bardzo, bardzo małe zanieczyszczenia (inkluzje) bardzo trudne do dostrzeżenia pod 10-krotnym powiększeniem.<br />
      <br />
      <strong>VS</strong> - (Very Small Inclusions) - nieznaczne, drobne znamiona wewnętrzne,trudne do dostrzeżenia w badaniu przy dziesięciokrotnym powiększeniu.<br />
      <br />
      <strong>SI</strong> - (Small Inclusions) - małe inkluzje, czyli dostrzegalne znamiona wewnętrzne widoczne w badaniu przy 10-krotnym powiększeniu. W Polsce sąto najczęściej spotykane kamienie, polecane są klientom szukającym równowagi pomiędzy wartością estetyczną pierścionka a ceną.<br />
      <br />
      <strong>I1, I2, I3</strong> (Included 1, 2, 3. Zamiennie używa się też nazw P1, P2, P3) - kamienie o niskiej klasie czystości z zanieczyszczeniami widocznymi gołym okiem dla eksperta. Polecane osobom, dla których wysokość ceny jest ważniejsza niż wartość estetyczna  lub dla tych, którzy za tą sama cenę chcą kupić większy diament. Diamenty o stopniu czystości poza I3 (lub P3) noszą nazwę odrzutów oraz uważa się, że są poza skalą czystości.</p>
  </article>
  <div class="clear"></div>
</div>
<div class="gemologia-full-news">
  <article class="news-left">
    <h2 id="masa">Masa:<br />
      <span class="smaller-sec-header">(Carat)</span></h2>
  </article>
  <article class="news-right">
    <p>Jednym z najważniejszych kryteriów przy wyborze diamentu jest jego masa, którą podaje się w karatach. Jeden karat (1 ct) to 0,2 grama. Karat dzielony jest na 100 punktów,  czyli diament mający 50 punktów ważyć będzie 0,1 grama. Prawidłowe wartości dla kamienia 1-karatowego o szlifie brylantowym okrągłym zakładają wymiary kamienia 6,5 mm średnicy oraz 3,9 mm wysokości. Kamień pół karatowy (0,5 ct) powinien posiadać następujące wymiary: 5,2 mm średnicy oraz 3,1 mm wysokości.</p>
  </article>
  <div class="clear"></div>
</div>
<div class="gemologia-full-news">
  <article class="news-left">
    <h2 id="barwa">Barwa:<br />
      <span class="smaller-sec-header">(Color)</span></h2>
  </article>
  <article class="news-right">
    <p>Zdecydowana większość wydobywanych diamentów jest zabarwiona. Wyróżnia się bardzo rzadkie diamenty o barwach fantazyjnych, m.in. niebieskiej, różowej, pomarańczowej oraz diamenty o barwach typowych tzn. bezbarwnych po różne odcienie barwy żółtej, brązowej lub szarej. Pośród diamentów o barwach typowych, które oceniane są w skali od D do Z. Najrzadsze i najbardziej cenione są kamienie o barwie D, czyli całkowicie bezbarwne. Skala określająca stopień bezbarwności diamentów została ustalona przez GEMMOLOGICAL INSTITUTE OF AMERICA (GIA) i jest powszechnie stosowana. Różnice w typowych barwach diamentów są bardzo subtelne, dlatego stopień bezbarwności określa się w specjalnych warunkach oświetleniowych oraz porównując do wzorców. Ta skala nie określa diamentów o barwie fantazyjnej.</p>
    <img src="graf/gemologia_kolor_580x265.jpg" width="580" height="265" alt="Barwa - gemmologia"> </article>
  <div class="clear"></div>
</div>
<div class="gemologia-full-news">
  <article class="news-left">
    <h2 id="szlif">Szlif:<br />
      <span class="smaller-sec-header">(Cut)</span></h2>
  </article>
  <article class="news-right">
    <p>Pełny, klasyczny szlif brylantowy zawiera nie mniej niż 57tzw. „faset”: 33 w koronie i 24w podstawie. Tylko dzięki właściwemu oszlifowaniu diamentu można dostrzec jego wspaniałą brylancję. To zjawisko powstaje w wyniku całkowitego, wewnętrznego odbicia światła od zewnętrznych powierzchni (faset). Podczas oceny szlifu dokładniej analizie podlegają kształt, proporcje oraz wykończenie szlifu. Ocenia się je w skali: Wspaniały (Excellent) Bardzo Dobry (VeryGood), Dobry (Good), Średni (Fair) i Słaby (Poor). Tylko wyjątkowo wykonany szlif może sprawić, by światło przenikające przez diament uwalniało jego wewnętrzny ogień. Wśród kamieni można wyróżnić kamienie o szlifie brylantowym okrągłym (czyli brylanty) oraz o kształtach fantazyjnych, takich jak: markiza, owal, gruszka, serce, princessa.</p>
    <img src="graf/gemologia_budowa_580x340.jpg" width="580" height="340" alt="Szlif - gemologia"> </article>
  <div class="clear"></div>
</div><?php }} ?>
