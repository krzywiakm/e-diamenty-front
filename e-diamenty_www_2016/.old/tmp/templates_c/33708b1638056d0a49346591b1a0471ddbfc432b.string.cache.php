<?php /* Smarty version Smarty-3.1.16, created on 2015-05-13 14:23:57
         compiled from "33708b1638056d0a49346591b1a0471ddbfc432b" */ ?>
<?php /*%%SmartyHeaderCode:7447647675553425de8d2b3-62331058%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '33708b1638056d0a49346591b1a0471ddbfc432b' => 
    array (
      0 => '33708b1638056d0a49346591b1a0471ddbfc432b',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '7447647675553425de8d2b3-62331058',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5553425dea0095_72449355',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5553425dea0095_72449355')) {function content_5553425dea0095_72449355($_smarty_tpl) {?>html, body, div, span, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, abbr, address, cite, code, del, dfn, em, img, ins, kbd, q, samp, small, strong, sub, sup, var, b, i, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, figcaption, figure, footer, header, hgroup, menu, nav, section, summary, time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	outline: 0;
	font-size: 100%;
	vertical-align: baseline;
	background: transparent;
}
body {
	line-height: 1;
}
article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {
	display: block;
}
ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after, q:before, q:after {
	content: '';
	content: none;
}
a {
	margin: 0;
	padding: 0;
	font-size: 100%;
	vertical-align: baseline;
	background: transparent;
}
/* change colours to suit your needs */
ins {
	background-color: #ff9;
	color: #000;
	text-decoration: none;
}
/* change colours to suit your needs */
mark {
	background-color: #ff9;
	color: #000;
	font-style: italic;
	font-weight: bold;
}
del {
	text-decoration: line-through;
}
abbr[title], dfn[title] {
	border-bottom: 1px dotted;
	cursor: help;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
/* change border colour to suit your needs */
hr {
	display: block;
	height: 1px;
	border: 0;
	border-top: 1px solid #cccccc;
	margin: 1em 0;
	padding: 0;
}
input, select {
	vertical-align: middle;
}









body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #FFF;
}
#all {
	width: 1020px;
	margin: -65px auto 0 auto;
	background:#FFF;
}
h1, h2, h3, h4, h5, h6 {
	font-family: 'PT Sans', sans-serif;
	font-weight:400;
}
h1, h2, h3, h4, h5, h6, p {
	padding: 15px;
}
h1 {
	font-size: 45px
}
h2 {
	font-size:26px;
}
h3 {
	font-size: 25px
}
a {height:auto;}
a:link {
	color: #FFF;
	text-decoration: none;
}
a:visited {
	color: #FFF;
	text-decoration: none;
}
a:hover {
	color: #FFF;
	text-decoration: none;
}
a:active {
	color: #FFF;
}
#top {
	background: url(../../graf/bg_top_main_1920x240.jpg) center;
	height: 240px;
	text-align: center;
}

#top-inner {
	width: 1020px;
	margin: 0 auto;
	height: 175px;
	position: relative;
}
#top-face-twitter {
	position: absolute;
	top: 38px;
	right: 0px;
}
#top-face-twitter li {
	float: left;
}


.top-face {background:url(../../graf/ico_facebook_90x48.png); height:24px; width:90px;}
.top-line {background:url(../../graf/ico_fb_twitter_vr_24x1.png); height:24px; width:1px;}
.top-twitter {background:url(../../graf/ico_twitter_90x48.png); height:24px;width:90px;}


.top-face:hover {background-position:0 -24px;cursor:pointer;}
.top-twitter:hover {background-position:0 -24px;cursor:pointer;}











#top img {
	padding-top: 10px;
	height:150px;
}

.clear{clear:both;height:0;}




#main-menu {
	text-align: center;
	background: url(../../graf/bg_menu_1020x50.jpg);
	height: 50px;
	font-family: 'PT Sans', sans-serif;
	font-size: 18px;
	z-index:5;
}
#main-menu li {
	float: left;
	width: 255px;
	height: 50px;
	line-height: 50px;
	color: #FFF;
	cursor: pointer;
	
}
#main-menu li a {
	-webkit-transition: all .40s;
	-moz-transition: all .40s;
	-ms-transition: all .40s;
	-o-transition: all .40s;
	transition: all .40s;
}
#main-menu li a:hover {
	background-color: rgba(255, 255, 255, 0.2);
	-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#33FFFFFF,endColorstr=#33FFFFFF)"; /* IE8 */
 filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#33FFFFFF, endColorstr=#33FFFFFF);   /* IE6 & 7 */
	zoom: 1;
}









#main-menu ul li a {
	display: block;
}
#main-menu .active {
	background-color: rgba(255, 255, 255, 0.2);
	-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#33FFFFFF,endColorstr=#33FFFFFF)"; /* IE8 */
 filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#33FFFFFF, endColorstr=#33FFFFFF);   /* IE6 & 7 */
	zoom: 1;
}
.active a:hover {
	background-color: rgba(255, 255, 255, 0)!important;
	-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#00FFFFFF,endColorstr=#00FFFFFF)"; /* IE8 */
 filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#00FFFFFF, endColorstr=#00FFFFFF);   /* IE6 & 7 */
	zoom: 1;
}














#content-left {
	float: left;
	width: 670px;
	height: 512px;
}

#content-left .wyszukiwarka-text {
	text-align: right;
	margin-right: 50px;
}






#content-right {
	float: left;
	width: 350px;
	height: 512px;
	background: #CCC;
}
#content-right a, #content-right a:hover {
	color: #000;
}






.news-right {
	height: 128px;
	width:350px;
	
	background:url(../../graf/bg_news_350x256.jpg);
}


.news-right:hover {
	background-position:0 -128px;
}

.news-right h2 {
	padding:25px 20px 0 20px;
	font-size:20px;
}
.news-right p {
	padding:5px 20px 0 20px;
	color: #666;
	text-align:justify;

}



#stopka {
height:125px;


background-image: -webkit-gradient(
	linear,
	left top,
	left bottom,
	color-stop(0, #11141E),
	color-stop(1, #323845)
);
background-image: -o-linear-gradient(bottom, #11141E 0%, #323845 100%);
background-image: -moz-linear-gradient(bottom, #11141E 0%, #323845 100%);
background-image: -webkit-linear-gradient(bottom, #11141E 0%, #323845 100%);
background-image: -ms-linear-gradient(bottom, #11141E 0%, #323845 100%);
background-image: linear-gradient(to bottom, #11141E 0%, #323845 100%);


 filter: progid:DXImageTransform.Microsoft.Gradient(startColorstr='#11141E', endColorstr='#323845');

}
#stopka p {
	color:#5F6E80;
padding:3px 0 3px 0;

}
#stopka a{color:#5F6E80;}
#stopka a:hover{color:#DDD;}

.footer-img {float:left;margin:0 48px 0 48px;line-height:125px;}
.footer-text {float:left;margin:42px 25px 25px 25px;}














.da-slider{
	width:670px;
	height: 512px;
	position: relative;
	overflow: hidden;
	font-family: 'PT Sans', sans-serif;
	background: transparent url(../../graf/bg_slider_default_670x512.jpg) repeat 0% 0%;
	-webkit-transition: background-position 1s ease-out 0.3s;
	-moz-transition: background-position 1s ease-out 0.3s;
	-o-transition: background-position 1s ease-out 0.3s;
	-ms-transition: background-position 1s ease-out 0.3s;
	transition: background-position 1s ease-out 0.3s;
}
.da-slide{
	position: absolute;
	width: 100%;
	height: 100%;
	top: 0px;
	left: 0px;
	text-align: left;
}
.da-slide-current{
	z-index: 1000;
}
.da-slider-fb .da-slide{
	left: 100%;
}
.da-slider-fb  .da-slide.da-slide-current{
	left: 0px;
}
.da-slide h2,
.da-slide p,
.da-slide .da-link,
.da-slide .da-img{
	position: absolute;
	opacity: 0;
	left: 20%;
}
.da-slider-fb .da-slide h2,
.da-slider-fb .da-slide p,
.da-slider-fb .da-slide .da-link{
	left: 10%;
	opacity: 1;
}
.da-slider-fb .da-slide .da-img{
	left: 20%;
	opacity: 1;
}
.da-slide h2 {
	color: #fff;
	font-size: 38px;
	font-family: 'PT Sans', sans-serif;
	top: 80px;
	white-space: nowrap;
	z-index: 10;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.1);
	z-index:55;
}
.da-slide p {
	top: 120px;
	font-size: 22px;
	line-height: 26px;
	height: 80px;
	overflow: hidden;
	z-index:55;
}
.da-slide .da-img{
	text-align: center;
	width: 30%;
	top: 180px;
	height: 256px;
	line-height: 320px;
	left: 20%; 
}
.da-slide .da-link{
	top: 420px; /*depends on p height*/
	color: #fff;
	font-size: 18px;
	line-height: 30px;
	margin:0 15px 0 15px;
	width:auto;
	z-index:55;
}
.da-dots{
	width: 100%;
	position: absolute;
	text-align: center;
	left: 0px;
	bottom: 20px;
	z-index: 2000;
	-moz-user-select: none;
	-webkit-user-select: none;
}
.da-dots span{
	display: inline-block;
	position: relative;
	background:url(../../graf/slider_nav_empty_9x9.png);
	width: 9px;
	height: 9px;
	margin: 3px;
	cursor: pointer;
}
.da-dots span.da-dots-current:after{
	content: '';
	width: 9px;
	height: 9px;
	position: absolute;
	top: 0px;
	left: 0px;
	background: rgb(255,255,255);
	background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(47%,rgba(246,246,246,1)), color-stop(100%,rgba(237,237,237,1)));
	background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%);
	background: -o-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%);
	background: -ms-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%);
	background: linear-gradient(top, rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ededed',GradientType=0 );
}
.da-arrows{
	-moz-user-select: none;
	-webkit-user-select: none;
}
.da-arrows span{
	position: absolute;
	top: 50%;
	height: 30px;
	width: 30px;
	cursor: pointer;
	z-index: 2000;
	opacity: 0;
	-webkit-transition: opacity 0.4s ease-in-out-out 0.2s;
	-moz-transition: opacity 0.4s ease-in-out-out 0.2s;
	-o-transition: opacity 0.4s ease-in-out-out 0.2s;
	-ms-transition: opacity 0.4s ease-in-out-out 0.2s;
	transition: opacity 0.4s ease-in-out-out 0.2s;
}
.da-slider:hover .da-arrows span{
	opacity: 1;
}
.da-arrows span:after{
	content: '';
	position: absolute;
	width: 20px;
	height: 20px;
	top: 5px;
	left: 5px;
	background: transparent url(../../graf/arrows.png) no-repeat top left;

}

.da-arrows span.da-arrows-next:after{
	background-position: top right;
}
.da-arrows span.da-arrows-prev{
	left: 15px;
}
.da-arrows span.da-arrows-next{
	right: 15px;
}

.da-slide-current h2,
.da-slide-current p,
.da-slide-current .da-link{
	left: 10%;
	opacity: 1;
}
.da-slide-current .da-img{
	left: 20%;
	opacity: 1;
}
/* Animation classes and animations */

/* Slide in from the right*/
.da-slide-fromright h2{
	-webkit-animation: fromRightAnim1 0.6s ease-in-out 0.8s both;
	-moz-animation: fromRightAnim1 0.6s ease-in-out 0.8s both;
	-o-animation: fromRightAnim1 0.6s ease-in-out 0.8s both;
	-ms-animation: fromRightAnim1 0.6s ease-in-out 0.8s both;
	animation: fromRightAnim1 0.6s ease-in-out 0.8s both;
}
.da-slide-fromright p{
	-webkit-animation: fromRightAnim2 0.6s ease-in-out 0.8s both;
	-moz-animation: fromRightAnim2 0.6s ease-in-out 0.8s both;
	-o-animation: fromRightAnim2 0.6s ease-in-out 0.8s both;
	-ms-animation: fromRightAnim2 0.6s ease-in-out 0.8s both;
	animation: fromRightAnim2 0.6s ease-in-out 0.8s both;
}
.da-slide-fromright .da-link{
	-webkit-animation: fromRightAnim3 0.4s ease-in-out 1.2s both;
	-moz-animation: fromRightAnim3 0.4s ease-in-out 1.2s both;
	-o-animation: fromRightAnim3 0.4s ease-in-out 1.2s both;
	-ms-animation: fromRightAnim3 0.4s ease-in-out 1.2s both;
	animation: fromRightAnim3 0.4s ease-in-out 1.2s both;
}
.da-slide-fromright .da-img{
	-webkit-animation: fromRightAnim4 0.6s ease-in-out 0.8s both;
	-moz-animation: fromRightAnim4 0.6s ease-in-out 0.8s both;
	-o-animation: fromRightAnim4 0.6s ease-in-out 0.8s both;
	-ms-animation: fromRightAnim4 0.6s ease-in-out 0.8s both;
	animation: fromRightAnim4 0.6s ease-in-out 0.8s both;
}
@-webkit-keyframes fromRightAnim1{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-webkit-keyframes fromRightAnim2{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-webkit-keyframes fromRightAnim3{
	0%{ left: 110%; opacity: 0; }
	1%{ left: 10%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-webkit-keyframes fromRightAnim4{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 20%; opacity: 1; }
}

@-moz-keyframes fromRightAnim1{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-moz-keyframes fromRightAnim2{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-moz-keyframes fromRightAnim3{
	0%{ left: 110%; opacity: 0; }
	1%{ left: 10%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-moz-keyframes fromRightAnim4{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 20%; opacity: 1; }
}

@-o-keyframes fromRightAnim1{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-o-keyframes fromRightAnim2{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-o-keyframes fromRightAnim3{
	0%{ left: 110%; opacity: 0; }
	1%{ left: 10%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-o-keyframes fromRightAnim4{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 20%; opacity: 1; }
}

@-ms-keyframes fromRightAnim1{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-ms-keyframes fromRightAnim2{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-ms-keyframes fromRightAnim3{
	0%{ left: 110%; opacity: 0; }
	1%{ left: 10%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-ms-keyframes fromRightAnim4{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 20%; opacity: 1; }
}

@keyframes fromRightAnim1{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@keyframes fromRightAnim2{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@keyframes fromRightAnim3{
	0%{ left: 110%; opacity: 0; }
	1%{ left: 10%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@keyframes fromRightAnim4{
	0%{ left: 110%; opacity: 0; }
	100%{ left: 20%; opacity: 1; }
}
/* Slide in from the left*/
.da-slide-fromleft h2{
	-webkit-animation: fromLeftAnim1 0.6s ease-in-out 0.6s both;
	-moz-animation: fromLeftAnim1 0.6s ease-in-out 0.6s both;
	-o-animation: fromLeftAnim1 0.6s ease-in-out 0.6s both;
	-ms-animation: fromLeftAnim1 0.6s ease-in-out 0.6s both;
	animation: fromLeftAnim1 0.6s ease-in-out 0.6s both;
}
.da-slide-fromleft p{
	-webkit-animation: fromLeftAnim2 0.6s ease-in-out 0.6s both;
	-moz-animation: fromLeftAnim2 0.6s ease-in-out 0.6s both;
	-o-animation: fromLeftAnim2 0.6s ease-in-out 0.6s both;
	-ms-animation: fromLeftAnim2 0.6s ease-in-out 0.6s both;
	animation: fromLeftAnim2 0.6s ease-in-out 0.6s both;
}
.da-slide-fromleft .da-link{
	-webkit-animation: fromLeftAnim3 0.4s ease-in-out 1.2s both;
	-moz-animation: fromLeftAnim3 0.4s ease-in-out 1.2s both;
	-o-animation: fromLeftAnim3 0.4s ease-in-out 1.2s both;
	-ms-animation: fromLeftAnim3 0.4s ease-in-out 1.2s both;
	animation: fromLeftAnim3 0.4s ease-in-out 1.2s both;
}
.da-slide-fromleft .da-img{
	-webkit-animation: fromLeftAnim4 0.6s ease-in-out 0.6s both;
	-moz-animation: fromLeftAnim4 0.6s ease-in-out 0.6s both;
	-o-animation: fromLeftAnim4 0.6s ease-in-out 0.6s both;
	-ms-animation: fromLeftAnim4 0.6s ease-in-out 0.6s both;
	animation: fromLeftAnim4 0.6s ease-in-out 0.6s both;
}
@-webkit-keyframes fromLeftAnim1{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-webkit-keyframes fromLeftAnim2{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-webkit-keyframes fromLeftAnim3{
	0%{ left: -110%; opacity: 0; }
	1%{ left: 10%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-webkit-keyframes fromLeftAnim4{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 20%; opacity: 1; }
}

@-moz-keyframes fromLeftAnim1{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-moz-keyframes fromLeftAnim2{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-moz-keyframes fromLeftAnim3{
	0%{ left: -110%; opacity: 0; }
	1%{ left: 10%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-moz-keyframes fromLeftAnim4{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 20%; opacity: 1; }
}

@-o-keyframes fromLeftAnim1{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-o-keyframes fromLeftAnim2{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-o-keyframes fromLeftAnim3{
	0%{ left: -110%; opacity: 0; }
	1%{ left: 10%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-o-keyframes fromLeftAnim4{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 20%; opacity: 1; }
}

@-ms-keyframes fromLeftAnim1{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-ms-keyframes fromLeftAnim2{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-ms-keyframes fromLeftAnim3{
	0%{ left: -110%; opacity: 0; }
	1%{ left: 10%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@-ms-keyframes fromLeftAnim4{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 20%; opacity: 1; }
}

@keyframes fromLeftAnim1{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@keyframes fromLeftAnim2{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@keyframes fromLeftAnim3{
	0%{ left: -110%; opacity: 0; }
	1%{ left: 10%; opacity: 0; }
	100%{ left: 10%; opacity: 1; }
}
@keyframes fromLeftAnim4{
	0%{ left: -110%; opacity: 0; }
	100%{ left: 20%; opacity: 1; }
}
/* Slide out to the right */
.da-slide-toright h2{
	-webkit-animation: toRightAnim4 0.2s ease-in-out both;
	-moz-animation: toRightAnim4 0.2s ease-in-out both;
	-o-animation: toRightAnim4 0.2s ease-in-out both;
	-ms-animation: toRightAnim4 0.2s ease-in-out both;
	animation: toRightAnim4 0.2s ease-in-out both;
}
.da-slide-toright p{
	-webkit-animation: toRightAnim4 0.2s ease-in-out both;
	-moz-animation: toRightAnim4 0.2s ease-in-out both;
	-o-animation: toRightAnim4 0.2s ease-in-out both;
	-ms-animation: toRightAnim4 0.2s ease-in-out both;
	animation: toRightAnim4 0.2s ease-in-out both;
}
.da-slide-toright .da-link{
	-webkit-animation: toRightAnim4 0.2s ease-in-out both;
	-moz-animation: toRightAnim4 0.2s ease-in-out both;
	-o-animation: toRightAnim4 0.2s ease-in-out both;
	-ms-animation: toRightAnim4 0.2s ease-in-out both;
	animation: toRightAnim4 0.2s ease-in-out both;
}
.da-slide-toright .da-img{
	-webkit-animation: toRightAnim4 0.2s ease-in-out both;
	-moz-animation: toRightAnim4 0.2s ease-in-out both;
	-o-animation: toRightAnim4 0.2s ease-in-out both;
	-ms-animation: toRightAnim4 0.2s ease-in-out both;
	animation: toRightAnim4 0.2s ease-in-out both;
}
@-webkit-keyframes toRightAnim1{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}
@-webkit-keyframes toRightAnim2{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}
@-webkit-keyframes toRightAnim3{
	0%{ left: 10%;  opacity: 1; }
	99%{ left: 10%; opacity: 0; }
	100%{ left: 100%; opacity: 0; }
}
@-webkit-keyframes toRightAnim4{
	0%{ left: 20%;  opacity: 1; }
	30%{ left: 55%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}

@-moz-keyframes toRightAnim1{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}
@-moz-keyframes toRightAnim2{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}
@-moz-keyframes toRightAnim3{
	0%{ left: 10%;  opacity: 1; }
	99%{ left: 10%; opacity: 0; }
	100%{ left: 100%; opacity: 0; }
}
@-moz-keyframes toRightAnim4{
	0%{ left: 20%;  opacity: 1; }
	30%{ left: 55%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}

@-o-keyframes toRightAnim1{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}
@-o-keyframes toRightAnim2{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}
@-o-keyframes toRightAnim3{
	0%{ left: 10%;  opacity: 1; }
	99%{ left: 10%; opacity: 0; }
	100%{ left: 100%; opacity: 0; }
}
@-o-keyframes toRightAnim4{
	0%{ left: 20%;  opacity: 1; }
	30%{ left: 55%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}

@-ms-keyframes toRightAnim1{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}
@-ms-keyframes toRightAnim2{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}
@-ms-keyframes toRightAnim3{
	0%{ left: 10%;  opacity: 1; }
	99%{ left: 10%; opacity: 0; }
	100%{ left: 100%; opacity: 0; }
}
@-ms-keyframes toRightAnim4{
	0%{ left: 20%;  opacity: 1; }
	30%{ left: 55%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}

@keyframes toRightAnim1{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}
@keyframes toRightAnim2{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}
@keyframes toRightAnim3{
	0%{ left: 10%;  opacity: 1; }
	99%{ left: 10%; opacity: 0; }
	100%{ left: 100%; opacity: 0; }
}
@keyframes toRightAnim4{
	0%{ left: 20%;  opacity: 1; }
	30%{ left: 55%;  opacity: 1; }
	100%{ left: 100%; opacity: 0; }
}
/* Slide out to the left*/
.da-slide-toleft h2{
	-webkit-animation: toLeftAnim1 0.6s ease-in-out both;
	-moz-animation: toLeftAnim1 0.6s ease-in-out both;
	-o-animation: toLeftAnim1 0.6s ease-in-out both;
	-ms-animation: toLeftAnim1 0.6s ease-in-out both;
	animation: toLeftAnim1 0.6s ease-in-out both;
}
.da-slide-toleft p{
	-webkit-animation: toLeftAnim2 0.6s ease-in-out 0.3s both;
	-moz-animation: toLeftAnim2 0.6s ease-in-out 0.3s both;
	-o-animation: toLeftAnim2 0.6s ease-in-out 0.3s both;
	-ms-animation: toLeftAnim2 0.6s ease-in-out 0.3s both;
	animation: toLeftAnim2 0.6s ease-in-out 0.3s both;
}
.da-slide-toleft .da-link{
	-webkit-animation: toLeftAnim3 0.6s ease-in-out 0.6s both;
	-moz-animation: toLeftAnim3 0.6s ease-in-out 0.6s both;
	-o-animation: toLeftAnim3 0.6s ease-in-out 0.6s both;
	-ms-animation: toLeftAnim3 0.6s ease-in-out 0.6s both;
	animation: toLeftAnim3 0.6s ease-in-out 0.6s both;
}
.da-slide-toleft .da-img{
	-webkit-animation: toLeftAnim4 0.6s ease-in-out 0.5s both;
	-moz-animation: toLeftAnim4 0.2s ease-in-out 0.5s both;
	-o-animation: toLeftAnim4 0.2s ease-in-out 0.5s both;
	-ms-animation: toLeftAnim4 0.2s ease-in-out 0.5s both;
	animation: toLeftAnim4 0.2s ease-in-out 0.5s both;
}
@-webkit-keyframes toLeftAnim1{
	0%{ left: 10%;  opacity: 1; }
	30%{ left: 15%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@-webkit-keyframes toLeftAnim2{
	0%{ left: 10%;  opacity: 1; }
	30%{ left: 15%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@-webkit-keyframes toLeftAnim3{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@-webkit-keyframes toLeftAnim4{
	0%{ left: 20%;  opacity: 1; }
	70%{ left: 30%;  opacity: 0; }
	100%{ left: -99%; opacity: 0; }
}

@-moz-keyframes toLeftAnim1{
	0%{ left: 10%;  opacity: 1; }
	30%{ left: 15%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@-moz-keyframes toLeftAnim2{
	0%{ left: 10%;  opacity: 1; }
	30%{ left: 15%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@-moz-keyframes toLeftAnim3{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@-moz-keyframes toLeftAnim4{
	0%{ left: 20%;  opacity: 1; }
	70%{ left: 30%;  opacity: 0; }
	100%{ left: -99%; opacity: 0; }
}

@-o-keyframes toLeftAnim1{
	0%{ left: 10%;  opacity: 1; }
	30%{ left: 15%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@-o-keyframes toLeftAnim2{
	0%{ left: 10%;  opacity: 1; }
	30%{ left: 15%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@-o-keyframes toLeftAnim3{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@-o-keyframes toLeftAnim4{
	0%{ left: 20%;  opacity: 1; }
	70%{ left: 30%;  opacity: 0; }
	100%{ left: -99%; opacity: 0; }
}

@-ms-keyframes toLeftAnim1{
	0%{ left: 10%;  opacity: 1; }
	30%{ left: 15%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@-ms-keyframes toLeftAnim2{
	0%{ left: 10%;  opacity: 1; }
	30%{ left: 15%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@-ms-keyframes toLeftAnim3{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@-ms-keyframes toLeftAnim4{
	0%{ left: 20%;  opacity: 1; }
	70%{ left: 30%;  opacity: 0; }
	100%{ left: -99%; opacity: 0; }
}

@keyframes toLeftAnim1{
	0%{ left: 10%;  opacity: 1; }
	30%{ left: 15%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@keyframes toLeftAnim2{
	0%{ left: 10%;  opacity: 1; }
	30%{ left: 15%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@keyframes toLeftAnim3{
	0%{ left: 10%;  opacity: 1; }
	100%{ left: -99%; opacity: 0; }
}
@keyframes toLeftAnim4{
	0%{ left: 20%;  opacity: 1; }
	70%{ left: 30%;  opacity: 0; }
	100%{ left: -99%; opacity: 0; }
}
/* Stylesheet: e-home-css Modified On 2014-08-19 21:46:50 */
<?php }} ?>
