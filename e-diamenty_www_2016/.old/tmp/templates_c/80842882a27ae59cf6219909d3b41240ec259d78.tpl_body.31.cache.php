<?php /* Smarty version Smarty-3.1.16, created on 2015-05-15 22:39:29
         compiled from "tpl_body:31" */ ?>
<?php /*%%SmartyHeaderCode:28096907455565981c3fa88-10450986%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '80842882a27ae59cf6219909d3b41240ec259d78' => 
    array (
      0 => 'tpl_body:31',
      1 => 1408477234,
      2 => 'tpl_body',
    ),
  ),
  'nocache_hash' => '28096907455565981c3fa88-10450986',
  'function' => 
  array (
  ),
  'has_nocache_code' => true,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_55565981c57093_97945390',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55565981c57093_97945390')) {function content_55565981c57093_97945390($_smarty_tpl) {?><?php if (!is_callable('smarty_function_global_content')) include '/home/ediamentypl/public_html/plugins/function.global_content.php';
?>
<body>
<div id="top">
  <div id="top-inner"> <a href="index.php"><img src="graf/logo_e-diamenty_main_150x150.png" width="150" height="150" alt="e-diamenty" /></a>
    <ul id="top-face-twitter">
      <li class="top-face"> <a href="#"><!--aa--></a> </li>
      <li class="top-line"><a href="#"><!--aa--></a></li>
      <li class="top-twitter"><a href="#"><!--aa--></a></li>
    </ul>
  </div>
</div>
<div id="all">
  <nav id="main-menu"> <?php echo MenuManager::function_plugin(array(),$_smarty_tpl);?>
 </nav>
  <?php echo '/*%%SmartyNocache:28096907455565981c3fa88-10450986%%*/<?php CMS_Content_Block::smarty_internal_fetch_contentblock(array(),$_smarty_tpl); ?>/*/%%SmartyNocache:28096907455565981c3fa88-10450986%%*/';?>

  <div id="content">
    <div class="regulamin-news">
      <article class="news-left">
        <h2 id="regulamin-strony">Regulamin<br />
          strony</h2>
      </article>
<article class="news-right">
  <h3>Regulamin Sklepu – e-diamenty.pl</h3>
  <ul>
    <li>I. Postanowienia ogólne
      <ul class="wciecie">
        <li>1. Właścicielem oraz administratorem sklepu internetowego e-diamenty.pl znajdującego się pod adresem internetowym: www.e-diamenty.pl jest: Diamond Investment Company T. Kwiatkiewicz M. Kwiatkiewicz sp.j. Wpisaną do KRS pod numerem: 0000380507, Sąd Rejonowy Poznań – Nowe Miasto i Wilda w Poznaniu, VIII Wydział Gospodarczy Krajowego Rejestru Sądowego. Wyłączne prawo do prowadzenia wskazanego powyżej sklepu, ma : Diamond Investment Company T. Kwiatkiewicz M. Kwiatkiewicz sp.j. zwana w niniejszym regulaminie Sprzedawcą.</li>
        <li>2. Wszystkie produkty oferowane w sklepie e-diamenty.pl są fabrycznie nowe, wolne od wad prawnych fizycznych, oraz zostały legalnie wprowadzone na rynek polski.</li>
        <li>3. Przedmiotem działalności sklepu internetowego e-diamenty jest sprzedaż detaliczna diamentów.</li>
        <li>4. W sprawach nie uregulowanych niniejszym Regulaminem zastosowanie znajdą właściwe przepisy prawa obowiązujące na terytorium Rzeczypospolitej Polskiej, a w szczególności: ustawy z dnia 23 kwietnia 1964 roku - Kodeks Cywilny (Dz. U. nr 16, poz. 93 ze zm.), ustawy z dnia 27 lipca 2002 roku o szczególnych warunkach sprzedaży konsumenckiej oraz o zmianie Kodeksu Cywilnego (Dz. U. nr 141, poz. 1176 ze zm.), ustawy z dnia 2 marca 2000 roku o ochronie niektórych praw konsumentów oraz odpowiedzialności za szkodę wyrządzoną przez produkt niebezpieczny (Dz. U. nr 22, poz. 271 ze zm.) oraz ustawy z dnia 29 sierpnia 1997 roku o ochronie danych osobowych (tekst jedn.: Dz. U. z 2002 r. nr 101, poz. 926 ze zm.)</li>
        <li>5. Postanowienia niniejszego Regulaminu nie mają na celu wyłączać ani ograniczać jakichkolwiek praw Klienta będącego jednocześnie Konsumentem w rozumieniu przepisów ustawy z dnia 23 kwietnia 1964 roku - Kodeks Cywilny (Dz. U. nr 16, poz. 93 ze zm.), przysługujących mu na mocy bezwzględnie obowiązujących przepisów prawa. W przypadku niezgodności postanowień niniejszego Regulaminu z powyższymi przepisami, pierwszeństwo mają te przepisy.</li>
      </ul>
    </li>
    <li>II. Definicje
      <ul class="wciecie">
        <li>1. REGULAMIN - niniejszy regulamin</li>
        <li>2. SKLEP - sklep internetowy jest dostępny pod adresem: www.e-diamenty.pl</li>
        <li>3. SPRZEDAWCA – Diamond Investment Company T. Kwiatkiewicz M. Kwiatkiewicz sp.j. z siedzibą w Poznaniu NIP: 7792389565. REGON: 301692462, wpisaną do KRS pod numerem 0000380507.</li>
        <li>4. KLIENT – osoba fizyczna, osoba prawna oraz jednostka organizacyjna nie posiadająca osobowości prawnej, której ustawa przyznaje zdolność prawną, nabywająca produkty za pośrednictwem sklepu internetowego, dostępnego pod adresem www.e-diamenty.pl</li>
        <li>5. ZAMÓWIENIE – złożona przez Klienta za pośrednictwem sklepu internetowego e-diamenty.pl, oferta zawarcia umowy sprzedaży. </li>
        <li>6. UŻYTKOWNIK – każdy podmiot korzystający ze sklepu internetowego.</li>
        <li>7. UMOWA SPRZEDAŻY - umowa sprzedaży towaru zawarta pomiędzy Sprzedawcą a Klientem za pośrednictwem sklepu internetowego.</li>
        <li>8. TOWAR – każdy przedmiot sprzedawany za pośrednictwem sklepu internetowego.</li>
        <li>9. KONSUMENT – osoba fizyczna nabywająca produkty za pośrednictwem sklepu internetowego e-diamenty.pl w celu niezwiązanym bezpośrednio z jej działalnością gospodarczą lub zawodową.</li>
      </ul>
    </li>
    <li>III. Zamówienia
      <ul class="wciecie">
        <li>1. Klient może składać zamówienia w Sklepie 24 godziny na dobę 7 dni w tygodniu poprzez stronę internetową www.e-diamenty.pl</li>
        <li>2. Sklep prowadzi sprzedaż na terytorium Rzeczpospolitej Polskiej jak i poza granicami kraju. Informacje o produktach prezentowane na stronach internetowych Sklepu nie stanowią oferty w rozumieniu przepisów Kodeksu cywilnego; stanowią zaproszenie do zawarcia umowy sprzedaży.</li>
        <li>3. Złożenie zamówienia stanowi ofertę w rozumieniu kodeksu cywilnego, złożoną Sprzedawcy przez Klienta.</li>
        <li>4. Klient ma możliwość złożenia zamówienia za pośrednictwem sklepu internetowego bez uprzedniej rejestracji.</li>
        <li>5. Za moment zawarcia umowy uznaje się więc wysłanie do klienta informacji zwrotnej z potwierdzeniem przyjęcia zamówienia. Zamówiony Towar dostarczony zostanie na adres wskazany przez Klienta. </li>
        <li>6. Kamienie szlachetne są tworzywami naturalnymi, stąd z uwagi na ich indywidualność może okazać się obiektywnie niemożliwym uzyskanie elementu będącego dokładnym odwzorowaniem modelu, znajdującego się w ofercie sklepu. Sklep, w przypadku takiej sytuacji po uprzedniej zgodzie Klienta może zaproponować Klientowi kamień, nieco różniący się od tego, znajdującego się w ofercie. Właściwości takiego kamienia będą wówczas zachowywać taką samą lub wyższą wartość rynkową. Jeżeli Klient nie wyrazi zgody na zastąpienie kamienia zamówionego proponowanym, może bez żadnych konsekwencji odstąpić od zawartej umowy ze sklepem. Prezentowane na stronach sklepu zdjęcia i inne formy wizualizacji oraz prezentacji kamieni mogą nie oddawać ich rzeczywistej wielkości oraz jakości – mają jedynie charakter informacyjny. </li>
        <li>7. Klient może wycofać lub zmodyfikować złożone zamówienie telefonicznie pod numerem: 61 8611278 lub e-mailem, pod adresem kontakt@e-diamenty.plNie jest możliwe dokonanie anulowania zamówienia produktu, który został już wysłany.</li>
        <li>8. Realizacja zamówienia złożonego w dni powszednie po godzinie 12.00, oraz w soboty, niedziele i święta rozpoczyna się najbliższego dnia roboczego. W wyjątkowych przypadkach, zlecenia przyjmowane są telefonicznie pod numerem telefonu: 61 861 112 78(w dni robocze w godzinach 8.00 – 16.00). </li>
      </ul>
    </li>
    <li>IV. Sposób płatności
      <ul class="wciecie">
        <li>1. Wszystkie ceny w Sklepie są cenami brutto, zawierającymi podatek od Towarów i usług (VAT) w wysokości wynikającej z odrębnych przepisów. Koszty dostawy Towaru do Klienta podane będą oddzielnie, na życzenie klienta.
        <li>2. Klient ma do wyboru następujące formy płatności:
          <ul class="wciecie">
            <li> 1. przelewem/przekazem – wpłata na rachunek bankowy sklepu e-diamenty.pl pełnej wartości zamówienia wyliczonej w kalkulacji, przed dostarczeniem zamówionego towaru</li>
            <li>2. płatność gotówką – przy odbiorze (możliwe do kwoty zamówienia 15.000 EUR lub wyższej po akceptacji Sprzedawcy)</li>
          </ul>
        </li>
        <li>4. Klient dokonuje zakupu towaru oraz zamówienia usługi dostawy, (jeśli taka występuje) według cen oraz wysokości kosztów dostawy obowiązujących w chwili złożenia zamówienia.</li>
        <li>5. Sprzedawca zastrzega sobie prawo do zmiany cen oraz wysokości kosztów dostawy, w szczególności w przypadku zmiany cenników usług świadczonych przez podmioty realizujące dostawy. Postanowienie to nie dotyczy zamówień już realizowanych.</li>
        <li>6. W przypadku wyboru formy płatności opisanej w podpunkcie „1” punktu 2 brak zapłaty za zamówiony Towar w terminie 7 dni liczonych od dnia złożenia zamówienia, skutkuje anulowaniem zamówienia. Nie wyklucza to możliwości złożenia przez Klienta ponownego zamówienia tego samego Towaru.</li>
        <li>7. Klient ma możliwość dokonania wyboru sposobu zapłaty za zamówiony towar w Sklepie. Sprzedawca będąc otwartym na potrzeby Klienta, może prowadzić z nim indywidualne uzgodnienia dotyczące sposobów zapłaty.</li>
      </ul>
    </li>
    <li>V. Realizacja dostawy
      <ul class="wciecie">
        <li>1. Dostawa następuje na adres wskazany przez Klienta w zamówieniu oraz w terminie podanym przy prezentacji produktu.</li>
        <li>2. Do każdego zamówienia wystawiany jest paragon, lub na życzenie Klienta faktura VAT.</li>
        <li>3. Zamówiony Towar dostarczany jest za pomocą firmy kurierskiej GLS Poland. Termin realizacji zamówienia będzie ustalany indywidualnie. W przypadku nie zastania Klienta, zostawione zostaje awizo. W niektórych przypadkach, po nieudanej próbie doręczenia, paczki przekazywane są do najbliższego dla Klienta, punktu odbioru paczek Parcel Shop firmy GLS Poland.</li>
        <li>4. Wszystkie koszty związane z wysyłką Towarów na terenie Polski, pokrywa sprzedawca. Przesyłki zagraniczne są realizowane tylko i wyłącznie poprzez firmę kurierską,</li>
        <li>5. Koszty wysyłki za granicę (na terenie UE) pokrywa Klient. Nie wysyłamy kamieni poza UE.</li>
        <li>6. Przesyłki krajowe powinny dotrzeć do Klienta następnego dnia roboczego po dniu ich nadania. Przesyłki zagraniczne dostarczane za pomocą firmy kurierskiej GLS Poland, powinny dotrzeć w ciągu 5 dni od daty jej nadania.</li>
        <li>7. Jeżeli Sprzedawca nie może spełnić świadczenia z tego powodu, że Towar nie jest dostępny, niezwłocznie, najpóźniej jednak w terminie dziesięciu dni od zawarcia Umowy, zawiadomi o tym Klienta, który podejmie decyzję o dalszych losach złożonego przez niego zamówienia.</li>
        <li>8. Jeżeli Sprzedawca nie może wykonać świadczenia o właściwościach indywidualnie zamówionych przez Klienta z powodu przejściowej niemożności jego spełnienia, Sprzedawca może za zgodą Klienta spełnić świadczenie zastępcze, odpowiadające tej samej jakości i przeznaczeniu oraz za tę samą cenę lub w inny ustalony przez strony sposób.</li>
        <li>9. Sprzedawca mając na uwadze komfort Klienta, jak również bezpieczeństwo przy realizacji zamówienia, zapewnia, że wszelkie niedogodności na jakimkolwiek etapie zamówienia - gdyby się takowe pojawiły - będą konsultowane z Klientem i realizowane za porozumieniem stron. Sprzedawca dokłada wszelkich starań, aby Klient był usatysfakcjonowany.</li>
        <li>10. Termin realizacji zamówienia jest wskazany przy danym Towarze i dotyczy wysyłek realizowanych na terenie Polski. Termin wskazany przy Towarze nie dotyczy: </li>
        <li>11. Zaleca się, aby Klient, będący jednocześnie konsumentem w rozumieniu art. 221 Kodeksu Cywilnego, w miarę możliwości dokonał sprawdzenia stanu Towaru po dostarczeniu przesyłki i w obecności przedstawiciela podmiotu realizującego dostawę (kurier, operator pocztowy, etc.) spisał odpowiedni protokół. Sprawdzenie przesyłki ułatwi i przyspieszy dochodzenie ewentualnych roszczeń od podmiotu odpowiedzialnego w przypadku mechanicznego uszkodzenia przesyłki powstałego w trakcie transportu. W takich sytuacjach zaleca się, aby Klient skontaktował się w miarę możliwości w jak najszybszym czasie ze Sprzedawcą.</li>
        <li>12. Klient, który nie jest konsumentem w rozumieniu art.221 Kodeksu Cywilnego, jest zobowiązany do sprawdzenia stanu Towaru po dostarczeniu przesyłki i w obecności przedstawiciela podmiotu realizującego dostawę (kurier, operator pocztowy etc.). W przypadku stwierdzenia uszkodzeń mechanicznych przesyłki powstałych w trakcie transportu Klient powinien spisać protokół szkody i niezwłocznie skontaktować się ze Sprzedawcą.</li>
      </ul>
    </li>
    <li>VI. Reklamacje
      <ul class="wciecie">
        <li>1. W przypadku umów zawieranych z Klientami będącymi jednocześnie konsumentami w rozumieniu art. 221 Kodeksu cywilnego, Sprzedawca odpowiada za niezgodność Towaru z umową w przypadku jej stwierdzenia przez Klienta przed upływem 2 lat od dnia Dostawy. Klient zobowiązany jest do zawiadomienia Sprzedawcy o stwierdzonej niezgodności Towaru z umową w terminie 2 miesięcy od dnia stwierdzenia takiej niezgodności.</li>
        <li>2. Sprzedawca w ciągu 14 (czternastu) dni ustosunkuje się do reklamacji Towaru zgłoszonej przez Klienta i powiadomi go o sposobie dalszego postępowania.</li>
        <li>3. W celu ułatwienia procesu reklamacyjnego, Klient proszony jest o przesłanie Towaru oraz w miarę możliwości opisu niezgodności towaru z umową.</li>
        <li>4. W przypadku rozpatrzenia reklamacji na korzyść Klienta, Sprzedawca doprowadzi Towar do stanu zgodnego z umową, zgodnie z żądaniem Klienta, lub wymieni na nowy. Jeśli wymiananarażałaby Klienta na znaczne niedogodności Sprzedawca, zgodnie z żądaniem Klienta, obniży cenę lub zwróci – na skutek odstąpienia przez Klienta od umowy – pełną należność za reklamowany produkt w ciągu 7 (siedmiu) dni.</li>
        <li>5. W przypadku umów zawieranych z Klientami niebędącymi jednocześnie konsumentami w rozumieniu art. 221 Kodeksu cywilnego, na podstawie art. 558 § 1 Kodeksu cywilnego, odpowiedzialność Sprzedawcy z tytułu rękojmi jest wyłączona.</li>
        <li>6. Klient może reklamować Towar posiadający wady:
          Korzystając z uprawnień wynikających z udzielonej gwarancji – w takich okolicznościach Klient może dokonać reklamacji u Sprzedawcy, który jest tylko pośrednikiem przekazującym złożoną reklamację producentowi
          Korzystając z uprawnień przysługujących Klientowi będącemu jednocześnie konsumentem w rozumieniu art. 221 Kodeksu cywilnego wobec Sprzedawcy w związku z niezgodnością Towaru z umową.</li>
      </ul>
    </li>
    <li>VII. Zwroty
      <ul class="wciecie">
        <li>1. Oświadczenie można wysłać drogą elektroniczną na adres: kontakt@e-diamenty.pl bądź też listownie na adres:
          E-DIAMENTY.PL
          DIAMOND INVESTMENT COMPANY T.KWIATKIEWICZ M.KWIATKIEWICZ SP.J..
          UL.ŚWIERZAWSKA 1
          60-321, POZNAŃ </li>
        <li>2. Prawo odstąpienia od umowy zawartej na odległość nie przysługuje konsumentowi w wypadkach:<br />
          • dotyczących nagrań audialnych i wizualnych oraz zapisanych na informatycznych nośnikach danych po usunięciu przez konsumenta ich oryginalnego opakowania;<br />
          • umów dotyczących świadczeń, za które cena lub wynagrodzenie zależy wyłącznie od ruchu cen na rynku finansowym;<br />
          • świadczeń o właściwościach określonych przez konsumenta w złożonym przez niego zamówieniu lub ściśle związanych z jego osobą;<br />
          • świadczeń, które z uwagi na ich charakter nie mogą zostać zwrócone lub których przedmiot ulega szybkiemu zepsuciu;<br />
          • dostarczania prasy;<br />
          • usług w zakresie gier hazardowych. </li>
        <li>3. Zgodnie z ustawą o ochronie niektórych praw konsumentów oraz odpowiedzialności za szkodę wyrządzoną przez produkt niebezpieczny pod pojęciem świadczeń o właściwościach określonych przez konsumenta w złożonym przez niego zamówieniu lub ściśle związanych z jego osobą rozumie się towary wykonane na indywidualne zamówienie, a więc zawierające w szczególności: towary niewystępujące w standardowej ofercie e-diamenty.pl sprowadzane na specjalne zamówienie Klienta. Towary o szczególnych właściwościach, tj. właściwościach określonych przez konsumenta w złożonym przez niego zamówieniu lub sprowadzone na indywidualne zamówienie Klienta tj. ściśle związanych z jego osobą nie podlegają zwrotowi. </li>
        <li>4. Powyższe wskazanie nie wyłącza odpowiedzialności Sprzedawcy określonych w dziale VII Reklamacje, w związku z niezgodnością towaru z umową.
        <li>5. W przypadku chęci dokonania zwrotu biżuterii z kamieniami szlachetnymi prosimy o kontakt ze Sprzedawcą, bowiem zgodnie z niniejszym Regulaminem, ewentualne zwroty kamieni szlachetnychze względu na ich indywidualny charakter lub właściwości, nie podlegają zwrotowi. Sprzedawca umożliwia jednak taką możliwość na warunkach indywidualnie ustalonych z Klientem. </li>
      </ul>
    </li>
    <li>VIII. Postanowienia końcowe
      <ul class="wciecie">
        <li>1. Zmiana treści niniejszego regulaminu może nastąpić po uprzednim poinformowaniu Użytkowników o zakresie przewidywanych zmian nie później niż w terminie 14 dni przed dniem ich wejścia w życie.</li>
        <li>2. Zamówienia złożone w trakcie obowiązywania poprzedniej wersji regulaminu będą realizowane zgodnie z jego postanowieniami. Ewentualne spory powstałe pomiędzy Sprzedawcą a Klientem, który jest konsumentem w rozumieniu art.221 Kodeksu Cywilnego, rozstrzygane będą przez sąd powszechny właściwy zgodnie z przepisami Kodeksu postępowania cywilnego.</li>
        <li>3. Ewentualne spory powstałe pomiędzy Sprzedawcą a Klientem, który nie jest konsumentem w rozumieniu art.221 Kodeksu Cywilnego, rozstrzygane będą przez sąd powszechny właściwy ze względu na siedzibę Sklepu.</li>
        <li>4. Użytkownicy mogą kontaktować się ze Sprzedawcą w następujący sposób: <br />
          • telefon: 618611278<br />
          • poczta elektroniczna na adres: kontakt@e-diamenty.pl<br />
          • pisemnie na adres:<br />
          E-DIAMENTY.PL
          DIAMOND INVESTMENT COMPANY T.KWIATKIEWICZ M.KWIATKIEWICZ SP.J..
          UL.ŚWIERZAWSKA 1
          60-321, POZNAŃ </li>
        <li>5. Klienci mogą uzyskać dostęp do niniejszego Regulaminu w każdym czasie za pośrednictwem linku zamieszczonego na stronie głównej sklepu internetowego e-diamenty.pl. Regulamin może zostać utrwalony, pozyskany i odtworzony poprzez jego wydrukowanie lub zapisanie go na odpowiednim nośniku danych.</li>
        <li>6. Nazwa sklepu internetowego e-diamenty.pl, adres strony, pod którym jest dostępny: e-diamenty.plraz wszelkie materiały w nim się znajdujące stanowią przedmiot prawa autorskiego i podlegają ochronie prawnej. Wykorzystywanie i rozpowszechnianie ich bez zgody właściciela sklepu jest zabronione.</li>
        <li>7. Niniejszy regulamin obowiązuje od dnia 15.03.2011.</li>
        <li>Wszelkie prawa zastrzeżone. Kopiowanie zdjęć i innych materiałów graficznych, przedruk tekstów zamieszczonych na stronie oraz ich udostępnianie w Internecie lub innej formie jest możliwe wyłącznie za pisemną zgodą właściciela serwisu.</li>
      </ul>
    </li>
  </ul>
</article>





      <div class="clear"></div>
    </div>
    <div class="regulamin-news">
      <article class="news-left">
        <h2 id="regulamin-cookies">Regulamin<br />
          cookies</h2>
      </article>
      <article class="news-right">
        <h3>Informacje ogólne</h3>
        <ul>
          <li>Serwis nie zbiera w sposób automatyczny żadnych informacji, z wyjątkiem informacji zawartych <br />
            w plikach cookies.</li>
          <li>Pliki cookies (tzw. „ciasteczka”) stanowią dane informatyczne, w szczególności pliki tekstowe, które przechowywane są w urządzeniu końcowym Użytkownika Serwisu i przeznaczone są do korzystania ze stron internetowych Serwisu. Cookies zazwyczaj zawierają nazwę strony internetowej, z której pochodzą, czas przechowywania ich na urządzeniu końcowym oraz unikalny numer.</li>
          <li>Podmiotem zamieszczającym na urządzeniu końcowym Użytkownika Serwisu pliki cookies oraz uzyskującym do nich dostęp jest administrator serwisu e-diamenty.pl</li>
          <li>Pliki cookies wykorzystywane są w celu:
            <ul class="wciecie">
              <li>dostosowania zawartości stron internetowych Serwisu do preferencji Użytkownika oraz optymalizacji korzystania ze stron internetowych; w szczególności pliki te pozwalają rozpoznać urządzenie Użytkownika Serwisu i odpowiednio wyświetlić stronę internetową, dostosowaną do jego indywidualnych potrzeb;</li>
              <li>tworzenia statystyk, które pomagają zrozumieć, w jaki sposób Użytkownicy Serwisu korzystają ze stron internetowych, co umożliwia ulepszanie ich struktury i zawartości;</li>
              <li>utrzymanie sesji Użytkownika Serwisu (po zalogowaniu), dzięki której Użytkownik nie musi na każdej podstronie Serwisu ponownie wpisywać loginu i hasła;</li>
            </ul>
          </li>
          <li> W ramach Serwisu stosowane są dwa zasadnicze rodzaje plików cookies: „sesyjne”  (session cookies) oraz „stałe” (persistent cookies). Cookies „sesyjne” są plikami tymczasowymi, które przechowywane są w urządzeniu końcowym Użytkownika do czasu wylogowania, opuszczenia strony internetowej lub wyłączenia oprogramowania (przeglądarki internetowej). „Stałe” pliki cookies przechowywane są w urządzeniu końcowym Użytkownika przez czas określony w parametrach plików cookies lub do czasu ich usunięcia przez Użytkownika. </li>
          <li> W ramach Serwisu stosowane są następujące rodzaje plików cookies:
            <ul class="wciecie">
              <li>„niezbędne” pliki cookies, umożliwiające korzystanie z usług dostępnych w ramach Serwisu, np. uwierzytelniające pliki cookies wykorzystywane do usług wymagających uwierzytelniania w ramach Serwisu;</li>
              <li>pliki cookies służące do zapewnienia bezpieczeństwa, np. wykorzystywane do wykrywania nadużyć w zakresie uwierzytelniania w ramach Serwisu;</li>
              <li>„wydajnościowe” pliki cookies, umożliwiające zbieranie informacji o sposobie korzystania ze stron internetowych Serwisu;</li>
              <li>„funkcjonalne” pliki cookies, umożliwiające „zapamiętanie” wybranych przez Użytkownika ustawień i personalizację interfejsu Użytkownika, np. w zakresie wybranego języka lub regionu, z którego pochodzi Użytkownik, rozmiaru czcionki, wyglądu strony internetowej itp.;</li>
            </ul>
          </li>
          <li>W wielu przypadkach oprogramowanie służące do przeglądania stron internetowych (przeglądarka internetowa) domyślnie dopuszcza przechowywanie plików cookies w urządzeniu końcowym Użytkownika. Użytkownicy Serwisu mogą dokonać w każdym czasie zmiany ustawień dotyczących plików cookies. Ustawienia te mogą zostać zmienione w szczególności w taki sposób, aby blokować automatyczną obsługę plików cookies w ustawieniach przeglądarki internetowej bądź informować o ich każdorazowym zamieszczeniu w urządzeniu Użytkownika Serwisu. Szczegółowe informacje o możliwości i sposobach obsługi plików cookies dostępne są w ustawieniach oprogramowania (przeglądarki internetowej).</li>
          <li>Operator Serwisu informuje, że ograniczenia stosowania plików cookies mogą wpłynąć na niektóre funkcjonalności dostępne na stronach internetowych Serwisu.</li>
          <li>Pliki cookies zamieszczane w urządzeniu końcowym Użytkownika Serwisu i wykorzystywane mogą być również przez współpracujących z operatorem Serwisu reklamodawców oraz partnerów.</li>
          <li>Więcej informacji na temat plików cookies dostępnych jest w ustawieniach przeglądarki internetowej.</li>
        </ul>
        <p class="font-small">Wzór polityki cookies dzięki IAB Polska, http://wszystkoociasteczkach.pl</p>
      </article>
      <div class="clear"></div>
    </div>
 
    <div class="clear"></div>
  </div>
 <div class="clear"></div> <footer id="stopka"> <?php echo smarty_function_global_content(array('name'=>'footer'),$_smarty_tpl);?>
 </footer>
</div>
</body>
</html><?php }} ?>
