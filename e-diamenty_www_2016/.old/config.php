<?php
# CMS Made Simple Configuration File
# Documentation: /doc/CMSMS_config_reference.pdf
#
$config['dbms'] = 'mysqli';
$config['db_hostname'] = 'localhost';
$config['db_username'] = 'ediament_user';
$config['db_password'] = '[!-t_uLN8dt@';
$config['db_name'] = 'ediament_database';
$config['db_prefix'] = 'cms_';
$config['timezone'] = 'Europe/Warsaw';
$config['url_rewriting'] = 'mod_rewrite';
$config['page_extension'] = '/';
$config['locale'] = 'pl_PL.UTF8';
?>
