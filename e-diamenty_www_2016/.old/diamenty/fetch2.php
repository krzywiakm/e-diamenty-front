<?php
define("QUOTES_GPC", (ini_get('magic_quotes_gpc') ? TRUE : FALSE));
require_once "vendor/xml2array.php";

if ($waluty = @file_get_contents('http://www.nbp.pl/kursy/xml/a127z130703.xml'))
{
	$waluty = xml2array($waluty);
	$usd = $waluty['tabela_kursow']['pozycja'][1]['kurs_sredni'];
	unset($waluty);
	$usd = (double)str_replace(",", ".", $usd);
	if (!$usd)
	{
		// domyslny przelicznik, jeden dolec=1zl, gdy brak danych na serwerze
		$usd = 1;
	}
}
else
{
	// domyslny przelicznik, jeden dolec=1zl, gdy nie pobierze z serwera
	$usd = 1;
}

// USER CONFIG
$username = '61204';
$password = 'Crave+Stone';

if (!isset($_GET['diamond_id']) || !isnum($_GET['diamond_id']))
	die('bad parameter "diamond_id"');

// logowanie
$client = new SoapClient("https://technet.rapaport.com/WebServices/RetailFeed/Feed.asmx?WSDL",
      array( "trace" => 1, "exceptions" => 0, "cache_wsdl" => 0) );
$params = array('Username'=>$username, 'Password'=>$password);
$client->__soapCall("Login", array($params), NULL, NULL, $output_headers);
$ticket = $output_headers["AuthenticationTicketHeader"]->Ticket;

// uruchomienie klienta
$client1 = new SoapClient("https://technet.rapaport.com/WebServices/RetailFeed/Feed.asmx?WSDL", array( "trace" => 1, "exceptions" => 0, "cache_wsdl" => 0) );
$ns = "http://technet.rapaport.com/";
$headerBody = array("Ticket" => $ticket);
$header = new SoapHeader($ns, 'AuthenticationTicketHeader', $headerBody);
$client1->__setSoapHeaders($header);

$params1 = array("DiamondID" => $_GET['diamond_id']);

$client1->__soapCall("GetSingleDiamond", array($params1), NULL, NULL, $output_headers);

// odpowiedz serwera
$response = $client1->__getLastResponse();
// konwersja xml na tablice
$xml = xml2array($response);
$diamond = @$xml['soap:Envelope']['soap:Body']['GetSingleDiamondResponse']['GetSingleDiamondResult']['diffgr:diffgram']['NewDataSet']['Table'];

if (empty($diamond))
{
	die('not found');
}
else
{
	//print_r($diamond);
	echo "<meta charset='utf-8' />";

echo "<style>
.ds-popup {
	width: 506px;

	background: white;
	border: 2px solid #C0C3D1;
	overflow: hidden;
	font-family: Arial;
	padding-bottom: 2px;
}

.ds-popup-header {
	height: 40px;
	background: #C0C3D1;
}

.ds-popup-header h2 {
	float: left;
	margin: 0;
	padding: 0;
	font-size: 14px;
	line-height: 40px;
	margin-left: 3px;
}

.ds-popup-header a {
	background: url(img/popup-close.png) no-repeat;
	display: block;
	float: right;
	text-indent: -9999px;
	width: 80px;
	height: 30px;
	margin-top: 4px;
	margin-right: 3px;
}

.ds-popup-info {
	margin: 0;
	padding: 0;
	list-style: none;
	font-size: 13px;
	float: left;
	margin-left: 5px;
	width: 230px;
}

.ds-popup-info li {
	border-top: 1px solid #e3e3e4;
	position: relative;
	overflow: hidden;
	padding: 3px 0;
}

.ds-popup-info li:first-child {
	border-top: none;
}

.ds-popup-info li span {
	display: block;
	position: absolute;
	left: 5px;
}

.ds-popup-info li p {
	float: left;
	margin: 0;
	margin-left: 100px;
}

.ds-popup-gallery {
	float: right;
	border: 1px solid #b5bfc4;
	padding: 1px;
	margin-right: 5px;
	margin-top: 5px;
}

.ds-popup-image {
	width: 245px;
	height: 250px;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
}

.ds-popup-images {
	list-style: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	margin-left: 60px;
	margin-top: 15px;
	margin-bottom: 15px;
}

.ds-popup-images li {
	float: left;
	margin-right: 5px;
	position: relative;
	width: 40px;
	height: 40px;
	min-height: 15px;
}

.ds-popup-images .selected {
	position: absolute;
	left: 0;
	top: 0;
	opacity: 0;
	z-index: 2;
}

.ds-popup-images a {
	border: 0;
	text-decoration: none;
}

.ds-popup-images a.select .selected {
	opacity: 1;
}

.ds-popup-interere {
	clear: both;
	display: block;
	margin-top: 5px;
}

.ds-popup-form {
	margin-top: 5px;
	margin-left: 5px;
}

.ds-popup-form label {
	font-size: 13px;
	width: 50%;
	float: left;
	overflow: hidden;
	margin-bottom: 5px;
	line-height: 20px;
}

.ds-popup-form label input {
	display: block;
	float: right;
	width: 170px;
	margin-right: 5px;
	border: 1px solid #8f8f8f;
	background: white;
	height: 20px;
}

.ds-popup-form label.ds-popup-textarea {
	clear: both;
	width: 100%;
}

.ds-popup-form label.ds-popup-textarea textarea {
	border: 1px solid #8f8f8f;
	background: white;
	float: right;
	display: block;
	width: 420px;
	margin-right: 5px;
	resize: none;
	height: 40px;
}

.ds-popup-form button {
	float: right;
	text-indent: -9999px;
	color: transparent;
	font-size: 1px;
	background: url(img/popup-send.png) no-repeat;
	width: 80px;
	height: 30px;
	border: 0;
	margin-right: 5px;
	margin-bottom: 5px;
}

.ds-popup-form p {
	float: left;
	margin: 0;
	padding: 0;
	color: gray;
	font-size: 13px;
	margin-top: 5px;
	margin-left: 75px;
}
</style>\n";


	echo "<div class='ds-popup'>
<div class='ds-popup-header'>
	<h2>".$diamond['ShapeTitle'].", ".$diamond['Weight']."ct, ".$diamond['ColorTitle'].", ".$diamond['ClarityTitle']."</h2>
	<a href='#' class='ds-popup-close'>Zamknij</a>
</div>
<ul class='ds-popup-info'>
	".(!empty($diamond['ShapeTitle']) ? "<li><span>Kształt</span><p> ".$diamond['ShapeTitle']."</p></li>" : "")."
	".(!empty($diamond['Weight']) ? "<li><span>Masa</span><p> ".$diamond['Weight']."</p></li>" : "")."
	".(!empty($diamond['FinalPrice']) ? "<li><span>Cena</span><p> ".number_format($diamond['FinalPrice']*$usd, 2, ".", "")." zł</p></li>" : "")."
	".(!empty($diamond['labTitle']) ? "<li><span>Certyfikat</span><p> ".$diamond['labTitle']."</p></li>" : "")."
	".(!empty($diamond['ColorTitle']) ? "<li><span>Barwa</span><p> ".$diamond['ColorTitle']."</p></li>" : "")."
	".(!empty($diamond['ClarityTitle']) ? "<li><span>Czystość</span><p> ".$diamond['ClarityTitle']."</p></li>" : "")."
	".(!empty($diamond['CutLongTitle']) ? "<li><span>Szlif</span><p> ".$diamond['CutLongTitle']."</p></li>" : "")."
	".(!empty($diamond['DepthPercent']) ? "<li><span>Głębokość %</span><p> ".$diamond['DepthPercent']."</p></li>" : "")."
	".(!empty($diamond['TablePercent']) ? "<li><span>Tafla %</span><p> ".$diamond['TablePercent']."</p></li>" : "")."
	".(!empty($diamond['PolishTitle']) ? "<li><span>Poler</span><p> ".$diamond['PolishTitle']."</p></li>" : "")."
	".(!empty($diamond['SymmetryTitle']) ? "<li><span>Symetria</span><p> ".$diamond['SymmetryTitle']."</p></li>" : "")."
	".(!empty($diamond['GirdleSizeMin']) ? "<li><span>Rondysta</span><p> ".$diamond['GirdleSizeMin']."<br />".$diamond['GirdleSizeMax']."</p></li>" : "")."
	".(!empty($diamond['CuletSizeTitle']) ? "<li><span>Kolet</span><p> ".$diamond['CuletSizeTitle']."</p></li>" : "")."
	".(!empty($diamond['FluorescenceIntensityTitle']) ? "<li><span>Fluorescencja</span><p> ".$diamond['FluorescenceIntensityTitle']."</p></li>" : "")."
	".(!empty($diamond['MeasLength']) ? "<li><span>Wymiary</span><p> ".$diamond['MeasLength']."*".$diamond['MeasWidth']."*".$diamond['MeasDepth']."</p></li>" : "")."
</ul>
<div class='ds-popup-gallery'>
	<div class='ds-popup-image'>
		<img src='img/diamonds/".$diamond['ShapeTitle']."TopView_245x250px.jpg' />
	</div>
	<ul class='ds-popup-images'>
		<li><a href='img/diamonds/".$diamond['ShapeTitle']."TopView_245x250px.jpg'>
			<img src='http://www.diamondselections.com/Images/AdvancedDesign/DiamondDetails/".$diamond['ShapeTitle']."TopView_40x40px.jpg' />
			<img src='http://www.diamondselections.com/Images/AdvancedDesign/DiamondDetails/".$diamond['ShapeTitle']."TopView_40x40px_Selected.jpg' class='selected' />
		</a></li>
		<li><a href='img/diamonds/".$diamond['ShapeTitle']."Profile_245x250px.jpg'>
			<img src='http://www.diamondselections.com/Images/AdvancedDesign/DiamondDetails/".$diamond['ShapeTitle']."Profile_40x40px.jpg' />
			<img src='http://www.diamondselections.com/Images/AdvancedDesign/DiamondDetails/".$diamond['ShapeTitle']."Profile_40x40px_Selected.jpg' class='selected' />
		</a></li>
	</ul>
</div>
<img src='img/popup-interere.png' class='ds-popup-interere' />
<form class='ds-popup-form'>
<input type='hidden' name='diamond_id' value='".$_GET['diamond_id']."' />
<label>Imię* <input type='text' name='name' required /></label>
<label>Nazwisko* <input type='text' name='surname' required /></label>
<label>E-mail* <input type='email' name='email' required /></label>
<label>Telefon <input type='text' name='tel' /></label>
<label class='ds-popup-textarea'>Komentarz <textarea name='comment'></textarea></label>
<p>* Pola wymagane</p>
<button type='submit'>Wyślij</button>
</form>
</div>\n";
exit;
	//die(json_encode(array('diamond' => $diamond), JSON_FORCE_OBJECT));
}

// funkcje
function stripinput($text) {
	if (!is_array($text)) {
		$text = stripslash(trim($text));
		$text = preg_replace("/(&amp;)+(?=\#([0-9]{2,3});)/i", "&", $text);
		$search = array("&", "\"", "'", "\\", '\"', "\'", "<", ">", "&nbsp;");
		$replace = array("&amp;", "&quot;", "&#39;", "&#92;", "&quot;", "&#39;", "&lt;", "&gt;", " ");
		$text = str_replace($search, $replace, $text);
	} else {
		foreach ($text as $key => $value) {
			$text[$key] = stripinput($value);
		}
	}
	return $text;
}

function stripslash($text) {
	if (QUOTES_GPC) { $text = stripslashes($text); }
	return $text;
}

function isnum($value) {
	if (!is_array($value)) {
		return (preg_match("/^[0-9]+$/", $value));
	} else {
		return false;
	}
}