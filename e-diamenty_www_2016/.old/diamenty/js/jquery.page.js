$(function()
{
	$('#ds-shapes a').click(function()
	{
		if ($(this).parent().hasClass('active'))
		{
			if ($(this).parent().parent().find('li.active').length <= 1)
			{
				return false;
			}

			$(this).parent().removeClass('active');
			$(this).parent().find('input').attr('name', 'tmp');
		}
		else
		{

			$(this).parent().addClass('active');
			$(this).parent().find('input').attr('name', 'ds_shapes[]');
		}

		ds_load_data();

		return false;
	});

	$('.ds-size-slider').noUiSlider({
		range: [0, 100],
		start: [0, 100],
		step: 0.01,
		slide: function(){
			var values = $(this).val();
			$(".ds-size .ds-slide-from").val(values[0]);
			$(".ds-size .ds-slide-to").val(values[1]);

			ds_load_data();
		}
	});

	$('.ds-price-slider').noUiSlider({
		range: [10, $('.ds-price .ds-slide-from').attr('max')],
		start: [10, $('.ds-price .ds-slide-from').attr('max')],
		step: 0.01,
		slide: function(){
			var values = $(this).val();
			$(".ds-price .ds-slide-from").val(values[0]);
			$(".ds-price .ds-slide-to").val(values[1]);

			ds_load_data();
		}
	});

	$('.ds-color-slider').noUiSlider({
		range: [1, 10],
		start: [1, 10],
		step: 1,
		slide: function(){
			var values = $(this).val();
			$(".ds-color .ds-slide-from").val(values[1]);
			$(".ds-color .ds-slide-to").val(values[0]);

			ds_load_data();
		}
	});

	$('.ds-clean-slider').noUiSlider({
		range: [2, 10],
		start: [2, 10],
		step: 1,
		slide: function(){
			var values = $(this).val();
			$(".ds-clean .ds-slide-from").val(values[0]);
			$(".ds-clean .ds-slide-to").val(values[1]);

			ds_load_data();
		}
	});

	$('.ds-cut-slider').noUiSlider({
		range: [2, 5],
		start: [2, 5],
		step: 1,
		slide: function(){
			var values = $(this).val();
			$(".ds-cut .ds-slide-from").val(values[1]);
			$(".ds-cut .ds-slide-to").val(values[0]);

			ds_load_data();
		}
	});

	$('.ds-poler-slider').noUiSlider({
		range: [2, 5],
		start: [2, 5],
		step: 1,
		slide: function(){
			var values = $(this).val();
			$(".ds-poler .ds-slide-from").val(values[1]);
			$(".ds-poler .ds-slide-to").val(values[0]);

			ds_load_data();
		}
	});

	$('.ds-symetry-slider').noUiSlider({
		range: [1, 4],
		start: [1, 4],
		step: 1,
		slide: function(){
			var values = $(this).val();
			$(".ds-symetry .ds-slide-from").val(values[1]);
			$(".ds-symetry .ds-slide-to").val(values[0]);

			ds_load_data();
		}
	});

	$('.ds-slide input').keyup(function()
	{
		val_left = $(this).parent().parent().find('.ds-slide-from').val();
		val_right = $(this).parent().parent().find('.ds-slide-to').val();

		if (val_right != '' && val_left != '')
		{
			$(this).parent().parent().find('.noUiSlider').val([val_left, val_right]);

			ds_load_data();
		}
	});

	$('.ds-price .ds-slide input').unbind('keyup').keyup(function()
	{
		val_left = $(this).parent().parent().find('.ds-slide-from').val();
		val_right = $(this).parent().parent().find('.ds-slide-to').val();

		if (val_right != '' && val_left != '')
		{
			$(this).parent().parent().parent().find('.noUiSlider').val([val_left, val_right]);

			ds_load_data();
		}
	});


	$('.ds-checkbox').click(function()
	{
		if ($(this).hasClass('checked'))
		{
			$(this).parent().find('input[name="'+$(this).attr('data-input-name')+'"]').val('0');
			$(this).removeClass('checked');
		}
		else
		{
			$(this).parent().find('input[name="'+$(this).attr('data-input-name')+'"]').val('1');
			$(this).addClass('checked');
		}

		ds_load_data();
	});

	$('.lol').click(function()
	{
		data = $('#ds-form').serialize();
		$('.test').text('');
		$.post('fetch.php', data, function(ret)
		{

			$('.test').text(ret);
		});
		//ds_load_data();

		return false;
	});

	$('.ds-table-reset').click(function()
	{
		 location.reload();

		 return false;
	});

	pagin_count = 0;
	pagin_pages = 0;
	pagin_steps = 0;
	pagin_current_step = 0;

	function ds_load_data(reload)
	{
		reload = (reload == undefined ? true : reload);

		data = $('#ds-form').serialize();

		$('.ds-loader img').fadeTo(200, 1)
		$('.ds-table #ds-results').fadeTo(200, 0.4);
		$.post('fetch.php', data, function(ret)
		{

			$('.ds-count').text(ret.diamonds_count);
			if (ret.message)
			{
				$('.ds-table-count').fadeTo(200, 0);
				$('.ds-table-footer').css('visibility', 'hidden');
				$('#ds-results').html('<tr><td colspan="10">Nie znaleziono żadnego diamentu.</td></tr>');
			}
			else if (ret.error)
			{
				$('#ds-results').html('<tr><td colspan="10">Wystąpił błąd. Kod błędu: <strong>'+ret.error+'</strong>. Skontaktuj się z administracją.</td></tr>');
			}
			else
			{
				$('.ds-table-count').fadeTo(200, 1);
				ds_apply_data(ret.diamonds);

				$(".ds-table tr td:not(.ds-link)").tooltip({
					left: -200,
					blocked: true,
					bodyHandler: function() {
						return $(this).parent().attr("data-tip");
					},
					showURL: false
				});

				ds_apply_pagin(ret.diamonds_count, 10, reload);
			}




			$('.ds-loader img').fadeTo(200, 0);
			$('.ds-table #ds-results').fadeTo(200, 1.0);
		}, "json");
	}

	function ds_apply_pagin(count, per_page, reload)
	{
		if (reload == true)
		{
			$('.ds-table-footer').css('visibility', 'visible');
			$('.ds-pagin').html('');
			$('.ds-pagin').css('left', '0px');

			count = parseInt(count);
			pages = Math.ceil(count/per_page);
			pagin_count = count;
			pagin_pages = pages;
			pagin_steps = Math.ceil(pagin_pages/10);

			if (pagin_steps == 1)
			{
				$('.ds-pagin-prev').css('visibility', 'hidden');
				$('.ds-pagin-next').css('visibility', 'hidden');
			}
			else
			{
				$('.ds-pagin-prev').css('visibility', 'hidden');
				$('.ds-pagin-next').css('visibility', 'visible');
			}

			pagin_current_step = 0;

			for(var i=1;i<=pages;i++)
			{
				if (i == 1)
				{
					$('.ds-pagin').append('<li><a href="#" class="current">'+i+'</a></li>');
				}
				else
				{
					$('.ds-pagin').append('<li><a href="#">'+i+'</a></li>');
				}
			}
		}
	}


	$('.ds-pagin').on('click', 'a', function(e)
	{
		$('.ds-pagin li a').removeClass('current');
		$(this).addClass('current');

		//alert($(this).text());

		$('input[name="ds_page"]').val($(this).text());

		ds_load_data(false);

		return false;
	});

	$('.ds-pagin-next').click(function()
	{
		pagin_current_step = pagin_current_step+1;

		$('.ds-pagin').css('left', '-'+(pagin_current_step*350)+'px');

		if (pagin_current_step == pagin_steps-1)
		{
			$(this).css('visibility', 'hidden');
		}

		$('.ds-pagin-prev').css('visibility', 'visible');

		return false;
	});

	$('.ds-pagin-prev').click(function()
	{
		pagin_current_step = pagin_current_step-1;

		$('.ds-pagin').css('left', '-'+(pagin_current_step*350)+'px');

		if (pagin_current_step == 0)
		{
			$(this).css('visibility', 'hidden');
		}

		$('.ds-pagin-next').css('visibility', 'visible');

		return false;
	});

	$('.ds-table th').click(function()
	{
		if ($(this).attr('data-sort') != undefined)
		{
			$('.ds-table th').removeClass('sorted');
			$(this).addClass('sorted');
			$('input[name="SortBy"]').val($(this).attr('data-sort'));
			ds_load_data(false);
		}
	});

	function ds_apply_data(data)
	{
		$('#ds-results').html('');
		$.each(data, function(index, v)
		{
			if (v.ShapeTitle)
			{
				price = v.FinalPrice*USD;
				price = price.toFixed(2);
				$('#ds-results').append('<tr data-tip="<div class=\'table-tip\'><h4>Szczegóły</h4><ul><li><span class=\'l\'>Głębokość %</span> <span class=\'r\'>'+(v.DepthPercent != undefined ? v.DepthPercent : '-')+'</span></li><li><span class=\'l\'>Tafla %</span> <span class=\'r\'>'+(v.TablePercent != undefined ? v.TablePercent : '-')+'</span></li><li><span class=\'l\'>Poler</span> <span class=\'r\'>'+(v.PolishTitle != undefined ? v.PolishTitle : '-')+'</span></li><li><span class=\'l\'>Symetria</span> <span class=\'r\'>'+(v.SymmetryTitle != undefined ? v.SymmetryTitle : '-')+'</span></li><li><span class=\'l\'>Rondysta</span> <span class=\'r\'>'+(v.GirdleSizeMin != undefined ? v.GirdleSizeMin+'<br />'+v.GirdleSizeMax : '-')+'</span></li><li><span class=\'l\'>Kolet</span> <span class=\'r\'>'+(v.CuletSizeTitle != undefined ? v.CuletSizeTitle : '-')+'</span></li><li><span class=\'l\'>Fluorescencja</span> <span class=\'r\'>'+(v.FluorescenceIntensityTitle != undefined ? v.FluorescenceIntensityTitle : '-')+'</span></li><li><span class=\'l\'>Wymiary</span> <span class=\'r\'>'+(v.MeasLength != undefined ? v.MeasLength+'*'+v.MeasWidth+'*'+v.MeasDepth : '-')+'</span></li></ul></div>">'+
					'<td>'+v.ShapeTitle+'</td>'+
					'<td>'+v.Weight+'</td>'+
					'<td>'+v.ColorTitle+'</td>'+
					'<td>'+v.ClarityTitle+'</td>'+
					'<td>'+price+' zł</td>'+
					'<td>'+v.LabTitle+'</td>'+
					'<td>'+(v.PolishTitle != undefined ? v.PolishTitle : 'b/d')+'</td>'+
					'<td>'+(v.SymmetryTitle != undefined ? v.SymmetryTitle : 'b/d')+'</td>'+
					'<td>'+(v.CutLongTitle != undefined ? v.CutLongTitle : 'b/d')+'</td>'+
					'<td class="ds-link"><a href="#" class="ds-table-link" data-diamond-id="'+v.DiamondID+'">Zobacz</a></td>'+
				'</tr>');
			}
		});
	}

	fancy = false;

	$('.ds-table').on('click', '.ds-table-link', function(e)
	{
		e.preventDefault();
		if ($(this).attr('data-diamond-id') == undefined)
		{
			return false;
		}
		else
		{

			$.fancybox.showActivity();

			did = $(this).attr('data-diamond-id');
			$.get('fetch2.php', {'diamond_id': did}, function(ret)
			{


				$.fancybox(ret,
				{
					'width'         		: 'auto',
					'height'        		: 590,
					'padding': 0,
					'margin': 0,
					'scrolling': 'no',
					'centerOnScroll': true,
					'titleShow': false,
					'showCloseButton': false,
					'onComplete': function()
					{
						$('.ds-popup-images').on('click', 'a', function(e)
						{
							e.preventDefault();

							$('.ds-popup-images a').removeClass('select');
							$(this).addClass('select');

							$('.ds-popup-image img').remove();
							$('.ds-popup-image').html('<img src="'+$(this).attr('href')+'" />');

							return false;
						});

						$('.ds-popup-header').on('click', 'a', function(e)
						{
							e.preventDefault();

							$.fancybox.close();

							return false;
						});

						$('.ds-popup-form').on('submit', function(e)
						{
							e.preventDefault();

							data = $('.ds-popup-form').serialize();

							$('.ds-popup-form button').attr('disabled', true).fadeTo('fast', 0.5);

							$.post('mail.php', data, function(ret)
							{
								if (ret == 'ok')
								{
									$('.ds-popup-form p').html('<span style="color:green">Wiadomość została wysłana!</span>');
								}
								else
								{
									console.log(ret);
									$('.ds-popup-form p').html('<span style="color:red">Wystąpił błąd!</span>');
								}
								$('.ds-popup-form button').attr('disabled', false).fadeTo('fast', 1.0);
							});

							return false;
						});
					}
				});

			});
		}

		return false;
	});

	$('.ds-popup-header').on('click', 'a', function(e)
	{
		e.preventDefault();

		$.fancybox.close();

		return false;
	});

	$('.ds-popup-images').on('click', 'a', function(e)
	{
		e.preventDefault();

		$('.ds-popup-images a').removeClass('select');
		$(this).addClass('select');

		$('.ds-popup-image img').attr('src', $(this).attr('a'));

		return false;
	});

	$(".ds-help").tooltip({
		bodyHandler: function() {
			return $(this).attr("data-tip");
		},
		showURL: false
	});


	ds_load_data();
});