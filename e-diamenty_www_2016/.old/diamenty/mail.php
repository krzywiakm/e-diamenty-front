<?php
define("QUOTES_GPC", (ini_get('magic_quotes_gpc') ? TRUE : FALSE));

error_reporting(E_ALL);
require_once "vendor/phpmailer/class.phpmailer.php";

$to_email = 'bartek@bartek124.net';
$to_name = 'Diamenty';


ob_start();
$contact_email = stripinput($_POST['email']);
$contact_name = stripinput($_POST['name']." ".$_POST['surname']);
$contact_tel = stripinput($_POST['tel']);
$contact_message = stripinput($_POST['comment']);

$mail = new PHPMailer(false);
$mail->CharSet = "UTF-8";
$mail->AddReplyTo($contact_email, ($contact_name != '' ? $contact_name : ''));
$mail->AddAddress($to_email, $to_name);
$mail->SetFrom($contact_email, ($contact_name != '' ? $contact_name : ''));
$mail->Subject = 'Wiadomość kontaktowa z '.$to_name;

$mail_content = "";
$mail_content .= (!empty($contact_name) ? "Imię i nazwisko: ".$contact_name."<br />" : "");
$mail_content .= "E-Mail: ".$contact_email."<br />";
$mail_content .= (!empty($contact_tel) ? "Telefon: ".$contact_tel."<br />" : "");
$mail_content .= "Wiadomość:<br /><br />".nl2br($contact_message);

$mail->MsgHTML($mail_content);
$mail->Send();
ob_end_clean();
die('ok');

function stripinput($text) {
	if (!is_array($text)) {
		$text = stripslash(trim($text));
		$text = preg_replace("/(&amp;)+(?=\#([0-9]{2,3});)/i", "&", $text);
		$search = array("&", "\"", "'", "\\", '\"', "\'", "<", ">", "&nbsp;");
		$replace = array("&amp;", "&quot;", "&#39;", "&#92;", "&quot;", "&#39;", "&lt;", "&gt;", " ");
		$text = str_replace($search, $replace, $text);
	} else {
		foreach ($text as $key => $value) {
			$text[$key] = stripinput($value);
		}
	}
	return $text;
}

function stripslash($text) {
	if (QUOTES_GPC) { $text = stripslashes($text); }
	return $text;
}