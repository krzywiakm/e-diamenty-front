<?php
define("QUOTES_GPC", (ini_get('magic_quotes_gpc') ? TRUE : FALSE));
require_once "vendor/xml2array.php";

if ($waluty = @file_get_contents('http://www.nbp.pl/kursy/xml/a127z130703.xml'))
{
	$waluty = xml2array($waluty);
	$usd = $waluty['tabela_kursow']['pozycja'][1]['kurs_sredni'];
	unset($waluty);
	$usd = (double)str_replace(",", ".", $usd);
	if (!$usd)
	{
		// domyzlny przelicznik, jeden dolec=1zl, gdy nie brak danych na serwerze
		$usd = 1;
	}
}
else
{
	// domyzlny przelicznik, jeden dolec=1zl, gdy nie pobierze z serwera
	$usd = 1;
}

// USER CONFIG
$username = '61204';
$password = 'Crave+Stone';


$colors = array('1' => 'D', '2' => 'E', '3' => 'F', '4' => 'G', '5' => 'H', '6' => 'I', '7' => 'J', '8' => 'K', '9' => 'L', '10' => 'M');
$cleans = array('2' => 'IF', '3' => 'VVS1', '4' => 'VVS2', '5' => 'VS1', '6' => 'VS2', '7' => 'SI1', '8' => 'SI2', '9' => 'SI3', '10' => 'I1');
$goodnes = array('2' => 'Excellent', '3' => 'Very_Good', '4' => 'Good', '5' => 'Fair');
$goodnes2 = array('1' => 'Excellent', '2' => 'Very_Good', '3' => 'Good', '4' => 'Fair');
$sorts = array('price', 'shape', 'size', 'color', 'clarity', 'cut', 'lab');

$shapes = !empty($_POST['ds_shapes']) ? stripinput($_POST['ds_shapes']) : die(json_encode(array('error' => 'form0')));
foreach ($shapes as $i => $shape)
{
	$shapes[$i] = strtoupper($shape);
}

$sort = in_array($_POST['SortBy'], $sorts) ? $_POST['SortBy'] : 'price';

$_POST['ds_size_from'] = str_replace(",", ".", $_POST['ds_size_from']);
$_POST['ds_size_to'] = str_replace(",", ".", $_POST['ds_size_to']);

if ($_POST['ds_size_to'] > '100')
	$_POST['ds_size_to'] = 100;

$size_from = $_POST['ds_size_from'] >= 0 && $_POST['ds_size_from'] <= 100 ? $_POST['ds_size_from'] : die(json_encode(array('error' => 'form1')));
$size_to = $_POST['ds_size_to'] >= 0 && $_POST['ds_size_to'] <= 100 && $size_from <= $_POST['ds_size_to'] ? $_POST['ds_size_to'] : die(json_encode(array('error' => 'form2')));

$_POST['ds_price_from'] = str_replace(",", ".", $_POST['ds_price_from']);
$_POST['ds_price_to'] = str_replace(",", ".", $_POST['ds_price_to']);

if ($_POST['ds_price_from'] == '0')
	$_POST['ds_price_from'] = 10*$usd;

$price_from = is_numeric($_POST['ds_price_from']) && $_POST['ds_price_from'] > 0 ? $_POST['ds_price_from'] : die(json_encode(array('error' => 'form3')));
$price_to = is_numeric($_POST['ds_price_to']) && $_POST['ds_price_to'] > 0 ? $_POST['ds_price_to'] : die(json_encode(array('error' => 'form4')));
$price_from = $price_from/$usd;
$price_to = $price_to/$usd;

$color_from = isnum($_POST['ds_color_from']) && $_POST['ds_color_from'] >= 1 && $_POST['ds_color_from'] <= 10 ? $_POST['ds_color_from'] : die(json_encode(array('error' => 'form5')));
$color_to = isnum($_POST['ds_color_to']) && $_POST['ds_color_to'] >= 1 && $_POST['ds_color_to'] <= 10 ? $_POST['ds_color_to'] : die(json_encode(array('error' => 'form6')));
$color_from = $colors[$color_from];
$color_to = $colors[$color_to];

$clean_from = isnum($_POST['ds_clean_from']) && $_POST['ds_clean_from'] >= 2 && $_POST['ds_clean_from'] <= 10 ? $_POST['ds_clean_from'] : die(json_encode(array('error' => 'form7')));
$clean_to = isnum($_POST['ds_clean_to']) && $_POST['ds_clean_to'] >= 2 && $_POST['ds_clean_to'] <= 10 && $clean_from <= $_POST['ds_clean_to'] ? $_POST['ds_clean_to'] : die(json_encode(array('error' => 'form8')));
$clean_from = $cleans[$clean_from];
$clean_to = $cleans[$clean_to];

$cut_from = isnum($_POST['ds_cut_from']) && $_POST['ds_cut_from'] >= 2 && $_POST['ds_cut_from'] <= 5 ? $_POST['ds_cut_from'] : die(json_encode(array('error' => 'form9')));
$cut_to = isnum($_POST['ds_cut_to']) && $_POST['ds_cut_to'] >= 2 && $_POST['ds_cut_to'] <= 5 ? $_POST['ds_cut_to'] : die(json_encode(array('error' => 'form10')));
$cut_from = strtoupper($goodnes[$cut_from]);
$cut_to = strtoupper($goodnes[$cut_to]);

$poler_from = isnum($_POST['ds_poler_from']) && $_POST['ds_poler_from'] >= 2 && $_POST['ds_poler_from'] <= 5 ? $_POST['ds_poler_from'] : die(json_encode(array('error' => 'form11')));
$poler_to = isnum($_POST['ds_poler_to']) && $_POST['ds_poler_to'] >= 2 && $_POST['ds_poler_to'] <= 5 && $poler_from <= $_POST['ds_poler_to'] ? $_POST['ds_poler_to'] : die(json_encode(array('error' => 'form12')));
$poler_from = ($goodnes[$poler_from]);
$poler_to = ($goodnes[$poler_to]);

$symetry_from = isnum($_POST['ds_symetry_from']) && $_POST['ds_symetry_from'] >= 1 && $_POST['ds_symetry_from'] <= 4 ? $_POST['ds_symetry_from'] : die(json_encode(array('error' => 'form13')));
$symetry_to = isnum($_POST['ds_symetry_to']) && $_POST['ds_symetry_to'] >= 1 && $_POST['ds_symetry_to'] <= 4 && $symetry_from <= $_POST['ds_symetry_to'] ? $_POST['ds_symetry_to'] : die(json_encode(array('error' => 'form14')));
$symetry_from = ($goodnes2[$symetry_from]);
$symetry_to = ($goodnes2[$symetry_to]);

$page = isnum($_POST['ds_page']) ? $_POST['ds_page'] : die(json_encode(array('error' => 'form15')));

$certs = array();
if (!empty($_POST['ds_cert_gia']))
	$certs[] = 'GIA';
if (!empty($_POST['ds_cert_igi']))
	$certs[] = 'IGI';
if (!empty($_POST['ds_cert_hrd']))
	$certs[] = 'HRD';
if (!empty($_POST['ds_cert_egl']))
	$certs[] = 'EGL_USA';
if (!empty($_POST['ds_cert_ags']))
	$certs[] = 'AGS';
if (!empty($_POST['ds_cert_no']))
	$certs[] = 'NONE';


// logowanie
$client = new SoapClient("https://technet.rapaport.com/WebServices/RetailFeed/Feed.asmx?WSDL",
      array( "trace" => 1, "exceptions" => 0, "cache_wsdl" => 0) );
$params = array('Username'=>$username, 'Password'=>$password);
$client->__soapCall("Login", array($params), NULL, NULL, $output_headers);
$ticket = $output_headers["AuthenticationTicketHeader"]->Ticket;

// uruchomienie klienta
$client1 = new SoapClient("https://technet.rapaport.com/WebServices/RetailFeed/Feed.asmx?WSDL", array( "trace" => 1, "exceptions" => 0, "cache_wsdl" => 0) );
$ns = "http://technet.rapaport.com/";
$headerBody = array("Ticket" => $ticket);
$header = new SoapHeader($ns, 'AuthenticationTicketHeader', $headerBody);
$client1->__setSoapHeaders($header);

$searchparams["ShapeCollection"] = $shapes;
$searchparams["LabCollection"] = $certs;

$searchparams["SizeFrom"] = $size_from;
$searchparams["SizeTo"] = $size_to;
$searchparams["ColorFrom"] = $color_from;
$searchparams["ColorTo"] = $color_to;
$searchparams["ClarityFrom"] = $clean_from;
$searchparams["ClarityTo"] = $clean_to;
$searchparams["CutFrom"] = $cut_from;
$searchparams["CutTo"] = $cut_to;
$searchparams["SymmetryFrom"] = $symetry_from;
$searchparams["SymmetryTo"] = $symetry_to;
$searchparams["PolishFrom"] = $poler_from;
$searchparams["PolishTo"] = $poler_to;
$searchparams["PriceFrom"] = $price_from;
$searchparams["PriceTo"] = $price_to;

$searchparams["PageNumber"] = $page;
$searchparams["PageSize"] = 10;

$searchparams["SortDirection"] = 'ASC';
$searchparams["SortBy"] = strtoupper($sort);

//die(print_r($searchparams, true));

$params1 = array("SearchParams" => $searchparams, "DiamondsFound" => 0);

$client1->__soapCall("GetDiamonds", array($params1), NULL, NULL, $output_headers);

// odpowiedz serwera
$response = $client1->__getLastResponse();
// konwersja xml na tablice
$xml = xml2array($response);

// licznik diamentow
$diamonds_count = $xml['soap:Envelope']['soap:Body']['GetDiamondsResponse']['DiamondsFound'];

// wlasciwe drzewo
$diamonds = $xml['soap:Envelope']['soap:Body']['GetDiamondsResponse']['GetDiamondsResult']['diffgr:diffgram']['NewDataSet']['Table1'];
unset($xml);

if ($diamonds_count > 0)
{
	die(json_encode(array('diamonds_count' => $diamonds_count, 'diamonds' => $diamonds), JSON_FORCE_OBJECT));
}
else if ($diamonds_count == 0)
{
	die(json_encode(array('message' => 'not found')));
}
else
{
	die(json_encode(array('error' => 'unknown')));
}

// funkcje
function stripinput($text) {
	if (!is_array($text)) {
		$text = stripslash(trim($text));
		$text = preg_replace("/(&amp;)+(?=\#([0-9]{2,3});)/i", "&", $text);
		$search = array("&", "\"", "'", "\\", '\"', "\'", "<", ">", "&nbsp;");
		$replace = array("&amp;", "&quot;", "&#39;", "&#92;", "&quot;", "&#39;", "&lt;", "&gt;", " ");
		$text = str_replace($search, $replace, $text);
	} else {
		foreach ($text as $key => $value) {
			$text[$key] = stripinput($value);
		}
	}
	return $text;
}

function stripslash($text) {
	if (QUOTES_GPC) { $text = stripslashes($text); }
	return $text;
}

function isnum($value) {
	if (!is_array($value)) {
		return (preg_match("/^[0-9]+$/", $value));
	} else {
		return false;
	}
}