<?php
require_once "vendor/xml2array.php";

if ($waluty = @file_get_contents('http://www.nbp.pl/kursy/xml/a127z130703.xml'))
{
	$waluty = xml2array($waluty);
	$usd = $waluty['tabela_kursow']['pozycja'][1]['kurs_sredni'];
	unset($waluty);
	$usd = (double)str_replace(",", ".", $usd);

	if (!$usd)
	{
		// domyslny przelicznik, 1zl=1$, gdy brak danych na servie
		$usd = 1;
	}
}
else
{
	// domyslny przelicznik gdy problemy z polaczeniem do nbp.pl
	$usd = 1;
}
?>
<!doctype html>
<html>
		<head>
		<meta charset="utf-8">
		<title>Diamenty - e-diamenty</title>
		<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet'>
		<link rel="stylesheet" href="http://e-diamenty.pl/diamenty/style.css" />
		<script src="http://e-diamenty.pl/diamenty/js/jquery.min.js"></script>
		<script src="http://e-diamenty.pl/diamenty/js/jquery.nouislider.js"></script>
		<script src="http://e-diamenty.pl/diamenty/js/jquery.tooltip.js"></script>
		<script src="http://e-diamenty.pl/diamenty/fancybox/jquery.fancybox-1.3.4.js"></script>
		<script>
			var USD = <?php echo $usd; ?>;
		</script>
		<script src="http://e-diamenty.pl/js/jquery.page.js"></script>

		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		</head>

		<body>
<div id="top">
          <div id="top-inner"> <a href="http://e-diamenty.pl/"><img src="http://e-diamenty.pl/graf/logo_e-diamenty_main_150x150.png" width="150" height="150" alt="e-diamenty" /></a>
    <ul id="top-face-twitter">
              <li class="top-face"> <a href="#"><!--aa--></a> </li>
              <li class="top-line"><a href="#"><!--aa--></a></li>
              <li class="top-twitter"><a href="#"><!--aa--></a></li>
            </ul>
  </div>
        </div>
<div id="all">
          <nav id="main-menu">
    <ul>
              <li class="active"><a class="active" href="http://e-diamenty.pl/diamenty/"><span>Diamenty</span></a></li>
              <li><a href="http://e-diamenty.pl/gemmologia/"><span>Gemmologia</span></a></li>
              <li><a href="http://e-diamenty.pl/aktualnosci/"><span>Aktualności</span></a></li>
              <li><a href="http://e-diamenty.pl/kontakt/"><span>Kontakt</span></a></li>
            </ul>
  </nav>
          <div id="content">
    <div id="ds">
              <form method="post" id="ds-form">
        <div id="ds-shapes">
                  <h3>Wybór kształtu diamentu:</h3>
                  <ul>
            <li class="active"><a href="#">
              <div><img src="img/shapes/okragly.png" alt="Okrągły" /></div>
              <p>Okrągły</p>
              <input type='hidden' name='ds_shapes[]' value='round' />
              </a></li>
            <li><a href="#">
              <div><img src="img/shapes/gruszka.png" alt="Gruszka" /></div>
              <p>Gruszka</p>
              <input type='hidden' name='tmp' value='pear' />
              </a></li>
            <li><a href="#">
              <div><img src="img/shapes/princessa.png" alt="Princessa" /></div>
              <p>Princessa</p>
              <input type='hidden' name='tmp' value='princess' />
              </a></li>
            <li><a href="#">
              <div><img src="img/shapes/markiza.png" alt="Markiza" /></div>
              <p>Markiza</p>
              <input type='hidden' name='tmp' value='marquise' />
              </a></li>
            <li><a href="#">
              <div><img src="img/shapes/owal.png" alt="Owal" /></div>
              <p>Owal</p>
              <input type='hidden' name='tmp' value='oval' />
              </a></li>
            <li><a href="#">
              <div><img src="img/shapes/radiant.png" alt="Radiant" /></div>
              <p>Radiant</p>
              <input type='hidden' name='tmp' value='radiant' />
              </a></li>
            <li><a href="#">
              <div><img src="img/shapes/szmaragdowy.png" alt="Szmaragdowy" /></div>
              <p>Szmaragdowy</p>
              <input type='hidden' name='tmp' value='emerald' />
              </a></li>
            <li><a href="#">
              <div><img src="img/shapes/serce.png" alt="Serce" /></div>
              <p>Serce</p>
              <input type='hidden' name='tmp' value='heart' />
              </a></li>
            <li><a href="#">
              <div><img src="img/shapes/poduszka.png" alt="Poduszka" /></div>
              <p>Poduszka</p>
              <input type='hidden' name='tmp' value='cushion' />
              </a></li>
            <li><a href="#">
              <div><img src="img/shapes/asscher.png" alt="Asscher" /></div>
              <p>Asscher</p>
              <input type='hidden' name='tmp' value='asscher' />
              </a></li>
          </ul>
                </div>
        <hr />
        <div class="ds-two">
                  <div class="ds-1 ds-size">
            <header> <a href="#" class="ds-help" data-tip="<strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong>">Pomoc</a>
                      <h3>Masa (karaty):</h3>
                    </header>
            <div class="ds-size-slider noUiSlider"></div>
            <div class="ds-slide">
                      <input type="text" name="ds_size_from" class="ds-slide-from" value="0" size="2" min="0" max="100" />
                      <input type="text" name="ds_size_to" class="ds-slide-to" value="100" size="2" min="0" max="100" />
                    </div>
          </div>
                  <div class="ds-2 ds-price">
            <header> <a href="#" class="ds-help" data-tip="<strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong>">Pomoc</a>
                      <h3>Zakres cen:</h3>
                    </header>
            <div class="ds-price-slider noUiSlider"></div>
            <div class="ds-slide">
                      <label class="ds-price-from">
                <input type="text" name="ds_price_from" class="ds-slide-from" value="10" size="9" min="10" max="<?php echo 10000000*$usd; ?>" />
                zł</label>
                      <label>
                <input type="text" name="ds_price_to" class="ds-slide-to" value="<?php echo 10000000*$usd; ?>" size="9" min="10" max="<?php echo 10000000*$usd; ?>" />
                zł</label>
                    </div>
          </div>
                </div>
        <div class="ds-clear"></div>
        <hr />
        <div class="ds-three">
                  <div class="ds-1 ds-color">
            <header> <a href="#" class="ds-help" data-tip="<strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong>">Pomoc</a>
                      <h3>Barwa:</h3>
                    </header>
            <div class="ds-color-slider ds-expand noUiSlider"></div>
            <div class="ds-slide">
                      <input type="hidden" name="ds_color_from" class="ds-slide-from" value="1" size="1" />
                      <input type="hidden" name="ds_color_to" class="ds-slide-to" value="10" size="1" />
                    </div>
          </div>
                  <div class="ds-2 ds-clean">
            <header> <a href="#" class="ds-help" data-tip="<strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong>">Pomoc</a>
                      <h3>Czystość:</h3>
                    </header>
            <div class="ds-clean-slider ds-expand noUiSlider"></div>
            <div class="ds-slide">
                      <input type="hidden" name="ds_clean_from" class="ds-slide-from" value="2" size="1" />
                      <input type="hidden" name="ds_clean_to" class="ds-slide-to" value="10" size="1" />
                    </div>
          </div>
                  <div class="ds-3 ds-cut">
            <header> <a href="#" class="ds-help" data-tip="<strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong>">Pomoc</a>
                      <h3>Szlif:</h3>
                    </header>
            <div class="ds-cut-slider ds-expand noUiSlider"></div>
            <div class="ds-slide">
                      <input type="hidden" name="ds_cut_from" class="ds-slide-from" value="2" size="1" />
                      <input type="hidden" name="ds_cut_to" class="ds-slide-to" value="5" size="1" />
                    </div>
          </div>
                </div>
        <div class="ds-clear"></div>
        <hr />
        <div class="ds-cert">
        <header>
        <a href="#" class="ds-help" data-tip="<strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong>">Pomoc</a>
        <h3>Certyfikat:</h3>
        <ul>
        <li><span class="ds-checkbox checked" data-input-name="ds_cert_gia"></span>
                  <p>GIA</p>
                  <input type="hidden" name="ds_cert_gia" value="1" />
                </li>
        <li><span class="ds-checkbox checked" data-input-name="ds_cert_igi"></span>
                  <p>IGI</p>
                  <input type="hidden" name="ds_cert_igi" value="1" />
                </li>
        <li><span class="ds-checkbox checked" data-input-name="ds_cert_ags"></span>
                  <p>AGS</p>
                  <input type="hidden" name="ds_cert_ags" value="1" />
                </li>
        <li><span class="ds-checkbox checked" data-input-name="ds_cert_hrd"></span>
                  <p>HRD</p>
                  <input type="hidden" name="ds_cert_hrd" value="1" />
                </li>
        <li><span class="ds-checkbox checked" data-input-name="ds_cert_no"></span>
                  <p>Niecertyfikowane</p>
                  <input type="hidden" name="ds_cert_no" value="1" />
                </li>
        </header>
        </div>
        <hr />
        <div class="ds-advanced">
                  <h2>Opcje zaawansowane:</h2>
                  <div class="ds-poler">
            <header> <a href="#" class="ds-help" data-tip="<strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong>">Pomoc</a>
                      <h3>Poler:</h3>
                    </header>
            <div class="ds-poler-slider ds-expand noUiSlider"></div>
            <div class="ds-slide">
                      <input type="hidden" name="ds_poler_from" class="ds-poler-from" value="2" size="1" />
                      <input type="hidden" name="ds_poler_to" class="ds-poler-to" value="5" size="1" />
                    </div>
          </div>
                  <div class="ds-symetry">
            <header> <a href="#" class="ds-help" data-tip="<strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong>">Pomoc</a>
                      <h3>Symetria:</h3>
                    </header>
            <div class="ds-symetry-slider ds-expand noUiSlider"></div>
            <div class="ds-slide">
                      <input type="hidden" name="ds_symetry_from" class="ds-symetry-from" value="1" size="1" />
                      <input type="hidden" name="ds_symetry_to" class="ds-symetry-to" value="4" size="1" />
                    </div>
          </div>
                </div>
        <input type="hidden" name="tmp" value="" />
        <input type="hidden" name="ds_page" value="1" />
        <input type="hidden" name="SortBy" value="shape" />
        <input type="hidden" name="SortDirection" value="asc" />
        <!--<button type="submit" class="lol">Szukaj</button>
			<pre>
			<div class="test"></div>
			</pre>-->
      </form>
              <div class="ds-table-header">
        <p class="ds-table-count">Znaleziono <span class="ds-count">0</span> diamentów odpowiadających kryteriom wyszukiwania</p>
        <a href="#" class="ds-table-reset">Resetuj</a>
        <p class="ds-table-sort">Kliknij kolumnę by posortować</p>
      </div>
              <table class="ds-table" cellpadding="0" cellspacing="0">
        <tbody>
                  <tr>
            <th data-sort="shape">Kształt</th>
            <th data-sort="size">Masa</th>
            <th data-sort="color">Barwa</th>
            <th data-sort="clarity">Czystość</th>
            <th data-sort="price">Cena</th>
            <th data-sort="lab">Certyfikat</th>
            <th>Poler</th>
            <th>Symetria</th>
            <th data-sort="cut">Szlif</th>
            <th class="ds-loader"><img src="img/loader.png" alt="loader" /></th>
          </tr>
                </tbody>
        <tbody id="ds-results">
                  <tr>
            <td colspan="10">Brak diamentów o podanych parametrach. Zmień kryteria wyszukiwania.</td>
          </tr>
                </tbody>
      </table>
              <div class="ds-table-footer">
        <p>Strony:</p>
        <a href="#" class="ds-pagin-next">Następne 10 stron</a>
        <div class="ds-pagin-container">
                  <ul class="ds-pagin">
            <li><a href="#" class="current">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">6</a></li>
            <li><a href="#">7</a></li>
            <li><a href="#">8</a></li>
            <li><a href="#">9</a></li>
            <li><a href="#">10</a></li>
            <li><a href="#">11</a></li>
            <li><a href="#">12</a></li>
            <li><a href="#">13</a></li>
          </ul>
                </div>
        <a href="#" class="ds-pagin-prev">Poprzednie 10 stron</a> </div>
            </div>
            <div class="clear"></div>
  </div>
          <footer id="stopka">
    <div class="footer-img"> <img src="http://e-diamenty.pl/graf/logo_e-diamenty_small_33x22.png" width="33" height="22" alt="diament"> </div>
    <ul class="footer-list">
              <li>KONTAKT</li>
              <li>+48 61 095 654</li>
              <li>+48 61 095 654</li>
              <li><a href="mailto:kontakt@e-diamenty.pl">kontakt@e-diamenty.pl</a></li>
            </ul>
    <ul class="footer-list">
              <li>REGULAMIN</li>
              <li><a href="regulamin/#regulamin-strony">Regulamin</a></li>
              <li><a href="regulamin/#regulamin-cookies">Regulamin cookies</a></li>
              <li><a href="regulamin/#regulamin-inne">Inne</a></li>
            </ul>
    <ul class="footer-list">
              <li>AKTUALNOŚCI</li>
              <li><a href="http://e-diamenty.pl/news/2/61/swiatowa-Rada-Zlota-ma-na-celu-modernizacje-London-Gold-Fix/" title="Światowa Rada Złota ma na celu modernizację London Gold Fix">Najnowsze</a></li>
              <li><a href="archiwum">Archiwum</a></li>
            </ul>
    <ul class="footer-list">
              <li>GEMMOLOGIA</li>
              <li><a href="gemmologia#czystosc">Czystość</a></li>
              <li><a href="gemmologia#budowa">Budowa</a></li>
              <li><a href="gemmologia#budowa2">Budowa 2</a></li>
              <li><a href="gemmologia#budowa3">Budowa 3</a></li>
            </ul>
  </footer>
        </div>
</body>
</html>
