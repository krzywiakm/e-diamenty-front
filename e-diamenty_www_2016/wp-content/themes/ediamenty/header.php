<!DOCTYPE html>
<html lang="pl">
<head>
<meta charset="utf-8" />
<meta name="description" content="<?php bloginfo("description"); ?>" /> 
<meta name="robots" content="index, follow" />
<title><?php if ( is_home() ) { ?><?php bloginfo('name'); ?><?php } else { ?><?php wp_title(''); ?> | <?php bloginfo('name'); ?><?php } ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo( "template_url" ); ?>/assets/css/jquery.fullPage.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo( "template_url" ); ?>/assets/css/normalize.css" />       
<link rel="stylesheet" href="<?php bloginfo( "stylesheet_url" ); ?>" type="text/css" />     
<link rel="stylesheet" type="text/css" href="<?php bloginfo( "template_url" ); ?>/assets/css/responsive.css" />
<link rel='shortcut icon' href='<?php bloginfo( "template_url" ); ?>/assets/images/favicon.png' type='image/x-icon' />
<script async src="<?php bloginfo( "template_url" ); ?>/assets/js/modernizr.custom.js"></script>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 

<?php wp_head(); ?>
</head>
<body>
<?php if(!is_front_page()): ?>
<header id="header">
    <nav id="nav" class="fixed-nav">
        <div class="grid grid-pad">
            <div class="row">
                <div class="col-2-12 ">
                    <a id="nlogo" href="<?php bloginfo( "url" ); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/nav-logo.png" alt="E-diamenty Logo" /></a>
                </div>
                <div class="col-8-12">
                    <div id="cssmenu" class="page-menu">
                    <?php
                        $defaults = array(
                            'theme_location'  => '',
                            'menu'            => 'Nawigacja',
                            'container_class' => '',
                            'container_id'    => '',
                            'menu_class'      => 'menu',
                            'menu_id'         => '',
                            'echo'            => true,
                            'fallback_cb'     => 'wp_page_menu',
                            'before'          => '',
                            'after'           => '',
                            'link_before'     => '',
                            'link_after'      => '',
                            'items_wrap'      => '<ul>%3$s</ul>',
                            'depth'           => 0,
                            'walker'          => ''
                            );
                        wp_nav_menu($defaults);
                    ?>
                    </div>
                </div>
                <div class="col-2-12 ">
                    <ul class="social-media flright">
                        <li><a href="https://plus.google.com/117928545126692028251" title="Google+" rel="publisher" class="transition"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/gplus.png" alt="G+" width="26" height="26" /></a></li>
                        <li><a href="https://www.facebook.com/ediamentypl/" title="Facebook" class="transition"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/fb.png" alt="FB" width="26" height="26" /></a></li>
                        <li><a href="https://www.linkedin.com/company/diamond-investment-company" title="linkediIn" class="transition"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/in.png" alt="IN" width="26" height="26" /></a></li>
                        <li><a href="https://twitter.com/e_diamenty_pl" title="Twitter" class="transition"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/tw.png" alt="TW" width="26" height="26" /></a></li>                        
<li><a href="https://www.instagram.com/e_diamenty.pl/" title="Istagram" class="transition"><img src="http://e-diamenty.pl/wp-content/uploads/2015/12/instagram.png" alt="INS" width="26" height="26" /></a></li>
<li><a href="https://www.pinterest.com/ediamenty/" title="Twitter" class="transition"><img src="http://e-diamenty.pl/wp-content/uploads/2015/12/pinterest.png" alt="PIN" width="26" height="26" /></a></li>



                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>
<?php endif; ?>