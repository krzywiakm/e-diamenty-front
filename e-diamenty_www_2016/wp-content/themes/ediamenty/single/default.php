<?php get_header(); ?>
<div id="main">
    <div id="posts">
        <div class="posts-bar post-back">
            <div class="grid">
                <div class="row">
                    <div class="col-1-1"><a href="<?php echo esc_url( get_category_link( 1 ) ); ?>" title="Powrót do aktualności" class="prev">wstecz</a></div>
                </div>
            </div>
        </div>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="grid grid-single">
            <div class="row">
                <div class="col-5-12">
                    <?php if ( has_post_thumbnail() ) { 
                        the_post_thumbnail('post-single');
                    }?>   
                </div>
                <div class="col-7-12">
                    <article>
                        <header>
                            <span class="date"><?php the_time('d/m/Y'); ?></span>
                            <h2 class="title"><?php the_title(); ?></h2>
                        </header>
                        <?php the_content(); ?>
                        <?php get_template_part('parts/navigation'); ?>
                    </article>
                </div>
            </div>
        </div>
        <?php endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>