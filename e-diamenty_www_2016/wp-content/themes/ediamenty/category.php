<?php get_header(); ?>
<div id="main">
    <div id="posts">
        <?php get_template_part('parts/pagination'); ?>
        <div class="grid grid-post">
            <div class="row">
                <?php 
                    if ( have_posts() ) : while ( have_posts() ) : the_post(); 
                        
                        $content = get_the_content();
                ?>
                <div class="col-1-2">
                    <div class="grid grid-single-post">
                        <div class="row">
                            <div class="col-1-2">
                                <div class="thumbnail">
                                    <?php if ( has_post_thumbnail() ) { 
                                        the_post_thumbnail('post-category');
                                    }?>   
                                </div>
                            </div>
                            <div class="col-1-2">
                                <div class="post-content">
                                    <span class="date"><?php the_time('d/m/Y'); ?></span>
                                    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                                    <p><?php echo wp_trim_words($content, 20); ?></p>
                                    
                                    <a href="<?php the_permalink(); ?>" title="Zobacz więcej" class="rmore">Zobacz więcej</a>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>