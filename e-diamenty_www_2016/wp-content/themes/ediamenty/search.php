<?php get_header(); ?>
<section id="content" class="container posts">
    <div class="heading"><h1>Wyniki wyszukiwania:</h1></div>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <article>
            <ul class="form-search">
                <li><a href="<?php the_permalink();?>"><?php the_title(); ?></a></li>
            </ul>
        </article>
        <?php endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
        <?php endif; ?>
</section>
<?php get_footer(); ?>