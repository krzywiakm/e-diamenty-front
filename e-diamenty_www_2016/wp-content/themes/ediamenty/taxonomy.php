<?php
    $post = $wp_query->post;
    if ( is_tax( 'koniopedia_categories' ) ) {
        include(TEMPLATEPATH.'/taxonomy/koniopedia_categories.php');
    } elseif ( is_tax( 'contacts_categories' ) ) {
        include(TEMPLATEPATH . '/taxonomy/contacts.php');
    } else {
        include(TEMPLATEPATH.'/taxonomy/default.php');
    }
?> 