<?php if(!is_front_page()): ?>
    <div class="section fp-auto-height" id="footer">
        <a class="goup" href="#top">Przejdź na górę</a>
        <div class="grid">
            <div class="row">
                <div class="col-1-1">
                    <a id="flogo" href="<?php bloginfo( "url" ); ?>" title="E-diamenty"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/e-diamenty-flogo.png" alt="E-diamenty Footer Logo" /></a>
                </div>
            </div>
            
            <div class="row">
                <div class="col-1-1">
                    <nav id="fnav">
                        <?php
                            $defaults = array(
                                'theme_location'  => '',
                                'menu'            => 'Stopka',
                                'container_class' => '',
                                'container_id'    => '',
                                'menu_class'      => 'menu',
                                'menu_id'         => '',
                                'echo'            => true,
                                'fallback_cb'     => 'wp_page_menu',
                                'before'          => '',
                                'after'           => '',
                                'link_before'     => '',
                                'link_after'      => '',
                                'items_wrap'      => '<ul>%3$s</ul>',
                                'depth'           => 0,
                                'walker'          => ''
                                );
                            wp_nav_menu($defaults);
                        ?>
                    </nav>
                </div>
            </div>
            
            <div class="row">
                <div class="col-1-3">
                    <div class="copyright">
                            Created by <a href="http://hesna.pl" title="Hesna IMS">HESNA IMS</a>
                    </div>
                </div>
                <div class="col-1-3">
                   <div class="partners">
                        <ul>
                            <li><a href="http://www.gia.edu/" title="Gia" target="_blank"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/gia-partner.png" alt="GIA" /></a></li>
                            <li><a href="http://www.igiworldwide.com/" title="igiworldwide" target="_blank"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/igi-partner.png" alt="IGI" /></a></li>
                            <li><a href="http://www.hrdantwerp.com/" title="HRDa" target="_blank"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/hrda-partner.png" alt="HRDa" /></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-1-3">
                    <div class="biznes">
                        <ul class="social-media-footer">
                            <li><a href="https://plus.google.com/117928545126692028251" title="Google+" rel="publisher" class="transition"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/gplus.png" alt="G+" width="26" height="26" /></a></li>
                            <li><a href="https://www.facebook.com/ediamentypl/" title="Facebook" class="transition"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/fb.png" alt="FB" width="26" height="26" /></a></li>
                            <li><a href="https://www.linkedin.com/company/diamond-investment-company" title="linkediIn" class="transition"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/in.png" alt="IN" width="26" height="26" /></a></li>
                            <li><a href="https://twitter.com/e_diamenty_pl" title="Twitter" class="transition"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/tw.png" alt="TW" width="26" height="26" /></a></li>                        
                            <li><a href="https://www.instagram.com/e_diamenty.pl/" title="Istagram" class="transition"><img src="http://e-diamenty.pl/wp-content/uploads/2015/12/instagram.png" alt="INS" width="26" height="26" /></a></li>
                            <li><a href="https://www.pinterest.com/ediamenty/" title="Twitter" class="transition"><img src="http://e-diamenty.pl/wp-content/uploads/2015/12/pinterest.png" alt="PIN" width="26" height="26" /></a></li>
                        </ul>
                        <img src="http://e-diamenty.pl/wp-content/uploads/2016/02/gepard.png" alt="gepard" />
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script><script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php bloginfo( "template_url" ); ?>/assets/js/jquery.fullPage.js"></script>
<script type="text/javascript" src="<?php bloginfo( "template_url" ); ?>/assets/js/script.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#scrollpage').fullpage({
            anchors: ['home', 'o-nas', 'diamenty', 'poradnik', 'posty', 'stopka'],
            menu: '#menu',
            navigation: true,
            verticalCentered: false,
            autoScrolling: false
        });
    });
</script>

<script type="text/javascript">
$("a[href='#top']").click(function() {
  $("html, body").animate({ scrollTop: 0 }, "slow");
  return false;
});
</script>
<?php wp_footer(); ?>     
</body>
</html>
