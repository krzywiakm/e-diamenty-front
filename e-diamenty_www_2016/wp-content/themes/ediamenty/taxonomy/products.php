<?php get_header(); ?>
<section id="posts" class="products">
    <div class="container">
        <div class="heading">
        <?php
            $term =	$wp_query->queried_object;
        ?>        
            <h2 class="title"><?php echo $term->name; ?><span class="sep"></span></h2>
        </div>
        <ul class="posts">
            <?php 
                $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
                $term_link = get_term_link( $term, 'products_categories' ); 
                global $wp_query;
                query_posts( array_merge( $wp_query->query, array( 'post_type'=> 'products', 'posts_per_page' => 8, 'order' => 'DESC', ) ) );
                    if (have_posts()) : while ( have_posts() ) : the_post();
            ?>                
            <li>
                <article>
                    <div class="thumbnail row row-25">
                    <?php if ( has_post_thumbnail() ): ?>
                        <?php the_post_thumbnail('product-category'); ?>
                    <?php endif; ?>
                    </div>
                    <div class="content row row-75">
                        <header>
                            <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                        </header>
                        <p><?php echo wp_trim_words(get_the_content(), 50); ?></p>
                        <footer><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="rmore">więcej</a></footer>
                    </div>
                </article>
            </li>
            <?php endwhile; else: ?>
                <li><?php _e('Sorry, no posts matched your criteria.'); ?></li>
            <?php endif; ?>            
        </ul>
    </div>
</section>
<?php get_footer(); ?>