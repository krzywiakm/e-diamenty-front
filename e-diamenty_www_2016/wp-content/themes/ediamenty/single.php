<?php
    $post = $wp_query->post;
    if ( is_singular( 'koniopedia' ) ) {
        include(TEMPLATEPATH.'/single/koniopedia.php');
    } elseif ( is_singular( 'artist' ) ) {
        include(TEMPLATEPATH.'/single/artist.php');
    } else {
        include(TEMPLATEPATH.'/single/default.php');
    }
?> 