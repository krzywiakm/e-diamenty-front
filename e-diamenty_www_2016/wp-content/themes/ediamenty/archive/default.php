<?php get_header(); ?>
<?php get_template_part( 'parts/product-categories'); ?>
<section id="content" class="single-product container">
    <div class="indentation">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="custom_page">
            <h1 class="title"><?php the_title(); ?></h1>
            <div class="custom_page-content">
                <?php if ( has_post_thumbnail() ) { 
                    the_post_thumbnail('post-thumbnail', array('class' => 'alignright'));
                }?>     
                <?php the_content(); ?>
            </div>
        </div>
        <?php endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
        <?php endif; ?>
    </div>
</section>
<?php get_footer(); ?>