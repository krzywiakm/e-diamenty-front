<?php get_header(); ?>
<div id="scrollpage">
    <!-- Home -->
    <div class="section" id="home">
        <div class="container">
            <div class="grid">
                <div class="row">
                    <div class="col-1-1">
                        <a id="logo" href="<?php bloginfo( "url" ); ?>/" title="<?php bloginfo('name'); ?>"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/e-diamenty-logo.png" alt="E-diamenty logo" /></a>
                        <nav id="nav">
                            <div id="cssmenu">
                                <?php
                                    $defaults = array(
                                        'theme_location'  => '',
                                        'menu'            => 'Nawigacja',
                                        'container_class' => '',
                                        'container_id'    => '',
                                        'menu_class'      => 'menu',
                                        'menu_id'         => '',
                                        'echo'            => true,
                                        'fallback_cb'     => 'wp_page_menu',
                                        'before'          => '',
                                        'after'           => '',
                                        'link_before'     => '',
                                        'link_after'      => '',
                                        'items_wrap'      => '<ul>%3$s</ul>',
                                        'depth'           => 0,
                                        'walker'          => ''
                                        );
                                    wp_nav_menu($defaults);
                                ?>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>  
        <div id="scroll-page">Scroll Down</div>  
	</div>
    <!-- Home -->
    
    <!-- About -->
    <div class="section" id="about">
        <div class="container">
            <div class="grid grid-pad">
                <div class="row">
                    <div class="col-1-2"></div>
                    <div class="col-1-2 flright">
                        <article class="front-page">
                            <header>
                                <h2 class="title">O nas</h2>
                                <p>Jesteśmy największą na rynku firmą zajmującą się handlem diamentami i od lat zaopatrujemy największe sieci jubilerskie w Polsce w nieoprawione diamenty. Wchodzimy na rynek detaliczny aby klienci mogli kupować brylanty w realnych cenach hurtowych, bez marży całego łańcucha pośredników. </p>
                                <a href="<?php echo get_the_permalink(4); ?>" title="O nas" class="rmore">czytaj więcej</a>
                            </header>
                        </article>
                    </div>
                </div>
            </div>
        </div>    
	</div>
    <!-- About -->
    
    <!-- Diamenty -->
    <div class="section" id="diamonds">
        <div class="container">
            <div class="grid grid-pad">
                <div class="col-1-2 flleft">
                    <article class="front-page">
                        <header>
                            <h2 class="title">Diamenty</h2>
                            <p>W naszej stałej ofercie znajdują się tysiące diamentów dostępnych od ręki, które już niebawem będą widoczne w sklepie internetowym e-diamenty.pl. Do czasu uruchomienia skepu zapraszamy Państwa do bezpośredniego kontaktu z nami w celu poznania oferty oraz zakupu kamieni.</p> 
                            <a href="<?php echo get_the_permalink(8); ?>" title="Diamenty" class="button transition">Dalej</a>
                        </header>
                    </article>
                </div>
            </div>
        </div>    
	</div>
    <!-- Diamenty -->
    
    <!-- Poradnik -->
    <div class="section" id="guide">
        <div class="container">
            <div class="grid grid-pad">
                <div class="row">
                    <div class="col-1-2 flright">
                        <article class="front-page gemmology">
                            <header>
                                <h2 class="title">Poradnik</h2>
                                <p>Nieodłączna cechą kamieni szlachetnych są mikroskopijne inkluzje, zwane też „wrostkami”. Diamenty posiadają znamiona wewnętrzne (nazywane inkluzjami), jak również znamiona zewnętrzne (zwane skazami), które czynią każdy kamień wyjątkowym i niepowtarzalnym. Powstały w procesie formowania się kamieni. Oglądanie kamienia pod dziesięciokrotnym powiększeniem pozwala na określenie klasy czystości.Diament tym cenniejszy im mniej posiada inkluzji i skaz. Idealnie czyste brylanty są rzadkością. Szczególnym zainteresowaniem cieszą się te kamienie, których kryterium określającym czystość mieszczą się w przedziale SI – małe inkluzje. Jeżeli czystość oznaczona jest jako SI lub wyższa, „wrostki” zazwyczaj nie są widoczne gołym okiem. </p>
                                <a href="<?php echo get_the_permalink(15); ?>" title="Poradnik" class="rmore">czytaj więcej</a>
                            </header>
                        </article>
                    </div>
                </div>
            </div>
        </div>    
	</div>
    <!-- Poradnik -->

    <!-- Posty -->
    <div class="section fp-auto-height" id="posts">
        <div class="grid">
            <div class="row">
                <?php query_posts('cat=1&posts_per_page=4'); ?> 
                <?php 
                    if ( have_posts() ) : while ( have_posts() ) : the_post(); 
                        
                        $content = get_the_content();
                ?>
                <div class="col-1-2 col-post">
                    <div class="grid">
                        <div class="row">
                            <div class="col-1-2">
                                <div class="thumbnail">
                                    <?php if ( has_post_thumbnail() ) { 
                                        the_post_thumbnail('post-category');
                                    }?>  
                                </div>
                            </div>
                            <div class="col-1-2">
                                <div class="post-content">
                                    <span class="date"><?php the_time('d/m/Y'); ?></span>
                                    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                                    <p><?php echo wp_trim_words($content, 20); ?></p>
                                    <a href="<?php the_permalink(); ?>" title="Zobacz więcej" class="rmore">Zobacz więcej</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
    <!-- Posty -->
    
    <!-- Stopka -->
    <div class="section fp-auto-height" id="footer">
        <a class="goup" href="#top">Przejdź na górę</a>
        <div class="grid">
            <div class="row">
                <div class="col-1-1">
                    <a id="flogo" href="<?php bloginfo( "url" ); ?>" title="E-diamenty.pl"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/e-diamenty-flogo.png" alt="E-diamenty Footer Logo" /></a>
                </div>
            </div>
            
            <div class="row">
                <div class="col-1-1">
                    <nav id="fnav">
                        <?php
                            $defaults = array(
                                'theme_location'  => '',
                                'menu'            => 'Stopka',
                                'container_class' => '',
                                'container_id'    => '',
                                'menu_class'      => 'menu',
                                'menu_id'         => '',
                                'echo'            => true,
                                'fallback_cb'     => 'wp_page_menu',
                                'before'          => '',
                                'after'           => '',
                                'link_before'     => '',
                                'link_after'      => '',
                                'items_wrap'      => '<ul>%3$s</ul>',
                                'depth'           => 0,
                                'walker'          => ''
                                );
                            wp_nav_menu($defaults);
                        ?>
                    </nav>
                </div>
            </div>
            
            <div class="row">
                <div class="col-1-3">
                    <div class="copyright">
                            Created by <a href="http://hesna.pl" title="Hesna IMS">HESNA IMS</a>
                    </div>
                </div>
                <div class="col-1-3">
                   <div class="partners">
                        <ul>
                            <li><a href="http://www.gia.edu/" title="Gia" target="_blank"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/gia-partner.png" alt="GIA" /></a></li>
                            <li><a href="http://www.igiworldwide.com/" title="igiworldwide" target="_blank"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/igi-partner.png" alt="IGI" /></a></li>
                            <li><a href="http://www.hrdantwerp.com/" title="HRDa" target="_blank"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/hrda-partner.png" alt="HRDa" /></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-1-3">
                    <div class="biznes">
                        <ul class="social-media-footer">
                            <li><a href="https://plus.google.com/117928545126692028251" title="Google+" rel="publisher" class="transition"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/gplus.png" alt="G+" width="26" height="26" /></a></li>
                            <li><a href="https://www.facebook.com/ediamentypl/" title="Facebook" class="transition"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/fb.png" alt="FB" width="26" height="26" /></a></li>
                            <li><a href="https://www.linkedin.com/company/diamond-investment-company" title="linkediIn" class="transition"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/in.png" alt="IN" width="26" height="26" /></a></li>
                            <li><a href="https://twitter.com/e_diamenty_pl" title="Twitter" class="transition"><img src="<?php bloginfo( "template_url" ); ?>/assets/images/icons/tw.png" alt="TW" width="26" height="26" /></a></li>                        
                            <li><a href="https://www.instagram.com/e_diamenty.pl/" title="Istagram" class="transition"><img src="http://e-diamenty.pl/wp-content/uploads/2015/12/instagram.png" alt="INS" width="26" height="26" /></a></li>
                            <li><a href="https://www.pinterest.com/ediamenty/" title="Twitter" class="transition"><img src="http://e-diamenty.pl/wp-content/uploads/2015/12/pinterest.png" alt="PIN" width="26" height="26" /></a></li>
                        </ul>
                        <img src="http://e-diamenty.pl/wp-content/uploads/2016/02/gepard.png" alt="gepard" class="gepard" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Stopka -->
</div>
<?php get_footer(); ?>