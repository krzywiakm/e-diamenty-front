<?php get_header(); ?>
<div id="main" class="main-contact">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="grid grid-posts">
        <div class="row">
            <div class="col-1-2">
                <div class="contact contact-info">
                    <?php the_content(); ?>             
                </div>
            </div>
            <div class="col-1-2">
                <div class="contact contact-form">
                    <h2 class="title">Formularz kontaktowy</h2>
                    <p>Jeżeli mają Państwo jakiekolwiek pytania prosimy o wypełnienie poniższego formularza. Pola oznaczone * są wymagane.</p>
                    <?php echo do_shortcode('[contact-form-7 id="26" title="Formularz 1"]'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="google-maps">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2434.213820354643!2d16.860036900000004!3d52.402800899999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470444e483948195%3A0x54ec5d820be8545c!2zxZp3aWVyemF3c2thIDEsIFBvem5hxYQ!5e0!3m2!1spl!2spl!4v1444120478155" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <?php endwhile; else: ?>
        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
    <?php endif; ?>
</div>
<?php get_footer(); ?>