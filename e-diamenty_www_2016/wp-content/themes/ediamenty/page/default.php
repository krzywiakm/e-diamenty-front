<?php get_header(); ?>
<div id="main">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="grid grid-posts">
        <div class="row">
            <div class="col-1-1">
                <article>
                    <header><h1 class="title"><?php the_title(); ?></h1></header>
                    <?php the_content(); ?>
                </article>
            </div>
        </div>
    </div>
    <?php endwhile; else: ?>
        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
    <?php endif; ?>
</div>
<?php get_footer(); ?>