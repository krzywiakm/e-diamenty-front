<?php get_header(); ?>
<section id="content" class="container posts">
    <div class="heading"><h1><?php the_title(); ?></h1></div>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <article>
            <?php if ( has_post_thumbnail() ) { 
                the_post_thumbnail('post-thumbnail', array('class' => 'alignright'));
            }?>         
            <?php the_content(); ?>
        </article>
        <?php endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
        <?php endif; ?>
</section>
<?php get_footer(); ?>