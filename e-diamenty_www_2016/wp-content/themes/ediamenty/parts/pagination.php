<div class="posts-bar">
    <div class="grid grid-bar grid-pad">
        <div class="row">
            <?php
                $postlist_args = array(
                    'posts_per_page'  => -1,
                    'orderby'         => 'menu_order title',
                    'order'           => 'ASC',
                    'post_type'       => 'post',
                    'category' => $term->slug
                ); 
                
                $postlist = get_posts( $postlist_args );
                $ids = array();
                    
                foreach ($postlist as $thepost) {
                    $ids[] = $thepost->ID;
                }
                    
                $thisindex = array_search($post->ID, $ids);
                $previd = $ids[$thisindex-1];
                $nextid = $ids[$thisindex+1];
            ?>
            
            <div class="col-1-3">
                <?php if ( !empty($previd) ): ?>
                <div class="nav-previous"><?php next_posts_link( __( 'poprzednie' ) ); ?></div>
                <?php endif; ?>
            </div>
            
            <div class="col-1-3"><h1>Aktualności</h1></div>
            
            <div class="col-1-3">
                <?php if ( !empty($nextid) ): ?>
                <div class="nav-next"><?php previous_posts_link( __( 'następne' ) ); ?></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>