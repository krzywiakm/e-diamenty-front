<div class="small-navigation">
    <?php
        $postlist_args = array(
            'posts_per_page'  => -1,
            'orderby'         => 'menu_order title',
            'order'           => 'ASC',
            'post_type'       => 'post',
            'category' => $term->slug
        ); 
                    
        $postlist = get_posts( $postlist_args );
        $ids = array();
                        
        foreach ($postlist as $thepost) {
            $ids[] = $thepost->ID;
        }
                        
        $thisindex = array_search($post->ID, $ids);
        $previd = $ids[$thisindex-1];
        $nextid = $ids[$thisindex+1];
                        
        if ( !empty($previd) ) {
            echo '<a rel="prev" href="' . get_permalink($previd). '" class="prev" title="Poprzedni post: '.get_the_title($previd).'">Poprzedni</a>';
        }
    ?>
    <?php
                        
        if ( !empty($nextid) ) {
            echo '<a rel="next" href="' . get_permalink($nextid). '" class="next" title="Następny post: '.get_the_title($previd).'">Następny</a>';
        }
    ?> 
</div>