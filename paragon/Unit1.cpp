//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "THERMALLIBLib_OCX"
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
int StrtoWc( BSTR *bsData, AnsiString Data )
{
        //DebugBreak();
        wchar_t *wBuff = (wchar_t *)calloc(200,  sizeof( wchar_t ));

        int StrLen = 0;

        char cBuff[200];

        char *pc = cBuff;
        wchar_t *p_wBuff = wBuff;

        memset(cBuff, 0, sizeof(cBuff));


        if(Data.Length() > sizeof(cBuff)-1) return -1;

        strcpy(cBuff, Data.c_str());

       // wchar_t *p_wData = wData;

        while(1)
        {
           mbtowc( p_wBuff, pc, 1 );
           p_wBuff++;
           pc++;
           StrLen++;
           if(*pc == '\0') break;
        }

        *bsData = SysAllocString(wBuff);

        free(wBuff);

        return StrLen;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormActivate(TObject *Sender)
{
int paragonID=0;
if(Caption=="")
    Caption="paragon:0";

try { paragonID = StrToInt(Caption.SubString(9,Caption.Length()-8)); }
catch (Exception &exception) {Application->ShowException(&exception); Application->Terminate(); return;}

if(paragonID<=0)
    {
    Form1->Width=60;
    Form1->Height=27;
    Form2->ShowModal();
    Application->Terminate();
    return;
    }

AnsiString asPort, asSL1, asSL2, asSL3, asNL1, asNL2, asNL3;
TIniFile *plikIni = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
asPort = plikIni->ReadString("Druk", "port", "COM1");
asNL1 = plikIni->ReadString("Druk", "n1", "Linia 1");
asNL2 = plikIni->ReadString("Druk", "n2", "Linia 2");
asNL3 = plikIni->ReadString("Druk", "n3", "Linia 3");
asSL1 = plikIni->ReadString("Druk", "s1", "Linia 1");
asSL2 = plikIni->ReadString("Druk", "s2", "Linia 2");
asSL3 = plikIni->ReadString("Druk", "s3", "Linia 3");
delete plikIni;

String sParID = IntToStr(paragonID);
StaticText2->Caption = "Paragon id: "+sParID;
StaticText1->Caption = "��czenie z baz�...";
Application->ProcessMessages();
try { SQLConnection1->Open(); }
catch (Exception &exception) {Application->ShowException(&exception); Application->Terminate(); return;}

StaticText1->Caption = "Pobieranie danych...";
Application->ProcessMessages();
SQLDataSet1->CommandText="select * from FAKTURY where Rodzaj=0 and Id="+sParID;
try { SQLDataSet1->Open(); }
catch (Exception &exception) {Application->ShowException(&exception); Application->Terminate(); return;}

if(!SQLDataSet1->RecordCount)
    {
    ShowMessage("Brak paragonu o podanym id!");
    Application->Terminate();
    return;
    }

bool fisk = SQLDataSet1->FieldByName("Fisk")->AsInteger;
int nrzam = SQLDataSet1->FieldByName("IdZam")->AsInteger;
String sNumer = SQLDataSet1->FieldByName("Numer")->AsString;
float fRabat = SQLDataSet1->FieldByName("Rabat")->AsFloat;
SQLDataSet1->Close();
StaticText2->Caption = "Paragon id: "+sParID+", nr: "+sNumer;
Application->ProcessMessages();

if(fisk)
    {
    ShowMessage("Paragon zosta� ju� wydrukowany!");
    Application->Terminate();
    return;
    }

SQLDataSet1->CommandText="select *, count(*) Ilosc from FAKTURYPOZ where IdFak="+sParID+
                    " group by Symbol, Rozmiar order by Symbol, Rozmiar";
try { SQLDataSet1->Open(); }
catch (Exception &exception) {Application->ShowException(&exception); Application->Terminate(); return;}

int ile=0;
while(!SQLDataSet1->Eof) {SQLDataSet1->Next(); ile++;}
SQLDataSet1->Close();

try { SQLDataSet1->Open(); }
catch (Exception &exception) {Application->ShowException(&exception); Application->Terminate(); return;}

String *sSymbol=new String[ile];
int *iRozmiar=new int[ile], *iTyp=new int[ile], *iIlosc=new int[ile];
float *fBrutto=new float[ile];
for(int i=0; i<ile; i++)
    {
    sSymbol[i] = SQLDataSet1->FieldByName("Symbol")->AsString;
    iRozmiar[i] = SQLDataSet1->FieldByName("Rozmiar")->AsInteger;
    iTyp[i] = SQLDataSet1->FieldByName("Typ")->AsInteger;
    fBrutto[i] = SQLDataSet1->FieldByName("Brutto")->AsFloat;
    iIlosc[i] = SQLDataSet1->FieldByName("Ilosc")->AsInteger;
    if(sSymbol[i]=="ZALICZKA")
        fBrutto[i]=-fBrutto[i];
    SQLDataSet1->Next();
    }
SQLDataSet1->Close();
SQLConnection1->Close();
      //////////
//String x = sParID +" "+sNumer+" "+FormatFloat("0.00",fRabat)+"\n\n";
//for(int i=0; i<ile; i++)
//    x+=sSymbol[i]+" "+iRozmiar[i]+" "+iIlosc[i]+" "+FormatFloat("0.00",fBrutto[i])+"\n";
//
//ShowMessage(x);        
     ///////////


    //b1,b2,lb(loop),b3  (z przykladu thermallib)
//ThermalLib1->

//b1

        BSTR port;
        StrtoWc(&port, asPort);  

        ThermalLib1->THLOpenPort(port);
        Sleep(100);
        BSTR Info = SysAllocString(ThermalLib1->LBIDRQ());

        StaticText3->Caption = WideCharToString(Info);
        Application->ProcessMessages();

        if(StaticText3->Caption=="-10")
            {
            ShowMessage("B��d otwarcia portu!");
            Application->Terminate(); return;
            }

//b2

        StaticText1->Caption = "Drukowanie nag��wka...";
        Application->ProcessMessages();

        BSTR t1;
        BSTR t2;
        BSTR t3;

        StrtoWc(&t1, asNL1);
        StrtoWc(&t2, asNL2);
        StrtoWc(&t3, asNL3);

        long ret = ThermalLib1->LBTRSHDR(0,0,t1,t2,t3);

        if(ret == 0)
            StaticText1->Caption = "Nag��wek: OK";
        else
            {
            StaticText1->Caption = "Nag��wek: "+IntToStr(ret);
            ShowMessage("B��d wydruku nag��wka!");
            Application->Terminate(); return;
            }

        Application->ProcessMessages();

//lb

        double Dilosc  = 0,
               Dcena   = 0,
               Dbrutto = 0,
               dwartosc  = 0;



        for(int i=0; i<ile; i++)
            {
            StaticText1->Caption = "Drukowanie pozycji "+IntToStr(i)+"...";
            Application->ProcessMessages();

            BSTR Nazwa, Ilosc, Ptu, Cena, Brutto, Rabat, OpisRab;

            Dilosc =  iIlosc[i];
            Dcena  =  fBrutto[i];

            Dbrutto = Dilosc * Dcena;

            dwartosc += Dbrutto;

            AnsiString nazwa    = sSymbol[i];
            if(iTyp[i]==1)
                nazwa = sSymbol[i]+"("+IntToStr(iRozmiar[i])+")";
            AnsiString ilosc    = FormatFloat("0",Dilosc); //0.000
            AnsiString ptu      = "A";
            AnsiString cena     = FormatFloat("0.00",Dcena);
            AnsiString brutto   = FormatFloat("0.00",Dbrutto);
            AnsiString rabat    = "0";
            AnsiString opis_rab = "";

            //brutto[brutto.Pos(",")] = '.';
            //cena[cena.Pos(",")] = '.';
            //ilosc[ilosc.Pos(",")] = '.';

            //ShowMessage(brutto);

            StrtoWc(&Nazwa  , nazwa);
            StrtoWc(&Ilosc  , ilosc);
            StrtoWc(&Ptu    , ptu);
            StrtoWc(&Cena   , cena);
            StrtoWc(&Brutto , brutto);
            StrtoWc(&Rabat  , rabat);
            StrtoWc(&OpisRab, opis_rab);
                                               //
            ret = ThermalLib1->LBTRSLN((short)i+1,(short)0,(short)0,Nazwa,Ilosc,Ptu,Cena,Brutto,Rabat,OpisRab);

            if(ret == 0)
                {
                StaticText1->Caption = "Pozycja "+IntToStr(i)+": OK";
                Application->ProcessMessages();
                }
            else
                {
                StaticText1->Caption = "Pozycja "+IntToStr(i)+": "+IntToStr(ret);
                ShowMessage("B��d wydruku linii!");
                Application->Terminate(); return;
                }

            }//for


//b3

        //BSTR t1;
        //BSTR t2;
        //BSTR t3;

        StrtoWc(&t1, asSL1 + IntToStr(nrzam));
        StrtoWc(&t2, asSL2);
        StrtoWc(&t3, asSL3);

        BSTR Kasa;
        BSTR Wplata;
        BSTR Razem;
        BSTR Rab;


        StrtoWc(&Kasa  , "WM1");
       // StrtoWc(&Wplata, "5");


        double Drazem  = 0;
        Drazem =  dwartosc;

        AnsiString sRazem = FormatFloat("0.00",Drazem);
        AnsiString sRabat = FormatFloat("0.00",fRabat);

        //sRazem[sRazem.Pos(",")] = '.';

        StrtoWc(&Razem, sRazem);
        StrtoWc(&Wplata,sRazem);

        StrtoWc(&Rab, sRabat);


        ret = ThermalLib1->LBTREXIT(0,3,3, Kasa, t1,t2,t3,  Wplata, Razem, Rab );

        bool ok=false;
        if(ret == 0)
            {
            StaticText1->Caption = "Stopka: "+IntToStr(ret);
            Application->ProcessMessages();
            ok=true;
            }
        else
            {
            StaticText1->Caption = "Stopka: "+IntToStr(ret);
            ShowMessage("B��d wydruku stopki!");
            Application->Terminate(); return;
            }

        //zamkniecie portu
        ThermalLib1->THLClosePort();

if(ok)
    {
    StaticText1->Caption = "��czenie z baz�...";
    Application->ProcessMessages();
    try { SQLConnection1->Open(); }
    catch (Exception &exception) {Application->ShowException(&exception); Application->Terminate(); return;}
    SQLDataSet1->CommandText="update FAKTURY set Fisk=1 where Rodzaj=0 and Id="+sParID;
    StaticText1->Caption = "Zaznaczanie wydrukowanego paragonu...";
    Application->ProcessMessages();
    try { SQLDataSet1->ExecSQL(true); }
    catch (Exception &exception) {Application->ShowException(&exception); Application->Terminate(); return;}
    SQLConnection1->Close();
    StaticText1->Caption = "Gotowe!";
    Application->ProcessMessages();
    }

Sleep(3000);
Application->Terminate();
}
//---------------------------------------------------------------------------
bool __fastcall TForm1::MessageHandler(TMessage &Message)
{
    if(Message.Msg == msgRestore)
        {
        Application->Restore();
        Application->BringToFront();      
        return true;
        }
    else return false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
    msgRestore = RegisterWindowMessage("TylkoTy");
    Application->HookMainWindow(MessageHandler);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormDestroy(TObject *Sender)
{
    Application->UnhookMainWindow(MessageHandler);
}
//---------------------------------------------------------------------------

