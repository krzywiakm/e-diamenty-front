object Form1: TForm1
  Left = 244
  Top = 113
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'Form1'
  ClientHeight = 67
  ClientWidth = 372
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object StaticText1: TStaticText
    Left = 8
    Top = 25
    Width = 6
    Height = 17
    Caption = '|'
    TabOrder = 1
  end
  object ThermalLib1: TThermalLib
    Left = 336
    Top = 0
    Width = 32
    Height = 32
    ControlData = {00000100560A00002B05000000000000}
  end
  object StaticText2: TStaticText
    Left = 8
    Top = 8
    Width = 44
    Height = 17
    Caption = 'Paragon'
    TabOrder = 2
  end
  object StaticText3: TStaticText
    Left = 8
    Top = 42
    Width = 6
    Height = 17
    Caption = '|'
    TabOrder = 3
  end
  object SQLConnection1: TSQLConnection
    ConnectionName = 'MySQLConnection'
    DriverName = 'MySQL'
    GetDriverFunc = 'getSQLDriverMYSQL'
    LibraryName = 'dbexpmysql.dll'
    LoadParamsOnConnect = True
    LoginPrompt = False
    Params.Strings = (
      'DriverName=MySQL'
      'HostName=ServerName'
      'Database=DBNAME'
      'User_Name=user'
      'Password=password'
      'BlobSize=-1'
      'ErrorResourceFile='
      'LocaleCode=0000')
    VendorLib = 'libmysql.dll'
    Left = 256
  end
  object SQLDataSet1: TSQLDataSet
    SQLConnection = SQLConnection1
    Params = <>
    Left = 296
  end
end
