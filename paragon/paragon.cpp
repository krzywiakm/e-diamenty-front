//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("Unit1.cpp", Form1);
USEFORM("Unit2.cpp", Form2);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR a, int)
{
        try
        {
            //Sprawdzamy, czy istnieje mutex
            HANDLE Mutex = OpenMutex(MUTEX_ALL_ACCESS, false, "TylkoJedenEgzemplarz");
            if (Mutex == NULL) // to jest jedyny egzemplarz
                {
                //Tworzymy mutex
                Mutex = CreateMutex(NULL, true, "TylkoJedenEgzemplarz");
                }
            else //to nie jest jedyny egzemplarz
                {
                //Wysy�amy poprzedniemu egzemplarzowi aplikacji komunikat z pro�b�
                //o powr�t do pracy i wysuni�cie si� na pierwszy plan
                SendMessage(HWND_BROADCAST, RegisterWindowMessage("TylkoTy"), 0, 0);
                return 0;
                }

             //Pozwalamy aplikacji uruchomi� si� normalnie
             Application->Initialize();
             Application->CreateForm(__classid(TForm1), &Form1);
             Application->CreateForm(__classid(TForm2), &Form2);
             Application->MainForm->Caption = a;
             Application->Run();

             //Wypuszczamy mutex
             ReleaseMutex(Mutex);
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        catch (...)
        {
                 try
                 {
                         throw Exception("");
                 }
                 catch (Exception &exception)
                 {
                         Application->ShowException(&exception);
                 }
        }
        return 0;
}
//---------------------------------------------------------------------------
