object Form2: TForm2
  Left = 1277
  Top = 116
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Paragon - ustawienia'
  ClientHeight = 372
  ClientWidth = 283
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 58
    Width = 62
    Height = 13
    Caption = 'Nag'#322#243'wek 1:'
  end
  object Label2: TLabel
    Left = 8
    Top = 82
    Width = 62
    Height = 13
    Caption = 'Nag'#322#243'wek 2:'
  end
  object Label3: TLabel
    Left = 8
    Top = 106
    Width = 62
    Height = 13
    Caption = 'Nag'#322#243'wek 3:'
  end
  object Label4: TLabel
    Left = 8
    Top = 18
    Width = 63
    Height = 13
    Caption = 'Port drukarki:'
  end
  object Label5: TLabel
    Left = 192
    Top = 18
    Width = 81
    Height = 13
    Caption = '_______'
  end
  object Label6: TLabel
    Left = 8
    Top = 138
    Width = 46
    Height = 13
    Caption = 'Stopka 1:'
  end
  object Label7: TLabel
    Left = 8
    Top = 162
    Width = 46
    Height = 13
    Caption = 'Stopka 2:'
  end
  object Label8: TLabel
    Left = 8
    Top = 186
    Width = 46
    Height = 13
    Caption = 'Stopka 3:'
  end
  object Label9: TLabel
    Left = 8
    Top = 298
    Width = 32
    Height = 13
    Caption = 'Has'#322'o:'
  end
  object Label10: TLabel
    Left = 8
    Top = 226
    Width = 25
    Height = 13
    Caption = 'Host:'
  end
  object Label11: TLabel
    Left = 8
    Top = 250
    Width = 27
    Height = 13
    Caption = 'Baza:'
  end
  object Label12: TLabel
    Left = 8
    Top = 274
    Width = 25
    Height = 13
    Caption = 'User:'
  end
  object Bevel1: TBevel
    Left = 8
    Top = 212
    Width = 265
    Height = 2
    Shape = bsTopLine
  end
  object Bevel2: TBevel
    Left = 8
    Top = 44
    Width = 265
    Height = 2
    Shape = bsTopLine
  end
  object Edit1: TEdit
    Left = 72
    Top = 56
    Width = 200
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 72
    Top = 80
    Width = 200
    Height = 21
    TabOrder = 1
    Text = 'Edit2'
  end
  object Edit3: TEdit
    Left = 72
    Top = 104
    Width = 200
    Height = 21
    TabOrder = 2
    Text = 'Edit3'
  end
  object Edit4: TEdit
    Left = 72
    Top = 16
    Width = 57
    Height = 21
    TabOrder = 3
    Text = 'COM1'
  end
  object Button1: TButton
    Left = 144
    Top = 16
    Width = 41
    Height = 21
    Caption = 'Test'
    TabOrder = 4
    OnClick = Button1Click
  end
  object ThermalLib1: TThermalLib
    Left = 240
    Top = 8
    Width = 32
    Height = 32
    ControlData = {00000100560A00002B05000000000000}
  end
  object BitBtn1: TBitBtn
    Left = 72
    Top = 336
    Width = 75
    Height = 25
    TabOrder = 6
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 160
    Top = 336
    Width = 75
    Height = 25
    TabOrder = 7
    Kind = bkCancel
  end
  object Edit5: TEdit
    Left = 72
    Top = 136
    Width = 200
    Height = 21
    TabOrder = 8
    Text = 'Edit1'
  end
  object Edit6: TEdit
    Left = 72
    Top = 160
    Width = 200
    Height = 21
    TabOrder = 9
    Text = 'Edit2'
  end
  object Edit7: TEdit
    Left = 72
    Top = 184
    Width = 200
    Height = 21
    TabOrder = 10
    Text = 'Edit3'
  end
  object Edit8: TEdit
    Left = 72
    Top = 296
    Width = 201
    Height = 21
    PasswordChar = '*'
    TabOrder = 11
    Text = 'Edit8'
  end
  object Edit9: TEdit
    Left = 72
    Top = 224
    Width = 201
    Height = 21
    TabOrder = 12
    Text = 'Edit9'
  end
  object Edit10: TEdit
    Left = 72
    Top = 248
    Width = 201
    Height = 21
    TabOrder = 13
    Text = 'Edit10'
  end
  object Edit11: TEdit
    Left = 72
    Top = 272
    Width = 201
    Height = 21
    TabOrder = 14
    Text = 'Edit11'
  end
end
