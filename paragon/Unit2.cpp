//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "THERMALLIBLib_OCX"
#pragma resource "*.dfm"
TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------
int StrtoWc( BSTR *bsData, AnsiString Data )
{
        //DebugBreak();
        wchar_t *wBuff = (wchar_t *)calloc(200,  sizeof( wchar_t ));

        int StrLen = 0;

        char cBuff[200];

        char *pc = cBuff;
        wchar_t *p_wBuff = wBuff;

        memset(cBuff, 0, sizeof(cBuff));


        if(Data.Length() > sizeof(cBuff)-1) return -1;

        strcpy(cBuff, Data.c_str());

       // wchar_t *p_wData = wData;

        while(1)
        {
           mbtowc( p_wBuff, pc, 1 );
           p_wBuff++;
           pc++;
           StrLen++;
           if(*pc == '\0') break;
        }

        *bsData = SysAllocString(wBuff);

        free(wBuff);

        return StrLen;
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Button1Click(TObject *Sender)
{
BSTR port;
StrtoWc(&port, Edit4->Text);

ThermalLib1->THLOpenPort(port);
Sleep(100);
BSTR Info = SysAllocString(ThermalLib1->LBIDRQ());

Label5->Caption = WideCharToString(Info);

ThermalLib1->THLSendBel();
ThermalLib1->THLClosePort();
}
//---------------------------------------------------------------------------
void __fastcall TForm2::BitBtn1Click(TObject *Sender)
{
TIniFile *plikIni = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
plikIni->WriteString("Druk", "port", Edit4->Text);
plikIni->WriteString("Druk", "n1", Edit1->Text);
plikIni->WriteString("Druk", "n2", Edit2->Text);
plikIni->WriteString("Druk", "n3", Edit3->Text);
plikIni->WriteString("Druk", "s1", Edit5->Text);
plikIni->WriteString("Druk", "s2", Edit6->Text);
plikIni->WriteString("Druk", "s3", Edit7->Text);
delete plikIni;

plikIni = new TIniFile(ExtractFilePath(Application->ExeName)+"dbxconnections.ini");
plikIni->WriteString("MySQLConnection", "Password", Edit8->Text);
plikIni->WriteString("MySQLConnection", "HostName", Edit9->Text);
plikIni->WriteString("MySQLConnection", "Database", Edit10->Text);
plikIni->WriteString("MySQLConnection", "User_Name", Edit11->Text);
delete plikIni;
}
//---------------------------------------------------------------------------
void __fastcall TForm2::FormShow(TObject *Sender)
{
TIniFile *plikIni = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
Edit4->Text = plikIni->ReadString("Druk", "port", "COM1");
Edit1->Text = plikIni->ReadString("Druk", "n1", "Linia 1");
Edit2->Text = plikIni->ReadString("Druk", "n2", "Linia 2");
Edit3->Text = plikIni->ReadString("Druk", "n3", "Linia 3");
Edit5->Text = plikIni->ReadString("Druk", "s1", "Linia 1");
Edit6->Text = plikIni->ReadString("Druk", "s2", "Linia 2");
Edit7->Text = plikIni->ReadString("Druk", "s3", "Linia 3");
delete plikIni;

plikIni = new TIniFile(ExtractFilePath(Application->ExeName)+"dbxconnections.ini");
Edit8->Text = plikIni->ReadString("MySQLConnection", "Password", "");
Edit9->Text = plikIni->ReadString("MySQLConnection", "HostName", "");
Edit10->Text = plikIni->ReadString("MySQLConnection", "Database", "");
Edit11->Text = plikIni->ReadString("MySQLConnection", "User_Name", "");
delete plikIni;
}
//---------------------------------------------------------------------------
