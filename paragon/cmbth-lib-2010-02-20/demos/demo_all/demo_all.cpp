//
//
//
//			POSNET BASE EXAMPLES
//			EXAMPLE DIFFERENT REQUEST HANDLING
//
//

#include <stdlib.h>
#include <string>
#include <iostream>
#include <istream>
#include <limits>
#include <time.h>
#ifdef WIN32
#include <windows.h>
#else
#define INFINITE 0xFFFFFFFF
#endif
#include "../../include/posnet.h"

using namespace std;

/* Funkcja pomocnicza - drukuje status rozkazu jesli niezerowy, zwraca otrzymany status */
POSNET_STATUS PrintStatus(POSNET_STATUS status)
{
	const char *txt;
	char bufor[1024];
	if (status!=0)
	{
		txt=POS_GetErrorString(status,"pl");
		printf("B��d: %d - %s\n",status,txt?txt:"(brak opisu)");
	}
	return status;
}

/* oczekiwanie na zako�czenie z mozliwoscia przedluzenia oczekiwania */
bool WaitForCompleted(POSNET_HANDLE hRequest,POSNET_HANDLE hLocalDevice)
{
	POSNET_STATUS status;
	do {
		// oczekujemy 10 s  na zako�zcenie rozkazu
		status=POS_WaitForRequestCompleted(hRequest,10000);
		if (status==POSNET_STATUS_OK) return true;
		if (status!=POSNET_STATUS_TIMEOUT)
		{
			// je�li status nie jest POSNET_STATUS_TIMEOUT ani POSNET_STATUS_OK to
			// nast�pi� b��d wykonania, wy�witlamy go i ko�czymy
			PrintStatus(status);
			return false;
		}
		// je�li jest TIMEOUT to sprawd�my i wydrukujmy status drukarki
		long globalStatus=0,printerStatus=0;
		PrintStatus(POS_GetPrnDeviceStatus(hLocalDevice,1,&globalStatus,&printerStatus));
		printf("Status drukarki - Drukarka: %ld, Mechanizm: %ld\n",globalStatus,printerStatus);
		// dajemy u�ytkownikowi szans� na przed�u�enie oczekiwania
		printf("Nie otrzymano odpowiedzi na polecenie, czy czeka� dalej (T/N + ENTER)\n");
		do {
			char c;
			if (scanf("%c",&c)<=0) { fflush(stdin); continue; }
			if (c=='N' || c=='n') return false;
			if (c=='T' || c=='t') break;
		}
		while (1);
	} while(1);
	return true;
}


void FunRaportDobowy(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequest(hLocalDevice,"dailyrep");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}

		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;

		if (!WaitForCompleted(hRequest,hLocalDevice)) break;

		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;

		POS_DestroyRequest(hRequest);
		
		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
	}
}

void FunUstawienieStopki(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequestEx(hLocalDevice,"ftrcfg","bc,1234567890");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}

		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;

		if (!WaitForCompleted(hRequest,hLocalDevice)) break;

		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;

		POS_DestroyRequest(hRequest);
		
		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
	}
}

void FunForm25(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequestEx(hLocalDevice,"formstart","fn,25\nfh,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,25\nfl,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,25\nfl,1");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,25\nfl,2");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,25\nfl,3");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,25\nfl,4");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,25\nfl,5");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,25\nfl,6");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,25\nfl,7");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formend","fn,25");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
		hRequest = POS_CreateRequest(hLocalDevice,"prncancel");
		PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL));
		PrintStatus(POS_WaitForRequestCompleted(hRequest,5000));
		POS_DestroyRequest(hRequest);
	}
}

void FunForm32(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequestEx(hLocalDevice,"formstart","fn,32\nfh,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,1");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);
		
		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,2");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,3");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

			hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,4");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,5");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,6");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,7");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,8");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,9");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,10");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,11");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,12");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,13");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,14");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,15");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,16");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"formline","fn,32\nfl,17");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"s1","12345#####12345#####12345#####12345#####\n########################################\n########################################\n########################################");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);


		hRequest = POS_CreateRequestEx(hLocalDevice,"formend","fn,32");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
		hRequest = POS_CreateRequest(hLocalDevice,"prncancel");
		PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL));
		PrintStatus(POS_WaitForRequestCompleted(hRequest,5000));
		POS_DestroyRequest(hRequest);
	}
}

void FunTransakcjaOpakowan(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequest(hLocalDevice,"trpackinit");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trpack","na,1\nne,1\npr,100\nil,1\nwa,100");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trpack","na,2\nne,0\npr,100\nil,2\nwa,200");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trend","op,200\nom,100");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
		hRequest = POS_CreateRequest(hLocalDevice,"prncancel");
		PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL));
		PrintStatus(POS_WaitForRequestCompleted(hRequest,5000));
		POS_DestroyRequest(hRequest);
	}
}





void FunFakturaVATOsoby(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequestEx(hLocalDevice,"trfvinit","nb,102\nni,123-456-78-90\nna,HURTOWNIA\npd,2008-11-10\npt,got�wka\nsc,Adam Adamski\nss,Piotr Piotrowski\nps,1");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trline","na,Towar A\nvt,0\npr,10000");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trend","to,10000");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
		hRequest = POS_CreateRequest(hLocalDevice,"prncancel");
		PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL));
		PrintStatus(POS_WaitForRequestCompleted(hRequest,5000));
		POS_DestroyRequest(hRequest);
	}
}


void FunFakturaVAT(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequestEx(hLocalDevice,"trfvinit","nb,102\nni,123-456-78-90\nna,HURTOWNIA\npd,2008-11-10\npt,got�wka\nps,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trline","na,Towar A\nvt,0\npr,10000");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trend","to,10000");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
		hRequest = POS_CreateRequest(hLocalDevice,"prncancel");
		PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL));
		PrintStatus(POS_WaitForRequestCompleted(hRequest,5000));
		POS_DestroyRequest(hRequest);
	}
}


void FunSprzedazProsta(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequestEx(hLocalDevice,"trinit","bm,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trline","na,Towar A\nvt,0\npr,99999");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trend","to,99999");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
		hRequest = POS_CreateRequest(hLocalDevice,"prncancel");
		PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL));
		PrintStatus(POS_WaitForRequestCompleted(hRequest,5000));
		POS_DestroyRequest(hRequest);
	}
}

void FunSprzedazRabat(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequestEx(hLocalDevice,"trinit","bm,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trline","na,Towar A\nvt,0\npr,10000");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trdiscntbill","na,popo�udniowy\nrd,1\nrp,1000");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trend","to,9000");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);
		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
		hRequest = POS_CreateRequest(hLocalDevice,"prncancel");
		PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL));
		PrintStatus(POS_WaitForRequestCompleted(hRequest,5000));
		POS_DestroyRequest(hRequest);
	}
}


void FunSprzedazRabatLinii(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequestEx(hLocalDevice,"trinit","bm,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trline","na,Towar A\nvt,0\npr,9999999\nrd,1\nrn,rabacik\nrw,099");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trend","to,9999900");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
		hRequest = POS_CreateRequest(hLocalDevice,"prncancel");
		PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL));
		PrintStatus(POS_WaitForRequestCompleted(hRequest,5000));
		POS_DestroyRequest(hRequest);
	}
}

void FunSprzedazEURO(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequestEx(hLocalDevice,"trinit","bm,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trline","na,Towar A\nvt,0\npr,10000");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trpaymentcurr","wc,5000\nra,23000\nna,EURO\nsb,EUR");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trend","to,10000\nfp,11500");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);
		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
		hRequest = POS_CreateRequest(hLocalDevice,"prncancel");
		PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL));
		PrintStatus(POS_WaitForRequestCompleted(hRequest,5000));
		POS_DestroyRequest(hRequest);
	}
}

void FunSprzedazVISA(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequestEx(hLocalDevice,"trinit","bm,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trline","na,Towar A\nvt,0\npr,9999999");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trpayment","ty,2\nwa,10000000\nna,VISA");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trend","to,9999999\nfp,10000000");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);
		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
		hRequest = POS_CreateRequest(hLocalDevice,"prncancel");
		PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL));
		PrintStatus(POS_WaitForRequestCompleted(hRequest,5000));
		POS_DestroyRequest(hRequest);
	}
}

void FunSprzedazStopka(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{
		hRequest = POS_CreateRequestEx(hLocalDevice,"trinit","bm,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trline","na,Towar A\nvt,0\npr,9999999");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequestEx(hLocalDevice,"trend","to,9999999\nfe,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest = POS_CreateRequest(hLocalDevice,"trftrend");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);
		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
		hRequest = POS_CreateRequest(hLocalDevice,"prncancel");
		PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL));
		PrintStatus(POS_WaitForRequestCompleted(hRequest,5000));
		POS_DestroyRequest(hRequest);
	}
}


void FunLinieStopki(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{

		hRequest = POS_CreateRequestEx(hLocalDevice,"ftrinfoset","lb,0");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"tx","&w&cZapraszamy ponownie\n&w&cna wielkie zakupy\n&cPA PA PA");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
	}
}

void FunNaglowek(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{

		hRequest = POS_CreateRequest(hLocalDevice,"hdrset");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"tx","&cDrukarka fiskalna\n&w&cPOSNET COMBO DF FV");
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
	}
}



void FunOkresowy(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	char bufor[1024];
	char data_od[16];
	char data_do[16];

	do
	{
		int n;
		printf("Podaj dat� pocz�tkow� i ko�cow� okresu w formacie rrrr-mm-dd i naci�nij ENTER\n");
		n=scanf("%10s %10s",data_od,data_do);
		if (n==2) break;
		fflush(stdin);
	} while(1);

	sprintf(bufor,"fd,%s\ntd,%s\nsu,0",data_od,data_do);

	do
	{

		hRequest = POS_CreateRequestEx(hLocalDevice,"periodicrepbydates",bufor);
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
	}
}

void FunUstawVAT(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	do
	{

		hRequest = POS_CreateRequestEx(hLocalDevice,"vatset","va,22\nvb,7\nvc,0\nvd,3\nvg,100");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
	}
}

void FunUstawZegar(POSNET_HANDLE hGlobalDevice, POSNET_HANDLE hLocalDevice)
{
	int cancel=1;
	POSNET_HANDLE hRequest;
	char buffer[128];


	time_t t=time (NULL);
	struct tm *tm=localtime (&t);
	strftime (buffer, 128, "%Y-%m-%d,%H:%M", tm);

	do
	{

		hRequest = POS_CreateRequest(hLocalDevice,"rtcset");
		if (!hRequest)
		{
				PrintStatus(POS_GetError(hLocalDevice));
				break;		
		}
		POS_PushRequestParam(hRequest,"da",buffer);
		if (PrintStatus(POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL))!=POSNET_STATUS_OK) break;
		if (!WaitForCompleted(hRequest,hLocalDevice)) break;
		if (PrintStatus(POS_GetRequestStatus(hRequest)) != POSNET_STATUS_OK) break;
		POS_DestroyRequest(hRequest);

		hRequest=NULL;
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		if (hRequest) POS_DestroyRequest(hRequest);
	}
}

int main(int argc, char *argv[])
{

#ifdef WIN32
	WSADATA WSAData;
	if (WSAStartup (MAKEWORD(2,0), &WSAData) != 0) 
	{
		MessageBox (NULL, TEXT("WSAStartup failed!"), TEXT("Error"), MB_OK);
		return FALSE;
	}

 #endif
 
#include "../setup.inc"

	unsigned long pi=5;
	POS_SetDeviceParam(hDevice,POSNET_DEV_PARAM_STATUSPOLLINGINTERVAL,&pi);
	
	long globalStatus=0,printerStatus=0;
	while (1)
	{
		PrintStatus(POS_GetPrnDeviceStatus(hLocalDevice,1,&globalStatus,&printerStatus));
		printf("Status drukarki - Drukarka: %ld, Mechanizm: %ld\n",globalStatus,printerStatus);

		int selection;
		printf("Wybierz opcj� i naci�nij ENTER\n");
		printf("(0) Zakoncz.\n");
		printf("(1) Raport dobowy\n");
		printf("(2) Konfiguracja stopki wydruku - kod kreskowy [FTRCFG]\n");
		printf("(3) Formatka 25 - Bon rabatowy\n");
		printf("(4) Transakcja opakowan\n");
		printf("(5) Faktura VAT\n");
		printf("(6) Faktura VAT + Osoby w fakturze\n");
		printf("(7) Sprzedaz prosta\n");
		printf("(8) Sprzedaz z rabatem\n");
		printf("(9) Sprzedaz z rabatem linii\n");
		printf("(10) Sprzedaz z platnoscia walutowa\n");
		printf("(11) Sprzedaz z platnoscia karta\n");
		printf("(12) Sprzedaz ze stopka (wyslij najpierw [FTRINFOSET] - 13)\n");
		printf("(13) Linie stopki [FTRINFOSET]\n");
		printf("(14) Sprzedaz z kodem kreskowym\n");
		printf("(15) Ustaw naglowek\n");
		printf("(16) Raport okresowy\n");
		printf("(17) Ustaw stawki VAT\n");
		printf("(18) Ustaw zegar\n");
		printf("(19) Formatka 32 - Potwierdzenie zaplaty karta\n");

		if (scanf("%d",&selection)<=0)
		{
			fflush(stdin);
			continue;
		}
		if (selection==0) break;

		switch (selection)
		{
		case 1: FunRaportDobowy(hDevice, hLocalDevice); break;
		case 2: FunUstawienieStopki(hDevice, hLocalDevice); break;
		case 3: FunForm25(hDevice, hLocalDevice); break;
		case 4: FunTransakcjaOpakowan(hDevice, hLocalDevice); break;
		case 5: FunFakturaVAT(hDevice, hLocalDevice); break;
		case 6: FunFakturaVATOsoby(hDevice, hLocalDevice); break;
		case 7: FunSprzedazProsta(hDevice, hLocalDevice); break;
		case 8: FunSprzedazRabat(hDevice, hLocalDevice); break;
		case 9: FunSprzedazRabatLinii(hDevice, hLocalDevice); break;
		case 10: FunSprzedazEURO(hDevice, hLocalDevice); break;
		case 11: FunSprzedazVISA(hDevice, hLocalDevice); break;
		case 12: FunSprzedazStopka(hDevice, hLocalDevice); break;
		case 13: FunLinieStopki(hDevice, hLocalDevice); break;
		case 14: FunUstawienieStopki(hDevice, hLocalDevice);FunSprzedazProsta(hDevice, hLocalDevice); break;
		case 15: FunNaglowek(hDevice, hLocalDevice); break;
		case 16: FunOkresowy(hDevice, hLocalDevice); break;
		case 17: FunUstawVAT(hDevice, hLocalDevice); break;
		case 18: FunUstawZegar(hDevice, hLocalDevice); break;
		case 19: FunForm32(hDevice, hLocalDevice); break;
		}

	}
	
	POS_CloseDevice(hLocalDevice);
	POS_DestroyDeviceHandle(hDevice);
	return 0;
}
