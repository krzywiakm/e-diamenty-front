/*
		COMMON SETUP
*/

unsigned long type=POSNET_INTERFACE_RS232;
	void *hDevice=NULL;
	string usbser="";
#ifdef WIN32
	string comparams="COM1,9600,8,N,1,H";
#else
	string comparams="/dev/ttyS0,9600,8,N,1,H";
#endif
	// obsługa linii poleceń 
	if (argc>2)
	{
		string s=argv[1];
		if (s=="USB")
		{
			usbser=argv[2];
			if (argc>3)
			comparams=argv[3];
			type=POSNET_INTERFACE_USB;
		}
		else
		if (s=="SERIAL")
		{
			comparams=argv[2];
			type=POSNET_INTERFACE_RS232;
		}
	}
	else
		if (argc>1)
		{
			string s=argv[1];
			if (s=="help")
			{
				cout << "Usage: demo_XXXXX [ SERIAL | USB ] [ com_settings | usb_serial com_settings] \n";
				cout << "Defaults to SERIAL. Default params : SERIAL: COM1,8,N,1,H  , USB '' " << endl;
				return 1;
			}
		}
	// utworzenie uchwytu globalnego urządzenia
	hDevice=POS_CreateDeviceHandle(type);
	if (!hDevice)
	{
		cout << "Could not create a device." << endl;
		return 2;
	}
	// ustawienie trybu logowania na maksymalny
	POS_SetDebugLevel(hDevice,POSNET_DEBUG_ALL & 0xFFFFFFFE);

	// ustawienie parametrów komunikacji w zależności od rodzaju urządzenia
	switch (type)
	{
	case POSNET_INTERFACE_RS232:
		POS_SetDeviceParam(hDevice,POSNET_DEV_PARAM_COMSETTINGS,(void*)comparams.c_str());
		break;
	case POSNET_INTERFACE_USB:
		{
			char buffer[1024];
			POSNET_STATUS stat;
			if (stat=POS_GetDeviceParam(hDevice,POSNET_DEV_PARAM_LISTUSBSERIALS,buffer)!=POSNET_STATUS_OK)
			{
				cout << "Error listing USB serial numbers: 0x" << hex << stat << dec << endl;
				return 5;
			}
			cout << "Devices: " << buffer << endl;
			POS_SetDeviceParam(hDevice,POSNET_DEV_PARAM_USBSERIAL,(void*)usbser.c_str());
			POS_SetDeviceParam(hDevice,POSNET_DEV_PARAM_COMSETTINGS,(void*)comparams.c_str());
		}
		break;
	}
	// otwarcie urządzenia
	void *hLocalDevice=POS_OpenDevice(hDevice);
	if (!hLocalDevice)
	{
		POSNET_STATUS code=POS_GetError(hDevice);
		char *c=(char*)POS_GetErrorString(code,"pl");
		cout << "Error Opening device: 0x" << hex << code << dec << " " ;
		if (c) cout << c;
		cout  <<endl;
		return 4;
	}
	
	
