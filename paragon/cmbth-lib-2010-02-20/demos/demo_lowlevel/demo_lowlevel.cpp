//
//
//
//			POSNET BASE EXAMPLES
//			EXAMPLE QUEUE MODE
//
//

#include <stdlib.h>
#include <string>
#include <iostream>
#include <istream>
#include <limits>
#ifdef WIN32
#include <windows.h>
#else
#define INFINITE 0xFFFFFFFF
#define Sleep(a) {struct timespec tc,tc1;\
                 tc.tv_sec=a/1000; \
		 tc.tv_nsec=(a%1000)*1000*1000; \
		 nanosleep(&tc,&tc1);   \
		 }
						    
#endif
#include "../../include/posnet.h"

using namespace std;



int main(int argc, char *argv[])
{

#ifdef WIN32
	WSADATA WSAData;
	if (WSAStartup (MAKEWORD(2,0), &WSAData) != 0) 
	{
		MessageBox (NULL, TEXT("WSAStartup failed!"), TEXT("Error"), MB_OK);
		return FALSE;
	}

 #endif
 
#include "../setup.inc"

	/*
		UWAGA: W tym przyk�adzie obs�ug� b��d�w zmiejszono do minimum
		dla czytelno�ci pozosta�ej cz�ci kodu - prosz� skorzysta� z 
		programu demo_all dla zapoznania si� z pe�niejsz� obs�ug� b��d�w.
	*/

	POSNET_HANDLE hRequest;
	int cancel=1;


	// ustawienie parametru - czas pomi�dzy poszczeg�lnymi automatycznymi zapytaniami o status drukarki
	unsigned long pi=5;
	POS_SetDeviceParam(hDevice,POSNET_DEV_PARAM_STATUSPOLLINGINTERVAL,&pi);
	
	// pobranie statusu drukarki
	long globalStatus,printerStatus;
	POS_GetPrnDeviceStatus(hLocalDevice,1,&globalStatus,&printerStatus);
	printf("Printer status D:%ld, P%ld\n",globalStatus,printerStatus);
	
	// drobne op�nienie - tylko dla cel�w demonstracynych
	Sleep(2000);

	// je�li drukarka nie gotowa, nie wykonujemy polecenia
	if (globalStatus!=0) 
	{
		cancel=0;
	}
	else
	// poni�sza p�tla jest trikiem umo�liwiaj�cym proste obs�ugiwanie b��d�w
	// za po�rednictwem instrukcji break - p�tla wykonuje si� zawsze raz
	do
	{
		// utworzenie obiektu rozkazowego dla rozkazu trinit (pocz�tek transakcji)
		hRequest = POS_CreateRequest(hLocalDevice,"trinit");
		// je�li nie powiod�o si� przerywamy dzia�anie
		if (!hRequest)
		{
				printf("POS_CreateRequest status: %d",POS_GetError(hLocalDevice));
				break;		
		}
		// dodanie paramtru "bm" o warto�ci "1" do obiektu rozkazowego
		POS_PushRequestParam(hRequest,"bm","1");
		// wys�anie polecenia w podstawowym trybie SPOOL
		POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL);
		// oczekiwanie na zako�czenie polecenia (5 sekund)
		POS_WaitForRequestCompleted(hRequest,5000);
		// sprawdzenie statusu rozkazu, przerwa w przypadku b��du
		if (POS_GetRequestStatus(hRequest) != POSNET_STATUS_OK) break;
		// likwidacja wykonanego ju� rozkazu
		POS_DestroyRequest(hRequest);
		// utworzenie obiektu rozkazowego reprezentuj�cego lini� paragonu
		// z automatycznym dodaniem niezb�dnych dla tej linii parametr�w
		hRequest = POS_CreateRequestEx(hLocalDevice,"trline","na,Bu�ka Standardowa\nvt,0\npr,35");
			
		if (!hRequest)
		{
				printf("POS_CreateRequestEx status: %d",POS_GetError(hLocalDevice));
				break;		
		}
		// analogicznie jak powy�ej - wyslanie i oczekiwanie na zako�czenie
		POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL);
		POS_WaitForRequestCompleted(hRequest,5000);
		if (POS_GetRequestStatus(hRequest) != POSNET_STATUS_OK) break;

		POS_DestroyRequest(hRequest);
		// utworzenie obiektu rozkazowego reprezentuj�cego p�atno�� got�wk�
		hRequest = POS_CreateRequestEx(hLocalDevice,"trpayment","ty,0\nre,0\nwa,35");
		if (!hRequest)
		{
				printf("POS_CreateRequestEx status: %d",POS_GetError(hLocalDevice));
				break;		
		}
		// analogicznie jak powy�ej - wyslanie i oczekiwanie na zako�czenie
		POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL);
		POS_WaitForRequestCompleted(hRequest,5000);
		if (POS_GetRequestStatus(hRequest) != POSNET_STATUS_OK) break;

		POS_DestroyRequest(hRequest);
		// utworzenie obiektu rozkazowego reprezentuj�cego zako�czenie transakcji wraz
		// z dodaniem niezb�dnych parametr�w
		hRequest = POS_CreateRequestEx(hLocalDevice,"trend","to,35\nfp,35");
		// analogicznie jak powy�ej - wyslanie i oczekiwanie na zako�czenie
		POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL);
		POS_WaitForRequestCompleted(hRequest,5000);
		if (POS_GetRequestStatus(hRequest) != POSNET_STATUS_OK) break;

		POS_DestroyRequest(hRequest);
		hRequest=NULL;
		// uniemo�liwienie wykonania si� sekcji obs�ugi b�ed�w
		cancel=0;
	}
	while (0);
	if (cancel)
	{
		// likwidacja b��dnie wykonanej sekwencji
		if (hRequest) POS_DestroyRequest(hRequest);
		// utworzenie obiektu rozkazowego reprezentuj�cego anulowanie bie��cej transakcji
		hRequest = POS_CreateRequest(hLocalDevice,"prncancel");
		// oraz wyslanie go do drukarki
		POS_PostRequest(hRequest,POSNET_REQMODE_SPOOL);
		POS_WaitForRequestCompleted(hRequest,5000);
		POS_DestroyRequest(hRequest);
	}
	// zamkni�cie uchwyt�w urz�dze�
	POS_CloseDevice(hLocalDevice);
	POS_DestroyDeviceHandle(hDevice);
	return 0;
}
