//
//
//
//			POSNET BASE EXAMPLES
//			EXAMPLE ASYNC EVENT
//
//

#include <stdlib.h>
#include <string>
#include <iostream>
#include <istream>
#include <limits>
#ifdef WIN32
#include <windows.h>
#else
#include <pthread.h>
#include <semaphore.h>
#endif
#include "../../include/posnet.h"

using namespace std;


int main(int argc, char *argv[])
{

#ifdef WIN32
	WSADATA WSAData;
	if (WSAStartup (MAKEWORD(2,0), &WSAData) != 0) 
	{
		MessageBox (NULL, TEXT("WSAStartup failed!"), TEXT("Error"), MB_OK);
		return FALSE;
	}

#endif  
  
#include "../setup.inc"

	// ustawienie trybu obs�ugi zdarze� asynchronicznych na event i pobranie Enevt-u lub semafora
#ifdef WIN32
	POS_SetEventHandlingMode(hLocalDevice,POSNET_EVENT_EVENT);
	HANDLE event=POS_GetEvent(hLocalDevice);
#else
	POS_SetEventHandlingMode(hLocalDevice,POSNET_EVENT_SEMAPHORE);
	sem_t *event=(sem_t*)POS_GetSemaphore(hLocalDevice);
#endif
	// ustawienie parametru - czas pomi�dzy poszczeg�lnymi automatycznymi zapytaniami o status drukarki
	unsigned long pi=5;
	POS_SetDeviceParam(hDevice,POSNET_DEV_PARAM_STATUSPOLLINGINTERVAL,&pi);

	// ta p�tla ko�czy si� po 3 minutach - w tym czasie 
	// mo�na sprawdzi� dzia�anie przyk�adu np. poprzez
	// wywo�anie funkcji z klawiatury drukarki (np. wej�cie do menu)
	// lub poprzez wywyo�anie zdarzenia typu brak papieru
	int j=180;
	while (j--) 
	{
		// oczekiwanie na stan aktywny zdarzenia (z limitem czasu 1s)
#ifdef WIN32		
		if (WaitForSingleObject(event,1000)==WAIT_OBJECT_0)
		{
			// je�li event aktywny, to kasujemy go
			ResetEvent(event);
#else
		if (sem_wait(event)==0)
		{
#endif
			// oraz pobieramy i wy�wietlamy status drukarki
				long globalStatus,printerStatus;
				POS_GetPrnDeviceStatus(hLocalDevice,1,&globalStatus,&printerStatus);
				cout << "Printer status D: " << globalStatus << ", P: " << printerStatus << endl;
		}
		else
		{
			// je�li event nieaktywny, to czekamy dalej
			cout << "Oczekuje ..... " << endl;
		}
	}
	// zamkni�cie uchwyt�w urz�dze�
	POS_CloseDevice(hLocalDevice);
	POS_DestroyDeviceHandle(hDevice);
	return 0;
}
