//
//
//
//			POSNET BASE EXAMPLES
//			EXAMPLE ASYNC EVENT
//
//

#include <stdlib.h>
#include <string>
#include <iostream>
#include <istream>
#include <limits>
#ifdef WIN32
#include <windows.h>
#else
#include <pthread.h>
#include <semaphore.h>
#define Sleep(a) {struct timespec tc,tc1;\
                 tc.tv_sec=a/1000; \
		 tc.tv_nsec=(a%1000)*1000*1000; \
		 nanosleep(&tc,&tc1);   \
		 }
						    
#endif
#include "../../include/posnet.h"

using namespace std;


POSNET_HANDLE hLocal;
int mode=-1;

// callback function
// funkcja wywolywana jest w kontekscie watku drukarki
// nie mozna wiec uzywac POS_WaitForRequestCompleted
// a funkcja musi konczyc sie szybko
// w tym przyk�adzie funkcja dekoduje i wy�wietla status drukarki
void myCallback(unsigned long status)
{
	long globalStatus,printerStatus;
	globalStatus=status >> 16;
	printerStatus=status & 0xFFFF;
	cout << "Printer status D: " << globalStatus << ", P: " << printerStatus << endl;
}

int main(int argc, char *argv[])
{

#ifdef WIN32
	WSADATA WSAData;
	if (WSAStartup (MAKEWORD(2,0), &WSAData) != 0) 
	{
		MessageBox (NULL, TEXT("WSAStartup failed!"), TEXT("Error"), MB_OK);
		return FALSE;
	}

#endif  
  
#include "../setup.inc"
	// ustawienie parametru - czas pomi�dzy poszczeg�lnymi automatycznymi zapytaniami o status drukarki
	unsigned long pi=5;
	POS_SetDeviceParam(hDevice,POSNET_DEV_PARAM_STATUSPOLLINGINTERVAL,&pi);

	// ustawienie trybu obs�ugi zdarze� asynchronicznych na callback i ustawienie funkcji callback
	hLocal=hLocalDevice;
	POS_SetEventHandlingMode(hLocalDevice,POSNET_EVENT_CALLBACK);
	POS_SetCallback (hLocalDevice,myCallback);

	int j=18000;
	// ta p�tla ko�czy si� po 3 minutach - w tym czasie 
	// mo�na sprawdzi� dzia�anie przyk�adu np. poprzez
	// wywo�anie funkcji z klawiatury drukarki (np. wej�cie do menu)
	// lub poprzez wywyo�anie zdarzenia typu brak papieru
	while (j--) 
	{
		// Tu robimy cos innego .....
		Sleep(10);		
	}
	// zamkni�cie uchwyt�w urz�dze�
	POS_CloseDevice(hLocalDevice);
	POS_DestroyDeviceHandle(hDevice);
	return 0;
}
