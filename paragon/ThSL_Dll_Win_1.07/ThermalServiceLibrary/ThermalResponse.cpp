/* Tab 2 */

/*
	ThermalResponse.cpp - implementacja klasy CThermalResponse
*/

#include "ThermalResponse.h"

extern DWORD ExtendedErrorCode;
extern tTIMEOUT_DLG_FUNC TimeoutDialogFunc;

/******************************************************************************/

CThermalResponse::CThermalResponse()
{
	CThermalResponse::Clear();

	ClearResponseStructPtr();
}

/*----------------------------------------------------------------------------*/

CThermalResponse::~CThermalResponse()
{
}

/*----------------------------------------------------------------------------*/

void CThermalResponse::Clear()
{
/*
	Inicjalizujemy troch� rzeczy 
*/
	ExtendedErrorCode = 0;

	FieldCount = 0;

	VectOfTextFields.clear();
	VectOfFieldTypes.clear();
	VectOfTextLengths.clear();
}

/******************************************************************************/

void CThermalResponse::ClearResponseStructPtr()
{
	ResponseStructPtr = NULL;
}

/*----------------------------------------------------------------------------*/

void CThermalResponse::SetResponseStructPtr( tRESPONSE * aResponseStructPtr )
{
	_ASSERTE( aResponseStructPtr );

	ResponseStructPtr = aResponseStructPtr;
}

/*----------------------------------------------------------------------------*/

tRESPONSE * CThermalResponse::GetResponseStructPtr()
{
	return ResponseStructPtr;
}

/******************************************************************************/

int CThermalResponse::Receive( BYTE aProtocolType, BOOL aRespWithData, DWORD aResponseTime )
{
	static int RetI_Recv, RetI_Flush;

	RetI_Recv = Receive2( aProtocolType, aRespWithData, aResponseTime );
	RetI_Flush = CThComPort.LogFlush();

	if( RetI_Recv != SOK )
	{
		return RetI_Recv;
	}

	return RetI_Flush;
}

/*----------------------------------------------------------------------------*/

int CThermalResponse::Receive2( BYTE aProtocolType, BOOL aRespWithData, DWORD aResponseTime )
{
	static int RetI,
		UserDecision;

	static DWORD BytesRead,		/* Liczba bajt�w odczytanych */
		BytesToRead,						/* Jakiej d�ugo�ci pakiet ma by� odebrany */
		InBufNdx,								/* Indeks bufora wej�ciowego */
		FirstTick,							/* Tick Time - do odmierzania czasu */
		FrameEndMarkNdx,				/* Indeks znacznika ko�ca ramki */
		TmpFrameEndMarkNdx,			/* tymczasowy FrameEndMarkNdx */
		RetChecksum,						/* Ramka zwrotna: suma kontrolna */
		I_Retries,							/* Liczba pr�b - protok� w�oski */
		TimeSpan,								/* */
		RetCode,								/* Kod zwrotny (ten przed kodem rozkazu ) */
		RetCodeStrLen;					/* D�ugo�� kodu zwrotnego */

	
	static BYTE Checksum;			/* Suma kontrolna ramki odebranej */

	static BOOL SearchForP,		/* Czy czekamy na ESC P, czy te� mo�e odbieramy reszt� */
		RespStatusPresent;			/* Czy znaleziono co� co wygl�da na status wykonania rozkazu */

	ProtocolType = aProtocolType;

	F_Resp =					( (aProtocolType & PROTO_RESP) != 0 );
	F_Italian =				( (aProtocolType & PROTO_ITALIAN) != 0 );
	F_ItalianRemote = ( ((aProtocolType & PROTO_ITALIAN) != 0) && ((aProtocolType & PROTO_REMOTE) != 0) );
	F_RxChksum =			( (aProtocolType & PROTO_RX_CHKSUM) != 0 );

#ifdef _DEBUG
	memset( CThComPort.PortInBuf, 0 ,PORT_INPUT_BUFFER_LEN );
#endif // _DEBUG

	/* na wszelki wypadek... */
	if( !CThComPort.WasPortOpened() )
	{
		return ERR_PORT_NOT_OPEN;
	}

DLE_LOOP:
TOUT_LOOP:
	I_Retries = I_MAX_RETRIES ;		/* Max liczba pr�b dla protoko�u w�oskiego */

	/* czekamy na odpowied� - na pocz�tek jeden bajt, a po jego warto�ci okre�limy co dalej */
	if( F_Resp )
	{

/*
		Fuj ! Etykieta !  Kto u�ywa 'goto'... ;-)
*/
LOOP:

/*
		Dla protoko�u w�oskiego wprowadzi�em maksymaln� liczb� pr�b (gdy� tam w przypadku
		b��du nast�puje wys�anie negatywnego potwierdzenia i ponowne oczekiwanie na odebranie
		ramki)

		Uwaga - sprawdzanie wykonywane po wys�aniu ale przed odbiorem.
		
		Nie dotyczy trybu Remote PC
*/
		if( F_Italian && (!F_ItalianRemote) )
		{
			if( !I_Retries )
			{
				return ERR_I_MAX_RETRIES;
			}
			else
			{
				I_Retries--;
			}
		}

		SearchForP = TRUE;			/* B�dziemy czeka� na ESC P */

/*
		Timeout b�dzie wykrywany w�a�nie tutaj - przy czekaniu na pierwszy bajt
*/
		RetI = CThComPort.WaitForAnyByte( aResponseTime, CThComPort.PortInBuf );
		if( RetI != SOK )
		{
/*
			Spytamy u�ytkownika o to co s�dzi o tajma�cie ;-)
			Yes - czekanie dalej na odpowied�
			No - wys�anie ponowne
			Cancel - przerwanie
*/
			UserDecision = TimeoutDialogFunc();

			if( UserDecision == IDYES )
			{
				goto TOUT_LOOP;							/* IDYES -> no to jeszcze poczekamy */
			}
			else if( UserDecision == IDNO )
			{
				return ERR_RESEND;					/* IDNO -> wysy�amy ponownie */
			}
			else
			{
				return ERR_NO_RESPONSE;			/* cokolwiek innego -> wychodzimy z kodem b��du */
			}
		}

/*
		Je�li kto� w TimeoutDialogFunc() odpali� DLE i wybra� dalsze czekanie (IDYES)
		i przypadkiem odpowied� na DLE przysz�a dopiero teraz - trzeba j� pomin��.
		Je�li bajt nie pasuje do odpowiedzi na DLE - to przepu�cimy go do dalszego
		sprawdzania.
*/
		if( ! F_Italian )
		{
			if( (CThComPort.PortInBuf[0] & DLE_RESP_MASK) == DLE_RESP_SIGNATURE )	/* czy bajt odpowiedzi na DLE ? */
			{
				goto DLE_LOOP;	/* je�li odpowied� na DLE - to wracamy do pocz�tku */
			}
		}
		else if( F_Italian && (!F_ItalianRemote) )
		{
			if( (CThComPort.PortInBuf[0] & I_DLE_RESP_MASK) == I_DLE_RESP_SIGNATURE )
			{
				goto DLE_LOOP;
			}
		}


		if( ! F_ItalianRemote )	/* pobieramy warto�� timera systemowego (ITALIAN_REMOTE - nie) */
		{
			FirstTick = GetTickCount();
		}

/*
		Je�li pierwszym znakiem jest ESC, drugim P oznacza to, �e otrzymamy w odpowiedzi d�u�sz� ramk� -
		zako�czon� ESC \
		Wyszukiwanie znacznika ko�ca jest jedyn� skuteczn� metod� okre�lenia d�ugo�ci ramki.
*/

		InBufNdx = 1;
		
		if( CThComPort.PortInBuf[0] == ESC )
		{
			while( TRUE )
			{
				BytesToRead = min( READ_N_BYTES_AT_ONCE, PORT_INPUT_BUFFER_LEN - InBufNdx ); 	

				/* Odczyt paczkami po ile� tam (READ_N_BYTES_AT_ONCE) bajt�w */
				
				RetI = CThComPort.Read( BytesToRead, & BytesRead, & CThComPort.PortInBuf[InBufNdx] );
/*
				B��d mo�e by� np. spowodowany TimeOut-em, wi�c najpierw trzeba b�dzie sprawdzi� co 
				zosta�o odebrane
*/
				if( !F_ItalianRemote )		/* ITALIAN_REMOTE - nie sprawdzamy */
				{
					TimeSpan = GetTickCount() - FirstTick;

					if( TimeSpan > MAX_RESPONSE_FRAME_TIME )
					{
						if( F_Italian )	
						{
							Send_NACK();
							goto LOOP;	
						}
						else
						{
							return ERR_RESPONSE_TOUT;
						}
					}
				}

				if( !BytesRead )
				{
					continue;
				}
				else
				{
					InBufNdx += BytesRead;								/* wskazujemy na pierwszy bajt za tymi ju� odebranymi */

					if( SearchForP )											/* czy szukamy P wyst�puj�cego po ESC ? */
					{
						if( CThComPort.PortInBuf[1] != 'P' )
						{
							if( F_Italian )
							{
								Send_NACK();
								goto LOOP;	
							}
							else
							{
								return ERR_FMT_HEADER;
							}
						}
						else
						{
							SearchForP = FALSE;
						}
					}
/*
					Skoro w�a�nie znaleziono nag��wek ramki to znacznik ko�ca musi by� gdzie� dalej
					 czyli je�li odczytano zbyt ma�o bajt�w to nie ma sensu szuka�...
					Je�li natomiast nie zostanie znaleziony znacznik ko�ca to b�d� doczytywane kolejne bajty
					 i dalej b�dzie szukany ten znacznik - a� do skutku (albo do Timeout-u)
*/
					if( InBufNdx > 4 )		/* b�dzie chyba co� wi�cej ni� tylko znaczniki pocz�tku i ko�ca ? */
					{
						if( !FindFrameEnd(& CThComPort.PortInBuf[2], InBufNdx - 2, & FrameEndMarkNdx) )
						{
							if( InBufNdx == PORT_INPUT_BUFFER_LEN )
							{
								if( F_Italian )
								{
									Send_NACK();
									goto LOOP;	
								}
								else
								{
									return ERR_INPUT_BUF_OVRRUN;
								}
							}
							else
							{
								continue;
							}
						}
						else
						{
/*
							Ciekawe rzeczy si� dziej� w polskim protokole. Jak si� w��czy�o automatyczne odsy�anie
							odpowiedzi z kodem b��du - to je�li rozkaz odsy�a jak�� tam odpowied�, dodana na koniec zostanie
							ramka z kodem b��du (zazwyczaj r�wnym zero ?)
							No to musz� troch� inaczej wyszukiwa� koniec ramki - �eby znale�� koniec tej w�a�ciwej, szukaj�c
							od ko�ca
*/
							if( (!F_Italian) && (F_Resp) && (aRespWithData) )
							{
								BytesRead = 1;
/*
								Doczytujemy ewentualne resztki z ramki potwierdzenia, kt�ra zosta�a wys�ana zaraz za ramk�
								odpowiedzi.
*/
								while( BytesRead > 0 )
								{
									BytesToRead = min( READ_N_BYTES_AT_ONCE, PORT_INPUT_BUFFER_LEN - InBufNdx ); 	
									RetI = CThComPort.Read( BytesToRead, & BytesRead, & CThComPort.PortInBuf[InBufNdx] );
									InBufNdx += BytesRead;

									if( FindFrameEnd(& CThComPort.PortInBuf[2], InBufNdx - 2, & FrameEndMarkNdx) )
									{
										break;
									}
								}

								if( FrameEndMarkNdx > 8 )			/* Tyle ma najkr�tsza ramka zwrotna */
								{
/*
									Do FindFrameEnd przeka�emy liczb� bajt�w zmniejszon� o 2 - czyli o znacznik ko�ca.
									Wtedy b�dzie usi�owa� znale�� taki znacznik gdzie� wcze�niej.
*/									
									if( FindFrameEnd(& CThComPort.PortInBuf[2], InBufNdx - 4, & TmpFrameEndMarkNdx) )
									{
										FrameEndMarkNdx = TmpFrameEndMarkNdx;
									}
								}
							}

							FrameEndMarkNdx += 3;							/* tak b�dzie licz�c od pocz�tku bufora */
							break;
						}
					}
				}
			}
		}
		else
		{
			if( F_Italian )
			{
				Send_NACK();
				goto LOOP;	
			}
			else
			{
				return ERR_FMT_HEADER;
			}
		}

/*
		W�oski protok� przewiduje, �e ramki odpowiedzi zawsze maj� sum� kontroln�. Prawid�owo��
		sumy potwierdzana jest przez odes�anie znaku ACK. Odes�anie NACK spowoduje powt�rzenie
		odpowiedzi.

		W starym protokole jest inaczej - suma kontrolna nie zawsze jest obecna. I nawet jeden rozkaz
		mo�e w zale�no�ci od parametr�w zwaraca� sum� kontroln� a raz nie. 
*/
		if( F_RxChksum )
		{
			/* Wpisujemy 0 w pierwszy bajt znacznika ko�ca - aby sscanf mia� podany ci�g */
			CThComPort.PortInBuf[FrameEndMarkNdx] = 0;

			if( sscanf((char *) & CThComPort.PortInBuf[FrameEndMarkNdx-2], "%x", & RetChecksum) != 1 )
			{
				if( F_Italian )
				{
					Send_NACK();
					goto LOOP;	
				}
				else
				{
					return ERR_FMT_CHECKSUM;
				}
			}

			if( GenerateChecksum(& CThComPort.PortInBuf[2], FrameEndMarkNdx - 4) != (BYTE) RetChecksum )
			{
				if( F_Italian )
				{
					Send_NACK();
					goto LOOP;	
				}
				else
				{
					return ERR_CHECKSUM;
				}
			}

			if( !F_Italian)		/* poniewa� w polskim protokole suma kontrolna nie jest oddzielona od ostatniego */
			{									/* pola - sami j� oddzielamy, zapisuj�c pierwszy znak sumy znakiem ESC */
				CThComPort.PortInBuf[FrameEndMarkNdx-2] = ESC;
/*
				Je�li ostatnie pole ma porz�dny terminator - to FrameEndMarkNdx zmniejszamy o 2, �eby
				ESC nie zosta�o zaliczone jako terminator pustego pola.
				Je�li natomiast nie ma terminatora - to zmniejszamy o 1.
*/
				switch( CThComPort.PortInBuf[FrameEndMarkNdx-3] )
				{
					default:
						FrameEndMarkNdx--;
						break;

					case SEP_BYT:
					case SEP_NUM:
					case SEP_STR:
					case SEP_LIN:
						FrameEndMarkNdx -= 2;
						break;
						
				}
			}
			
		}

/* 
		Sprawdzimy czy w ramce zwrotnej zasygnalizowany jest b��d.
		Je�li nie - i je�li oczekiwana jest odpowied� z polami informacyjnymi - to
		przejdziemy do analizy p�l.

		(Znowu format ramki zwrotnej jest inny dla obu wersji protoko��w...)
*/
		if( F_Italian )
		{
			RespStatusPresent = ( sscanf((const char *) & CThComPort.PortInBuf[2],
				"#Z%c%c%d", & RetCmdString[0], & RetCmdString[1], & RetCode ) == 3 );
		}
		else
		{
			RespStatusPresent = ( sscanf((const char *) & CThComPort.PortInBuf[2],
				"%d#Z%c%c", & RetCode, & RetCmdString[0], & RetCmdString[1] ) == 3 );
		}

/*
		Czy ramka odpowiedzi pasuje do wzorca ramki potwierdzenia wykonania rozkazu ?
*/
		if( RespStatusPresent )
		{
			if( !RetCode )
			{
				if( F_Italian )
				{
					return SOK;										/* Rozkaz wykonany bez b��d�w - wychodzimy */
				}
				else
				{
					if( aRespWithData )						/* W wersji polskiej - kod b��du == 0 r�wnie� dla */
					{															/* nierozpoznanych rozkaz�w, wi�c skoro oczekiwali�my */
						return ERR_UNKNOWN_CMD;			/* innej odpowiedzi - to najwyra�niej nie rozpoznano rozkazu */
					}
					else
					{
						return SOK;
					}
				}
			}
			else
			{
																			/* B��d  wykonania - zapami�tujemy i wychodzimy */
				ExtendedErrorCode = MAKELONG( (WORD) RetCode , MAKEWORD(RetCmdString[1], RetCmdString[0]) );	
				return ERR_RETURN_CODE;
			}
		}
		else
		{
/*
			Wygl�da na to, �e ramka nie zawiera potwierdzenia - to mo�e s� w niej
			pola danych - ale czy oczekujemy takiej ramki ?
*/
			if( !aRespWithData )
			{
				if( F_Italian )
				{																					/* Nie oczekiwali�my ramki z danymi  */
																									/* wi�c musi to by� jaka� b��dna ramka */
					Send_NACK();														/* A we w�oskim protokole - odsy�amy */
					goto LOOP;															/* Negatywne Potwierdzenie (NACK) */	
				}
				else
				{
					return ERR_UNKNOWN_FRAME;								/* Stary protok� - wychodzimy z kodem b��du */
				}
			}
		}

/*
		Uff... Skoro tu doszli�my to chyba tylko dlatego, �e w ramce zwrotnej odes�ano jakie�
		informacje (tzn. nie by�a to ramka potwierdzaj�ca wykonanie rozkazu).
*/
		_ASSERTE( aRespWithData );

		if( aRespWithData )
		{
			DataPtr = CThComPort.PortInBuf;
			DataLen = FrameEndMarkNdx;

			if( F_Italian )
			{
/*
				SetReturnCode nie dotyczy protoko�u w�oskiego !!!
*/
				SetCmdString( DataPtr[2], DataPtr[3] );
				DataPtr += 4;			/* W�oski: ESC P i dwa znaki kodu rozkazu a potem dopiero dane */
				DataLen -= 6;			/* tu -6 bo 4 z powy�szego i jeszcze 2 sumy kontrolnej */
			}
			else
			{

/*
				Kod zwrotny w wi�kszo�ci ramek to tylko wype�niacz - jednobajtowy (0x30).
				Ale nie zawsze ! Dlatego te� reszta ramki si� mo�e przesuwa�.
*/
				if( isdigit(DataPtr[4]) )		/* Troche naiwne sprawdzenie - ale i tak */
				{														/* w przypadku bledu na wy�szym poziomie zostanie */
					RetCodeStrLen = 3;				/* wykryta jakas nieprawidlowosc */
				}
				else if( isdigit(DataPtr[3]) )
				{
					RetCodeStrLen = 2;
				}
				else
				{
					RetCodeStrLen = 1;
				}

				if( sscanf((char *) DataPtr + 2, "%d", & RetCode) != 1 )
				{
					return ERR_FMT_RETURN_CODE;
				}

				SetReturnCode( RetCode );

				SetCmdString( DataPtr[2 + RetCodeStrLen], DataPtr[3 + RetCodeStrLen] );
				DataPtr += (4 + RetCodeStrLen);
/*
				Czy jest suma kontrolna czy nie ma - to teraz nie ma ju� r�nicy je�li chodzi
				o d�ugo�� pola danych - je�li by�a suma, to FrameEndMarkNdx wcze�niej by� ju�
				zmodyfikowany - tak, jakby przesuni�ty zosta� znacznik ko�ca ramki
*/
				DataLen -= (4 + RetCodeStrLen);	

			}

			RetI = Translate( DataPtr, DataLen );
			if( RetI != SOK )
			{
				return RetI;
			}
		}
	}

	return SOK;
}

/******************************************************************************/

void CThermalResponse::SetCmdString( char aFirstChar, char aSecondChar )
{
	RetCmdString[0] = aFirstChar;
	RetCmdString[1] = aSecondChar;
	RetCmdString[2] = 0;
}

/*----------------------------------------------------------------------------*/

char * CThermalResponse::GetCmdString()
{
	return RetCmdString;
}

/******************************************************************************/

void CThermalResponse::SetReturnCode( DWORD aCode )
{
	RetCode = aCode;
}

/*----------------------------------------------------------------------------*/

DWORD CThermalResponse::GetReturnCode()
{
	return RetCode;
}

/******************************************************************************/

int CThermalResponse::Translate( BYTE *aDataPtr, DWORD aDataLen )
{
	static char	Separators[6];	/* Separatory - do analizy odpowiedzi (4 + ko�cz�ce zero */
															/* lub 5 + ko�cz�ce zero) */

	static BYTE	FieldSeparator,	/* Znaleziony separator - do okre�lenia rodzaju pola */
		FieldType;								/* Rodzaj pola - na podstawie separatora */

	static DWORD FieldOffsNdx,
		StrLen;

	if( F_Italian )
	{
		Separators[0] = SEP_BYT_IT;
		Separators[1] = SEP_NUM_IT;
		Separators[2] = SEP_STR_IT;
		Separators[3] = SEP_LIN_IT;
		Separators[4] = 0;
	}
	else
	{
		Separators[0] = SEP_BYT;
		Separators[1] = SEP_NUM;
		Separators[2] = SEP_STR;
		Separators[3] = SEP_LIN;
		Separators[4] = SEP_ESC;	/* to jest ESC - dorabiane "r�cznie" */
	}

	Separators[5] = 0;		/* strcspn wymaga ci�gu separator�w - zako�czonego zerem */

	Clear();

	FieldOffsNdx = 0;

/*
	Dop�ki mamy jeszcze dane do obrobienia...
*/
	while( FieldOffsNdx < aDataLen )
	{
/*
		Znajdujemy pole zako�czone jakim� ze znak�w separatora
*/
		StrLen = strcspn( (char *) & aDataPtr[FieldOffsNdx], Separators );

/*
		Bierzemy sepratator i na jego podstawie okre�lamy rodzaj pola.
		W miejsce separatora wpisujemy zero, aby p�niej sscanf mia� u�atwione zadanie.

		W wersji polskiej separatory nie odpowiadaj� rodzajom p�l - dlatego niespecjalnie si� nimi
		przejmujemy (b�d� sprawdzane w ograniczonym zakresie).
*/
		FieldSeparator = aDataPtr[FieldOffsNdx + StrLen];
		aDataPtr[FieldOffsNdx + StrLen] = 0;

		if( F_Italian )
		{
			switch( FieldSeparator )
			{
				default:									/* Jaki� dziwny separator - zg�osimy b��d */
					return ERR_FIELD_TYPE;	/* NO BREAK HERE !!! */

				case SEP_BYT_IT:
					FieldType = FLD_BYT;
					break;

				case SEP_NUM_IT:
					FieldType = FLD_NUM;
					break;

				case SEP_STR_IT:
					FieldType = FLD_STR;
					break;

				case SEP_LIN_IT:
					FieldType = FLD_LIN;
					break;
			}
		}
		else
		{
			switch( FieldSeparator )
			{
				default:									/* Jaki� dziwny separator - zg�osimy b��d */
					return ERR_FIELD_TYPE;	/* NO BREAK HERE !!! */

				case SEP_ESC:
					FieldType = FLD_UNK;		/* ostatnie pole - nie wiadomo jakiego typu ... */
					break;

				case SEP_BYT:
					FieldType = FLD_BYT;
					break;

				case SEP_NUM:
					FieldType = FLD_NUM;
					break;

				case SEP_STR:
					FieldType = FLD_STR;
					break;

				case SEP_LIN:
					FieldType = FLD_LIN;
					break;
			}
		}

/*
		Zapami�tujemy znalezione pola (teksty) w tablicach dynamicznych (vector...) - 
		z rodzajem pola i jego d�ugo�ci�.
*/
		AddField( (char *) & aDataPtr[FieldOffsNdx], FieldType, StrLen );

/*
		Na potrzeby zgodno�ci ze star� bibliotek� (PosnetDf.dll) odtwarzam separator,
		tak, aby mo�na zwr�ci� "surowe" pole danych.
*/
		aDataPtr[FieldOffsNdx + StrLen] = FieldSeparator;

		FieldOffsNdx += ( StrLen + 1 );
	}
		
	return SOK;
}

/******************************************************************************/

int CThermalResponse::CreateRespStruct( BYTE	aProtocolType, tRESPONSE *aResponseStruct )
{
	static BYTE FieldType,		/* Rodzaj pola */
		TrueFieldType,
		TmpFieldType,
		*RespFieldsPtr;

	BYTE *DescrPtr;

	static DWORD FieldNr,
		TmpNum,
		DescrLen,
		i;

	FieldNr = 0;

	DescrLen = aResponseStruct->DescriptionLen;
	DescrPtr = aResponseStruct->Description;
	RespFieldsPtr = aResponseStruct->FieldsStruct;

/*
	Czy liczba p�l w ramce odebranej odpowiada liczbie p�l dostarczonego opisu ?
*/
	if( DescrLen != GetFieldCount() )
	{
		return ERR_NUMBER_OF_FIELDS;
	}

/*
	Przegl�damy list� p�l i na podstawie ich typ�w generujemy struktur� komunikatu (- odpowiedzi
	lub rozkazu).
*/
	for( i = 0; i < DescrLen; i++ )
	{
/*
		Sprawdzimy czy rodzaje p�l wzi�tych z ramki zgodne s� z dostarczonym opisem.

		Uwaga (0) - [do obu wersji] na podstawie terminatora nie ma mo�liwo�ci rozr�nienia czy jest to
								FLD_NUM czy FLD_NUM_EXT (bo to jest wewn�trzne rozr�nienie).
		Uwaga (1) -	[werja w�oska] - �adnie i pi�knie, porz�dek... ;-)
		Uwaga (2) -	[wersja polska] tutaj panuje zbyt du�y ba�agan; w�a�ciwie tylko pola bajtowe
								i to jeszcze nie wszystkie (bo ostatnie nie) posiadaj� porz�dne separatory (terminatory ?).
								Sprawdzanie b�dzie dotyczy� przede wszystkim p�l bajtowych, pozosta�e - czy separator
								jest taki jak dla pola numerycznego (slash /).
								W tablicy opisuj�cej pola mo�na wymusi� inny terminator. 
*/
		FieldType = DescrPtr[i] & FLD_TYPE_MASK;	/* rodzaj pola - z tablicy opisuj�cej */

		if( aProtocolType & PROTO_ITALIAN )				/* w�oski */
		{
			if( FieldType !=  GetFieldType(i) )
			{
				if( (GetFieldType(i) != FLD_NUM) && (FieldType != FLD_NUM_EXT) )
				{
					return ERR_FIELD_TYPE;
				}
			}
		}
		else																		/* polski ... */
		{
			TrueFieldType = GetTrueFieldType( DescrPtr[i] );
			TmpFieldType = GetFieldType(i);

/*
			Sprawdza� zgodno�� mo�na tylko wtedy, gdy jaki� terminator jest - a niekt�re z ostatnich p�l
			nie maj� (FLD_UNK)...
*/
			if( (TmpFieldType != FLD_UNK) && (TrueFieldType !=  TmpFieldType) )
			{
				if( (TmpFieldType != FLD_NUM) && (TrueFieldType != FLD_NUM_EXT) )
				{
					return ERR_FIELD_TYPE;
				}
			}
		}

/*
		FieldType - rodzaj pola wzi�ty z opisu rozkazu a nie z ramki - ze wzgl�du na ba�agan w wersji
		polskiej, a w wersji w�oskiej FLD_NUM i FLD_NUM_EXT (to rozr�nienie wprowadzi�em sam)
*/
		switch( FieldType )		/* NO DEFAULT !!! - NOT REQUIRED */
		{
			case FLD_BYT:				/* sprawdzimy czy rzeczywi�cie jest to bajt */
				if( (sscanf(GetFieldText(i), "%d", & TmpNum) != 1) || (TmpNum > 256) )
				{
					return ERR_FIELD_CONTENTS;
				}

				*RespFieldsPtr = (BYTE) TmpNum;
				RespFieldsPtr += sizeof( BYTE );
				break;

			case FLD_NUM:				/* DWORD w odr�nieniu od FLD_NUM_EXT, kt�ry jest stringiem */	
				if( sscanf(GetFieldText(i), "%d", & TmpNum) != 1 )
				{
					return ERR_FIELD_CONTENTS;
				}

				*( (DWORD *) RespFieldsPtr ) = TmpNum;
				RespFieldsPtr += sizeof( DWORD );
				break;

/*
			Pozosta�e rodzaje p�l - stringi
*/	
			case FLD_NUM_EXT:		/* NO BREAK HERE !!! */
			case FLD_STR:				/* NO BREAK HERE !!! */
			case FLD_LIN:
				*( (char **) RespFieldsPtr ) = GetFieldText(i);
				RespFieldsPtr += sizeof( char * );
				break;
		}
	}
		
	return SOK;
}

/******************************************************************************/

DWORD CThermalResponse::GetExtendedErrorCode()
{
	return ExtendedErrorCode;
}

/******************************************************************************/

void CThermalResponse::GetDataFieldDescr( DWORD *aLenOut, BYTE **aPtrOut )
{
	*aLenOut = DataLen;
	*aPtrOut = DataPtr;
}

/******************************************************************************/

void CThermalResponse::AddField( char *aStrPtr, BYTE aFieldType, DWORD aStrLen )
{
	VectOfTextFields.push_back( aStrPtr );
	VectOfFieldTypes.push_back( aFieldType );
	VectOfTextLengths.push_back( aStrLen );
	FieldCount++;
}

/*----------------------------------------------------------------------------*/

DWORD CThermalResponse::GetFieldCount()
{
	return FieldCount;
} 

/*----------------------------------------------------------------------------*/

BYTE CThermalResponse::GetFieldType( DWORD aFieldNdx )
{
	_ASSERTE( aFieldNdx < VectOfFieldTypes.size() );

	return VectOfFieldTypes[aFieldNdx];
}

/*----------------------------------------------------------------------------*/

DWORD CThermalResponse::GetFieldLength( DWORD aFieldNdx )
{
	_ASSERTE( aFieldNdx < VectOfTextLengths.size() );

	return VectOfTextLengths[aFieldNdx];
}

/*----------------------------------------------------------------------------*/

char * CThermalResponse::GetFieldText( DWORD aFieldNdx )
{
	_ASSERTE( aFieldNdx < VectOfTextFields.size() );

	return (char *) VectOfTextFields[aFieldNdx].c_str();
}

/*----------------------------------------------------------------------------*/

void CThermalResponse::SetFieldText( DWORD aFieldNdx, char * aFieldText )
{
	_ASSERTE( aFieldNdx < VectOfTextFields.size() );
	_ASSERTE( aFieldText );

	VectOfTextFields[aFieldNdx] = aFieldText;
	VectOfTextLengths[aFieldNdx] = strlen( aFieldText );
}
