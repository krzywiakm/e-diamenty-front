/* Tab 2 */

/*
	ComPort.cpp - implementacja klasy CComPort
*/

#include "ComPort.h"

/******************************************************************************/

CComPort::CComPort()	/* konstruktor */
{
/*
	Tylko inicjalizacja zmiennych. Allokacja jest w Open(...)
*/	
	hCom = NULL;

	PortOutBuf = NULL;
	PortInBuf = NULL;

	LoggingState = LOGGING_INACTIVE;
}

/*----------------------------------------------------------------------------*/

CComPort::~CComPort()	/* destruktor */
{
	if( hCom )	/* je�li wcze�niej nie zamkni�to portu to go zamkniemy teraz */
	{
		Close();
	}


	/* zamykamy MailSlot-a, je�li by� utworzony */
	if( hMailSlut )
	{
		CloseHandle( hMailSlut );	
	}
}

/******************************************************************************/

int CComPort::Open( char *aPortNamePtr, DWORD aBaudRate )
{
	static BOOL RetB;
	static COMMPROP CommProp;

	if( !aPortNamePtr )
	{
		return SERROR;
	}

/*
	Transmisja synchroniczna (czyli z czekaniem na zako�czenie operacji zapisu czy te� odczytu)
*/
	hCom = CreateFile( aPortNamePtr, GENERIC_READ | GENERIC_WRITE, 0, NULL,
				OPEN_EXISTING, 0, NULL);	

	if( hCom == INVALID_HANDLE_VALUE )		/* czy uda�o si� otworzy� port ? */
	{
		hCom = NULL;
		return ERR_CREATE_FILE_COM;
	}

	Dcb.DCBlength					= sizeof( DCB );
	Dcb.BaudRate					= aBaudRate;		/* Baud Rate */
	Dcb.fBinary						= TRUE;
	Dcb.fParity						= FALSE;
	Dcb.fOutxCtsFlow			= FALSE;
	Dcb.fOutxDsrFlow			= FALSE;
	Dcb.fDtrControl				= DTR_CONTROL_DISABLE;
	Dcb.fDsrSensitivity		= FALSE;
	Dcb.fTXContinueOnXoff	= FALSE;
	Dcb.fOutX							= TRUE;					/* Xon/Xoff u�ywane tylko podczas nadawania */
	Dcb.fInX							= FALSE;			
	Dcb.fErrorChar				= FALSE;
	Dcb.fNull							= FALSE;
	Dcb.fRtsControl				= RTS_CONTROL_DISABLE;
	Dcb.fAbortOnError			= FALSE;				/* w przypadku wyst�pienia b��du kolejne dost�py nie b�d� blokowane */
	Dcb.XonLim						= 0;				
	Dcb.XoffLim						= 0;				
	Dcb.ByteSize					= 8;						/* 8 bit�w */
	Dcb.Parity						= NOPARITY;			/* bez bitu kontroli parzysto�ci */
	Dcb.StopBits					= ONESTOPBIT;		/* jeden bit stopu */
	Dcb.XonChar						= XON;
	Dcb.XoffChar					= XOFF;

	if( !SetCommState(hCom, & Dcb) )
	{
		CleanUp();
		return ERR_COMM_SETTINGS;
	}

/*
	Timeout na:
	- ca�o�� ( RD_INTERV_TOUT * (1 + nr_of_bytes) )
	- interwa� mi�dzybajtowy
*/

	Timeouts.ReadIntervalTimeout					= RD_INTERV_TOUT;
	Timeouts.ReadTotalTimeoutConstant			= RD_INTERV_TOUT;
	Timeouts.ReadTotalTimeoutMultiplier		= RD_INTERV_TOUT;
	Timeouts.WriteTotalTimeoutConstant		= 0;
	Timeouts.WriteTotalTimeoutMultiplier	= 0;

	if( !SetCommTimeouts( hCom, & Timeouts ) )
	{
		CleanUp();
		return ERR_COMM_SETTINGS;
	}

/*
	�adne zdarzenia zwi�zane z portem nie b�d� generowane !!!
*/
	if( !SetCommMask( hCom, EV_RXCHAR ) )
	{
		CleanUp();
		return ERR_COMM_SETTINGS;
	}


/*
	Tak na dobry pocz�tek - czy�cimy co si� da (kolejki...)
*/
	RetB = PurgeComm( hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_RXCLEAR | PURGE_TXCLEAR );
	_ASSERTE( RetB );


/*
	Ciekawe jak cz�sto b�dzie brakowa� pami�ci ?
*/
	PortInBuf = (BYTE *) calloc( PORT_INPUT_BUFFER_LEN, 1 );
	if( !PortInBuf )
	{
		CleanUp();
		return ERR_ALLOC;
	}

	PortOutBuf = (BYTE *) calloc( PORT_OUTPUT_BUFFER_LEN, 1 );
	if( !PortOutBuf )
	{
		CleanUp();
		return ERR_ALLOC;
	}

/*
	Ustawiamy d�ugo�ci kolejek: wej�ciowej i wyj�ciowej.
	Wej�ciowa - postaramy si� ustawi� jak�� sensown� warto��.
	Wyj�ciowa - zero (brak kolejkowania danych wychodz�cych).
*/
	if( !GetCommProperties(hCom, & CommProp) )
	{
		CleanUp();
		return ERR_COMM_SETTINGS;
	}
	else
	{
		DWORD RxQueueLen;

		if( !CommProp.dwMaxRxQueue )	/* 0 oznacza, �e nie ma ograniczenia rozmiaru kolejki wej. */
		{
			RxQueueLen = PORT_RX_QUEUE_LEN;
		}
		else
		{
			RxQueueLen = min( PORT_RX_QUEUE_LEN, CommProp.dwMaxRxQueue );
		}

		if( !SetupComm(hCom, RxQueueLen, 0) )
		{
			CleanUp();
			return ERR_COMM_SETTINGS;
		}
	}


/*
	W przypadku BUILD-u ze zdefiniowanym _PORTLOG, przy otwarciu portu rozpoczynane b�dzie logowanies
*/

#ifdef _PORTLOG
	LoggingStart( "TxRx.log" );
#endif // _PORTLOG
	
	return SOK;
}

/*----------------------------------------------------------------------------*/

int CComPort::SetBaudRate( DWORD aRate )
{
	static BOOL RetB;

	if( !hCom )
	{
		return SERROR;
	}
	
	Dcb.BaudRate = aRate;
	RetB = SetCommState( hCom, & Dcb );		/* zmieniamy jedno pole struktury DCB  - BaudRate */
	if( RetB )
	{
		return SOK;
	}
	else
	{
		return SERROR;
	}
}

/*----------------------------------------------------------------------------*/

int CComPort::Write( DWORD aNrOfBytes, BYTE *aDataPtr )
{
	static BOOL RetB;
	static DWORD BytesWritten;

	if( (hCom == NULL) || (aDataPtr == NULL) )
	{
		return SERROR;
	}


	RetB = WriteFile( hCom, aDataPtr, aNrOfBytes, & BytesWritten, NULL );	/* wysy�amy */
	if( (!RetB) || (BytesWritten != aNrOfBytes) )
	{
		return ERR_WRITE_FILE_COM;
	}

	if( LoggingState == LOGGING_ACTIVE )
	{
		return LogUpdate( LOG_WRITE, BytesWritten, aDataPtr );	/* i ewentualnie dopisujemy do logu */
	}

	return SOK;
}

/*----------------------------------------------------------------------------*/

int CComPort::Read( DWORD aNrOfBytes, DWORD *aNrOfBytesReadPtr, BYTE *aDataPtr )
{
	static DWORD NrOfBytesRead;

	if( (hCom == NULL) || (aNrOfBytesReadPtr == NULL) || (aDataPtr == NULL) )
	{
		return SERROR;
	}


/*
	Mo�e si� zdarzy�, �e odczytane zostanie mniej bajt�w ni� ��dano ( nawet zero ??? )
	- wi�c nie b�dzie por�wnania aNrOfBytes i BytesRead
*/
	if( !ReadFile(hCom, aDataPtr, aNrOfBytes, & NrOfBytesRead, NULL) )	/* odczytujemy */
	{
		return ERR_READ_FILE_COM;
	}

	*aNrOfBytesReadPtr = NrOfBytesRead;

	if( LoggingState == LOGGING_ACTIVE )
	{
		return LogUpdate( LOG_READ, NrOfBytesRead, aDataPtr );	 /* i ewentualnie dopisujemy do logu */
	}

	return SOK;
}

/*----------------------------------------------------------------------------*/

int CComPort::SendSingleByte( BYTE aByte )
{
	static int RetI;

	ClearRxQueue();

	RetI = Write( 1, & aByte );
	return RetI;
}

/*----------------------------------------------------------------------------*/

void CComPort::ClearRxQueue()
{
	static BOOL RetB;
	
	RetB = PurgeComm( hCom, PURGE_RXCLEAR );		/* PurgeComm wyczy�ci jedynie kolejk� wej�ciow� */
	_ASSERTE( RetB );
}

/*----------------------------------------------------------------------------*/

int CComPort::WaitForAnyByte( DWORD aTimeout, BYTE *aFetchedByte )
{
	static BOOL RetB;
	static BYTE FtchdByte;
	static DWORD BytesRead;
	static COMMTIMEOUTS TempTimeouts;

/*
	Ustawiamy nowe warto�ci Timeout-�w 
	- w sumie tylko ca�kowity czas na odebranie
*/
	memset( & TempTimeouts, 0 , sizeof(TempTimeouts) );
	TempTimeouts.ReadTotalTimeoutConstant = aTimeout;
	RetB = SetCommTimeouts( hCom, & TempTimeouts );
	if( !RetB )
	{
		return ERR_COMM_SETTINGS;
	}

/*
	Odczytujemy jeden bajt 
	- troch� lepsze rozwi�zanie ni� WaitCommEvent dla operacji synchronicznych
*/
	RetB = ReadFile( hCom, & FtchdByte, 1,  & BytesRead, NULL );
	if( !RetB )
	{
		return ERR_READ_FILE_COM;
	}
	

/*
	Przywracamy normalne warto�ci Timeout-�w
*/
	RetB = SetCommTimeouts( hCom, & Timeouts );
	if( !RetB )
	{
		return ERR_COMM_SETTINGS;
	}

	if( !BytesRead )
	{
		return ERR_NO_RESPONSE;			/* B��d - nie doczekali�my si�... */
	}
	else
	{
		/* Ewentualne zapami�tanie bajtu */
		if( aFetchedByte )
		{
			*aFetchedByte = FtchdByte;
		}

/*
		Ewentualne uaktualnienie logu...
		Nic mi tu nie wstawia� po tym IF-ie, bo jest RETURN !!!
*/		
		if( LoggingState == LOGGING_ACTIVE )
		{
			return LogUpdate( LOG_READ, 1, & FtchdByte );
		}
	}

	return SOK;
}

/*----------------------------------------------------------------------------*/

BOOL CComPort::WasPortOpened()
{
	return ( hCom != NULL );
}

/*----------------------------------------------------------------------------*/

int CComPort::Close()
{
	static int RetI;


	if( !hCom )
	{
		return SERROR;
	}

	RetI = SOK;

	CleanUp();

	if( LoggingState == LOGGING_ACTIVE )
	{
		RetI = LoggingStop();		/* przy zamkni�ciu portu wy��czamy logowanie (zamykamy plik) */
	}

	return RetI;
}

/*----------------------------------------------------------------------------*/

void CComPort::CleanUp()
{
	static BOOL RetB;

	/* deallokacja bufor�w - o ile zd��ono je zaallokowa� */
	if( PortOutBuf )
	{
		free( PortOutBuf );
	}

	if( PortInBuf )
	{
		free( PortInBuf );
	}

	RetB = CloseHandle( hCom );		/* w�a�ciwe zamkni�cie portu */
	_ASSERTE( RetB );

	hCom = NULL;
}

/******************************************************************************/

int CComPort::LoggingStart( char *aFileName )
{

	if( LoggingState == LOGGING_ACTIVE )	/* nie mo�na drugi raz rozpocz�� logowania, je�li si� */
	{											/* wcze�niej nie zako�czy�o poprzedniej sesji */
		return SERROR;
	}

/*
	Tworzymy plik, do kt�rego b�d� wpisywane logowane sekwencje
*/
	hLogFile = CreateFile( aFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
	
	if( hLogFile == INVALID_HANDLE_VALUE )
	{
		return ERR_CREATE_FILE;
	}

/*
	Otwieramy Mailslota do zapisu - ale tylko jeden raz. Nie b�dziemy go zamyka�, bo 
	mog�yby by� problemy z klientami.
	
	Wcze�niej musi by� jednak otwarty do odczytu - utworzony przez CreateMailslot
	(w programie zewn�trznym).
*/

	if( ! hMailSlut )
	{
		hMailSlut = CreateFile( SNOOP_MAILSLOT_NAME,
													GENERIC_WRITE,
													FILE_SHARE_READ,
													NULL,
													OPEN_EXISTING,
													FILE_ATTRIBUTE_NORMAL,
													NULL );

		if( hMailSlut == INVALID_HANDLE_VALUE )
		{
			hMailSlut = NULL;
		}
	}

/*
	LogLineLen = 0;
	LogLastAccess = LOG_READ;
*/

	LoggingState = LOGGING_ACTIVE;	/* od tej pory wszystkie operacje R/W b�d� logowane */
	LogLastOperation = LOG_READ;		/* najpierw na pewno b�dziemy co� wysy�a� - a inicjalizacja */
																	/* w tryb przeciwny */
	
	LogLineLen = 0;									/* na razie zero bajt�w w buforze */
	
	return SOK;
}

/*----------------------------------------------------------------------------*/

int CComPort::LoggingStop()
{
	static BOOL RetB;
	static int RetI;

	if( LoggingState == LOGGING_INACTIVE )
	{
		return SERROR;
	}

	RetI = LogCreateHexAndWrite();		/* je�li co� jeszcze siedzia�o w buforze - zapiszemy */

	LoggingState = LOGGING_INACTIVE;	/* koniec logowania */

	RetB = CloseHandle( hLogFile );		/* zamykamy plik */

	return SOK;
}

/*----------------------------------------------------------------------------*/

int CComPort::LogCreateHexAndWrite()
{
	static BOOL RetI;

	static DWORD i,
		BytesToWrite,
		BytesWritten;
	static char * DataPtr;

	DataPtr = (char *) LogStrLineBuf;

	if( LogLineLen > 0 )														/* je�li jest co� w buforze... */
	{
		for( i = 0; i < LogLineLen; i++ )							/* najpierw generujemy zrzut szesnastkowy */
		{
			DataPtr += sprintf( DataPtr, "%02X ", LogByteLineBuf[i] );
		}

		for( i = LogLineLen; i < MAX_LOG_LINE; i++ )	/* produkujemy ewentualn� dziur� (je�li mniej ni� MAX_LOG_LINE) */
		{
			DataPtr += sprintf( DataPtr, "   " );
		}

		DataPtr += sprintf( DataPtr, "   " );					/* odst�p */

		for( i = 0; i < LogLineLen; i++ )							/* no i w ko�cu posta� ASCII (lub kropka, gdy niedrukowalny) */
		{
			if( isprint(LogByteLineBuf[i]) )
			{
				DataPtr[i] = LogByteLineBuf[i];
			}
			else
			{
				DataPtr[i] = '.';
			}
		}

		DataPtr += LogLineLen;
		BytesToWrite = DataPtr - LogStrLineBuf;

		RetI = WriteToLog( (BYTE *) LogStrLineBuf, BytesToWrite, & BytesWritten, TRUE );
		if( (RetI != SOK) || (BytesWritten != BytesToWrite) )
		{
			return ERR_WRITE_FILE;
		}

		LogStrLineBuf[0] = '\n';
		RetI = WriteToLog( (BYTE *) LogStrLineBuf, 1, & BytesWritten, FALSE );
		if( (RetI != SOK) || (BytesWritten != 1) )
		{
			return ERR_WRITE_FILE;
		}
	}

	LogLineLen = 0;																/* ca�� zawarto�� bufora zapisali�my, mo�na wpisywa� nowe rzeczy */

	return SOK;
}

/*----------------------------------------------------------------------------*/

int CComPort::LogUpdate( enum LOG_OPERATION aLogOperation, DWORD aNrOfBytes, BYTE *aDataPtr )
{
	static int RetI;

	static DWORD BytesToWrite,
		BytesWritten,
		BytesToCopy;
	
	static char OperationWr[] = "Written:",
		OperationRd[] = "Read:";

	if( !aDataPtr )
	{
		return SERROR;
	}

	if( aLogOperation != LogLastOperation )				/* gdy poprzedni dost�p by� innego typu */
	{
		LogCreateHexAndWrite();											/* zapiszemy to co jeszcze znajduje si� w buforze */

		LogStrLineBuf[0] = '\n';
		RetI = WriteToLog( (BYTE *) LogStrLineBuf, 1, & BytesWritten, TRUE );
		if( (RetI != SOK) || (BytesWritten != 1) )
		{
			return ERR_WRITE_FILE;
		}

		if(	aLogOperation == LOG_READ )							/* nast�pnie dopiszemy nag��wek, m�wi�cy jaki rodzaj dost�pu */
		{
			BytesToWrite = sprintf( LogStrLineBuf, "%s", OperationRd );
		}
		else
		{
			BytesToWrite = sprintf( LogStrLineBuf, "%s", OperationWr );
		}

		RetI = WriteToLog( (BYTE *) LogStrLineBuf, BytesToWrite, & BytesWritten, TRUE );
		if( (RetI != SOK) || (BytesWritten != BytesToWrite) )
		{
			return ERR_WRITE_FILE;
		}

		LogStrLineBuf[0] = '\n';
		RetI = WriteToLog( (BYTE *) LogStrLineBuf, 1, & BytesWritten, FALSE );
		if( (RetI != SOK) || (BytesWritten != 1) )
		{
			return ERR_WRITE_FILE;
		}
	}


/*
	Dop�ki mamy pe�ne wielokrotno�ci MAX_LOG_LINE (po ewentualnym dopisaniu do tego, co wcze�niej znajdowa�o si� w buforze) -
	generujemy linie tekstowe i wpisujemy je do pliku
*/
	do
	{
		BytesToCopy = min( (MAX_LOG_LINE - LogLineLen), aNrOfBytes );

		memcpy( LogByteLineBuf + LogLineLen, aDataPtr, BytesToCopy );
		LogLineLen += BytesToCopy;
		aDataPtr += BytesToCopy;
		aNrOfBytes -= BytesToCopy;

		if( LogLineLen == MAX_LOG_LINE )
		{
			LogCreateHexAndWrite();
		}

	} while( aNrOfBytes > 0 );

	
	LogLastOperation = aLogOperation;							/* na koniec zapami�tujemy jaka operacja zosta�a dopisana do logu */
	return SOK;
}

/*----------------------------------------------------------------------------*/

int CComPort::WriteToLog( BYTE * aBufferAddr, DWORD aBytesToWrite, DWORD * aBytesWrittenPtr, BOOL aLogAlso )
{
	static BOOL RetB;

	if( (hMailSlut != NULL) && aLogAlso )
	{
		RetB = WriteFile( hMailSlut, aBufferAddr, aBytesToWrite, aBytesWrittenPtr, NULL );
	}

	RetB = WriteFile( hLogFile, aBufferAddr, aBytesToWrite, aBytesWrittenPtr, NULL ); /* dopisujemy do pliku */
	if( (! RetB) || (* aBytesWrittenPtr != aBytesToWrite) )
	{
		return ERR_WRITE_FILE;
	}

	return SOK;
}

/*----------------------------------------------------------------------------*/

int CComPort::LogFlush()
{
	if( LoggingState == LOGGING_ACTIVE )
	{
		return LogCreateHexAndWrite();
	}
	else
	{
		return SOK;
	}
}