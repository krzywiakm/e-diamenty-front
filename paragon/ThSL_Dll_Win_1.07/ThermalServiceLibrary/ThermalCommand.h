/* Tab 2 */

/*
	ThermalCommand.h - plik nag��wkowy dla ThermalCommand.cpp
*/

#ifndef _THERMAL_COMMAND
#define _THERMAL_COMMAND

#include "StdAfx.h"
#include "..\\CommonTexts.h"
#include "..\\ThermalServiceLibrary.h"
#include "FunctionDeclarations.h"

#define LBSERM_PS_MAX 3

extern DWORD ExtendedErrorCode;

class CThermalCommand
{
public:
	CThermalCommand();
	~CThermalCommand();

/*
	Clear
		Wyczy�� rozkaz (== inicjalizacja)

	Argumenty:
		---

	Zwraca:
		---
*/
	void Clear();


/*
	GetErrorMessage
		Pobierz komunikat o b��dzie zasygnalizowanym przez urz�dzenie
		
	Argumenty wej�ciowe:
		aExtendedErrorCode	- rozszerzony kod b��du (zawieraj�cy kod rozkazu i kod b��du)

	Argumenty wyj�ciowe:
		aStrOutPtr					- /PTR to PTR/ pod ten adres zostanie wpisany wska�nik do tekstu

	Zwraca:
		TRUE je�li wygenerowano opis b��du (warunkiem jest poprawna sk�adnia kodu b��du),
		FALSE je�li nie
*/
	BOOL GetErrorMessage( DWORD aExtendedErrorCode, char **aStrOutPtr );


/*
	Translate
		Zamie� rozkaz z zewn�trznej postaci (ci�g rozkazowy, struktura argument�w wraz z
		tablic� opisuj�c� rodzaje p�l struktury i ich dozwolonymi warto�ciami, na posta�
		wewn�trzn� (wektory: tekst�w, rodzaj�w p�l, d�ugo�ci tekst�w)

	Argumenty wej�ciowe:
		aCommandPtr					-	/PTR/ ci�g rozkazowy
		aFieldDescrTabLen		- wielko�� tablicy opisuj�cej pola struktury ( sizeof(...) )
		aFieldDescrTabPtr		-	/PTR/ tablica opisuj�ca pola struktury
		aArgStructPtr				- /PTR/ struktura z argumentami rozkazu

	Zwraca:
		SOK									- gdy operacja zako�czy�a si� pomy�lnie
		ERR_ACCESS_VIOLATION
		ERR_PARAM_VALUE
		ERR_PARAM_LEN
		
	Uwagi:
		1. aFieldDescrTabLen - podajemy wielko�� tablicy (jej elementy to struktury),
			natomiast liczba p�l jest wyliczana wewn�trz Translate
		2. kody b��d�w zawieraj� w sobie indeks (liczony od zera) argumentu, kt�ry spowodowa�
			b��d (suma logiczna maski oznaczaj�cej kod b��du i indeksu)
*/
	int Translate( BYTE	aProtocolType, char *aCommandPtr, DWORD aFieldDescrTabLen,
										 tPARAMETER_DESCRIPTION *aFieldDescrTabPtr,	BYTE *aArgStructPtr );


/*
	CreateFrame
		Utw�rz kompletn� ramk� - pobieraj�c dane z wewn�trznej postaci rozkazu

	Argumenty wej�ciowe:
		aMaxNrOfBytes				- d�ugo�� bufora wyj�ciowego (= maks. liczba bajt�w)
		aOutBufPtr					-	/PTR/ bufor wyj�ciowy

	Argumenty wyj�ciowe:
		aBytesWrittenPtr		- /PTR/ je�li zostanie podany (!=NULL) to wpisana tam b�dzie
													liczba bajt�w wpisanych do bufora
	Zwraca:
		SOK
		ERR_FRAME_TOO_LONG	- gdy bufor b�dzie za kr�tki (ramka si� nie zmie�ci)
*/
	int CreateFrame( DWORD aMaxNrOfBytes, BYTE *aOutBufPtr, DWORD *aBytesWrittenPtr );


/*
	SetCmdString
		Ustaw ci�g rozkazowy (- posta� wewn�trzna rozkazu)
	
	Argumenty wej�ciowe:
		aCmdStringPtr				- /PTR/ ci�g rozkazowy (dwuznakowy)

	Zwraca:
		---
*/
	void SetCmdString( char *aCmdStringPtr );


/*
	GetParam
		Pobierz parametr (ju� po konwersji na tekst)

	Argumenty wej�ciowe:
		aParamNdx						- indeks (od zera) parametru

	Zwraca:
		��dany parametr (obiekt klasy 'string')
*/
	string GetParam( DWORD aParamNdx );


/*
	GetParamCount
		Pobierz liczb� parametr�w

	Argumenty:
		---

	Zwraca:
		liczb� parametr�w
*/
	DWORD GetParamCount();


/*
	GetRequiredFrameBufferLength
		Pobierz wymagan� d�ugo�� bufora dla ramki

	Argumenty:
		---

	Zwraca:
		d�ugo�� bufora
*/
	DWORD GetRequiredFrameBufferLength();


/*
	AddByteParam
		Dodaj do wewn�trznej reprezentacji rozkazu parametr bajtowy

	Argumenty wej�ciowe:
		aParam							- parametr
		aFieldSep						- rodzaj separatora/terminatora (je�li nie jest zgodny z rodzajem pola)
		aMin								- warto�� minimalna
		aMax								- warto�� maksymalna

	Zwraca:
		SOK
		ERR_PARAM_VALUE			- gdy warto�� nie mie�ci si� wewn�trz podanego zakresu

	Uwagi:
		aMin i aMax oba r�wne zero -> brak kontroli
*/
	int AddByteParam( BYTE aParam, BYTE aFieldSep = 0, BYTE aMin = 0, BYTE aMax = 0 );

/*
	AddNumericParam
		Dodaj do wewn�trznej reprezentacji rozkazu parametr numeryczny (DWORD)

	Argumenty wej�ciowe:
		aParam							- parametr
		aFieldSep						- rodzaj separatora/terminatora (je�li nie jest zgodny z rodzajem pola)
		aMin								- warto�� minimalna
		aMax								- warto�� maksymalna

	Zwraca:
		SOK
		ERR_PARAM_VALUE			- gdy warto�� nie mie�ci si� wewn�trz podanego zakresu

	Uwagi:
		aMin i aMax oba r�wne zero -> brak kontroli
*/
	int AddNumericParam( DWORD aParam, BYTE aFieldSep = 0, DWORD aMin = 0, DWORD aMax = 0 );

/*
	AddNumericParamExt
		Dodaj do wewn�trznej reprezentacji rozkazu parametr numeryczny w postaci zewn�trznej

	Argumenty wej�ciowe:
		aParamPtr						- /PTR/ struktura, przez kt�r� przekazywany jest parametr
		aFieldSep						- rodzaj separatora/terminatora (je�li nie jest zgodny z rodzajem pola)
		aMaxLen							- maks. d�ugo�� tekstowej reprezentacji (��cznie z kropk�)
		aMaxIntg						- maks. d�ugo�� cz�ci ca�kowitej
		aMaxFact						- maks. d�ugo�� cz�ci u�amkowej

	Zwraca:
		SOK
		ERR_PARAM_LEN				- gdy nieprawid�owa d�ugo�� (ca�o�ci lub cz�ci)
		ERR_FIELD_TYPE			- gdy ci�g nie odpowiada warto�ci numerycznej

	Uwagi:
		aMaxLen r�wne 0 wy��cza dalsze sprawdzanie d�ugo�ci
		aMaxIntg i aMaxFact oba r�wne zero - nie b�d� sprawdzane d�ugo�ci cz�ci, natomiast
			mo�e by� sprawdzana d�ugo�� ca�o�ci (zale�y od aMaxLen)
*/
	int AddNumericParamExt( tEXTERNAL_NUMERIC *aParamPtr, BYTE aFieldSep = 0, BYTE aMaxLen = 0, BYTE aMaxIntg = 0, BYTE aMaxFact = 0 );

/*
	CheckNumericParamExt
		Sprawd� poprawno�� (sk�adniow�) postaci tekstowej warto�ci numerycznej.
		- Funkcja pomocnicza wykorzystywana przez AddNumericParamExt

	Argumenty wej�ciowe:
		aParamPtr						- /PTR/ tekst
		aLen								- d�ugo�� tekstu
		aMaxIntg						- maks. d�ugo�� cz�ci ca�kowitej
		aMaxFact						- maks. d�ugo�� cz�ci u�amkowej

	Zwraca:
		SOK
		ERR_PARAM_LEN				- gdy nieprawid�owa d�ugo��
		ERR_FIELD_TYPE			- gdy ci�g nie odpowiada warto�ci numerycznej

	Uwagi:
		aMaxIntg i aMaxFact oba r�wne zero -> bez sprawdzania d�ugo�ci
*/
	int CheckNumericParamExt( char *aParamPtr, BYTE aLen, BYTE aMaxIntg, BYTE aMaxFact );

/*
	AddStringParam
		Dodaj do wewn�trznej reprezentacji rozkazu parametr tekstowy
			
	Argumenty wej�ciowe:
		aParamPtr						- /PTR/ tekst
		aFieldSep						- rodzaj separatora/terminatora (je�li nie jest zgodny z rodzajem pola)
		aMaxLen							- maks. d�ugo�� ci�gu

	Zwraca:
		SOK
		ERR_PARAM_LEN				- gdy nieprawid�owa d�ugo�� ci�gu

	Przy podaniu zerowego wska�nika zapami�tany zostanie ci�g pusty,
	ale w	pole d�ugo�ci wpisany b�dzie specjalny	znacznik - STR_EMPTY.
	W takim przypadku pole to nie b�dzie obecne w ramce (dotyczy r�wnie� separatora).
*/
	int AddStringParam( char *aParamPtr, BYTE aFieldSep = 0, BYTE aMaxLen = 0 );

/*
	AddLineParam
		Dodaj do wewn�trznej reprezentacji rozkazu parametr tekstowy - d�ug� lini�
		(r�nica pomi�dzy AddStringParam a AddLineParam jest jedna - zapami�tany zostanie
		inny rodzaj pola, co da w wyniku inny separator)
			
	Argumenty wej�ciowe:
		aParamPtr						- /PTR/ tekst
		aFieldSep						- rodzaj separatora/terminatora (je�li nie jest zgodny z rodzajem pola)
		aMaxLen							- maks. d�ugo�� ci�gu

	Zwraca:
		SOK
		ERR_PARAM_LEN				- gdy nieprawid�owa d�ugo�� ci�gu

	Uwagi: jak przy AddStringParam
*/
	int AddLineParam( char *aParamPtr, BYTE aFieldSep = 0, BYTE aMaxLen = 0 );


/*
	ConcatenateParam
		Do��cz do wybranego parametru (po konwersji na tekst) podany ci�g
	
	Argumenty wej�ciowe:
		aParamNdx						- indeks (od zera) parametru
		aStringPtr					- /PTR/ ci�g kt�ry nale�y doda�
	
	Zwraca:
		---
*/
	void ConcatenateParam( DWORD aParamNdx, char *aStringPtr );


/*
	AddField
		Dodaj pole (parametr) do wewn�trznej reprezentacji rozkazu

	Argumenty wej�ciowe:
		aStrPtr							- /PTR/ parametr (w postaci tekstowej)
		aFieldType					- rodzaj pola:
														FLD_BYT - bajtowe
														FLD_NUM - numeryczne
														FLD_STR - tekstowe
														FLD_LIN - tekstowe wieloliniowe
		aStrLen							- d�ugo�� tekstu (parametru) lub
													STR_EMPTY je�li ma to by� puste pole

	Zwraca:
		---
*/
	void AddField( char *aStrPtr, BYTE aFieldType, DWORD aStrLen );


/*
	InsertParam
		Wstaw nowy parametr (tekst) do wewn�trznej reprezentacji rozkazu

	Argumenty wej�ciowe:
		aParamNdx						- indeks (od zera) parametru przed kt�rym zostanie wstawiony nowy tekst
		aFieldType					- rodzaj pola:
														FLD_BYT - bajtowe
														FLD_NUM - numeryczne
														FLD_STR - tekstowe
														FLD_LIN - tekstowe wieloliniowe
		aStringPtr					- /PTR/ tekst, kt�ry nale�y wstawi�

	Zwraca:
		---
*/
	void InsertParam( DWORD aParamNdx, BYTE aFieldType, char *aStringPtr );


/*
	Send, Send2   (Send to nak�adka na Send2, dodane jest flush-owanie logu)
		Generuje a nast�pnie wysy�a ramk� rozkazow�.
	
	Argumenty:
		---
	
	Zwraca:
		SOK
		ERR_PORT_NOT_OPEN	- pr�ba wys�ania do portu, kt�ry nie zosta� otwarty
		ERR_FRAME_TOO_LONG - gdy bufor za kr�tki (ramka si� nie zmie�ci)
		ERR_WRITE_FILE_COM - b��d zapisu do portu
		ERR_WRITE_FILE		- b��d zapisu do pliku (log) - je�li w��czone jest logowanie
*/
	int Send();
	int Send2();


/*
	GetExtendedErrorCode
		Pobierz rozszerzony kod b��du -> w niekt�rych rozkazach zwracane s� dodatkowe
		inforamcje o b��dach.

	Argumenty:
		---
	
	Zwraca:
		kod b��du.
*/
	DWORD GetExtendedErrorCode();


/*
	GetProtocolType
		Zwr�� rodzaj protoko�u (zapami�tany przy Translate(...))

	Argumenty:
		---
	
	Zwraca:
		rodzaj protoko�u (PROTOCOL_ITALIAN..., PROTOCOL_OLD...)
*/
	BYTE GetProtocolType();



/*
	Zmienne
*/
													/* Flagi: */
	BOOL F_Resp,						/* obecno�� odpowiedzi */
		F_Italian,						/* protok� w�oski */
		F_ItalianRemote,			/* protok� w�oski, Remote PC */
		F_RxChksum,						/* suma kontrolna w ramce nadawanej */
		F_TxChksum;						/* suma kontrolna w ramce odbieranej */

protected:
	char	CmdString[CMD_CMD_STR_LEN+1];

	DWORD	ParamCount,											/* Parameter Count */
		ByteParamCount;											/* Byte Parameter Count */

	BYTE ProtocolType;

/*
	Poni�sze trzy obiekty dotycz� ramki wysy�anej
*/
	tVectorOfStrings	VectOfTextFields;		/* argumenty rozkazu zamienione na stringi */
	tVectorOfBytes		VectOfFieldTypes;		/* rodzaj argumentu -> wyznacza separator */
	tVectorOfDwords		VectOfTextLengths;	/* d�ugo�� argumentu zamienionego na tekst */



/*
	RemoveDot
		Usu� kropk� z tekstowej postaci warto�ci numerycznej (to dla protoko�u w�oskiego)
	
	Argumenty wej�ciowe:
		aNumericInPtr				- /PTR/ ci�g wej�ciowy

	Argumenty wyj�ciowe:
		aNumericInPtr				- /PTR/ ci�g wyj�ciowy (je�li nie by�o kropki w wej�ciowym - to 
													zostanie tam skopiowany ci�g wej�ciowy
	Zwraca:
		TRUE je�li kropka zosta�a usuni�ta - wtedy ci�g wyj�ciowy jest o jeden znak kr�tszy 
		od wej�ciowego
*/
	BOOL RemoveDot( char * aNumericInPtr, char * aNumericOutPtr );


};

#endif // _THERMAL_COMMAND