/* Tab 2 */

/*
	ThermalServiceLibrary.cpp - g��wny plik, zawieraj�cy DllMain i funkcje
	eksportowane z biblioteki.
*/

#include "StdAfx.h"
#include "..\\ThermalServiceLibrary.h"
#include "StructDescriptions.h"
#include "Texts.h"
#include "ThermalCommand.h"
#include "ThermalResponse.h"
#include "ComPort.h"
#include "FunctionDeclarations.h"

#define DLL_VERSION "1.02"

/******************************* GLOBALNE *************************************/

CComPort						CThComPort;						/* port szeregowy */
CThermalCommand			CThCmd;								/* rozkaz (ramka nadawana) */
CThermalResponse		CThResp;							/* odpowied� (ramka odebrana) */

const char Zero[]		= "0";								/* wype�niacz - zero */
const char Empty[]	= "";									/* puste pole */

DWORD								ExtendedErrorCode;		/* rozszerzony kod b��du */

tTIMEOUT_DLG_FUNC		TimeoutDialogFunc =		/* adres funkcji wywo�ywanej przy */
											& TimeoutDialog;		/* wyst�pieniu timeout-u */

char DllVersion[] = "$ver: "DLL_VERSION;	/* identyfikator wersji */


/******************************** DllMain *************************************/

/*
		�adna z przyczyn wywo�ania DllMain nie posiada zaimplementowanej obs�ugi - 
		bo nie ma takiej potrzeby
*/
BOOL APIENTRY DllMain( HANDLE hModule, 
												DWORD  ul_reason_for_call, 
												LPVOID lpReserved)
{
	switch( ul_reason_for_call ) 
	{ 
		case DLL_PROCESS_ATTACH:
			break;

		case DLL_THREAD_ATTACH:
			break;

		case DLL_THREAD_DETACH:
			break;

		case DLL_PROCESS_DETACH:
			break;
	}

	return TRUE;
}

/******************************************************************************/

void ErrorMessage( char *aStrPtr, int aReturnCode )
{
	static string ErrString, ErrMessage;
	static DWORD ErrorCode;
	static char *ErrStrPtr, ErrorCodeStr[16];

	ErrMessage = aStrPtr;

	if( aReturnCode == ERR_RETURN_CODE )
	{
		ErrorCode = GetDeviceError();

		if( GetDeviceErrorMessage(ErrorCode, & ErrStrPtr) )
		{
			ErrString = ErrStrPtr;

			ErrMessage += "\n";
			ErrMessage += ErrString;
		}
	}
	else if( aReturnCode == ERR_NO_RESPONSE )
	{
		ErrMessage = aStrPtr;

		ErrMessage += "\n( ";
		ErrMessage += ERRMSG_NO_RESPONSE;
		ErrMessage += " )";

	}
	else
	{
		ErrMessage = aStrPtr;

		ErrorCode = aReturnCode & ERR_SPECIAL_MASK;
		
		if( (ErrorCode == ERR_ACCESS_VIOLATION) || (ErrorCode == ERR_PARAM_VALUE) ||
			(ErrorCode == ERR_PARAM_LEN) || (ErrorCode == ERR_FIELD_TYPE) )
		{
			sprintf( ErrorCodeStr, "0x%8X", aReturnCode );
		}
		else
		{
			sprintf( ErrorCodeStr, "%d", aReturnCode );
		}

		ErrMessage += "\n( ";
		ErrMessage += ERRMSG_INTERNAL_CODE;
		ErrMessage += ErrorCodeStr;
		ErrMessage += " )";
	}

	MessageBox( NULL, ErrMessage.c_str(), ERRMSG_TITLE, MB_OK | MB_ICONERROR );
}

/******************************************************************************/

int SetTimeoutDialogFunc( tTIMEOUT_DLG_FUNC aFuncAddr )
{
	/* Co tu mo�na sprawdza� ? Null pointer i nic wi�cej */

	if( aFuncAddr )
	{
		TimeoutDialogFunc = aFuncAddr;
		return SOK;
	}
	else
	{
		return SERROR;
	}
}

/*----------------------------------------------------------------------------*/

int TimeoutDialog()
{
	return MessageBox( NULL, QUESTNMSG_TIMEOUT, QUESTNMSG_TITLE, MB_YESNOCANCEL | MB_ICONQUESTION );
}

/******************************************************************************/

BOOL FindFrameEnd( BYTE *aBufferPtr, DWORD aNrOfBytesInBuffer, DWORD *aEndMarkOffsPtr )
{
	static DWORD i;		/* standardowe i ;-) */

/*
	W wersji DEBUGowalnej sprawdzamy czy wska�niki nie s� zerowe
*/
	_ASSERTE( aBufferPtr );
	_ASSERTE( aEndMarkOffsPtr );

/*
	Znacznik ko�ca ma dwa bajty - wi�c przynajniej tyle musi by� w buforze
*/
	if( aNrOfBytesInBuffer < 2 )
	{
		return FALSE;
	}
	
/*
	Przeszukiwanie od ko�ca bufora do drugiego jego elementu
*/
	for( i = (aNrOfBytesInBuffer - 1); i != 1; i-- )
	{
		if( aBufferPtr[i] == '\\' )					/* Je�li znaleziono znak \ */
		{
			if( aBufferPtr[i-1] == 0x1b )			/* a przed nim znak ESC */
			{
				*aEndMarkOffsPtr = i - 2;				/* to zwr�cimy indeks elementu przed ko�cem */
				return TRUE;										/* i wychodzimy - znale�li�my znacznik ko�ca */
			}
		}
	}

	return FALSE;	/* Wychodzimy - nie znale�li�my */
}

/******************************************************************************/

BYTE GetTrueFieldType( BYTE aFieldType )
{
	switch(aFieldType & FLD_SEP_MASK)
	{
		default:								/* rodzaj pola zgodny z terminatorem/separatorem*/
			break;
		
		case FLD_SEP_BYT:				/* case - dla p�l, kt�re maj� inaczej */
			aFieldType = FLD_BYT;
			break;

		case FLD_SEP_NUM:
			aFieldType = FLD_NUM;
			break;
		
		case FLD_SEP_STR:
			aFieldType = FLD_STR;
			break;
		
		case FLD_SEP_LIN:
			aFieldType = FLD_LIN;
			break;
	}

	return aFieldType;
}

/******************************************************************************/

char SeparatorFromFieldType( BYTE aFieldType, BOOL aProtoItalian )
{

/*
	Czy wyspecyfikowany zosta� inny rodzaj separatora ?
*/
	aFieldType = GetTrueFieldType( aFieldType );


	if( aProtoItalian )		/* w�oski protok� ma troch� inne terminatory... */
	{
		switch( aFieldType )
		{
			default:
				_ASSERTE( FALSE );		/* DEFAULT tylko w celach kontrolnych */
				break;

			case FLD_BYT:
				return (char) SEP_BYT_IT;

			case FLD_NUM:
				return (char) SEP_NUM_IT;

			case FLD_STR:
				return (char) SEP_STR_IT;

			case FLD_LIN:
				return (char) SEP_LIN_IT;
		}
	}
	else
	{
		switch( aFieldType )
		{
			default:
				_ASSERTE( FALSE );		/* DEFAULT tylko w celach kontrolnych */
				break;

			case FLD_BYT:
				return (char) SEP_BYT;

			case FLD_NUM:
				return (char) SEP_NUM;

			case FLD_STR:
				return (char) SEP_STR;

			case FLD_LIN:
				return (char) SEP_LIN;
		}
	}

	return 0;
}

/******************************************************************************/

BYTE GenerateChecksum( BYTE *aDataPtr, DWORD aNrOfBytes )
{
	static DWORD i;
	static BYTE ChkSum;
	
	ChkSum = aDataPtr[0];

	for( i = 1; i < aNrOfBytes; i++ )
	{
		ChkSum = ChkSum ^ aDataPtr[i];
	}

	return (ChkSum ^ 0xFF);						/* suma kontrolna zanegowana */
}

/******************************************************************************/

char * ConvertNumericToAmount( char *aNumStrIn, BOOL aEuroMode )
{
	static DWORD StrLen;
	static string StrIn, TmpStr;
	
	_ASSERTE( aNumStrIn );
	
	
	if( aEuroMode )
	{
		
		StrIn = aNumStrIn;
		StrLen = StrIn.length();
		
		if( StrLen == 1 )
		{
			StrIn.insert( 0, "0.0" );
		}
		else if( StrLen == 2 )
		{
			StrIn.insert( 0, "0." );
		}
		else if( StrLen > 2 )
		{
			TmpStr = StrIn;
			StrIn = TmpStr.substr( 0, StrLen - 2 );
			StrIn += ".";
			StrIn += TmpStr.substr( StrLen - 2 - 1, 2 );
		}
		else
		{
			return NULL;
		}
		
		return (char *) StrIn.c_str();
	}
	else
	{
		return aNumStrIn;
	}
}

/******************************************************************************/

char * ConvertNumericToQuantity( char *aNumStrIn )
{
	static DWORD StrLen;
	static string StrIn, TmpStr;

	_ASSERTE( aNumStrIn );

	StrIn = aNumStrIn;
	StrLen = StrIn.length();

	if( StrLen == 1 )
	{
		StrIn.insert( 0, "0.00" );
	}
	else if( StrLen == 2 )
	{
		StrIn.insert( 0, "0.0" );
	}
	else if( StrLen == 3 )
	{
		StrIn.insert( 0, "0." );
	}
	else if( StrLen > 3 )
	{
		TmpStr = StrIn;
		StrIn = TmpStr.substr( 0, StrLen - 3 );
		StrIn += ".";
		StrIn += TmpStr.substr( StrLen - 3, 3 );
	}
	else
	{
		return NULL;
	}

	return (char *) StrIn.c_str();

}
/******************************************************************************/

BOOL CheckDate( BYTE aYear, BYTE aMonth, BYTE aDay, BYTE * aOffendingArg )
{
	BYTE off = -1;

	if( aYear > 99 )
	{
		*aOffendingArg = 0;
	}
	else if( (aMonth < 1) || (aMonth > 12) )
	{
		*aOffendingArg = 1;
	}
	else if( (aDay < 1) || (aDay > 31) )
	{
		*aOffendingArg = 2;
	}
	else
	{
		return TRUE;
	}
	
	return FALSE;
}

/******************************************************************************/

int Execute( CThermalCommand *aCThCmd, CThermalResponse *aCThResp,
									DWORD aResponseTime = NORMAL_RESPONSE_TIME )
{
	static int RetI;
	static BOOL RespWithData;
	static tCASH_REGISTER_STATUS CRStat;
	static BYTE ProtocolType;

	_ASSERTE( aCThCmd );
	_ASSERTE( aCThResp );

	while( true )
	{
		RetI = aCThCmd->Send();
		if( RetI != SOK )
		{
			return RetI;
		}
		
		if( aCThResp->GetResponseStructPtr() )
		{
			RespWithData = TRUE;
		}
		else
		{
			RespWithData = FALSE;
		}
		
		ProtocolType = aCThCmd->GetProtocolType();
		
		RetI = aCThResp->Receive( ProtocolType, RespWithData, aResponseTime );
/* 
		Przy wyst�pieniu timeout-u u�ytkownik wybra� mo�e opcje: przerwania operacji,
		wznowienia czekania na odpowied� lub ponownego wys�ania ramki i czekania
*/
		if( RetI != ERR_RESEND )
			break;
	}

	if( RetI != SOK )
	{
		aCThResp->ClearResponseStructPtr();
		return RetI;
	}

/*
	Je�eli rozkaz zwraca odpowied� w postaci ramki z jakimi� tam polami - to przeprowadzimy ich analiz�.
*/
	if( RespWithData )
	{
		RetI = CThResp.CreateRespStruct( ProtocolType, aCThResp->GetResponseStructPtr() );

		aCThResp->ClearResponseStructPtr();

		if( RetI != SOK )
		{
			return RetI;
		}
	}
	else
	{
		aCThResp->ClearResponseStructPtr();
	}

/*
	Protok� polski - skoro tu dotarli�my to oznacza to, �e prawdopodobnie rozkaz zosta� wykonany poprawnie.
	Je�li nie zosta� rozpoznany to odpowied� b�dzie posiada� zerowy kod b��du, ale w odpowiedzi na ENQ
	otrzymamy CMD wyzerowane (GetStatus... wpisze do odpowiedniej flagi FALSE)..

	Taka sytuacja zostanie wcze�niej wy�apana, je�li rozkaz ma odsy�a� jak�� odpowied� r�n� od standardowego
	potwierdzenia - bo wtedy Receive(...) wykryje, �e otrzymano ramk� potwierdzenia. Wi�c je�li co�
	
*/

	if( ((ProtocolType & (PROTO_RESP | PROTO_ITALIAN)) == PROTO_RESP) && (!RespWithData) )
	{
		RetI = GetStatus_CashRegister( & CRStat );

		if( !CRStat.CMD )
		{
			return ERR_UNKNOWN_CMD;
		}
	}

	return SOK;
}

/*******************************************************************************
*******************************  WSP�LNE  **************************************
*******************************************************************************/

int Port_Open( char *aPortNamePtr, DWORD aBaudRate )
{
	return CThComPort.Open( aPortNamePtr, aBaudRate );
}

/*----------------------------------------------------------------------------*/

int Port_SetBaudRate( DWORD aRate )
{
	return CThComPort.SetBaudRate( aRate );
}

/*----------------------------------------------------------------------------*/

void Port_Close()
{
	CThComPort.Close();
}

/*----------------------------------------------------------------------------*/

int Port_LoggingStart( char *aFileName )
{
	return CThComPort.LoggingStart( aFileName );
}

/*----------------------------------------------------------------------------*/
int Port_LoggingStop()
{
	return CThComPort.LoggingStop();
}
/******************************************************************************/

BOOL GetDeviceErrorMessage( DWORD aExtendedErrorCode, char **aStrOutPtr )
{
	return CThCmd.GetErrorMessage( aExtendedErrorCode, aStrOutPtr );
}

/*----------------------------------------------------------------------------*/

DWORD GetDeviceError()
{
	return CThCmd.GetExtendedErrorCode();		// r�wnie dobrze mo�na z CThResp
}

/******************************************************************************/

void Send_ACK()
{
	CThComPort.SendSingleByte( ACK );
}

/*----------------------------------------------------------------------------*/

void Send_NACK()
{
	CThComPort.SendSingleByte( NACK );
}

/*----------------------------------------------------------------------------*/

void Send_CAN()
{
	CThComPort.SendSingleByte( CAN );
}

/*----------------------------------------------------------------------------*/

void Send_BEL()
{
	CThComPort.SendSingleByte( BEL );
}

/*******************************************************************************
***************************  PROTOKӣ W�OSKI  **********************************
*******************************************************************************/

int I_LOGIN( tLOGIN_PARAMS *aLOGIN_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aLOGIN_ParamsPtr );

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, LOGIN_CMD_STR, sizeof(LOGIN_Descr), LOGIN_Descr,
													(BYTE *) aLOGIN_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int I_LOGOUT()
{
	static int RetI;

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, LOGOUT_CMD_STR, 0, NULL, NULL );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int I_SETOPT( tSETOPT_PARAMS *aSETOPT_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aSETOPT_ParamsPtr );

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, SETOPT_CMD_STR, sizeof(SETOPT_Descr), SETOPT_Descr,
													(BYTE *) aSETOPT_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int I_SETVAT( tSETVAT_PARAMS *aSETVAT_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aSETVAT_ParamsPtr );

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, SETVAT_CMD_STR, sizeof(SETVAT_Descr), SETVAT_Descr,
													(BYTE *) aSETVAT_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int I_TRSHDR( tTRSHDR_PARAMS *aTRSHDR_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aTRSHDR_ParamsPtr );

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, TRSHDR_CMD_STR, sizeof(TRSHDR_Descr), TRSHDR_Descr,
													(BYTE *) aTRSHDR_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int I_TRSLNE( tTRSLNE_PARAMS *aTRSLNE_ParamsPtr )
{
	static int RetI;
	
	_ASSERTE( aTRSLNE_ParamsPtr );

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, TRSLNE_CMD_STR, sizeof(TRSLNE_Descr), TRSLNE_Descr,
													(BYTE *) aTRSLNE_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int I_TRSEND( tTRSEND_PARAMS *aTRSEND_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aTRSEND_ParamsPtr );

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, TRSEND_CMD_STR, sizeof(TRSEND_Descr), TRSEND_Descr,
													(BYTE *) aTRSEND_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int I_TRSEXIT()
{
	static int RetI;

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, TRSEXIT_CMD_STR, 0, NULL, NULL );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int I_RECADD( tRECADD_PARAMS *aRECADD_ParamsPtr )
{
	static int RetI;
	static DWORD ParamDescrSize;

/*
	W tej tablicy przygotujemy opis parametr�w rozkazu.
	Tablica Cashier_Descr zajmuje najwi�cej miejsca.
*/
	static tPARAMETER_DESCRIPTION ParamDescr[sizeof(RECADD_Descr) + sizeof(Cashier_Descr)];

/*
	BYTE				Database;
	BYTE				ModFlag;
	DWORD				RecNr;
	tDB_RECORD	DatabaseRecord;

	tDEPARTMENT_DB_RECORD:				 9
	tFORMS_OF_PAYMENT_DB_RECORD:	 3
	tDISCOUNT_DB_RECORD:					 3
	tPLU_DB_RECORD:								 9
	tBARCODE_FMT_DB_RECORD:				 1
	tCASHIER_DB_RECORD:						23
*/
	_ASSERTE( aRECADD_ParamsPtr );

/*
	Rekordy poszczeg�lnych baz maj� r�ne postaci. Trzeba dla Translate(...) przygotowa� odpowiedni opis
	struktury parametr�w rozkazu.
*/
	memcpy( ParamDescr, RECADD_Descr, sizeof(RECADD_Descr) );
	ParamDescrSize = sizeof( RECADD_Descr );

	switch( aRECADD_ParamsPtr->Database )
	{
		default:
			_ASSERTE( FALSE );
			return SERROR;						/* NO BREAK HERE ! */

		case DB_DEPARTMENT:
			memcpy( ParamDescr + ParamDescrSize, Department_Descr, sizeof(Department_Descr) );
			ParamDescrSize += sizeof( Department_Descr );
			break;

/*	Na razie nie zaimplementowane !!!
		case DB_HOT_KEYS:
			break;
		case DB_CASHIER:
			break;
*/
		case DB_FORMS:
			memcpy( ParamDescr + ParamDescrSize, FormsOfPayment_Descr, sizeof(FormsOfPayment_Descr) );
			ParamDescrSize += sizeof( FormsOfPayment_Descr );
			break;

		case DB_DISCOUNT:
			memcpy( ParamDescr + ParamDescrSize, Discount_Descr, sizeof(Discount_Descr) );
			ParamDescrSize += sizeof( Discount_Descr );
			break;

		case DB_PLU:
			memcpy( ParamDescr + ParamDescrSize, PLU_Descr, sizeof(PLU_Descr) );
			ParamDescrSize += sizeof( PLU_Descr );
			break;

		case DB_BARCODE:
			memcpy( ParamDescr + ParamDescrSize, BarcodeFmt_Descr, sizeof(BarcodeFmt_Descr) );
			ParamDescrSize += sizeof( BarcodeFmt_Descr );
			break;
	}

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, RECADD_CMD_STR, ParamDescrSize, ParamDescr,
													(BYTE *) aRECADD_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int I_RECGET( BYTE aDatabase, DWORD aRecordNr, tRECGET_RESPONSE *aRecGetResponsePtr )
{
	static int RetI;
	static tRECGET_PARAMS RecgetParams;
	static tRESPONSE ResponseStruct;

	_ASSERTE( aRecGetResponsePtr );

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, RECGET_CMD_STR, sizeof(RECGET_Descr), RECGET_Descr,
													(BYTE *) & RecgetParams );
	if( RetI != SOK)
	{
		return RetI;
	}

	switch( aDatabase )
	{
		default:
			_ASSERTE( FALSE );
			return SERROR;						/* NO BREAK HERE ! */

		case DB_DEPARTMENT:
			ResponseStruct.Description = Department_Resp_Descr;
			ResponseStruct.DescriptionLen = sizeof( Department_Resp_Descr );
			break;

/*	Na razie nie zaimplementowane !!!
		case DB_HOT_KEYS:
			break;
		case DB_CASHIER:
			break;
*/
		case DB_FORMS:
			ResponseStruct.Description = FormsOfPayment_Resp_Descr;
			ResponseStruct.DescriptionLen = sizeof( FormsOfPayment_Resp_Descr );
			break;

		case DB_DISCOUNT:
			ResponseStruct.Description = Discount_Resp_Descr;
			ResponseStruct.DescriptionLen = sizeof( Discount_Resp_Descr );
			break;

		case DB_PLU:
			ResponseStruct.Description = PLU_Resp_Descr;
			ResponseStruct.DescriptionLen = sizeof( PLU_Resp_Descr );
			break;

		case DB_BARCODE:
			ResponseStruct.Description = BarcodeFmt_Resp_Descr;
			ResponseStruct.DescriptionLen = sizeof( BarcodeFmt_Resp_Descr );
			break;
	}

	ResponseStruct.FieldsStruct = (BYTE *) aRecGetResponsePtr;
	RecgetParams.Database = aDatabase;
	RecgetParams.RecNr = aRecordNr;
	CThResp.SetResponseStructPtr( & ResponseStruct );

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int I_GETSTS( tGETSTS_RESPONSE *aGetStsResponsePtr )
{
	static int RetI;
	static tRESPONSE ResponseStruct;

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, GETSTS_CMD_STR, 0, NULL, NULL );
	if( RetI != SOK)
	{
		return RetI;
	}

	ResponseStruct.Description = GETSTS_Resp_Descr;
	ResponseStruct.DescriptionLen = sizeof( GETSTS_Resp_Descr );
	ResponseStruct.FieldsStruct = (BYTE *) aGetStsResponsePtr;
	CThResp.SetResponseStructPtr( & ResponseStruct );

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int I_GETTOT( BOOL aEuroMode, tGETTOT_RESPONSE *aGetTotResponsePtr )
{
	static int RetI;
	static tRESPONSE ResponseStruct;
	static char * StrPtr;

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, GETTOT_CMD_STR, 0, NULL, NULL );
	if( RetI != SOK)
	{
		return RetI;
	}

	ResponseStruct.Description = GETTOT_Resp_Descr;
	ResponseStruct.DescriptionLen = sizeof( GETTOT_Resp_Descr );
	ResponseStruct.FieldsStruct = (BYTE *) aGetTotResponsePtr;
	CThResp.SetResponseStructPtr( & ResponseStruct );

	RetI = Execute( & CThCmd, & CThResp );
	if( RetI != SOK )
	{
		return RetI;
	}

/*
	Zamieniamy warto�ci z formatu wewn�trznego (bez przecinka/kropki) na bardziej
	user-friendly, czyli z kropk� dziesi�tn�.

	Hmmm... Indeksy p�l s� sztywno zdefiniowane w kodzie... (zmieni� ???)
	W sumie wszystkie pola s� tego samego rodzaju, wi�c nie ma problemu.
*/

	StrPtr = ConvertNumericToAmount( aGetTotResponsePtr->TotA, aEuroMode );
	_ASSERTE( StrPtr );
	CThResp.SetFieldText( 0, StrPtr );

	StrPtr = ConvertNumericToAmount( aGetTotResponsePtr->TotB, aEuroMode );
	_ASSERTE( StrPtr );
	CThResp.SetFieldText( 1, StrPtr );

	StrPtr = ConvertNumericToAmount( aGetTotResponsePtr->TotC, aEuroMode );
	_ASSERTE( StrPtr );
	CThResp.SetFieldText( 2, StrPtr );

	StrPtr = ConvertNumericToAmount( aGetTotResponsePtr->TotD, aEuroMode );
	_ASSERTE( StrPtr );
	CThResp.SetFieldText( 3, StrPtr );

	StrPtr = ConvertNumericToAmount( aGetTotResponsePtr->TotE, aEuroMode );
	_ASSERTE( StrPtr );
	CThResp.SetFieldText( 4, StrPtr );

	StrPtr = ConvertNumericToAmount( aGetTotResponsePtr->TotF, aEuroMode );
	_ASSERTE( StrPtr );
	CThResp.SetFieldText( 5, StrPtr );

	StrPtr = ConvertNumericToAmount( aGetTotResponsePtr->TotG, aEuroMode );
	_ASSERTE( StrPtr );
	CThResp.SetFieldText( 6, StrPtr );
	
	return SOK;
}

/******************************************************************************/

int I_GetStatus_CashRegister( tI_CASH_REGISTER_STATUS *aStatusOutPtr )
{
	static BYTE RespByte;
	static int RetI;

	_ASSERTE( aStatusOutPtr );
	_ASSERTE( CThComPort.WasPortOpened() );

	CThComPort.ClearRxQueue();

	CThComPort.PortOutBuf[0] = ENQ;		/* wysy�amy ENQ */
	
	RetI = CThComPort.Write( 1, CThComPort.PortOutBuf );
	_ASSERTE( RetI == SOK );

	RetI = CThComPort.WaitForAnyByte( SHORT_RESPONSE_TIME, CThComPort.PortInBuf );
	if( RetI != SOK )
	{
		return RetI;
	}

	RespByte = CThComPort.PortInBuf[0];
	if( (RespByte & I_ENQ_RESP_MASK) != I_ENQ_RESP_SIGNATURE )	/* czy poprawny bajt odpowiedzi na ENQ ? */
	{
		return ERR_FMT_ENQ;
	}

	/* EUR FSK CMD PAR SUP */
		
	aStatusOutPtr->EUR = ( (RespByte & 0x10) != 0 );
	aStatusOutPtr->FSK = ( (RespByte & 0x08) != 0 );
	aStatusOutPtr->CMD = ( (RespByte & 0x04) != 0 );
	aStatusOutPtr->PAR = ( (RespByte & 0x02) != 0 );
	aStatusOutPtr->SUP = ( (RespByte & 0x01) != 0 );

	return SOK;
}

/******************************************************************************/

int I_GetStatus_Printer( tI_PRINTER_STATUS *aStatusOutPtr )
{
	static BYTE RespByte;
	static int RetI;

	_ASSERTE( aStatusOutPtr );
	_ASSERTE( CThComPort.WasPortOpened() );

	CThComPort.ClearRxQueue();

	CThComPort.PortOutBuf[0] = DLE;		/* wysy�amy DLE */
	
	RetI = CThComPort.Write( 1, CThComPort.PortOutBuf );
	_ASSERTE( RetI == SOK );

	RetI = CThComPort.WaitForAnyByte( SHORT_RESPONSE_TIME, CThComPort.PortInBuf );
	if( RetI != SOK )
	{
		return RetI;
	}

	RespByte = CThComPort.PortInBuf[0];
	if( (RespByte & I_DLE_RESP_MASK) != I_DLE_RESP_SIGNATURE )	/* czy poprawny bajt odpowiedzi na DLE ? */
	{
		return ERR_FMT_DLE;
	}

	/* POW LVR PAP ERR */
		
	aStatusOutPtr->POW = ( (RespByte & 0x08) != 0 );
	aStatusOutPtr->LVR = ( (RespByte & 0x04) != 0 );
	aStatusOutPtr->PAP = ( (RespByte & 0x02) != 0 );
	aStatusOutPtr->ERR = ( (RespByte & 0x01) != 0 );

	return SOK;
}

/******************************************************************************/

int I_Get_Message( BOOL aEuroMode, BYTE *aMessageTypeOutPtr, BYTE **aMessageFieldsOutPtr )
{
	static int RetI;

	static tSBHDR_MESSAGE		SBHDR_Msg;
	static tSBITEM_MESSAGE	SBITEM_Msg;
	static tSBLINE_MESSAGE	SBLINE_Msg;
	static tSBDSC_MESSAGE		SBDSC_Msg;
	static tSBEND_MESSAGE		SBEND_Msg;

	static tRESPONSE				RespStruct;

	static char * StrPtr;

#ifdef _DEBUG
	static BYTE * In, * Out;

	In = CThComPort.PortInBuf;
	Out = CThComPort.PortOutBuf;
#endif

	_ASSERTE( aMessageTypeOutPtr );
	_ASSERTE( aMessageFieldsOutPtr );
	
	RetI = CThResp.Receive( PROTOCOL_ITALIAN_REMOTE, TRUE );
	if( RetI == SOK )
	{
		if( strcmp(CThResp.GetCmdString(), SBHDR_CMD_STR) == 0 )
		{																														/* SBHDR */
			RespStruct.Description		=	SBHDR_Message_Descr;
			RespStruct.DescriptionLen	=	sizeof(SBHDR_Message_Descr);
			RespStruct.FieldsStruct		=	(BYTE *) & SBHDR_Msg;

			RetI = CThResp.CreateRespStruct( PROTOCOL_ITALIAN, & RespStruct );
			if( RetI == SOK )
			{
				*aMessageTypeOutPtr = MESSAGE_SBHDR;
				*aMessageFieldsOutPtr = (BYTE *) & SBHDR_Msg;
			}
		}
		else if( strcmp(CThResp.GetCmdString(), SBITEM_CMD_STR) == 0 )
		{																														/* SBITEM */
			RespStruct.Description		=	SBITEM_Message_Descr;
			RespStruct.DescriptionLen	=	sizeof(SBITEM_Message_Descr);
			RespStruct.FieldsStruct		=	(BYTE *) & SBITEM_Msg;

			RetI = CThResp.CreateRespStruct( PROTOCOL_ITALIAN, & RespStruct );
			if( RetI == SOK )
			{
				*aMessageTypeOutPtr = MESSAGE_SBITEM;
				*aMessageFieldsOutPtr = (BYTE *) & SBITEM_Msg;
			}
		}
		else if( strcmp(CThResp.GetCmdString(), SBLINE_CMD_STR) == 0 )
		{																														/* SBLINE */
			RespStruct.Description		=	SBLINE_Message_Descr;
			RespStruct.DescriptionLen	=	sizeof(SBLINE_Message_Descr);
			RespStruct.FieldsStruct		=	(BYTE *) & SBLINE_Msg;

			RetI = CThResp.CreateRespStruct( PROTOCOL_ITALIAN, & RespStruct );
			if( RetI == SOK )
			{
				*aMessageTypeOutPtr = MESSAGE_SBLINE;
/*
				Indeksy zamienianych p�l s� wpisane 'na sztywno'
*/
				StrPtr = ConvertNumericToAmount( SBLINE_Msg.Price, aEuroMode );
				_ASSERTE( StrPtr );
				CThResp.SetFieldText( 3, StrPtr );

				StrPtr = ConvertNumericToQuantity( SBLINE_Msg.Quantity );
				_ASSERTE( StrPtr );
				CThResp.SetFieldText( 4, StrPtr );

				*aMessageFieldsOutPtr = (BYTE *) & SBLINE_Msg;
			}
		}
		else if( strcmp(CThResp.GetCmdString(), SBDSC_CMD_STR) == 0 )
		{																														/* SBDSC */
			RespStruct.Description		=	SBDSC_Message_Descr;
			RespStruct.DescriptionLen	=	sizeof(SBDSC_Message_Descr);
			RespStruct.FieldsStruct		=	(BYTE *) & SBDSC_Msg;

			RetI = CThResp.CreateRespStruct( PROTOCOL_ITALIAN, & RespStruct );
			if( RetI == SOK )
			{
				*aMessageTypeOutPtr = MESSAGE_SBDSC;

				StrPtr = ConvertNumericToAmount( SBDSC_Msg.DscAmt, aEuroMode );
				_ASSERTE( StrPtr );
				CThResp.SetFieldText( 1, StrPtr );

				*aMessageFieldsOutPtr = (BYTE *) & SBDSC_Msg;
			}
		}
		else if( strcmp(CThResp.GetCmdString(), SBEND_CMD_STR) == 0 )
		{																														/* SBEND */
			RespStruct.Description		=	SBEND_Message_Descr;
			RespStruct.DescriptionLen	=	sizeof(SBEND_Message_Descr);
			RespStruct.FieldsStruct		=	(BYTE *) & SBEND_Msg;

			RetI = CThResp.CreateRespStruct( PROTOCOL_ITALIAN, & RespStruct );
			if( RetI == SOK )
			{
				*aMessageTypeOutPtr = MESSAGE_SBEND;
				
				StrPtr = ConvertNumericToAmount( SBEND_Msg.Total, aEuroMode );
				_ASSERTE( StrPtr );
				CThResp.SetFieldText( 0, StrPtr );

				StrPtr = ConvertNumericToAmount( SBEND_Msg.Cash, aEuroMode );
				_ASSERTE( StrPtr );
				CThResp.SetFieldText( 1, StrPtr );

				StrPtr = ConvertNumericToAmount( SBEND_Msg.DscAmt, aEuroMode );
				_ASSERTE( StrPtr );
				CThResp.SetFieldText( 3, StrPtr );

				StrPtr = ConvertNumericToAmount( SBEND_Msg.Fp1, aEuroMode );
				_ASSERTE( StrPtr );
				CThResp.SetFieldText( 4, StrPtr );

				StrPtr = ConvertNumericToAmount( SBEND_Msg.Fp2, aEuroMode );
				_ASSERTE( StrPtr );
				CThResp.SetFieldText( 5, StrPtr );

				StrPtr = ConvertNumericToAmount( SBEND_Msg.Fp3, aEuroMode );
				_ASSERTE( StrPtr );
				CThResp.SetFieldText( 6, StrPtr );

				StrPtr = ConvertNumericToAmount( SBEND_Msg.Fp4, aEuroMode );
				_ASSERTE( StrPtr );
				CThResp.SetFieldText( 7, StrPtr );

				*aMessageFieldsOutPtr = (BYTE *) & SBEND_Msg;
			}
		}
		else if( strcmp(CThResp.GetCmdString(), SBEXIT_CMD_STR) == 0 )
		{																														/* SBEXIT */
			/* SBEXIT nie posiada �adnych parametr�w */
			*aMessageTypeOutPtr = MESSAGE_SBEXIT;
			*aMessageFieldsOutPtr = NULL;		/* no, niech tylko kto� nie pr�buje odczyta�... */
		} 
		else
		{
			return SERROR;
			/* KASZANA */
		}
	}
	
	return RetI;
}

/******************************************************************************/

int I_S_TRSITEM( tS_TRSITEM_PARAMS *aTRSITEM_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aTRSITEM_ParamsPtr );

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, S_TRSITEM_CMD_STR, sizeof(S_TRSITEM_Descr), S_TRSITEM_Descr,
													(BYTE *) aTRSITEM_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int I_S_TRSDSC( tS_TRSDSC_PARAMS *aTRSDSC_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aTRSDSC_ParamsPtr );

	RetI = CThCmd.Translate( PROTOCOL_ITALIAN, S_TRSDSC_CMD_STR, sizeof(S_TRSDSC_Descr), S_TRSDSC_Descr,
													(BYTE *) aTRSDSC_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}


	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/*******************************************************************************
**************  PROTOKӣ POLSKI - STARA I NOWA HOMOLOGACJA  ********************
**************     (rozkazy starej hom. maj� prefiks O_)    ********************
*******************************************************************************/

int GetStatus_CashRegister( tCASH_REGISTER_STATUS *aStatusOutPtr )
{
	static BYTE RespByte;
	static int RetI;

	_ASSERTE( aStatusOutPtr );
	_ASSERTE( CThComPort.WasPortOpened() );

	CThComPort.ClearRxQueue();

	CThComPort.PortOutBuf[0] = ENQ;		/* wysy�amy ENQ */
	
	RetI = CThComPort.Write( 1, CThComPort.PortOutBuf );
	_ASSERTE( RetI == SOK );

	RetI = CThComPort.WaitForAnyByte( SHORT_RESPONSE_TIME, CThComPort.PortInBuf );
	if( RetI != SOK )
	{
		return RetI;
	}

	RespByte = CThComPort.PortInBuf[0];
	if( (RespByte & ENQ_RESP_MASK) != ENQ_RESP_SIGNATURE )	/* czy poprawny bajt odpowiedzi na ENQ ? */
	{
		return ERR_FMT_ENQ;
	}

	/* FSK CMD PAR TRF */
	aStatusOutPtr->FSK = ( (RespByte & 0x08) != 0 );
	aStatusOutPtr->CMD = ( (RespByte & 0x04) != 0 );
	aStatusOutPtr->PAR = ( (RespByte & 0x02) != 0 );
	aStatusOutPtr->TRF = ( (RespByte & 0x01) != 0 );

	CThComPort.LogFlush();

	return SOK;
}

/******************************************************************************/

int GetStatus_Printer( tPRINTER_STATUS *aStatusOutPtr )
{
	static BYTE RespByte;
	static int RetI;

	_ASSERTE( aStatusOutPtr );
	_ASSERTE( CThComPort.WasPortOpened() );

	CThComPort.ClearRxQueue();

	CThComPort.PortOutBuf[0] = DLE;		/* wysy�amy DLE */

	RetI = CThComPort.Write( 1, CThComPort.PortOutBuf );
	_ASSERTE( RetI == SOK );

	RetI = CThComPort.WaitForAnyByte( SHORT_RESPONSE_TIME, CThComPort.PortInBuf );
	if( RetI != SOK )
	{
		return RetI;
	}

	RespByte = CThComPort.PortInBuf[0];
	if( (RespByte & DLE_RESP_MASK) != DLE_RESP_SIGNATURE )		/* czy poprawny bajt odpowiedzi na DLE ? */
	{
		return ERR_FMT_DLE;
	}

	/* ONL PE ERR */
	aStatusOutPtr->ONL = ( (RespByte & 0x04) != 0 );
	aStatusOutPtr->PE  = ( (RespByte & 0x02) != 0 );
	aStatusOutPtr->ERR = ( (RespByte & 0x01) != 0 );

	CThComPort.LogFlush();

	return SOK;
}

/******************************************************************************/

int LBSETCK( tLBSETCK_PARAMS *aLBSETCK_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aLBSETCK_ParamsPtr );

/*
	Albo oba (CashRegNr i Cashier) s�, albo ich nie ma...
*/
	if( (aLBSETCK_ParamsPtr->CashRegNr != NULL) != (aLBSETCK_ParamsPtr->Cashier != NULL) )
	{
		return ERR_OPT_PARAMS;
	}

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBSETCK_CMD_STR,
														sizeof(LBSETCK_Descr), LBSETCK_Descr, (BYTE *) aLBSETCK_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int LBDSP( BYTE aMode, char *aString )
{
	static int RetI;
	static tLBDSP_PARAMS LBDSPparams;

	LBDSPparams.Ps = aMode;
	LBDSPparams.String = aString;

	RetI = CThCmd.Translate( PROTOCOL_OLD_NCHK_RESP_NCHK, LBDSP_CMD_STR,
														sizeof(LBDSP_Descr), LBDSP_Descr, (BYTE *) & LBDSPparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

/*
	LBSETPTU -
	Nie obs�uguj� Ps r�wnego zero bo to jest to samo co Ps == 4.
*/

int LBSETPTU( tLBSETPTU_PARAMS *aLBSETPTU_ParamsPtr )
{
	static int RetI;
	static BYTE Date[3], Offending;
	static tLBSETPTU_PARAMS_INT LBSETPTUparams;

	_ASSERTE( aLBSETPTU_ParamsPtr );

/*
	Parametry Cashier i CashRegNr - je�li maj� wyst�powa�, to musz� by� oba.
*/
	if( (aLBSETPTU_ParamsPtr->Cashier != NULL) != (aLBSETPTU_ParamsPtr->CashRegNr != NULL) )
	{
		return ERR_OPT_PARAMS;
	}

	LBSETPTUparams.Cashier = aLBSETPTU_ParamsPtr->Cashier;
	LBSETPTUparams.CashRegNr = aLBSETPTU_ParamsPtr->CashRegNr;
	LBSETPTUparams.Ps = aLBSETPTU_ParamsPtr->Ps;
	LBSETPTUparams.Ps_2 = aLBSETPTU_ParamsPtr->Ps;
	LBSETPTUparams.VatRates = aLBSETPTU_ParamsPtr->VatRates;

	if( aLBSETPTU_ParamsPtr->_Date )
	{
		if( ! CheckDate(aLBSETPTU_ParamsPtr->Py, aLBSETPTU_ParamsPtr->Pm,
										aLBSETPTU_ParamsPtr->Pd, & Offending) )
		{
			return ( ERR_PARAM_VALUE | (Offending +1) );	// +1 bo jest jeszcze Ps
		}																								
		else
		{
			Date[0] = aLBSETPTU_ParamsPtr->Py;
			Date[1] = aLBSETPTU_ParamsPtr->Pm;
			Date[2] = aLBSETPTU_ParamsPtr->Pd;
		
			LBSETPTUparams._Date = 3;
			LBSETPTUparams._DateTab = Date;
		}
	}
	else
	{
		LBSETPTUparams._Date = 0;
		LBSETPTUparams._DateTab = NULL;
	}

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBSETPTU_CMD_STR,
														sizeof(LBSETPTU_Descr), LBSETPTU_Descr, (BYTE *) & LBSETPTUparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int LBSETHDR( char *aHdrString, char *aCashRegNr, char *aCashier )
{
	static int RetI;
	static tLBSETHDR_PARAMS_INT LBSETHDRparams;

	_ASSERTE( aHdrString );

/*
	Albo oba (CashRegNr i Cashier) s�, albo ich nie ma...
*/
	if( (aCashRegNr != NULL) != (aCashier != NULL) )
	{
		return ERR_OPT_PARAMS;
	}

	LBSETHDRparams._dummy = 0;	/* parametr - wype�niacz */
	LBSETHDRparams.HdrString	= aHdrString;
	LBSETHDRparams.CashRegNr	= aCashRegNr;
	LBSETHDRparams.Cashier		= aCashier;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBSETHDR_CMD_STR,
														sizeof(LBSETHDR_Descr), LBSETHDR_Descr, (BYTE *) & LBSETHDRparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int LBFEED( BYTE aNrOfLines )
{
	static int RetI;
	static tLBFEED_PARAMS LBFEEDparams;

	LBFEEDparams.Ps = aNrOfLines;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBFEED_CMD_STR,
														sizeof(LBFEED_Descr), LBFEED_Descr, (BYTE *) & LBFEEDparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int LBSERM( BYTE aMode )
{
	static int RetI;
	static tLBSERM_PARAMS_INT LBSERMparams;

	LBSERMparams.Ps = aMode;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBSERM_CMD_STR,
														sizeof(LBSERM_Descr), LBSERM_Descr, (BYTE *) & LBSERMparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int LBTRSHDR( BYTE aNrOfLines )
{
	static int RetI;
	static tLBTRSHDR_PARAMS_INT LBTRSHDRparams;

	LBTRSHDRparams.Pl = aNrOfLines;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBTRSHDR_CMD_STR,
		      	sizeof(LBTRSHDR_Descr), LBTRSHDR_Descr, (BYTE *) & LBTRSHDRparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

// w celu zapewnienia zgodno�ci ze star� bibliotek�
int LBTRSHDR_A( tLBTRSHDR_A_PARAMS *aLBTRSHDR_ParamsPtr )
{
	static int RetI;

	switch( aLBTRSHDR_ParamsPtr->Pn )
	{
		case 0:
			aLBTRSHDR_ParamsPtr->Line1 = NULL;
		case 1:
			aLBTRSHDR_ParamsPtr->Line2 = NULL;
		case 2:
			aLBTRSHDR_ParamsPtr->Line3 = NULL;
	}

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBTRSHDR_CMD_STR,
														sizeof(LBTRSHDR_A_Descr), LBTRSHDR_A_Descr, (BYTE *) aLBTRSHDR_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}


/******************************************************************************/

/*
	Stare LBTRSLN w przypadku, gdy Pr==0 (brak rabatu) nie dopuszcza wyst�powania
	pola RABAT (nie mo�e wyst�pi� nawet jego terminator). Czyli inaczej ni� w
	wersji nowej, w kt�rej co� tam musi by� (i jest wpisywany ci�g "0" );
*/

int LBTRSLN_COMMON( tLBTRSLN_PARAMS *aLBTRSLN_ParamsPtr, BOOL aNewVersion )
{
	static int RetI;
	static tLBTRSLN_PARAMS_INT LBTRSLNparams;
	static char VatStr[2];

/*
typedef struct 
{
	BYTE								Pi;
	BYTE								Pr;
	char *							Name;
	tEXTERNAL_NUMERIC		Quantity;
	char *							Vat;				<---- niestandardowy separator (uwzgl�dnione w LBTRSLN_Descr[] )
	tEXTERNAL_NUMERIC		Price;
	tEXTERNAL_NUMERIC		Gross;
	tEXTERNAL_NUMERIC		Discount;
} tLBTRSLN_PARAMS_INT;

typedef struct 
{
	BYTE								Pi;
	BYTE								Pr;
	char *							Name;
	tEXTERNAL_NUMERIC		Quantity;
	char								Vat;
	tEXTERNAL_NUMERIC		Price;
	tEXTERNAL_NUMERIC		Gross;
	tEXTERNAL_NUMERIC		Discount;
} tLBTRSLN_PARAMS;
*/

	_ASSERTE( aLBTRSLN_ParamsPtr );

	VatStr[0] = aLBTRSLN_ParamsPtr->Vat;
	VatStr[1] = 0;
	
	LBTRSLNparams.Pi				= aLBTRSLN_ParamsPtr->Pi;
	LBTRSLNparams.Pr				= aLBTRSLN_ParamsPtr->Pr;
	LBTRSLNparams.Name			= aLBTRSLN_ParamsPtr->Name;
	LBTRSLNparams.Quantity	= aLBTRSLN_ParamsPtr->Quantity;
	LBTRSLNparams.Vat				= VatStr;
	LBTRSLNparams.Price			= aLBTRSLN_ParamsPtr->Price;
	LBTRSLNparams.Gross			= aLBTRSLN_ParamsPtr->Gross;

	if( LBTRSLNparams.Pr == LBTRSLN_NO_DISCNT )
	{
		LBTRSLNparams.Discount.TypeTranslFuncPtr = NULL;

		if( aNewVersion )
		{
			LBTRSLNparams.Discount.ParamValuePtr = "0";
		}
		else
		{
			LBTRSLNparams.Discount.ParamValuePtr = NULL;
		}
	}
	else
	{
		LBTRSLNparams.Discount = aLBTRSLN_ParamsPtr->Discount;
	}


	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBTRSLN_CMD_STR,
														sizeof(LBTRSLN_Descr), LBTRSLN_Descr, (BYTE *) & LBTRSLNparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/*----------------------------------------------------------------------------*/

int LBTRSLN( tLBTRSLN_PARAMS *aLBTRSLN_ParamsPtr )
{
	return LBTRSLN_COMMON( aLBTRSLN_ParamsPtr, TRUE );
}

/*----------------------------------------------------------------------------*/

int O_LBTRSLN( tLBTRSLN_PARAMS *aLBTRSLN_ParamsPtr )
{
	return LBTRSLN_COMMON( aLBTRSLN_ParamsPtr, FALSE );
}


/******************************************************************************/

/*
	LBDEP obs�uguje nast�puj�ce rozkazy:
	LBDEP_P
	LBDEP_M
	LBDEPSTR_P
	LBDEPSTR_M
*/

int LBDEP( BYTE aMode, tLBDEP_PARAMS *aLBDEP_ParamsPtr )
{
	static int RetI;
	static tLBDEP_PARAMS_INT LBDEPparams;

	_ASSERTE( aLBDEP_ParamsPtr );

/*
	BYTE								Mode;
	tEXTERNAL_NUMERIC		Amount;
	char *							Number;
	tEXTERNAL_NUMERIC		Quantity;
*/

/*
	Albo oba (Number, Quantity) s�, albo ich nie ma...
*/
	if( (aLBDEP_ParamsPtr->Number != NULL) != (aLBDEP_ParamsPtr->Quantity.ParamValuePtr != NULL) )
	{
		return ERR_OPT_PARAMS;
	}

	LBDEPparams.Mode = aMode;
	LBDEPparams.Amount = aLBDEP_ParamsPtr->Amount;
	LBDEPparams.Number = aLBDEP_ParamsPtr->Number;
	LBDEPparams.Quantity= aLBDEP_ParamsPtr->Quantity;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBDEP_CMD_STR,
														sizeof(LBDEP_Descr), LBDEP_Descr, (BYTE *) & LBDEPparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/*----------------------------------------------------------------------------*/

int LBDEP_P( tLBDEP_PARAMS *aLBDEP_ParamsPtr )
{
	return LBDEP( LBDEP_MODE_P, aLBDEP_ParamsPtr );
}

/*----------------------------------------------------------------------------*/

int LBDEPSTR_P( tLBDEP_PARAMS *aLBDEP_ParamsPtr )
{
	return LBDEP( LBDEP_MODE_STRP, aLBDEP_ParamsPtr );
}

/*----------------------------------------------------------------------------*/

int LBDEP_M( tLBDEP_PARAMS *aLBDEP_ParamsPtr )
{
	return LBDEP( LBDEP_MODE_M, aLBDEP_ParamsPtr );
}

/*----------------------------------------------------------------------------*/

int LBDEPSTR_M( tLBDEP_PARAMS *aLBDEP_ParamsPtr )
{
	return LBDEP( LBDEP_MODE_STRM, aLBDEP_ParamsPtr );
}

/******************************************************************************/

int LBTREXITCAN( char *aCashRegNr, char *aCashier )
{
	static int RetI;
	static tLBTREXITCAN_PARAMS_INT LBTREXITCANparams;

/*
	BYTE		_dummy;
	char *	CashRegNr;
	char *	Cashier;
*/

/*
	CashRegNr i Cashier - albo oba s�, albo nie ma �adnego
*/
	if( (aCashRegNr != NULL ) != (aCashier != NULL ) )
	{
		return ERR_OPT_PARAMS;
	}

	LBTREXITCANparams._dummy		= 0;
	LBTREXITCANparams.CashRegNr	= aCashRegNr;
	LBTREXITCANparams.Cashier		= aCashier;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBTREXITCAN_CMD_STR,
														sizeof(LBTREXITCAN_Descr), LBTREXITCAN_Descr, (BYTE *) & LBTREXITCANparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int LBTREXIT( tLBTREXIT_PARAMS *aLBTREXIT_PARAMS )
{
	static int RetI;

	_ASSERTE( aLBTREXIT_PARAMS );

	switch( aLBTREXIT_PARAMS->Pn )
	{
		case 0:
			aLBTREXIT_PARAMS->Line1 = (char *) Empty;	// NO BREAK HERE !!!
		case 1:
			aLBTREXIT_PARAMS->Line2 = (char *) Empty;	// NO BREAK HERE !!!
		case 2:
			aLBTREXIT_PARAMS->Line3 = (char *) Empty;	// NO BREAK HERE !!!
	}

	if( aLBTREXIT_PARAMS->Px == 0 )
	{
		aLBTREXIT_PARAMS->Discount.ParamValuePtr	= (LPVOID) Zero;
		aLBTREXIT_PARAMS->Discount.TypeTranslFuncPtr	= NULL;
	}

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBTREXIT_CMD_STR,
			       	sizeof(LBTREXIT_Descr), LBTREXIT_Descr, (BYTE *) aLBTREXIT_PARAMS );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

int LBTREXIT_A( tLBTREXIT_A_PARAMS *aLBTREXIT_PARAMS )
{
	static int RetI;

	_ASSERTE( aLBTREXIT_PARAMS );

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBTREXIT_CMD_STR,
														sizeof(LBTREXIT_A_Descr), LBTREXIT_A_Descr, (BYTE *) aLBTREXIT_PARAMS );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

int LBTREXIT_B( tLBTREXIT_B_PARAMS *aLBTREXIT_PARAMS )
{
	static int RetI;

	_ASSERTE( aLBTREXIT_PARAMS );

	switch( aLBTREXIT_PARAMS->Pn )
	{
		case 0:
			aLBTREXIT_PARAMS->Line1 = NULL;	// NO BREAK HERE !!!
		case 1:
			aLBTREXIT_PARAMS->Line2 = NULL;	// NO BREAK HERE !!!
		case 2:
			aLBTREXIT_PARAMS->Line3 = NULL;	// NO BREAK HERE !!!
	}

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBTREXIT_CMD_STR,
														sizeof(LBTREXIT_B_Descr), LBTREXIT_B_Descr, (BYTE *) aLBTREXIT_PARAMS );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

int LBTREXIT_C( tLBTREXIT_PARAMS *aLBTREXIT_PARAMS )
{
	static int RetI;

	_ASSERTE( aLBTREXIT_PARAMS );

	switch( aLBTREXIT_PARAMS->Pn )
	{
		case 0:
			aLBTREXIT_PARAMS->Line1 = NULL;	// NO BREAK HERE !!!
		case 1:
			aLBTREXIT_PARAMS->Line2 = NULL;	// NO BREAK HERE !!!
		case 2:
			aLBTREXIT_PARAMS->Line3 = NULL;	// NO BREAK HERE !!!
	}

	if( aLBTREXIT_PARAMS->Px == 0 )	// nie b�dzie pola 'Rabat' !!!
	{
		aLBTREXIT_PARAMS->Discount.ParamValuePtr	= NULL;
		aLBTREXIT_PARAMS->Discount.TypeTranslFuncPtr	= NULL;
	}

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBTREXIT_CMD_STR,
			       	sizeof(LBTREXIT_Descr), LBTREXIT_Descr, (BYTE *) aLBTREXIT_PARAMS );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}


/*----------------------------------------------------------------------------*/

int O_LBTREXIT()
{
	static int RetI;
	static tO_LBTREXIT_PARAMS_INT LBTREXITparams;

/*
	BYTE		_dummy;
*/
	LBTREXITparams._dummy		= 0;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBTREXIT_CMD_STR,
			  	sizeof(O_LBTREXIT_Descr), O_LBTREXIT_Descr, (BYTE *) & LBTREXITparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int O_LBTRXEND( tO_LBTRXEND_PARAMS *aLBTRXEND_ParamsPtr )
{
	static int RetI;
	static tO_LBTRXEND_PARAMS_INT LBTRXENDparams;

	static char * LinePtr[LBTRXEND_MAX_ADD_LINES];

	static DWORD NrOfLines, i;

	_ASSERTE( aLBTRXEND_ParamsPtr );

/*
typedef struct
{
	BYTE								Pn;
	BYTE								Pc;
	BYTE								Py;
	BYTE								Px;
	BYTE								Pg;
	BYTE								Pk;
	BYTE								Pz;
	BYTE								Pb;
	BYTE								Po1;
	BYTE								Po2;
	BYTE								Pr;
	char *							Code;
	DWORD								Pn_1;
	char **							Line;
	char *							CardName;
	char *							ChequeName;
	char *							CouponName;
	tEXTERNAL_NUMERIC		Total;
	tEXTERNAL_NUMERIC		Discount;
	tEXTERNAL_NUMERIC		Cash;
	tEXTERNAL_NUMERIC		Card;
	tEXTERNAL_NUMERIC		Cheque;
	tEXTERNAL_NUMERIC		Coupon;
	tEXTERNAL_NUMERIC		DepositTaken;
	tEXTERNAL_NUMERIC		DepositReturned;
	tEXTERNAL_NUMERIC		Rest;
} tLBTRXEND_PARAMS_INT;

typedef struct
{
	BYTE								Pn;
	BYTE								Pc;
	BYTE								Py;
	BYTE								Px;
	BYTE								Pg;
	BYTE								Pk;
	BYTE								Pz;
	BYTE								Pb;
	BYTE								Po1;
	BYTE								Po2;
	BYTE								Pr;
	char *							Code;
	char **							Line;
	char *							CardName;
	char *							ChequeName;
	char *							CouponName;
	tEXTERNAL_NUMERIC		Total;
	tEXTERNAL_NUMERIC		Discount;
	tEXTERNAL_NUMERIC		Cash;
	tEXTERNAL_NUMERIC		Card;
	tEXTERNAL_NUMERIC		Cheque;
	tEXTERNAL_NUMERIC		Coupon;
	tEXTERNAL_NUMERIC		DepositTaken;
	tEXTERNAL_NUMERIC		DepositReturned;
	tEXTERNAL_NUMERIC		Rest;
} tLBTRXEND_PARAMS;
*/
	LBTRXENDparams.Pn		= aLBTRXEND_ParamsPtr->Pn;
	LBTRXENDparams.Pc		= aLBTRXEND_ParamsPtr->Pc;
	LBTRXENDparams.Py		= aLBTRXEND_ParamsPtr->Py;
	LBTRXENDparams.Px		= aLBTRXEND_ParamsPtr->Px;
	LBTRXENDparams.Pg		= aLBTRXEND_ParamsPtr->Pg;
	LBTRXENDparams.Pk		= aLBTRXEND_ParamsPtr->Pk;
	LBTRXENDparams.Pz		= aLBTRXEND_ParamsPtr->Pz;
	LBTRXENDparams.Pb		= aLBTRXEND_ParamsPtr->Pb;
	LBTRXENDparams.Po1	= aLBTRXEND_ParamsPtr->Po1;
	LBTRXENDparams.Po2	= aLBTRXEND_ParamsPtr->Po2;
	LBTRXENDparams.Pr		= aLBTRXEND_ParamsPtr->Pr;

	LBTRXENDparams.Code	= aLBTRXEND_ParamsPtr->Code;	/* KOD jest zawsze */

	NrOfLines = LBTRXENDparams.Pn;
	for( i = 0; i < NrOfLines; i++ )
	{
		LinePtr[i] = aLBTRXEND_ParamsPtr->Line[i];
	}

	for( i = NrOfLines; i < LBTRXEND_MAX_ADD_LINES; i++ )
	{
		LinePtr[i] = (char *) Empty;
	}

	LBTRXENDparams.Line	= LinePtr;
	LBTRXENDparams.Pn_1	= LBTRXEND_MAX_ADD_LINES;

	if( LBTRXENDparams.Pk == 1 )
	{
		LBTRXENDparams.CardName	= aLBTRXEND_ParamsPtr->CardName;
		LBTRXENDparams.Card			= aLBTRXEND_ParamsPtr->Card;
	}
	else
	{
		LBTRXENDparams.CardName								= (char *) Empty;
		LBTRXENDparams.Card.ParamValuePtr			= (char *) Zero;
		LBTRXENDparams.Card.TypeTranslFuncPtr	= NULL;
	}

	if( LBTRXENDparams.Pz == 1 )
	{
		LBTRXENDparams.ChequeName	= aLBTRXEND_ParamsPtr->ChequeName;
		LBTRXENDparams.Cheque			= aLBTRXEND_ParamsPtr->Cheque;
	}
	else
	{
		LBTRXENDparams.ChequeName								= (char *) Empty;
		LBTRXENDparams.Cheque.ParamValuePtr			= (char *) Zero;
		LBTRXENDparams.Cheque.TypeTranslFuncPtr	= NULL;
	}

	if( LBTRXENDparams.Pb == 1 )
	{
		LBTRXENDparams.CouponName	= aLBTRXEND_ParamsPtr->CouponName;
		LBTRXENDparams.Coupon			= aLBTRXEND_ParamsPtr->Coupon;
	}
	else
	{
		LBTRXENDparams.CouponName								= (char *) Empty;
		LBTRXENDparams.Coupon.ParamValuePtr			= (char *) Zero;
		LBTRXENDparams.Coupon.TypeTranslFuncPtr	= NULL;
	}

	LBTRXENDparams.Total = aLBTRXEND_ParamsPtr->Total;	/* TOTAL jest zawsze */

	if( LBTRXENDparams.Px == 1 )
	{
		LBTRXENDparams.Discount = aLBTRXEND_ParamsPtr->Discount;
	}
	else
	{
		LBTRXENDparams.Discount.ParamValuePtr			= (char *) Zero;
		LBTRXENDparams.Discount.TypeTranslFuncPtr	= NULL;
	}

	if( LBTRXENDparams.Pg == 1 )
	{
		LBTRXENDparams.Cash = aLBTRXEND_ParamsPtr->Cash;
	}
	else
	{
		LBTRXENDparams.Cash.ParamValuePtr			= (char *) Zero;
		LBTRXENDparams.Cash.TypeTranslFuncPtr	= NULL;
	}

	if( LBTRXENDparams.Po1 == 1 )
	{
		LBTRXENDparams.DepositTaken = aLBTRXEND_ParamsPtr->DepositTaken;
	}
	else
	{
		LBTRXENDparams.DepositTaken.ParamValuePtr			= (char *) Zero;
		LBTRXENDparams.DepositTaken.TypeTranslFuncPtr	= NULL;
	}

	if( LBTRXENDparams.Po2 == 1 )
	{
		LBTRXENDparams.DepositReturned = aLBTRXEND_ParamsPtr->DepositReturned;
	}
	else
	{
		LBTRXENDparams.DepositReturned.ParamValuePtr			= (char *) Zero;
		LBTRXENDparams.DepositReturned.TypeTranslFuncPtr	= NULL;
	}


	if( LBTRXENDparams.Pr == 1 )
	{
		LBTRXENDparams.Rest = aLBTRXEND_ParamsPtr->Rest;
	}
	else
	{
		LBTRXENDparams.Rest.ParamValuePtr			= (char *) Zero;
		LBTRXENDparams.Rest.TypeTranslFuncPtr	= NULL;
	}

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBTRXEND_CMD_STR,
														sizeof(O_LBTRXEND_Descr), O_LBTRXEND_Descr, (BYTE *) & LBTRXENDparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int LBTRFORMPLAT( tLBTRFORMPLAT_PARAMS *aLBTRFORMPLAT_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aLBTRFORMPLAT_ParamsPtr );

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBTRFORMPLAT_CMD_STR,
														sizeof(LBTRFORMPLAT_Descr), LBTRFORMPLAT_Descr, (BYTE *) aLBTRFORMPLAT_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int LBTRXEND1( tLBTRXEND1_PARAMS *aLBTRXEND1_ParamsPtr )
{
	static int RetI;
	static tLBTRXEND1_PARAMS_INT LBTRXEND1params;

	_ASSERTE( aLBTRXEND1_ParamsPtr );

/*
typedef struct
{
	BYTE									Pn;
	BYTE									Pc;
	BYTE									Py;
	BYTE									Pdsp;
	BYTE									Px;
	BYTE									Pkb;
	BYTE									Pkz;
	BYTE									Pns;
	BYTE									Pfn;
	BYTE									Pr;
	BYTE									Pg;
	DWORD									Pfn_1;
	BYTE *								Pfx;
	char *								CashRegNr;
	char *								Cashier;
	char *								SystemNr;
	DWORD									Pn_1;
	char **								Line;
	DWORD									Pfn_2;
	char **								FormOfPaymentName;
	DWORD									Pkb_1;
	char **								DepositTakenNr;
	DWORD									Pkb_2;
	tEXTERNAL_NUMERIC *		DepositTakenQuantity;
	DWORD									Pkz_1;
	char **								DepositReturnedNr;
	DWORD									Pkz_2;
	tEXTERNAL_NUMERIC *		DepositReturnedQuantity;
	tEXTERNAL_NUMERIC			Total;
	tEXTERNAL_NUMERIC			Dsp;
	tEXTERNAL_NUMERIC			Discount;
	tEXTERNAL_NUMERIC			Cash;
	DWORD									Pfn_3;
	tEXTERNAL_NUMERIC *		FormOfPaymentAmount;
	tEXTERNAL_NUMERIC			Rest;
	DWORD									Pkb_3;
	tEXTERNAL_NUMERIC *		DepositTakenAmount;
	DWORD									Pkz_3;
	tEXTERNAL_NUMERIC *		DepositReturnedAmount;
} tLBTRXEND1_PARAMS_INT;

typedef struct
{
	BYTE									Pc;
	BYTE									Py;
	char *								CashRegNr;
	char *								Cashier;
	BYTE									Pns;
	char *								SystemNr;
	BYTE									Pn;
	char **								Line;
	BYTE									Pkb;
	char **								DepositTakenNr;
	tEXTERNAL_NUMERIC *		DepositTakenQuantity;
	tEXTERNAL_NUMERIC *		DepositTakenAmount;
	BYTE									Pkz;
	char **								DepositReturnedNr;
	tEXTERNAL_NUMERIC *		DepositReturnedQuantity;
	tEXTERNAL_NUMERIC *		DepositReturnedAmount;
	BYTE									Pfn;
	BYTE *								Pfx;
	char **								FormOfPaymentName;
	tEXTERNAL_NUMERIC *		FormOfPaymentAmount;
	tEXTERNAL_NUMERIC			Total;
	BYTE									Pdsp;
	tEXTERNAL_NUMERIC			Dsp;
	BYTE									Px;
	tEXTERNAL_NUMERIC			Discount;
	BYTE									Pg;
	tEXTERNAL_NUMERIC			Cash;
	BYTE									Pr;
	tEXTERNAL_NUMERIC			Rest;
} tLBTRXEND1_PARAMS;
*/

	LBTRXEND1params.Pn										= aLBTRXEND1_ParamsPtr->Pn;
	LBTRXEND1params.Pc										= aLBTRXEND1_ParamsPtr->Pc;
	LBTRXEND1params.Py										= aLBTRXEND1_ParamsPtr->Py;
	LBTRXEND1params.Pdsp									= aLBTRXEND1_ParamsPtr->Pdsp;
	LBTRXEND1params.Px										= aLBTRXEND1_ParamsPtr->Px;
	LBTRXEND1params.Pkb										= aLBTRXEND1_ParamsPtr->Pkb;
	LBTRXEND1params.Pkz										= aLBTRXEND1_ParamsPtr->Pkz;
	LBTRXEND1params.Pns										= aLBTRXEND1_ParamsPtr->Pns;
	LBTRXEND1params.Pfn										= aLBTRXEND1_ParamsPtr->Pfn;
	LBTRXEND1params.Pr										= aLBTRXEND1_ParamsPtr->Pr;
	LBTRXEND1params.Pg										= aLBTRXEND1_ParamsPtr->Pg;
	LBTRXEND1params.Pfn_1									= aLBTRXEND1_ParamsPtr->Pfn;
	LBTRXEND1params.Pfx										= aLBTRXEND1_ParamsPtr->Pfx;

	LBTRXEND1params.CashRegNr							= aLBTRXEND1_ParamsPtr->CashRegNr;
	LBTRXEND1params.Cashier								= aLBTRXEND1_ParamsPtr->Cashier;
	LBTRXEND1params.SystemNr							= aLBTRXEND1_ParamsPtr->SystemNr;
	LBTRXEND1params.Pn_1									= aLBTRXEND1_ParamsPtr->Pn;
	LBTRXEND1params.Line									= aLBTRXEND1_ParamsPtr->Line;
	LBTRXEND1params.Pfn_2									= aLBTRXEND1_ParamsPtr->Pfn;
	LBTRXEND1params.FormOfPaymentName			= aLBTRXEND1_ParamsPtr->FormOfPaymentName;
	LBTRXEND1params.Pkb_1									= aLBTRXEND1_ParamsPtr->Pkb;
	LBTRXEND1params.DepositTakenNr				= aLBTRXEND1_ParamsPtr->DepositTakenNr;
	LBTRXEND1params.Pkb_2									= aLBTRXEND1_ParamsPtr->Pkb;
	LBTRXEND1params.DepositTakenQuantity	= aLBTRXEND1_ParamsPtr->DepositTakenQuantity;
	LBTRXEND1params.Pkz_1									= aLBTRXEND1_ParamsPtr->Pkz;
	LBTRXEND1params.DepositReturnedNr			= aLBTRXEND1_ParamsPtr->DepositReturnedNr;
	LBTRXEND1params.Pkz_2									= aLBTRXEND1_ParamsPtr->Pkz;
	LBTRXEND1params.DepositReturnedQuantity	= aLBTRXEND1_ParamsPtr->DepositReturnedQuantity;
	LBTRXEND1params.Total									= aLBTRXEND1_ParamsPtr->Total;
	LBTRXEND1params.Dsp										= aLBTRXEND1_ParamsPtr->Dsp;

	if( LBTRXEND1params.Px != 0 )
	{
		LBTRXEND1params.Discount = aLBTRXEND1_ParamsPtr->Discount;
	}
	else
	{
		LBTRXEND1params.Discount.ParamValuePtr			= (char *) Zero;
		LBTRXEND1params.Discount.TypeTranslFuncPtr	= NULL;
	}

	if( LBTRXEND1params.Pg == 1 )
	{
		LBTRXEND1params.Cash = aLBTRXEND1_ParamsPtr->Cash;
	}
	else
	{
		LBTRXEND1params.Cash.ParamValuePtr			= (char *) Zero;
		LBTRXEND1params.Cash.TypeTranslFuncPtr	= NULL;
	}

	LBTRXEND1params.Pfn_3								= aLBTRXEND1_ParamsPtr->Pfn;
	LBTRXEND1params.FormOfPaymentAmount	= aLBTRXEND1_ParamsPtr->FormOfPaymentAmount;

	if( LBTRXEND1params.Pr == 1 )
	{
		LBTRXEND1params.Rest = aLBTRXEND1_ParamsPtr->Rest;
	}
	else
	{
		LBTRXEND1params.Rest.ParamValuePtr			= (char *) Zero;
		LBTRXEND1params.Rest.TypeTranslFuncPtr	= NULL;;
	}

	LBTRXEND1params.Pkb_3									= aLBTRXEND1_ParamsPtr->Pkb;
	LBTRXEND1params.DepositTakenAmount		= aLBTRXEND1_ParamsPtr->DepositTakenAmount;
	LBTRXEND1params.Pkz_3									= aLBTRXEND1_ParamsPtr->Pkz;
	LBTRXEND1params.DepositReturnedAmount	= aLBTRXEND1_ParamsPtr->DepositReturnedAmount;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBTRXEND1_CMD_STR,
														sizeof(LBTRXEND1_Descr), LBTRXEND1_Descr, (BYTE *) & LBTRXEND1params );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int LBCSHREP1( tLBCSHREP1_PARAMS *aLBCSHREP1_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aLBCSHREP1_ParamsPtr );

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBCSHREP1_CMD_STR,
														sizeof(LBCSHREP1_Descr), LBCSHREP1_Descr, (BYTE *) aLBCSHREP1_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp, MAX_RESPONSE_TIME );	/* d�ugie czekanie */
	return RetI;

}

/******************************************************************************/

int LBCSHREP2( tLBCSHREP2_PARAMS *aLBCSHREP2_ParamsPtr )
{
	static int RetI;
	static tLBCSHREP2_PARAMS_INT LBCSHREP2params;

	_ASSERTE( aLBCSHREP2_ParamsPtr );

	LBCSHREP2params.Pkb											= aLBCSHREP2_ParamsPtr->Pkb;
	LBCSHREP2params.Pkz											= aLBCSHREP2_ParamsPtr->Pkz;
	LBCSHREP2params.Pfn											= aLBCSHREP2_ParamsPtr->Pfn;
	LBCSHREP2params.Pg											= aLBCSHREP2_ParamsPtr->Pg;
	LBCSHREP2params.Pfn_1										= LBCSHREP2params.Pfn;	
	LBCSHREP2params.Pfx											= aLBCSHREP2_ParamsPtr->Pfx;
	LBCSHREP2params.Shift										= aLBCSHREP2_ParamsPtr->Shift;
	LBCSHREP2params.CashRegNr								= aLBCSHREP2_ParamsPtr->CashRegNr;
	LBCSHREP2params.Cashier									= aLBCSHREP2_ParamsPtr->Cashier;
	LBCSHREP2params.Beginning								= aLBCSHREP2_ParamsPtr->Beginning;
	LBCSHREP2params.End											= aLBCSHREP2_ParamsPtr->End;
	LBCSHREP2params.Pfn_2										= LBCSHREP2params.Pfn;
	LBCSHREP2params.FormOfPaymentName				= aLBCSHREP2_ParamsPtr->FormOfPaymentName;
	LBCSHREP2params.Pkb_1										= LBCSHREP2params.Pkb;
	LBCSHREP2params.DepositTakenName				= aLBCSHREP2_ParamsPtr->DepositTakenName;
	LBCSHREP2params.Pkz_1										= LBCSHREP2params.Pkz;
	LBCSHREP2params.DepositReturnedName			= aLBCSHREP2_ParamsPtr->DepositReturnedName;
	LBCSHREP2params.Takings									= aLBCSHREP2_ParamsPtr->Takings;
	LBCSHREP2params.SalesCash								= aLBCSHREP2_ParamsPtr->SalesCash;
	LBCSHREP2params.PaymentIn								= aLBCSHREP2_ParamsPtr->PaymentIn;
	LBCSHREP2params.Expenditures						= aLBCSHREP2_ParamsPtr->Expenditures;
	LBCSHREP2params.PaymentOut							= aLBCSHREP2_ParamsPtr->PaymentOut;
	LBCSHREP2params.Pfn_3										= LBCSHREP2params.Pfn;
	LBCSHREP2params.FormOfPaymentAmount			= aLBCSHREP2_ParamsPtr->FormOfPaymentAmount;
	LBCSHREP2params.DepositTakenTotal				= aLBCSHREP2_ParamsPtr->DepositTakenTotal;
	LBCSHREP2params.Pkb_2										= LBCSHREP2params.Pkb;
	LBCSHREP2params.DepositTakenAmount			= aLBCSHREP2_ParamsPtr->DepositTakenAmount;
	LBCSHREP2params.DepositReturnedTotal		= aLBCSHREP2_ParamsPtr->DepositReturnedTotal;
	LBCSHREP2params.Pkz_2										= LBCSHREP2params.Pkz;
	LBCSHREP2params.DepositReturnedAmount		= aLBCSHREP2_ParamsPtr->DepositReturnedAmount;
	LBCSHREP2params.Cash										= aLBCSHREP2_ParamsPtr->Cash;
	LBCSHREP2params.NrOfReceipts						= aLBCSHREP2_ParamsPtr->NrOfReceipts;
	LBCSHREP2params.NrOfCancelledReceipts		= aLBCSHREP2_ParamsPtr->NrOfCancelledReceipts;
	LBCSHREP2params.NrOfCancelledItems			= aLBCSHREP2_ParamsPtr->NrOfCancelledItems;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBCSHREP2_CMD_STR,
														sizeof(LBCSHREP2_Descr), LBCSHREP2_Descr, (BYTE *) & LBCSHREP2params );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp, MAX_RESPONSE_TIME );	/* d�ugie czekanie */
	return RetI;
}

/******************************************************************************/

/*
	LBTRSCARD i LBSTOCARD r�ni� si� jedynie kodem rozkazu.

	LBCARD s�u�y do obs�ugi obu rozkaz�w.
*/
int LBCARD( char *aCmdStr, tLBCARD_PARAMS *aLBCARD_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aLBCARD_ParamsPtr );

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, aCmdStr,
														sizeof(LBCARD_Descr), LBCARD_Descr, (BYTE *) aLBCARD_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/*----------------------------------------------------------------------------*/

int LBTRSCARD( tLBCARD_PARAMS *aLBTRSCARD_ParamsPtr )
{
	return LBCARD( LBTRSCARD_CMD_STR, aLBTRSCARD_ParamsPtr );
}

/*----------------------------------------------------------------------------*/

int LBSTOCARD( tLBCARD_PARAMS *aLBSTOCARD_ParamsPtr )
{
	return LBCARD( LBSTOCARD_CMD_STR, aLBSTOCARD_ParamsPtr );
}

/******************************************************************************/

/*
	LBINCCSH i LBDECCSH r�ni� si� jedynie kodem rozkazu

	LBCSH s�u�y do obs�ugi obu tych funkcji (w��cznie ze sprawdzaniem parametr�w)
*/
int LBCSH( char *aCmdStr, tLBCSH_PARAMS *aLBCSH_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aLBCSH_ParamsPtr );

/*
	Albo oba (CashRegNr i Cashier) s�, albo ich nie ma...
*/
	if( (aLBCSH_ParamsPtr->CashRegNr != NULL) != (aLBCSH_ParamsPtr->Cashier != NULL) )
	{
		return ERR_OPT_PARAMS;
	}

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, aCmdStr,
														sizeof(LBCSH_Descr), LBCSH_Descr, (BYTE *) aLBCSH_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/*----------------------------------------------------------------------------*/

int LBINCCSH( tLBCSH_PARAMS *aLBINCCSH_ParamsPtr )
{
	return LBCSH( LBINCCSH_CMD_STR, aLBINCCSH_ParamsPtr );
}

/*----------------------------------------------------------------------------*/

int LBDECCSH( tLBCSH_PARAMS *aLBDECCSH_ParamsPtr )
{
	return LBCSH( LBDECCSH_CMD_STR, aLBDECCSH_ParamsPtr );
}

/******************************************************************************/

int LBCSHSTS( char *aCashRegNr, char *aCashier )
{
	static int RetI;
	static tLBCSHSTS_PARAMS_INT LBCSHSTSparams;

/*
	Albo oba (CashRegNr i Cashier) s�, albo ich nie ma...
*/
	if( (aCashRegNr != NULL) != (aCashier != NULL) )
	{
		return ERR_OPT_PARAMS;
	}

	LBCSHSTSparams._dummy = 0;	/* parametr - wype�niacz */
	LBCSHSTSparams.CashRegNr	= aCashRegNr;
	LBCSHSTSparams.Cashier		= aCashier;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBCSHSTS_CMD_STR,
														sizeof(LBCSHSTS_Descr), LBCSHSTS_Descr, (BYTE *) & LBCSHSTSparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int LBCSHREP( tLBCSHREP_PARAMS *aLBCSHREP_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aLBCSHREP_ParamsPtr );

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBCSHREP_CMD_STR,
														sizeof(LBCSHREP_Descr), LBCSHREP_Descr, (BYTE *) aLBCSHREP_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp, MAX_RESPONSE_TIME );	/* d�ugie czekanie */
	return RetI;
}

/******************************************************************************/

/*
	LBLOGIN i LBLOGOUT r�ni� si� jedynie kodem rozkazu

	LBLOG s�u�y do obs�ugi obu tych funkcji (w��cznie ze sprawdzaniem parametr�w)
*/
int LBLOG( char *aCmdStr, tLBLOG_PARAMS *aLBLOG_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aLBLOG_ParamsPtr );

	aLBLOG_ParamsPtr->_dummy = 0;		/* bajt - wype�niacz, ustalamy warto�� 0 */

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, aCmdStr,
														sizeof(LBLOG_Descr), LBLOG_Descr, (BYTE *) aLBLOG_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/*----------------------------------------------------------------------------*/

int LBLOGIN( tLBLOG_PARAMS *aLBLOGIN_ParamsPtr )
{
	return LBLOG( LBLOGIN_CMD_STR, aLBLOGIN_ParamsPtr );
}

/*----------------------------------------------------------------------------*/

int LBLOGOUT( tLBLOG_PARAMS *aLBLOGOUT_ParamsPtr )
{
	return LBLOG( LBLOGOUT_CMD_STR, aLBLOGOUT_ParamsPtr );
}

/******************************************************************************/

/*
	LBFSKREP ma dwie, znacznie si� r�ni�ce, wersje. Dlatego te� istniej� dwa oddzielne rozkazy.
	_D - zakres: data pocz�tku i ko�ca
	_R - zakres: numer pocz�tkowego i ko�cowego rekordu
*/
int LBFSKREP_D( tLBFSKREP_D_PARAMS *aLBFSKREP_D_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aLBFSKREP_D_ParamsPtr );

/*
	Parametry Cashier i CashRegNr - je�li maj� wyst�powa�, to musz� by� oba.
*/
	if( (aLBFSKREP_D_ParamsPtr->Cashier != NULL) != (aLBFSKREP_D_ParamsPtr->CashRegNr != NULL) )
	{
		return ERR_OPT_PARAMS;
	}

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBFSKREP_CMD_STR,
														sizeof(LBFSKREP_D_Descr), LBFSKREP_D_Descr, (BYTE *) aLBFSKREP_D_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp, MAX_RESPONSE_TIME );	/* d�ugie czekanie */
	return RetI;
}

/*----------------------------------------------------------------------------*/

int LBFSKREP_R( tLBFSKREP_R_PARAMS *aLBFSKREP_R_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aLBFSKREP_R_ParamsPtr );

/*
	Parametry Cashier i CashRegNr - je�li maj� wyst�powa�, to musz� by� oba.
*/
	if( (aLBFSKREP_R_ParamsPtr->Cashier != NULL) != (aLBFSKREP_R_ParamsPtr->CashRegNr != NULL) )
	{
		return ERR_OPT_PARAMS;
	}

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBFSKREP_CMD_STR,
														sizeof(LBFSKREP_R_Descr), LBFSKREP_R_Descr, (BYTE *) aLBFSKREP_R_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp, MAX_RESPONSE_TIME );	/* d�ugie czekanie */
	return RetI;
}

/******************************************************************************/

int LBDAYREP( tLBDAYREP_PARAMS *aLBDAYREP_ParamsPtr )
{
	static int RetI;
	static tLBDAYREP_PARAMS_INT LBDAYREPparams;
	static BYTE Date[3], Offending;

	_ASSERTE( aLBDAYREP_ParamsPtr );

/*
	Parametry Cashier i CashRegNr - je�li maj� wyst�powa�, to musz� by� oba.
*/
	if( (aLBDAYREP_ParamsPtr->Cashier != NULL) != (aLBDAYREP_ParamsPtr->CashRegNr != NULL) )
	{
		return ERR_OPT_PARAMS;
	}

	if( aLBDAYREP_ParamsPtr->_Date != 0 )	// wersja z dat�
	{
		if( ! CheckDate(aLBDAYREP_ParamsPtr->Py, aLBDAYREP_ParamsPtr->Pm,
										aLBDAYREP_ParamsPtr->Pd, & Offending) )
		{
			return ( ERR_PARAM_VALUE | (Offending +1) );	// +1 bo jest jeszcze Ps, kt�ry tutaj
		}																								// nosi nazw� '_Date'
		else
		{
			Date[0] = aLBDAYREP_ParamsPtr->Py;
			Date[1] = aLBDAYREP_ParamsPtr->Pm;
			Date[2] = aLBDAYREP_ParamsPtr->Pd;
			LBDAYREPparams._Date = 3;
			LBDAYREPparams._DateTab = Date;
		}
	}
	else
	{
		LBDAYREPparams._Date = 0;
		LBDAYREPparams._DateTab = NULL;
	}

	LBDAYREPparams.Cashier = aLBDAYREP_ParamsPtr->Cashier;
	LBDAYREPparams.CashRegNr = aLBDAYREP_ParamsPtr->CashRegNr;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBDAYREP_CMD_STR,
														sizeof(LBDAYREP_Descr), LBDAYREP_Descr, (BYTE *) & LBDAYREPparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp, MAX_RESPONSE_TIME );	/* d�ugie czekanie */
	return RetI;
}


/******************************************************************************/

int LBDBREP( tLBDBREP_PARAMS *aLBDBREP_ParamsPtr, BYTE *aResult )
{
	BYTE PeEs;
	int RetI;

	_ASSERTE( aLBDBREP_ParamsPtr );
	tRESPONSE ResponseStruct;
	tLBDBREP_RESPONSE Resp;

	PeEs = aLBDBREP_ParamsPtr->Ps;

	CThResp.ClearResponseStructPtr();	// inicjalizujemy - brak odpowiedzi z danymi

	if( (PeEs == 0) || (PeEs == 4) )
	{
		aLBDBREP_ParamsPtr->Name = NULL;
		aLBDBREP_ParamsPtr->Vat = NULL;
		aLBDBREP_ParamsPtr->CashRegNr = NULL;
		aLBDBREP_ParamsPtr->Cashier = NULL;

		ResponseStruct.Description = LBDBREP_Resp_Descr;	// odpowied� tylko dla Ps == { 0, 4 }
		ResponseStruct.DescriptionLen = sizeof( LBDBREP_Resp_Descr );
		ResponseStruct.FieldsStruct = (BYTE *) & Resp;
		CThResp.SetResponseStructPtr( & ResponseStruct );
	}
	else if( PeEs == 1 )
	{
		aLBDBREP_ParamsPtr->CashRegNr = NULL;
		aLBDBREP_ParamsPtr->Cashier = NULL;
	}
	else if( PeEs == 2 )
	{
		if(	(aLBDBREP_ParamsPtr->CashRegNr == NULL) != (aLBDBREP_ParamsPtr->Cashier == NULL ) )
		{
			return ERR_OPT_PARAMS;
		}

	}
	// a 3 ?? Eeee tam... Ps > 4 wykryje Translate

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBDBREP_CMD_STR,
													sizeof(LBDBREP_Descr), LBDBREP_Descr, (BYTE *) aLBDBREP_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );

	if( (RetI == SOK) && (PeEs == 1) )	// by� rekord danych, przepiszemy wynik testu
	{
		*aResult = Resp.Result;
	}

	return RetI;
}


/******************************************************************************/

int LBDBREPRS( char *aName, char aVat, tLBDBREPRS_RESPONSE *aLBDBREPRS_ResponsePtr )
{
	static int RetI;
	static char VatStr[2];
	static tLBDBREPRS_PARAMS_INT LBDBREPRSparams;
	static tRESPONSE ResponseStruct;

	_ASSERTE( aName );
	_ASSERTE( aLBDBREPRS_ResponsePtr );

	VatStr[0] = toupper( aVat );
	VatStr[1] = 0;

	LBDBREPRSparams.Name = aName;
	LBDBREPRSparams.Vat = VatStr;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBDBREPRS_CMD_STR,
														sizeof(LBDBREPRS_Descr), LBDBREPRS_Descr, (BYTE *) & LBDBREPRSparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	ResponseStruct.Description = LBDBREPRS_Resp_Descr;
	ResponseStruct.DescriptionLen = sizeof( LBDBREPRS_Resp_Descr );
	ResponseStruct.FieldsStruct = (BYTE *) aLBDBREPRS_ResponsePtr;
	CThResp.SetResponseStructPtr( & ResponseStruct );

	RetI = Execute( & CThCmd, & CThResp, MAX_RESPONSE_TIME );	/* d�ugie czekanie */
	return RetI;
}

/******************************************************************************/

int LBSENDCK( tLBSENDCK_RESPONSE *aLBSENDCK_ResponsePtr )
{
	static int RetI;
	static tRESPONSE ResponseStruct;
	static tLBSENDCK_PARAMS_INT LBSENDCKparams;

	_ASSERTE( aLBSENDCK_ResponsePtr );

	LBSENDCKparams._dummy = 0;

	RetI = CThCmd.Translate( PROTOCOL_OLD_NCHK_RESP_NCHK, LBSENDCK_CMD_STR,
														sizeof(LBSENDCK_Descr), LBSENDCK_Descr, (BYTE *) & LBSENDCKparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	ResponseStruct.Description = LBSENDCK_Resp_Descr;
	ResponseStruct.DescriptionLen = sizeof( LBSENDCK_Resp_Descr );
	ResponseStruct.FieldsStruct = (BYTE *) aLBSENDCK_ResponsePtr;
	CThResp.SetResponseStructPtr( & ResponseStruct );

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/
/*WM*****************************************************************************/

int LBFSTRQ22( tLBFSTRS1_RESPONSE *aLBFSTRS1_ResponsePtr )
{
	static int RetI;
	static tRESPONSE ResponseStruct;
	static tLBFSTRQ_PARAMS LBFSTRQparams;

	_ASSERTE( aLBFSTRS1_ResponsePtr );

	LBFSTRQparams.Ps = 22;		/* Ps==22 -> odpowied� LBFSTRS1 */

	RetI = CThCmd.Translate( PROTOCOL_OLD_NCHK_RESP_CHK, LBFSTRQ_CMD_STR,
			     	sizeof(LBFSTRQ_Descr), LBFSTRQ_Descr, (BYTE *) & LBFSTRQparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	ResponseStruct.Description = LBFSTRS1_Resp_Descr;
	ResponseStruct.DescriptionLen = sizeof( LBFSTRS1_Resp_Descr );
	ResponseStruct.FieldsStruct = (BYTE *) aLBFSTRS1_ResponsePtr;
	CThResp.SetResponseStructPtr( & ResponseStruct );

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}
/*----------------------------------------------------------------------------*/
int LBFSTRQ( tLBFSTRS1_RESPONSE *aLBFSTRS1_ResponsePtr )
{
	static int RetI;
	static tRESPONSE ResponseStruct;
	static tLBFSTRQ_PARAMS LBFSTRQparams;

	_ASSERTE( aLBFSTRS1_ResponsePtr );

	LBFSTRQparams.Ps = 23;		/* Ps==23 -> odpowied� LBFSTRS1 */

	RetI = CThCmd.Translate( PROTOCOL_OLD_NCHK_RESP_CHK, LBFSTRQ_CMD_STR,
			     	sizeof(LBFSTRQ_Descr), LBFSTRQ_Descr, (BYTE *) & LBFSTRQparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	ResponseStruct.Description = LBFSTRS1_Resp_Descr;
	ResponseStruct.DescriptionLen = sizeof( LBFSTRS1_Resp_Descr );
	ResponseStruct.FieldsStruct = (BYTE *) aLBFSTRS1_ResponsePtr;
	CThResp.SetResponseStructPtr( & ResponseStruct );

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}
/*----------------------------------------------------------------------------*/

/*
	Stara homologacja...
	Parser nie by� pisany z my�l� o zmiennej liczbie p�l w ramkach zwrotnych
	(bo i taka sytuacja nie wyst�puje w protokole w�oskim i polskim w nowej
	homologacji)
*/

int O_LBFSTRQ( BYTE *aNrOfVatRatesOutPtr, tO_LBFSTRS_RESPONSE *aO_LBFSTRS_ResponsePtr )
{
	static int RetI;
	static DWORD FldCount;
	static tRESPONSE ResponseStruct;
	static tLBFSTRQ_PARAMS LBFSTRQparams;
	static tCASH_REGISTER_STATUS CRStat;
	static BYTE ProtocolType, NrOfVatRates;

	ProtocolType = PROTOCOL_OLD_NCHK_RESP_NCHK;		/* w starej homologacji nie ma sumy kontr. */

	_ASSERTE( aNrOfVatRatesOutPtr );
	_ASSERTE( aO_LBFSTRS_ResponsePtr );

	LBFSTRQparams.Ps = 0;					/* Ps w starej homologacji jest ignorowane */

/*
	Translacja na posta� wewn�trzn�
*/
	RetI = CThCmd.Translate( ProtocolType, LBFSTRQ_CMD_STR,
														sizeof(LBFSTRQ_Descr), LBFSTRQ_Descr, (BYTE *) & LBFSTRQparams );
	if( RetI != SOK)
	{
		return RetI;
	}

/*
	Wys�anie sekwencji
*/
	RetI = CThCmd.Send();
	if( RetI != SOK )
	{
		return RetI;
	}

/*
	Odebranie odpowiedzi
*/
	RetI = CThResp.Receive( ProtocolType, TRUE, NORMAL_RESPONSE_TIME );
	if( RetI != SOK )
	{
		CThResp.ClearResponseStructPtr();		/* trzeba r�cznie wyzerowa� wska�nik ! */
		return RetI;
	}

/*
	Pobranie liczby p�l
*/
	FldCount = CThResp.GetFieldCount();

/*
	Na podstawie liczby p�l okre�lamy, jaki jest opis parametr�w
*/
	switch( FldCount )
	{
		default:													/* nieprawid�owa liczba p�l */
			return ERR_NUMBER_OF_FIELDS;
			/* NO BREAK HERE ! */

		case 15:
			ResponseStruct.Description = O_LBFSTRS_Resp_Descr1;
			NrOfVatRates = 1;
			break;

		case 17:
			ResponseStruct.Description = O_LBFSTRS_Resp_Descr2;
			NrOfVatRates = 2;
			break;

		case 19:
			ResponseStruct.Description = O_LBFSTRS_Resp_Descr3;
			NrOfVatRates = 3;
			break;

		case 21:
			ResponseStruct.Description = O_LBFSTRS_Resp_Descr4;
			NrOfVatRates = 4;
			break;

		case 23:
			ResponseStruct.Description = O_LBFSTRS_Resp_Descr5;
			NrOfVatRates = 5;
			break;

		case 25:
			ResponseStruct.Description = O_LBFSTRS_Resp_Descr6;
			NrOfVatRates = 6;
			break;
	}

	ResponseStruct.DescriptionLen = FldCount;
	ResponseStruct.FieldsStruct		= (BYTE *) aO_LBFSTRS_ResponsePtr;

/*
	Wype�niamy struktur� zwrotn�
*/
	RetI = CThResp.CreateRespStruct( ProtocolType, & ResponseStruct );
	CThResp.ClearResponseStructPtr();	/* to tak na wszelki wypadek... */

/*
	Je�li odebrano sensown� ramk�, zwr�cimy (wpiszemy pod podany adres) liczb�
	zdefiniowanych stawek PTU.
*/
	if( RetI == SOK )
	{
		*aNrOfVatRatesOutPtr = NrOfVatRates;
	}

	return RetI;
/*
	Nie trzeba ju� sprawdza� bitu CMD odpowiedzi na ENQ - bo w przypadku b��du ju� wcze�niej
	b�dziemy wiedzie�, �e co� jest nie tak - otrzymamy nie to, na co czekamy.
*/
}


/******************************************************************************/

int LBERNRQ( BYTE *aErrCodeOutPtr )
{
	static int RetI;
	static tLBERNRQ_PARAMS_INT LBERNRQparams;
	static tLBERNRQ_RESPONSE LBERNRQresponse;
	static tRESPONSE ResponseStruct;

	_ASSERTE( aErrCodeOutPtr );

	LBERNRQparams._dummy = 0;

	RetI = CThCmd.Translate( PROTOCOL_OLD_NCHK_RESP_NCHK, LBERNRQ_CMD_STR,
														sizeof(LBERNRQ_Descr), LBERNRQ_Descr, (BYTE *) & LBERNRQparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	ResponseStruct.Description = LBERNRQ_Resp_Descr;
	ResponseStruct.DescriptionLen = sizeof( LBERNRQ_Resp_Descr );
	ResponseStruct.FieldsStruct = (BYTE *) & LBERNRQresponse;
	CThResp.SetResponseStructPtr( & ResponseStruct );
	RetI = Execute( & CThCmd, & CThResp );

/*
	Poniewa� zwracamy jeden bajt (przez wska�nik) to teraz trzeba go wpisa� we w�a�ciwe miejsce -
	oczywi�cie o ile rozkaz wykonany by� pomy�lnie
*/
	if( RetI == SOK )
	{
		*aErrCodeOutPtr = LBERNRQresponse.Pe;
	}

	return RetI;
}

/******************************************************************************/

int LBIDRQ( tLBIDRQ_RESPONSE *aLBIDRQ_ResponsePtr )
{
	static int RetI;
	static tLBIDRQ_PARAMS_INT LBIDRQparams;
	static tRESPONSE ResponseStruct;

	_ASSERTE( aLBIDRQ_ResponsePtr );

	LBIDRQparams._dummy = 0;

	RetI = CThCmd.Translate( PROTOCOL_OLD_NCHK_RESP_NCHK, LBIDRQ_CMD_STR,
														sizeof(LBIDRQ_Descr), LBIDRQ_Descr, (BYTE *) & LBIDRQparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	ResponseStruct.Description = LBIDRQ_Resp_Descr;
	ResponseStruct.DescriptionLen = sizeof( LBIDRQ_Resp_Descr );
	ResponseStruct.FieldsStruct = (BYTE *) aLBIDRQ_ResponsePtr;
	CThResp.SetResponseStructPtr( & ResponseStruct );

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

int LBSNDMD( BYTE aMode )
{
	static int RetI;
	static tLBSNDMD_PARAMS_INT LBSNDMDparams;

	LBSNDMDparams.Ps = aMode;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBSNDMD_CMD_STR,
														sizeof(LBSNDMD_Descr), LBSNDMD_Descr, (BYTE *) & LBSNDMDparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/******************************************************************************/

/*
	Ciekawe... Trafi� si� jeden rozkaz, kt�rego ramka nie zawiera �adnych parametr�w (nawet wype�niacza bajtowego)
*/
int LBCASREP()
{
	static int RetI;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, LBCASREP_CMD_STR, 0, NULL, NULL );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp, MAX_RESPONSE_TIME );	/* d�ugie czekanie */
	return RetI;
}

/******************************************************************************/

int LBFSTRQ24( tLBFSTRQ1_RESPONSE *aLBFSTRQ1_ResponsePtr )
{
	static int RetI;
	static tRESPONSE ResponseStruct;
	static tLBFSTRQ_PARAMS LBFSTRQparams;

	_ASSERTE( aLBFSTRQ1_ResponsePtr );

	LBFSTRQparams.Ps = 24;											/* Ps == 24 -> LBFSTRQ1 */

	RetI = CThCmd.Translate( PROTOCOL_OLD_NCHK_RESP_CHK, LBFSTRQ_CMD_STR,
														sizeof(LBFSTRQ_Descr), LBFSTRQ_Descr, (BYTE *) & LBFSTRQparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	ResponseStruct.Description = LBFSTRQ1_Resp_Descr;
	ResponseStruct.DescriptionLen = sizeof( LBFSTRQ1_Resp_Descr );
	ResponseStruct.FieldsStruct = (BYTE *) aLBFSTRQ1_ResponsePtr;
	CThResp.SetResponseStructPtr( & ResponseStruct );

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/*----------------------------------------------------------------------------*/

int LBFSTRQ25( tLBFSTRQ25_PARAMS *aLBFSTRQ25_ParamsPtr )
{
	static int RetI;

	_ASSERTE( aLBFSTRQ25_ParamsPtr );

	aLBFSTRQ25_ParamsPtr->Ps = 25;							/* Ps == 25 */

	RetI = CThCmd.Translate( PROTOCOL_OLD_NCHK_RESP_NCHK, LBFSTRQ_CMD_STR,
														sizeof(LBFSTRQ25_Descr), LBFSTRQ25_Descr, (BYTE *) aLBFSTRQ25_ParamsPtr );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/*----------------------------------------------------------------------------*/

int LBFSTRQ26( DWORD aRecordNr )
{
	static int RetI;
	static tLBFSTRQ26_PARAMS LBFSTRQ26params;

	LBFSTRQ26params.Ps = 26;											/* Ps == 26 */
	LBFSTRQ26params.Nr = aRecordNr;								/* No i jest pole 'Nr' */

	RetI = CThCmd.Translate( PROTOCOL_OLD_NCHK_RESP_NCHK, LBFSTRQ_CMD_STR,
														sizeof(LBFSTRQ26_Descr), LBFSTRQ26_Descr, (BYTE *) & LBFSTRQ26params );
	if( RetI != SOK)
	{
		return RetI;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return RetI;
}

/*----------------------------------------------------------------------------*/

/*
	Ps==27 powoduje odes�anie bie��cego rekordu. Jaki jest to rodzaj - dowiedzie�
	si� mo�na dopiero po sprawdzeniu zwrotnego bajtowego (?) kodu - tego, kt�ry
	jest zaraz po znaczniku pocz�tku ramki
*/

int LBFSTRQ27( tLBFSTRQ27_RESPONSE *aLBFSTRQ27_ResponsePtr, BYTE *aRecordTypeOutPtr )
{
	static int RetI;
	static tRESPONSE ResponseStruct;
	static tLBFSTRQ_PARAMS LBFSTRQparams;

	_ASSERTE( aLBFSTRQ27_ResponsePtr );
	_ASSERTE( aRecordTypeOutPtr );

	LBFSTRQparams.Ps = 27;											/* Ps == 27 -> LBFSTRQ27 */

	RetI = CThCmd.Translate( PROTOCOL_OLD_NCHK_RESP_CHK, LBFSTRQ_CMD_STR,
														sizeof(LBFSTRQ_Descr), LBFSTRQ_Descr, (BYTE *) & LBFSTRQparams );
	if( RetI != SOK)
	{
		return RetI;
	}

	ResponseStruct.Description = LBFSTRQ27_Resp_Descr;
	ResponseStruct.DescriptionLen = sizeof( LBFSTRQ27_Resp_Descr );
	ResponseStruct.FieldsStruct = (BYTE *) aLBFSTRQ27_ResponsePtr;
	CThResp.SetResponseStructPtr( & ResponseStruct );

	RetI = Execute( & CThCmd, & CThResp );

/*
	Gdy otrzymana ramka odpowiedzi jest poprawna (pod wzgl�dem sk�adniowym) - 
	zwr�cimy jeszcze kod oznaczaj�cy rodzaj rekordu
*/
	if( RetI == SOK )
	{
		*aRecordTypeOutPtr = (BYTE) CThResp.GetReturnCode();
	}

	return RetI;
}


/*******************************************************************************
********************************************************************************
************************ INTERFEJS DLA KONWENCJI STDCALL ***********************
********************************************************************************
*******************************************************************************/

int _stdcall p_SetTimeoutDialogFunc( tTIMEOUT_DLG_FUNC aFuncAddr )
{
	return SetTimeoutDialogFunc( aFuncAddr );
}

int _stdcall p_Port_Open( char *aPortNamePtr, DWORD aBaudRate )
{
	return Port_Open( aPortNamePtr, aBaudRate );
}

int _stdcall p_Port_SetBaudRate( DWORD aRate )
{
	return Port_SetBaudRate( aRate );
}

void _stdcall p_Port_Close()
{
	Port_Close();
}

int _stdcall p_Port_LoggingStart( char *aFileName )
{
	return Port_LoggingStart( aFileName );
}

int _stdcall p_Port_LoggingStop()
{
	return Port_LoggingStop();
}

void _stdcall p_ErrorMessage( char *aStrPtr, int aReturnCode )
{
	ErrorMessage( aStrPtr, aReturnCode );
}

BOOL _stdcall p_GetDeviceErrorMessage( DWORD aExtendedErrorCode, char **aStrOutPtr )
{
	return GetDeviceErrorMessage( aExtendedErrorCode, aStrOutPtr );
}

DWORD _stdcall p_GetDeviceError()
{
	return GetDeviceError();
}

void _stdcall p_Send_ACK()
{
	Send_ACK();
}

void _stdcall p_Send_NACK()
{
	Send_NACK();
}

void _stdcall p_Send_CAN()
{
	Send_CAN();
}

void _stdcall p_Send_BEL()
{
	Send_BEL();
}

int _stdcall p_I_LOGIN( tLOGIN_PARAMS *aLOGIN_ParamsPtr )
{
	return I_LOGIN( aLOGIN_ParamsPtr );
}

int _stdcall p_I_LOGOUT()
{
	return I_LOGOUT();
}

int _stdcall p_I_SETOPT( tSETOPT_PARAMS *aSETOPT_ParamsPtr )
{
	return I_SETOPT( aSETOPT_ParamsPtr );
}

int _stdcall p_I_SETVAT( tSETVAT_PARAMS *aSETVAT_ParamsPtr )
{
	return I_SETVAT( aSETVAT_ParamsPtr );
}

int _stdcall p_I_TRSHDR( tTRSHDR_PARAMS *aTRSHDR_ParamsPtr )
{
	return I_TRSHDR( aTRSHDR_ParamsPtr );
}

int _stdcall p_I_TRSLNE( tTRSLNE_PARAMS *aTRSLNE_ParamsPtr )
{
	return I_TRSLNE( aTRSLNE_ParamsPtr );
}

int _stdcall p_I_TRSEND( tTRSEND_PARAMS *aTRSEND_ParamsPtr )
{
	return I_TRSEND( aTRSEND_ParamsPtr );
}

int _stdcall p_I_TRSEXIT()
{
	return I_TRSEXIT();
}

int _stdcall p_I_RECADD( tRECADD_PARAMS *aRECADD_ParamsPtr )
{
	return I_RECADD( aRECADD_ParamsPtr );
}

int _stdcall p_I_RECGET( BYTE aDatabase, DWORD aRecordNr, tRECGET_RESPONSE *aRecGetResponsePtr )
{
	return I_RECGET( aDatabase, aRecordNr, aRecGetResponsePtr );
}

int _stdcall p_I_GETSTS( tGETSTS_RESPONSE *aGetStsResponsePtr )
{
	return I_GETSTS( aGetStsResponsePtr );
}

int _stdcall p_I_GETTOT( BOOL aEuroMode, tGETTOT_RESPONSE *aGetTotResponsePtr )
{
	return I_GETTOT( aEuroMode, aGetTotResponsePtr );
}

int _stdcall p_I_GetStatus_CashRegister( tI_CASH_REGISTER_STATUS *aStatusOutPtr )
{
	return I_GetStatus_CashRegister( aStatusOutPtr );
}

int _stdcall p_I_GetStatus_Printer( tI_PRINTER_STATUS *aStatusOutPtr )
{
	return I_GetStatus_Printer( aStatusOutPtr );
}

int _stdcall p_I_Get_Message( BOOL aEuroMode, BYTE *aMessageTypeOutPtr, BYTE **aMessageFieldsOutPtr )
{
	return I_Get_Message( aEuroMode, aMessageTypeOutPtr, aMessageFieldsOutPtr );
}

int _stdcall p_I_S_TRSITEM( tS_TRSITEM_PARAMS *aTRSITEM_ParamsPtr )
{
	return I_S_TRSITEM( aTRSITEM_ParamsPtr );
}

int _stdcall p_I_S_TRSDSC( tS_TRSDSC_PARAMS *aTRSDSC_ParamsPtr )
{
	return I_S_TRSDSC( aTRSDSC_ParamsPtr );
}

int _stdcall p_GetStatus_CashRegister( tCASH_REGISTER_STATUS *aStatusOutPtr )
{
	return GetStatus_CashRegister( aStatusOutPtr );
}

int _stdcall p_GetStatus_Printer( tPRINTER_STATUS *aStatusOutPtr )
{
	return GetStatus_Printer( aStatusOutPtr );
}

int _stdcall p_LBSETCK( tLBSETCK_PARAMS *aLBSETCK_ParamsPtr )
{
	return LBSETCK( aLBSETCK_ParamsPtr );
}

int _stdcall p_LBDSP( BYTE aMode, char *aString )
{
	return LBDSP( aMode, aString );
}

int _stdcall p_LBSETPTU( tLBSETPTU_PARAMS *aLBSETPTU_ParamsPtr )
{
	return LBSETPTU( aLBSETPTU_ParamsPtr );
}

int _stdcall p_LBSETHDR( char *aHdrString, char *aCashRegNr, char *aCashier )
{
	return LBSETHDR( aHdrString, aCashRegNr, aCashier );
}

int _stdcall p_LBFEED( BYTE aNrOfLines )
{
	return LBFEED( aNrOfLines );
}

int _stdcall p_LBSERM( BYTE aMode )
{
	return LBSERM( aMode );
}

int _stdcall p_LBTRSHDR( BYTE aNrOfLines )
{
	return LBTRSHDR( aNrOfLines );
}

int _stdcall p_LBTRSLN( tLBTRSLN_PARAMS *aLBTRSLN_ParamsPtr )
{
	return LBTRSLN( aLBTRSLN_ParamsPtr );
}

int _stdcall p_O_LBTRSLN( tLBTRSLN_PARAMS *aLBTRSLN_ParamsPtr )
{
	return O_LBTRSLN( aLBTRSLN_ParamsPtr );
}

int _stdcall p_LBDEP_P( tLBDEP_PARAMS *aLBDEP_ParamsPtr )
{
	return LBDEP_P( aLBDEP_ParamsPtr );
}

int _stdcall p_LBDEPSTR_P( tLBDEP_PARAMS *aLBDEP_ParamsPtr )
{
	return LBDEPSTR_P( aLBDEP_ParamsPtr );
}

int _stdcall p_LBDEP_M( tLBDEP_PARAMS *aLBDEP_ParamsPtr )
{
	return LBDEP_M( aLBDEP_ParamsPtr );
}

int _stdcall p_LBDEPSTR_M( tLBDEP_PARAMS *aLBDEP_ParamsPtr )
{
	return LBDEPSTR_M( aLBDEP_ParamsPtr );
}

int _stdcall p_LBTREXITCAN( char *aCashRegNr, char *aCashier )
{
	return LBTREXITCAN( aCashRegNr, aCashier );
}

 int _stdcall p_O_LBTREXIT()
{
	return O_LBTREXIT();
}
//WM
int _stdcall p_LBTREXIT(tLBTREXIT_PARAMS *aLBTREXIT_PARAMS)
{
	return LBTREXIT(aLBTREXIT_PARAMS);
}

int _stdcall p_O_LBTRXEND( tO_LBTRXEND_PARAMS *aLBTRXEND_ParamsPtr )
{
	return O_LBTRXEND( aLBTRXEND_ParamsPtr );
}

int _stdcall p_LBTRFORMPLAT( tLBTRFORMPLAT_PARAMS *aLBTRFORMPLAT_ParamsPtr )
{
	return LBTRFORMPLAT( aLBTRFORMPLAT_ParamsPtr );
}

int _stdcall p_LBTRXEND1( tLBTRXEND1_PARAMS *aLBTRXEND1_ParamsPtr )
{
	return LBTRXEND1( aLBTRXEND1_ParamsPtr );
}

int _stdcall p_LBCSHREP2( tLBCSHREP2_PARAMS *aLBCSHREP2_ParamsPtr )
{
	return LBCSHREP2( aLBCSHREP2_ParamsPtr );
}

int _stdcall p_LBTRSCARD( tLBCARD_PARAMS *aLBTRSCARD_ParamsPtr )
{
	return LBTRSCARD( aLBTRSCARD_ParamsPtr );
}

int _stdcall p_LBSTOCARD( tLBCARD_PARAMS *aLBSTOCARD_ParamsPtr )
{
	return LBSTOCARD( aLBSTOCARD_ParamsPtr );
}

int _stdcall p_LBINCCSH( tLBCSH_PARAMS *aLBINCCSH_ParamsPtr )
{
	return LBINCCSH( aLBINCCSH_ParamsPtr );
}

int _stdcall p_LBDECCSH( tLBCSH_PARAMS *aLBDECCSH_ParamsPtr )
{
	return LBDECCSH( aLBDECCSH_ParamsPtr );
}

int _stdcall p_LBCSHSTS( char *aCashRegNr, char *aCashier )
{
	return LBCSHSTS( aCashRegNr, aCashier );
}

int _stdcall p_LBCSHREP( tLBCSHREP_PARAMS *aLBCSHREP_ParamsPtr )
{
	return LBCSHREP( aLBCSHREP_ParamsPtr );
}

int _stdcall p_LBLOGIN( tLBLOG_PARAMS *aLBLOGIN_ParamsPtr )
{
	return LBLOGIN( aLBLOGIN_ParamsPtr );
}

int _stdcall p_LBLOGOUT( tLBLOG_PARAMS *aLBLOGOUT_ParamsPtr )
{
	return LBLOGOUT( aLBLOGOUT_ParamsPtr );
}

int _stdcall p_LBFSKREP_D( tLBFSKREP_D_PARAMS *aLBFSKREP_D_ParamsPtr )
{
	return LBFSKREP_D( aLBFSKREP_D_ParamsPtr );
}

int _stdcall p_LBFSKREP_R( tLBFSKREP_R_PARAMS *aLBFSKREP_R_ParamsPtr )
{
	return LBFSKREP_R( aLBFSKREP_R_ParamsPtr );
}

int _stdcall p_LBDAYREP( tLBDAYREP_PARAMS *aLBDAYREP_ParamsPtr )
{
	return LBDAYREP( aLBDAYREP_ParamsPtr );
}

int _stdcall p_LBDBREPRS( char *aName, char aVat, tLBDBREPRS_RESPONSE *aLBDBREPRS_ResponsePtr )
{
	return LBDBREPRS( aName, aVat, aLBDBREPRS_ResponsePtr );
}

int _stdcall p_LBSENDCK( tLBSENDCK_RESPONSE *aLBSENDCK_ResponsePtr )
{
	return LBSENDCK( aLBSENDCK_ResponsePtr );
}
//WM
int _stdcall p_LBFSTRQ22( tLBFSTRS1_RESPONSE *aLBFSTRS1_ResponsePtr )
{
	return LBFSTRQ22( aLBFSTRS1_ResponsePtr );
}

int _stdcall p_LBFSTRQ( tLBFSTRS1_RESPONSE *aLBFSTRS1_ResponsePtr )
{
	return LBFSTRQ( aLBFSTRS1_ResponsePtr );
}

int _stdcall p_O_LBFSTRQ( BYTE *aNrOfVatRatesOutPtr, tO_LBFSTRS_RESPONSE *aO_LBFSTRS_ResponsePtr )
{
	return O_LBFSTRQ( aNrOfVatRatesOutPtr, aO_LBFSTRS_ResponsePtr );
}

int _stdcall p_LBERNRQ( BYTE *aErrCodeOutPtr )
{
	return LBERNRQ( aErrCodeOutPtr );
}

int _stdcall p_LBIDRQ( tLBIDRQ_RESPONSE *aLBIDRQ_ResponsePtr )
{
	return LBIDRQ( aLBIDRQ_ResponsePtr );
}

int _stdcall p_LBSNDMD( BYTE aMode )
{
	return LBSNDMD( aMode );
}

int _stdcall p_LBCASREP()
{
	return LBCASREP();
}

int _stdcall p_LBFSTRQ24( tLBFSTRQ1_RESPONSE *aLBFSTRQ1_ResponsePtr )
{
	return LBFSTRQ24( aLBFSTRQ1_ResponsePtr );
}

int _stdcall p_LBFSTRQ25( tLBFSTRQ25_PARAMS *aLBFSTRQ25_ParamsPtr )
{
	return LBFSTRQ25( aLBFSTRQ25_ParamsPtr );
}

int _stdcall p_LBFSTRQ26( DWORD aRecordNr )
{
	return LBFSTRQ26( aRecordNr );
}

int _stdcall p_LBFSTRQ27( tLBFSTRQ27_RESPONSE *aLBFSTRQ27_ResponsePtr, BYTE *aRecordTypeOutPtr )
{
	return LBFSTRQ27( aLBFSTRQ27_ResponsePtr, aRecordTypeOutPtr );
}


/*******************************************************************************
********************************************************************************
************************ INTERFEJS DO STAREJ BIBLIOTEKI ************************
********************************************************************************
*******************************************************************************/

/*
	�eby zachowa� zgodno�� ze star� bibliotek� powinienem wy��czy� sprawdzanie
	b��d�w, ale niestety (?) korzystam z funkcji z nowej biblioteki. A nie chc�
	pisa� wszystkiego od nowa...

	Czyli: jedyne co mog� zrobi�, to nie umieszcza� �adnych ASSERT�w.	
	Wtedy zgodno�� b�dzie wi�ksza...
*/

/*----------------------------------------------------------------------------*/

int ExtNumConv( LPVOID aDwordParamPtr, char *aOutpBuffer )
{
	DWORD Val, Intg, Fact;

	Val = *( (DWORD *) aDwordParamPtr );

	Intg = Val / 100;		/* no i nie b�dziemy robi� nic na zmiennoprzecinkowych */
	Fact = Val % 100;

	sprintf( aOutpBuffer, "%d.%02d", Intg, Fact );
	return SOK;
}


LONG FloatToLong100( char *aInput )						// UWAGA: kropka musi by� !!!
{
	DWORD Intg, Fact;

	if( aInput[0] == '.' )											// .YY
	{
		Intg = 0;
		sscanf( aInput + 1, "%d", & Fact );
	}
	else if ( aInput[strlen(aInput) - 1] == '.' )		// XX.
	{
		Fact = 0;
		sscanf( aInput, "%d", & Intg );
	}
	else																				// XX.YY
	{
		sscanf( aInput, "%d.%d", & Intg, & Fact );
	}

	return (Intg * 100) + Fact;

}


/*----------------------------------------------------------------------------*/

BOOL _stdcall DfConnect(BYTE pNum)
{
	int RetI;
	char PortName[8];
	DWORD ExtErrCode;

#ifdef _DEBUG
	_asm int 3;
#endif

	sprintf( PortName, "COM%d", pNum );

	RetI = Port_Open( PortName, CBR_9600 );
	if( RetI == SOK )
	{
		RetI = LBSERM( 3 );
		ExtErrCode = GetDeviceError();
		Send_CAN();
		RetI = LBTREXITCAN( NULL, NULL );
		ExtErrCode = GetDeviceError();
	}

	if( RetI != SOK )
	{
		ExtErrCode = GetDeviceError();
		Port_Close();
	}

	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfDisconnect(void)
{
	Port_Close();
	return TRUE;
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfDayRepExt(BYTE py, BYTE pm, BYTE pd)
{
	int RetI;
	tLBDAYREP_PARAMS Params;

	Params._Date = 1;
	Params.Py = py;
	Params.Pm = pm;
	Params.Pd = pd;
	Params.CashRegNr = NULL;
	Params.Cashier = NULL;

	RetI = LBDAYREP( & Params );

	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfDayRepStd(void)
{
	int RetI;
	tLBDAYREP_PARAMS Params;

	Params._Date = 0;
	Params.Py = 0;
	Params.Pm = 0;
	Params.Pd = 0;
	Params.CashRegNr = NULL;
	Params.Cashier = NULL;

	RetI = LBDAYREP( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfFskRep(BYTE py1, BYTE pm1, BYTE pd1,  BYTE py2, BYTE pm2, BYTE pd2, BYTE pt)
{
	int RetI;
	tLBFSKREP_D_PARAMS Params;

	Params.Py1 = py1;
	Params.Pm1 = pm1;
	Params.Pd1 = pd1;
	Params.Py2 = py2;
	Params.Pm2 = pm2;
	Params.Pd2 = pd2;
	Params.Pt = pt;
	Params.CashRegNr = NULL;
	Params.Cashier = NULL;

	RetI = LBFSKREP_D(  & Params ); 
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfCshRep(BYTE ps, char *szZmiana, char *szKasjer)
{
	int RetI;
	tLBCSHREP_PARAMS Params;

	Params.Ps = 0;
	Params.Shift = szZmiana;
	Params.Cashier = szKasjer;
	Params.CashRegNr = NULL;

	RetI = LBCSHREP( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfCshSts(BYTE ps)
{
	int RetI;
	RetI = LBCSHSTS( NULL, NULL );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfIncCsh(BYTE ps, long wpl)
{
	int RetI;
	tLBCSH_PARAMS Params;

	Params._dummy = 0;
	Params.Payment.TypeTranslFuncPtr = ExtNumConv;
	Params.Payment.ParamValuePtr = & wpl;
	Params.CashRegNr = NULL;
	Params.Cashier = NULL;

	RetI = LBINCCSH( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfDecCsh(BYTE ps, long wyp)
{
	int RetI;
	tLBCSH_PARAMS Params;

	Params._dummy = 0;
	Params.Payment.TypeTranslFuncPtr = ExtNumConv;
	Params.Payment.ParamValuePtr = & wyp;
	Params.CashRegNr = NULL;
	Params.Cashier = NULL;

	RetI = LBDECCSH( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfCshRepExt(BYTE pk,BYTE pc, BYTE pb,
							char * szZmiana, char *szKasjer, 
							char *szKarta1, char *szKarta2, char *szKarta3, char *szKarta4, 
							char *szKarta5, char *szKarta6, char *szKarta7, char *szKarta8, 
							char *szCzek1, char *szCzek2, char *szCzek3, char *szCzek4, 
							char *szBon1, char *szBon2, char *szBon3, char *szBon4, 
							char *szPocz, char *szKoniec, 
							long przych, long gotow, 
							long *pKarty, long *pCzeki, long *pBony, 
							long wPlaty, long kaucjaPob, long wYplaty, 
							long kaucjaZw, long stan, 
							char *szNParag,  char* szNAnul, char *szNStorno )
{
	int RetI;
	tLBCSHREP1_PARAMS Params;

	Params.Pk											= pk;
	Params.Pc											= pc;
	Params.Pb											= pb;
	Params.Shift									= szZmiana;
	Params.Cashier								= szKasjer;

	Params.CardName1							= szKarta1;
	Params.CardName2							= szKarta2;
	Params.CardName3							= szKarta3;
	Params.CardName4							= szKarta4;
	Params.CardName5							= szKarta5;
	Params.CardName6							= szKarta6;
	Params.CardName7							= szKarta7;
	Params.CardName8							= szKarta8;

	Params.ChequeName1						= szCzek1;
	Params.ChequeName2						= szCzek2;
	Params.ChequeName3						= szCzek3;
	Params.ChequeName4						= szCzek4;

	Params.CouponName1						= szBon1;
	Params.CouponName2						= szBon2;
	Params.CouponName3						= szBon3;
	Params.CouponName4						= szBon4;

	Params.Beginning							= szPocz;
	Params.End										= szKoniec;

	Params.Takings.ParamValuePtr				= & przych;
	Params.Takings.TypeTranslFuncPtr		= ExtNumConv;
	Params.SalesCash.ParamValuePtr			= & gotow;
	Params.SalesCash.TypeTranslFuncPtr	= ExtNumConv;

	switch( pk )
	{
		case 8:
			Params.Card8.ParamValuePtr					= & pKarty[7];
			Params.Card8.TypeTranslFuncPtr			= ExtNumConv;
		case 7:
			Params.Card7.ParamValuePtr					= & pKarty[6];
			Params.Card7.TypeTranslFuncPtr			= ExtNumConv;
		case 6:
			Params.Card6.ParamValuePtr					= & pKarty[5];
			Params.Card6.TypeTranslFuncPtr			= ExtNumConv;
		case 5:
			Params.Card5.ParamValuePtr					= & pKarty[4];
			Params.Card5.TypeTranslFuncPtr			= ExtNumConv;
		case 4:
			Params.Card4.ParamValuePtr					= & pKarty[3];
			Params.Card4.TypeTranslFuncPtr			= ExtNumConv;
		case 3:
			Params.Card3.ParamValuePtr					= & pKarty[2];
			Params.Card3.TypeTranslFuncPtr			= ExtNumConv;
		case 2:
			Params.Card2.ParamValuePtr					= & pKarty[1];
			Params.Card2.TypeTranslFuncPtr			= ExtNumConv;
		case 1:
			Params.Card1.ParamValuePtr					= & pKarty[0];
			Params.Card1.TypeTranslFuncPtr			= ExtNumConv;
	}

	switch( pk )
	{
		case 0:
			Params.Card1.ParamValuePtr					= (LPVOID) Zero;
			Params.Card1.TypeTranslFuncPtr			= NULL;
		case 1:
			Params.Card2.ParamValuePtr					= (LPVOID) Zero;
			Params.Card2.TypeTranslFuncPtr			= NULL;
		case 2:
			Params.Card3.ParamValuePtr					= (LPVOID) Zero;
			Params.Card3.TypeTranslFuncPtr			= NULL;
		case 3:
			Params.Card4.ParamValuePtr					= (LPVOID) Zero;
			Params.Card4.TypeTranslFuncPtr			= NULL;
		case 4:
			Params.Card5.ParamValuePtr					= (LPVOID) Zero;
			Params.Card5.TypeTranslFuncPtr			= NULL;
		case 5:
			Params.Card6.ParamValuePtr					= (LPVOID) Zero;
			Params.Card6.TypeTranslFuncPtr			= NULL;
		case 6:
			Params.Card7.ParamValuePtr					= (LPVOID) Zero;
			Params.Card7.TypeTranslFuncPtr			= NULL;
		case 7:
			Params.Card8.ParamValuePtr					= (LPVOID) Zero;
			Params.Card8.TypeTranslFuncPtr			= NULL;
	}

	switch( pc )
	{
		case 4:
			Params.Cheque4.ParamValuePtr					= & pCzeki[3];
			Params.Cheque4.TypeTranslFuncPtr			= ExtNumConv;
		case 3:
			Params.Cheque3.ParamValuePtr					= & pCzeki[2];
			Params.Cheque3.TypeTranslFuncPtr			= ExtNumConv;
		case 2:
			Params.Cheque2.ParamValuePtr					= & pCzeki[1];
			Params.Cheque2.TypeTranslFuncPtr			= ExtNumConv;
		case 1:
			Params.Cheque1.ParamValuePtr					= & pCzeki[0];
			Params.Cheque1.TypeTranslFuncPtr			= ExtNumConv;
	}

	switch( pc )
	{
		case 0:
			Params.Cheque1.ParamValuePtr					= (LPVOID) Zero;
			Params.Cheque1.TypeTranslFuncPtr			= NULL;
		case 1:
			Params.Cheque2.ParamValuePtr					= (LPVOID) Zero;
			Params.Cheque2.TypeTranslFuncPtr			= NULL;
		case 2:
			Params.Cheque3.ParamValuePtr					= (LPVOID) Zero;
			Params.Cheque3.TypeTranslFuncPtr			= NULL;
		case 3:
			Params.Cheque4.ParamValuePtr					= (LPVOID) Zero;
			Params.Cheque4.TypeTranslFuncPtr			= NULL;
	}


	switch( pb )
	{
		case 4:
			Params.Coupon4.ParamValuePtr					= & pBony[3];
			Params.Coupon4.TypeTranslFuncPtr			= ExtNumConv;
		case 3:
			Params.Coupon3.ParamValuePtr					= & pBony[2];
			Params.Coupon3.TypeTranslFuncPtr			= ExtNumConv;
		case 2:
			Params.Coupon2.ParamValuePtr					= & pBony[1];
			Params.Coupon2.TypeTranslFuncPtr			= ExtNumConv;
		case 1:
			Params.Coupon1.ParamValuePtr					= & pBony[0];
			Params.Coupon1.TypeTranslFuncPtr			= ExtNumConv;
	}

	switch( pb )
	{
		case 0:
			Params.Coupon1.ParamValuePtr					= (LPVOID) Zero;
			Params.Coupon1.TypeTranslFuncPtr			= NULL;
		case 1:
			Params.Coupon2.ParamValuePtr					= (LPVOID) Zero;
			Params.Coupon2.TypeTranslFuncPtr			= NULL;
		case 2:
			Params.Coupon3.ParamValuePtr					= (LPVOID) Zero;
			Params.Coupon3.TypeTranslFuncPtr			= NULL;
		case 3:
			Params.Coupon4.ParamValuePtr					= (LPVOID) Zero;
			Params.Coupon4.TypeTranslFuncPtr			= NULL;
	}

	Params.PaymentIn.ParamValuePtr			= & wPlaty;
	Params.PaymentIn.TypeTranslFuncPtr	= ExtNumConv;

	Params.DepositTaken.ParamValuePtr			= & kaucjaPob;
	Params.DepositTaken.TypeTranslFuncPtr	= ExtNumConv;

	Params.PaymentOut.ParamValuePtr				= & wYplaty;
	Params.PaymentOut.TypeTranslFuncPtr		= ExtNumConv;

	Params.DepositReturned.ParamValuePtr			= & kaucjaZw;
	Params.DepositReturned.TypeTranslFuncPtr	= ExtNumConv;

	Params.Cash.ParamValuePtr									= & stan;
	Params.Cash.TypeTranslFuncPtr							= ExtNumConv;

	Params.NrOfReceipts						= szNParag;
	Params.NrOfCancelledReceipts	= szNAnul;
	Params.NrOfCancelledItems			= szNStorno;
	Params.CashRegNr							= NULL;	// tego nie b�dzie w ramce

	RetI = LBCSHREP1( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfDBRepTest(char *szName, char *szPtu)
{
	int RetI;
	tLBDBREP_PARAMS Params;
	BYTE Result;

	Params.Ps = 1;
	Params.Name = szName;
	Params.Vat = szPtu;
	// reszta jest zerowana w LBDBREP

	RetI = LBDBREP( & Params, & Result );

	if( RetI == SOK )
	{
		return ( Result == 1 );
	}
	return ( RetI == SOK );
}

BOOL _stdcall DfDBRepEnd(void)
{
	int RetI;
	tLBDBREP_PARAMS Params;

	Params.Ps = 2;
	Params.Cashier = NULL;
	Params.CashRegNr = NULL;
	// reszta jest zerowana w LBDBREP

	RetI = LBDBREP( & Params, NULL );
	return ( RetI == SOK );
}

BOOL _stdcall DfDBRepStart(void)
{
	int RetI;
	tLBDBREP_PARAMS Params;

	Params.Ps = 0;		// w starej bibliotece Ps r�wne jest 0
	// reszta jest zerowana w LBDBREP

	RetI = LBDBREP( & Params, NULL );
	return ( RetI == SOK );
}


/*----------------------------------------------------------------------------*/

BOOL _stdcall DfTrsHdr(BYTE pl, BYTE pn, char *l1, char *l2, char *l3)
{
	int RetI;
	tLBTRSHDR_A_PARAMS Params;

	Params.Pl			= pl;
	Params.Pn			= pn;
	Params.Line1	= l1;
	Params.Line2	= l2;
	Params.Line3	= l3;

	RetI = LBTRSHDR_A( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfTrsLnSmp(BYTE pi,
		    				char *szName, char* szIl, char cPtu,
								long cen, long brt)
{
	int RetI;
	tLBTRSLN_PARAMS Params;

	Params.Pi = pi;
	Params.Pr = 0;
	Params.Name = szName;
	Params.Quantity.TypeTranslFuncPtr = NULL;
	Params.Quantity.ParamValuePtr = szIl;
	Params.Vat = cPtu;
	Params.Price.TypeTranslFuncPtr = ExtNumConv;
	Params.Price.ParamValuePtr = & cen;
	Params.Gross.TypeTranslFuncPtr = ExtNumConv;
	Params.Gross.ParamValuePtr = & brt;
	Params.Discount.TypeTranslFuncPtr = NULL;
	Params.Discount.ParamValuePtr = (LPVOID) Zero;

	RetI = O_LBTRSLN( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfTrsLnExt(BYTE pi, BYTE pr,
								char *szNaz, char* szIl, char cPtu, 
								long cen, long brt,long rab)
{
	int RetI;
	tLBTRSLN_PARAMS Params;

	Params.Pi = pi;
	Params.Pr = pr;
	Params.Name = szNaz;
	Params.Quantity.TypeTranslFuncPtr = NULL;
	Params.Quantity.ParamValuePtr = szIl;
	Params.Vat = cPtu;
	Params.Price.TypeTranslFuncPtr = ExtNumConv;
	Params.Price.ParamValuePtr = & cen;
	Params.Gross.TypeTranslFuncPtr = ExtNumConv;
	Params.Gross.ParamValuePtr = & brt;
	Params.Discount.TypeTranslFuncPtr = ExtNumConv;
	Params.Discount.ParamValuePtr = & rab;

	RetI = O_LBTRSLN( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfTrsExitCancel()
{
	int RetI;
	RetI = LBTREXITCAN( NULL, NULL );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfTrsExitOkStd(BYTE pr, char *szKod, long wpl, long tot)
{
	int RetI;
	tLBTREXIT_A_PARAMS Params;

	Params.Ps		= 1;
	Params.Pr		= pr;
	Params.Code	= szKod;
	Params.PaymentIn.ParamValuePtr			= & wpl;
	Params.PaymentIn.TypeTranslFuncPtr	= ExtNumConv;
	Params.Total.ParamValuePtr					= & tot;
	Params.Total.TypeTranslFuncPtr			= ExtNumConv;

	RetI = LBTREXIT_A( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfTrsExitOkExt(BYTE pr, BYTE pn, BYTE pc, 
								 char* szKod, char *szL1, char *szL2, char *szL3,
									long wpl, long tot)
{
	int RetI;
	tLBTREXIT_B_PARAMS Params;

	Params.Ps		= 1;
	Params.Pr		= pr;
	Params.Pc		= pc;
	Params.Code	= szKod;
	Params.Line1	= szL1;
	Params.Line2	= szL2;
	Params.Line3	= szL3;
	Params.PaymentIn.ParamValuePtr			= & wpl;
	Params.PaymentIn.TypeTranslFuncPtr	= ExtNumConv;
	Params.Total.ParamValuePtr					= & tot;
	Params.Total.TypeTranslFuncPtr			= ExtNumConv;

	RetI = LBTREXIT_B( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfTrsExitOkFull(BYTE pr, BYTE pn, BYTE pc, BYTE px, BYTE py,
			      char *szKod, char *szL1, char *szL2, char *szL3,
			      long wpl, long tot, long rbt)
{
	int RetI;
	tLBTREXIT_PARAMS Params;

	Params.Ps		= 1;
	Params.Pr		= pr;
	Params.Pn		= pn;
	Params.Pc		= pc;
	Params.Px		= px;
	Params.Py		= py;
	Params.Code	= szKod;
	Params.Line1	= szL1;
	Params.Line2	= szL2;
	Params.Line3	= szL3;
	Params.PaymentIn.ParamValuePtr			= & wpl;
	Params.PaymentIn.TypeTranslFuncPtr	= ExtNumConv;
	Params.Total.ParamValuePtr					= & tot;
	Params.Total.TypeTranslFuncPtr			= ExtNumConv;
	Params.Discount.ParamValuePtr				= & rbt;
	Params.Discount.TypeTranslFuncPtr		= ExtNumConv;

	RetI = LBTREXIT_C( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfTrsXEnd(BYTE pn, BYTE pc, BYTE py, BYTE px, BYTE pg, 
								BYTE pk, BYTE pz, BYTE pb, BYTE po1, BYTE po2, BYTE pr,
								char *szKod, 
								char *szL1, char *szL2, char *szL3, char *szL4, char *szL5,
								char *szKarta, char *szCzek, char *szBon,
								long tot, long rab, long wpl,
								long karta, long czek, long bon, 
								long kaucjaP, long kaucjaZ, long resz)
{
	int RetI;
	tO_LBTRXEND_PARAMS Params;

	char *Line[5];

	Line[0]						= szL1;
	Line[1]						= szL2;
	Line[2]						= szL3;
	Line[3]						= szL4;
	Line[4]						= szL5;

	Params.Pn					= pn;
	Params.Pc					= pc;
	Params.Py					= py;
	Params.Px					= px;
	Params.Pg					= pg;
	Params.Pk					= pk;
	Params.Pz					= pz;
	Params.Pb					= pb;
	Params.Po1				= po1;
	Params.Po2				= po2;
	Params.Pr					= pr;
	Params.Code				= szKod;
	Params.Line				= Line;
	Params.CardName		= szKarta;
	Params.ChequeName	= szCzek;
	Params.CouponName	= szBon;

	Params.Total.TypeTranslFuncPtr			= ExtNumConv;
	Params.Total.ParamValuePtr					= & tot;
	Params.Discount.TypeTranslFuncPtr		= ExtNumConv;
	Params.Discount.ParamValuePtr				= & rab;
	Params.Cash.TypeTranslFuncPtr				= ExtNumConv;
	Params.Cash.ParamValuePtr						= & wpl;
	Params.Card.TypeTranslFuncPtr				= ExtNumConv;
	Params.Card.ParamValuePtr						= & karta;
	Params.Cheque.TypeTranslFuncPtr			= ExtNumConv;
	Params.Cheque.ParamValuePtr					= & czek;
	Params.Coupon.TypeTranslFuncPtr			= ExtNumConv;
	Params.Coupon.ParamValuePtr					= & bon;
	Params.DepositTaken.TypeTranslFuncPtr		= ExtNumConv;
	Params.DepositTaken.ParamValuePtr				= & kaucjaP;
	Params.DepositReturned.TypeTranslFuncPtr	= ExtNumConv;
	Params.DepositReturned.ParamValuePtr		= & kaucjaZ;
	Params.Rest.TypeTranslFuncPtr						= ExtNumConv;
	Params.Rest.ParamValuePtr								= & resz;

	RetI = O_LBTRXEND( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfTrsCard(BYTE ps, BYTE pn, char *szKasa, char *szKasjer, 
								char *szPar, char *szKontr, char *szTerm, 
								char *szKarta, char *szNKarta, 
								char *szM, char *szR,  char *szKod, long kwota )
{
	int RetI;
	tLBCARD_PARAMS Params;

	Params.Ps					= ps;
	Params.Pn					= pn;
	Params.CashRegNr	= szKasa;
	Params.CashierNr	= szKasjer;
	Params.ReceiptNr	= szPar;
	Params.Contract		= szKontr;
	Params.Terminal		= szTerm;
	Params.CardName		= szKarta;
	Params.CardNr			= szNKarta;
	Params.DateMM			= szM;
	Params.DateYY			= szR;
	Params.AuthCode		= szKod;
	Params.Amount.TypeTranslFuncPtr	= ExtNumConv;
	Params.Amount.ParamValuePtr			= & kwota;

	RetI = LBTRSCARD( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfStoCard(BYTE ps, BYTE pn, char *szKasa, char *szKasjer, 
								char *szPar, char *szKontr, char *szTerm, 
								char *szKarta, char *szNKarta, 
								char *szM, char *szR,  char *szKod, long kwota )
{
	int RetI;
	tLBCARD_PARAMS Params;

	Params.Ps					= ps;
	Params.Pn					= pn;
	Params.CashRegNr	= szKasa;
	Params.CashierNr	= szKasjer;
	Params.ReceiptNr	= szPar;
	Params.Contract		= szKontr;
	Params.Terminal		= szTerm;
	Params.CardName		= szKarta;
	Params.CardNr			= szNKarta;
	Params.DateMM			= szM;
	Params.DateYY			= szR;
	Params.AuthCode		= szKod;
	Params.Amount.TypeTranslFuncPtr	= ExtNumConv;
	Params.Amount.ParamValuePtr			= & kwota;

	RetI = LBSTOCARD( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfDSPDep(long kwota)
{
	int RetI;
	tLBDEP_PARAMS Params;

	Params.Amount.TypeTranslFuncPtr	= ExtNumConv;
	Params.Amount.ParamValuePtr			= & kwota;
	Params.Number										= NULL;
	Params.Quantity.TypeTranslFuncPtr = NULL;
	Params.Quantity.ParamValuePtr		= NULL;

	RetI = LBDEP_P( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfCAN(void)
{
	Send_CAN();
	return SOK;
}

/*----------------------------------------------------------------------------*/

BYTE _stdcall DfENQ(void)
{
	BYTE RespByte;
	int RetI;
	tCASH_REGISTER_STATUS Status;

	RetI = GetStatus_CashRegister( & Status );

	if( RetI == SOK )
	{
		RespByte = 0x60 + 
								( Status.FSK ? 0x08: 0 ) +
								( Status.CMD ? 0x04 : 0 ) +
								( Status.PAR ? 0x02 : 0 ) +
								( Status.TRF ? 0x01 : 0 );

		return RespByte;
	}
	else
	{
		return 255;
	}
}

/*----------------------------------------------------------------------------*/

BYTE _stdcall DfDLE(void)
{
	BYTE RespByte;
	int RetI;
	tPRINTER_STATUS Status;

	RetI = GetStatus_Printer( & Status );

	if( RetI == SOK )
	{
		RespByte = 0x70 + 
								( Status.ONL ? 0x04 : 0 ) +
								( Status.PE ? 0x02 : 0 ) +
								( Status.ERR ? 0x01 : 0 );

		return RespByte;
	}
	else
	{
		return 255;
	}
}

/*----------------------------------------------------------------------------*/

BYTE _stdcall DfErNRq(void)
{
	BYTE ErrCode;
	int RetI;
	RetI = LBERNRQ( & ErrCode );

	if( RetI == SOK )
	{
		return ErrCode;
	}
	else
	{
		return 255;
	}
}

/*----------------------------------------------------------------------------*/
// Trzeba by�o O_LBFSTRQ rozbebeszy�, �eby m�c zaimplementowa� to DfFStRq...

BOOL _stdcall DfFStRq(char* szBuff)
{
	static int RetI;
	static tLBFSTRQ_PARAMS LBFSTRQparams;
	static BYTE ProtocolType, NrOfVatRates;

	ProtocolType = PROTOCOL_OLD_NCHK_RESP_NCHK;		/* w starej homologacji nie ma sumy kontr. */

	LBFSTRQparams.Ps = 0;					/* Ps w starej homologacji jest ignorowane */

/*
	Translacja na posta� wewn�trzn�
*/
	RetI = CThCmd.Translate( ProtocolType, LBFSTRQ_CMD_STR,
														sizeof(LBFSTRQ_Descr), LBFSTRQ_Descr, (BYTE *) & LBFSTRQparams );
	if( RetI != SOK)
	{
		return FALSE;
	}

/*
	Wys�anie sekwencji
*/
	RetI = CThCmd.Send();
	if( RetI != SOK )
	{
		return FALSE;
	}

/*
	Odebranie odpowiedzi
*/
	RetI = CThResp.Receive( ProtocolType, TRUE, NORMAL_RESPONSE_TIME );
	if( RetI != SOK )
	{
		CThResp.ClearResponseStructPtr();		/* trzeba r�cznie wyzerowa� wska�nik ! */
		return FALSE;
	}

	BYTE *DataPtr;
	DWORD DataLen;

	CThResp.GetDataFieldDescr( & DataLen, & DataPtr );
	memcpy( szBuff, DataPtr, DataLen );

	return TRUE;
}

/*----------------------------------------------------------------------------*/
// Tu mo�na wykorzysta� O_LBFSTRQ...

BOOL _stdcall DfFStRqDecode(BYTE *ptuNum, BYTE *pe, BYTE *pp, BYTE *pt, BYTE *px, BYTE *pz, 
								BYTE *py, BYTE *pm, BYTE *pd, 
								long *tblPtu, long *par, long *tblTot,
								unsigned long *csh, char *szNNN )
{
	int RetI;
	BYTE NrOfVatRates, i;
	tO_LBFSTRS_RESPONSE Resp;

	char *VatRates[6],
			*Totalizers[6],
			*Cash;

	RetI = O_LBFSTRQ( & NrOfVatRates, & Resp );

	if( RetI == SOK )
	{
		// paramatery bajtowe s� niezale�ne od liczby stawek VAT, a w strukturach wyst�puj�
		// w tym samym miejscu
		*ptuNum	= NrOfVatRates;

		*pe			= Resp.NrOfRatesEq1.Pe;
		*pp			= Resp.NrOfRatesEq1.Pm;
		*pt			= Resp.NrOfRatesEq1.Pt;
		*px			= Resp.NrOfRatesEq1.Px;
		*pz			= Resp.NrOfRatesEq1.Pz;
		*py			= Resp.NrOfRatesEq1.Pyy;
		*pm			= Resp.NrOfRatesEq1.Pmm;
		*pd			= Resp.NrOfRatesEq1.Pdd;

		switch( NrOfVatRates )
		{
			case 1:
				VatRates[0]		= Resp.NrOfRatesEq1.VatA;
				Totalizers[0]	= Resp.NrOfRatesEq1.TotA;
				Cash					= Resp.NrOfRatesEq1.Cash;
				*par					= Resp.NrOfRatesEq1.NrOfReceipts;
				strcpy( szNNN, Resp.NrOfRatesEq1.SerialNr );
				break;
			
			case 2:
				VatRates[0]		= Resp.NrOfRatesEq2.VatA;
				Totalizers[0]	= Resp.NrOfRatesEq2.TotA;
				VatRates[1]		= Resp.NrOfRatesEq2.VatB;
				Totalizers[1]	= Resp.NrOfRatesEq2.TotB;
				Cash					= Resp.NrOfRatesEq2.Cash;
				*par					= Resp.NrOfRatesEq2.NrOfReceipts;
				strcpy( szNNN, Resp.NrOfRatesEq2.SerialNr );
				break;
			
			case 3:
				VatRates[0]		= Resp.NrOfRatesEq3.VatA;
				Totalizers[0]	= Resp.NrOfRatesEq3.TotA;
				VatRates[1]		= Resp.NrOfRatesEq3.VatB;
				Totalizers[1]	= Resp.NrOfRatesEq3.TotB;
				VatRates[2]		= Resp.NrOfRatesEq3.VatC;
				Totalizers[2]	= Resp.NrOfRatesEq3.TotC;
				Cash					= Resp.NrOfRatesEq3.Cash;
				*par					= Resp.NrOfRatesEq3.NrOfReceipts;
				strcpy( szNNN, Resp.NrOfRatesEq3.SerialNr );
				break;
			
			case 4:
				VatRates[0]		= Resp.NrOfRatesEq4.VatA;
				Totalizers[0]	= Resp.NrOfRatesEq4.TotA;
				VatRates[1]		= Resp.NrOfRatesEq4.VatB;
				Totalizers[1]	= Resp.NrOfRatesEq4.TotB;
				VatRates[2]		= Resp.NrOfRatesEq4.VatC;
				Totalizers[2]	= Resp.NrOfRatesEq4.TotC;
				VatRates[3]		= Resp.NrOfRatesEq4.VatD;
				Totalizers[3]	= Resp.NrOfRatesEq4.TotD;
				Cash					= Resp.NrOfRatesEq4.Cash;
				*par					= Resp.NrOfRatesEq4.NrOfReceipts;
				strcpy( szNNN, Resp.NrOfRatesEq4.SerialNr );
				break;
			
			case 5:
				VatRates[0]		= Resp.NrOfRatesEq5.VatA;
				Totalizers[0]	= Resp.NrOfRatesEq5.TotA;
				VatRates[1]		= Resp.NrOfRatesEq5.VatB;
				Totalizers[1]	= Resp.NrOfRatesEq5.TotB;
				VatRates[2]		= Resp.NrOfRatesEq5.VatC;
				Totalizers[2]	= Resp.NrOfRatesEq5.TotC;
				VatRates[3]		= Resp.NrOfRatesEq5.VatD;
				Totalizers[3]	= Resp.NrOfRatesEq5.TotD;
				VatRates[4]		= Resp.NrOfRatesEq5.VatE;
				Totalizers[4]	= Resp.NrOfRatesEq5.TotE;
				Cash					= Resp.NrOfRatesEq5.Cash;
				*par					= Resp.NrOfRatesEq5.NrOfReceipts;
				strcpy( szNNN, Resp.NrOfRatesEq5.SerialNr );
				break;
			
			case 6:
				VatRates[0]		= Resp.NrOfRatesEq6.VatA;
				Totalizers[0]	= Resp.NrOfRatesEq6.TotA;
				VatRates[1]		= Resp.NrOfRatesEq6.VatB;
				Totalizers[1]	= Resp.NrOfRatesEq6.TotB;
				VatRates[2]		= Resp.NrOfRatesEq6.VatC;
				Totalizers[2]	= Resp.NrOfRatesEq6.TotC;
				VatRates[3]		= Resp.NrOfRatesEq6.VatD;
				Totalizers[3]	= Resp.NrOfRatesEq6.TotD;
				VatRates[4]		= Resp.NrOfRatesEq6.VatE;
				Totalizers[4]	= Resp.NrOfRatesEq6.TotE;
				VatRates[5]		= Resp.NrOfRatesEq6.VatF;
				Totalizers[5]	= Resp.NrOfRatesEq6.TotF;
				Cash					= Resp.NrOfRatesEq6.Cash;
				*par					= Resp.NrOfRatesEq6.NrOfReceipts;
				strcpy( szNNN, Resp.NrOfRatesEq6.SerialNr );
				break;
		}

		*csh = FloatToLong100( Cash );

		for( i =0; i < NrOfVatRates; i++ )
		{
			tblPtu[i] = FloatToLong100( VatRates[i] );
			tblTot[i] = FloatToLong100( Totalizers[i] );
		}
	}

	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfSErMStd(BYTE ps)
{
	int RetI;
	RetI = LBSERM( ps );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/
// Tu czego� nie rozumiem... Po co kto� wysy�a LBSERM i odczytuje status jego wykonania ???
// Stara biblioteka to jaki� kit...

BOOL _stdcall DfSErMExt(BYTE *p, char *c1, char *c2)
{
	int RetI;
	DWORD ErrCode;

	*c1	= 0;
	*c2	= 0;
	RetI = LBSERM( *p );
	*p	= 0;
	
	if( RetI == ERR_RETURN_CODE )
	{
		ErrCode = GetDeviceError();
		*p = LOBYTE(LOWORD(ErrCode));
		*c1 = HIBYTE(HIWORD(ErrCode));
		*c2 = LOBYTE(HIWORD(ErrCode));
	}
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/
// Mo�e lepiej zrobi� jak�� za�lepk� ???
// W nowej bibliotece zawsze si� oczekuje na odpowied� i j� analizuje...

BOOL _stdcall DfSErSts(BYTE *p, char *c1, char *c2)
{
	int RetI;
	DWORD ErrCode;
	
	*c1	= 0;
	*c2	= 0;
	*p	= 0;
	
/*
	Odebranie odpowiedzi
*/
	RetI = CThResp.Receive( PROTOCOL_OLD_NCHK_RESP_NCHK, FALSE, NORMAL_RESPONSE_TIME );		// odpowied� bez danych
	if( RetI != SOK )
	{
		CThResp.ClearResponseStructPtr();		/* trzeba r�cznie wyzerowa� wska�nik ! */

		if( RetI == ERR_RETURN_CODE )
		{
			ErrCode = GetDeviceError();
			*p = LOBYTE(LOWORD(ErrCode));
			*c1 = HIBYTE(HIWORD(ErrCode));
			*c2 = LOBYTE(HIWORD(ErrCode));

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	return TRUE;
}

/*----------------------------------------------------------------------------*/
// Skopiowane ze starej biblioteki
#define BEGIN_CASE( x ) switch( (x) ) {
#define CASE_ITEM( n, var, x )      case (n): (var) = (x); break
#define CASE_DEFAULT( var, x )      default:  (var) = (x)
#define END_CASE }

BOOL _stdcall DfErrorDescription(BYTE eNum, char *szError,char *szExtError)
{
	char *msg;  
  
	strcpy(szExtError,"");
	
	BEGIN_CASE(eNum)
		CASE_ITEM(0,msg,"Rozkaz nierozpoznany");
		CASE_ITEM(1,msg,"Nie zainicjalizowany zegar RTC");
		CASE_ITEM(2,msg,"B��d bajtu kontrolnego");
		CASE_ITEM(3,msg,"Z�a ilo�� parametr�w");
		CASE_ITEM(4,msg,"B��d danych");
		CASE_ITEM(5,msg,"B��d wykonania (zapisu) do zegara RTC lub b��d odczytu zegara RTC");
		CASE_ITEM(6,msg,"B��d odczytu totalizer�w, b��d operacji z kas� fiskaln�");
		CASE_ITEM(7,msg,"Data wcze�niejsza od daty ostatniego zapisu w pami�ci fiskalnej");
		CASE_ITEM(8,msg,"B��d operacji - niezerowe totalizery");
		CASE_ITEM(9,msg,"B��d operacji I/O");
		CASE_ITEM(10,msg,"B��d nieudokumentowany");
		CASE_ITEM(11,msg,"Z�a ilo�� warto�ci, b��d liczby lub zdefiniowanie tych samych "
			"stawek PTU");
		CASE_ITEM(12,msg,"B��d nieudokumentowany");
		CASE_ITEM(13,msg,"Pr�ba fiskalizacji sfiskalizowanego urz�dzenia");
		CASE_ITEM(14,msg,"Pr�ba przes�ania do RAM nag��wka dla urz�dzenia sfiskalizowanego");
		CASE_ITEM(15,msg,"B��d w dodatkowych liniach nag��wka");
		CASE_ITEM(16,msg,"B��dna nazwa");
		CASE_ITEM(17,msg,"B��dne oznaczenie ilo�ci");
		CASE_ITEM(18,msg,"B��dne oznaczenie stawki PTU, tak�e pr�ba sprzeda�y w stawce "
			"nieaktywnej");
		CASE_ITEM(19,msg,"B��d warto�ci CENA");
		CASE_ITEM(20,msg,"B��d warto�ci BRUTTO albo RABAT lub b��d niespe�nienia warunku: "
			"ilo�� x cena = warto��, albo uwzgl�dnienie rabatu prowadzi do "
			"ujemnego wyniku");
		CASE_ITEM(21,msg,"Sekwencja odebrana przez drukark� przy wy��czonym trybie transakcji");
		CASE_ITEM(22,msg,"B��d operacji STORNO lub b��d operacji z rabatem");
		CASE_ITEM(23,msg,"B��dna liczba rekord�w paragonu");
		CASE_ITEM(24,msg,"Przepe�nienie bufora drukarki");
		CASE_ITEM(25,msg,"B��dny kod terminala/kasjera, lub b��dna tre�� dodatkowych linii");
		CASE_ITEM(26,msg,"B��d kwoty WPLATA");
		CASE_ITEM(27,msg,"B��dna suma ca�kowita TOTAL lub b��dna kwota RABAT");
		CASE_ITEM(28,msg,"Przepe�nienie totalizera");
		CASE_ITEM(29,msg,"��danie pozytywnego zako�czenia nieaktywnego trybu transakcji");
		CASE_ITEM(30,msg,"B��d kwoty WP�ATA");
		CASE_ITEM(31,msg,"Nadmiar dodawania");
		CASE_ITEM(32,msg,"Warto�� po odj�ciu staje si� ujemna (stan staje si� zerowy!)");
		CASE_ITEM(33,msg,"B��d napisu <zmiana> lub <kasjer> lub <numer> lub <kaucja>");
		CASE_ITEM(34,msg,"B��d jednej z kwot lub pozosta�ych napis�w");
		CASE_ITEM(35,msg,"Zerowy stan totalizer�w");
		CASE_ITEM(36,msg,"Ju� istnieje zapis o tej dacie");
		CASE_ITEM(37,msg,"Operacja przerwana z klawiatury");
		CASE_ITEM(38,msg,"B��d nazwy");
		CASE_ITEM(39,msg,"B��d oznaczenia PTU");
		CASE_ITEM(40,msg,"Brak w pami�ci RAM nag��wka lub wyst�pienie b��du blokuj�cego "
			"tryb fiskalny");
		CASE_ITEM(41,msg,"B��d napisu <numer_kasy>");
		CASE_ITEM(42,msg,"B��d napisu <numer_kasjera>");
		CASE_ITEM(43,msg,"B��d napisu <numer_par>");
		CASE_ITEM(44,msg,"B��d napisu <kontrahent>");
		CASE_ITEM(45,msg,"B��d napisu <terminal>");
		CASE_ITEM(46,msg,"B��d napisu <nazwa_karty>");
		CASE_ITEM(47,msg,"B��d napisu <numer_karty>");
		CASE_ITEM(48,msg,"B��d napisu <data_m>");
		CASE_ITEM(49,msg,"B��d napisu <data_r>");
		CASE_ITEM(50,msg,"B��d napisu <kod_autoryz>");
		CASE_ITEM(51,msg,"B��d warto�ci <kwota>");
		CASE_DEFAULT(msg,"B��d nieudokumentowany");
	END_CASE
	
	strcpy(szError, msg);
	
	// Nie ma u mnie czego� takiego jak _errorMsg i nie b�dzie !!!
/*
	if( _error != ERROR_NONE )
	{
		strcpy(szExtError,_errorMsg);
	}
*/

	return TRUE;
}

/*----------------------------------------------------------------------------*/

BOOL DfSendMD( BYTE aPs )				// DfSendMD - cz�� wsp�lna dla DfSendMDOn i Off
{
	int RetI;
	tLBSNDMD_PARAMS_INT Params;		// ca�e szcz�cie, �e struktury i opis pasuj� do nowej homologacji
																// tyle, �e rozkaz co innego robi ;-)
	Params.Ps = aPs;

	RetI = CThCmd.Translate( PROTOCOL_OLD_NCHK_RESP_NCHK, O_LBSNDMD_CMD_STR,
														sizeof(LBSNDMD_Descr), LBSNDMD_Descr, (BYTE *) & Params );
	if( RetI != SOK)
	{
		return FALSE;
	}

	RetI = Execute( & CThCmd, & CThResp, NORMAL_RESPONSE_TIME );
	return ( RetI == SOK );
}

BOOL _stdcall DfSendMDOn(void)
{
	return DfSendMD( 94 );
}

BOOL _stdcall DfSendMDOff(void)
{
	return DfSendMD( 95 );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall	DfIdRq(char *szTyo, char *szWer)
{
	int RetI;
	tLBIDRQ_RESPONSE Resp;

	RetI = LBIDRQ( & Resp );
	if( RetI == SOK )
	{
		strcpy( szTyo, Resp.Type );
		strcpy( szWer, Resp.Version );
	}

	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfBel(void)
{
	Send_BEL();
	return TRUE;
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfFeed(BYTE ps)
{
	int RetI;
	RetI = LBFEED( ps );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/
// Ps jest ignorowane !!!
BOOL _stdcall DfSetHdr(BYTE ps, char* szBuff)
{
	int RetI;

	RetI = LBSETHDR( szBuff, NULL, NULL );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfSetPTU(BYTE ps, long *ptu)
{
	int RetI;
	BYTE i;
	tLBSETPTU_PARAMS Params;
	tEXTERNAL_NUMERIC Rates[6];

	Params.Ps					= ps;
	Params._Date			= 0;				// Py, Pm i Pd b�d� zignorowane
	Params.Cashier		= NULL;			// bez kasjera i numeru kasy
	Params.CashRegNr	= NULL;
	Params.VatRates		= Rates;

	for( i = 0; i < ps; i++ )
	{
		Rates[i].ParamValuePtr			= & ptu[i];
		Rates[i].TypeTranslFuncPtr	= ExtNumConv;
	}

	RetI = LBSETPTU( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfLogin(BYTE ps, char *szKasjer, char *szNr)
{
	int RetI;
	tLBLOG_PARAMS Params;

	Params._dummy			= 0;
	Params.Cashier		= szKasjer;
	Params.CashRegNr	= szNr;

	RetI = LBLOGIN( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfLogout(BYTE ps, char *szKasjer, char *szNr)
{
	int RetI;
	tLBLOG_PARAMS Params;

	Params._dummy			= 0;
	Params.Cashier		= szKasjer;
	Params.CashRegNr	= szNr;

	RetI = LBLOGOUT( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfOpak(BYTE ps, char* szBuff)
{
	int RetI;
	t_O_LBOPAK_PARAMS Params;

	Params.DepositReturned = szBuff;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, O_LBOPAK_CMD_STR,
														sizeof(O_LBOPAK_Descr), O_LBOPAK_Descr,
													(BYTE *) & Params );
	if( RetI != SOK)
	{
		return FALSE;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfStoCsh(long wyp)
{
	int RetI;
	t_O_LBSTOCSH_PARAMS Params;

	Params.Returned.ParamValuePtr			= & wyp;
	Params.Returned.TypeTranslFuncPtr	= ExtNumConv;

	RetI = CThCmd.Translate( PROTOCOL_OLD_CHK_RESP_NCHK, O_LBSTOCSH_CMD_STR,
														sizeof(O_LBSTOCSH_Descr), O_LBSTOCSH_Descr,
													(BYTE *) & Params );
	if( RetI != SOK)
	{
		return FALSE;
	}

	RetI = Execute( & CThCmd, & CThResp );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfSendCk(WORD *wYear, WORD *wMonth, WORD *wDay, 
								WORD *wHour, WORD *wMinute, WORD *wSecond )
{
	int RetI;
	tLBSENDCK_RESPONSE Resp;

	RetI = LBSENDCK( & Resp );
	if( RetI == SOK )
	{
		*wYear		= Resp.Pyy;
		*wMonth		= Resp.Pmm;
		*wDay			= Resp.Pdd;
		*wHour		= Resp.Ph;
		*wMinute	= Resp.Pm;
		*wSecond	= Resp._Ps;
	}

	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfSetCk(WORD wYear, WORD wMonth, WORD wDay, 
								WORD wHour, WORD wMinute, WORD wSecond)
{
	int RetI;
	tLBSETCK_PARAMS Params;

	Params.Cashier		= NULL;
	Params.CashRegNr	= NULL;

	Params.Py		= wYear;
	Params.Pm		= wMonth;
	Params.Pd		= wDay;
	Params.Ph		= wHour;
	Params.Pmn	= wMinute;
	Params.Ps		= wSecond;

	RetI = LBSETCK( & Params );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfClearDSP(void)
{
	int RetI;
	RetI = LBDSP( 2 , "" );		// tak jest w oryginale
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/


BOOL _stdcall DfDSP(BYTE par, char *szBuff)
{
	int RetI;
	RetI = LBDSP( par, szBuff );
	return ( RetI == SOK );
}

/*----------------------------------------------------------------------------*/

BOOL _stdcall DfLastCmdOk( void )
{
	int RetI;
	tCASH_REGISTER_STATUS Resp;
	RetI = GetStatus_CashRegister( & Resp );

	if( RetI == SOK )
	{
		return Resp.CMD;
	}
	else
	{
		return FALSE;
	}
}

/*----------------------------------------------------------------------------*/

char _Win1250Tbl[19]	= "����󜿟��ʣ�ӌ��";
char _MazTbl[19]			= "������������������";

char* _stdcall DfWin2Maz(char* szBuff)
{
	DWORD dwLen;
	unsigned i;

	dwLen = strlen(szBuff);

	for( i = 0; i< dwLen; i++)
	{
		if(  (unsigned char) szBuff[i] > 0x80 )
		{
            // Znak mo�e by� polsk� liter�
			for(unsigned j=0; j<sizeof(_Win1250Tbl); j++)
			{
				if( _Win1250Tbl[j] == szBuff[i] )
				{
                    // To jest polska litera - zamiana.
					szBuff[i] = _MazTbl[j];
					break;
				}
			}
		}
	}
	
	return szBuff;
}

/*----------------------------------------------------------------------------*/

char* _stdcall DfMaz2Win(char* szBuff)
{
	DWORD dwLen;

	dwLen = strlen( szBuff );
	for( unsigned i=0; i<dwLen; i++)
    {
		if(  (unsigned char) szBuff[i] > 0x80 )
		{
            // Znak mo�e by� polsk� liter�
			for(unsigned j=0; j<sizeof(_MazTbl); j++)
			{
				if( _MazTbl[j] == szBuff[i] )
				{
                    // To jest polska litera - zamiana.
					szBuff[i] = _Win1250Tbl[j];
					break;
				}
			}
		}
	}

	return szBuff;
}

/*----------------------------------------------------------------------------*/

char* _stdcall DfFillStr(char *buf, BYTE upTo, BYTE align, char c, char e)
{
    int len = strlen(buf);
    char nszBuff[256];
    
    if( upTo > len )
    {
        memcpy(nszBuff,buf,len);
        memset(buf,c,upTo);
        buf[upTo] = '\0';
        switch(align)
        {
            case 0: // string to left              
                memcpy(buf,nszBuff,len);          
            break;
            case 1: // string to right
                memcpy(buf+(upTo-len),nszBuff,len);
                break;
            case 2: // center
                int len2, upTo2;
            
                len2 = len / 2;
                upTo2 = upTo / 2;
            
                memcpy(buf+(upTo2-len2),nszBuff,len2);
                memcpy(buf+upTo2,nszBuff+len2,len2);
            break;
        }
    }
	
    // Znak na ko�cu:
    buf[upTo-1] = e;
	
    return buf;
}

/*----------------------------------------------------------------------------*/
// ZA�LEPKA
BOOL _stdcall DfClearInBuff(void)
{
	return SOK;
}

/*----------------------------------------------------------------------------*/
// ZA�LEPKA
BOOL _stdcall DfClearOutBuff(void)
{
	return SOK;
}

/*----------------------------------------------------------------------------*/
// ZA�LEPKA - jak wyst�pi Timeout to mo�na przecie� zdecydowa� co dalej robi�
// a zreszt� ja wiem lepiej jakie maj� by� Timeout'y
BOOL _stdcall DfSetRTimeouts(DWORD dwConstant, DWORD dwMultiply)
{
	return SOK;
}

