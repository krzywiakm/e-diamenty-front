/* Tab 2 */

/*
	StructDescriptions.h - tablice opisuj�ce struktury parametr�w i struktury 
	zwrotne
*/

#ifndef _STRUCT_DESCRIPTIONS
#define _STRUCT_DESCRIPTIONS

/******************** OPISY STRUKTUR - PROTOKӣ W�OSKI ************************/

/* !!!! RECADD */

/*
	Department database record - field descriptions
*/
tPARAMETER_DESCRIPTION Department_Descr[9] =
{
	{ FLD_STR, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
};


/*
	Forms of payment database record - field descriptions
*/
tPARAMETER_DESCRIPTION FormsOfPayment_Descr[3] =
{
	{ FLD_STR, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
};


/*
	Discount database record - field descriptions
*/
tPARAMETER_DESCRIPTION Discount_Descr[3] =
{
	{ FLD_STR, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
};


/*
	PLU database record - field descriptions
*/
tPARAMETER_DESCRIPTION PLU_Descr[9] =
{
	{ FLD_STR, 0, 0, 0 },
	{ FLD_STR, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
};


/*
	Barcode format database record - field descriptions
*/
tPARAMETER_DESCRIPTION BarcodeFmt_Descr[1] =
{
	{ FLD_STR, 0, 0, 0 },
};


/*
	Cashier database record record - field descriptions
*/
tPARAMETER_DESCRIPTION Cashier_Descr[23] =
{
	{ FLD_STR, 0, 0, 0 },
	{ FLD_STR, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
};

/* !!!! RECGET */

BYTE Department_Resp_Descr[9] =
{
	FLD_STR,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_NUM_EXT,
	FLD_NUM_EXT
};

BYTE FormsOfPayment_Resp_Descr[3] =
{
	FLD_STR,
	FLD_BYT,
	FLD_NUM_EXT
};

BYTE Discount_Resp_Descr[3] =
{
	FLD_STR,
	FLD_NUM_EXT,
	FLD_BYT
};

BYTE PLU_Resp_Descr[9] =
{
	FLD_STR,
	FLD_STR,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT
};

BYTE BarcodeFmt_Resp_Descr[1] =
{
	FLD_STR
};

BYTE Cashier_Resp_Descr[23] =
{
	FLD_STR,
	FLD_STR,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT
};



/*
	LOGIN command parameters
*/
tPARAMETER_DESCRIPTION LOGIN_Descr[1] =
{
	{ FLD_STR, 0, 0, 0 },
};


/*
	SETOPT command parameters
*/
tPARAMETER_DESCRIPTION SETOPT_Descr[2] =
{
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
};


/*
	SETVAT command parameters
*/
tPARAMETER_DESCRIPTION SETVAT_Descr[7] =
{
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
};


/*
	SETDHR command parameters
*/
tPARAMETER_DESCRIPTION SETHDR_Descr[2] =
{
	{ FLD_STR_TAB, 0, 0, 0 },
	{ FLD_BYT_TAB, 0, 0, 0 },
};


/*
	SETADDHDR command parameters
*/
tPARAMETER_DESCRIPTION SETADDHDR_Descr[2] =
{
	{ FLD_STR_TAB, 0, 0, 0 },
	{ FLD_BYT_TAB, 0, 0, 0 },
};


/*
	SETADDFTR command parameters
*/
tPARAMETER_DESCRIPTION SETADDFTR_Descr[2] =
{
	{ FLD_STR_TAB, 0, 0, 0 },
	{ FLD_BYT_TAB, 0, 0, 0 },
};


/*
	SETPWD command parameters
*/
tPARAMETER_DESCRIPTION SETPWD_Descr[2] =
{
	{ FLD_STR, 0, 0, 0 },
	{ FLD_STR, 0, 0, 0 },
};


/*
	RECADD command parameters

	- specjalny przypadek, tRECADD_PARAMS zawiera uni� (DatabaseRecord)
	i dlatego trzeba dokleja� opis konkretnego rekordu

*/
tPARAMETER_DESCRIPTION RECADD_Descr[3] =
{
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_NUM, 0, 0, 0 },
};


/*
	GRSET command parameters
*/
tPARAMETER_DESCRIPTION GRSET_Desr[2] =
{
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
};


/*
	GRSTART command parameters
*/
tPARAMETER_DESCRIPTION GRSTART_Descr[3] =
{
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
};


/*
	GRDATA command parameters
*/
tPARAMETER_DESCRIPTION GRDATA_Descr[2] =
{
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_STR, 0, 0, 0 },
};


/*
	TRSHDR command parameters
*/
tPARAMETER_DESCRIPTION TRSHDR_Descr[1] =
{
	{ FLD_STR, 0, 0, 0 },
};


/*
	TRSLNE command parameters
*/
tPARAMETER_DESCRIPTION TRSLNE_Descr[8] =
{
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_NUM, 0, 0, 0 },
	{ FLD_STR, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
};


/*
	TRSDSC command parameters
*/
tPARAMETER_DESCRIPTION TRSDSC_Descr[3] =
{
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_STR, 0, 0, 0 },
};


/*
	TRSEND command parameters
*/
tPARAMETER_DESCRIPTION TRSEND_Descr[7] =
{
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
};

tPARAMETER_DESCRIPTION S_TRSITEM_Descr[4] =
{
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_NUM, 0, 0, 0 },
	{ FLD_STR, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
};

tPARAMETER_DESCRIPTION S_TRSDSC_Descr[2] =
{
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
};


BYTE SBHDR_Message_Descr[1] =
{
	FLD_BYT
};


BYTE SBITEM_Message_Descr[1] =
{
	FLD_STR
};


BYTE SBLINE_Message_Descr[5] =
{
	FLD_BYT,
	FLD_BYT,
	FLD_NUM,
	FLD_NUM_EXT,
	FLD_NUM_EXT
};


BYTE SBDSC_Message_Descr[2] =
{
	FLD_BYT,
	FLD_NUM_EXT
};


BYTE SBEND_Message_Descr[8] =
{
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_BYT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT
};


/*
	GETSTS command response
*/
BYTE GETSTS_Resp_Descr[17] =
{
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_STR,
	FLD_STR,
	FLD_STR,
	FLD_STR,
	FLD_NUM,
	FLD_NUM,
	FLD_NUM,
	FLD_NUM,
	FLD_NUM,
	FLD_NUM,
	FLD_NUM
};


/*
	GETTOT command response
*/
BYTE GETTOT_Resp_Descr[7] =
{
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
};

tPARAMETER_DESCRIPTION RECGET_Descr[2] =
{
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_NUM, 0, 0, 0 },
};

/******************** OPISY STRUKTUR - PROTOKӣ POLSKI ************************/

tPARAMETER_DESCRIPTION LBSETCK_Descr[8] =
{
	{ FLD_BYT, 0, 0, 99 },
	{ FLD_BYT, 0, 1, 12 },	
	{ FLD_BYT, 0, 1, 31 },
	{ FLD_BYT, 0, 0, 23 },
	{ FLD_BYT, 0, 0, 59 },
	{ FLD_BYT, 0, 0, 59 },
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 32, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBDSP_Descr[2] =
{
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania
	{ FLD_STR, 0, 0, 0 },	// bez sprawdzania
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBSETPTU_Descr[5] =
{
	{ FLD_BYT, 0, 1, 7 },	// Ps == 0 nie dozwolone !
	{ FLD_BYT_TAB, 0, 0, 0 },	// bez sprawdzania
	{ FLD_NUM_EXT_TAB, 6, 3, 2 },
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 32, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBSETHDR_Descr[4] =
{
	{ FLD_BYT, 0, 0, 0 },		// bez sprawdzania (nie potrzebne)
	{ FLD_LIN, 210, 0, 0 },	// 200 + 5 * (CR i pogrubienie)
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 32, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBFEED_Descr[1] =
{
	{ FLD_BYT, 0, 0, 20 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBSERM_Descr[1] =
{
	{ FLD_BYT, 0, 0, 3 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBTRSHDR_Descr[1] =
{
	{ FLD_BYT, 0, 0, 80 },
};

// tylko w ceku zapewnienia zgodno�ci ze star� bibliotek�
tPARAMETER_DESCRIPTION LBTRSHDR_A_Descr[5] =
{
	{ FLD_BYT, 0, 0, 48 },		// hehehe, tutaj jest ograniczenie do 48 linii
	{ FLD_BYT, 0, 0, 3 },
	{ FLD_STR, 40, 0, 0 },
	{ FLD_STR, 40, 0, 0 },
	{ FLD_STR, 40, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBTRSLN_Descr[8] =
{	
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania (nie potrzebne)
	{ FLD_BYT, 0, 0, 4 },	
	{ FLD_STR, 40, 0, 0 },
	{ FLD_NUM_EXT | FLD_SEP_STR, 16, 10, 10 },
	{ FLD_STR | FLD_SEP_NUM, 1, 0, 0 },
	{ FLD_NUM_EXT, 10, 6, 4 },
	{ FLD_NUM_EXT, 10, 6, 4 },
	{ FLD_NUM_EXT, 10, 8, 2 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBDEP_Descr[4] =
{
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania (nie potrzebne)
	{ FLD_NUM_EXT, 10, 8, 2 },	
	{ FLD_STR, 4, 0, 0 },
	{ FLD_NUM_EXT | FLD_SEP_STR, 16, 0, 0 },	// tylko d�ugo��
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBTREXITCAN_Descr[3] =
{
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania (nie potrzebne)
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 32, 0, 0 },	// 17 albo 32 ? hehehe
};

/*----------------------------------------------------------------------------*/

// LBTREXIT_A i _B to stare rzeczy, konieczne do zapewnienia zgodno�ci
// ze star� bibliotek� (w nowej s� tylko najbardziej rozbudowane wersje sekwencji)
tPARAMETER_DESCRIPTION LBTREXIT_A_Descr[5] =
{
	{ FLD_BYT, 0, 0 ,1 },
	{ FLD_BYT, 0, 0, 99 },
	{ FLD_STR, 3, 0 ,0 },	
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },
};

tPARAMETER_DESCRIPTION LBTREXIT_B_Descr[10] =
{
	{ FLD_BYT, 0, 0 ,1 },
	{ FLD_BYT, 0, 0, 99 },
	{ FLD_BYT, 0, 0, 3 },
	{ FLD_BYT, 0, 0, 2 },
	{ FLD_STR, 3, 0 ,0 },	
	{ FLD_STR, 40, 0 ,0 },
	{ FLD_STR, 40, 0 ,0 },
	{ FLD_STR, 40, 0 ,0 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },
};

//
tPARAMETER_DESCRIPTION LBTREXIT_Descr[13] =
{
	{ FLD_BYT, 0, 0 ,1 },
	{ FLD_BYT, 0, 0, 99 },
	{ FLD_BYT, 0, 0, 3 },
	{ FLD_BYT, 0, 0, 2 },
	{ FLD_BYT, 0, 0, 4 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_STR, 3, 0 ,0 },	
	{ FLD_STR, 40, 0 ,0 },
	{ FLD_STR, 40, 0 ,0 },
	{ FLD_STR, 40, 0 ,0 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT, 5, 2, 2 },	// xx.xx - czyli maks: 99.99 %
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION O_LBTREXIT_Descr[1] =
{
	{ FLD_BYT, 0, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION O_LBTRXEND_Descr[25] =
{
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_BYT, 0, 0, 0 },	
	{ FLD_BYT, 0, 0, 0 },
	{ FLD_STR, 0, 0, 0 },
	{ FLD_STR_TAB, 0, 0, 0 },
	{ FLD_STR, 0, 0, 0 },
	{ FLD_STR, 0, 0, 0 },
	{ FLD_STR, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
	{ FLD_NUM_EXT, 0, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBTRFORMPLAT_Descr[4] =
{
	{ FLD_BYT, 0, 1, 2 },
	{ FLD_BYT, 0, 0, 5 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_STR, 15, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBTRXEND1_Descr[29] =
{
	{ FLD_BYT, 0, 0, 3 },
	{ FLD_BYT, 0, 0, 2 },
	{ FLD_BYT, 0, 0, 1 },
	{ FLD_BYT, 0, 0, 1 },
	{ FLD_BYT, 0, 0, 4 },
	{ FLD_BYT, 0, 0, 0 },	// Pkb bez sprawdzania
	{ FLD_BYT, 0, 0, 0 },	// Pkz bez sprawdzania
	{ FLD_BYT, 0, 0, 1 },
	{ FLD_BYT, 0, 0, 0 },	// Pfn bez sprawdzania
	{ FLD_BYT, 0, 0, 1 },
	{ FLD_BYT, 0, 0, 1 },
	{ FLD_BYT_TAB, 0, 1, 5 },
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 32, 0, 0 },
	{ FLD_STR, 0, 0, 0 },	// numer_systemowy bez sprawdzania
	{ FLD_STR_TAB, 40, 0, 0 },
	{ FLD_STR_TAB, 16, 0, 0 },
	{ FLD_STR_TAB, 4, 0, 0 },
	{ FLD_NUM_EXT_TAB | FLD_SEP_STR, 10, 0, 0 },	// ilo��_kaucji - tylko d�ugo��
	{ FLD_STR_TAB, 4, 0, 0 },
	{ FLD_NUM_EXT_TAB | FLD_SEP_STR, 10, 0, 0 },	// ilo��_kaucji - tylko d�ugo��
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },	// RABAT - sprawdzanie jak dla kwoty
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT_TAB, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT_TAB, 10, 8, 2 },
	{ FLD_NUM_EXT_TAB, 10, 8, 2 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBCSHREP1_Descr[50] =
{
	{ FLD_BYT, 0, 0, 8 },
	{ FLD_BYT, 0, 0, 4 },
	{ FLD_BYT, 0, 0, 4 },
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 32, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 15, 0, 0 },
	{ FLD_STR, 15, 0, 0 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_NUM_EXT, 10, 8 ,2 },
	{ FLD_STR, 5, 0, 0 },
	{ FLD_STR, 5, 0, 0 },
	{ FLD_STR, 5, 0, 0 },
	{ FLD_STR, 8, 0 ,0 },
};


/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBCSHREP2_Descr[27] =
{
	{ FLD_BYT, 0, 0, 0 },	// Pkb bez sprawdzania
	{ FLD_BYT, 0, 0, 0 },	// Pkz bez sprawdzania
	{ FLD_BYT, 0, 0, 0 },	// Pfn bez sprawdzania
	{ FLD_BYT, 0, 0, 1 },	
	{ FLD_BYT_TAB, 0, 1, 5 },
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 32, 0, 0 },
	{ FLD_STR, 15, 0, 0 },
	{ FLD_STR, 15, 0, 0 },
	{ FLD_STR_TAB, 0, 0, 0 },	// nazwa_form bez sprawdzania
	{ FLD_STR_TAB, 24, 0, 0 },
	{ FLD_STR_TAB, 24, 0, 0 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT_TAB, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT_TAB, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM_EXT_TAB, 10, 8, 2 },
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_NUM | FLD_SEP_STR, 5, 0, 0 },
	{ FLD_NUM | FLD_SEP_STR, 5, 0, 0 },
	{ FLD_NUM | FLD_SEP_STR, 5, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBCARD_Descr[13] =
{
	{ FLD_BYT, 0, 0, 1 },
	{ FLD_BYT, 0, 0, 2 },
	{ FLD_STR, 2, 0, 0 },
	{ FLD_STR, 5, 0, 0 },
	{ FLD_STR, 12, 0, 0 },	// numer_par - tylko d�ugo��
	{ FLD_STR, 15, 0, 0 },
	{ FLD_STR, 8, 0, 0 },	// kontrahent - tylko d�ugo��
	{ FLD_STR, 16, 0, 0 },
	{ FLD_STR, 20, 0, 0 },	// numer_karty - tylko d�ugo��
	{ FLD_STR, 2, 0, 0 },	// data_m - tylko d�ugo��
	{ FLD_STR, 2, 0, 0 },	// data_r - tylko d�ugo��
	{ FLD_STR, 9, 0, 0 },
	{ FLD_NUM_EXT, 10, 8, 2 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBCSH_Descr[4] =
{
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania (nie potrzebne)
	{ FLD_NUM_EXT, 10, 8, 2 },
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 32, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBCSHSTS_Descr[3] =
{
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania (nie potrzebne)
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 32, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBCSHREP_Descr[4] =
{
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania (zam�cone)
	{ FLD_STR, 8, 0, 0 },	
	{ FLD_STR, 32, 0, 0 },
	{ FLD_STR, 8, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBLOG_Descr[3] =
{
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania (nie potrzebne)
	{ FLD_STR, 32, 0, 0 },
	{ FLD_STR, 8, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBFSKREP_D_Descr[9] =
{
	{ FLD_BYT, 0, 0, 99 },	// Py1 (?)
	{ FLD_BYT, 0, 1, 12 },	
	{ FLD_BYT, 0, 1, 31 },
	{ FLD_BYT, 0, 0, 99 },	// Py2 (?)
	{ FLD_BYT, 0, 1, 12 },
	{ FLD_BYT, 0, 1, 31 },
	{ FLD_BYT, 0, 0, 7 },	// Pt - sprawdzanie uproszczone (przedzia�)
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 32, 0, 0 },
};

tPARAMETER_DESCRIPTION LBFSKREP_R_Descr[5] =
{
	{ FLD_BYT, 0, 16, 17 },
	{ FLD_NUM, 0, 0, 0 },	// Od - bez sprawdzania (?)
	{ FLD_NUM, 0, 0, 0 },	// Do - bez sprawdzania (?)
	{ FLD_STR, 8, 0, 0 },	
	{ FLD_STR, 32, 0, 0 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBDAYREP_Descr[6] =
{
	{ FLD_BYT, 0, 0, 0 },			// bez sprawdzania (nie potrzebne)
	{ FLD_BYT_TAB, 0, 0, 0 },	// sprawdzanie na wy�szym poziomie
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 32, 0, 0 },
};


/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBDBREP_Descr[5] =
{
	{ FLD_BYT, 0, 0, 4 },	// Ps - ograniczone sprawdzanie (przedzia�)
	{ FLD_STR, 40, 0, 0 },
	{ FLD_STR | FLD_SEP_NUM, 1, 0, 0 }, 			/* niestandardowy terminator */
	{ FLD_STR, 8, 0, 0 },
	{ FLD_STR, 32, 0, 0 },
};


// odpowied� na LBDBREP
BYTE LBDBREP_Resp_Descr[1] =
{
	FLD_BYT
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBDBREPRS_Descr[2] =
{
	{ FLD_STR, 40, 0, 0 },
	{ FLD_STR | FLD_SEP_NUM, 1, 0, 0 },			/* niestandardowy terminator */
};

BYTE LBDBREPRS_Resp_Descr[2] =
{
	FLD_BYT | FLD_SEP_NUM,		/* niestandardowe terminatory */
	FLD_STR | FLD_SEP_NUM
};


/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBSENDCK_Descr[1] =
{
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania (nie potrzebne)
};


BYTE LBSENDCK_Resp_Descr[6] =
{
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT | FLD_SEP_ESC
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBFSTRQ_Descr[1] =
{
	{ FLD_BYT, 0, 0, 0 },	// // bez sprawdzania (zam�cone)
};


tPARAMETER_DESCRIPTION LBFSTRQ26_Descr[2] =
{
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania (nie potrzebne)
	{ FLD_NUM, 0, 0, 0 },	// bez sprawdzania (?)
	
};


tPARAMETER_DESCRIPTION LBFSTRQ25_Descr[7] =
{
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania (nie potrzebne)
	{ FLD_BYT, 0, 0, 99 },
	{ FLD_BYT, 0, 1, 12 },
	{ FLD_BYT, 0, 1, 31 },
	{ FLD_BYT, 0, 0, 23 },
	{ FLD_BYT, 0, 0, 59 },
	{ FLD_BYT, 0, 0, 59 },
};

/*
	Odpowied� na Ps==23.
*/
BYTE LBFSTRS1_Resp_Descr[26] =
{
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT | FLD_SEP_NUM,	/* niestandardowy terminator */
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_STR | FLD_SEP_ESC		/* tu w og�le nie ma terminatora - jest dorabiany (wpisywany w pierwszy bajt sumy kontr.) */
};

BYTE LBFSTRQ1_Resp_Descr[13] =
{
	FLD_NUM | FLD_SEP_BYT,	/* sprawdzi� czy nie mo�e by� FLD_BYT !!! */
	FLD_BYT,
	FLD_BYT,
	FLD_NUM	| FLD_SEP_BYT,
	FLD_NUM	| FLD_SEP_BYT,
	FLD_NUM	| FLD_SEP_BYT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT
};

BYTE LBFSTRQ27_Resp_Descr[17] =
{
	FLD_NUM | FLD_SEP_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_NUM | FLD_SEP_BYT,
	FLD_NUM | FLD_SEP_BYT,
	FLD_NUM | FLD_SEP_BYT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
};

/*
	Stara homologacja - ramki zwrotne mog� by� r�ne
*/

BYTE O_LBFSTRS_Resp_Descr1[15] =
{
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT | FLD_SEP_NUM,	/* niestandardowy terminator */
	FLD_NUM_EXT,
	FLD_NUM,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_STR | FLD_SEP_ESC		/* tu w og�le nie ma terminatora - jest dorabiany (wpisywany w pierwszy bajt sumy kontr.) */
};

BYTE O_LBFSTRS_Resp_Descr2[17] =
{
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT | FLD_SEP_NUM,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_STR | FLD_SEP_ESC
};

BYTE O_LBFSTRS_Resp_Descr3[19] =
{
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT | FLD_SEP_NUM,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_STR | FLD_SEP_ESC
};

BYTE O_LBFSTRS_Resp_Descr4[21] =
{
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT | FLD_SEP_NUM,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_STR | FLD_SEP_ESC
};

BYTE O_LBFSTRS_Resp_Descr5[23] =
{
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT | FLD_SEP_NUM,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_STR | FLD_SEP_ESC
};


BYTE O_LBFSTRS_Resp_Descr6[25] =
{
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT,
	FLD_BYT | FLD_SEP_NUM,	/* niestandardowy terminator */
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_NUM_EXT,
	FLD_STR | FLD_SEP_ESC,	/* tu w og�le nie ma terminatora - jest dorabiany (wpisywany w pierwszy bajt sumy kontr.) */
};

/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBERNRQ_Descr[1] =
{
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania (nie potrzebne)
};

BYTE LBERNRQ_Resp_Descr[1] =
{
	FLD_BYT | FLD_SEP_ESC	/* tu od razu po parametrze jest ESC */
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBIDRQ_Descr[1] =
{
	{ FLD_BYT, 0, 0, 0 },	// bez sprawdzania (?)
};

BYTE LBIDRQ_Resp_Descr[2] =
{
	FLD_STR | FLD_SEP_NUM,	/* najpierw jaki� brzydki terminator */
	FLD_STR | FLD_SEP_ESC,	/* a potem to i bez terminatora */
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION LBSNDMD_Descr[1] =
{
	{ FLD_BYT, 0, 0, 1 },
};

/*----------------------------------------------------------------------------*/

tPARAMETER_DESCRIPTION O_LBOPAK_Descr[1] = 
{
	{ FLD_STR, 20, 0, 0 },
};

tPARAMETER_DESCRIPTION O_LBSTOCSH_Descr[1] = 
{
	{ FLD_NUM_EXT, 10, 8, 2 },
};




#endif // _STRUCT_DESCRIPTIONS