/* Tab 2 */

/*
	ThermalResponse.h - plik nag��wkowy dla ThermalResponse.cpp
*/

#ifndef _THERMAL_RESPONSE
#define _THERMAL_RESPONSE

#include "StdAfx.h"
#include "..\\ThermalServiceLibrary.h"
#include "ComPort.h"
#include "ThermalCommand.h"
#include "FunctionDeclarations.h"

extern CComPort CThComPort;
extern CThermalCommand CThCmd;

class CThermalResponse
{
public:
	CThermalResponse();
	~CThermalResponse();

/*
	Clear
		Wyczy�� rozkaz (== inicjalizacja)

	Argumenty:
		---

	Zwraca:
		---
*/
	void Clear();

/*
	Receive, Receive2 (Receive to nak�adka na Receive2, dodane flush-owanie logu)
		Odbierz ramk� odpowiedzi, je�li zawiera pola danych to je wczytaj.

	Argumenty wej�ciowe:
		aProtocolType				- rodzaj (odmiana) protoko�u	(PROTOCOL
		aRespWithData				- TRUE je�li ramka zawiera dane (tzn. nie jest to ramka potwierdzenia)
		aResponseTime				- maksymalny czas oczekiwania na odpowied�

	Zwraca:
		SOK
		ERR_PORT_NOT_OPEN		- gdy pr�bowano co� odebra� bez otwierania portu
		ERR_I_MAX_RETRIES		- (w�oski) zbyt du�a liczba pr�b odebrania ramki
		ERR_NO_RESPONSE			- brak odpowiedzi (nie odebrano �adnego bajtu)
													w wyznaczonym czasie
		ERR_RESPONSE_TOUT		- przekroczony czas od odebrania pierwszego bajtu do
													odebrania znacznika ko�ca ramki
		ERR_FMT_HEADER			- nag��wek (znacznik pocz�tku) nieprawid�owy (lub brak)
		ERR_RETURN_CODE			-	urz�dzenie zwr�ci�o kod b��du r�ny od 0
		ERR_FMT_RETURN_CODE	- pole kodu b��du - z�y format (lub brak)
		ERR_CHECKSUM				-	suma kontrolna - z�a warto�� (niezgodna z wyliczon�)
		ERR_FMT_CHECKSUM		- suma kontrolna - nieprawid�owy format pola (lub brak)
		ERR_INPUT_BUF_OVRRUN	- zape�nienie bufora wej�ciowego
		ERR_UNKNOWN_CMD			- urz�dzenie zasygnalizowa�o, �e nie rozpozna�o rozkazu
		ERR_UNKNOWN_FRAME		- urz�dzenie odes�a�o ramk� niezgodn� z oczekiwaniami
		ERR_WRITE_FILE			- b��d zapisu do pliku (log) - je�li w��czone jest logowanie
*/
	int Receive( BYTE aProtocolType, BOOL aRespWithData, DWORD aResponseTime = NORMAL_RESPONSE_TIME );
	int Receive2( BYTE aProtocolType, BOOL aRespWithData, DWORD aResponseTime );

/*
	Translate
		Zamie� odebran� ramk� (pola) na posta� wewn�trzn� (wczytanie i okre�lenie
		rodzaj�w p�l)

	Argumenty wej�ciowe:
		aDataPtr						- adres pierwszego bajtu pierwszego pola ramki
		aDataLen						- liczba bajt�w (do znacznika ko�ca ramki)

	Zwraca:
		SOK
		ERR_FIELD_TYPE			- nieznany rodzaj pola (b��d terminatora)
*/
	int Translate( BYTE *aDataPtr, DWORD aDataLen );


/*
	CreateRespStruct

	Argumenty wej�ciowe:
		aProtocolType				- rodzaj protoko�u
		aResponseStruct			- struktura okre�laj�ca liczb� i rodzaje p�l oraz
													adres wyj�ciowej struktury zawieraj�cej pola

	Zwraca:
		SOK
		ERR_NUMBER_OF_FIELDS - ramka zawiera liczb� p�l r�n� od oczekiwanej
		ERR_FIELD_TYPE			-	pole nie odpowiada dostarczonemu opisowi
													(niezgodny terminator)
		ERR_FIELD_CONTENTS	- z�a zawarto�� pola (nie odpowiada rodzajowi pola)
*/
	int CreateRespStruct( BYTE	aProtocolType, tRESPONSE * aResponseStruct );

/*
	GetDataFieldDescr
		Pobierz informacj� o polu danych

	Argumenty wyj�ciowe:
		aLenOut							- tu zostanie wpisana liczba bajt�w danych
		aPtrOut							- tu zostanie wpisany adres pocz�tku pola danych

	Zwraca:
		---
*/
	void GetDataFieldDescr( DWORD *aLenOut, BYTE **aPtrOut );


/*
	AddField

	Argumenty wej�ciowe:
		aStrPtr							- /PTR/ zawarto�� pola (tekst)
		aFieldType					-	rodzaj pola (FLD_...)
		aStrLen							- d�ugo�� pola (liczba bajt�w bez ko�cz�cego zera)

	Zwraca:
		---

*/
	void AddField( char *aStrPtr, BYTE aFieldType, DWORD aStrLen );


/*
	GetExtendedErrorCode

	Argumenty:
		---

	Zwraca:
		rozszerzony kod b��du (zawieraj�cy zwr�cone przez urz�dzenie:
		kod rozkazu i kod b��du)
*/
	DWORD GetExtendedErrorCode();


/*
	GetFieldCount
		Pobierz liczb� p�l odpowiedzi

	Argumenty:
		---

	Zwraca:
		liczb� p�l
*/
	DWORD GetFieldCount();


/*
	GetFieldType
		Pobierz typ wybranego pola z ramki odpowiedzi

	Argumenty wej�ciowe:
		aFieldNdx						- indeks (od zera) pola
	
	Zwraca:
		typ pola (FLD_...)
*/
	BYTE GetFieldType( DWORD aFieldNdx );


/*
	GetFieldLength
		Pobierz d�ugo�� wybranego pola z ramki odpowiedzi (pole jest tekstem)

	Argumenty wej�ciowe:
		aFieldNdx						- indeks (od zera) pola
	
	Zwraca:
		d�ugo�� pola
*/
	DWORD GetFieldLength( DWORD aFieldNdx );


/*
	GetFieldLength
		Pobierz d�ugo�� wybranego pola z ramki odpowiedzi (pole jest tekstem)

	Argumenty wej�ciowe:
		aFieldNdx						- indeks (od zera) pola
	
	Zwraca:
		d�ugo�� pola
*/
	void SetFieldText( DWORD aFieldNdx, char * aFieldText );


/*
	GetFieldText
		Pobierz wybrane pole z ramki odpowiedzi

	Argumenty wej�ciowe:
		aFieldNdx						- indeks (od zera) pola
	
	Zwraca:
		wska�nik do (tekstu) wybranego pola. Nie wolno dokonywa� �adnych zmian we wskazanym polu -
		je�li trzeba co� zmienia� to nale�y skopiowa� wcze�niej do jakiego� bufora.
*/
	char * GetFieldText( DWORD aFieldNdx );


/*
	SetCmdString
		Ustaw kod rozkazu (dwa znaki)

	Argumenty wej�ciowe:
		aFirstChar					- pierwszy znak
		aSecondChar					- drugi znak
	
	Zwraca:
		---
*/
	void SetCmdString( char aFirstChar, char aSecondChar );


/*
	GetCmdString
		Zwr�� kod rozkazu

	Argumenty:
		---
	
	Zwraca:
		wska�nik do kodu rozkazu (dwuznakowego tekstu)
*/
	char * GetCmdString();


/*
	SetReturnCode
		Ustaw kod potwierdzenia/b��du wykonania rozkazu

	Argumenty wej�ciowe:
		aCode								- warto�� kodu
	
	Zwraca:
		---
*/
	void SetReturnCode( DWORD aCode );


/*
	GetReturnCode
		Pobierz kod potwierdzenia/b��du wykonania rozkazu

	Argumenty:
		---
	
	Zwraca:
		warto�� kodu
*/
	DWORD GetReturnCode();


/*
	ClearResponseStructPtr
		Skasuj (wyczy��) wska�nik do struktury opisuj�cej odpowied�

	Argumenty:
		---
	
	Zwraca:
		---
*/
	void ClearResponseStructPtr();


/*
	SetResponseStructPtr
		Ustaw wska�nik do struktury opisuj�cej odpowied�

	Argumenty wej�ciowe:
		aResponseStructPtr	- /PTR/ struktura

	Zwraca:
		---
*/
	void SetResponseStructPtr( tRESPONSE * aResponseStructPtr );


/*
	GetResponseStructPtr
		Pobierz wska�nik do struktury opisuj�cej odpowied�

	Argumenty:
		---

	Zwraca:
		wska�nik do struktury
*/
	tRESPONSE * GetResponseStructPtr();


/*
	Zmienne
*/
													/* Flagi: */
	BOOL F_Resp,						/* obecno�� odpowiedzi */
		F_Italian,						/* protok� w�oski */
		F_ItalianRemote,			/* protok� w�oski, Remote PC */
		F_RxChksum;						/* suma kontrolna w ramce nadawanej */

protected:

	char RetCmdString[CMD_CMD_STR_LEN+1];		/* kod rozkazu - ramka zwrotna */ 
	DWORD RetCode;													/* ramka zwrotna: kod zwrotny (ten, kt�ry umieszczony jest */
																					/* przed kodem rozkazu */

	DWORD	FieldCount,												/* liczba p�l w ramce */
			DataLen;														/* D�ugo�� pola danych */
	BYTE * DataPtr;													/* pocz�tek pola danych */

	BYTE ProtocolType;											/* protok� (charakterystyka) */

	tRESPONSE *ResponseStructPtr;						/* /PTR/ struktura odpowiedzi (pola oraz ich opis -  */
																					/* liczba i rodzaje p�l) */

	tVectorOfStrings	VectOfTextFields;			/* argumenty rozkazu zamienione na stringi */
	tVectorOfBytes		VectOfFieldTypes;			/* rodzaj argumentu -> wyznacza separator */
	tVectorOfDwords		VectOfTextLengths;		/* d�ugo�� argumentu zamienionego na tekst */
};

#endif // _THERMAL_RESPONSE