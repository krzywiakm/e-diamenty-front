/* Tab 2 */

/*
	Protocol.h - sta�e zwi�zane z protoko�em komunikacyjnym
*/

#ifndef _PROTOCOL
#define _PROTOCOL

/********************************** ZNAKI *************************************/

#define XON		(BYTE) 0x11
#define XOFF	(BYTE) 0x13

#define ESC		(BYTE) 0x1B

#define ACK		(BYTE) 0x06
#define NACK	(BYTE) 0x15

#define ENQ		(BYTE) 0x05
#define DLE		(BYTE) 0x10

#define BEL		(BYTE) 0x07
#define CAN		(BYTE) 0x18


/************************* RODZAJE/WARIANTY PROTOKO�U *************************/

/*
	Rodzaj protoko�u - konstruowany z: PROTO_...
*/
#define PROTO_TX_CHKSUM		(BYTE) 0x01
#define PROTO_RX_CHKSUM		(BYTE) 0x02
#define PROTO_RESP				(BYTE) 0x04
#define PROTO_REMOTE			(BYTE) 0x40
#define PROTO_ITALIAN			(BYTE) 0x80

/*
	W�oski - tam nie ma takich rozbie�no�ci jak w polskim...
	Oddzieli�em tryb pracy ze zdalnym PC.
*/
#define PROTOCOL_ITALIAN							PROTO_ITALIAN | PROTO_RESP | PROTO_TX_CHKSUM | PROTO_RX_CHKSUM
#define PROTOCOL_ITALIAN_REMOTE				PROTO_ITALIAN | PROTO_REMOTE | PROTO_RESP | PROTO_TX_CHKSUM | PROTO_RX_CHKSUM

/* Stary: suma kontrolna w ramce nadanej, odpowied� z sum� kontroln� */
#define PROTOCOL_OLD_CHK_RESP_CHK			PROTO_RESP | PROTO_TX_CHKSUM | PROTO_RX_CHKSUM

/* Stary: suma kontrolna w ramce nadanej, odpowied� bez sumy kontrolnej */
#define PROTOCOL_OLD_CHK_RESP_NCHK		PROTO_RESP | PROTO_TX_CHKSUM

/* Stary: suma kontrolna w ramce nadanej, brak odpowiedzi */
#define PROTOCOL_OLD_CHK_NRESP				PROTO_TX_CHKSUM

/* Stary: brak sumy kontrolnej w ramce nadanej, odpowied� z sum� kontroln�*/
#define PROTOCOL_OLD_NCHK_RESP_CHK		PROTO_RESP | PROTO_RX_CHKSUM

/* Stary: brak sumy kontrolnej w ramce nadanej, odpowied� bez sumy kontrolnej */
#define PROTOCOL_OLD_NCHK_RESP_NCHK		PROTO_RESP

/* Stary: brak sumy kontrolnej w ramce nadanej, brak odpowiedzi */
#define PROTOCOL_OLD_NCHK_NRESP				(BYTE) 0


#define I_ENQ_RESP_MASK				0xE0
#define I_ENQ_RESP_SIGNATURE	0x60

#define I_DLE_RESP_MASK				0xF0
#define I_DLE_RESP_SIGNATURE	0x40

#define ENQ_RESP_MASK					0xF0
#define ENQ_RESP_SIGNATURE		0x60

#define DLE_RESP_MASK					0xF8
#define DLE_RESP_SIGNATURE		0x70



/**************************** STA�E CZASOWE [ms] ******************************/

/* czekanie na pierwszy bajt odpowiedzi - najd�u�sze (100 sek) */
#define MAX_RESPONSE_TIME 100000

/* czekanie na pierwszy bajt odpowiedzi - normalne (7 sek) */
#define NORMAL_RESPONSE_TIME 7000

/* kr�tkie oczekiwanie na odpowied� - np. przy DLE i ENQ */
#define SHORT_RESPONSE_TIME 1000

/* czas od odebrania pierwszego bajtu ramki do odebrania ostatniego (-> znacznik ko�ca) */
#define MAX_RESPONSE_FRAME_TIME 5000

/* maks. odst�p czasowy mi�dzy bajtami */
#define RD_INTERV_TOUT 20


/******************************** INNE STA�E **********************************/

/*
	Protok� w�oski - doda�em zabezpieczenie (tyle b�dzie negatywnych potwierdze�
	zanim zostanie zasygnalizowany b��d
*/
#define I_MAX_RETRIES 10

/* Wielko�ci bufor�w: wej�ciowego i wyj�ciowego (za du�e, a co tam...) */
#define PORT_INPUT_BUFFER_LEN 1024
#define PORT_OUTPUT_BUFFER_LEN 2048

/* D�uogo�� kolejki wej�ciowej (kolejki wyj�ciowej nie ma) */
#define PORT_RX_QUEUE_LEN 1024

/* Odczyty b�d� wykonywane paczkami o tej wielko�ci */
#define READ_N_BYTES_AT_ONCE 16

/*
	Sta�e wynikaj�ce z formatu ramki:
	- d�ugo�� ci�gu okre�laj�cego rozkaz
	- maks. liczba parametr�w bajtowych
*/
#define CMD_CMD_STR_LEN 2
#define CMD_MAX_NUM_PARAMS 16

/*
	Chyba nie jest to nigdzie opisane, ale na wszelki wypadek dodam zabezpieczenie
	A MO�E gdzie� tam jest, ale dokumentacja jest m�tna i �eby co� z niej wy�owi�... !!!
*/
#define CMD_MAX_STR_PARAMS 200
#define CMD_MAX_STR_PARAM_LEN 100


#endif // _PROTOCOL