/* Tab 2 */

/*
	ComPort.h - plik nag��wkowy dla ComPort.cpp
*/

#ifndef _COM_PORT
#define _COM_PORT

#include "StdAfx.h"
#include "..\\ThermalServiceLibrary.h"
#include "FunctionDeclarations.h"

#define MAX_LOG_LINE 16

extern DWORD ExtendedErrorCode;

class CComPort
{
public:
	CComPort();
	~CComPort();


/*
	Open
		Otwiera i konfiguruje wybrany port, allokuje obszary pami�ci na bufory (wej�ciowy i wyj�ciowy)
	
	Argumenty wej�ciowe:
		aPortNamePtr				-	nazwa portu (taka, pod jak� wyst�puje w systemie)
		aBaudRate						- pr�dko�� transmisji (podana wprost, CBR_...)

	Zwraca:
		SOK									- port zosta� otwarty
		ERR_COMM_SETTINGS
		ERR_CREATE_FILE
		ERR_ALLOC
*/
	int Open( char *aPortNamePtr, DWORD aBaudRate );


/*
	WasPortOpened
		Czy port zosta� otwarty ?
	
	Argumenty:						---
	
	Zwraca:
		TRUE je�li otwarty, FALSE je�li nie
*/
	BOOL WasPortOpened();


/*
	SetBaudRate
		Ustaw pr�dko�� transmisji
	
	Argumenty wej�ciowe:
		aRate								- pr�dko�� transmisji (podana wprost)

	Zwraca:
		SOK
		SERROR
*/
	int SetBaudRate( DWORD aRate );


/*
	Write
		Zapisz (wy�lij) ci�g bajt�w
	
	Argumenty wej�ciowe:
		aNrOfBytes					-	liczba bajt�w do wys�ania
		aDataPtr						- /PTR/ pocz�tek obszaru do wys�ania

	Zwraca:
		SOK
		ERR_WRITE_FILE
		ERR_WRITE_FILE_COM

*/
	int Write( DWORD aNrOfBytes, BYTE * );

	
/*
	Read
		Odczytaj (odbierz) ci�g bajt�w. Timeouty s� ustalone z g�ry (po otwarciu portu)
	
	Argumenty wej�ciowe:
		aNrOfBytes					- liczba bajt�w do odebrania
		aNrOfBytesReadPtr		- /PTR/ bufor do kt�rego zostan� wpisane odebrane bajty

	Argumenty wyj�ciowe:
		aDataPtr						- /PTR/ tu zostanie wpisana liczba bajt�w odczytanych
	
	Zwraca:
		SOK
		ERR_READ_FILE_COM
		ERR_WRITE_FILE
*/
	int Read( DWORD aNrOfBytes, DWORD *aNrOfBytesReadPtr, BYTE *aDataPtr );


/*
	SendSingleByte
		Wy�lij pojedynczy bajt (np. ACK czy NACK)
	
	Argumenty wej�ciowe:
		aByte								- bajt do wys�ania

	Zwraca:
		SOK
		ERR_WRITE_FILE_COM
		ERR_WRITE_FILE
*/
	int SendSingleByte( BYTE aByte );


/*
	ClearRxQueue
		Czyszczenie kolejki wej�ciowej (usuwa wszystkie bajty czekaj�ce w buforze wej�ciowym)
	
	Argumenty:						---

	Zwraca:								---
*/
	void ClearRxQueue();


/*
	WaitForAnyByte
		Oczekiwanie na odbi�r bajtu (dowolnego) - z ewentualnym jego zapami�taniem
	
	Argumenty wej�ciowe:
		aTimeout						-	maks. czas oczekiwania (milisekundy)

	Argumenty wyj�ciowe:
		aFetchedBytePtr			- /PTR/ je�li != NULL to pod ten adres zostanie wpisany odebrany bajt
	
	Zwraca:
		SOK
		ERR_NO_RESPONSE
		ERR_COMM_SETTINGS
		ERR_READ_FILE_COM
		ERR_WRITE_FILE
*/
	int WaitForAnyByte( DWORD aTimeout, BYTE *aFetchedBytePtr );


/*
	Close
		Zamyka port (i usuwa zaallokowane bufory)
	
	Argumenty:						---

	Zwraca:
		SOK
		SERROR							- gdy pr�bowano zamkn�� port bez wcze�niejszego otworzenia;
												  lub nie powiod�o si� zamkni�cie portu
		ERR_WRITE_FILE			- logowanie
*/
	int Close();


/*
	LoggingStart
		Rozpocz�cie logowania transmisji do pliku
	
	Argumenty wej�ciowe:
		aFileNamePtr				- /PTR/ nazwa (scie�ka dost�pu) pliku
	
	Zwraca:
		SOK
		ERR_CREATE_FILE
*/
	int LoggingStart( char *aFileNamePtr );


/*
	LoggingStop
		Zako�czenie logowania 
	
	Argumenty wej�ciowe:	---

	Zwraca:
		SOK
		SERROR
		ERR_WRITE_FILE_COM
*/
	int LoggingStop();


/*
	LogUpdate
		Uaktualnienie logu (dodanie sekwencji bajt�w zapisywanych / odczytywanych )

	Argumenty wej�ciowe:
		aLogOperation				- operacja: zapis (LOG_WRITE) / odczyt (LOG_READ)
		aNrOfBytes					- liczba bajt�w do dodania do logu
		aDataPtr						- /PTR/ pocz�tek bufora, z kt�rego pobrane zostan� bajty

		Zwraca:
		SOK
		...
*/
	int LogUpdate( enum LOG_OPERATION aLogOperation, DWORD aNrOfBytes, BYTE *aDataPtr );


/*
	LogCreateHexAndWrite
		Wygenerowanie linii tekstowej i zapis do pliku
	
	Argumenty:						---
	
	Zwraca:
		SOK
		ERR_WRITE_FILE
*/
	int LogCreateHexAndWrite();

/*
	WriteToLog
		Funkcja pomocnicza - zapis do mailslota i ewentualnie do pliku logu.
	
	Argumenty wej�ciowe:
		aBufferAddr					-	adres bufora
		aBytesToWrite				- liczba bajt�w do zapisu
		aLogAlso						- TRUE je�li dane maj� by� wys�ane r�wnie� do pliku

	Argumenty wyj�ciowe:
		aBytesWrittenPtr		- /PTR/ tu b�dzie wpisana liczba bajt�w zapisanych

	Zwraca:
		SOK
		ERR_...
*/
	int WriteToLog( BYTE * aBufferAddr, DWORD aBytesToWrite, DWORD * aBytesWrittenPtr, BOOL aLogAlso );

/*
	LogFlush
		Funkcja opr�nia wyj�ciowy bufor logu - wysy�aj�c zawarto�� do mailslota/pliku
		(w trakcie normalnej pracy takie opr�nienie nast�puje przy zmianie operacji - R/W)

	Argumenty:						---
	
	Zwraca:
		SOK
		ERR_...
*/
	int LogFlush();


/*
	A tu s� zmienne...
*/
	BYTE *PortOutBuf,				/* Bufor wyj�ciowy (transmisja) */
		*PortInBuf;						/* Bufor wej�ciowy */


protected:

/*
	CleanUp
		Czyszczenie zmiennych i usuwanie bufor�w zwi�zanych z portem komunikacyjnym
	
	Argumenty:						---

	Zwraca:								---
*/
	void CleanUp();


/*
	A tu s� zmienne...
*/
	HANDLE hCom,						/* - handle portu komunikacyjnego */
		hLogFile,							/* - handle pliku (log) */
		hMailSlut;						/* - handle Mailslota (log) */

	DCB Dcb;								/* DCB */
	COMMTIMEOUTS Timeouts;	/* COMMTIMEOUTS */

	enum LOGGING_STATES LoggingState;				/* Stan logowania (aktywne czy nie) */
	enum LOG_OPERATION	LogLastOperation;		/* Ostatnia operacja (zapis lub odczyt) */

	DWORD LogLineLen;				/* Liczba bajt�w w buforze linii bajtowej (log) */

	BYTE LogByteLineBuf[MAX_LOG_LINE];			/* Bufor linii bajtowej (log) */
	char LogStrLineBuf[MAX_LOG_LINE*4 + 5];	/* Bufor linii tekstowej (log)  */
};

#endif // _COM_PORT