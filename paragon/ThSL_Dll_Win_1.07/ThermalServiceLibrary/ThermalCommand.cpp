/* Tab 2 */

/*
	ThermalCommand.cpp - implementacja klasy CThermalCommand
*/

#include "ComPort.h"
#include "ThermalCommand.h"
#include "ThermalResponse.h"

extern CComPort CThComPort;
extern CThermalResponse CThResp;

/******************************************************************************/

// Filtr dla SEH (Structured Exception Handling)

int ExceptionFilter( DWORD aExceptionCode )
{
	if( (aExceptionCode == EXCEPTION_ACCESS_VIOLATION) ||
			(aExceptionCode == EXCEPTION_ARRAY_BOUNDS_EXCEEDED) )
	{
		return EXCEPTION_EXECUTE_HANDLER;
	}
	else
	{
		return EXCEPTION_CONTINUE_SEARCH;
	}
}

/******************************************************************************/

CThermalCommand::CThermalCommand()
{
	CThermalCommand::Clear();
}

/*----------------------------------------------------------------------------*/

CThermalCommand::~CThermalCommand()
{

}

/*----------------------------------------------------------------------------*/

void CThermalCommand::Clear()
{
/*
	Inicjalizujemy troch� rzeczy 
*/
	ParamCount = 0;
	ByteParamCount = 0;

	ExtendedErrorCode = 0;

	VectOfTextFields.clear();
	VectOfFieldTypes.clear();
	VectOfTextLengths.clear();
}

/******************************************************************************/

BYTE CThermalCommand::GetProtocolType()
{
	return ProtocolType;
}

/*----------------------------------------------------------------------------*/

int CThermalCommand::Translate( BYTE	aProtocolType, char *aCommandPtr, DWORD aFieldDescrTabLen,
			        tPARAMETER_DESCRIPTION *aFieldDescrTabPtr,	BYTE *aArgStructPtr )
{
	static DWORD CurrField,
		NrOfElements,
		CurrElement,
		*DwordPtr;

	static char		**StrPtrPtr;
	static BYTE		*BytePtr, NewSep;
	static tEXTERNAL_NUMERIC *ExternalNumericPtr;
	static BYTE		*CurrParamPtr;
	
	static BOOL		CountByteParams;

	static BYTE		MaxLen;	/* maks. d�ugo�� pola - dotyczy ci�g�w i warto�ci
													numerycznych zamienionych na ci�gi */
	static DWORD	M1,			/* Minimum dla bajt�w i dword�w / maks. liczba cyfr
													cz�ci ca�kowitej dla numerycznych */
								M2;			/* Maksimum dla bajt�w i dword�w / maks. liczba  cyfr
													cz�ci u�amkowej dla numerycznych */

	static int RetI;

/*
	Tablica opisuj�ca typy argument�w i ich dozwolone warto�ci ma elementy w postaci struktur,
	zamienimy wi�c aFieldDescrTabLen na liczb� p�l.
	Najpiew jeszcze b�dzie kontrolny _ASSERT (czy aField... ma prawid�ow� warto��)
*/
	_ASSERTE( (aFieldDescrTabLen % sizeof(tPARAMETER_DESCRIPTION)) == 0 );
	aFieldDescrTabLen = aFieldDescrTabLen / sizeof(tPARAMETER_DESCRIPTION);

	ProtocolType = aProtocolType;

	F_Resp =					( (aProtocolType & PROTO_RESP) != 0 );
	F_Italian =				( (aProtocolType & PROTO_ITALIAN) != 0 );
	F_ItalianRemote = ( ((aProtocolType & PROTO_ITALIAN) != 0) && ((aProtocolType & PROTO_REMOTE) != 0) );
	F_RxChksum =			( (aProtocolType & PROTO_RX_CHKSUM) != 0 );
	F_TxChksum =			( (aProtocolType & PROTO_TX_CHKSUM) != 0 );

	if( F_Italian )
	{
		CountByteParams = FALSE;
	}
	else
	{
		CountByteParams = TRUE;
	}

	CurrParamPtr = (BYTE *) aArgStructPtr;
	
	Clear();

/*
	Sprawdzenie poprawno�ci ci�gu okre�laj�cego rozkaz:
	- czy wska�nik nie zerowy
	- czy d�ugo�� ci�gu prawid�owa
	
	-> je�li poprawny to go zapami�tujemy
*/

	if( !aCommandPtr )
	{
		return SERROR;
	}

	if( strlen(aCommandPtr) != CMD_CMD_STR_LEN )
	{
		return SERROR;
	}
	else
	{
		SetCmdString( aCommandPtr );
	}

/*
	__try (SEH, a nie C++ EH) - bo kto� mo�e nam spreparowa� argumenty tak,
	�e b�dziemy pr�bowa� odczyta� co� z nieprawid�owych adres�w
	(a _CrtIsValidPointer dzia�a tylko w wersji DEBUG).
*/
	__try
	{
		if( aFieldDescrTabLen )	/* je�li rozkaz posiada jakie� argumenty... */
		{
			if( !aFieldDescrTabPtr )
			{
				return SERROR;
			}
			
/*	
			Bazuj�c na dostarczonym opisie argument�w (rodzaje), wyci�gamy kolejne argumenty
			z tablicy i zapami�tujemy	je w wewn�trznej reprezentacji (tzn. tekstowej),
			ewentualnie konwertuj�c je z formatu numerycznego zewn�trznego
*/
			for( CurrField = 0; CurrField < aFieldDescrTabLen; CurrField++ )
			{
				_ASSERTE( (aFieldDescrTabPtr[CurrField].FieldType & FLD_TYPE_MASK)< FLD_DEF_MAX );

				NewSep = aFieldDescrTabPtr[CurrField].FieldType & FLD_SEP_MASK;
				MaxLen = aFieldDescrTabPtr[CurrField].MaxStrLen;
				M1 = aFieldDescrTabPtr[CurrField].R1;
				M2 = aFieldDescrTabPtr[CurrField].R2;

				switch( aFieldDescrTabPtr[CurrField].FieldType & FLD_TYPE_MASK )
				{
					default:
						_ASSERTE( FALSE );		/* DEFAULT tylko w celach kontrolnych */
						break;

					case FLD_BYT:						/* bajt */
						RetI = AddByteParam( *((BYTE *) CurrParamPtr), NewSep, (BYTE) M1, (BYTE) M2 );
						if( RetI != SOK )
						{
							return (RetI | CurrField);
						}

						CurrParamPtr += sizeof(BYTE);

						if( CountByteParams )
						{
							ByteParamCount++;
						}
						break;


					case FLD_BYT_TAB:				/* tablica bajt�w (-> liczba element�w, wska�nik) */
						NrOfElements = *( (DWORD *) CurrParamPtr );
						CurrParamPtr += sizeof(DWORD);

						if( NrOfElements > 0 )
						{
							BytePtr = *( (BYTE **) CurrParamPtr );
/*
							Konieczne b�dzie sprawdzenie czy wska�nik nie kieruje nas w krzaki -
							tzn. czy mo�na odczyta� okre�lon� liczb� bajt�w od wyznaczonego adresu
*/
							_ASSERTE( _CrtIsValidPointer( BytePtr, NrOfElements * sizeof(BYTE), FALSE ) );

							for( CurrElement = 0; CurrElement < NrOfElements; CurrElement++ )
							{
								RetI = AddByteParam( BytePtr[CurrElement], NewSep, (BYTE) M1, (BYTE) M2  );
								if( RetI != SOK )
								{
									return (RetI | CurrField);
								}
							}

							if( CountByteParams )
							{
								ByteParamCount += NrOfElements;
							}
						}

						CurrParamPtr += sizeof(BYTE *);
						break;


					case FLD_NUM:						/* DWORD */
						CountByteParams = FALSE;

						RetI = AddNumericParam( *((DWORD *) CurrParamPtr), NewSep, M1, M2 );
						if( RetI != SOK )
						{
							return (RetI | CurrField);
						}

						CurrParamPtr += sizeof(DWORD);
						break;


					case FLD_NUM_TAB:				/* tablica DWORD�w (-> liczba element�w, wska�nik)*/
						CountByteParams = FALSE;
						NrOfElements = *( (DWORD *) CurrParamPtr );
						CurrParamPtr += sizeof(DWORD);

						if( NrOfElements > 0 )
						{
							DwordPtr = *( (DWORD **) CurrParamPtr );
/*
							Konieczne b�dzie sprawdzenie czy wska�nik nie kieruje nas w krzaki -
							tzn. czy mo�na odczyta� okre�lon� liczb� DWORD�w od wyznaczonego adresu
*/
							_ASSERTE( _CrtIsValidPointer( DwordPtr, NrOfElements * sizeof(DWORD), FALSE ) );

							for( CurrElement = 0; CurrElement < NrOfElements; CurrElement++ )
							{
								RetI = AddNumericParam( DwordPtr[CurrElement], NewSep, M1, M2 );
								if( RetI != SOK )
								{
									return (RetI | CurrField);
								}
							}
						}

						CurrParamPtr += sizeof(DWORD *);
						break;


					case FLD_NUM_EXT:				/* format numeryczny zewn�trzny */
						CountByteParams = FALSE;

						RetI = AddNumericParamExt( (tEXTERNAL_NUMERIC *) CurrParamPtr, NewSep, MaxLen, M1, M2 );
						if( RetI != SOK )
						{
							return (RetI | CurrField);
						}
					
						CurrParamPtr += sizeof(tEXTERNAL_NUMERIC);
						break;


					case FLD_NUM_EXT_TAB:		/* tablica num. zewn. (-> liczba element�w, wska�nik)*/
						CountByteParams = FALSE;
						NrOfElements = *( (DWORD *) CurrParamPtr );
						CurrParamPtr += sizeof(DWORD);
/*				
						ASSERT dla ExternalNumericPtr nie jest potrzebny - jest w AddNumericParamExt(...)
						Poniewa� przekazujemy adres elementu tablicy a nie jego zawarto�� -	przed
						wej�ciem do AddNumericParamExt nie b�dzie odczytu - wi�c wszystko gra
*/
						if( NrOfElements > 0 )
						{
							ExternalNumericPtr = *((tEXTERNAL_NUMERIC **) CurrParamPtr);
						
							for( CurrElement = 0; CurrElement < NrOfElements; CurrElement++ )
							{
								RetI = AddNumericParamExt( & ExternalNumericPtr[CurrElement], NewSep, MaxLen, M1, M2 );
								if( RetI != SOK )
								{
									return (RetI | CurrField);
								}
							}
						}

						CurrParamPtr += sizeof(tEXTERNAL_NUMERIC *);
						break;


					case FLD_STR:
						CountByteParams = FALSE;
					
						RetI = AddStringParam( *((char **) CurrParamPtr), NewSep, MaxLen );
						if( RetI != SOK )
						{
							return (RetI | CurrField);
						}

						CurrParamPtr += sizeof(char *);
						break;


					case FLD_STR_TAB:
						CountByteParams = FALSE;
						NrOfElements = *( (DWORD *) CurrParamPtr );
						CurrParamPtr += sizeof(DWORD);

						if( NrOfElements > 0 )
						{
							StrPtrPtr = (char **)(* ((char **) CurrParamPtr));
/*
							Konieczne b�dzie sprawdzenie czy wska�nik nie kieruje nas w krzaki -
							tzn. czy mo�na odczyta� okre�lon� liczb� wska�nik�w od wyznaczonego adresu
*/
							_ASSERTE( _CrtIsValidPointer( StrPtrPtr, NrOfElements * sizeof(char *), FALSE ) );

							for( CurrElement = 0; CurrElement < NrOfElements; CurrElement++ )
							{
								RetI = AddStringParam( StrPtrPtr[CurrElement], NewSep, MaxLen );
								if( RetI != SOK )
								{
									return (RetI | CurrField);
								}
							}
						}

						CurrParamPtr += sizeof(char *);
						break;


					case FLD_LIN:
						CountByteParams = FALSE;
						RetI = AddLineParam( *((char **) CurrParamPtr), NewSep, MaxLen );
						if( RetI != SOK )
						{
							return (RetI | CurrField);
						}

						CurrParamPtr += sizeof(char *);
						break;


					case FLD_LIN_TAB:
						CountByteParams = FALSE;
						NrOfElements = *( (DWORD *) CurrParamPtr );
						CurrParamPtr += sizeof(DWORD);

						if( NrOfElements > 0 )
						{
							StrPtrPtr = (char **)(* ((char **) CurrParamPtr));
/*
							Konieczne b�dzie sprawdzenie czy wska�nik nie kieruje nas w krzaki -
							tzn. czy mo�na odczyta� okre�lon� liczb� wska�nik�w od wyznaczonego adresu
*/
							_ASSERTE( _CrtIsValidPointer( StrPtrPtr, NrOfElements * sizeof(char *), FALSE ) );

							for( CurrElement = 0; CurrElement < NrOfElements; CurrElement++ )
							{
								RetI = AddLineParam( StrPtrPtr[CurrElement], NewSep, MaxLen );
								if( RetI != SOK )
								{
									return (RetI | CurrField);
								}
							}
						}

						CurrParamPtr += sizeof(char *);
						break;
				}
			}
		}
	}
	__except( ExceptionFilter(GetExceptionCode()) )
	{
		return (ERR_ACCESS_VIOLATION | CurrField);
	}

	return SOK;
}

/*----------------------------------------------------------------------------*/

void CThermalCommand::SetCmdString( char *aCmdStringPtr )
{
	strcpy( CmdString, aCmdStringPtr );
}

/*----------------------------------------------------------------------------*/

DWORD CThermalCommand::GetParamCount()
{
	return ParamCount;
} 

/*----------------------------------------------------------------------------*/

DWORD CThermalCommand::GetExtendedErrorCode()
{
	return ExtendedErrorCode;
}

/*----------------------------------------------------------------------------*/

BOOL CThermalCommand::GetErrorMessage( DWORD aExtendedErrorCode, char **aStrOutPtr )
{
	static char MsgStr[64];
	static char Char1, Char2;
	static WORD Code;

	Code = LOWORD( aExtendedErrorCode );

	Char1 = HIBYTE( HIWORD(aExtendedErrorCode) );
	Char2 = LOBYTE( HIWORD(aExtendedErrorCode) );

	if( (Char1 == '$') || (Char1 == '*') || (Char1 == '#') )
	{
		sprintf( MsgStr, FMTSTR_GET_ERROR_MESSAGE, Char1, Char2, Code );
		*aStrOutPtr = MsgStr;
		return TRUE;
	}

	return FALSE;
}

/*----------------------------------------------------------------------------*/

DWORD CThermalCommand::GetRequiredFrameBufferLength()
{
	static DWORD Sum,
		CurrField;

/*
	ESC P, Rozkaz, ESC backslash - tylko to jest wsp�lne z protoko�em w�oskim
*/
	Sum  = 6;

/*
	Separatory - s� jednobajtowe (wi�c jest ich tyle co parametr�w)
*/
	Sum += ParamCount;

/*
	Stara wersja protoko�u - parametry bajtowe - ostatni nie ma separatora
*/
	if( ByteParamCount )
	{	
		Sum--;
	}

	for( CurrField = 0; CurrField < ParamCount; CurrField++ )
	{
		Sum += VectOfTextLengths[CurrField];
	}

/*
	W�oski - ramki maj� sum� kontroln�
	Polski - nie wszystkie
*/
	if( F_Italian )
	{
		Sum += 2;
	}
	
	return Sum;
}

/******************************************************************************/

void CThermalCommand::AddField( char *aStrPtr, BYTE aFieldType, DWORD aStrLen )
{
	VectOfTextFields.push_back( aStrPtr );
	VectOfFieldTypes.push_back( aFieldType );
	VectOfTextLengths.push_back( aStrLen );
	ParamCount++;
}

/*----------------------------------------------------------------------------*/

int CThermalCommand::AddByteParam( BYTE aParam, BYTE aFieldSep, BYTE aMin, BYTE aMax )
{
	static char StrBuf[4];		/* 3 znaki na posta� tekstow� + ko�cz�ce zero */
	static DWORD StrLen;

	_ASSERTE( (aFieldSep & FLD_TYPE_MASK) == 0 );

	if( aMin || aMax )
	{
		if( (aParam < aMin) || (aParam > aMax ) )
		{
			return ERR_PARAM_VALUE;
		}
	}

	StrLen = sprintf( StrBuf, "%d", aParam );
	AddField( StrBuf, FLD_BYT | aFieldSep, StrLen );

	return SOK;
}

/*----------------------------------------------------------------------------*/

int CThermalCommand::AddNumericParam( DWORD aParam, BYTE aFieldSep, DWORD aMin, DWORD aMax )
{
	static char StrBuf[11];		/* 10 znak�w na posta� tekstow� + ko�cz�ce zero */
	static DWORD StrLen;

	_ASSERTE( (aFieldSep & FLD_TYPE_MASK) == 0 );

	if( aMin || aMax )
	{
		if( (aParam < aMin) || (aParam > aMax ) )
		{
			return ERR_PARAM_VALUE;
		}
	}

	StrLen = sprintf( StrBuf, "%d", aParam );
	AddField( StrBuf, FLD_NUM | aFieldSep, StrLen );

	return SOK;
}

/*----------------------------------------------------------------------------*/

int CThermalCommand::CheckNumericParamExt( char *aParamPtr, BYTE aLen, BYTE aMaxIntg, BYTE aMaxFact )
{
	BOOL DotFound;
	DWORD Intg, Fact, i;

	Intg = 0;
	Fact = 0;

	if( (aMaxIntg | aMaxFact) == 0 )	/* oba pola r�wne 0 m�wi� �eby nie sprawdza� */
	{
		return SOK;
	}

	DotFound = FALSE;			/* na razie nie znaleziono kropki */
	Intg = 0;							/* i zero znak�w */
	Fact = 0;

	for( i = 0; i < aLen; i++ )	/* sprawdzamy znak po znaku */
	{
		if( aParamPtr[i] == '.' )	/* czy kropka ? */
		{
			if( DotFound )					/* tak, ale je�li ju� by�a - to b��d */
			{
				return ERR_FIELD_TYPE;
			}
			else
			{
				DotFound = TRUE;			/* zaznaczamy, �e wchodzimy w cz�� u�amkow� */
			}
		}
		else if( isdigit(aParamPtr[i]) )	/* czy cyfra ? */
		{
			if( DotFound )									/* cz�� u�amkowa ? */
			{
				Fact++;
			}
			else
			{
				Intg++;
			}

		}
		else											/* ani kropka ani cyfra - to b��d */
		{
			return ERR_FIELD_TYPE;
		}
	}

	if( (Fact > aMaxFact) || (Intg > aMaxIntg) )	/* czy kt�ra� z cz�ci za du�a ? */
	{
		return ERR_PARAM_LEN;
	}

	return SOK;
}


/*----------------------------------------------------------------------------*/

int CThermalCommand::AddNumericParamExt( tEXTERNAL_NUMERIC *aParamPtr, BYTE aFieldSep,
																				BYTE aMaxLen, BYTE aMaxIntg, BYTE aMaxFact )
{
	static char StrBuf[32];		/* 8 cyfr cz�ci ca�kowitej, kropka, 2 cyfry cz�ci u�amkowej 
														+ ko�cz�ce zero. I jeszcze zostanie troch� zapasu */
	static DWORD StrLen;
	static int RetI;

	static char * ParamPtr;

	_ASSERTE( aParamPtr );
	_ASSERTE( (aFieldSep & FLD_TYPE_MASK) == 0 );

	if( aParamPtr->ParamValuePtr )
	{
/*
		Format zewn�trzny - trzeba wywo�a� (zewn�trzn�) funkcj� konwertuj�c� - je�li
		jest podany adres funkcji konwertuj�cej
*/
		if( aParamPtr->TypeTranslFuncPtr )	// b�dzie konwersja na tekst
		{
			_ASSERTE( aParamPtr->ParamValuePtr );

			( aParamPtr->TypeTranslFuncPtr )( aParamPtr->ParamValuePtr, StrBuf ); 
			ParamPtr = StrBuf;
		}
		else																// bez konwersji - jest ju� tekst
		{
			ParamPtr = (char *) aParamPtr->ParamValuePtr;
		}

		StrLen = strlen( ParamPtr );
		if( (aMaxLen != 0) && (StrLen > aMaxLen) )
		{
			return ERR_PARAM_LEN;
		}

		RetI = CheckNumericParamExt( ParamPtr, StrLen, aMaxIntg, aMaxFact );
		if( RetI != SOK )
		{
			return RetI;
		}
			
		AddField( ParamPtr, FLD_NUM | aFieldSep, StrLen );
	}
	else	// zerowy wska�nik do warto�ci - brak parametru
	{
		AddField( "", FLD_NUM | aFieldSep, STR_EMPTY );		// pusty ci�g i STR_EMPTY -> oznacza brak param.
	}

	return SOK;
}

/*----------------------------------------------------------------------------*/

int CThermalCommand::AddStringParam( char *aParamPtr, BYTE aFieldSep, BYTE aMaxLen )
{
	static DWORD StrLen;

	_ASSERTE( (aFieldSep & FLD_TYPE_MASK) == 0 );

	if( aParamPtr )
	{	
		StrLen = strlen( aParamPtr );

		if( (aMaxLen != 0) && (StrLen > aMaxLen) )
		{
			return ERR_PARAM_LEN;
		}

		AddField( aParamPtr, FLD_STR | aFieldSep, StrLen );
	}
	else
	{
		AddField( "", FLD_STR | aFieldSep, STR_EMPTY );
	}

	return SOK;
}

/*----------------------------------------------------------------------------*/

int CThermalCommand::AddLineParam( char *aParamPtr, BYTE aFieldSep, BYTE aMaxLen )
{
	static DWORD StrLen;

	_ASSERTE( (aFieldSep & FLD_TYPE_MASK) == 0 );

	if( aParamPtr )
	{	
		StrLen = strlen( aParamPtr );

		if( (aMaxLen != 0) && (StrLen > aMaxLen) )
		{
			return ERR_PARAM_LEN;
		}

		AddField( aParamPtr, FLD_LIN | aFieldSep, StrLen );
	}
	else
	{
		AddField( "", FLD_LIN | aFieldSep, STR_EMPTY );
	}

	return SOK;
}

/******************************************************************************/

string CThermalCommand::GetParam( DWORD aParamNdx )
{
	_ASSERTE( aParamNdx < VectOfTextFields.size() );

	return VectOfTextFields[aParamNdx];
}

/*----------------------------------------------------------------------------*/

void CThermalCommand::InsertParam( DWORD aParamNdx, BYTE aFieldType, char *aStringPtr )
{
	_ASSERTE( aParamNdx <= VectOfTextFields.size() );
	_ASSERTE( aStringPtr );

	VectOfTextFields.insert( VectOfTextFields.begin() + aParamNdx, aStringPtr );
	VectOfFieldTypes.insert( VectOfFieldTypes.begin() + aParamNdx, aFieldType );
	VectOfTextLengths.insert( VectOfTextLengths.begin() + aParamNdx, strlen(aStringPtr) );

	ParamCount++;
}

/******************************************************************************/

int CThermalCommand::CreateFrame(	DWORD aMaxNrOfBytes, BYTE *aOutBufPtr,
																	DWORD *aBytesWrittenPtr )
{
	static DWORD WrkOffs,
		ParNdx,
		StrLen;

	static BYTE Checksum;

	static char NumericTmp[32];

	_ASSERTE( aOutBufPtr );


#ifdef _DEBUG
	memset( aOutBufPtr, 0 ,aMaxNrOfBytes );	/* w wersji DEBUG czyszczenie bufora */
#endif // _DEBUG

	if( aMaxNrOfBytes < GetRequiredFrameBufferLength() )
	{
		return ERR_FRAME_TOO_LONG;
	}

	WrkOffs = 0;	/* zaczynamy od pocz�tku bufora */

	/* Na pocz�tku jest nag��wek: ESC P */
	aOutBufPtr[WrkOffs++] = 0x1b;					/* znak ESC */
	aOutBufPtr[WrkOffs++] = 'P';					/* znak P */

/*
	Do��czamy parametry oraz kod rozkazu (w wersji w�oskiej b�dzie na pocz�tku,
	normalnie - jest za parametrami bajtowymi).
*/
	if( ParamCount )
	{

/*
		Kod rozkazu - w wersji w�oskiej jest na pocz�tku (ByteParamCount b�dzie
		r�wne zero, gdy� dla w wersji w�oskiej nie liczymy parametr�w bajtowych).

		Natomiast przy protokole polskim kod rozkazu jest po parametrach bajtowych,
		kt�rych jest niezerowa liczba. Ale... nie dotyczy to $g (LBDBREPRS), kt�ry nie
		ma �adnego parametru bajtowego. Wi�c ByteParamCount b�dzie r�wne 0.
*/
		if( ByteParamCount == 0 )
		{
			WrkOffs += sprintf( (char *) & aOutBufPtr[WrkOffs], "%s", CmdString );
		}

		for( ParNdx = 0; ParNdx < ParamCount ; ParNdx++ )
		{
/*
			Sprawdzamy, czy dane pole ma by� obecne w ramce.
			W wewn�trznej reprezentacji rozkazu puste pola maj� d�ugo�� STR_EMPTY -
			i w ramce nie zostan� umieszczone nawet separatory takich p�l.
*/
			StrLen = VectOfTextLengths[ParNdx];
			if( StrLen != DWORD(STR_EMPTY) )
			{

/*
				Puste ci�gi maj� d�ugo�� 0 - i jako takie zostan� w��czone do ramki (sam separator )
*/
				if( StrLen )		
				{
/*
					W�oski protok� wymaga warto�ci numerycznych w postaci sta�oprzecinkowej - 
					bez znaku kropki - z kontekstu wynika ile jest pozycji cz�ci u�amkowej.
*/
					if( F_Italian && (VectOfFieldTypes[ParNdx] == FLD_NUM)  )
					{
						if( RemoveDot((char *) VectOfTextFields[ParNdx].c_str(), NumericTmp) )
						{
							StrLen--;
						}

						strcpy( (char *) & aOutBufPtr[WrkOffs], NumericTmp );
					}
					else
					{
						strcpy( (char *) & aOutBufPtr[WrkOffs], VectOfTextFields[ParNdx].c_str() );
					}

					WrkOffs += StrLen;
				}

/*
				W wersji normalnej separatory nie wyst�puj� przy:
				- ostatnim parametrze bajtowym

				W pozosta�ych przypadkach s� obecne.
*/
				if( (F_Italian) || 
						((!F_Italian) && (ParNdx != (ByteParamCount - 1))) )	//  && (ParNdx != (ParamCount - 1))) )
				{
					aOutBufPtr[WrkOffs++] = SeparatorFromFieldType( VectOfFieldTypes[ParNdx], F_Italian );
				}

/*
				Kod rozkazu - w wersji polskiej wyst�puje po parametrach bajtowych (zawsze jest przynajmniej 
				jeden taki parametr)
*/
				if( !F_Italian )
				{
					if( ParNdx == (ByteParamCount -1) )
					{
						WrkOffs += sprintf( (char *) & aOutBufPtr[WrkOffs], "%s", CmdString );
					}
				}
			}
		}
	}
	else	/* Brak parametr�w - to musimy teraz do��czy� ci�g rozkazowy */
	{
		WrkOffs += sprintf( (char *) & aOutBufPtr[WrkOffs], "%s", CmdString );
	}

	/* Jeszcze opcjonalna suma kontrolna */
	if( F_TxChksum )
	{
		Checksum = GenerateChecksum( & aOutBufPtr[2], WrkOffs - 2   );		/* -2 bo 2 bajty nag��wka */
		WrkOffs += sprintf( (char *) & aOutBufPtr[WrkOffs], "%02X", Checksum );
	}

	/* Znacznik ko�ca ESC \ */
	aOutBufPtr[WrkOffs++] = 0x1b;						/* znak ESC */
	aOutBufPtr[WrkOffs++] = '\\';						/* znak \   */

	_ASSERTE( aBytesWrittenPtr );
	
	*aBytesWrittenPtr = WrkOffs;

	return SOK;
}

/******************************************************************************/

BOOL CThermalCommand::RemoveDot( char * aNumericInPtr, char * aNumericOutPtr )
{
	static DWORD DotOffs;
	static char * Dot;

	_ASSERTE( aNumericInPtr );
	_ASSERTE( aNumericOutPtr );

	Dot = strchr( aNumericInPtr, '.' );
	if( Dot )
	{
		DotOffs = Dot - aNumericInPtr;
		strncpy( aNumericOutPtr, aNumericInPtr, DotOffs );
		strcpy( aNumericOutPtr + DotOffs, aNumericInPtr + DotOffs + 1 );
		return TRUE;
	}

	strcpy( aNumericOutPtr, aNumericInPtr );
	return FALSE;
}

/******************************************************************************/

int CThermalCommand::Send()
{
	static int RetI_Send, RetI_Flush;

	RetI_Send = Send2();
	RetI_Flush = CThComPort.LogFlush();

	if( RetI_Send != SOK )
	{
		return RetI_Send;
	}

	return RetI_Flush;
}

/*----------------------------------------------------------------------------*/

int CThermalCommand::Send2()
{
	static int RetI;				
	static DWORD BytesInBuffer;					/* Ile bajt�w zajmie wygenerowana ramka */

	if( !CThComPort.WasPortOpened() )
	{
		return ERR_PORT_NOT_OPEN;
	}

	/* Generujemy ramk� */
	RetI = CreateFrame( PORT_OUTPUT_BUFFER_LEN, CThComPort.PortOutBuf, & BytesInBuffer );
	if( RetI != SOK )
	{
		return RetI;
	}
	
	CThComPort.ClearRxQueue();

	RetI = CThComPort.Write( BytesInBuffer, CThComPort.PortOutBuf );
	return RetI;
}
