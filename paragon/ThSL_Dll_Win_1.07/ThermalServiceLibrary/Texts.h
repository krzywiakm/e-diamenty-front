/* Tab 2 */

/*
	Texts.h - teksty, kt�re wykorzystywane s� wy��cznie w Dll-u
*/

#include "..\\CommonTexts.h"

#ifndef _TEXTS
#define _TEXTS

#ifdef LANGUAGE_ENGLISH


#define ERRMSG_NO_RESPONSE		"No response"
#define ERRMSG_INTERNAL_CODE	"Internal error code: "


#elif defined LANGUAGE_POLISH


#define ERRMSG_NO_RESPONSE		"Brak odpowiedzi"
#define ERRMSG_INTERNAL_CODE	"Wewn�trzny kod b��du: "


#endif // LANGUAGE...

#endif // _TEXTS