/* Tab 2 */

/*
	FunctionDeclarations.h
	deklaracje funkcji globalnych, kt�re s� cz�sto u�ywane w ThermalServiceLibrary.cpp,
	ThermalCommand.cpp, ThermalResponse.cpp i ComPort.h
	(pozosta�e funkcje to te, kt�re s� eksportowane z DLL-a i nie musz� mie� deklaracji)
*/

#ifndef _FUNCTION_DECLARATIONS
#define _FUNCTION_DECLARATIONS

/*
	GetStatus_CashRegister
		Pobierz status (stan) kasy

	Argumenty wyj�ciowe:
		aStatusOutPtr					- /PTR/ struktura z flagami opisuj�cymi stan kasy

	Zwraca:
		SOK
		ERR_READ_FILE_COM			- b��d przy odczycie z portu
		ERR_NO_RESPONSE				- brak odpowiedzi
		ERR_FMT_ENQ						- b��dna odpowied� (z�y format)
*/
int GetStatus_CashRegister( tCASH_REGISTER_STATUS *aStatusOutPtr );


/*
	GetStatus_Printer
		Pobierz status (stan) drukarki

	Argumenty wyj�ciowe:
		aStatusOutPtr					- /PTR/ struktura z flagami opisuj�cymi stan drukarki

	Zwraca:
		SOK
		ERR_READ_FILE_COM			- b��d przy odczycie z portu
		ERR_NO_RESPONSE				- brak odpowiedzi
		ERR_FMT_DLE						- b��dna odpowied� (z�y format)
*/
int GetStatus_Printer( tPRINTER_STATUS *aStatusOutPtr );


/*
	ErrorMessage
		Wy�wietl komunikat (podany z zewn�trz) o b��dzie.
		Analizuje przekazany kodu b��du (zwr�cony przez funkcje).
		Rozpoznaje ERR_RETURN_CODE i wtedy pobiera rozszerzony kod b��du oraz
		go interpretuje, dopisuj�c do podanego tekstu kod rozkazu i numer b��du.

	Argumenty wej�ciowe:
		aStrPtr								- tekst (b�dzie wypisany na pierwszym miejscu)
		aReturnCode						- kod b��du

	Zwraca:
		---
*/
void ErrorMessage( char *aStrPtr, int aReturnCode );


/*
	TimeoutDialog
		Wy�wietl okienko dialogowe z zapytaniem o reakcj� na Timeout (czy czeka�
		dalej)

	Argumenty:
		---

	Zwraca:	
		IDYES									- u�ytkownik chce czeka� dalej								
		IDNO									- u�ytkownik nie chce czeka�
*/
int TimeoutDialog();


/*
	GetTrueFieldType
		Pobierz prawdziwy rodzaj pola (ten ze struktury opisuj�cej a nie wyznaczony
		na podstawie terminatora pola)

	Argumenty wej�ciowe:
		aFieldType						- rodzaj pola

	Zwraca:	
		rodzaj pola (FLD_...)
*/
BYTE GetTrueFieldType( BYTE aFieldType );


/*
	SeparatorFromFieldType
		Pobierz separator/terminator dla danego typu pola i protoko�u

	Argumenty wej�ciowe:
		aFieldType						- rodzaj pola
		aProtoItalian					- TRUE dla protoko�u w�oskiego, FALSE dla polskiego

	Zwraca:	
		znak separatora/terminatora
*/
char SeparatorFromFieldType( BYTE aFieldType, BOOL aProtoItalian );


/*
	FindFrameEnd
		Znajd� koniec ramki

	Argumenty wej�ciowe:
		aBufferPtr						- adres bufora
		aNrOfBytesInBuffer		- liczba bajt�w w buforze

	Argumenty wyj�ciowe:
		aEndMarkOffsPtr				- /PTR/ pod ten adres zostanie wpisany offset znalezionego
														znacznika ko�ca ramki (o ile zosta� znaleziony)
	Zwraca:
		TRUE je�li znaleziono znacznik ko�ca ramki
		FALSE gdy nie znaleziono
*/
BOOL FindFrameEnd( BYTE *aBufferPtr, DWORD aNrOfBytesInBuffer, DWORD *aEndMarkOffsPtr );


/* Execute zakomentowane ze wagl�du na CThermalCommand i CThermalResponse */
#if 0

/*
	Execute
		Wykonaj rozkaz (wygeneruj i wy�lij ramk� oraz ewentualnie czekaj na
		odpowied� a je�li	zawiera ona pola danych - to je wczytaj/przekonwertuj)

	Argumenty wej�ciowe:
		aCThCmd								- /PTR/ rozkaz (obiekt)
		aCThResp							- /PTR/ odpowied� (obiekt)
		aResponseTime					- jak d�ugo czeka� na odpowied�
		
	Zwraca:
		SOK										- gdy operacja zako�czy�a si� pomy�lnie
		ERR_RETURN_CODE				- odpowied� z kodem b��du -> GetExtendedErrorCode()
														aby pobra� kod b��du i kod rozkazu
		ERR_NO_RESPONSE				- ca�kowity brak odpowiedzi
		ERR_RESPONSE_TIMEOUT	- przekroczony czas oczekiwania na zako�czenie odpowiedzi
		ERR_INPUT_BUF_OVRRUN	- przepe�niony bufor wej�ciowy
		ERR_FMT_HEADER				- nieprawid�owe pole znacznika pocz�tku ramki
		ERR_FMT_RETURN_CODE		- nieprawid�owe pole kodu b��du
*/
	int Execute( CThermalCommand *aCThCmd, CThermalResponse *aCThResp,
									DWORD aResponseTime = NORMAL_RESPONSE_TIME );

#endif // 0

/*
	GenerateChecksum
		Wyznacza sum� kontroln�
	
	Argumenty wej�ciowe:
		aDataPtr						- /PTR/ pocz�tek obszaru, z kt�rego zostanie wyliczona suma kontrolna
		aNrOfBytes					- liczba bajt�w, kt�re wejd� do sumy kontrolnej
	
	Zwraca:
		sum� kontroln�
*/
BYTE GenerateChecksum( BYTE *aDataPtr, DWORD aNrOfBytes );


/*
	ConvertNumericToAmount
		Zamienia warto�� numeryczn� na kwot� (obs�uga rozkaz�w w�oskich) - przerabia
		tekstowo podan� warto�� na posta� z kropk� - o ile dotyczy to Euro.

	Argumenty wej�ciowe:
		aNumStrIn						- /PTR/ warto��, kt�r� nale�y przekonwertowa�
		aEuroMode						- TRUE, je�li tryb Euro
	
	Zwraca:
		wska�nik do przekonwertowanego ci�gu (nie wolno zmienia� ci�gu, je�li zajdzie potrzeba,
		trzeba wcze�niej skopiowa�)
*/
char * ConvertNumericToAmount( char *aNumStrIn, BOOL aEuroMode );


/*
	ConvertNumericToQuantity
		Zamienia warto�� numeryczn� na ilo�� (obs�uga rozkaz�w w�oskich) - przerabia
		tekstowo podan� warto�� na posta� z kropk�.

	Argumenty wej�ciowe:
		aNumStrIn						- /PTR/ warto��, kt�r� nale�y przekonwertowa�
	
	Zwraca:
		wska�nik do przekonwertowanego ci�gu (nie wolno zmienia� ci�gu, je�li zajdzie potrzeba,
		trzeba wcze�niej skopiowa�)
*/
char * ConvertNumericToQuantity( char *aNumStrIn );


/*
	GetDeviceErrorMessage
		Pobierz tekst komunikatu o b��dzie zasygnalizowanym przez urz�dzenie
		(kod rozkazu i numer b��du)

	Argumenty wej�ciowe:
		aExtendedErrorCode	- rozszerzony kod b��du (kod rozkazu i numer b��du spakowane do DWORDa)
	
	Argumenty wej�ciowe:
		aStrOutPtr					- /PTR to PTR/ tu zostanie wpisany adres wygenerowanego tekstu komunikatu
													(nie nale�y modyfikowa� zawarto�ci tekstu !)

	Zwraca:
		TRUE je�l rozszerzony kod b��du mia� prawid�ow� posta� i wygenerowany zosta� komunikat,
		FALSE w przeciwnym razie
*/
BOOL GetDeviceErrorMessage( DWORD aExtendedErrorCode, char **aStrOutPtr );

/*
	GetDeviceExtErrorCode
		Pobierz (rozszerzony) kod b��du (ma sens tylko gdy urz�dzenie zasygnalizuje b��d)

	Argumenty:
		---

	Zwraca:
		(rozszerzony) kod b��du
*/
DWORD GetDeviceError();

/*
	Send_ACK, Send_NACK, Send_CAN, Send_BEL
		Wy�lij odpowiednio:
			ACK (potwierdzenie pozytywne)
			NACK (potwierdzenie negatywne)
			CAN (taki "soft reset" parsera)
			BEL (sygna� d�wi�kowy)

	Argumenty:
		---

	Zwraca:
		---
*/
void Send_ACK();
void Send_NACK();
void Send_CAN();
void Send_BEL();



/*
	CheckDate
		Sprawdza czy podana data jest poprawna - ale jedynie pod wzgl�dem zakres�w warto�ci.
		Nie jest sprawdzane czy dany dzie� istnieje.

	Argumenty wej�ciowe:
		aYear								- rok
		aMonth							- miesi�c
		aDay								- dzie�

	Argumenty wyj�ciowe:
		aOffendingArg				- /PTR/ tu zostanie wpisany indeks parametru, kt�ry wykracza poza
													dozwolony zakres
	Zwraca:
		TRUE je�li data jest poprawna
		FALSE je�li nie (wtedy te� wpisana zostanie warto�� w aOffendingArg)
		
*/
BOOL CheckDate( BYTE aYear, BYTE aMonth, BYTE aDay, BYTE * aOffendingArg );

#endif // _FUNCTION_DECLARATIONS