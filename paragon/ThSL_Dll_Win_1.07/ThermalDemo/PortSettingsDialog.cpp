// PortSettingsDialog.cpp : implementation file
//

#include "stdafx.h"
#include "ThermalDemo.h"
#include "PortSettingsDialog.h"

extern CThermalDemoApp theApp;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPortSettingsDialog dialog


CPortSettingsDialog::CPortSettingsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CPortSettingsDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPortSettingsDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CPortSettingsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPortSettingsDialog)
	DDX_Control(pDX, IDC_CO_PORT_NAME, m_co_Name);
	DDX_Control(pDX, IDC_CO_PORT_BAUD, m_co_Baud);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPortSettingsDialog, CDialog)
	//{{AFX_MSG_MAP(CPortSettingsDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPortSettingsDialog message handlers

/*----------------------------------------------------------------------------*/

BOOL CPortSettingsDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if( theApp.BaudRateNdx != -1 )
	{
		m_co_Baud.SetCurSel( theApp.BaudRateNdx );
	}

	if( theApp.PortNameNdx != -1 )
	{
		m_co_Name.SetCurSel( theApp.PortNameNdx );
	}
	
	return TRUE;
}

/*----------------------------------------------------------------------------*/

void CPortSettingsDialog::OnOK() 
{
	int BaudNdx, NameNdx;
	DWORD Rate;
	char TmpStr[8];
	
	BaudNdx = m_co_Baud.GetCurSel();
	NameNdx = m_co_Name.GetCurSel();

	if( BaudNdx != CB_ERR )
	{
		m_co_Baud.GetWindowText( TmpStr, sizeof(TmpStr) );
		
		sscanf( TmpStr, "%d", & Rate );
		theApp.BaudRateNdx = BaudNdx;
		theApp.BaudRate = Rate;
	}

	if( NameNdx != CB_ERR )
	{
		theApp.PortNameNdx = NameNdx;
		m_co_Name.GetWindowText( theApp.PortName );
	}

	CDialog::OnOK();
}

/*----------------------------------------------------------------------------*/

void CPortSettingsDialog::OnCancel() 
{
	CDialog::OnCancel();
}
