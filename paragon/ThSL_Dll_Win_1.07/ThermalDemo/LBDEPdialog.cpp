// LBDEPdialog.cpp : implementation file
//

#include "stdafx.h"
#include "ThermalDemo.h"
#include "LBDEPdialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CThermalDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CLBDEPdialog dialog


CLBDEPdialog::CLBDEPdialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLBDEPdialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLBDEPdialog)
	m_bf_Cancellation = FALSE;
	m_str_Amount = _T("");
	m_i_DepositSelection = -1;
	//}}AFX_DATA_INIT
}


void CLBDEPdialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLBDEPdialog)
	DDX_Control(pDX, IDC_ED_QUANTITY, m_ed_Quantity);
	DDX_Control(pDX, IDC_ST_QUANTITY, m_st_Quantity);
	DDX_Control(pDX, IDCANCEL, m_bu_Cancel);
	DDX_Control(pDX, IDC_STG_OPT_PARAMS, m_stg_OptParams);
	DDX_Control(pDX, IDC_ST_NUMBER, m_st_Number);
	DDX_Control(pDX, IDC_ED_NUMBER, m_ed_Number);
	DDX_Control(pDX, IDC_CH_OPT_PARAMS, m_ch_OptParams);
	DDX_Check(pDX, IDC_CH_CANCELLATION, m_bf_Cancellation);
	DDX_Text(pDX, IDC_ED_AMOUNT, m_str_Amount);
	DDX_Radio(pDX, IDC_RA_DEP_TAKEN, m_i_DepositSelection);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLBDEPdialog, CDialog)
	//{{AFX_MSG_MAP(CLBDEPdialog)
	ON_BN_CLICKED(IDC_B_SEND, OnBSend)
	ON_BN_CLICKED(IDC_B_KONIEC, OnBKoniec)
	ON_BN_CLICKED(IDC_B_DEFAULT, OnBDefault)
	ON_BN_CLICKED(IDC_CH_OPT_PARAMS, OnChOptParams)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLBDEPdialog message handlers

BOOL CLBDEPdialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	OnBDefault();

	return TRUE;
}

void CLBDEPdialog::OnBKoniec() 
{
	DestroyWindow();
}

void CLBDEPdialog::OnCancel() 
{
	DestroyWindow();
}

void CLBDEPdialog::PostNcDestroy() 
{
	theApp.p_CLBDEPdialog = NULL;
	delete this;

	CDialog::PostNcDestroy();
}


void CLBDEPdialog::OnBDefault() 
{
	m_bf_Cancellation = FALSE;
	m_i_DepositSelection = 0;

	UpdateData( FALSE );

	m_ch_OptParams.SetCheck( 1 );
	m_ed_Number.SetWindowText( "1" );
	m_ed_Quantity.SetWindowText( "1" );
	OnChOptParams();
}


void CLBDEPdialog::OnChOptParams() 
{
	static BOOL State;

	State = ( m_ch_OptParams.GetCheck() == 1 );

	m_ed_Number.EnableWindow( State );
	m_ed_Quantity.EnableWindow( State );

	m_st_Number.EnableWindow( State );
	m_st_Quantity.EnableWindow( State );
	m_stg_OptParams.EnableWindow( State );
}


void CLBDEPdialog::OnBSend() 
{
	static int RetI;
	static tLBDEP_PARAMS LBDEPparams;
	static CString Number, Quantity;

	UpdateData();

	LBDEPparams.Amount.ParamValuePtr = (char *) LPCTSTR( m_str_Amount );
	LBDEPparams.Amount.TypeTranslFuncPtr = NULL;

	if( m_ch_OptParams.GetCheck() == 1 )
	{
		m_ed_Number.GetWindowText( Number );
		m_ed_Quantity.GetWindowText( Quantity );

		LBDEPparams.Number = (char *) LPCTSTR( Number );
		LBDEPparams.Quantity.ParamValuePtr = (char *) LPCTSTR( Quantity );
		LBDEPparams.Quantity.TypeTranslFuncPtr = NULL;
	}
	else
	{
		LBDEPparams.Number = NULL;
		LBDEPparams.Quantity.ParamValuePtr = NULL;
		LBDEPparams.Quantity.TypeTranslFuncPtr = NULL;
	}

	if( !m_bf_Cancellation )
	{
		if( !m_i_DepositSelection )
		{
			RetI = theApp.ThSL_LBDEP_P( & LBDEPparams );
		}
		else
		{
			RetI = theApp.ThSL_LBDEP_M( & LBDEPparams );
		}
	}
	else
	{
		if( !m_i_DepositSelection )
		{
			RetI = theApp.ThSL_LBDEPSTR_P( & LBDEPparams );
		}
		else
		{
			RetI = theApp.ThSL_LBDEPSTR_M( & LBDEPparams );
		}
	}

	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBDEP !", RetI );
	}
}
