#if !defined(AFX_LBTRXENDDIALOG_H__AEED6AA3_0B5C_11D6_BAB2_00500400878B__INCLUDED_)
#define AFX_LBTRXENDDIALOG_H__AEED6AA3_0B5C_11D6_BAB2_00500400878B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LBTRXENDdialog.h : header file
//

#include "LBTRXENDcommon.h"

/////////////////////////////////////////////////////////////////////////////
// CLBTRXENDdialog dialog

class CLBTRXENDdialog : public CDialog
{
// Construction
public:
	CLBTRXENDdialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLBTRXENDdialog)
	enum { IDD = IDD_LBTRXEND };
	CButton	m_ch_Rest;
	CButton	m_ch_Discount;
	CButton	m_ra_Surcharge;
	CButton	m_ra_Discount;
	CButton	m_bu_Cancel;
	CButton	m_stg_Discount;
	CButton	m_stg_AdditionalLines;
	CStatic	m_st_Rest;
	CStatic	m_st_DiscountValue;
	CStatic	m_st_DepositTakenAmount;
	CStatic	m_st_DepositTaken;
	CStatic	m_st_DepositReturnedAmount;
	CStatic	m_st_DepositReturned;
	CStatic	m_st_CouponName;
	CStatic	m_st_CouponAmount;
	CStatic	m_st_Coupon;
	CStatic	m_st_ChequeName;
	CStatic	m_st_ChequeAmount;
	CStatic	m_st_Cheque;
	CStatic	m_st_Cash;
	CStatic	m_st_CardName;
	CStatic	m_st_CardAmount;
	CStatic	m_st_Card;
	CStatic	m_st_AdditionalLinesUD;
	CStatic	m_st_AdditionalLines;
	CSpinButtonCtrl	m_sp_AdditionalLinesUD;
	CEdit	m_ed_Total;
	CEdit	m_ed_Rest;
	CEdit	m_ed_Line5;
	CEdit	m_ed_Line4;
	CEdit	m_ed_Line3;
	CEdit	m_ed_Line2;
	CEdit	m_ed_Line1;
	CEdit	m_ed_DiscountValue;
	CEdit	m_ed_DepositTakenAmount;
	CEdit	m_ed_DepositReturnedAmount;
	CEdit	m_ed_CouponName;
	CEdit	m_ed_CouponAmount;
	CEdit	m_ed_Code;
	CEdit	m_ed_ChequeName;
	CEdit	m_ed_ChequeAmount;
	CEdit	m_ed_Cash;
	CEdit	m_ed_CardName;
	CEdit	m_ed_CardAmount;
	CButton	m_ch_DepositTaken;
	CButton	m_ch_DepositReturned;
	CButton	m_ch_Coupon;
	CButton	m_ch_Cheque;
	CButton	m_ch_Cash;
	CButton	m_ch_Card;
	CButton	m_ch_AdditLines;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLBTRXENDdialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	BOOL ChkBoxState;
	void EnableAdditionalLines( int aNrOfLines );

	// Generated message map functions
	//{{AFX_MSG(CLBTRXENDdialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnBSend();
	afx_msg void OnBDefault();
	afx_msg void OnChAdditLines();
	afx_msg void OnChCash();
	afx_msg void OnChRest();
	afx_msg void OnChCard();
	afx_msg void OnChCheque();
	afx_msg void OnChCoupon();
	afx_msg void OnChDiscount();
	afx_msg void OnChDepositTaken();
	afx_msg void OnChDepositReturned();
	afx_msg void OnDeltaposSpAdditLinesUd(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBQuit();
	virtual void OnCancel();
	afx_msg void OnBCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LBTRXENDDIALOG_H__AEED6AA3_0B5C_11D6_BAB2_00500400878B__INCLUDED_)
