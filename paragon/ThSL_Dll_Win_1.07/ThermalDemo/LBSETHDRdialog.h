#if !defined(AFX_LBSETHDRDIALOG_H__AF1AD498_4204_4327_A5EF_82A708E05134__INCLUDED_)
#define AFX_LBSETHDRDIALOG_H__AF1AD498_4204_4327_A5EF_82A708E05134__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LBSETHDRdialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLBSETHDRdialog dialog

class CLBSETHDRdialog : public CDialog
{
// Construction
public:
	CLBSETHDRdialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLBSETHDRdialog)
	enum { IDD = IDD_LBSETHDR };
	CButton	m_bu_Cancel;
	CButton	m_stg_OptParams_CashReg;
	CStatic	m_st_Cashier;
	CStatic	m_st_CashRegNr;
	CEdit	m_ed_Header;
	CEdit	m_ed_Cashier;
	CEdit	m_ed_CashRegNr;
	CButton	m_ch_OptParams_CashReg;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLBSETHDRdialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLBSETHDRdialog)
	afx_msg void OnBDefault();
	afx_msg void OnBWyslij();
	afx_msg void OnBKoniec();
	afx_msg void OnChOptParamsCashReg();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LBSETHDRDIALOG_H__AF1AD498_4204_4327_A5EF_82A708E05134__INCLUDED_)
