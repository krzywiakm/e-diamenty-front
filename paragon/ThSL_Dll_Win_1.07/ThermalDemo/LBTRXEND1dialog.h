#if !defined(AFX_LBTRXEND1DIALOG_H__AEED6AA3_0B5C_11D6_BAB2_00500400878B__INCLUDED_)
#define AFX_LBTRXEND1DIALOG_H__AEED6AA3_0B5C_11D6_BAB2_00500400878B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LBTRXEND1dialog.h : header file
//

#include "LBTRXENDcommon.h"


/////////////////////////////////////////////////////////////////////////////
// CLBTRXEND1dialog dialog

class CLBTRXEND1dialog : public CDialog
{
// Construction
public:
	CLBTRXEND1dialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLBTRXEND1dialog)
	enum { IDD = IDD_LBTRXEND1 };
	CButton	m_bu_Cancel;
	CStatic	m_st_kzi;
	CStatic	m_st_kpi;
	CEdit	m_ed_kzi;
	CEdit	m_ed_kpi;
	CStatic	m_st_lindod_ud;
	CStatic	m_st_lindod;
	CStatic	m_st_kzn;
	CStatic	m_st_kzk;
	CStatic	m_st_kpn;
	CStatic	m_st_kpk;
	CButton	m_b_kz_usun;
	CButton	m_b_kz_dodaj;
	CButton	m_b_kp_usun;
	CButton	m_b_kp_dodaj;
	CButton	m_b_fp_usun;
	CButton	m_b_fp_dodaj;
	CStatic	m_st_fpr;
	CStatic	m_st_fpn;
	CStatic	m_st_fpk;
	CStatic	m_st_reszta;
	CStatic	m_st_wplata;
	CStatic	m_st_rabat_rodz;
	CStatic	m_st_rabat_kw;
	CButton	m_group_rabat;
	CStatic	m_st_nrsyst;
	CComboBox	m_combox_rabat;
	CComboBox	m_combox_fpr;
	CListCtrl	m_listctrl_kz;
	CListCtrl	m_listctrl_kp;
	CListCtrl	m_listctrl_fp;
	CButton	m_group_lindod;
	CButton	m_group_kz;
	CButton	m_group_kp;
	CButton	m_group_fp;
	CSpinButtonCtrl	m_spin_lindod;
	CEdit	m_ed_wplata;
	CEdit	m_ed_total;
	CEdit	m_ed_reszta;
	CEdit	m_ed_rabat;
	CEdit	m_ed_nrsyst;
	CEdit	m_ed_CashRegNr;
	CEdit	m_ed_linia3;
	CEdit	m_ed_linia2;
	CEdit	m_ed_linia1;
	CEdit	m_ed_kzn;
	CEdit	m_ed_kzk;
	CEdit	m_ed_kpk;
	CEdit	m_ed_kpn;
	CEdit	m_ed_Cashier;
	CEdit	m_ed_fpn;
	CEdit	m_ed_fpk;
	CEdit	m_ed_dsp;
	CButton	m_chk_wplata;
	CButton	m_chk_reszta;
	CButton	m_chk_rabat;
	CButton	m_chk_nrsyst;
	CButton	m_chk_lindod;
	CButton	m_chk_kz;
	CButton	m_chk_kp;
	CButton	m_chk_fp;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLBTRXEND1dialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	BOOL ChkBoxState;
			
	DWORD FpItems,
			KpItems,
			KzItems;

	StrArray		DepositTakenNrStrArr;
	StrPtrArray	DepositTakenNrPtrArr;

	StrArray		DepositTakenQuantityStrArr;
	ExtNumArray	DepositTakenQuantityArr;
	
	StrArray		DepositTakenAmountStrArr;
	ExtNumArray	DepositTakenAmountArr;
	
	StrArray		DepositReturnedNrStrArr;
	StrPtrArray	DepositReturnedNrPtrArr;
	
	StrArray		DepositReturnedQuantityStrArr;
	ExtNumArray	DepositReturnedQuantityArr;
	
	StrArray		DepositReturnedAmountStrArr;
	ExtNumArray	DepositReturnedAmountArr;
	
	StrArray		FormOfPaymentNameStrArr;
	StrPtrArray	FormOfPaymentNamePtrArr;
	
	StrArray		FormOfPaymentAmountStrArr;
	ExtNumArray	FormOfPaymentAmountArr;

/*
		Ble ble ble
*/
	void EnableAdditionalLines( int );

//	void EnableAdditionalLines( int aNrOfLines );

	// Generated message map functions
	//{{AFX_MSG(CLBTRXEND1dialog)
	afx_msg void OnChNrsyst();
	afx_msg void OnChRabat();
	afx_msg void OnChWplata();
	afx_msg void OnChReszta();
	afx_msg void OnChFp();
	afx_msg void OnChKp();
	afx_msg void OnChKz();
	afx_msg void OnBWartdomysl();
	afx_msg void OnBWyslij();
	afx_msg void OnBKoniec();
	virtual BOOL OnInitDialog();
	afx_msg void OnChLindod();
	afx_msg void OnDeltaposSpLindodUd(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnFpDodaj();
	afx_msg void OnFpUsun();
	afx_msg void OnKpDodaj();
	afx_msg void OnKpUsun();
	afx_msg void OnKzDodaj();
	afx_msg void OnKzUsun();
	virtual void OnCancel();
	afx_msg void OnBAnuluj();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LBTRXEND1DIALOG_H__AEED6AA3_0B5C_11D6_BAB2_00500400878B__INCLUDED_)
