// LBSETHDRdialog.cpp : implementation file
//

#include "stdafx.h"
#include "ThermalDemo.h"
#include "LBSETHDRdialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CThermalDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CLBSETHDRdialog dialog


CLBSETHDRdialog::CLBSETHDRdialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLBSETHDRdialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLBSETHDRdialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CLBSETHDRdialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLBSETHDRdialog)
	DDX_Control(pDX, IDCANCEL, m_bu_Cancel);
	DDX_Control(pDX, IDC_STG_OPT_PARAMS_CASH_REG, m_stg_OptParams_CashReg);
	DDX_Control(pDX, IDC_ST_CASHIER, m_st_Cashier);
	DDX_Control(pDX, IDC_ST_CASH_REG_NR, m_st_CashRegNr);
	DDX_Control(pDX, IDC_ED_HEADER, m_ed_Header);
	DDX_Control(pDX, IDC_ED_CASHIER, m_ed_Cashier);
	DDX_Control(pDX, IDC_ED_CASH_REG_NR, m_ed_CashRegNr);
	DDX_Control(pDX, IDC_CH_OPT_PARAMS_CASH_REG, m_ch_OptParams_CashReg);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLBSETHDRdialog, CDialog)
	//{{AFX_MSG_MAP(CLBSETHDRdialog)
	ON_BN_CLICKED(IDC_B_DEFAULT, OnBDefault)
	ON_BN_CLICKED(IDC_B_WYSLIJ, OnBWyslij)
	ON_BN_CLICKED(IDC_B_KONIEC, OnBKoniec)
	ON_BN_CLICKED(IDC_CH_OPT_PARAMS_CASH_REG, OnChOptParamsCashReg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLBSETHDRdialog message handlers

/*----------------------------------------------------------------------------*/

void CLBSETHDRdialog::OnBDefault()
{
	m_ed_CashRegNr.SetWindowText("kas1");
	m_ed_Cashier.SetWindowText("kasjer2");

	m_ch_OptParams_CashReg.SetCheck( 1 );
	OnChOptParamsCashReg();
}

/*----------------------------------------------------------------------------*/

void CLBSETHDRdialog::OnBWyslij()
{
	static int RetI, 
		i;

	static CString Cashier,
		CashRegNr,
		Header;

	static char *pCashier,
		*pCashRegNr;

	if( m_ch_OptParams_CashReg.GetCheck() == 1 )
	{
		m_ed_Cashier.GetWindowText( Cashier );
		m_ed_CashRegNr.GetWindowText( CashRegNr );

		pCashier = (char *) LPCTSTR( Cashier );
		pCashRegNr = (char *) LPCTSTR( CashRegNr );
	}
	else
	{
/*
		Oba pola z zerowym wska�nikiem - nie zostan� umieszczone w ramce (nawet nie b�dzie ich terminator�w)
*/
		pCashier = NULL;
		pCashRegNr = NULL;
	}

	m_ed_Header.GetWindowText( Header );

	i = 0;

	while( i < Header.GetLength() )
	{
		i = Header.Find( '\n', i );

		if( i == -1 )
		{
			break;
		}
		else
		{
			Header.Delete( i, 1 );
		}
	}

	
	RetI = theApp.ThSL_LBSETHDR( (char *) LPCTSTR(Header), pCashRegNr, pCashier );
	if( RetI != SOK )
	{
			theApp.ThSL_ErrorMessage( "LBSETHDR !", RetI );
	}

}

/*----------------------------------------------------------------------------*/

void CLBSETHDRdialog::OnBKoniec() 
{
	DestroyWindow();
}

/*----------------------------------------------------------------------------*/

void CLBSETHDRdialog::OnChOptParamsCashReg() 
{
	static BOOL State;

	State = (m_ch_OptParams_CashReg.GetCheck() == 1);

	m_stg_OptParams_CashReg.EnableWindow( State );

	m_ed_Cashier.EnableWindow( State );
	m_st_Cashier.EnableWindow( State );
	
	m_ed_CashRegNr.EnableWindow( State );
	m_st_CashRegNr.EnableWindow( State );
}

/*----------------------------------------------------------------------------*/

void CLBSETHDRdialog::PostNcDestroy() 
{
	theApp.p_CLBSETHDRdialog = NULL;
	delete this;
	
	CDialog::PostNcDestroy();
}

/*----------------------------------------------------------------------------*/

BOOL CLBSETHDRdialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_bu_Cancel.EnableWindow( FALSE );
	m_bu_Cancel.ShowWindow( FALSE );

	m_ch_OptParams_CashReg.SetCheck( 1 );
	OnBDefault();
	
	return TRUE;
}

/*----------------------------------------------------------------------------*/

void CLBSETHDRdialog::OnCancel() 
{
	DestroyWindow();
}
