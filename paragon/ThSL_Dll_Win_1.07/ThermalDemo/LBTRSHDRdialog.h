#if !defined(AFX_LBTRSHDRDIALOG_H__8A17E226_0E4D_11D6_BAB2_00500400878B__INCLUDED_)
#define AFX_LBTRSHDRDIALOG_H__8A17E226_0E4D_11D6_BAB2_00500400878B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LBTRSHDRdialog.h : header file
//

#define MAX_PAR_LINES 80

#define K_DATABASE_FILE						"Database file"
#define K_DATABASE_FILE_PATH			"Path"
#define K_EURO_MODE								"Euro mode"
#define K_EURO_MODE_FLAG					"Flag"

#define K_PORT_SETTINGS						"Communication port settings"
#define K_PORT_SETTINGS_NAME_NDX	"Name index"
#define K_PORT_SETTINGS_NAME			"Name"
#define K_PORT_SETTINGS_BAUD_NDX	"Baud rate index"
#define K_PORT_SETTINGS_BAUD			"Baud rate"


/////////////////////////////////////////////////////////////////////////////
// CLBTRSHDRdialog dialog

class CLBTRSHDRdialog : public CDialog
{
// Construction
public:
	CLBTRSHDRdialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLBTRSHDRdialog)
	enum { IDD = IDD_LBTRSHDR };
	CButton	m_bu_Cancel;
	CSpinButtonCtrl	m_sp_NrOfLines;
	CStatic	m_st_NrOfLines;
	CEdit	m_ed_NrOfLines;
	CButton	m_ch_Online;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLBTRSHDRdialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	BOOL ChkBoxState;

	// Generated message map functions
	//{{AFX_MSG(CLBTRSHDRdialog)
	afx_msg void OnBWyslij();
	afx_msg void OnBKoniec();
	virtual BOOL OnInitDialog();
	afx_msg void OnChOnline();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LBTRSHDRDIALOG_H__8A17E226_0E4D_11D6_BAB2_00500400878B__INCLUDED_)
