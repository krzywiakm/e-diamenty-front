// ThermalDemo.h : main header file for the THERMALDEMO application
//

#if !defined(AFX_THERMALDEMO_H__BC3BEF24_06B5_11D6_BAB2_00500400878B__INCLUDED_)
#define AFX_THERMALDEMO_H__BC3BEF24_06B5_11D6_BAB2_00500400878B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

#include "..\\ThermalServiceLibrary.h"
#include "..\\ThermalServiceLibraryTypedefs.h"

#include "stdafx.h"
#include <afxtempl.h>

#include "MainFrm.h"
#include "GlobalStuff.h"
#include "Texts.h"
#include "LBTRSHDRdialog.h"
#include "LBTRSLNdialog.h"
#include "LBTRXEND1dialog.h"
#include "LBTRXENDdialog.h"
#include "PortSettingsDialog.h"
#include "LBSETPTUdialog.h"
#include "LBSETHDRdialog.h"
#include "LBFSTRQdialog.h"
#include "LBFSTRQrepDialog.h"
#include "LBDEPdialog.h"
#include "SnooperDialog.h"

/////////////////////////////////////////////////////////////////////////////
// CThermalDemoApp:
// See ThermalDemo.cpp for the implementation of this class
//

class CThermalDemoApp : public CWinApp
{
public:
	CThermalDemoApp();

	int OpenLibrary();
	int CloseLibrary();
	void SaveSettings();

	BOOL ThermalVersionNew;

	// do tego "uchwytu" doczepiona zostanie biblioteka
	HINSTANCE hThermalServiceLibrary;

	// HANDLE do Mailslota (podgl�d logowania sekwencji)
	HANDLE hMailSlot;

	// a tu s� odpowiednio podefiniowane wska�niki do funkcji z tej biblioteki
 	tTHSL_PORT_OPEN										ThSL_Port_Open;
	tTHSL_PORT_SET_BAUD_RATE					ThSL_Port_SetBaudRate;
	tTHSL_PORT_CLOSE									ThSL_Port_Close;
	tTHSL_PORT_LOGGING_START					ThSL_Port_LoggingStart;
	tTHSL_PORT_LOGGING_STOP						ThSL_Port_LoggingStop;

	tTHSL_ERROR_MESSAGE								ThSL_ErrorMessage;
	tTHSL_GET_DEVICE_ERROR_CODE				ThSL_GetDeviceErrorCode;
	tTHSL_SEND_ACK										ThSL_Send_ACK;
	tTHSL_SEND_NACK										ThSL_Send_NACK;

	tTHSL_GET_STATUS_CASH_REGISTER		ThSL_GetStatus_CashRegister;
	tTHSL_GET_STATUS_PRINTER					ThSL_GetStatus_Printer;

	tTHSL_LBSETCK											ThSL_LBSETCK;
	tTHSL_LBDSP												ThSL_LBDSP;
	tTHSL_LBSETPTU										ThSL_LBSETPTU;
	tTHSL_LBSETHDR										ThSL_LBSETHDR;
	tTHSL_LBFEED											ThSL_LBFEED;
	tTHSL_LBSERM											ThSL_LBSERM;
	tTHSL_LBTRSHDR										ThSL_LBTRSHDR;
	tTHSL_LBTRSLN											ThSL_LBTRSLN;
	tTHSL_O_LBTRSLN										ThSL_O_LBTRSLN;
	tTHSL_LBDEP_P											ThSL_LBDEP_P;
	tTHSL_LBDEPSTR_P									ThSL_LBDEPSTR_P;
	tTHSL_LBDEP_M											ThSL_LBDEP_M;
	tTHSL_LBDEPSTR_M									ThSL_LBDEPSTR_M;
	tTHSL_LBTREXITCAN									ThSL_LBTREXITCAN;
	tTHSL_O_LBTREXIT									ThSL_O_LBTREXIT;
	tTHSL_O_LBTRXEND									ThSL_O_LBTRXEND;
	tTHSL_LBTRFORMPLAT								ThSL_LBTRFORMPLAT;
	tTHSL_LBTRXEND1										ThSL_LBTRXEND1;

	tTHSL_LBCSHREP2										ThSL_LBCSHREP2;
	tTHSL_LBTRSCARD										ThSL_LBTRSCARD;
	tTHSL_LBSTOCARD										ThSL_LBSTOCARD;
	tTHSL_LBINCCSH										ThSL_LBINCCSH;
	tTHSL_LBDECCSH										ThSL_LBDECCSH;
	tTHSL_LBCSHSTS										ThSL_LBCSHSTS;
	tTHSL_LBCSHREP										ThSL_LBCSHREP;
	tTHSL_LBLOGIN											ThSL_LBLOGIN;
	tTHSL_LBLOGOUT										ThSL_LBLOGOUT;
	tTHSL_LBFSKREP_D									ThSL_LBFSKREP_D;
	tTHSL_LBFSKREP_R									ThSL_LBFSKREP_R;
	tTHSL_LBDAYREP										ThSL_LBDAYREP;

	tTHSL_LBDBREPRS										ThSL_LBDBREPRS;
	tTHSL_LBSENDCK										ThSL_LBSENDCK;
	tTHSL_LBFSTRQ											ThSL_LBFSTRQ;
	tTHSL_O_LBFSTRQ										ThSL_O_LBFSTRQ;
	tTHSL_LBERNRQ											ThSL_LBERNRQ;
	tTHSL_LBIDRQ											ThSL_LBIDRQ;
	tTHSL_LBSNDMD											ThSL_LBSNDMD;
	tTHSL_LBCASREP										ThSL_LBCASREP;

	tTHSL_LBFSTRQ24										ThSL_LBFSTRQ24;
	tTHSL_LBFSTRQ25										ThSL_LBFSTRQ25;
	tTHSL_LBFSTRQ26										ThSL_LBFSTRQ26;
	tTHSL_LBFSTRQ27										ThSL_LBFSTRQ27;
	

	CLBTRSHDRdialog										*p_CLBTRSHDRdialog;
	CLBTRSLNdialog										*p_CLBTRSLNdialog;
	CLBTRXENDdialog										*p_CLBTRXENDdialog;
	CLBTRXEND1dialog									*p_CLBTRXEND1dialog;
	CLBSETPTUdialog										*p_CLBSETPTUdialog;
	CLBSETHDRdialog										*p_CLBSETHDRdialog;
	CLBFSTRQdialog										*p_CLBFSTRQdialog;
	CLBFSTRQrepDialog									*p_CLBFSTRQrepDialog;
	CLBDEPdialog											*p_CLBDEPdialog;

	CSnooperDialog										*p_CSnooperDialog;
	CPortSettingsDialog								*p_CPortSettingsDialog;

	int	BaudRateNdx,
		PortNameNdx;
	DWORD BaudRate;

	CString PortName;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CThermalDemoApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

public:
	//{{AFX_MSG(CThermalDemoApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THERMALDEMO_H__BC3BEF24_06B5_11D6_BAB2_00500400878B__INCLUDED_)
