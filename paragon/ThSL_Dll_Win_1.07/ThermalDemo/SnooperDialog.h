#if !defined(AFX_SNOOPERDIALOG_H__C8555C89_43CE_49CC_B243_5E172CD96E40__INCLUDED_)
#define AFX_SNOOPERDIALOG_H__C8555C89_43CE_49CC_B243_5E172CD96E40__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SnooperDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSnooperDialog dialog

class CSnooperDialog : public CDialog
{
// Construction
public:
	CSnooperDialog(CWnd* pParent = NULL);   // standard constructor

	void AddLine( char * aString );

	HANDLE hMSlut;			// MailSlut ;-)
	volatile BOOL ExitFlag;

// Dialog Data
	//{{AFX_DATA(CSnooperDialog)
	enum { IDD = IDD_SNOOPER };
	CListBox	m_lb_Log;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSnooperDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	DWORD NrOfLines;
	CWinThread * Thread;
	
	// Generated message map functions
	//{{AFX_MSG(CSnooperDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SNOOPERDIALOG_H__C8555C89_43CE_49CC_B243_5E172CD96E40__INCLUDED_)
