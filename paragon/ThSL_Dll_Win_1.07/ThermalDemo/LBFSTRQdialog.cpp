// LBFSTRQdialog.cpp : implementation file
//

#include "stdafx.h"
#include "ThermalDemo.h"
#include "LBFSTRQdialog.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CThermalDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CLBFSTRQdialog dialog


CLBFSTRQdialog::CLBFSTRQdialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLBFSTRQdialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLBFSTRQdialog)
	//}}AFX_DATA_INIT
}


void CLBFSTRQdialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLBFSTRQdialog)
	DDX_Control(pDX, IDC_DT_TIME, m_dt_Time);
	DDX_Control(pDX, IDC_ED_NUMBER, m_ed_Number);
	DDX_Control(pDX, IDC_RA_NUMBER, m_ra_Number);
	DDX_Control(pDX, IDC_RA_DATE, m_ra_Date);
	DDX_Control(pDX, IDC_DT_DATE, m_dt_Date);
	DDX_Control(pDX, IDC_TR_FMEM_RECORDS, m_tr_FMem_Records);
	DDX_Control(pDX, IDCANCEL, m_bu_Cancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLBFSTRQdialog, CDialog)
	//{{AFX_MSG_MAP(CLBFSTRQdialog)
	ON_BN_CLICKED(IDC_RA_DATE, OnRaDate)
	ON_BN_CLICKED(IDC_RA_NUMBER, OnRaNumber)
	ON_BN_CLICKED(IDC_B_GET_RECORDS, OnBGetRecords)
	ON_BN_CLICKED(IDC_B_KONIEC, OnBKoniec)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLBFSTRQdialog message handlers

/*----------------------------------------------------------------------------*/

BOOL CLBFSTRQdialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_bu_Cancel.EnableWindow( FALSE );
	m_bu_Cancel.ShowWindow( FALSE );

	m_ra_Number.SetCheck( 1 );
	OnRaNumber();

	m_ed_Number.SetWindowText( "0" );
	
	return TRUE;
}

/*----------------------------------------------------------------------------*/

void CLBFSTRQdialog::PostNcDestroy() 
{
	theApp.p_CLBFSTRQdialog = NULL;
	delete this;

	CDialog::PostNcDestroy();
}

/*----------------------------------------------------------------------------*/

void CLBFSTRQdialog::OnCancel() 
{
	DestroyWindow();
}

/*----------------------------------------------------------------------------*/

void CLBFSTRQdialog::OnBKoniec() 
{
	DestroyWindow();	
}


/******************************************************************************/

void CLBFSTRQdialog::FirstRecordSelection( BOOL aByDate ) 
{
	m_dt_Date.EnableWindow( aByDate );
	m_dt_Time.EnableWindow( aByDate );
	m_ed_Number.EnableWindow( !aByDate );
}

/*----------------------------------------------------------------------------*/

void CLBFSTRQdialog::OnRaDate() 
{
	FirstRecordSelection( m_ra_Date.GetCheck() == 1 );
}

/*----------------------------------------------------------------------------*/

void CLBFSTRQdialog::OnRaNumber() 
{
	FirstRecordSelection( m_ra_Date.GetCheck() == 1 );
}

/******************************************************************************/

void CLBFSTRQdialog::OnBGetRecords() 
{
	static int RetI;
	static tLBFSTRQ_PARAMS LBFSTRQparams;
	static tLBFSTRQ25_PARAMS LBFSTRQ25params;

	static tLBFSTRQ1_RESPONSE LBFSTRQ1response;
	static tLBFSTRQ27_RESPONSE LBFSTRQ27response;

	static CString RecordNrStr, DateStr, TmpStr;
	static DWORD CurrRecordNr; //, NrOfRecords;
	static BYTE CurrRecordType;

	static BOOL MoreRecords;

	static HTREEITEM hTreeItem1, hTreeItem2, hTreeItem3;

/*
#define LBFSTRQ27_DAILY_REPORT			(BYTE) 10
#define LBFSTRQ27_VAT_CHANGE				(BYTE) 11
#define LBFSTRQ27_RAM_CLEAR					(BYTE) 12
#define LBFSTRQ27_SALE_AFTER_CLEAR	(BYTE) 13
#define LBFSTRQ27_NO_MORE_RECORDS		(BYTE) 25
*/

/*
	Pierwszy rekord mo�e by� okre�lony za pomoc� daty lub numeru.
*/
	if( m_ra_Number.GetCheck() == 1 )		/* wyb�r przez numer  ? */
	{
		m_ed_Number.GetWindowText( RecordNrStr );
			
		if( (!RecordNrStr.IsEmpty() ) &&		/* jest co� wpisane ? */
				(sscanf(LPCTSTR(RecordNrStr), "%d", &CurrRecordNr) == 1) )
		{
			RetI = theApp.ThSL_LBFSTRQ26( CurrRecordNr );

			if( RetI != SOK )
			{
				theApp.ThSL_ErrorMessage( "LBFSTRQ26 !", RetI );
				return;
			}
		}
	}
	else	/* wyb�r przez dat� - DOROBI� !!! */
	{
		COleDateTime OleDate, OleTime;

		DWORD Year;

		m_dt_Date.GetTime( OleDate );

		Year = OleDate.GetYear();

		if( (Year >= 1950) && (Year <= 2049) )
		{
			if( Year > 1999 )
			{
				Year -= 2000;
			}
			else
			{
				Year -= 1900;
			}
		}
		else
		{
			WarningMessageBox( WARNMSG_LBFSTRQ25_DATE );
			return;
		}

		LBFSTRQ25params.Py = (BYTE) Year;
		LBFSTRQ25params.Pm = (BYTE) OleDate.GetMonth();
		LBFSTRQ25params.Pd = (BYTE) OleDate.GetDay();

		m_dt_Time.GetTime( OleTime );
		LBFSTRQ25params.Ph = (BYTE) OleTime.GetHour();
		LBFSTRQ25params.Pmin = (BYTE) OleTime.GetMinute();
		LBFSTRQ25params.Psec = (BYTE) OleTime.GetSecond();

		RetI = theApp.ThSL_LBFSTRQ25( & LBFSTRQ25params );

		if( RetI != SOK )
		{
			theApp.ThSL_ErrorMessage( "LBFSTRQ25 !", RetI );
			return;
		}
	}

	m_tr_FMem_Records.DeleteAllItems();

	MoreRecords = TRUE;

	while( MoreRecords )		/* dop�ki s� rekordy... */
	{
		RetI = theApp.ThSL_LBFSTRQ27( & LBFSTRQ27response, & CurrRecordType );
		
		if( RetI != SOK )
		{
			theApp.ThSL_ErrorMessage( "LBFSTRQ27 !", RetI );
			return;
		}

		DateStr.Format( "%4d-%02d-%02d %02d:%02d:%02d  ",
											LBFSTRQ27response.Year,
											LBFSTRQ27response.Month,
											LBFSTRQ27response.Day,
											LBFSTRQ27response.Hour,
											LBFSTRQ27response.Min,
											LBFSTRQ27response.Sec );

		switch( CurrRecordType )
		{
			default:
				_ASSERTE( FALSE );
				break;
/*
#define LBFSTRQ27_DAILY_REPORT			(BYTE) 10
#define LBFSTRQ27_VAT_CHANGE				(BYTE) 11
#define LBFSTRQ27_RAM_CLEAR					(BYTE) 12
#define LBFSTRQ27_SALE_AFTER_CLEAR	(BYTE) 13
#define LBFSTRQ27_NO_MORE_RECORDS		(BYTE) 25
*/
			case LBFSTRQ27_NO_MORE_RECORDS:		/* koniec rekord�w */
				MoreRecords = FALSE;
				break;

			case LBFSTRQ27_DAILY_REPORT:
				hTreeItem1 = m_tr_FMem_Records.InsertItem( DateStr + STR_LBFSTRQ27_DAILY_REPORT );
				hTreeItem2 = m_tr_FMem_Records.InsertItem( STR_COUNTERS, hTreeItem1 );

				TmpStr.Format( "%d", LBFSTRQ27response.Num1 );
				hTreeItem3 = m_tr_FMem_Records.InsertItem( STR_RECEIPT_CNT + TmpStr, hTreeItem2 );

				TmpStr.Format( "%d", LBFSTRQ27response.Num2 );
				hTreeItem3 = m_tr_FMem_Records.InsertItem( STR_CANC_CNT + TmpStr, hTreeItem2 );
				
				TmpStr.Format( "%d", LBFSTRQ27response.Num3 );
				hTreeItem3 = m_tr_FMem_Records.InsertItem( STR_BCHNG_CNT + TmpStr, hTreeItem2 );

				
				hTreeItem2 = m_tr_FMem_Records.InsertItem( STR_TOTALIZERS, hTreeItem1 );

				TmpStr = LBFSTRQ27response.Val1;
				hTreeItem3 = m_tr_FMem_Records.InsertItem( STR_CANC_CNT + TmpStr, hTreeItem2 );

				TmpStr = LBFSTRQ27response.ValA;
				hTreeItem3 = m_tr_FMem_Records.InsertItem( STR_A + TmpStr, hTreeItem2 );

				TmpStr = LBFSTRQ27response.ValB;
				hTreeItem3 = m_tr_FMem_Records.InsertItem( STR_B + TmpStr, hTreeItem2 );

				TmpStr = LBFSTRQ27response.ValC;
				hTreeItem3 = m_tr_FMem_Records.InsertItem( STR_C + TmpStr, hTreeItem2 );

				TmpStr = LBFSTRQ27response.ValD;
				hTreeItem3 = m_tr_FMem_Records.InsertItem( STR_D + TmpStr, hTreeItem2 );

				TmpStr = LBFSTRQ27response.ValE;
				hTreeItem3 = m_tr_FMem_Records.InsertItem( STR_E + TmpStr, hTreeItem2 );
				
				TmpStr = LBFSTRQ27response.ValF;
				hTreeItem3 = m_tr_FMem_Records.InsertItem( STR_F + TmpStr, hTreeItem2 );

				TmpStr = LBFSTRQ27response.ValG;
				hTreeItem3 = m_tr_FMem_Records.InsertItem( STR_G + TmpStr, hTreeItem2 );
				break;

			case LBFSTRQ27_VAT_CHANGE:
				hTreeItem1 = m_tr_FMem_Records.InsertItem( DateStr + STR_LBFSTRQ27_VAT_CHANGE );

				TmpStr = LBFSTRQ27response.ValA;
				hTreeItem2 = m_tr_FMem_Records.InsertItem( STR_A + TmpStr, hTreeItem1 );

				TmpStr = LBFSTRQ27response.ValB;
				hTreeItem2 = m_tr_FMem_Records.InsertItem( STR_B + TmpStr, hTreeItem1 );

				TmpStr = LBFSTRQ27response.ValC;
				hTreeItem2 = m_tr_FMem_Records.InsertItem( STR_C + TmpStr, hTreeItem1 );

				TmpStr = LBFSTRQ27response.ValD;
				hTreeItem2 = m_tr_FMem_Records.InsertItem( STR_D + TmpStr, hTreeItem1 );

				TmpStr = LBFSTRQ27response.ValE;
				hTreeItem2 = m_tr_FMem_Records.InsertItem( STR_E + TmpStr, hTreeItem1 );
				
				TmpStr = LBFSTRQ27response.ValF;
				hTreeItem2 = m_tr_FMem_Records.InsertItem( STR_F + TmpStr, hTreeItem1 );

				TmpStr = LBFSTRQ27response.ValG;
				hTreeItem2 = m_tr_FMem_Records.InsertItem( STR_G + TmpStr, hTreeItem1 );
				break;

			case LBFSTRQ27_RAM_CLEAR:
				hTreeItem1 = m_tr_FMem_Records.InsertItem( DateStr + STR_LBFSTRQ27_RAM_CLEAR );

				TmpStr.Format( "%d", LBFSTRQ27response.Num1 );
				hTreeItem2 = m_tr_FMem_Records.InsertItem( STR_REASON + TmpStr, hTreeItem1 );

				TmpStr.Format( "%d", LBFSTRQ27response.Num2 );
				hTreeItem2 = m_tr_FMem_Records.InsertItem( STR_NUMBER + TmpStr, hTreeItem1 );
				break;

			case LBFSTRQ27_SALE_AFTER_CLEAR:
				hTreeItem1 = m_tr_FMem_Records.InsertItem( DateStr + STR_LBFSTRQ27_SALE_AFTER_CLEAR );
				break;

		}
	}

}
