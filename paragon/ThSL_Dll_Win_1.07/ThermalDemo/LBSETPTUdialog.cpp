// LBSETPTUdialog.cpp : implementation file
//

#include "stdafx.h"
#include "ThermalDemo.h"
#include "LBSETPTUdialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CThermalDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CLBSETPTUdialog dialog


CLBSETPTUdialog::CLBSETPTUdialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLBSETPTUdialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLBSETPTUdialog)
	//}}AFX_DATA_INIT
}


void CLBSETPTUdialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLBSETPTUdialog)
	DDX_Control(pDX, IDC_DT_DATE, m_dt_Date);
	DDX_Control(pDX, IDCANCEL, m_bu_Cancel);
	DDX_Control(pDX, IDC_STG_OPT_PARAMS_DATE, m_stg_OptParams_Date);
	DDX_Control(pDX, IDC_STG_OPT_PARAMS_CASH_REG, m_stg_OptParams_CashReg);
	DDX_Control(pDX, IDC_CH_OPT_PARAMS_DATE, m_ch_OptParams_Date);
	DDX_Control(pDX, IDC_CH_OPT_PARAMS_CASH_REG, m_ch_OptParams_CashReg);
	DDX_Control(pDX, IDC_ED_VAT_F, m_ed_VatF);
	DDX_Control(pDX, IDC_CH_VAT_F_TAX_FREE, m_ch_VatF_TaxFree);
	DDX_Control(pDX, IDC_SP_NR_OF_RATES_UD, m_sp_NrOfRates);
	DDX_Control(pDX, IDC_ST_NR_OF_RATES_UD, m_st_NrOfRates);
	DDX_Control(pDX, IDC_ST_VAT_G, m_st_VatG);
	DDX_Control(pDX, IDC_ST_VAT_F, m_st_VatF);
	DDX_Control(pDX, IDC_ST_VAT_E, m_st_VatE);
	DDX_Control(pDX, IDC_ST_VAT_D, m_st_VatD);
	DDX_Control(pDX, IDC_ST_VAT_C, m_st_VatC);
	DDX_Control(pDX, IDC_ST_VAT_B, m_st_VatB);
	DDX_Control(pDX, IDC_ST_VAT_A, m_st_VatA);
	DDX_Control(pDX, IDC_ST_CASHIER, m_st_Cashier);
	DDX_Control(pDX, IDC_ST_CASH_REG_NR, m_st_CashRegNr);
	DDX_Control(pDX, IDC_ED_VAT_G, m_ed_VatG);
	DDX_Control(pDX, IDC_ED_VAT_E, m_ed_VatE);
	DDX_Control(pDX, IDC_ED_VAT_D, m_ed_VatD);
	DDX_Control(pDX, IDC_ED_VAT_C, m_ed_VatC);
	DDX_Control(pDX, IDC_ED_VAT_B, m_ed_VatB);
	DDX_Control(pDX, IDC_ED_VAT_A, m_ed_VatA);
	DDX_Control(pDX, IDC_ED_CASHIER, m_ed_Cashier);
	DDX_Control(pDX, IDC_ED_CASH_REG_NR, m_ed_CashRegNr);
	DDX_Control(pDX, IDC_CH_VAT_B_TAX_FREE, m_ch_VatB_TaxFree);
	DDX_Control(pDX, IDC_CH_VAT_G_TAX_FREE, m_ch_VatG_TaxFree);
	DDX_Control(pDX, IDC_CH_VAT_E_TAX_FREE, m_ch_VatE_TaxFree);
	DDX_Control(pDX, IDC_CH_VAT_D_TAX_FREE, m_ch_VatD_TaxFree);
	DDX_Control(pDX, IDC_CH_VAT_C_TAX_FREE, m_ch_VatC_TaxFree);
	DDX_Control(pDX, IDC_CH_VAT_A_TAX_FREE, m_ch_VatA_TaxFree);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLBSETPTUdialog, CDialog)
	//{{AFX_MSG_MAP(CLBSETPTUdialog)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SP_NR_OF_RATES_UD, OnDeltaposSpNrOfRatesUd)
	ON_BN_CLICKED(IDC_CH_VAT_A_TAX_FREE, OnChVatATaxFree)
	ON_BN_CLICKED(IDC_CH_VAT_B_TAX_FREE, OnChVatBTaxFree)
	ON_BN_CLICKED(IDC_CH_VAT_C_TAX_FREE, OnChVatCTaxFree)
	ON_BN_CLICKED(IDC_CH_VAT_D_TAX_FREE, OnChVatDTaxFree)
	ON_BN_CLICKED(IDC_CH_VAT_G_TAX_FREE, OnChVatGTaxFree)
	ON_BN_CLICKED(IDC_CH_VAT_E_TAX_FREE, OnChVatETaxFree)
	ON_BN_CLICKED(IDC_CH_VAT_F_TAX_FREE, OnChVatFTaxFree)
	ON_BN_CLICKED(IDC_B_DEFAULT, OnBDefault)
	ON_BN_CLICKED(IDC_B_WYSLIJ, OnBWyslij)
	ON_BN_CLICKED(IDC_B_KONIEC, OnBKoniec)
	ON_BN_CLICKED(IDC_CH_OPT_PARAMS_CASH_REG, OnChOptParamsCashReg)
	ON_BN_CLICKED(IDC_CH_OPT_PARAMS_DATE, OnChOptParamsDate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLBSETPTUdialog message handlers

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnDeltaposSpNrOfRatesUd(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;

	EnableVatRates( pNMUpDown->iPos + pNMUpDown->iDelta );
	
	
	*pResult = 0;
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::EnableVatRates( DWORD aNrOfRates )
{
	if( (aNrOfRates > 0) && (aNrOfRates <= MAX_VAT_RATES) )
	{
		switch( aNrOfRates )
		{
			default:	
				// NO BREAK HERE

			case 1:
				m_st_VatA.EnableWindow( FALSE );
				m_ch_VatA_TaxFree.EnableWindow( FALSE );
				m_ed_VatA.EnableWindow( FALSE );
				// NO BREAK HERE

			case 2:
				m_st_VatB.EnableWindow( FALSE );
				m_ch_VatB_TaxFree.EnableWindow( FALSE );
				m_ed_VatB.EnableWindow( FALSE );
				// NO BREAK HERE

			case 3:
				m_st_VatC.EnableWindow( FALSE );
				m_ch_VatC_TaxFree.EnableWindow( FALSE );
				m_ed_VatC.EnableWindow( FALSE );
				// NO BREAK HERE

			case 4:
				m_st_VatD.EnableWindow( FALSE );
				m_ch_VatD_TaxFree.EnableWindow( FALSE );
				m_ed_VatD.EnableWindow( FALSE );
				// NO BREAK HERE

			case 5:
				m_st_VatE.EnableWindow( FALSE );
				m_ch_VatE_TaxFree.EnableWindow( FALSE );
				m_ed_VatE.EnableWindow( FALSE );
				// NO BREAK HERE

			case 6:
				m_st_VatF.EnableWindow( FALSE );
				m_ch_VatF_TaxFree.EnableWindow( FALSE );
				m_ed_VatF.EnableWindow( FALSE );
				// NO BREAK HERE

			case 7:
				m_st_VatG.EnableWindow( FALSE );
				m_ch_VatG_TaxFree.EnableWindow( FALSE );
				m_ed_VatG.EnableWindow( FALSE );
				// NO BREAK HERE
		}

		switch( aNrOfRates )
		{
			default:	
				// NO BREAK HERE

			case 7:
				m_st_VatG.EnableWindow( TRUE );
				m_ch_VatG_TaxFree.EnableWindow( TRUE );
				OnChVatGTaxFree();
				// NO BREAK HERE

			case 6:
				m_st_VatF.EnableWindow( TRUE );
				m_ch_VatF_TaxFree.EnableWindow( TRUE );
				OnChVatFTaxFree();
				// NO BREAK HERE

			case 5:
				m_st_VatE.EnableWindow( TRUE );
				m_ch_VatE_TaxFree.EnableWindow( TRUE );
				OnChVatETaxFree();
				// NO BREAK HERE

			case 4:
				m_st_VatD.EnableWindow( TRUE );
				m_ch_VatD_TaxFree.EnableWindow( TRUE );
				OnChVatDTaxFree();
				// NO BREAK HERE

			case 3:
				m_st_VatC.EnableWindow( TRUE );
				m_ch_VatC_TaxFree.EnableWindow( TRUE );
				OnChVatCTaxFree();
				// NO BREAK HERE

			case 2:
				m_st_VatB.EnableWindow( TRUE );
				m_ch_VatB_TaxFree.EnableWindow( TRUE );
				OnChVatBTaxFree();
				// NO BREAK HERE

			case 1:
				m_st_VatA.EnableWindow( TRUE );
				m_ch_VatA_TaxFree.EnableWindow( TRUE );
				OnChVatATaxFree();
				// NO BREAK HERE
		}
	}
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnChVatATaxFree() 
{
	m_ed_VatA.EnableWindow( m_ch_VatA_TaxFree.GetCheck() == 0 );
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnChVatBTaxFree() 
{
	m_ed_VatB.EnableWindow( m_ch_VatB_TaxFree.GetCheck() == 0 );
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnChVatCTaxFree() 
{
	m_ed_VatC.EnableWindow( m_ch_VatC_TaxFree.GetCheck() == 0 );
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnChVatDTaxFree() 
{
	m_ed_VatD.EnableWindow( m_ch_VatD_TaxFree.GetCheck() == 0 );
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnChVatETaxFree() 
{
	m_ed_VatE.EnableWindow( m_ch_VatE_TaxFree.GetCheck() == 0 );
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnChVatFTaxFree() 
{
	m_ed_VatF.EnableWindow( m_ch_VatF_TaxFree.GetCheck() == 0 );
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnChVatGTaxFree() 
{
	m_ed_VatG.EnableWindow( m_ch_VatG_TaxFree.GetCheck() == 0 );
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::DefaultValues()
{
	static int RetI;
	static tLBSENDCK_RESPONSE LBSENDCK_Resp;
	static CString TmpStr;
	static COleDateTime OleDate;
	static DWORD Year;

	m_ch_VatA_TaxFree.SetCheck( 0 );
	m_ch_VatB_TaxFree.SetCheck( 0 );
	m_ch_VatC_TaxFree.SetCheck( 0 );
	m_ch_VatD_TaxFree.SetCheck( 0 );
	m_ch_VatE_TaxFree.SetCheck( 0 );
	m_ch_VatF_TaxFree.SetCheck( 1 );
	m_ch_VatG_TaxFree.SetCheck( 1 );

	m_st_NrOfRates.SetWindowText( "1" );
	m_sp_NrOfRates.SetPos( MAX_VAT_RATES );
	EnableVatRates( MAX_VAT_RATES );

	m_ed_CashRegNr.SetWindowText("kas1");
	m_ed_Cashier.SetWindowText("kasjer2");

	m_ed_VatA.SetWindowText( "7.00");
	m_ed_VatB.SetWindowText( "8.00");
	m_ed_VatC.SetWindowText( "9.00");
	m_ed_VatD.SetWindowText( "10.00");
	m_ed_VatE.SetWindowText( "11.00");
	m_ed_VatF.SetWindowText( "");
	m_ed_VatG.SetWindowText( "");

	m_ch_OptParams_CashReg.SetCheck( 1 );
	OnChOptParamsCashReg();
	
	RetI = theApp.ThSL_LBSENDCK( & LBSENDCK_Resp );
	if( RetI != SOK )
	{
		m_ch_OptParams_Date.SetCheck( 0 );
		theApp.ThSL_ErrorMessage( "LBSENDCK !", RetI );
	}
	else
	{
		m_ch_OptParams_Date.SetCheck( 1 );

		if( LBSENDCK_Resp.Pyy > 49 )
		{
			Year = 1900 + LBSENDCK_Resp.Pyy;
		}
		else
		{
			Year = 2000 + LBSENDCK_Resp.Pyy;
		}

		OleDate.SetDate( Year, LBSENDCK_Resp.Pmm, LBSENDCK_Resp.Pdd );

		m_dt_Date.SetTime( OleDate );
	}

	OnChOptParamsDate();
}

/*----------------------------------------------------------------------------*/

BOOL CLBSETPTUdialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_bu_Cancel.EnableWindow( FALSE );
	m_bu_Cancel.ShowWindow( FALSE );
	
	m_sp_NrOfRates.SetRange( 1, MAX_VAT_RATES );
	
	DefaultValues();

	m_ch_VatFree[0]		= & m_ch_VatA_TaxFree;
	m_ed_VatRate[0]		= & m_ed_VatA;

	m_ch_VatFree[1]		= & m_ch_VatB_TaxFree;
	m_ed_VatRate[1]		= & m_ed_VatB;

	m_ch_VatFree[2]		= & m_ch_VatC_TaxFree;
	m_ed_VatRate[2]		= & m_ed_VatC;

	m_ch_VatFree[3]		= & m_ch_VatD_TaxFree;
	m_ed_VatRate[3]		= & m_ed_VatD;

	m_ch_VatFree[4]		= & m_ch_VatE_TaxFree;
	m_ed_VatRate[4]		= & m_ed_VatE;

	m_ch_VatFree[5]		= & m_ch_VatF_TaxFree;
	m_ed_VatRate[5]		= & m_ed_VatF;

	m_ch_VatFree[6]		= & m_ch_VatG_TaxFree;
	m_ed_VatRate[6]		= & m_ed_VatG;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnBDefault() 
{
	DefaultValues();
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnBWyslij() 
{
	static int RetI;
	static DWORD NrOfVatRates,
		i,
		Year;

	static BYTE Date[3];

	static tLBSETPTU_PARAMS LBSETPTUparams;

	static CString Cashier,
		CashRegNr,
		VatRatesStr[7],
		Free,
		Inactive;

	static tEXTERNAL_NUMERIC VatRates[7];

	static COleDateTime OleDate;

	Free = "100";
	Inactive = "101";

/*
	
*/
	memset( VatRates, 0, sizeof(VatRates) );

	if( m_ch_OptParams_CashReg.GetCheck() == 1 )
	{
		m_ed_Cashier.GetWindowText( Cashier );
		m_ed_CashRegNr.GetWindowText( CashRegNr );

		LBSETPTUparams.Cashier = (char *) LPCTSTR( Cashier );
		LBSETPTUparams.CashRegNr = (char *) LPCTSTR( CashRegNr );
	}
	else
	{
/*
		Oba pola z zerowym wska�nikiem - nie zostan� umieszczone w ramce (nawet nie b�dzie ich separator�w)
*/
		LBSETPTUparams.Cashier = NULL;
		LBSETPTUparams.CashRegNr = NULL;
	}

	LBSETPTUparams._Date = 0;	/* inicjalizujemy - brak daty */

	if( m_ch_OptParams_Date.GetCheck() == 1 )	/* ale mo�e jednak jest wybrana data ? */
	{
		m_dt_Date.GetTime( OleDate );

		Year = OleDate.GetYear();
		if( (Year >= 1950) && (Year <= 2049) )
		{
			if( Year > 1999 )
			{
				Year -= 2000;
			}
			else
			{
				Year -= 1900;
			}
		}
		else
		{
			WarningMessageBox( WARNMSG_LBFSTRQ25_DATE );
			return;
		}

		LBSETPTUparams._Date = 1;
		LBSETPTUparams.Py = (BYTE) Year;
		LBSETPTUparams.Pm = (BYTE) OleDate.GetMonth();
		LBSETPTUparams.Pd = (BYTE) OleDate.GetDay();
	}


	NrOfVatRates = m_sp_NrOfRates.GetPos();

	for( i = 0; i < NrOfVatRates; i++ )
	{
		if( m_ch_VatFree[i]->GetCheck() == 1 )
		{
			VatRates[i].ParamValuePtr = (char *) LPCTSTR( Free );
		}
		else
		{
			m_ed_VatRate[i]->GetWindowText( VatRatesStr[i] );
			VatRates[i].ParamValuePtr = (char *) LPCTSTR( VatRatesStr[i] );
		}
	}

	LBSETPTUparams.Ps = (BYTE) NrOfVatRates;
	LBSETPTUparams.VatRates = VatRates;

	RetI = theApp.ThSL_LBSETPTU( & LBSETPTUparams );
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBSETPTU !", RetI );
	}

}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnBKoniec() 
{
	DestroyWindow();
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnChOptParamsCashReg() 
{
	static BOOL State;

	State = (m_ch_OptParams_CashReg.GetCheck() == 1);

	m_stg_OptParams_CashReg.EnableWindow( State );

	m_ed_Cashier.EnableWindow( State );
	m_st_Cashier.EnableWindow( State );
	
	m_ed_CashRegNr.EnableWindow( State );
	m_st_CashRegNr.EnableWindow( State );

}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnChOptParamsDate() 
{
	static BOOL State;

	State = (m_ch_OptParams_Date.GetCheck() == 1);

	m_stg_OptParams_Date.EnableWindow( State );

	m_dt_Date.EnableWindow( State );
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::PostNcDestroy() 
{
	theApp.p_CLBSETPTUdialog = NULL;
	delete this;
	
	CDialog::PostNcDestroy();
}

/*----------------------------------------------------------------------------*/

void CLBSETPTUdialog::OnCancel() 
{
	DestroyWindow();
}
