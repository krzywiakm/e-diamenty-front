// LBFSTRQrepDialog.cpp : implementation file
//

#include "stdafx.h"
#include "ThermalDemo.h"
#include "LBFSTRQrepDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CThermalDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CLBFSTRQrepDialog dialog


CLBFSTRQrepDialog::CLBFSTRQrepDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLBFSTRQrepDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLBFSTRQrepDialog)
	m_bf_Mode_Fiscal = FALSE;
	m_bf_Mode_Training = FALSE;
	m_bf_Mode_Transaction = FALSE;
	m_bf_PrevSeqErr = FALSE;
	m_bf_TRF = FALSE;
	m_str_Cash = _T("");
	m_str_LastWrite = _T("");
	m_str_NrOfRAMclr = _T("");
	m_str_NrOfReceipts = _T("");
	m_str_SerialNumber = _T("");
	m_str_TotA = _T("");
	m_str_TotB = _T("");
	m_str_TotC = _T("");
	m_str_TotD = _T("");
	m_str_TotE = _T("");
	m_str_TotF = _T("");
	m_str_TotG = _T("");
	m_str_VatA = _T("");
	m_str_VatB = _T("");
	m_str_VatC = _T("");
	m_str_VatD = _T("");
	m_str_VatE = _T("");
	m_str_VatF = _T("");
	m_str_VatG = _T("");
	m_str_FiscalizationDate = _T("");
	m_str_NrOfBlocked = _T("");
	m_str_NrOfDailyRec = _T("");
	m_str_NrOfFreeRec = _T("");
	m_str_ReceiptA = _T("");
	m_str_ReceiptB = _T("");
	m_str_ReceiptC = _T("");
	m_str_ReceiptD = _T("");
	m_str_ReceiptE = _T("");
	m_str_ReceiptF = _T("");
	m_str_ReceiptG = _T("");
	//}}AFX_DATA_INIT
}


void CLBFSTRQrepDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLBFSTRQrepDialog)
	DDX_Control(pDX, IDCANCEL, m_bu_Cancel);
	DDX_Check(pDX, IDC_CH_MODE_FISCAL, m_bf_Mode_Fiscal);
	DDX_Check(pDX, IDC_CH_MODE_TRAINING, m_bf_Mode_Training);
	DDX_Check(pDX, IDC_CH_MODE_TRANSACTION, m_bf_Mode_Transaction);
	DDX_Check(pDX, IDC_CH_PREV_SEQ_ERR, m_bf_PrevSeqErr);
	DDX_Check(pDX, IDC_CH_FLAG_TRF, m_bf_TRF);
	DDX_Text(pDX, IDC_ST_CASH, m_str_Cash);
	DDX_Text(pDX, IDC_ST_LAST_WRITE, m_str_LastWrite);
	DDX_Text(pDX, IDC_ST_NR_OF_RAM_CLR, m_str_NrOfRAMclr);
	DDX_Text(pDX, IDC_ST_NR_OF_RECEIPTS, m_str_NrOfReceipts);
	DDX_Text(pDX, IDC_ST_SERIAL_NUMBER, m_str_SerialNumber);
	DDX_Text(pDX, IDC_ST_TOT_A, m_str_TotA);
	DDX_Text(pDX, IDC_ST_TOT_B, m_str_TotB);
	DDX_Text(pDX, IDC_ST_TOT_C, m_str_TotC);
	DDX_Text(pDX, IDC_ST_TOT_D, m_str_TotD);
	DDX_Text(pDX, IDC_ST_TOT_E, m_str_TotE);
	DDX_Text(pDX, IDC_ST_TOT_F, m_str_TotF);
	DDX_Text(pDX, IDC_ST_TOT_G, m_str_TotG);
	DDX_Text(pDX, IDC_ST_VAT_A, m_str_VatA);
	DDX_Text(pDX, IDC_ST_VAT_B, m_str_VatB);
	DDX_Text(pDX, IDC_ST_VAT_C, m_str_VatC);
	DDX_Text(pDX, IDC_ST_VAT_D, m_str_VatD);
	DDX_Text(pDX, IDC_ST_VAT_E, m_str_VatE);
	DDX_Text(pDX, IDC_ST_VAT_F, m_str_VatF);
	DDX_Text(pDX, IDC_ST_VAT_G, m_str_VatG);
	DDX_Text(pDX, IDC_ST_FISCALIZATION_DATE, m_str_FiscalizationDate);
	DDX_Text(pDX, IDC_ST_NR_OF_BLOCKED, m_str_NrOfBlocked);
	DDX_Text(pDX, IDC_ST_NR_OF_DAILY_REC, m_str_NrOfDailyRec);
	DDX_Text(pDX, IDC_ST_NR_OF_FREE_REC, m_str_NrOfFreeRec);
	DDX_Text(pDX, IDC_ST_RECEIPT_A, m_str_ReceiptA);
	DDX_Text(pDX, IDC_ST_RECEIPT_B, m_str_ReceiptB);
	DDX_Text(pDX, IDC_ST_RECEIPT_C, m_str_ReceiptC);
	DDX_Text(pDX, IDC_ST_RECEIPT_D, m_str_ReceiptD);
	DDX_Text(pDX, IDC_ST_RECEIPT_E, m_str_ReceiptE);
	DDX_Text(pDX, IDC_ST_RECEIPT_F, m_str_ReceiptF);
	DDX_Text(pDX, IDC_ST_RECEIPT_G, m_str_ReceiptG);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLBFSTRQrepDialog, CDialog)
	//{{AFX_MSG_MAP(CLBFSTRQrepDialog)
	ON_BN_CLICKED(IDC_B_KONIEC, OnBKoniec)
	ON_BN_CLICKED(IDC_B_UPDATE, OnBUpdate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLBFSTRQrepDialog message handlers

BOOL CLBFSTRQrepDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_bu_Cancel.EnableWindow( FALSE );
	m_bu_Cancel.ShowWindow( SW_HIDE );
	
	return TRUE;
}

void CLBFSTRQrepDialog::OnCancel() 
{
	DestroyWindow();
}

void CLBFSTRQrepDialog::PostNcDestroy() 
{
	theApp.p_CLBFSTRQrepDialog = NULL;
	delete this;
	
	CDialog::PostNcDestroy();
}

void CLBFSTRQrepDialog::OnBKoniec() 
{
	DestroyWindow();
}

void CLBFSTRQrepDialog::OnBUpdate() 
{
	static int RetI;
	static DWORD Year;
	static tLBFSTRS1_RESPONSE LBFSTRS1resp;
	static tLBFSTRQ1_RESPONSE LBFSTRQ1resp;
	
/*
	Najpierw LBFSTRQ z parametrem Ps==23
*/
	RetI = theApp.ThSL_LBFSTRQ( & LBFSTRS1resp );
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBFSTRQ !", RetI );
	}

	m_bf_PrevSeqErr =			( LBFSTRS1resp.Pe != 0 );
	m_bf_Mode_Fiscal =		( LBFSTRS1resp.Pm == 1 );
	m_bf_Mode_Training =	!m_bf_Mode_Fiscal;

	m_bf_Mode_Transaction = ( LBFSTRS1resp.Pt == 1 );

	m_bf_TRF =						( LBFSTRS1resp.Px == 1 );

	m_str_NrOfRAMclr.Format( "%d", LBFSTRS1resp.Pz );

	Year = LBFSTRS1resp.Pyy;

	if( Year > 49 )
	{
		Year += 1900;
	}
	else
	{
		Year += 2000;
	}

	m_str_LastWrite.Format( "%4d-%02d-%02d", Year, LBFSTRS1resp.Pmm, LBFSTRS1resp.Pdd );

	m_str_VatA = LBFSTRS1resp.VatA;
	m_str_VatB = LBFSTRS1resp.VatB;
	m_str_VatC = LBFSTRS1resp.VatC;
	m_str_VatD = LBFSTRS1resp.VatD;
	m_str_VatE = LBFSTRS1resp.VatE;
	m_str_VatF = LBFSTRS1resp.VatF;
	m_str_VatG = LBFSTRS1resp.VatG;

	m_str_NrOfReceipts.Format( "%d", LBFSTRS1resp.NrOfReceipts );

	m_str_TotA = LBFSTRS1resp.TotA;
	m_str_TotB = LBFSTRS1resp.TotB;
	m_str_TotC = LBFSTRS1resp.TotC;
	m_str_TotD = LBFSTRS1resp.TotD;
	m_str_TotE = LBFSTRS1resp.TotE;
	m_str_TotF = LBFSTRS1resp.TotF;
	m_str_TotG = LBFSTRS1resp.TotG;

	m_str_Cash = LBFSTRS1resp.Cash;
	m_str_SerialNumber = LBFSTRS1resp.SerialNr;

/*
	A teraz LBFSTRQ z parametrem Ps==24
*/
	RetI = theApp.ThSL_LBFSTRQ24( & LBFSTRQ1resp );
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBFSTRQ24 !", RetI );
	}

	m_str_FiscalizationDate.Format( "%4d-%02d-%02d",
																	LBFSTRQ1resp.Py, LBFSTRQ1resp.Pm, LBFSTRQ1resp.Pd );

	m_str_ReceiptA = LBFSTRQ1resp.AmountA;
	m_str_ReceiptB = LBFSTRQ1resp.AmountB;
	m_str_ReceiptC = LBFSTRQ1resp.AmountC;
	m_str_ReceiptD = LBFSTRQ1resp.AmountD;
	m_str_ReceiptE = LBFSTRQ1resp.AmountE;
	m_str_ReceiptF = LBFSTRQ1resp.AmountF;
	m_str_ReceiptG = LBFSTRQ1resp.AmountG;

	m_str_NrOfDailyRec.Format( "%d", LBFSTRQ1resp.DailyRecNr );
	m_str_NrOfFreeRec.Format( "%d", LBFSTRQ1resp.DailyRecRem );
	m_str_NrOfBlocked.Format( "%d", LBFSTRQ1resp.Blocked );

	UpdateData( FALSE );
}

