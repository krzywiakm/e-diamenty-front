#ifndef _GLOBAL_STUFF
#define _GLOBAL_STUFF

#include "stdafx.h"
#include <afxtempl.h>
#include "Texts.h"


void ErrorMessageBox( char * aStrPtr );
void WarningMessageBox( char * aStrPtr );
void InfoMessageBox( char * aStrPtr );


#endif // _GLOBAL_STUFF