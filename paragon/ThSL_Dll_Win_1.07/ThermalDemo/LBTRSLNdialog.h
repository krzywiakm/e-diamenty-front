#if !defined(AFX_LBTRSLNDIALOG_H__8A17E227_0E4D_11D6_BAB2_00500400878B__INCLUDED_)
#define AFX_LBTRSLNDIALOG_H__8A17E227_0E4D_11D6_BAB2_00500400878B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LBTRSLNdialog.h : header file
//

#include <afxtempl.h>


/////////////////////////////////////////////////////////////////////////////
// CLBTRSLNdialog dialog

class CLBTRSLNdialog : public CDialog
{
// Construction
public:
	CLBTRSLNdialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLBTRSLNdialog)
	enum { IDD = IDD_LBTRSLN };
	CButton	m_bu_Cancel;
	CButton	m_group_rabat;
	CStatic	m_st_rabat_rodz;
	CStatic	m_st_rabat_kw;
	CListCtrl	m_listctrl_paragon;
	CEdit	m_ed_rabat;
	CEdit	m_ed_nazwa;
	CEdit	m_ed_ilosc;
	CEdit	m_ed_cena;
	CEdit	m_ed_brutto;
	CComboBox	m_combox_rabat;
	CComboBox	m_combox_ptu;
	CButton	m_chk_rabat;
	CButton	m_b_usun;
	CButton	m_b_dodaj;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLBTRSLNdialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	void Dodaj( DWORD aItemNr );

	BOOL ChkBoxState;
			
	DWORD ParItems,
		LineNumber;


	// Generated message map functions
	//{{AFX_MSG(CLBTRSLNdialog)
	afx_msg void OnBDodaj();
	afx_msg void OnBUsun();
	afx_msg void OnChRabat();
	afx_msg void OnBWartdomysl();
	afx_msg void OnBWyslij();
	afx_msg void OnBKoniec();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LBTRSLNDIALOG_H__8A17E227_0E4D_11D6_BAB2_00500400878B__INCLUDED_)
