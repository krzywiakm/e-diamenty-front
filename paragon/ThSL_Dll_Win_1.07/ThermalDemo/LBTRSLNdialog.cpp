// LBTRSLNdialog.cpp : implementation file
//

#include "stdafx.h"
#include "ThermalDemo.h"
#include "LBTRSLNdialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CThermalDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CLBTRSLNdialog dialog


CLBTRSLNdialog::CLBTRSLNdialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLBTRSLNdialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLBTRSLNdialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CLBTRSLNdialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLBTRSLNdialog)
	DDX_Control(pDX, IDCANCEL, m_bu_Cancel);
	DDX_Control(pDX, IDC_STG_RABAT, m_group_rabat);
	DDX_Control(pDX, IDC_ST_RABATR, m_st_rabat_rodz);
	DDX_Control(pDX, IDC_ST_RABATK, m_st_rabat_kw);
	DDX_Control(pDX, IDC_LI_PARAGON, m_listctrl_paragon);
	DDX_Control(pDX, IDC_ED_RABAT, m_ed_rabat);
	DDX_Control(pDX, IDC_ED_NAZWA, m_ed_nazwa);
	DDX_Control(pDX, IDC_ED_ILOSC, m_ed_ilosc);
	DDX_Control(pDX, IDC_ED_CENA, m_ed_cena);
	DDX_Control(pDX, IDC_ED_BRUTTO, m_ed_brutto);
	DDX_Control(pDX, IDC_CO_RABAT, m_combox_rabat);
	DDX_Control(pDX, IDC_CO_PTU, m_combox_ptu);
	DDX_Control(pDX, IDC_CH_RABAT, m_chk_rabat);
	DDX_Control(pDX, IDC_B_USUN, m_b_usun);
	DDX_Control(pDX, IDC_B_DODAJ, m_b_dodaj);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLBTRSLNdialog, CDialog)
	//{{AFX_MSG_MAP(CLBTRSLNdialog)
	ON_BN_CLICKED(IDC_B_DODAJ, OnBDodaj)
	ON_BN_CLICKED(IDC_B_USUN, OnBUsun)
	ON_BN_CLICKED(IDC_CH_RABAT, OnChRabat)
	ON_BN_CLICKED(IDC_B_WARTDOMYSL, OnBWartdomysl)
	ON_BN_CLICKED(IDC_B_WYSLIJ, OnBWyslij)
	ON_BN_CLICKED(IDC_B_KONIEC, OnBKoniec)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLBTRSLNdialog message handlers

/*----------------------------------------------------------------------------*/

void CLBTRSLNdialog::Dodaj( DWORD aItemNr )
{
	static CString TempStr;

	TempStr.Format( "%d", aItemNr );
	m_listctrl_paragon.InsertItem( ParItems, LPCTSTR(TempStr) );

	m_ed_nazwa.GetWindowText( TempStr );
	m_listctrl_paragon.SetItemText( ParItems, 1, LPCTSTR(TempStr) );

	m_ed_ilosc.GetWindowText( TempStr );
	m_listctrl_paragon.SetItemText( ParItems, 2, LPCTSTR(TempStr) );

	m_combox_ptu.GetWindowText( TempStr );
	m_listctrl_paragon.SetItemText( ParItems, 3, LPCTSTR(TempStr) );

	m_ed_cena.GetWindowText( TempStr );
	m_listctrl_paragon.SetItemText( ParItems, 4, LPCTSTR(TempStr) );

	m_ed_brutto.GetWindowText( TempStr );
	m_listctrl_paragon.SetItemText( ParItems, 5, LPCTSTR(TempStr) );

	if( ChkBoxState )
	{
		m_combox_rabat.GetWindowText( TempStr );
		m_listctrl_paragon.SetItemText( ParItems, 6, LPCTSTR(TempStr) );

		m_ed_rabat.GetWindowText( TempStr );
		m_listctrl_paragon.SetItemText( ParItems, 7, LPCTSTR(TempStr) );
	}
}

/*----------------------------------------------------------------------------*/

void CLBTRSLNdialog::OnBDodaj() 
{
	Dodaj( LineNumber++ );
	OnBWyslij();
	ParItems++;
}

/*----------------------------------------------------------------------------*/

void CLBTRSLNdialog::OnBUsun() 
{
	Dodaj( 0 );
	OnBWyslij();
	ParItems++;
}

/*----------------------------------------------------------------------------*/

void CLBTRSLNdialog::OnChRabat() 
{
	ChkBoxState = ( m_chk_rabat.GetCheck() == 1);

	m_ed_rabat.EnableWindow( ChkBoxState );
	m_group_rabat.EnableWindow( ChkBoxState );
	m_st_rabat_kw.EnableWindow( ChkBoxState );
	m_st_rabat_rodz.EnableWindow( ChkBoxState );
	m_combox_rabat.EnableWindow( ChkBoxState );
}

/*----------------------------------------------------------------------------*/

void CLBTRSLNdialog::OnBWartdomysl() 
{
	// Domy�lnie - brak rabatu
	m_chk_rabat.SetCheck( 0 );
	OnChRabat();

	m_combox_rabat.SetCurSel( 0 );
	m_listctrl_paragon.DeleteAllItems();

	ParItems = 0;													// Pozycje paragonu

	m_b_dodaj.EnableWindow( TRUE );
	m_b_usun.EnableWindow( FALSE );

	m_ed_nazwa.SetWindowText("towar1");
	m_ed_ilosc.SetWindowText("10");
	m_combox_ptu.SetCurSel( 0 );
	m_ed_cena.SetWindowText("1.20");
	m_ed_brutto.SetWindowText("12");
	m_ed_rabat.SetWindowText("");

	LineNumber = 1;												// Numer linii - w przypadku sprzeda�y
}

/*----------------------------------------------------------------------------*/

void CLBTRSLNdialog::OnBWyslij() 
{
	static int RetI;

	static DWORD dwNumber;

	static tLBTRSLN_PARAMS LBTRSLNparams;

	static CString Number,
		Name,
		Quantity,
		Vat,
		Price,
		Gross,
		Discnt,
		DiscntType;

	Number = m_listctrl_paragon.GetItemText( ParItems, 0 );
	sscanf( Number, "%d", & dwNumber );

	LBTRSLNparams.Pi = (BYTE) ( dwNumber );										// Pi

	Name = m_listctrl_paragon.GetItemText( ParItems, 1 );
	Quantity = m_listctrl_paragon.GetItemText( ParItems, 2 );
	Vat = m_listctrl_paragon.GetItemText( ParItems, 3 );
	Price = m_listctrl_paragon.GetItemText( ParItems, 4 );
	Gross = m_listctrl_paragon.GetItemText( ParItems, 5 );

	DiscntType = m_listctrl_paragon.GetItemText( ParItems, 6 );	// rodzaj rabatu
	Discnt = m_listctrl_paragon.GetItemText( ParItems, 7 );			// warto�� rabatu

	if( (!Discnt.IsEmpty()) && (!DiscntType.IsEmpty()) )
	{
		// Oczywi�cie kolejno�� inna ni� w przypadku LBTRXEND1...
		if( DiscntType.CompareNoCase("Rab. %") == 0 )
		{
			LBTRSLNparams.Pr = LBTRSLN_DISCNT_PERCNT;
		}
		else
		{
			if( DiscntType.CompareNoCase("Rab. kw.") == 0 )
			{
				LBTRSLNparams.Pr = LBTRSLN_DISCNT_AMT;
			}
			else
			{
				if( DiscntType.CompareNoCase("Dop�. %") == 0 )
				{
					LBTRSLNparams.Pr = LBTRSLN_SURCH_PERCNT;
				}
				else
				{
					LBTRSLNparams.Pr = LBTRSLN_SURCH_AMT;		/* Pozostaje dop�ata kwotowa */
				}
			}
		}
	}
	else
	{
		LBTRSLNparams.Pr = DISCOUNT_NONE;
		Discnt = "0";
	}

	LBTRSLNparams.Name = (char *) LPCTSTR( Name );

	LBTRSLNparams.Quantity.ParamValuePtr = (char *) LPCTSTR( Quantity );
	LBTRSLNparams.Quantity.TypeTranslFuncPtr = NULL;

	LBTRSLNparams.Vat = Vat.GetAt( 0 );

	LBTRSLNparams.Price.ParamValuePtr = (char *) LPCTSTR( Price );
	LBTRSLNparams.Price.TypeTranslFuncPtr = NULL;

	LBTRSLNparams.Gross.ParamValuePtr = (char *) LPCTSTR( Gross );
	LBTRSLNparams.Gross.TypeTranslFuncPtr = NULL;

	LBTRSLNparams.Discount.ParamValuePtr = (char *) LPCTSTR( Discnt );
	LBTRSLNparams.Discount.TypeTranslFuncPtr = NULL;

/*
	R�nice w starej i nowej homologacji - dlatego s� trzeba wywo�ywa� inne
	funkcje.
*/
	if( theApp.ThermalVersionNew )
	{
		RetI = theApp.ThSL_LBTRSLN( & LBTRSLNparams );
	}
	else
	{
		RetI = theApp.ThSL_O_LBTRSLN( & LBTRSLNparams );
	}

	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBTRSLN !", RetI );
	}
}

/*----------------------------------------------------------------------------*/

void CLBTRSLNdialog::OnBKoniec() 
{
	DestroyWindow();
}

/*----------------------------------------------------------------------------*/

BOOL CLBTRSLNdialog::OnInitDialog() 
{
	static int Width;

	CDialog::OnInitDialog();

	m_bu_Cancel.EnableWindow( FALSE );
	m_bu_Cancel.ShowWindow( FALSE );

	Width = m_listctrl_paragon.GetStringWidth( "__Nr__" );
	m_listctrl_paragon.InsertColumn( 0, "Nr.", LVCFMT_RIGHT, Width, 1 );

	Width = m_listctrl_paragon.GetStringWidth( "___Nazwa__" );
	m_listctrl_paragon.InsertColumn( 1, "Nazwa", LVCFMT_RIGHT, Width, 2 );

	Width = m_listctrl_paragon.GetStringWidth( "__Ilo��_" );
	m_listctrl_paragon.InsertColumn( 2, "Ilo��", LVCFMT_RIGHT, Width, 3 );

	Width = m_listctrl_paragon.GetStringWidth( "_Ptu_" );
	m_listctrl_paragon.InsertColumn( 3, "Ptu", LVCFMT_RIGHT, Width, 4 );

	Width = m_listctrl_paragon.GetStringWidth( "__Cena__" );
	m_listctrl_paragon.InsertColumn( 4, "Cena", LVCFMT_RIGHT, Width, 5 );

	Width = m_listctrl_paragon.GetStringWidth( "__Brutto__" );
	m_listctrl_paragon.InsertColumn( 5, "Brutto", LVCFMT_RIGHT, Width, 6 );

	Width = m_listctrl_paragon.GetStringWidth( "_Rab. rodz._" );
	m_listctrl_paragon.InsertColumn( 6, "Rab. rodz.", LVCFMT_RIGHT, Width, 7 );

	Width = m_listctrl_paragon.GetStringWidth( "_Rab. wart._" );
	m_listctrl_paragon.InsertColumn( 7, "Rab. wart.", LVCFMT_RIGHT, Width, 8 );

	OnBWartdomysl();

	return TRUE;
}

/*----------------------------------------------------------------------------*/

void CLBTRSLNdialog::PostNcDestroy() 
{
	theApp.p_CLBTRSLNdialog = NULL;
	delete this;
	
	CDialog::PostNcDestroy();
}

/*----------------------------------------------------------------------------*/

void CLBTRSLNdialog::OnCancel() 
{
	DestroyWindow();
}
