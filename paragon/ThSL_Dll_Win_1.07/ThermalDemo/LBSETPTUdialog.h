#if !defined(AFX_LBSETPTUDIALOG_H__139A7B7C_2976_4D61_B928_C157312F44E9__INCLUDED_)
#define AFX_LBSETPTUDIALOG_H__139A7B7C_2976_4D61_B928_C157312F44E9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LBSETPTUdialog.h : header file
//

#define MAX_VAT_RATES 7

/////////////////////////////////////////////////////////////////////////////
// CLBSETPTUdialog dialog

class CLBSETPTUdialog : public CDialog
{
// Construction
public:
	CLBSETPTUdialog(CWnd* pParent = NULL);   // standard constructor

	CButton *m_ch_VatActive[7],
		*m_ch_VatFree[7];

	CEdit *m_ed_VatRate[7];

// Dialog Data
	//{{AFX_DATA(CLBSETPTUdialog)
	enum { IDD = IDD_LBSETPTU };
	CDateTimeCtrl	m_dt_Date;
	CButton	m_bu_Cancel;
	CButton	m_stg_OptParams_Date;
	CButton	m_stg_OptParams_CashReg;
	CButton	m_ch_OptParams_Date;
	CButton	m_ch_OptParams_CashReg;
	CButton	m_ch_OptionalParams;
	CEdit	m_ed_VatF;
	CButton	m_ch_VatF_TaxFree;
	CSpinButtonCtrl	m_sp_NrOfRates;
	CStatic	m_st_NrOfRates;
	CStatic	m_st_VatG;
	CStatic	m_st_VatF;
	CStatic	m_st_VatE;
	CStatic	m_st_VatD;
	CStatic	m_st_VatC;
	CStatic	m_st_VatB;
	CStatic	m_st_VatA;
	CStatic	m_st_Cashier;
	CStatic	m_st_CashRegNr;
	CEdit	m_ed_VatG;
	CEdit	m_ed_VatE;
	CEdit	m_ed_VatD;
	CEdit	m_ed_VatC;
	CEdit	m_ed_VatB;
	CEdit	m_ed_VatA;
	CEdit	m_ed_Cashier;
	CEdit	m_ed_CashRegNr;
	CButton	m_ch_VatB_TaxFree;
	CButton	m_ch_VatG_TaxFree;
	CButton	m_ch_VatE_TaxFree;
	CButton	m_ch_VatD_TaxFree;
	CButton	m_ch_VatC_TaxFree;
	CButton	m_ch_VatA_TaxFree;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLBSETPTUdialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	void EnableVatRates( DWORD aNrOfRates );
	void DefaultValues();

	// Generated message map functions
	//{{AFX_MSG(CLBSETPTUdialog)
	afx_msg void OnDeltaposSpNrOfRatesUd(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChVatATaxFree();
	afx_msg void OnChVatBTaxFree();
	afx_msg void OnChVatCTaxFree();
	afx_msg void OnChVatDTaxFree();
	afx_msg void OnChVatGTaxFree();
	afx_msg void OnChVatETaxFree();
	afx_msg void OnChVatFTaxFree();
	virtual BOOL OnInitDialog();
	afx_msg void OnBDefault();
	afx_msg void OnBWyslij();
	afx_msg void OnBKoniec();
	afx_msg void OnChOptParamsCashReg();
	afx_msg void OnChOptParamsDate();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LBSETPTUDIALOG_H__139A7B7C_2976_4D61_B928_C157312F44E9__INCLUDED_)
