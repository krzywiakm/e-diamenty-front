// LBTRXEND1dialog.cpp : implementation file
//

#include "stdafx.h"
#include "ThermalDemo.h"
#include "LBTRXEND1dialog.h"

extern CThermalDemoApp theApp;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CLBTRXEND1dialog dialog


CLBTRXEND1dialog::CLBTRXEND1dialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLBTRXEND1dialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLBTRXEND1dialog)
	//}}AFX_DATA_INIT
}


void CLBTRXEND1dialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLBTRXEND1dialog)
	DDX_Control(pDX, IDCANCEL, m_bu_Cancel);
	DDX_Control(pDX, IDC_ST_KZI, m_st_kzi);
	DDX_Control(pDX, IDC_ST_KPI, m_st_kpi);
	DDX_Control(pDX, IDC_ED_KZI, m_ed_kzi);
	DDX_Control(pDX, IDC_ED_KPI, m_ed_kpi);
	DDX_Control(pDX, IDC_ST_LINDOD_UD, m_st_lindod_ud);
	DDX_Control(pDX, IDC_ST_LINDOD, m_st_lindod);
	DDX_Control(pDX, IDC_ST_KZN, m_st_kzn);
	DDX_Control(pDX, IDC_ST_KZK, m_st_kzk);
	DDX_Control(pDX, IDC_ST_KPN, m_st_kpn);
	DDX_Control(pDX, IDC_ST_KPK, m_st_kpk);
	DDX_Control(pDX, IDC_B_KZ_USUN, m_b_kz_usun);
	DDX_Control(pDX, IDC_B_KZ_DODAJ, m_b_kz_dodaj);
	DDX_Control(pDX, IDC_B_KP_USUN, m_b_kp_usun);
	DDX_Control(pDX, IDC_B_KP_DODAJ, m_b_kp_dodaj);
	DDX_Control(pDX, IDC_B_FP_USUN, m_b_fp_usun);
	DDX_Control(pDX, IDC_B_FP_DODAJ, m_b_fp_dodaj);
	DDX_Control(pDX, IDC_ST_FPR, m_st_fpr);
	DDX_Control(pDX, IDC_ST_FPN, m_st_fpn);
	DDX_Control(pDX, IDC_ST_FPK, m_st_fpk);
	DDX_Control(pDX, IDC_ST_RESZTA, m_st_reszta);
	DDX_Control(pDX, IDC_ST_WPLATA, m_st_wplata);
	DDX_Control(pDX, IDC_ST_RR, m_st_rabat_rodz);
	DDX_Control(pDX, IDC_ST_RK, m_st_rabat_kw);
	DDX_Control(pDX, IDC_STG_RABAT, m_group_rabat);
	DDX_Control(pDX, IDC_ST_NRSYST, m_st_nrsyst);
	DDX_Control(pDX, IDC_CO_R, m_combox_rabat);
	DDX_Control(pDX, IDC_CO_FPR, m_combox_fpr);
	DDX_Control(pDX, IDC_LI_KZ, m_listctrl_kz);
	DDX_Control(pDX, IDC_LI_KP, m_listctrl_kp);
	DDX_Control(pDX, IDC_LI_FP, m_listctrl_fp);
	DDX_Control(pDX, IDC_STG_LINDOD, m_group_lindod);
	DDX_Control(pDX, IDC_STG_KZ, m_group_kz);
	DDX_Control(pDX, IDC_STG_KP, m_group_kp);
	DDX_Control(pDX, IDC_STG_FP, m_group_fp);
	DDX_Control(pDX, IDC_SP_LINDOD_UD, m_spin_lindod);
	DDX_Control(pDX, IDC_ED_WPLATA, m_ed_wplata);
	DDX_Control(pDX, IDC_ED_TOTAL, m_ed_total);
	DDX_Control(pDX, IDC_ED_RESZTA, m_ed_reszta);
	DDX_Control(pDX, IDC_ED_R, m_ed_rabat);
	DDX_Control(pDX, IDC_ED_NRSYST, m_ed_nrsyst);
	DDX_Control(pDX, IDC_ED_NRKASY, m_ed_CashRegNr);
	DDX_Control(pDX, IDC_ED_LINIA3, m_ed_linia3);
	DDX_Control(pDX, IDC_ED_LINIA2, m_ed_linia2);
	DDX_Control(pDX, IDC_ED_LINIA1, m_ed_linia1);
	DDX_Control(pDX, IDC_ED_KZN, m_ed_kzn);
	DDX_Control(pDX, IDC_ED_KZK, m_ed_kzk);
	DDX_Control(pDX, IDC_ED_KPK, m_ed_kpk);
	DDX_Control(pDX, IDC_ED_KPN, m_ed_kpn);
	DDX_Control(pDX, IDC_ED_KASJER, m_ed_Cashier);
	DDX_Control(pDX, IDC_ED_FPN, m_ed_fpn);
	DDX_Control(pDX, IDC_ED_FPK, m_ed_fpk);
	DDX_Control(pDX, IDC_ED_DSP, m_ed_dsp);
	DDX_Control(pDX, IDC_CH_WPLATA, m_chk_wplata);
	DDX_Control(pDX, IDC_CH_RESZTA, m_chk_reszta);
	DDX_Control(pDX, IDC_CH_RABAT, m_chk_rabat);
	DDX_Control(pDX, IDC_CH_NRSYST, m_chk_nrsyst);
	DDX_Control(pDX, IDC_CH_LINDOD, m_chk_lindod);
	DDX_Control(pDX, IDC_CH_KZ, m_chk_kz);
	DDX_Control(pDX, IDC_CH_KP, m_chk_kp);
	DDX_Control(pDX, IDC_CH_FP, m_chk_fp);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLBTRXEND1dialog, CDialog)
	//{{AFX_MSG_MAP(CLBTRXEND1dialog)
	ON_BN_CLICKED(IDC_CH_NRSYST, OnChNrsyst)
	ON_BN_CLICKED(IDC_CH_RABAT, OnChRabat)
	ON_BN_CLICKED(IDC_CH_WPLATA, OnChWplata)
	ON_BN_CLICKED(IDC_CH_RESZTA, OnChReszta)
	ON_BN_CLICKED(IDC_CH_FP, OnChFp)
	ON_BN_CLICKED(IDC_CH_KP, OnChKp)
	ON_BN_CLICKED(IDC_CH_KZ, OnChKz)
	ON_BN_CLICKED(IDC_B_WARTDOMYSL, OnBWartdomysl)
	ON_BN_CLICKED(IDC_B_WYSLIJ, OnBWyslij)
	ON_BN_CLICKED(IDC_B_KONIEC, OnBKoniec)
	ON_BN_CLICKED(IDC_CH_LINDOD, OnChLindod)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SP_LINDOD_UD, OnDeltaposSpLindodUd)
	ON_BN_CLICKED(IDC_B_FP_DODAJ, OnFpDodaj)
	ON_BN_CLICKED(IDC_B_FP_USUN, OnFpUsun)
	ON_BN_CLICKED(IDC_B_KP_DODAJ, OnKpDodaj)
	ON_BN_CLICKED(IDC_B_KP_USUN, OnKpUsun)
	ON_BN_CLICKED(IDC_B_KZ_DODAJ, OnKzDodaj)
	ON_BN_CLICKED(IDC_B_KZ_USUN, OnKzUsun)
	ON_BN_CLICKED(IDC_B_ANULUJ, OnBAnuluj)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLBTRXEND1dialog message handlers

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnChNrsyst() 
{
	ChkBoxState = ( m_chk_nrsyst.GetCheck() == 1);

	m_ed_nrsyst.EnableWindow( ChkBoxState );
	m_st_nrsyst.EnableWindow( ChkBoxState );
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnChRabat() 
{
	ChkBoxState = ( m_chk_rabat.GetCheck() == 1);

	m_ed_rabat.EnableWindow( ChkBoxState );
	m_group_rabat.EnableWindow( ChkBoxState );
	m_st_rabat_kw.EnableWindow( ChkBoxState );
	m_st_rabat_rodz.EnableWindow( ChkBoxState );
	m_combox_rabat.EnableWindow( ChkBoxState );
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnChWplata() 
{
	ChkBoxState = ( m_chk_wplata.GetCheck() == 1);

	m_ed_wplata.EnableWindow( ChkBoxState );
	m_st_wplata.EnableWindow( ChkBoxState );
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnChReszta() 
{
	ChkBoxState = ( m_chk_reszta.GetCheck() == 1);

	m_ed_reszta.EnableWindow( ChkBoxState );
	m_st_reszta.EnableWindow( ChkBoxState );
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnChFp() 
{
	ChkBoxState = ( m_chk_fp.GetCheck() == 1);

	m_ed_fpk.EnableWindow( ChkBoxState );
	m_ed_fpn.EnableWindow( ChkBoxState );
	m_group_fp.EnableWindow( ChkBoxState );
	m_combox_fpr.EnableWindow( ChkBoxState );
	m_listctrl_fp.EnableWindow( ChkBoxState );
	m_st_fpk.EnableWindow( ChkBoxState );
	m_st_fpn.EnableWindow( ChkBoxState );
	m_st_fpr.EnableWindow( ChkBoxState );

	if( FpItems == LBTRXEND1_MAX_FP_ITEMS )
	{
		m_b_fp_dodaj.EnableWindow( FALSE );
	}
	else
	{
		m_b_fp_dodaj.EnableWindow( ChkBoxState );
	}

	if( FpItems )
	{
		m_b_fp_usun.EnableWindow( ChkBoxState );
	}
	else
	{
		m_b_fp_usun.EnableWindow( FALSE );
	}

}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnChKp() 
{
	ChkBoxState = ( m_chk_kp.GetCheck() == 1);

	m_ed_kpn.EnableWindow( ChkBoxState );
	m_ed_kpk.EnableWindow( ChkBoxState );
	m_ed_kpi.EnableWindow( ChkBoxState );
	m_group_kp.EnableWindow( ChkBoxState );
	m_listctrl_kp.EnableWindow( ChkBoxState );
	
	m_st_kpk.EnableWindow( ChkBoxState );
	m_st_kpn.EnableWindow( ChkBoxState );
	m_st_kpi.EnableWindow( ChkBoxState );
	
	m_b_kp_dodaj.EnableWindow( ChkBoxState );
	
	if( KpItems )
	{
		m_b_kp_usun.EnableWindow( ChkBoxState );
	}
	else
	{
		m_b_kp_usun.EnableWindow( FALSE );
	}
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnChKz() 
{
	ChkBoxState = ( m_chk_kz.GetCheck() == 1);

	m_ed_kzn.EnableWindow( ChkBoxState );
	m_ed_kzk.EnableWindow( ChkBoxState );
	m_ed_kzi.EnableWindow( ChkBoxState );
	m_group_kz.EnableWindow( ChkBoxState );
	m_listctrl_kz.EnableWindow( ChkBoxState );
	
	m_st_kzk.EnableWindow( ChkBoxState );
	m_st_kzn.EnableWindow( ChkBoxState );
	m_st_kzi.EnableWindow( ChkBoxState );
	
	m_b_kz_dodaj.EnableWindow( ChkBoxState );

	if( KzItems )
	{
		m_b_kz_usun.EnableWindow( ChkBoxState );
	}
	else
	{
		m_b_kz_usun.EnableWindow( FALSE );
	}
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnChLindod() 
{
	ChkBoxState = ( m_chk_lindod.GetCheck() == 1);

	m_spin_lindod.EnableWindow( ChkBoxState );
	m_st_lindod.EnableWindow( ChkBoxState );
	m_st_lindod_ud.EnableWindow( ChkBoxState );

	if( ChkBoxState )
	{
		EnableAdditionalLines( m_spin_lindod.GetPos() );
	}
	else
	{
		EnableAdditionalLines( 0 );
	}
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnBWartdomysl() 
{

	m_ed_CashRegNr.SetWindowText( STR_LBTRXEND1_CASH_REG_NR );
	m_ed_Cashier.SetWindowText( STR_LBTRXEND1_CASHIER );

	// Nr systemowy - jest 
	m_ed_nrsyst.SetWindowText("nr1");
	m_chk_nrsyst.SetCheck( 1 );
	OnChNrsyst();
	
	// Linie dodatkowe - nie ma
	m_ed_linia1.SetWindowText("");
	m_ed_linia2.SetWindowText("");
	m_ed_linia3.SetWindowText("");
	m_chk_lindod.SetCheck( 0 );
	OnChLindod();

	// Formy p�atno�ci - s� (karta:visa, czek:czek)
	m_listctrl_fp.DeleteAllItems();
	FpItems = 0;
	m_b_fp_dodaj.EnableWindow( TRUE );
	m_b_fp_usun.EnableWindow( FALSE );

	m_combox_fpr.SetCurSel( 0 );
	m_ed_fpn.SetWindowText( "visa" );
	m_ed_fpk.SetWindowText( "10" );
	OnFpDodaj();
	m_combox_fpr.SetCurSel( 1 );
	m_ed_fpn.SetWindowText( "czek" );
	m_ed_fpk.SetWindowText( "2" );
	OnFpDodaj();

	m_chk_fp.SetCheck( 1 );
	OnChFp();

	// Kaucje pobrane - nie ma
	m_listctrl_kp.DeleteAllItems();
	KpItems = 0;
	m_b_kp_dodaj.EnableWindow( TRUE );
	m_b_kp_usun.EnableWindow( FALSE );
	m_ed_kpn.SetWindowText( "" );
	m_ed_kpi.SetWindowText( "" );
	m_ed_kpk.SetWindowText( "" );
	m_chk_kp.SetCheck( 0 );
	OnChKp();

	// Kaucje zwr�cone - nie ma
	m_listctrl_kz.DeleteAllItems();
	KzItems = 0;
	m_b_kz_dodaj.EnableWindow( TRUE );
	m_b_kz_usun.EnableWindow( FALSE );
	m_ed_kzn.SetWindowText( "" );
	m_ed_kzi.SetWindowText( "" );
	m_ed_kzk.SetWindowText( "" );
	m_chk_kz.SetCheck( 0 );
	OnChKz();

	// Total
	m_ed_total.SetWindowText("12");

	// Dsp
	m_ed_dsp.SetWindowText("12");

	// Rabat - nie ma
	m_combox_rabat.SetCurSel( 0 );
	m_ed_rabat.SetWindowText("");
	m_chk_rabat.SetCheck( 0 );
	OnChRabat();

	// Wplata - nie ma
	m_ed_wplata.SetWindowText("");
	m_chk_wplata.SetCheck( 0 );
	OnChWplata();

	// Reszta - nie ma
	m_ed_reszta.SetWindowText("");
	m_chk_reszta.SetCheck( 0 );
	OnChReszta();
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnBWyslij() 
{
	static int RetI;

	static char * LinePtr[3];

	static BYTE FormOfPaymentType[LBTRXEND1_MAX_FP_ITEMS];	// Tyle

	static tLBTRXEND1_PARAMS LBTRXEND1params;

	static DWORD i,
		SpinPos,
		FormaPlat;
	
	static CString CashRegNr,
		Cashier,
		SystemNr,
		Line[3],
		Total,
		Dsp,
		Discount,
		Cash,
		Rest,
		TmpStr;

	static tEXTERNAL_NUMERIC TmpExtNum;

	DepositTakenNrStrArr.RemoveAll();
	DepositTakenNrPtrArr.RemoveAll();

	DepositTakenQuantityStrArr.RemoveAll();
	DepositTakenQuantityArr.RemoveAll();
	
	DepositTakenAmountStrArr.RemoveAll();
	DepositTakenAmountArr.RemoveAll();
	
	DepositReturnedNrStrArr.RemoveAll();
	DepositReturnedNrPtrArr.RemoveAll();
	
	DepositReturnedQuantityStrArr.RemoveAll();
	DepositReturnedQuantityArr.RemoveAll();
	
	DepositReturnedAmountStrArr.RemoveAll();
	DepositReturnedAmountArr.RemoveAll();
	
	FormOfPaymentNameStrArr.RemoveAll();
	FormOfPaymentNamePtrArr.RemoveAll();
	
	FormOfPaymentAmountStrArr.RemoveAll();
	FormOfPaymentAmountArr.RemoveAll();

#ifdef _DEBUG
	memset( & LBTRXEND1params, 0, sizeof(LBTRXEND1params) );
#endif // _DEBUG

	// <nr_kasy>
	m_ed_CashRegNr.GetWindowText( CashRegNr );
	LBTRXEND1params.CashRegNr = (char *) LPCTSTR( CashRegNr );
	
	// <kasjer>
	m_ed_Cashier.GetWindowText( Cashier );
	LBTRXEND1params.Cashier = (char *) LPCTSTR( Cashier );

	// <numer_systemowy>
	if( m_chk_nrsyst.GetCheck() == 1)
	{
		m_ed_nrsyst.GetWindowText( SystemNr );
		LBTRXEND1params.SystemNr = (char *) LPCTSTR( SystemNr );

		LBTRXEND1params.Pns = 1;											// Pns
	}
	else
	{
		LBTRXEND1params.Pns = 0;
	}

	// <linia1>...
	if( m_chk_lindod.GetCheck() == 1 )
	{
		SpinPos = m_spin_lindod.GetPos();
		LBTRXEND1params.Pn = (BYTE) SpinPos;					// Pn

		m_ed_linia1.GetWindowText( Line[0] );

		if( SpinPos > 1 )
		{
			m_ed_linia2.GetWindowText( Line[1] );

			if( SpinPos > 2 )
			{
				m_ed_linia3.GetWindowText( Line[2] );
			}
		}

		LinePtr[0] = (char *) LPCTSTR( Line[0] );
		LinePtr[1] = (char *) LPCTSTR( Line[1] );
		LinePtr[2] = (char *) LPCTSTR( Line[2] );

		LBTRXEND1params.Line = LinePtr;
	}
	else
	{
		LBTRXEND1params.Pn = 0;
	}

/*
	<nazwa_form_plat_1>...
	FORM_PLAT_1... - kwoty
	oraz Pfx-y
*/
	if( (m_chk_fp.GetCheck() == 1) && (FpItems > 0) )
	{
		for( i = 0; i < FpItems; i++ )
		{
			FormOfPaymentNameStrArr.Add( m_listctrl_fp.GetItemText(i, 1) );
			FormOfPaymentNamePtrArr.Add( (char *) LPCTSTR(FormOfPaymentNameStrArr[i]) );

			FormOfPaymentAmountStrArr.Add( m_listctrl_fp.GetItemText(i, 2) );	// kwota
			TmpExtNum.ParamValuePtr = (char *) LPCTSTR( FormOfPaymentAmountStrArr[i] );
			TmpExtNum.TypeTranslFuncPtr = NULL;
			FormOfPaymentAmountArr.Add( TmpExtNum );
		
			TmpStr = m_listctrl_fp.GetItemText( i, 0 );		// rodzaj

			if( TmpStr.CompareNoCase("Karta") == 0 )
			{
				FormaPlat = FORM_CARD;
			}
			else
			{
				if( TmpStr.CompareNoCase("Czek") == 0 )
				{
					FormaPlat = FORM_CHEQUE;
				}
				else
				{
					if( TmpStr.CompareNoCase("Bon") == 0 )
					{
						FormaPlat = FORM_COUPON;
					}
					else
					{
						if( TmpStr.CompareNoCase("Kredyt") == 0 )
						{
							FormaPlat = FORM_CREDIT;
						}
						else
						{
							FormaPlat = FORM_OTHER;								// Inna forma p�atno�ci
						}	
					}
				}
			}

			FormOfPaymentType[i] = FormaPlat;
		}

		LBTRXEND1params.Pfn = (BYTE) FpItems;					// Pfn
		LBTRXEND1params.FormOfPaymentName = FormOfPaymentNamePtrArr.GetData();
		LBTRXEND1params.FormOfPaymentAmount = FormOfPaymentAmountArr.GetData();
		LBTRXEND1params.Pfx = FormOfPaymentType;
	}
	else
	{
		LBTRXEND1params.Pfn = 0;
	}


/*
	<numer_kaucji_pobr_1>
	<ilosc_kaucji_pobr_1>
	KAUCJA_POBR1... - kwoty
*/
	if( (m_chk_kp.GetCheck() == 1) && (KpItems > 0) )
	{
		for( i = 0; i < KpItems; i++ )
		{
			// <numer>
			DepositTakenNrStrArr.Add( m_listctrl_kp.GetItemText(i, 0) );
			DepositTakenNrPtrArr.Add( (char *) LPCTSTR(DepositTakenNrStrArr[i]) );

			// <ilosc>
			DepositTakenQuantityStrArr.Add( m_listctrl_kp.GetItemText(i, 1) );
			TmpExtNum.ParamValuePtr = (char *) LPCTSTR(DepositTakenQuantityStrArr[i]);
			TmpExtNum.TypeTranslFuncPtr = NULL;
			DepositTakenQuantityArr.Add( TmpExtNum );

			// kwoty
			DepositTakenAmountStrArr.Add( m_listctrl_kp.GetItemText(i, 2) );
			TmpExtNum.ParamValuePtr = (char *) LPCTSTR( DepositTakenAmountStrArr[i] );
			TmpExtNum.TypeTranslFuncPtr = NULL;
			DepositTakenAmountArr.Add( TmpExtNum );
		}

		LBTRXEND1params.Pkb = (BYTE) KpItems;					// Pkb
		LBTRXEND1params.DepositTakenNr = DepositTakenNrPtrArr.GetData();
		LBTRXEND1params.DepositTakenAmount = DepositTakenAmountArr.GetData();
		LBTRXEND1params.DepositTakenQuantity = DepositTakenQuantityArr.GetData();
	}
	else
	{
		LBTRXEND1params.Pkb = 0;
	}

/*
	<numer_kaucji_zwr_1>
	<ilosc_kaucji_zwr_1>
	KAUCJA_ZWR1... - kwoty
*/
	if( (m_chk_kz.GetCheck() == 1) && (KzItems > 0) )
	{
		for( i = 0; i < KzItems; i++ )
		{
			// <numer>
			DepositReturnedNrStrArr.Add( m_listctrl_kz.GetItemText(i, 0) );
			DepositReturnedNrPtrArr.Add( (char *) LPCTSTR(DepositReturnedNrStrArr[i]) );

			// <ilosc>
			DepositReturnedQuantityStrArr.Add( m_listctrl_kz.GetItemText(i, 1) );
			TmpExtNum.ParamValuePtr = (char *) LPCTSTR( DepositReturnedQuantityStrArr[i] );
			TmpExtNum.TypeTranslFuncPtr = NULL;
			DepositReturnedQuantityArr.Add( TmpExtNum );

			// kwoty		
			DepositReturnedAmountStrArr.Add( m_listctrl_kz.GetItemText(i, 2) );
			TmpExtNum.ParamValuePtr = (char *) LPCTSTR( DepositReturnedAmountStrArr[i] );
			TmpExtNum.TypeTranslFuncPtr = NULL;
			DepositReturnedAmountArr.Add( TmpExtNum );
		}

		LBTRXEND1params.Pkz = (BYTE) KzItems;					// Pkz
		LBTRXEND1params.DepositReturnedNr = DepositReturnedNrPtrArr.GetData();
		LBTRXEND1params.DepositReturnedAmount = DepositReturnedAmountArr.GetData();
		LBTRXEND1params.DepositReturnedQuantity = DepositReturnedQuantityArr.GetData();
	}
	else
	{
		LBTRXEND1params.Pkz = 0;
	}

	// TOTAL
	m_ed_total.GetWindowText( Total );
	LBTRXEND1params.Total.ParamValuePtr = (char *) LPCTSTR( Total );
	LBTRXEND1params.Total.TypeTranslFuncPtr = NULL;

	// DSP
	m_ed_dsp.GetWindowText( Dsp );
	LBTRXEND1params.Dsp.ParamValuePtr = (char *) LPCTSTR( Dsp );
	LBTRXEND1params.Dsp.TypeTranslFuncPtr = NULL;

	// RABAT
	LBTRXEND1params.Discount.TypeTranslFuncPtr = NULL;
	if( m_chk_rabat.GetCheck() == 1 )
	{
		m_combox_rabat.GetWindowText( TmpStr );
		
		if( TmpStr.CompareNoCase("Rab. %") == 0 )
		{
			LBTRXEND1params.Px = LBTRXEND1_DISCNT_PERCNT;			// Px
		}
		else
		{
			if( TmpStr.CompareNoCase("Rab. kw.") == 0 )
			{
				LBTRXEND1params.Px = LBTRXEND1_DISCNT_AMT;
			}
			else
			{
				if( TmpStr.CompareNoCase("Dop�. %") == 0 )
				{
					LBTRXEND1params.Px = LBTRXEND1_SURCH_PERCNT;
				}
				else
				{
					LBTRXEND1params.Px = LBTRXEND1_SURCH_AMT;			// Pozostaje dop�ata kwotowa
				}
			}
		}

		m_ed_rabat.GetWindowText( Discount );
		LBTRXEND1params.Discount.ParamValuePtr = (char *) LPCTSTR( Discount );
	}
	else
	{
		LBTRXEND1params.Px = 0;
	}

	// WPLATA
	LBTRXEND1params.Cash.TypeTranslFuncPtr = NULL;
	if( m_chk_wplata.GetCheck() == 1 )
	{
		LBTRXEND1params.Pg = 1;												// Pg

		m_ed_wplata.GetWindowText( Cash );
		LBTRXEND1params.Cash.ParamValuePtr = (char *) LPCTSTR( Cash );
	}
	else
	{
		LBTRXEND1params.Pg = 0;
	}


	// RESZTA
	LBTRXEND1params.Rest.TypeTranslFuncPtr = NULL;
	if( m_chk_reszta.GetCheck() == 1 )
	{
		LBTRXEND1params.Pr = 1;														// Pr

		m_ed_reszta.GetWindowText( Rest );
		LBTRXEND1params.Rest.ParamValuePtr = (char *) LPCTSTR( Rest );
	}
	else
	{
		LBTRXEND1params.Pr = 0;
	}

/*
	Pozosta�e parametry numeryczne, ewentualnie dorobi� obs�ug� Pdsp !!!
*/

	// Pc
	LBTRXEND1params.Pc = 0;
	
	// Py - skr�cone podsumowanie w��czone !!!
	LBTRXEND1params.Py = 1;
	
	// Pdsp  - zrobi� sprawdzanie znaku !!!
	LBTRXEND1params.Pdsp = 0;

	RetI = theApp.ThSL_LBTRXEND1( & LBTRXEND1params );
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBTRXEND1 !", RetI );
	}
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnBAnuluj() 
{
	static int RetI;
		
	CString CaNr,
			CaName;

	// <nr_kasy>
	m_ed_CashRegNr.GetWindowText( CaNr );
	
	// <kasjer>
	m_ed_Cashier.GetWindowText( CaName );

	RetI = theApp.ThSL_LBTREXITCAN( (char *) LPCTSTR(CaNr), (char *) LPCTSTR(CaName) );
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBTREXITCAN !", RetI );
	}
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnBKoniec() 
{
	DestroyWindow();
}

/*----------------------------------------------------------------------------*/

BOOL CLBTRXEND1dialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	int Width;

	DepositTakenNrStrArr.SetSize(10);
	DepositTakenNrPtrArr.SetSize(10);

	DepositTakenQuantityStrArr.SetSize(10);
	DepositTakenQuantityArr.SetSize(10);
	
	DepositTakenAmountStrArr.SetSize(10);
	DepositTakenAmountArr.SetSize(10);
	
	DepositReturnedNrStrArr.SetSize(10);
	DepositReturnedNrPtrArr.SetSize(10);
	
	DepositReturnedQuantityStrArr.SetSize(10);
	DepositReturnedQuantityArr.SetSize(10);
	
	DepositReturnedAmountStrArr.SetSize(10);
	DepositReturnedAmountArr.SetSize(10);
	
	FormOfPaymentNameStrArr.SetSize(10);
	FormOfPaymentNamePtrArr.SetSize(10);
	
	FormOfPaymentAmountStrArr.SetSize(10);
	FormOfPaymentAmountArr.SetSize(10);
	
	FpItems = 0;	// Ile pozycji Form P�atno�ci
	KpItems = 0;	// Ile pozycji Kaucji Pobranej
	KzItems = 0;	// Ile pozycji Kaucji Zwr�conej

	m_spin_lindod.SetRange( 1, LBTRXEND1_MAX_ADD_LINES );
	m_spin_lindod.SetPos( LBTRXEND1_MAX_ADD_LINES );
		
	Width = m_listctrl_fp.GetStringWidth( "_Rodzaj_" );
	m_listctrl_fp.InsertColumn( 0, "Rodzaj", LVCFMT_RIGHT, Width, 1 );

	Width = m_listctrl_fp.GetStringWidth( "____Nazwa____" );
	m_listctrl_fp.InsertColumn( 1, "Nazwa", LVCFMT_RIGHT, Width, 2 );

	Width = m_listctrl_fp.GetStringWidth( "__Kwota__" );
	m_listctrl_fp.InsertColumn( 2, "Kwota", LVCFMT_RIGHT , Width, 3 );


	Width = m_listctrl_kp.GetStringWidth( "__Numer__" );
	m_listctrl_kp.InsertColumn( 0, "Numer", LVCFMT_RIGHT, Width, 1 );

	Width = m_listctrl_kp.GetStringWidth( "__Ilosc__" );
	m_listctrl_kp.InsertColumn( 1, "Ilosc", LVCFMT_RIGHT, Width, 2 );

	Width = m_listctrl_kp.GetStringWidth( "__Kwota__" );
	m_listctrl_kp.InsertColumn( 2, "Kwota", LVCFMT_RIGHT , Width, 3 );


	Width = m_listctrl_kz.GetStringWidth( "__Numer__" );
	m_listctrl_kz.InsertColumn( 0, "Numer", LVCFMT_RIGHT, Width, 1 );

	Width = m_listctrl_kz.GetStringWidth( "__Ilosc__" );
	m_listctrl_kz.InsertColumn( 1, "Ilosc", LVCFMT_RIGHT, Width, 2 );

	Width = m_listctrl_kz.GetStringWidth( "__Kwota__" );
	m_listctrl_kz.InsertColumn( 2, "Kwota", LVCFMT_RIGHT , Width, 3 );

	OnBWartdomysl();
	
	return TRUE;
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnDeltaposSpLindodUd(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;

	static int NrOfLines;

	NrOfLines = pNMUpDown->iPos + pNMUpDown->iDelta;
	if( NrOfLines > 0 )
	{
		EnableAdditionalLines( NrOfLines );
	}

	*pResult = 0;
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::EnableAdditionalLines( int aNrOfLines )
{

	if( aNrOfLines  <= LBTRXEND1_MAX_ADD_LINES )
	{
		switch( aNrOfLines )
		{
			default:	
				// NO BREAK HERE

			case 1:
				m_ed_linia1.EnableWindow( FALSE );
				// NO BREAK HERE

			case 2:
				m_ed_linia2.EnableWindow( FALSE );
				// NO BREAK HERE

			case 3:
				m_ed_linia3.EnableWindow( FALSE );
				break;
		}
	}

	if( (aNrOfLines > 0) && (aNrOfLines <= LBTRXEND1_MAX_ADD_LINES) )
	{
		switch( aNrOfLines )
		{
			default:	
				// NO BREAK HERE

			case 3:
				m_ed_linia3.EnableWindow( TRUE );
				// NO BREAK HERE

			case 2:
				m_ed_linia2.EnableWindow( TRUE );
				// NO BREAK HERE

			case 1:
				m_ed_linia1.EnableWindow( TRUE );
				break;
		}
	}
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnFpDodaj() 
{
	static CString TempStr;


	// rodzaj
	m_combox_fpr.GetWindowText( TempStr );
	m_listctrl_fp.InsertItem( FpItems, LPCTSTR(TempStr) );

	// nazwa
	m_ed_fpn.GetWindowText( TempStr );
	m_listctrl_fp.SetItemText( FpItems, 1 , LPCTSTR(TempStr) );

	// kwota
	m_ed_fpk.GetWindowText( TempStr );
	m_listctrl_fp.SetItemText( FpItems, 2 , LPCTSTR(TempStr) );

	FpItems++;

	if( FpItems == LBTRXEND1_MAX_FP_ITEMS )
	{
		m_b_fp_dodaj.EnableWindow( FALSE );
	}

	m_b_fp_usun.EnableWindow( TRUE );

}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnFpUsun() 
{
	m_b_fp_dodaj.EnableWindow( TRUE );

	m_listctrl_fp.DeleteItem(--FpItems);

	if( !FpItems )
	{
		m_b_fp_usun.EnableWindow( FALSE );
	}
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnKpDodaj() 
{
	static CString TempStr;

	// numer
	m_ed_kpn.GetWindowText( TempStr );
	m_listctrl_kp.InsertItem( KpItems, LPCTSTR(TempStr) );

	// ilo��
	m_ed_kpi.GetWindowText( TempStr );
	m_listctrl_kp.SetItemText( KpItems, 1, LPCTSTR(TempStr) );

	// kwota
	m_ed_kpk.GetWindowText( TempStr );
	m_listctrl_kp.SetItemText( KpItems, 2, LPCTSTR(TempStr) );

	KpItems++;

	m_b_kp_usun.EnableWindow( TRUE );
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnKpUsun() 
{
	m_listctrl_kp.DeleteItem(--KpItems);

	if( !KpItems )
	{
		m_b_kp_usun.EnableWindow( FALSE );
	}
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnKzDodaj() 
{
	static CString TempStr;

	// numer
	m_ed_kzn.GetWindowText( TempStr );
	m_listctrl_kz.InsertItem( KzItems, LPCTSTR(TempStr) );

	// ilo��
	m_ed_kzi.GetWindowText( TempStr );
	m_listctrl_kz.SetItemText( KzItems, 1, LPCTSTR(TempStr) );

	// kwota
	m_ed_kzk.GetWindowText( TempStr );
	m_listctrl_kz.SetItemText( KzItems, 2 , LPCTSTR(TempStr) );

	KzItems++;

	m_b_kz_usun.EnableWindow( TRUE );
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnKzUsun() 
{
	m_listctrl_kz.DeleteItem(--KzItems);

	if( !KzItems )
	{
		m_b_kz_usun.EnableWindow( FALSE );
	}
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::PostNcDestroy() 
{
	theApp.p_CLBTRXEND1dialog = NULL;
	delete this;
	
	CDialog::PostNcDestroy();
}

/*----------------------------------------------------------------------------*/

void CLBTRXEND1dialog::OnCancel() 
{
	DestroyWindow();
}
