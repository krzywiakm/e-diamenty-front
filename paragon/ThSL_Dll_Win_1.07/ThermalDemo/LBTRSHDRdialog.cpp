// LBTRSHDRdialog.cpp : implementation file
//

#include "stdafx.h"
#include "ThermalDemo.h"
#include "LBTRSHDRdialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CThermalDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CLBTRSHDRdialog dialog


CLBTRSHDRdialog::CLBTRSHDRdialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLBTRSHDRdialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLBTRSHDRdialog)
	//}}AFX_DATA_INIT
}


void CLBTRSHDRdialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLBTRSHDRdialog)
	DDX_Control(pDX, IDCANCEL, m_bu_Cancel);
	DDX_Control(pDX, IDC_SP_POZYCJE_UD, m_sp_NrOfLines);
	DDX_Control(pDX, IDC_ST_POZYCJE, m_st_NrOfLines);
	DDX_Control(pDX, IDC_ED_POZYCJE_UD, m_ed_NrOfLines);
	DDX_Control(pDX, IDC_CH_ONLINE, m_ch_Online);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLBTRSHDRdialog, CDialog)
	//{{AFX_MSG_MAP(CLBTRSHDRdialog)
	ON_BN_CLICKED(IDC_B_WYSLIJ, OnBWyslij)
	ON_BN_CLICKED(IDC_B_KONIEC, OnBKoniec)
	ON_BN_CLICKED(IDC_CH_ONLINE, OnChOnline)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLBTRSHDRdialog message handlers

/*----------------------------------------------------------------------------*/

void CLBTRSHDRdialog::OnBWyslij() 
{
	static int RetI;
	static DWORD NrOfLines;

	static CString TmpStr;

	// Jest tylko jeden parametr numeryczny
	if( m_ch_Online.GetCheck() == 1 )
	{
		NrOfLines = 0;
	}
	else
	{
		m_ed_NrOfLines.GetWindowText( TmpStr );

		if( sscanf((LPCTSTR) TmpStr, "%d", &NrOfLines) != 1 )
		{
			WarningMessageBox( WARNMSG_LBTRSHDR_NROFLINES );
			return;
		}
	}

	RetI = theApp.ThSL_LBTRSHDR( (BYTE) NrOfLines );
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBTRSHDR !", RetI );
	}
}

/*----------------------------------------------------------------------------*/

void CLBTRSHDRdialog::OnBKoniec() 
{
	DestroyWindow();
}

/*----------------------------------------------------------------------------*/

BOOL CLBTRSHDRdialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_bu_Cancel.EnableWindow( FALSE );
	m_bu_Cancel.ShowWindow( FALSE );

	m_sp_NrOfLines.SetRange( 1, MAX_PAR_LINES );
	m_sp_NrOfLines.SetPos( 1 );

	// Domy�lnie: ON-LINE
	m_ch_Online.SetCheck( 1 );
	OnChOnline();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/*----------------------------------------------------------------------------*/

void CLBTRSHDRdialog::OnChOnline() 
{
	ChkBoxState = ( m_ch_Online.GetCheck() != 1);

	m_sp_NrOfLines.EnableWindow( ChkBoxState );
	m_ed_NrOfLines.EnableWindow( ChkBoxState );
	m_st_NrOfLines.EnableWindow( ChkBoxState );
}

/*----------------------------------------------------------------------------*/

void CLBTRSHDRdialog::PostNcDestroy() 
{
	theApp.p_CLBTRSHDRdialog = NULL;
	delete this;

	CDialog::PostNcDestroy();
}

/*----------------------------------------------------------------------------*/

void CLBTRSHDRdialog::OnCancel() 
{
	DestroyWindow();
}
