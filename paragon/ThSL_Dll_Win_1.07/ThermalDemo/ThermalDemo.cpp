// ThermalDemo.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ThermalDemo.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define PRG_VERSION "1.01"


/////////////////////////////////////////////////////////////////////////////
// CThermalDemoApp

BEGIN_MESSAGE_MAP(CThermalDemoApp, CWinApp)
	//{{AFX_MSG_MAP(CThermalDemoApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CThermalDemoApp construction

CThermalDemoApp::CThermalDemoApp()
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CThermalDemoApp object

CThermalDemoApp theApp;

char PrgVersion[] = "$ver: "PRG_VERSION;	/* ci�g identyfikuj�cy wersj� programu */

/////////////////////////////////////////////////////////////////////////////
// CThermalDemoApp initialization

BOOL CThermalDemoApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	hThermalServiceLibrary =				NULL;
	hMailSlot =											NULL;

	ThSL_Port_Open =								NULL;
	ThSL_Port_SetBaudRate =					NULL;
	ThSL_Port_Close =								NULL;
	ThSL_ErrorMessage =							NULL;
	ThSL_GetDeviceErrorCode =				NULL;
	ThSL_Send_ACK =									NULL;
	ThSL_Send_NACK =								NULL;

	ThSL_GetStatus_CashRegister =		NULL;
	ThSL_GetStatus_Printer =				NULL;

	ThSL_LBSETCK =									NULL;
	ThSL_LBDSP =										NULL;
	ThSL_LBSETPTU =									NULL;
	ThSL_LBSETHDR =									NULL;
	ThSL_LBFEED =										NULL;
	ThSL_LBSERM =										NULL;
	ThSL_LBTRSHDR =									NULL;
	ThSL_LBTRSLN =									NULL;
	ThSL_O_LBTRSLN =								NULL;
	ThSL_LBDEP_P =									NULL;
	ThSL_LBDEPSTR_P =								NULL;
	ThSL_LBDEP_M =									NULL;
	ThSL_LBDEPSTR_M =								NULL;
	ThSL_LBTREXITCAN =							NULL;
	ThSL_O_LBTREXIT =								NULL;
	ThSL_O_LBTRXEND =								NULL;
	ThSL_LBTRFORMPLAT =							NULL;
	ThSL_LBTRXEND1 =								NULL;

	ThSL_LBCSHREP2 =								NULL;
	ThSL_LBTRSCARD =								NULL;
	ThSL_LBSTOCARD =								NULL;
	ThSL_LBINCCSH =									NULL;
	ThSL_LBDECCSH =									NULL;
	ThSL_LBCSHSTS =									NULL;
	ThSL_LBCSHREP =									NULL;
	ThSL_LBLOGIN =									NULL;
	ThSL_LBLOGOUT =									NULL;
	ThSL_LBFSKREP_D =								NULL;
	ThSL_LBFSKREP_R =								NULL;
	ThSL_LBDAYREP =									NULL;
	
	ThSL_LBDBREPRS =								NULL;
	ThSL_LBSENDCK =									NULL;
	ThSL_LBFSTRQ =									NULL;
	ThSL_O_LBFSTRQ =								NULL;
	ThSL_LBERNRQ =									NULL;
	ThSL_LBIDRQ =										NULL;
	ThSL_LBSNDMD =									NULL;
	ThSL_LBCASREP =									NULL;

	ThSL_LBFSTRQ24 =								NULL;
	ThSL_LBFSTRQ25 =								NULL;
	ThSL_LBFSTRQ26 =								NULL;
	ThSL_LBFSTRQ27 =								NULL;
	

	p_CLBTRSHDRdialog =							NULL;
	p_CLBTRSLNdialog =							NULL;
	p_CLBTRXENDdialog =							NULL;
	p_CLBTRXEND1dialog =						NULL;
	p_CLBSETPTUdialog =							NULL;
	p_CLBSETHDRdialog =							NULL;
	p_CLBFSTRQdialog =							NULL;
	p_CLBFSTRQrepDialog =						NULL;
	p_CLBDEPdialog =								NULL;

	p_CSnooperDialog =							NULL;
	p_CPortSettingsDialog =					NULL;

	PortName = "";
	PortNameNdx = -1;
	BaudRateNdx = -1;
	BaudRate = -1;

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	SetRegistryKey(_T(REGISTRY_KEY_NAME));

	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object.

	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;

	// create and load the frame with its resources

	pFrame->LoadFrame(IDR_MAINFRAME,
		WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL,
		NULL);

	// The one and only window has been initialized, so show and update it.
	pFrame->ShowWindow(SW_SHOW);
	pFrame->UpdateWindow();

	if( OpenLibrary() != SOK )
	{
		return FALSE;
	}

	hMailSlot = CreateMailslot( SNOOP_MAILSLOT_NAME, 0, MAILSLOT_WAIT_FOREVER, NULL );
	if( hMailSlot == INVALID_HANDLE_VALUE )
	{
		hMailSlot = NULL;
		InfoMessageBox( INFOMSG_NO_MAILSLOT );
	}

	ThSL_Port_LoggingStart( "TxRx.log" );	// rozpoczynamy logowanie - do mailslota i do 
																				// pliku

	DWORD TmpPortNameNdx,
		TmpBaudRateNdx,
		TmpBaudRate;

	CString TmpPortName;

/*
	Indeksy w ComboBox-ach liczone s� od 0, natomiast GetProfileInt zwraca 0 je�li
	w profilu dany ci�g nie by� liczb�.
	-> Dlatego warto�ci indeks�w przed zapisem s� inkrementowane.
*/

	TmpPortNameNdx = GetProfileInt( K_PORT_SETTINGS, K_PORT_SETTINGS_NAME_NDX, -1 );

	if( (TmpPortNameNdx != -1) && (TmpPortNameNdx != 0) )
	{
		TmpPortName = GetProfileString( K_PORT_SETTINGS, K_PORT_SETTINGS_NAME,	"" );
		
		if( !TmpPortName.IsEmpty() )
		{
			TmpBaudRateNdx = GetProfileInt( K_PORT_SETTINGS, K_PORT_SETTINGS_BAUD_NDX, -1 );

			if( (TmpBaudRateNdx != -1) && (TmpBaudRateNdx != 0) )
			{
				TmpBaudRate = GetProfileInt( K_PORT_SETTINGS, K_PORT_SETTINGS_BAUD,	-1 );

				if( (TmpBaudRate != -1) && (TmpBaudRate != 0) )
				{
					PortNameNdx = TmpPortNameNdx - 1;
					PortName = TmpPortName;

					BaudRateNdx = TmpBaudRateNdx - 1;
					BaudRate = TmpBaudRate;
					
				}
			}
		}
	}

	int RetI;
	tCASH_REGISTER_STATUS CashRegStatus;

	while( TRUE )
	{
		ThSL_Port_Close();

		while( (BaudRateNdx == -1) || (PortNameNdx == -1) )
		{
			static CPortSettingsDialog PortSettingsDlg;

			if( PortSettingsDlg.DoModal() == IDCANCEL )
			{
				return FALSE;
			}
		}

		/* Otwieramy port */
		RetI = ThSL_Port_Open( (char *) LPCTSTR(PortName), BaudRate );
		if( RetI != SOK )
		{
			BaudRateNdx = -1;
			PortNameNdx = -1;

			ErrorMessageBox( ERRMSG_PORT_OPEN );

			continue;
		}
		else
		{
			RetI = ThSL_GetStatus_CashRegister( & CashRegStatus );
			if( RetI != SOK )
			{
				BaudRateNdx = -1;
				PortNameNdx = -1;

				theApp.ThSL_ErrorMessage( ERRMSG_GET_STAT_CASH_REG, RetI );
				continue;
			}

/*
			Wysy�amy LBSERM z parametrem 3 -> automatyczne odsy�anie kodu b��du po wykonaniu rozkazu.
			Druga cz�� warunku w 'if' dotyczy sytuacji gdy aktywna jest transakcja - wtedy LBSERM
			zwr�ci b��d.
*/
			RetI = ThSL_LBSERM( 3 );
			if( (RetI != SOK) 
				&& (RetI != ERR_RETURN_CODE) && (!CashRegStatus.PAR) )
			{
				BaudRateNdx = -1;
				PortNameNdx = -1;

				theApp.ThSL_ErrorMessage( "LBSERM !", RetI );
				continue;
			}
			else
			{
				break;
			}
		}
	}

/*
	Rozpoznanie rodzaju urz�dzenia - po tym, jaka jest odpowied� na LBFSTRQ
*/
	tLBFSTRS1_RESPONSE LBFSTRS1_Response;
	tO_LBFSTRS_RESPONSE O_LBFSTRS_Response;
	BYTE NrOfVatRates;

/*
	Nowa homologacja, Ps jest r�wne 23.
	W starej homologacji Ps jest ignorowany - drukarka ode�le informacje kasowe,
	ale w formie innej od tej, na kt�r� czekamy).
*/
	RetI = ThSL_LBFSTRQ( & LBFSTRS1_Response );		/* nowa homologacja ? */
	if( RetI == SOK )
	{
		ThermalVersionNew = TRUE;
	}
	else																					/* chyba nie - to sprawdzimy czy */
	{																							/* jest to stara homologacja */
		RetI = ThSL_O_LBFSTRQ( & NrOfVatRates, & O_LBFSTRS_Response );
		if( RetI == SOK )
		{
			ThermalVersionNew = FALSE;
		}
		else
		{
			ErrorMessageBox( ERRMSG_UNRECOGNIZED_DEVICE_VERSION );
			return FALSE;
		}
	}

	pFrame->RemoveExcessMenus();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CThermalDemoApp message handlers





/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CThermalDemoApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CThermalDemoApp message handlers

/*----------------------------------------------------------------------------*/

int CThermalDemoApp::ExitInstance() 
{
	CloseLibrary();									/* Nie sprawdzam zwr�conego kodu !!! */

	if( hMailSlot )
	{
		CloseHandle( hMailSlot );
	}

	return CWinApp::ExitInstance();
}

/*----------------------------------------------------------------------------*/

void CThermalDemoApp::SaveSettings()
{
	if( (PortNameNdx != -1) && (BaudRateNdx != -1) )
	{
		WriteProfileInt( K_PORT_SETTINGS, K_PORT_SETTINGS_NAME_NDX, PortNameNdx + 1 );
		WriteProfileString( K_PORT_SETTINGS, K_PORT_SETTINGS_NAME, PortName );
		
		WriteProfileInt( K_PORT_SETTINGS, K_PORT_SETTINGS_BAUD_NDX, BaudRateNdx + 1 );
		WriteProfileInt( K_PORT_SETTINGS, K_PORT_SETTINGS_BAUD, BaudRate );
	}
}

/*----------------------------------------------------------------------------*/

int CThermalDemoApp::OpenLibrary()
{
	DWORD i, NrOfImports;
	CString Name;

/*
	Wska�nik do funkcji - aby nie okre�la� wprost szeroko�ci adresu (32 bity ?)
*/
	typedef void ( *tFUN ) ();

	tFUN Addr;

	typedef struct
	{
		char *	Name;
		tFUN *	Address;
	} tDLL_IMPORT;

	tDLL_IMPORT Dll_Import_Table[] =
	{
		{ "Port_Open",					(tFUN *) (& ThSL_Port_Open)						},
		{ "Port_SetBaudRate",		(tFUN *) (& ThSL_Port_SetBaudRate)		},
		{ "Port_Close",					(tFUN *) (& ThSL_Port_Close)					},
		{ "Port_LoggingStart",	(tFUN *) (& ThSL_Port_LoggingStart)		},
		{ "Port_LoggingStop",		(tFUN *) (& ThSL_Port_LoggingStop)		},

		{ "ErrorMessage",				(tFUN *) (& ThSL_ErrorMessage)				},
		{ "GetDeviceError",			(tFUN *) (& ThSL_GetDeviceErrorCode)	},
		{ "Send_ACK",						(tFUN *) (& ThSL_Send_ACK)						},
		{ "Send_NACK",					(tFUN *) (& ThSL_Send_NACK)						},
		// Send_CAN nie jest u�ywane przez program Demo...

		{ "GetStatus_CashRegister",	(tFUN *) (& ThSL_GetStatus_CashRegister)	},
		{ "GetStatus_Printer",			(tFUN *) (& ThSL_GetStatus_Printer)				},

		{ "LBSETCK",			(tFUN *) (& ThSL_LBSETCK)				},
		{ "LBDSP",				(tFUN *) (& ThSL_LBDSP)					},
		{ "LBSETPTU",			(tFUN *) (& ThSL_LBSETPTU)			},
		{ "LBSETHDR",			(tFUN *) (& ThSL_LBSETHDR)			},
		{ "LBFEED",				(tFUN *) (& ThSL_LBFEED)				},
		{ "LBSERM",				(tFUN *) (& ThSL_LBSERM)				},
		{ "LBTRSHDR",			(tFUN *) (& ThSL_LBTRSHDR)			},
		{ "LBTRSLN",			(tFUN *) (& ThSL_LBTRSLN)				},
		{ "O_LBTRSLN",		(tFUN *) (& ThSL_O_LBTRSLN)			},
		{ "LBDEP_P",			(tFUN *) (& ThSL_LBDEP_P)				},
		{ "LBDEPSTR_P",		(tFUN *) (& ThSL_LBDEPSTR_P)		},
		{ "LBDEP_M",			(tFUN *) (& ThSL_LBDEP_M)				},
		{ "LBDEPSTR_M",		(tFUN *) (& ThSL_LBDEPSTR_M)		},
		{ "LBTREXITCAN",	(tFUN *) (& ThSL_LBTREXITCAN)		},
		{ "O_LBTREXIT",		(tFUN *) (& ThSL_O_LBTREXIT)		},
		{ "O_LBTRXEND",		(tFUN *) (& ThSL_O_LBTRXEND)		},
		{ "LBTRFORMPLAT",	(tFUN *) (& ThSL_LBTRFORMPLAT)	},
		{ "LBTRXEND1",		(tFUN *) (& ThSL_LBTRXEND1)			},

		{ "LBCSHREP2",		(tFUN *) (& ThSL_LBCSHREP2)			},
		{ "LBTRSCARD",		(tFUN *) (& ThSL_LBTRSCARD)			},
		{ "LBSTOCARD",		(tFUN *) (& ThSL_LBSTOCARD)			},
		{ "LBINCCSH",			(tFUN *) (& ThSL_LBINCCSH)			},
		{ "LBDECCSH",			(tFUN *) (& ThSL_LBDECCSH)			},
		{ "LBCSHSTS",			(tFUN *) (& ThSL_LBCSHSTS)			},
		{ "LBCSHREP",			(tFUN *) (& ThSL_LBCSHREP)			},
		{ "LBLOGIN",			(tFUN *) (& ThSL_LBLOGIN)				},
		{ "LBLOGOUT",			(tFUN *) (& ThSL_LBLOGOUT)			},
		{ "LBFSKREP_D",		(tFUN *) (& ThSL_LBFSKREP_D)		},
		{ "LBFSKREP_R",		(tFUN *) (& ThSL_LBFSKREP_R)		},
		{ "LBDAYREP",			(tFUN *) (& ThSL_LBDAYREP)			},

		{ "LBDBREPRS",		(tFUN *) (& ThSL_LBDBREPRS)			},
		{ "LBSENDCK",			(tFUN *) (& ThSL_LBSENDCK)			},
		{ "LBFSTRQ",			(tFUN *) (& ThSL_LBFSTRQ)				},
		{ "O_LBFSTRQ",		(tFUN *) (& ThSL_O_LBFSTRQ)			},
		{ "LBERNRQ",			(tFUN *) (& ThSL_LBERNRQ)				},
		{ "LBIDRQ",				(tFUN *) (& ThSL_LBIDRQ)				},
		{ "LBSNDMD",			(tFUN *) (& ThSL_LBSNDMD)	 			},
		{ "LBCASREP",			(tFUN *) (& ThSL_LBCASREP)			},

		{ "LBFSTRQ24",		(tFUN *) (& ThSL_LBFSTRQ24)			},
		{ "LBFSTRQ25",		(tFUN *) (& ThSL_LBFSTRQ25)			},
		{ "LBFSTRQ26",		(tFUN *) (& ThSL_LBFSTRQ26)			},
		{ "LBFSTRQ27",		(tFUN *) (& ThSL_LBFSTRQ27)			},

	};

/*
	�adujemy swojego DLL-a i wyci�gamy adresy funkcji "
*/

	if( !(hThermalServiceLibrary = 
		LoadLibrary("ThermalServiceLibrary.dll")) )
	{
		ErrorMessageBox( ERRMSG_DLL_NOT_LOADED );
		return SERROR;
	}

	NrOfImports = sizeof( Dll_Import_Table ) / sizeof( tDLL_IMPORT );

	for( i = 0; i < NrOfImports; i++ )
	{
		Name = Dll_Import_Table[i].Name;
		Addr = (tFUN) GetProcAddress(hThermalServiceLibrary, Name);

		if( Addr )
		{
			*( Dll_Import_Table[i].Address) = Addr;
		}
		else
		{
			ErrorMessageBox( (char *) LPCTSTR(ERRMSG_FUNC_NOT_FOUND + Name) );
			return SERROR;
		}
	}


	return SOK;
}

/*----------------------------------------------------------------------------*/

int CThermalDemoApp::CloseLibrary()
{
	if( !FreeLibrary( hThermalServiceLibrary) )
	{
		ErrorMessageBox( ERRMSG_FREE_LIBRARY );
		return SERROR;
	}
	
	return SOK;
}
