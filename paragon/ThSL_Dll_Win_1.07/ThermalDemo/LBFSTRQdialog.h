#if !defined(AFX_LBFSTRQDIALOG_H__27BF8FCC_D38A_41E0_9CC7_B4D5366FBFB2__INCLUDED_)
#define AFX_LBFSTRQDIALOG_H__27BF8FCC_D38A_41E0_9CC7_B4D5366FBFB2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LBFSTRQdialog.h : header file
//

#include "GlobalStuff.h"

/////////////////////////////////////////////////////////////////////////////
// CLBFSTRQdialog dialog

class CLBFSTRQdialog : public CDialog
{
// Construction
public:
	CLBFSTRQdialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLBFSTRQdialog)
	enum { IDD = IDD_LBFSTRQ };
	CDateTimeCtrl	m_dt_Time;
	CEdit	m_ed_Number;
	CButton	m_ra_Number;
	CButton	m_ra_Date;
	CDateTimeCtrl	m_dt_Date;
	CTreeCtrl	m_tr_FMem_Records;
	CButton	m_bu_Cancel;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLBFSTRQdialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	void FirstRecordSelection( BOOL aByDate ) ;

	// Generated message map functions
	//{{AFX_MSG(CLBFSTRQdialog)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnRaDate();
	afx_msg void OnRaNumber();
	afx_msg void OnBGetRecords();
	afx_msg void OnBKoniec();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LBFSTRQDIALOG_H__27BF8FCC_D38A_41E0_9CC7_B4D5366FBFB2__INCLUDED_)
