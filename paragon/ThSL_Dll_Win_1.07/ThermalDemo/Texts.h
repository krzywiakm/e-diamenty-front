#ifndef _TEXTS
#define _TEXTS

#include "..\\CommonTexts.h"

#define REGISTRY_KEY_NAME "Posnet - ThermalDemo"

#ifdef LANGUAGE_ENGLISH

#define WARNMSG_LBTRSHDR_NROFLINES "Number of lines should be from 1-80 range"

#define MSG_ALL_WILL_BE_REMOVED		"All selected items and forms of payment\nwill be now removed..."

#define WARNMSG_NEGATIVE_SALE			"Negative amount ? Check your receipt ..."
#define WARNMSG_PAYMENT_LT_SALE		"Amount of payment less than amount of sale..."

#define ERRMSG_PARSING_DATABASE		"An error occured while parsing the database file\n";



#elif defined LANGUAGE_POLISH

#define WARNMSG_LBTRSHDR_NROFLINES "Numery linii powinny by� z zakresu 1-80"

#define WARNMSG_LBFSTRQ25_DATE		"Data powinna by� z zakresu <1950,2049>"

#define INFOMSG_NO_MAILSLOT				"Nie uda�o si� utworzy� Mailslot-a\nNie b�dzie mo�liwy podgl�d sekwencji"
#define INFOMSG_NO_RECORDS				"W pami�ci fiskalnej nie ma �adnych rekord�w.\nRaport nie b�dzie drukowany"

/*
#define MSG_ALL_WILL_BE_REMOVED		"All selected items and forms of payment\nwill be now removed..."

#define WARNMSG_NEGATIVE_SALE			"Negative amount ? Check your receipt ..."
#define WARNMSG_PAYMENT_LT_SALE		"Amount of payment less than amount of sale..."

#define ERRMSG_PARSING_DATABASE		"An error occured while parsing the database file\n";
*/

#define STR_LBFSTRQ27_DAILY_REPORT			"Raport dobowy"
#define STR_LBFSTRQ27_VAT_CHANGE				"Zmiana stawek VAT"
#define STR_LBFSTRQ27_RAM_CLEAR					"Zerowanie pami�ci RAM"
#define STR_LBFSTRQ27_SALE_AFTER_CLEAR	"Sprzeda� po zerowaniu RAM"

#define STR_LBTRXEND1_CASH_REG_NR			"Kasa 1"
#define STR_LBTRXEND1_CASHIER					"Kasjer 1"

#define STR_LBTRXEND_LINE1	"Linia 1"
#define STR_LBTRXEND_LINE2	"Linia 2"
#define STR_LBTRXEND_LINE3	"Linia 3"
#define STR_LBTRXEND_LINE4	"Linia 4"
#define STR_LBTRXEND_LINE5	"Linia 5"
#define STR_LBTRXEND_CARD_NAME			"Karta"
#define STR_LBTRXEND_CARD_AMOUNT		"1.00"
#define STR_LBTRXEND_CHEQUE_NAME		"Czek"
#define STR_LBTRXEND_CHEQUE_AMOUNT	"1.00"
#define STR_LBTRXEND_COUPON_NAME		"Bon"
#define STR_LBTRXEND_COUPON_AMOUNT	"1.00"
#define STR_LBTRXEND_DEP_TAKEN		"5.00"
#define STR_LBTRXEND_DEP_RETURNED	"5.00"
#define STR_LBTRXEND_TOTAL				"12.00"
#define STR_LBTRXEND_DISCNT_VAL		"5.5"
#define STR_LBTRXEND_CASH					"1.00"
#define STR_LBTRXEND_REST					"1.00"

#define STR_LBCSHREP2_FOP_NAME_1			"Karta1"
#define STR_LBCSHREP2_FOP_AMOUNT_1		"3.14"
#define STR_LBCSHREP2_FOP_NAME_2			"Czek1"
#define STR_LBCSHREP2_FOP_AMOUNT_2		"6.67"
#define STR_LBCSHREP2_DEP_T_NAME_1		"Kaucja pobr. 1"
#define STR_LBCSHREP2_DEP_T_AMOUNT_1	"1"
#define STR_LBCSHREP2_DEP_T_NAME_2		"Kaucja pobr. 2"
#define STR_LBCSHREP2_DEP_T_AMOUNT_2	"2"
#define STR_LBCSHREP2_DEP_T_NAME_3		"Kaucja pobr. 3"
#define STR_LBCSHREP2_DEP_T_AMOUNT_3	"3"
#define STR_LBCSHREP2_DEP_T_TOTAL			"6"
#define STR_LBCSHREP2_DEP_R_NAME_1		"Kaucja zwr. 1"
#define STR_LBCSHREP2_DEP_R_AMOUNT_1	"2"
#define STR_LBCSHREP2_DEP_R_NAME_2		"Kaucja zwr. 2"
#define STR_LBCSHREP2_DEP_R_AMOUNT_2	"3"
#define STR_LBCSHREP2_DEP_R_TOTAL			"5"
#define STR_LBCSHREP2_CASH						"12.3"
#define STR_LBCSHREP2_SHIFT						"Zmiana 1"
#define STR_LBCSHREP2_CASH_REG_NR			STR_LBTRXEND1_CASH_REG_NR
#define STR_LBCSHREP2_CASHIER					STR_LBTRXEND1_CASHIER
#define STR_LBCSHREP2_BEGINNING				"02:03:04 08:01"
#define STR_LBCSHREP2_END							"02:03:04 17:00"
#define STR_LBCSHREP2_TAKINGS					"567.89"
#define STR_LBCSHREP2_EXPENDITURES		"234.56"
#define STR_LBCSHREP2_SALES_CASH			"345.67"
#define STR_LBCSHREP2_PAYMENT_IN			"543.21"
#define STR_LBCSHREP2_PAYMENT_OUT			"123.45"
#define VAL_LBCSHREP2_RECEIPTS							777
#define VAL_LBCSHREP2_CANCELLED_RECEPIPTS		13
#define VAL_LBCSHREP2_CANCELLED_ITEMS				14


#define STR_LBCSHREP_SHIFT						STR_LBCSHREP2_SHIFT
#define STR_LBCSHREP_CASH_REG_NR			STR_LBTRXEND1_CASH_REG_NR
#define STR_LBCSHREP_CASHIER					STR_LBTRXEND1_CASHIER


#define STR_COUNTERS		"Liczniki"
#define STR_TOTALIZERS	"Totalizery"


#define STR_RECEIPT_CNT		"Paragony: "
#define STR_BCHNG_CNT		"Zmiany bazy: "
#define STR_CANC_CNT		"Storno: "

#define STR_REASON			"Przyczyna: "
#define STR_NUMBER			"Numer: "

#define STR_A						"A: "
#define STR_B						"B: "
#define STR_C						"C: "
#define STR_D						"D: "
#define STR_E						"E: "
#define STR_F						"F: "
#define STR_G						"G: "


#endif // LANGUAGE...


#endif // _TEXTS