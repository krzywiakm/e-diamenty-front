#if !defined(AFX_PORTSETTINGSDIALOG_H__4A7ECDC5_1A48_11D6_BAB2_00500400878B__INCLUDED_)
#define AFX_PORTSETTINGSDIALOG_H__4A7ECDC5_1A48_11D6_BAB2_00500400878B__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PortSettingsDialog.h : header file
//

#include "Texts.h"

/////////////////////////////////////////////////////////////////////////////
// CPortSettingsDialog dialog

class CPortSettingsDialog : public CDialog
{
// Construction
public:
	CPortSettingsDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPortSettingsDialog)
	enum { IDD = IDD_PORT_SETTINGS };
	CComboBox	m_co_Name;
	CComboBox	m_co_Baud;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPortSettingsDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPortSettingsDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PORTSETTINGSDIALOG_H__4A7ECDC5_1A48_11D6_BAB2_00500400878B__INCLUDED_)
