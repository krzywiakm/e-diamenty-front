#if !defined(AFX_LBDEPDIALOG_H__852E5928_80FA_4EC9_AE14_FA3EBB58AF0F__INCLUDED_)
#define AFX_LBDEPDIALOG_H__852E5928_80FA_4EC9_AE14_FA3EBB58AF0F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LBDEPdialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLBDEPdialog dialog

class CLBDEPdialog : public CDialog
{
// Construction
public:
	CLBDEPdialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLBDEPdialog)
	enum { IDD = IDD_LBDEP };
	CEdit	m_ed_Quantity;
	CStatic	m_st_Quantity;
	CButton	m_bu_Cancel;
	CButton	m_stg_OptParams;
	CStatic	m_st_Number;
	CEdit	m_ed_Number;
	CButton	m_ch_OptParams;
	BOOL	m_bf_Cancellation;
	CString	m_str_Amount;
	int		m_i_DepositSelection;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLBDEPdialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLBDEPdialog)
	afx_msg void OnBSend();
	afx_msg void OnBKoniec();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBDefault();
	afx_msg void OnChOptParams();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LBDEPDIALOG_H__852E5928_80FA_4EC9_AE14_FA3EBB58AF0F__INCLUDED_)
