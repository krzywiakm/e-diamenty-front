#include "GlobalStuff.h"

/*----------------------------------------------------------------------------*/

void ErrorMessageBox( char * aStrPtr )
{
	_ASSERTE( aStrPtr );

	MessageBox( NULL, aStrPtr, ERRMSG_TITLE, MB_OK | MB_ICONERROR );
}

/*----------------------------------------------------------------------------*/

void WarningMessageBox( char * aStrPtr )
{
	_ASSERTE( aStrPtr );

	MessageBox( NULL, aStrPtr, WARNMSG_TITLE, MB_OK | MB_ICONWARNING );
}

/*----------------------------------------------------------------------------*/

void InfoMessageBox( char * aStrPtr )
{
	_ASSERTE( aStrPtr );

	MessageBox( NULL, aStrPtr, INFOMSG_TITLE, MB_OK | MB_ICONINFORMATION );
}

