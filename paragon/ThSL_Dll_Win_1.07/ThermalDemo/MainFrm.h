// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__BC3BEF28_06B5_11D6_BAB2_00500400878B__INCLUDED_)
#define AFX_MAINFRM_H__BC3BEF28_06B5_11D6_BAB2_00500400878B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ChildView.h"
#include "LBTRXEND1dialog.h"
#include "..\\ThermalServiceLibraryTypedefs.h"

#define TMP_CHAR_BUF_LEN 512
#define TMP_BYTE_BUF_LEN 512
#define MAX_PARAM_TABLE_ENTRIES 64

class CMainFrame : public CFrameWnd
{
	
public:
	CMainFrame();
protected: 
	DECLARE_DYNAMIC(CMainFrame)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void RemoveExcessMenus();

protected:  // control bar embedded members
//	CStatusBar  m_wndStatusBar;
//	CToolBar    m_wndToolBar;

	CChildView    m_wndView;

/*
	char TmpCharBuf[TMP_CHAR_BUF_LEN];
	BYTE TmpByteBuf[TMP_BYTE_BUF_LEN];
*/

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnAppTransStart();
	afx_msg void OnAppTransEntry();
	afx_msg void OnAppTransEnd();
	afx_msg void OnAppProgVat();
	afx_msg void OnAppProgHeader();
	afx_msg void OnAppExit();
	afx_msg void OnAppCommsParams();
	afx_msg void OnAppConfSave();
	afx_msg void OnAppMemRead();
	afx_msg void OnAppPrinterStat();
	afx_msg void OnAppDeposit();
	afx_msg void OnAppSeqLogging();
	afx_msg void OnAppRepCashier();
	afx_msg void OnAppRepForms();
	afx_msg void OnAppRepDaily();
	afx_msg void OnAppRepDailyD();
	afx_msg void OnAppRepPeriodD();
	afx_msg void OnAppRepPeriodR();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__BC3BEF28_06B5_11D6_BAB2_00500400878B__INCLUDED_)
