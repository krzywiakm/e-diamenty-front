// LBTRXENDdialog.cpp : implementation file
//

#include "stdafx.h"
#include "ThermalDemo.h"
#include "LBTRXENDdialog.h"

extern CThermalDemoApp theApp;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CLBTRXENDdialog dialog


CLBTRXENDdialog::CLBTRXENDdialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLBTRXENDdialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLBTRXENDdialog)
	//}}AFX_DATA_INIT
}


void CLBTRXENDdialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLBTRXENDdialog)
	DDX_Control(pDX, IDC_CH_REST, m_ch_Rest);
	DDX_Control(pDX, IDC_CH_DISCOUNT, m_ch_Discount);
	DDX_Control(pDX, IDC_RA_SURCHARGE, m_ra_Surcharge);
	DDX_Control(pDX, IDC_RA_DISCOUNT, m_ra_Discount);
	DDX_Control(pDX, IDCANCEL, m_bu_Cancel);
	DDX_Control(pDX, IDC_STG_DISCOUNT, m_stg_Discount);
	DDX_Control(pDX, IDC_STG_ADDIT_LINES, m_stg_AdditionalLines);
	DDX_Control(pDX, IDC_ST_REST, m_st_Rest);
	DDX_Control(pDX, IDC_ST_DISCOUNT_VALUE, m_st_DiscountValue);
	DDX_Control(pDX, IDC_ST_DEPOSIT_TAKEN_AMOUNT, m_st_DepositTakenAmount);
	DDX_Control(pDX, IDC_ST_DEPOSIT_TAKEN, m_st_DepositTaken);
	DDX_Control(pDX, IDC_ST_DEPOSIT_RETURNED_AMOUNT, m_st_DepositReturnedAmount);
	DDX_Control(pDX, IDC_ST_DEPOSIT_RETURNED, m_st_DepositReturned);
	DDX_Control(pDX, IDC_ST_COUPON_NAME, m_st_CouponName);
	DDX_Control(pDX, IDC_ST_COUPON_AMOUNT, m_st_CouponAmount);
	DDX_Control(pDX, IDC_ST_COUPON, m_st_Coupon);
	DDX_Control(pDX, IDC_ST_CHEQUE_NAME, m_st_ChequeName);
	DDX_Control(pDX, IDC_ST_CHEQUE_AMOUNT, m_st_ChequeAmount);
	DDX_Control(pDX, IDC_ST_CHEQUE, m_st_Cheque);
	DDX_Control(pDX, IDC_ST_CASH, m_st_Cash);
	DDX_Control(pDX, IDC_ST_CARD_NAME, m_st_CardName);
	DDX_Control(pDX, IDC_ST_CARD_AMOUNT, m_st_CardAmount);
	DDX_Control(pDX, IDC_ST_CARD, m_st_Card);
	DDX_Control(pDX, IDC_ST_ADDIT_LINES_UD, m_st_AdditionalLinesUD);
	DDX_Control(pDX, IDC_ST_ADDIT_LINES, m_st_AdditionalLines);
	DDX_Control(pDX, IDC_SP_ADDIT_LINES_UD, m_sp_AdditionalLinesUD);
	DDX_Control(pDX, IDC_ED_TOTAL, m_ed_Total);
	DDX_Control(pDX, IDC_ED_REST, m_ed_Rest);
	DDX_Control(pDX, IDC_ED_LINE5, m_ed_Line5);
	DDX_Control(pDX, IDC_ED_LINE4, m_ed_Line4);
	DDX_Control(pDX, IDC_ED_LINE3, m_ed_Line3);
	DDX_Control(pDX, IDC_ED_LINE2, m_ed_Line2);
	DDX_Control(pDX, IDC_ED_LINE1, m_ed_Line1);
	DDX_Control(pDX, IDC_ED_DISCOUNT_VALUE, m_ed_DiscountValue);
	DDX_Control(pDX, IDC_ED_DEPOSIT_TAKEN_AMOUNT, m_ed_DepositTakenAmount);
	DDX_Control(pDX, IDC_ED_DEPOSIT_RETURNED_AMOUNT, m_ed_DepositReturnedAmount);
	DDX_Control(pDX, IDC_ED_COUPON_NAME, m_ed_CouponName);
	DDX_Control(pDX, IDC_ED_COUPON_AMOUNT, m_ed_CouponAmount);
	DDX_Control(pDX, IDC_ED_CODE, m_ed_Code);
	DDX_Control(pDX, IDC_ED_CHEQUE_NAME, m_ed_ChequeName);
	DDX_Control(pDX, IDC_ED_CHEQUE_AMOUNT, m_ed_ChequeAmount);
	DDX_Control(pDX, IDC_ED_CASH, m_ed_Cash);
	DDX_Control(pDX, IDC_ED_CARD_NAME, m_ed_CardName);
	DDX_Control(pDX, IDC_ED_CARD_AMOUNT, m_ed_CardAmount);
	DDX_Control(pDX, IDC_CH_DEPOSIT_TAKEN, m_ch_DepositTaken);
	DDX_Control(pDX, IDC_CH_DEPOSIT_RETURNED, m_ch_DepositReturned);
	DDX_Control(pDX, IDC_CH_COUPON, m_ch_Coupon);
	DDX_Control(pDX, IDC_CH_CHEQUE, m_ch_Cheque);
	DDX_Control(pDX, IDC_CH_CASH, m_ch_Cash);
	DDX_Control(pDX, IDC_CH_CARD, m_ch_Card);
	DDX_Control(pDX, IDC_CH_ADDIT_LINES, m_ch_AdditLines);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLBTRXENDdialog, CDialog)
	//{{AFX_MSG_MAP(CLBTRXENDdialog)
	ON_BN_CLICKED(IDC_B_SEND, OnBSend)
	ON_BN_CLICKED(IDC_B_DEFAULT, OnBDefault)
	ON_BN_CLICKED(IDC_CH_ADDIT_LINES, OnChAdditLines)
	ON_BN_CLICKED(IDC_CH_CASH, OnChCash)
	ON_BN_CLICKED(IDC_CH_REST, OnChRest)
	ON_BN_CLICKED(IDC_CH_CARD, OnChCard)
	ON_BN_CLICKED(IDC_CH_CHEQUE, OnChCheque)
	ON_BN_CLICKED(IDC_CH_COUPON, OnChCoupon)
	ON_BN_CLICKED(IDC_CH_DISCOUNT, OnChDiscount)
	ON_BN_CLICKED(IDC_CH_DEPOSIT_TAKEN, OnChDepositTaken)
	ON_BN_CLICKED(IDC_CH_DEPOSIT_RETURNED, OnChDepositReturned)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SP_ADDIT_LINES_UD, OnDeltaposSpAdditLinesUd)
	ON_BN_CLICKED(IDC_B_QUIT, OnBQuit)
	ON_BN_CLICKED(IDC_B_CANCEL, OnBCancel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLBTRXENDdialog message handlers


/*----------------------------------------------------------------------------*/

void CLBTRXENDdialog::OnBDefault()
{

	// kod
	m_ed_Code.SetWindowText( "12A" );

	// Linie dodatkowe - nieaktywne (ale teksty wpisane)
	m_ed_Line1.SetWindowText( STR_LBTRXEND_LINE1 );
	m_ed_Line2.SetWindowText( STR_LBTRXEND_LINE2 );
	m_ed_Line3.SetWindowText( STR_LBTRXEND_LINE3 );
	m_ed_Line4.SetWindowText( STR_LBTRXEND_LINE4 );
	m_ed_Line5.SetWindowText( STR_LBTRXEND_LINE5 );
	m_ch_AdditLines.SetCheck( 0 );
	OnChAdditLines();

	// Formy p�atno�ci - nieaktywne (ale warto�ci wpisane)
	m_ed_CardName.SetWindowText( STR_LBTRXEND_CARD_NAME );
	m_ed_CardAmount.SetWindowText( STR_LBTRXEND_CARD_AMOUNT );
	m_ch_Card.SetCheck( 0 );
	OnChCard();

	m_ed_ChequeName.SetWindowText( STR_LBTRXEND_CHEQUE_NAME );
	m_ed_ChequeAmount.SetWindowText( STR_LBTRXEND_CHEQUE_AMOUNT );
	m_ch_Cheque.SetCheck( 0 );
	OnChCheque();

	m_ed_CouponName.SetWindowText( STR_LBTRXEND_COUPON_NAME );
	m_ed_CouponAmount.SetWindowText( STR_LBTRXEND_COUPON_AMOUNT );
	m_ch_Coupon.SetCheck( 0 );
	OnChCoupon();

	// Kaucje - nieaktywne (ale warto�ci wpisane)
	m_ed_DepositTakenAmount.SetWindowText( STR_LBTRXEND_DEP_TAKEN );
	m_ch_DepositTaken.SetCheck( 0 );
	OnChDepositTaken();

	m_ed_DepositReturnedAmount.SetWindowText( STR_LBTRXEND_DEP_RETURNED );
	m_ch_DepositReturned.SetCheck( 0 );
	OnChDepositReturned();

	// Total
	m_ed_Total.SetWindowText( STR_LBTRXEND_TOTAL );


	// Rabat - nieaktywny (ale warto�ci wpisane i wybrana pozycja "Rabat")
	m_ra_Discount.SetCheck( 0 );
	m_ch_Discount.SetCheck( 0 );
	OnChDiscount();
	m_ed_DiscountValue.SetWindowText( STR_LBTRXEND_DISCNT_VAL );

	// Wplata - niekatywna (ale warto�� wpisana )
	m_ch_Cash.SetCheck( 0 );
	OnChCash();
	m_ed_Cash.SetWindowText( STR_LBTRXEND_CASH );

	// Reszta - nieaktywna (ale warto�� wpisana )
	m_ch_Rest.SetCheck( 0 );
	OnChRest();
	m_ed_Rest.SetWindowText( STR_LBTRXEND_REST );
}

/*----------------------------------------------------------------------------*/

void CLBTRXENDdialog::OnBSend()
{
	static int RetI;
	
	static char * LinePtr[5];

	static tO_LBTRXEND_PARAMS LBTRXENDparams;

	static DWORD SpinPos, i;
	
	static CString Code,
		Line[5],
		TotalStr,
		DiscountStr,
		CashStr,
		RestStr,
		CardAmountStr, CardName,
		ChequeAmountStr, ChequeName,
		CouponAmountStr, CouponName,
		DepositTakenStr,
		DepositReturnedStr;

	static char Empty[] = "";
	

#ifdef _DEBUG
	memset( & LBTRXENDparams, 0, sizeof( LBTRXENDparams) );		/* czyszczenie - tylko DEBUG */
#endif // _DEBUG

/*
	Kod
*/
	m_ed_Code.GetWindowText( Code );
	LBTRXENDparams.Code= (char *) LPCTSTR( Code );
	
/*
	Linie dodatkowe
*/
	if( m_ch_AdditLines.GetCheck() == 1 )
	{
		SpinPos = m_sp_AdditionalLinesUD.GetPos();

		m_ed_Line1.GetWindowText( Line[0] );
		LinePtr[0] = (char *) LPCTSTR( Line[0] );

		if( SpinPos > 1 )
		{
			m_ed_Line2.GetWindowText( Line[1] );
			LinePtr[1] = (char *) LPCTSTR( Line[1] );

			if( SpinPos > 2 )
			{
				m_ed_Line3.GetWindowText( Line[2] );
				LinePtr[2] = (char *) LPCTSTR( Line[2] );

				if( SpinPos > 3 )
				{
					m_ed_Line4.GetWindowText( Line[3] );
					LinePtr[3] = (char *) LPCTSTR( Line[3] );

					if( SpinPos > 4 )
					{
						m_ed_Line5.GetWindowText( Line[4] );
						LinePtr[4] = (char *) LPCTSTR( Line[4] );
					}
				}
			}
		}

		LBTRXENDparams.Line = LinePtr;
		LBTRXENDparams.Pn = (BYTE) SpinPos;					// Pn
	}
	else
	{
		LBTRXENDparams.Pn = 0;
	}



/*
	<nazwa_karty>, KARTA
*/
	LBTRXENDparams.Card.TypeTranslFuncPtr = NULL;

	if( m_ch_Card.GetCheck() == 1 )
	{
		m_ed_CardName.GetWindowText( CardName );
		m_ed_CardAmount.GetWindowText( CardAmountStr );

		LBTRXENDparams.Pk = 1;
		LBTRXENDparams.CardName = (char *) LPCTSTR( CardName );
		LBTRXENDparams.Card.ParamValuePtr = (char *) LPCTSTR( CardAmountStr );
	}
	else
	{
		LBTRXENDparams.Pk = 0;	/* nie ma karty */
	}

/*
	<nazwa_czeku>, CZEK
*/
	LBTRXENDparams.Cheque.TypeTranslFuncPtr = NULL;

	if( m_ch_Cheque.GetCheck() == 1 )
	{
		m_ed_ChequeName.GetWindowText( ChequeName );
		m_ed_ChequeAmount.GetWindowText( ChequeAmountStr );

		LBTRXENDparams.Pz = 1;
		LBTRXENDparams.ChequeName = (char *) LPCTSTR( ChequeName );
		LBTRXENDparams.Cheque.ParamValuePtr = (char *) LPCTSTR( ChequeAmountStr );
	}
	else
	{
		LBTRXENDparams.Pz = 0;	/* nie ma czeku */
	}

/*
	<nazwa_bonu>, BON
*/
	LBTRXENDparams.Coupon.TypeTranslFuncPtr = NULL;

	if( m_ch_Coupon.GetCheck() == 1 )
	{
		m_ed_CouponName.GetWindowText( CouponName );
		m_ed_CouponAmount.GetWindowText( CouponAmountStr );

		LBTRXENDparams.Pb = 1;
		LBTRXENDparams.CouponName = (char *) LPCTSTR( CouponName );
		LBTRXENDparams.Coupon.ParamValuePtr = (char *) LPCTSTR( CouponAmountStr );
	}
	else
	{
		LBTRXENDparams.Pb = 0;	/* nie ma bonu */
	}

/*
	TOTAL
*/
	m_ed_Total.GetWindowText( TotalStr );
	LBTRXENDparams.Total.ParamValuePtr = (char *) LPCTSTR( TotalStr );
	LBTRXENDparams.Total.TypeTranslFuncPtr = NULL;

/*
	RABAT
*/
	LBTRXENDparams.Discount.TypeTranslFuncPtr = NULL;

	if( m_ch_Discount.GetCheck() == 1 )
	{
		if( m_ra_Discount.GetCheck() == 1 )
		{
			LBTRXENDparams.Px = LBTRXEND1_DISCNT_PERCNT;
		}
		else
		{
			LBTRXENDparams.Px = LBTRXEND_SURCH_PERCNT;
		}

		m_ed_DiscountValue.GetWindowText( DiscountStr );
		LBTRXENDparams.Discount.ParamValuePtr = (char *) LPCTSTR( DiscountStr );
	}
	else
	{
		LBTRXENDparams.Px = 0;	/* brak rabatu */
	}

/*
	WPLATA
*/
	LBTRXENDparams.Cash.TypeTranslFuncPtr = NULL;

	if( m_ch_Cash.GetCheck() == 1 )
	{
		LBTRXENDparams.Pg = 1;

		m_ed_Cash.GetWindowText( CashStr );
		LBTRXENDparams.Cash.ParamValuePtr = (char *) LPCTSTR( CashStr );
	}
	else
	{
		LBTRXENDparams.Pg = 0;
	}

/*
	KAUCJA_POBRANA
*/
	LBTRXENDparams.DepositTaken.TypeTranslFuncPtr = NULL;

	if( m_ch_DepositTaken.GetCheck() == 1 )
	{
		LBTRXENDparams.Po1 = 1;

		m_ed_DepositTakenAmount.GetWindowText( DepositTakenStr );
		LBTRXENDparams.DepositTaken.ParamValuePtr = (char *) LPCTSTR( DepositTakenStr );
	}
	else
	{
		LBTRXENDparams.Po1 = 0;
	}

/*
	KAUCJA_ZWROCONA
*/
	LBTRXENDparams.DepositReturned.TypeTranslFuncPtr = NULL;

	if( m_ch_DepositReturned.GetCheck() == 1 )
	{
		LBTRXENDparams.Po2 = 1;

		m_ed_DepositReturnedAmount.GetWindowText( DepositReturnedStr );
		LBTRXENDparams.DepositReturned.ParamValuePtr = (char *) LPCTSTR( DepositReturnedStr );
	}
	else
	{
		LBTRXENDparams.Po2 = 0;
	}

/*
	RESZTA
*/
	LBTRXENDparams.Rest.TypeTranslFuncPtr = NULL;

	if( m_ch_Rest.GetCheck() == 1 )
	{
		LBTRXENDparams.Pr = 1;														// Pr

		m_ed_Rest.GetWindowText( RestStr );
		LBTRXENDparams.Rest.ParamValuePtr = (char *) LPCTSTR( RestStr );
		
	}
	else
	{
		LBTRXENDparams.Pr = 0;
	}

/*
	Pozosta�e parametry numeryczne, wpisane sta�e warto�ci
*/
	// Pc
	LBTRXENDparams.Pc = 0;
	
	// Py - skr�cone podsumowanie w��czone !!!
	LBTRXENDparams.Py = 1;

	RetI = theApp.ThSL_O_LBTRXEND( & LBTRXENDparams );

	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBTRXEND !", RetI );
	}
}

/*----------------------------------------------------------------------------*/

BOOL CLBTRXENDdialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_sp_AdditionalLinesUD.SetRange( 1, LBTRXEND_MAX_ADD_LINES );
	m_sp_AdditionalLinesUD.SetPos( LBTRXEND_MAX_ADD_LINES );
		
	OnBDefault();
	
	return TRUE;
}

/*----------------------------------------------------------------------------*/

void CLBTRXENDdialog::PostNcDestroy() 
{
	theApp.p_CLBTRXENDdialog = NULL;
	delete this;
	
	CDialog::PostNcDestroy();
}


/*----------------------------------------------------------------------------*/

void CLBTRXENDdialog::OnCancel() 
{
	DestroyWindow();
}

/*----------------------------------------------------------------------------*/


void CLBTRXENDdialog::EnableAdditionalLines( int aNrOfLines )
{

	if( aNrOfLines  <= LBTRXEND_MAX_ADD_LINES )
	{
		switch( aNrOfLines )
		{
			default:	
				// NO BREAK HERE

			case 1:
				m_ed_Line1.EnableWindow( FALSE );
				// NO BREAK HERE

			case 2:
				m_ed_Line2.EnableWindow( FALSE );
				// NO BREAK HERE

			case 3:
				m_ed_Line3.EnableWindow( FALSE );
				// NO BREAK HERE

			case 4:
				m_ed_Line4.EnableWindow( FALSE );
				// NO BREAK HERE

			case 5:
				m_ed_Line5.EnableWindow( FALSE );
				break;
		}
	}

	if( (aNrOfLines > 0) && (aNrOfLines <= LBTRXEND_MAX_ADD_LINES) )
	{
		switch( aNrOfLines )
		{
			default:
				// NO BREAK HERE

			case 5:
				m_ed_Line5.EnableWindow( TRUE );
				// NO BREAK HERE

			case 4:
				m_ed_Line4.EnableWindow( TRUE );
				// NO BREAK HERE

			case 3:
				m_ed_Line3.EnableWindow( TRUE );
				// NO BREAK HERE

			case 2:
				m_ed_Line2.EnableWindow( TRUE );
				// NO BREAK HERE

			case 1:
				m_ed_Line1.EnableWindow( TRUE );
				break;
		}
	}
}


void CLBTRXENDdialog::OnChAdditLines() 
{
	ChkBoxState = ( m_ch_AdditLines.GetCheck() == 1);
	
	m_st_AdditionalLines.EnableWindow( ChkBoxState );
	m_st_AdditionalLinesUD.EnableWindow( ChkBoxState );
	m_stg_AdditionalLines.EnableWindow( ChkBoxState );
	m_sp_AdditionalLinesUD.EnableWindow( ChkBoxState );

	if( ChkBoxState )
	{
		EnableAdditionalLines( m_sp_AdditionalLinesUD.GetPos() );
	}
	else
	{
		EnableAdditionalLines( 0 );
	}
}

void CLBTRXENDdialog::OnChCash() 
{
	ChkBoxState = ( m_ch_Cash.GetCheck() == 1);

	m_st_Cash.EnableWindow( ChkBoxState );
	m_ed_Cash.EnableWindow( ChkBoxState );
}

void CLBTRXENDdialog::OnChRest() 
{
	ChkBoxState = ( m_ch_Rest.GetCheck() == 1);

	m_st_Rest.EnableWindow( ChkBoxState );
	m_ed_Rest.EnableWindow( ChkBoxState );
}

void CLBTRXENDdialog::OnChCard() 
{
	ChkBoxState = ( m_ch_Card.GetCheck() == 1);

	m_st_Card.EnableWindow( ChkBoxState );
	m_st_CardAmount.EnableWindow( ChkBoxState );
	m_st_CardName.EnableWindow( ChkBoxState );
	m_ed_CardAmount.EnableWindow( ChkBoxState );
	m_ed_CardName.EnableWindow( ChkBoxState );
}

void CLBTRXENDdialog::OnChCheque() 
{
	ChkBoxState = ( m_ch_Cheque.GetCheck() == 1);

	m_st_Cheque.EnableWindow( ChkBoxState );
	m_st_ChequeAmount.EnableWindow( ChkBoxState );
	m_st_ChequeName.EnableWindow( ChkBoxState );
	m_ed_ChequeAmount.EnableWindow( ChkBoxState );
	m_ed_ChequeName.EnableWindow( ChkBoxState );
}

void CLBTRXENDdialog::OnChCoupon() 
{
	ChkBoxState = ( m_ch_Coupon.GetCheck() == 1);

	m_st_Coupon.EnableWindow( ChkBoxState );
	m_st_CouponAmount.EnableWindow( ChkBoxState );
	m_st_CouponName.EnableWindow( ChkBoxState );
	m_ed_CouponAmount.EnableWindow( ChkBoxState );
	m_ed_CouponName.EnableWindow( ChkBoxState );
}

void CLBTRXENDdialog::OnChDiscount() 
{
	ChkBoxState = ( m_ch_Discount.GetCheck() == 1);

	m_stg_Discount.EnableWindow( ChkBoxState );
	m_ra_Discount.EnableWindow( ChkBoxState );
	m_ra_Surcharge.EnableWindow( ChkBoxState );
	m_st_DiscountValue.EnableWindow( ChkBoxState );
	m_ed_DiscountValue.EnableWindow( ChkBoxState );
}

void CLBTRXENDdialog::OnChDepositTaken() 
{
	ChkBoxState = ( m_ch_DepositTaken.GetCheck() == 1);

	m_st_DepositTaken.EnableWindow( ChkBoxState );
	m_st_DepositTakenAmount.EnableWindow( ChkBoxState );
	m_ed_DepositTakenAmount.EnableWindow( ChkBoxState );
}

void CLBTRXENDdialog::OnChDepositReturned() 
{
	ChkBoxState = ( m_ch_DepositReturned.GetCheck() == 1);

	m_st_DepositReturned.EnableWindow( ChkBoxState );
	m_st_DepositReturnedAmount.EnableWindow( ChkBoxState );
	m_ed_DepositReturnedAmount.EnableWindow( ChkBoxState );
}

void CLBTRXENDdialog::OnDeltaposSpAdditLinesUd(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;

	static int NrOfLines;

	NrOfLines = pNMUpDown->iPos + pNMUpDown->iDelta;
	if( NrOfLines > 0 )
	{
		EnableAdditionalLines( NrOfLines );
	}
	
	*pResult = 0;
}

void CLBTRXENDdialog::OnBQuit() 
{
	DestroyWindow();
}

void CLBTRXENDdialog::OnBCancel() 
{
	static int RetI;

	RetI = theApp.ThSL_O_LBTREXIT();
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "O_LBTREXIT!", RetI );
	}
	
}
