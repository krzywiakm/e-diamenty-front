// MainFrm.cpp : implementation of the CMainFrame class
//


#include "stdafx.h"
#include "ThermalDemo.h"
#include "MainFrm.h"
#include "LBTRSHDRdialog.h"
#include "LBTRSLNdialog.h"
#include "LBTRXENDcommon.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define SOK 0

extern CThermalDemoApp theApp;
extern int TranslFunc_float( float *aParamValue, char *aDestString );
extern int TranslFunc_int( int *aParamValue, char *aDestString );

/******************************************************************************/

int TranslFunc_float( float *aParamValue, char *aDestString )
{
	sprintf( aDestString,"%.2f", *aParamValue );

	return SOK;
}

/*----------------------------------------------------------------------------*/

int TranslFunc_int( int *aParamValue, char *aDestString )
{
	sprintf( aDestString,"%d", *aParamValue );

	return SOK;
}

/******************************************************************************/

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_COMMAND(ID_APP_TRANS_START, OnAppTransStart)
	ON_COMMAND(ID_APP_TRANS_ENTRY, OnAppTransEntry)
	ON_COMMAND(ID_APP_TRANS_END, OnAppTransEnd)
	ON_COMMAND(ID_APP_PROG_VAT, OnAppProgVat)
	ON_COMMAND(ID_APP_PROG_HEADER, OnAppProgHeader)
	ON_COMMAND(ID_APP_EXIT, OnAppExit)
	ON_COMMAND(ID_APP_COMMS_PARAMS, OnAppCommsParams)
	ON_COMMAND(ID_APP_CONF_SAVE, OnAppConfSave)
	ON_COMMAND(ID_APP_MEM_READ, OnAppMemRead)
	ON_COMMAND(ID_APP_PRINTER_STAT, OnAppPrinterStat)
	ON_COMMAND(ID_APP_DEPOSIT, OnAppDeposit)
	ON_COMMAND(ID_APP_SEQ_LOGGING, OnAppSeqLogging)
	ON_COMMAND(ID_APP_REP_CASHIER, OnAppRepCashier)
	ON_COMMAND(ID_APP_REP_FORMS, OnAppRepForms)
	ON_COMMAND(ID_APP_REP_DAILY, OnAppRepDaily)
	ON_COMMAND(ID_APP_REP_DAILY_D, OnAppRepDailyD)
	ON_COMMAND(ID_APP_REP_PERIOD_D, OnAppRepPeriodD)
	ON_COMMAND(ID_APP_REP_PERIOD_R, OnAppRepPeriodR)
	//}}AFX_MSG_MAP
	// Global help commands
	ON_COMMAND(ID_HELP_FINDER, CFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_HELP, CFrameWnd::OnHelp)
	ON_COMMAND(ID_CONTEXT_HELP, CFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CFrameWnd::OnHelpFinder)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

/******************************************************************************/

CMainFrame::CMainFrame()
{
}

/*----------------------------------------------------------------------------*/

CMainFrame::~CMainFrame()
{
}

/******************************************************************************/

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	// create a view to occupy the client area of the frame
	if (!m_wndView.Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
	{
		TRACE0("Failed to create view window\n");
		return -1;
	}

	this->m_bAutoMenuEnable = TRUE;

	return 0;
}

/*----------------------------------------------------------------------------*/

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
void CMainFrame::OnSetFocus(CWnd* pOldWnd)
{
	// forward focus to the view window
	m_wndView.SetFocus();
}

/*----------------------------------------------------------------------------*/

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// let the view have first crack at the command
	if (m_wndView.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	// otherwise, do default handling
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

/******************************************************************************/

void CMainFrame::OnAppCommsParams() 
{
	CPortSettingsDialog PortSettingsDlg;

	if( PortSettingsDlg.DoModal() == IDOK )
	{
		if( (theApp.PortNameNdx != -1) && (theApp.BaudRateNdx != -1) )
		{
/*
			EnableMNU_Operation( FALSE );
			EnableMNU_DeviceConnect( TRUE );
			EnableMNU_DeviceDisconnect( FALSE );
*/
		}
	}
}

/*----------------------------------------------------------------------------*/

void CMainFrame::OnAppConfSave()
{
	theApp.SaveSettings();
}

/*----------------------------------------------------------------------------*/

void CMainFrame::OnAppExit() 
{
	DestroyWindow();
}

/******************************************************************************/

void CMainFrame::OnAppProgHeader() 
{
	if( !theApp.p_CLBSETHDRdialog )
	{
		theApp.p_CLBSETHDRdialog = new CLBSETHDRdialog;

		if( !theApp.p_CLBSETHDRdialog )
		{
			AfxMessageBox( "new CLBSETHDRdialog failed !" );
		}
		else
		{
			if( !theApp.p_CLBSETHDRdialog->Create(IDD_LBSETHDR, this) )
			{
				delete theApp.p_CLBSETHDRdialog;
				theApp.p_CLBSETHDRdialog = NULL;

				AfxMessageBox( "p_CLBSETHDRdialog->Create() failed !" );
			}
			else
			{
				theApp.p_CLBSETHDRdialog->ShowWindow( SW_SHOW );
			}
		}
	}
	else
	{
		theApp.p_CLBSETHDRdialog->SetActiveWindow();
	}
}

/*----------------------------------------------------------------------------*/

void CMainFrame::OnAppProgVat() 
{
	if( !theApp.p_CLBSETPTUdialog )
	{
		theApp.p_CLBSETPTUdialog = new CLBSETPTUdialog;

		if( !theApp.p_CLBSETPTUdialog )
		{
			AfxMessageBox( "new CLBSETPTUdialog failed !" );
		}
		else
		{
			if( !theApp.p_CLBSETPTUdialog->Create(IDD_LBSETPTU, this) )
			{
				delete theApp.p_CLBSETPTUdialog;
				theApp.p_CLBSETPTUdialog = NULL;

				AfxMessageBox( "p_CLBSETPTUdialog->Create() failed !" );
			}
			else
			{
				theApp.p_CLBSETPTUdialog->ShowWindow( SW_SHOW );
			}
		}
	}
	else
	{
		theApp.p_CLBSETPTUdialog->SetActiveWindow();
	}
}

/******************************************************************************/

void CMainFrame::OnAppTransStart() 
{
	if( !theApp.p_CLBTRSHDRdialog )
	{
		theApp.p_CLBTRSHDRdialog = new CLBTRSHDRdialog;

		if( !theApp.p_CLBTRSHDRdialog )
		{
			AfxMessageBox( "new CLBTRSHDRdialog failed !" );
		}
		else
		{
			if( !theApp.p_CLBTRSHDRdialog->Create(IDD_LBTRSHDR, this) )
			{
				delete theApp.p_CLBTRSHDRdialog;
				theApp.p_CLBTRSHDRdialog = NULL;

				AfxMessageBox( "p_CLBTRSHDRdialog->Create() failed !" );
			}
			else
			{
				theApp.p_CLBTRSHDRdialog->ShowWindow( SW_SHOW );
			}
		}
	}
	else
	{
		theApp.p_CLBTRSHDRdialog->SetActiveWindow();
	}
}

/*----------------------------------------------------------------------------*/

void CMainFrame::OnAppTransEntry() 
{
	if( !theApp.p_CLBTRSLNdialog )
	{
		theApp.p_CLBTRSLNdialog = new CLBTRSLNdialog;

		if( !theApp.p_CLBTRSLNdialog )
		{
			AfxMessageBox( "new CLBTRSLNdialog failed !" );
		}
		else
		{
			if( !theApp.p_CLBTRSLNdialog->Create(IDD_LBTRSLN, this) )
			{
				delete theApp.p_CLBTRSLNdialog;
				theApp.p_CLBTRSLNdialog = NULL;

				AfxMessageBox( "p_CLBTRSLNdialog->Create() failed !" );
			}
			else
			{
				theApp.p_CLBTRSLNdialog->ShowWindow( SW_SHOW );
			}
		}
	}
	else
	{
		theApp.p_CLBTRSLNdialog->SetActiveWindow();
	}
}

/*----------------------------------------------------------------------------*/

void CMainFrame::OnAppDeposit() 
{
	if( !theApp.p_CLBDEPdialog )
	{
		theApp.p_CLBDEPdialog = new CLBDEPdialog;

		if( !theApp.p_CLBDEPdialog )
		{
			AfxMessageBox( "new p_CLBDEPdialog failed !" );
		}
		else
		{
			if( !theApp.p_CLBDEPdialog->Create(IDD_LBDEP, this) )
			{
				delete theApp.p_CLBDEPdialog;
				theApp.p_CLBDEPdialog = NULL;

				AfxMessageBox( "p_CLBDEPdialog->Create() failed !" );
			}
			else
			{
				theApp.p_CLBDEPdialog->ShowWindow( SW_SHOW );
			}
		}
	}
	else
	{
		theApp.p_CLBDEPdialog->SetActiveWindow();
	}
}

/*----------------------------------------------------------------------------*/

/*
	W zale�no�ci od tego, czy urz�dzenie jest ze starej czy nowej homologacji,
	uruchomimy inne okno dialogowe.
	Nowa	- LBTRXEND1
	Stara	- LBTRXNED
*/

void CMainFrame::OnAppTransEnd() 
{
	if( theApp.ThermalVersionNew )
	{
		if( !theApp.p_CLBTRXEND1dialog )
		{
			theApp.p_CLBTRXEND1dialog = new CLBTRXEND1dialog;
			
			if( !theApp.p_CLBTRXEND1dialog )
			{
				AfxMessageBox( "new CLBTRXEND1dialog failed !" );
			}
			else
			{
				if( !theApp.p_CLBTRXEND1dialog->Create(IDD_LBTRXEND1, this) )
				{
					delete theApp.p_CLBTRXEND1dialog;
					theApp.p_CLBTRXEND1dialog = NULL;
					
					AfxMessageBox( "p_CLBTRXEND1dialog->Create() failed !" );
				}
				else
				{
					theApp.p_CLBTRXEND1dialog->ShowWindow( SW_SHOW );
				}
			}
		}
		else
		{
			theApp.p_CLBTRXEND1dialog->SetActiveWindow();
		}
	}
	else
	{
		if( !theApp.p_CLBTRXENDdialog )
		{
			theApp.p_CLBTRXENDdialog = new CLBTRXENDdialog;
			
			if( !theApp.p_CLBTRXENDdialog )
			{
				AfxMessageBox( "new CLBTRXENDdialog failed !" );
			}
			else
			{
				if( !theApp.p_CLBTRXENDdialog->Create(IDD_LBTRXEND, this) )
				{
					delete theApp.p_CLBTRXENDdialog;
					theApp.p_CLBTRXENDdialog = NULL;
					
					AfxMessageBox( "p_CLBTRXENDdialog->Create() failed !" );
				}
				else
				{
					theApp.p_CLBTRXENDdialog->ShowWindow( SW_SHOW );
				}
			}
		}
		else
		{
			theApp.p_CLBTRXENDdialog->SetActiveWindow();
		}
	}
}

/******************************************************************************/

void CMainFrame::OnAppMemRead() 
{
	if( ! theApp.ThermalVersionNew )
	{
		return;
	}
	
	if( !theApp.p_CLBFSTRQdialog )
	{
		theApp.p_CLBFSTRQdialog = new CLBFSTRQdialog;

		if( !theApp.p_CLBFSTRQdialog )
		{
			AfxMessageBox( "new CLBFSTRQdialog failed !" );
		}
		else
		{
			if( !theApp.p_CLBFSTRQdialog->Create(IDD_LBFSTRQ, this) )
			{
				delete theApp.p_CLBFSTRQdialog;
				theApp.p_CLBFSTRQdialog = NULL;

				AfxMessageBox( "p_CLBFSTRQdialog->Create() failed !" );
			}
			else
			{
				theApp.p_CLBFSTRQdialog->ShowWindow( SW_SHOW );
			}
		}
	}
	else
	{
		theApp.p_CLBFSTRQdialog->SetActiveWindow();
	}
}

/*----------------------------------------------------------------------------*/

void CMainFrame::OnAppPrinterStat() 
{
	if( ! theApp.ThermalVersionNew )
	{
		return;
	}

	if( !theApp.p_CLBFSTRQrepDialog )
	{
		theApp.p_CLBFSTRQrepDialog = new CLBFSTRQrepDialog;

		if( !theApp.p_CLBFSTRQrepDialog )
		{
			AfxMessageBox( "new CLBFSTRQrepDialog failed !" );
		}
		else
		{
			if( !theApp.p_CLBFSTRQrepDialog->Create(IDD_LBFSTRQ_REP, this) )
			{
				delete theApp.p_CLBFSTRQrepDialog;
				theApp.p_CLBFSTRQrepDialog = NULL;

				AfxMessageBox( "p_CLBFSTRQrepDialog->Create() failed !" );
			}
			else
			{
				theApp.p_CLBFSTRQrepDialog->ShowWindow( SW_SHOW );
			}
		}
	}
	else
	{
		theApp.p_CLBFSTRQrepDialog->SetActiveWindow();
	}
}

/******************************************************************************/


void CMainFrame::OnAppSeqLogging() 
{
	if( !theApp.hMailSlot )		/* tylko je�li jest Mailslot */
	{
		return;
	}

	if( !theApp.p_CSnooperDialog )
	{
		theApp.p_CSnooperDialog = new CSnooperDialog;

		if( !theApp.p_CSnooperDialog )
		{
			AfxMessageBox( "new CSnooperDialog failed !" );
		}
		else
		{
			if( !theApp.p_CSnooperDialog->Create(IDD_SNOOPER, this) )
			{
				delete theApp.p_CSnooperDialog;
				theApp.p_CSnooperDialog = NULL;

				AfxMessageBox( "p_CSnooperDialog->Create() failed !" );
			}
			else
			{
				theApp.p_CSnooperDialog->ShowWindow( SW_SHOW );
			}
		}
	}
	else
	{
		theApp.p_CSnooperDialog->SetActiveWindow();
	}
}

/******************************************************************************/

/*
	Raport dobowy, wersja bez daty
*/
void CMainFrame::OnAppRepDaily() 
{
	static int RetI;
	static tLBDAYREP_PARAMS LBDAYREP_params;

	LBDAYREP_params.CashRegNr	= "Nr1";
	LBDAYREP_params.Cashier		= "Kasjer1";
	LBDAYREP_params._Date = 0;	// bez daty

	RetI = theApp.ThSL_LBDAYREP( & LBDAYREP_params );
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBDAYREP", RetI );
	}

}

/*----------------------------------------------------------------------------*/

/*
	Raport dobowy, wersja z dat� (aktualn� - wi�c nie b�dzie pytania o 
	potwierdzenie
*/
void CMainFrame::OnAppRepDailyD() 
{
	static int RetI;
	static tLBDAYREP_PARAMS LBDAYREP_params;
	static tLBSENDCK_RESPONSE LBSENDCKresp;

	RetI = theApp.ThSL_LBSENDCK( & LBSENDCKresp );
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBSENDCK", RetI );
	}
	else
	{
		LBDAYREP_params.CashRegNr	= "Nr1";
		LBDAYREP_params.Cashier		= "Kasjer1";
		LBDAYREP_params._Date			= 1;	// jest data
		LBDAYREP_params.Py				= LBSENDCKresp.Pyy;
		LBDAYREP_params.Pm				= LBSENDCKresp.Pmm;
		LBDAYREP_params.Pd				= LBSENDCKresp.Pdd;

		RetI = theApp.ThSL_LBDAYREP( & LBDAYREP_params );
		if( RetI != SOK )
		{
			theApp.ThSL_ErrorMessage( "LBDAYREP", RetI );
		}
	}
}

/*----------------------------------------------------------------------------*/

/*
	Raport okresowy - z podanymi datami
*/
void CMainFrame::OnAppRepPeriodD() 
{
	static int RetI;
	static tLBFSKREP_D_PARAMS LBFSKREP_D_params;
	static tLBSENDCK_RESPONSE LBSENDCKresp;

	RetI = theApp.ThSL_LBSENDCK( & LBSENDCKresp );
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBSENDCK", RetI );
	}
	else
	{
		LBFSKREP_D_params.CashRegNr	= "Nr1";
		LBFSKREP_D_params.Cashier		= "Kasjer1";

/*
		Od: 2001.01.01
*/
		LBFSKREP_D_params.Py1	= 1;
		LBFSKREP_D_params.Pm1	= 1;
		LBFSKREP_D_params.Pd1	= 1;
/*
		Do: bie��ca data
*/
		LBFSKREP_D_params.Py2	= LBSENDCKresp.Pyy;
		LBFSKREP_D_params.Pm2	= LBSENDCKresp.Pmm;
		LBFSKREP_D_params.Pd2	= LBSENDCKresp.Pdd;
/*
		Podsumowanie z okre�lonego zakresu dat
*/
		LBFSKREP_D_params.Pt	= 1;

		RetI = theApp.ThSL_LBFSKREP_D( & LBFSKREP_D_params );
		if( RetI != SOK )
		{
			theApp.ThSL_ErrorMessage( "LBFSKREP_D", RetI );
		}
	}
}

/*----------------------------------------------------------------------------*/

/*
	Raport okresowy - zakres podany przez numery rekord�w podanymi datami
*/
void CMainFrame::OnAppRepPeriodR()
{
	static int RetI;
	static tLBFSTRQ1_RESPONSE LBFSTRQ1response;
	static tLBFSKREP_R_PARAMS LBFSKREP_R_params;

	RetI = theApp.ThSL_LBFSTRQ24( & LBFSTRQ1response );
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBFSTRQ24", RetI );
	}
	else
	{
/*
		Zakres: od 0 do ostatniego rekordu - o ile s� jakie� rekordy.
*/
		if( LBFSTRQ1response.DailyRecNr )
		{
			LBFSKREP_R_params.CashRegNr	= "Nr1";
			LBFSKREP_R_params.Cashier		= "Kasjer1";

			LBFSKREP_R_params.From = 1;
			LBFSKREP_R_params.To = LBFSTRQ1response.DailyRecNr;

			LBFSKREP_R_params.Pt	= 17;

			RetI = theApp.ThSL_LBFSKREP_R( & LBFSKREP_R_params );
			if( RetI != SOK )
			{
				theApp.ThSL_ErrorMessage( "LBFSKREP_R", RetI );
			}
		}
		else
		{
			InfoMessageBox( INFOMSG_NO_RECORDS );


		}
	}
}

/*----------------------------------------------------------------------------*/

void CMainFrame::OnAppRepCashier() 
{
	static int RetI;
	static tLBCSHREP_PARAMS LBCSHREPparams;
	
	LBCSHREPparams.Ps					= 23;		/* raport czytaj�cy */

	LBCSHREPparams.Shift			= STR_LBCSHREP_SHIFT;
	LBCSHREPparams.CashRegNr	= STR_LBCSHREP_CASH_REG_NR;
	LBCSHREPparams.Cashier		= STR_LBCSHREP_CASHIER;

	RetI = theApp.ThSL_LBCSHREP( & LBCSHREPparams );
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBCSHREP", RetI );
	}
}

/*----------------------------------------------------------------------------*/

void CMainFrame::OnAppRepForms() 
{

/*
	BYTE									Pkb;
	BYTE									Pkz;
	BYTE									Pfn;
	BYTE									Pg;
	BYTE *								Pfx;
	char *								Shift;
	char *								CashRegNr;
	char *								Cashier;
	char *								Beginning;
	char *								End;
	char **								FormOfPaymentName;
	char **								DepositTakenName;
	char **								DepositReturnedName;
	tEXTERNAL_NUMERIC			Takings;
	tEXTERNAL_NUMERIC			SalesCash;
	tEXTERNAL_NUMERIC			PaymentIn;
	tEXTERNAL_NUMERIC			Expenditures;
	tEXTERNAL_NUMERIC			PaymentOut;
	tEXTERNAL_NUMERIC *		FormOfPaymentAmount;
	tEXTERNAL_NUMERIC			DepositTakenTotal;
	tEXTERNAL_NUMERIC *		DepositTakenAmount;
	tEXTERNAL_NUMERIC			DepositReturnedTotal;
	tEXTERNAL_NUMERIC *		DepositReturnedAmount;
	tEXTERNAL_NUMERIC			Cash;
	DWORD									NrOfReceipts;
	DWORD									NrOfCancelledReceipts;
	DWORD									NrOfCancelledItems;
*/

	static int RetI;
	static tLBCSHREP2_PARAMS LBCSHREP2params;

	
#ifdef _DEBUG
	memset( & LBCSHREP2params, 0, sizeof(LBCSHREP2params) );
#endif // _DEBUG



/*
	Dwie formy p�atno�ci
*/
	const BYTE FOP_Type[2] =
		{ FORM_CARD, FORM_CHEQUE };

	const char FOP_NameStr[2][8] =
		{ STR_LBCSHREP2_FOP_NAME_1, STR_LBCSHREP2_FOP_NAME_2 };

	const char * FOP_Name[2] =
		{ FOP_NameStr[0], FOP_NameStr[1] };

	const char FOP_AmountStr[2][8] =
		{ STR_LBCSHREP2_FOP_AMOUNT_1, STR_LBCSHREP2_FOP_AMOUNT_2 };

	const tEXTERNAL_NUMERIC FOP_Amount[2] =
		{ {NULL, (LPVOID) & FOP_AmountStr[0]}, {NULL, (LPVOID) & FOP_AmountStr[1]} };

	LBCSHREP2params.Pfn									= 2;
	LBCSHREP2params.FormOfPaymentName		= (char **) FOP_Name;
	LBCSHREP2params.FormOfPaymentAmount	= (tEXTERNAL_NUMERIC *) FOP_Amount;
	LBCSHREP2params.Pfx									= (BYTE *) FOP_Type;

/*
	Trzy kaucje pobrane.
*/
	const char DT_NameStr[3][16] =
		{ STR_LBCSHREP2_DEP_T_NAME_1, STR_LBCSHREP2_DEP_T_NAME_2,
			STR_LBCSHREP2_DEP_T_NAME_3 };

	const char * DT_Name[3] =
		{ DT_NameStr[0], DT_NameStr[1], DT_NameStr[2] };

	const char DT_AmountStr[3][8] =
		{ STR_LBCSHREP2_DEP_T_AMOUNT_1, STR_LBCSHREP2_DEP_T_AMOUNT_2,
			STR_LBCSHREP2_DEP_T_AMOUNT_3 };

	const tEXTERNAL_NUMERIC DT_Amount[3] =
		{ {NULL, (LPVOID) & DT_AmountStr[0]}, {NULL, (LPVOID) & DT_AmountStr[1]},
			{NULL, (LPVOID) & DT_AmountStr[2]} };

	LBCSHREP2params.Pkb									= 3;
	LBCSHREP2params.DepositTakenName		= (char **) DT_Name;
	LBCSHREP2params.DepositTakenAmount	= (tEXTERNAL_NUMERIC *) DT_Amount;

/*
	Zakumulowana warto�� kaucji pobranych.
*/
	LBCSHREP2params.DepositTakenTotal.ParamValuePtr			= (char *) STR_LBCSHREP2_DEP_T_TOTAL;
	LBCSHREP2params.DepositTakenTotal.TypeTranslFuncPtr	= NULL;

/*
	I jeszcze dwie kaucje zwr�cone.
*/
	const char DR_NameStr[2][16] =
		{ STR_LBCSHREP2_DEP_R_NAME_1, STR_LBCSHREP2_DEP_R_NAME_2 };

	const char * DR_Name[2] =
		{ DR_NameStr[0], DR_NameStr[1] };

	const char DR_AmountStr[2][8] =
		{ STR_LBCSHREP2_DEP_R_AMOUNT_1, STR_LBCSHREP2_DEP_R_AMOUNT_2 };

	const tEXTERNAL_NUMERIC DR_Amount[2] =
		{ {NULL, (LPVOID) & DR_AmountStr[0]}, {NULL, (LPVOID) & DR_AmountStr[1]} };

	LBCSHREP2params.Pkz										= 2;
	LBCSHREP2params.DepositReturnedName		= (char **) DR_Name;
	LBCSHREP2params.DepositReturnedAmount	= (tEXTERNAL_NUMERIC *) DR_Amount;

/*
	Zakumulowana warto�� kaucji zwr�conych.
*/
	LBCSHREP2params.DepositReturnedTotal.ParamValuePtr			= (char *) STR_LBCSHREP2_DEP_R_TOTAL;
	LBCSHREP2params.DepositReturnedTotal.TypeTranslFuncPtr	= NULL;

/*
	Got�wka - jest.
*/
	LBCSHREP2params.Pg												= 1;
	LBCSHREP2params.Cash.ParamValuePtr				= (char *) STR_LBCSHREP2_CASH;
	LBCSHREP2params.Cash.TypeTranslFuncPtr	= NULL;

/*
	Pozosta�e...
*/
	LBCSHREP2params.Shift			= STR_LBCSHREP2_SHIFT;
	LBCSHREP2params.CashRegNr	= STR_LBCSHREP2_CASH_REG_NR;
	LBCSHREP2params.Cashier		= STR_LBCSHREP2_CASHIER;
	LBCSHREP2params.Beginning	= STR_LBCSHREP2_BEGINNING;
	LBCSHREP2params.End				= STR_LBCSHREP2_END;

	LBCSHREP2params.Takings.ParamValuePtr			= STR_LBCSHREP2_TAKINGS;
	LBCSHREP2params.Takings.TypeTranslFuncPtr	= NULL;

	LBCSHREP2params.Expenditures.ParamValuePtr			= STR_LBCSHREP2_EXPENDITURES;
	LBCSHREP2params.Expenditures.TypeTranslFuncPtr	= NULL;

	LBCSHREP2params.SalesCash.ParamValuePtr			= STR_LBCSHREP2_SALES_CASH;
	LBCSHREP2params.SalesCash.TypeTranslFuncPtr	= NULL;

	LBCSHREP2params.PaymentIn.ParamValuePtr			= STR_LBCSHREP2_PAYMENT_IN;
	LBCSHREP2params.PaymentIn.TypeTranslFuncPtr	= NULL;

	LBCSHREP2params.PaymentOut.ParamValuePtr			= STR_LBCSHREP2_PAYMENT_OUT;
	LBCSHREP2params.PaymentOut.TypeTranslFuncPtr	= NULL;

	LBCSHREP2params.NrOfReceipts					= VAL_LBCSHREP2_RECEIPTS;
	LBCSHREP2params.NrOfCancelledReceipts	= VAL_LBCSHREP2_CANCELLED_RECEPIPTS;
	LBCSHREP2params.NrOfCancelledItems		= VAL_LBCSHREP2_CANCELLED_ITEMS;

	RetI = theApp.ThSL_LBCSHREP2( & LBCSHREP2params );
	if( RetI != SOK )
	{
		theApp.ThSL_ErrorMessage( "LBCSHREP2", RetI );
	}

}

/*----------------------------------------------------------------------------*/

/*
	Stary Thermal - usuwamy menu rapot�w i odczyt�w informacji z kasy
*/
void CMainFrame::RemoveExcessMenus()
{
	if( ! theApp.ThermalVersionNew )
	{
		CMenu * menu = CWnd::GetMenu();
		menu->DeleteMenu( 5, MF_BYPOSITION );
		menu->DeleteMenu( 4, MF_BYPOSITION );
	}
}