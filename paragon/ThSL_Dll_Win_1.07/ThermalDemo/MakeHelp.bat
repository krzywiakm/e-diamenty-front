@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by THERMALDEMO.HPJ. >"hlp\ThermalDemo.hm"
echo. >>"hlp\ThermalDemo.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\ThermalDemo.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\ThermalDemo.hm"
echo. >>"hlp\ThermalDemo.hm"
echo // Prompts (IDP_*) >>"hlp\ThermalDemo.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\ThermalDemo.hm"
echo. >>"hlp\ThermalDemo.hm"
echo // Resources (IDR_*) >>"hlp\ThermalDemo.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\ThermalDemo.hm"
echo. >>"hlp\ThermalDemo.hm"
echo // Dialogs (IDD_*) >>"hlp\ThermalDemo.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\ThermalDemo.hm"
echo. >>"hlp\ThermalDemo.hm"
echo // Frame Controls (IDW_*) >>"hlp\ThermalDemo.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\ThermalDemo.hm"
REM -- Make help for Project THERMALDEMO


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\ThermalDemo.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\ThermalDemo.hlp" goto :Error
if not exist "hlp\ThermalDemo.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\ThermalDemo.hlp" Debug
if exist Debug\nul copy "hlp\ThermalDemo.cnt" Debug
if exist Release\nul copy "hlp\ThermalDemo.hlp" Release
if exist Release\nul copy "hlp\ThermalDemo.cnt" Release
echo.
goto :done

:Error
echo hlp\ThermalDemo.hpj(1) : error: Problem encountered creating help file

:done
echo.
