// SnooperDialog.cpp : implementation file
//

#include "stdafx.h"
#include "ThermalDemo.h"
#include "SnooperDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CThermalDemoApp theApp;

#define MAX_SNOOP_LINES 500

/////////////////////////////////////////////////////////////////////////////
// CSnooperDialog dialog


CSnooperDialog::CSnooperDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CSnooperDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSnooperDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSnooperDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSnooperDialog)
	DDX_Control(pDX, IDC_LI_LOG, m_lb_Log);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSnooperDialog, CDialog)
	//{{AFX_MSG_MAP(CSnooperDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSnooperDialog message handlers


UINT Snooper_ThreadProc( LPVOID aParam )
{
	static CSnooperDialog * SnoopDlg;
	static BYTE InpBuffer[512];
	static DWORD NrOfBytesRead,
		NextSize,
		MsgCnt,
		i;

	SnoopDlg = (CSnooperDialog *) aParam;

	while( !SnoopDlg->ExitFlag )
	{
	  if( GetMailslotInfo(SnoopDlg->hMSlut, (LPDWORD) NULL, & NextSize, & MsgCnt, (LPDWORD) NULL) ) 
    {
	    if( NextSize != MAILSLOT_NO_MESSAGE ) 
		  {
		    while( MsgCnt )
				{
					GetMailslotInfo( SnoopDlg->hMSlut, (LPDWORD) NULL, & NextSize, & MsgCnt, (LPDWORD) NULL );

					if( NextSize != MAILSLOT_NO_MESSAGE )
					{
						_ASSERTE( NextSize <= sizeof(InpBuffer) );

						if( ReadFile(SnoopDlg->hMSlut, InpBuffer, sizeof(InpBuffer), & NrOfBytesRead, NULL) )
						{
							if( NrOfBytesRead < 3 )		/* CR czy inny LF jest przysy�any jako oddzielny komunikat */
							{													/* to zrobimy z niego pust� lini� */
								for( i = 0; i < NrOfBytesRead; i++ )
								{
									if( (InpBuffer[i] == 0x0A) || (InpBuffer[i] == 0x0D) )
									{
										InpBuffer[i] = 0x20;
									}
								}
							}

							InpBuffer[NrOfBytesRead] = 0;		/* ko�cz�ce zero (bo bufor nie jest czyszczony) */
																							/* a stringi s� przesy�ane bez zera */

							SnoopDlg->AddLine( (char *) InpBuffer );
						}
					}
				} 
			} 
		}
	}

	SnoopDlg->ExitFlag = FALSE;

	return 0;
}

void CSnooperDialog::AddLine( char * aString )
{
	m_lb_Log.InsertString( NrOfLines , aString );

/*
	Ograniczenie liczby pami�tanych pozycji - gdy osi�gniemy max, to po dodaniu nowego
	kasujemy pierwszy element z listy
*/
	if( NrOfLines >= MAX_SNOOP_LINES )
	{
		m_lb_Log.DeleteString( 0 );
	}
	else
	{
		NrOfLines++;
	}
}

BOOL CSnooperDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	NrOfLines = 0;
	hMSlut = theApp.hMailSlot;

	ExitFlag = FALSE;

	Thread = AfxBeginThread( & Snooper_ThreadProc, this );
	if( !Thread )
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}


void CSnooperDialog::OnOK() 
{
	DestroyWindow();
}

void CSnooperDialog::PostNcDestroy() 
{
/*
	Sygnalizujemy, �e trzeba zako�czy� w�tek - i czekamy na potwierdzenie.
*/
	ExitFlag = TRUE;
	while( ExitFlag );

	theApp.p_CSnooperDialog = NULL;
	delete this;
	
	CDialog::PostNcDestroy();
}
