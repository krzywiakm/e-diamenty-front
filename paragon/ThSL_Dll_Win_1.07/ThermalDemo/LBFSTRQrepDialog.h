#if !defined(AFX_LBFSTRQREPDIALOG_H__B5B691DA_96AD_45A4_B59D_D479105EEEE2__INCLUDED_)
#define AFX_LBFSTRQREPDIALOG_H__B5B691DA_96AD_45A4_B59D_D479105EEEE2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LBFSTRQrepDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLBFSTRQrepDialog dialog

class CLBFSTRQrepDialog : public CDialog
{
// Construction
public:
	CLBFSTRQrepDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLBFSTRQrepDialog)
	enum { IDD = IDD_LBFSTRQ_REP };
	CButton	m_bu_Cancel;
	BOOL	m_bf_Mode_Fiscal;
	BOOL	m_bf_Mode_Training;
	BOOL	m_bf_Mode_Transaction;
	BOOL	m_bf_PrevSeqErr;
	BOOL	m_bf_TRF;
	CString	m_str_Cash;
	CString	m_str_LastWrite;
	CString	m_str_NrOfRAMclr;
	CString	m_str_NrOfReceipts;
	CString	m_str_SerialNumber;
	CString	m_str_TotA;
	CString	m_str_TotB;
	CString	m_str_TotC;
	CString	m_str_TotD;
	CString	m_str_TotE;
	CString	m_str_TotF;
	CString	m_str_TotG;
	CString	m_str_VatA;
	CString	m_str_VatB;
	CString	m_str_VatC;
	CString	m_str_VatD;
	CString	m_str_VatE;
	CString	m_str_VatF;
	CString	m_str_VatG;
	CString	m_str_FiscalizationDate;
	CString	m_str_NrOfBlocked;
	CString	m_str_NrOfDailyRec;
	CString	m_str_NrOfFreeRec;
	CString	m_str_ReceiptA;
	CString	m_str_ReceiptB;
	CString	m_str_ReceiptC;
	CString	m_str_ReceiptD;
	CString	m_str_ReceiptE;
	CString	m_str_ReceiptF;
	CString	m_str_ReceiptG;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLBFSTRQrepDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLBFSTRQrepDialog)
	afx_msg void OnBKoniec();
	afx_msg void OnBUpdate();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LBFSTRQREPDIALOG_H__B5B691DA_96AD_45A4_B59D_D479105EEEE2__INCLUDED_)
