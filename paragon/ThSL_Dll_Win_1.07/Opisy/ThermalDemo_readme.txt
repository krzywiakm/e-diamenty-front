Wst�p:

Program ThermalDemo(.exe) umo�liwia przetestowanie najwa�niejszych funkcji udost�pnianych przez bibliotek� ThermalServiceLibrary(.dll). Program i biblioteka powinny znajdowa� si� w tym samym katalogu, ewentualnie biblioteka mo�e znajdowa� si� w katalogu systemowym.

Program ThermalDemo po uruchomieniu rozpoznaje typ urz�dzenia - stara czy nowa homologacja - i w zale�no�ci od typu inaczej s� obs�ugiwane pozycje menu (nowa homologacja - Sprzeda�, Zako�czenie transakcji - sekwencja LBTRXEND1, stara homologacja - LBTRXEND).

Uwaga: Program ThermalDemo wsp�pracuje tylko z urz�dzeniami pracuj�cymi z protoko�em "polskim".

Opis menu (w nawiasach kwadratowtych [] nazwy sekwencji):
---------------------------------------------------------------------------------
Ustawienia:
- Parametry komunikacji (port i pr�dko��)
- Zapis konfiguracji 
- Koniec (wyj�cie z programu)

Log. sekw. (pojawia si� okienko z zapisem przebiegu komunikacji, aktualizowanym na bie��co)

Programowanie:
- Nag��wek [LBSETHDR]
- Stawki VAT [LBSETPTU]

Sprzeda�:
- Rozpocz�cie transakcji [LBTRSHDR] (wyb�r transakcji blokowej b�d� on-line)
- Pozycja transakcji [LBTRSLN] (linijka paragonu, "Dodaj" wysy�a linijk� o kolejnym numerze, z akutualnie ustawionymi parametrami)
- Kaucje [LBDEP...]
- Zako�czenie transakcji ([LBTRXEND1/LBTRXEND] - nowa/stara hom.; przycisk "Anuluj" wysy�a rozkaz anulowania transakcji, przycisk "Zatwierd�" wysy�a rozkaz zatwierdzenia transakcji. Przyciski "Dodaj" i "Usu�" s�u�� do dodawania lub usuwania Form p�atno�ci i Kaucji, przy czym "Usu�" usuwa ostatni� pozycj�).

Raporty:
- Dobowy [LBDAYREP] (nie ma okienka, wybranie odpowiedniej pozycji uruchamia odpowiedni� sekwencj�):
	- Bez daty
	- Aktualna data
- Okresowy [LBFSKREP] (r�wnie� bez okienka)
	- Zakres: rekordy (wszystkie rekordy jakie s� w pami�ci)
	- Zakres: daty (od 2001-01-01 do bie��cej daty)
- Kasjera [LBCSHREP](warto�ci wpisane na sztywno)
- Formatki [LBCSHREP2](warto�ci wpisane na sztywno)

Statusy:
- D�ugi status drukarki [LBFSTRQ] (przycisk "Uaktualnij" rozpoczyna odpytywanie urz�dzenia)
- Odczyt pami�ci fiskalnej [LBFSTRQ] (poczynaj�c od rekordu o danym numerze b�d� od daty; przycisk "Pobierz" rozpoczyna odczyt pami�ci; je�li jakie� rekordy by�y w pami�ci, zostan� wypisane - dwukrotne klikni�cie na danej pozycji rozwija dany rekord)
---------------------------------------------------------------------------------

Podsumowuj�c:
Raporty maj� wpisane na sztywno warto�ci, pozosta�e pozycje maj� okienka dialogowe umo�liwiaj�ce podawanie parametr�w.


