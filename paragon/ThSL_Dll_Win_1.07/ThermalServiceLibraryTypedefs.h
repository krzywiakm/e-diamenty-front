/* Tab 2 */

#ifndef _THERMAL_SERVICE_LIBRARY_TYPEDEFS
#define _THERMAL_SERVICE_LIBRARY_TYPEDEFS

#include "ThermalServiceLibrary.h"

/*
	Rzeczy wsp�lne dla obu protoko��w
*/
typedef int	( *tTHSL_PORT_OPEN )			      ( char *, DWORD );
typedef int	( *tTHSL_PORT_SET_BAUD_RATE )		      ( DWORD );
typedef void	( *tTHSL_PORT_CLOSE )			      ( void );

typedef int	( *tTHSL_PORT_LOGGING_START )		      ( char * );
typedef	int	( *tTHSL_PORT_LOGGING_STOP )		      ();

typedef DWORD	( *tTHSL_GET_DEVICE_ERROR_CODE )	      ();
typedef void	( *tTHSL_ERROR_MESSAGE )		      ( char *, int );

typedef void	( *tTHSL_SEND_ACK )			      ();
typedef void	( *tTHSL_SEND_NACK )			      ();

typedef int	( *tTHSL_SET_TIMEOUT_DIALOG_FUNC )	      ( tTIMEOUT_DLG_FUNC * );

/*
	Rozkazy protoko�u starego (polskiego)
*/
typedef int		( *tTHSL_GET_STATUS_CASH_REGISTER )   ( tCASH_REGISTER_STATUS * );
typedef int		( *tTHSL_GET_STATUS_PRINTER )	      ( tPRINTER_STATUS * );

typedef int		( *tTHSL_LBSETCK )		      ( tLBSETCK_PARAMS * );
typedef int		( *tTHSL_LBDSP )		      ( BYTE, char * );
typedef int		( *tTHSL_LBSETPTU )		      ( tLBSETPTU_PARAMS * );
typedef int		( *tTHSL_LBSETHDR )		      ( char *, char *, char * );
typedef int		( *tTHSL_LBFEED )		      ( BYTE );
typedef int		( *tTHSL_LBSERM )		      ( BYTE );
typedef int		( *tTHSL_LBTRSHDR )		      ( BYTE );
typedef int		( *tTHSL_LBTRSLN )		      ( tLBTRSLN_PARAMS * );
typedef int		( *tTHSL_O_LBTRSLN )		      ( tLBTRSLN_PARAMS * );
typedef int		( *tTHSL_LBDEP_P )		      ( tLBDEP_PARAMS * );
typedef int		( *tTHSL_LBDEPSTR_P )		      ( tLBDEP_PARAMS * );
typedef int		( *tTHSL_LBDEP_M )		      ( tLBDEP_PARAMS * );
typedef int		( *tTHSL_LBDEPSTR_M )		      ( tLBDEP_PARAMS * );
typedef int		( *tTHSL_LBTREXITCAN )		      ( char *, char * );
typedef int		( *tTHSL_O_LBTREXIT )		      ( );
typedef int		( *tTHSL_LBTREXIT )		      ( tLBTREXIT_PARAMS * );
typedef int		( *tTHSL_O_LBTRXEND )		      ( tO_LBTRXEND_PARAMS * );
typedef int		( *tTHSL_LBTRFORMPLAT )		      ( tLBTRFORMPLAT_PARAMS * );
typedef int		( *tTHSL_LBTRXEND1 )		      ( tLBTRXEND1_PARAMS * );

typedef int		( *tTHSL_LBCSHREP2 )		      ( tLBCSHREP2_PARAMS * );
typedef int		( *tTHSL_LBTRSCARD )		      ( tLBCARD_PARAMS * );
typedef int		( *tTHSL_LBSTOCARD )		      ( tLBCARD_PARAMS * );
typedef int		( *tTHSL_LBINCCSH )		      ( tLBCSH_PARAMS * );
typedef int		( *tTHSL_LBDECCSH )		      ( tLBCSH_PARAMS * );
typedef int		( *tTHSL_LBCSHSTS )		      ( char *, char * );
typedef int		( *tTHSL_LBCSHREP )		      ( tLBCSHREP_PARAMS * );
typedef int		( *tTHSL_LBLOGIN )		      ( tLBLOG_PARAMS * );
typedef int		( *tTHSL_LBLOGOUT )		      ( tLBLOG_PARAMS * );
typedef int		( *tTHSL_LBFSKREP_D )		      ( tLBFSKREP_D_PARAMS * );
typedef int		( *tTHSL_LBFSKREP_R )		      ( tLBFSKREP_R_PARAMS * );
typedef int		( *tTHSL_LBDAYREP )		      ( tLBDAYREP_PARAMS * );

typedef int		( *tTHSL_LBDBREPRS )		      ( char *, char, tLBDBREPRS_RESPONSE * );
typedef int		( *tTHSL_LBSENDCK )		      ( tLBSENDCK_RESPONSE * );
typedef int		( *tTHSL_LBFSTRQ )		      ( tLBFSTRS1_RESPONSE * );
typedef int		( *tTHSL_O_LBFSTRQ )		      ( BYTE *, tO_LBFSTRS_RESPONSE * );
typedef int		( *tTHSL_LBERNRQ )		      ( BYTE * );
typedef int		( *tTHSL_LBIDRQ )		      ( tLBIDRQ_RESPONSE * );
typedef int		( *tTHSL_LBSNDMD )		      ( BYTE );
typedef int		( *tTHSL_LBCASREP )		      ();

typedef int		( *tTHSL_LBFSTRQ24 )		      ( tLBFSTRQ1_RESPONSE * );
typedef int		( *tTHSL_LBFSTRQ25 )		      ( tLBFSTRQ25_PARAMS * );
typedef int		( *tTHSL_LBFSTRQ26 )		      ( DWORD );
typedef int		( *tTHSL_LBFSTRQ27 )		      ( tLBFSTRQ27_RESPONSE *, BYTE * );


/*
		Rozkazy protoko�u w�oskiego
*/
typedef int		( *tTHSL_I_GET_STATUS_CASH_REGISTER )	( tI_CASH_REGISTER_STATUS *);
typedef int		( *tTHSL_I_GET_STATUS_PRINTER )				( tI_PRINTER_STATUS *);

typedef int		( *tTHSL_I_LOGIN )										( tLOGIN_PARAMS * );
typedef int		( *tTHSL_I_LOGOUT )										();

typedef int		( *tTHSL_I_SETOPT )										( tSETOPT_PARAMS * );

typedef int		( *tTHSL_I_SETVAT )										( tSETVAT_PARAMS * );

typedef int		( *tTHSL_I_TRSHDR )										( tTRSHDR_PARAMS * );
typedef int		( *tTHSL_I_TRSLNE )										( tTRSLNE_PARAMS * );
typedef int		( *tTHSL_I_TRSEND )										( tTRSEND_PARAMS * );
typedef int		( *tTHSL_I_TRSEXIT )									();

typedef int		( *tTHSL_I_RECGET )										( BYTE, DWORD, tRECGET_RESPONSE * );
typedef int		( *tTHSL_I_RECADD )										( tRECADD_PARAMS * );

typedef int		( *tTHSL_I_GETSTS )										( tGETSTS_RESPONSE * );
typedef int		( *tTHSL_I_GETTOT )										( BOOL, tGETTOT_RESPONSE * );

typedef int		( *tTHSL_I_GET_MESSAGE )							( BOOL, BYTE *, BYTE ** );

typedef int		( *tTHSL_I_S_TRSITEM )								( tS_TRSITEM_PARAMS * );
typedef int		( *tTHSL_I_S_TRSDSC )									( tS_TRSDSC_PARAMS * );


#endif // _THERMAL_SERVICE_LIBRARY_TYPEDEFS