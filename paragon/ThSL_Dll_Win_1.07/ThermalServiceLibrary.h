/* Tab 2 */

/*
	ThermalServiceLibrary.h
	plik nag��wkowy dla program�w korzystaj�cych z ThermalServiceLibrary.dll
*/

#ifndef _THERMAL_SERVICE_LIBRARY
#define _THERMAL_SERVICE_LIBRARY

/*
	Wy��czenie ostrze�enia 4786 (zbyt d�ugi identyfikator) spowodowanego  u�yciem:
	vector<string>
*/
#pragma warning( disable : 4786 )

/*----------------------------------------------------------------------------*/

#include <crtdbg.h>
#include <list>
#include <vector>
#include "winbase.h"

#include "Protocol.h"
#include "ErrorCodes.h"

/*----------------------------------------------------------------------------*/

using namespace std ;

/*----------------------------------------------------------------------------*/

/*
	Wektor string�w; wektor to w sumie lista z mo�liwo�ci�
	bezpo�redniego dost�pu po indeksach
*/
typedef vector<string> tVectorOfStrings;

/*
	Wektor bajt�w i podw�jnych s��w (16 bit) - skoro ju� jest wektor string�w.
	(mo�na by zastosowa� tablic�...)
*/
typedef vector<BYTE>	tVectorOfBytes;

typedef vector<DWORD>	tVectorOfDwords;

/*
	Definicja typu funkcji konwertuj�cej warto�� w postaci zewn�trznej na string:
	- LPVOID  wska�nik do warto�ci (siedz�cej w pami�ci w jakiej� tam postaci)
	-	char *  wska�nik do bufora wyj�ciowego (tam zostanie wpisany)
	Zwraca� powinna: SOK je�li uda�o si�, co� innego w przypadku b��du
*/
typedef int ( *tTYPE_TRANSL_FUNC )( LPVOID, char * ); 

/*
	Definicja typu funkcji (zewn�trznej) obs�uguj�cej okno dialogowe zapytania
	o dalsze kroki w przypadku wyst�pienia Timeout-u.
	Funkcja powinna zwraca�:
		IDYES			- je�li u�ytkownik chce jeszcze czeka� na odbi�r
		IDNO			- je�li chce ponownie wys�a� ramk� i czeka� na odbi�r
		IDCANCEL, lub co� innego - je�li operacja ma zosta� przerwana
	(standardowy MessageBox ma dost�pny styl MB_YESNOCANCEL )
*/
typedef int ( *tTIMEOUT_DLG_FUNC )();

/*
	Struktura, kt�ra s�u�y do budowania tablicy argument�w - przekazywanych do 
	niekt�rych funkcji
*/
typedef struct
{
  tTYPE_TRANSL_FUNC TypeTranslFuncPtr;	/* adres funkcji konwertuj�cej */
	LPVOID ParamValuePtr;									/* adres bufora */
} tEXTERNAL_NUMERIC;

/*
	Struktura s�u��ca do opisu parametru - typu i w�a�ciwo�ci, kt�re mo�na
	sprawdza� automatycznie (d�ugo�� itp).
*/
typedef struct 
{
	BYTE		FieldType;  	/* rodzaj pola i ewentualny terminator */
	BYTE		MaxStrLen;  	/* maks. dozwolona d�ugo�� tekstu */
	DWORD		R1;														/* R1 i R2 - znaczenie zale�ne od typu */
	DWORD		R2;
} tPARAMETER_DESCRIPTION;

/*
	Struktura, kt�ra wykorzystywana jest do opisu odpowiedzi
*/
typedef struct
{
	BYTE *	Description;			/* adres tablicy opisuj�cej typy p�l danych */
	DWORD		DescriptionLen;		/* d�ugo�� powy�szej tablicy */
	BYTE *	FieldsStruct;			/* adres struktury odpowiedzi */
} tRESPONSE;

/*----------------------------------------------------------------------------*/

/*
	Maski wyznaczaj�ce wewn�trz bajtu cz�ci odpowiadaj�ce rodzajowi pola i rodzajowi separatora/terminatora.
	Rodzaj sep./term. u�ywany jest jedynie w polskiej wersji protoko�u - i dotyczy tylko tych p�l, kt�re
	maj� zamienione separatory (0 oznacza, �e wyst�puje zgodno�� z rodzajem pola, pozosta�e warto�ci -> rodzaj sep.).
*/
#define FLD_TYPE_MASK		(BYTE) 0x0F
#define FLD_SEP_MASK		(BYTE) 0xF0

/*
	Modyfikatory rodzaju pola - rodzaje separator�w (to dotyczy raczej protoko�u polskiego,
	gdzie jest niez�a kaszana)
	FLD_SEP_ESC - czasami nie ma terminatora przy ostatnim parametrze - ale jest albo znacznik
	ko�ca, albo jest wpisywany ESC na miejsce pierwszego bajtu sumy kontrolnej
*/
#define FLD_SEP_BYT			(BYTE) 0x10
#define FLD_SEP_NUM			(BYTE) 0x20
#define FLD_SEP_STR			(BYTE) 0x30
#define FLD_SEP_LIN			(BYTE) 0x40
#define FLD_SEP_ESC			(BYTE) 0xF0

/*
	Rodzaje p�l w strukturach parametr�w rozkazu / danych zwracanych.
	
	W przypadku danych zwracanych w polskim protokole jest problem z rodzajami p�l - bo
	s� stosowane separatory (protok� w�oski - terminatory) i nie wszystkie s� zgodne z 
	zawarto�ci� pola. I jest problem z ostatnim polem. 
*/

/* bajt */
#define	FLD_BYT					(BYTE) 0

/* tablica bajt�w */ 
#define	FLD_BYT_TAB			(BYTE) 1

/* warto�� numeryczna DWORD */
#define	FLD_NUM					(BYTE) 2

/* tablica warto�ci numerycznych DWORD */
#define	FLD_NUM_TAB			(BYTE) 3

/* warto�� numeryczna w formacie zewn�trznym -> konwersja */
#define	FLD_NUM_EXT			(BYTE) 4

/* tablica warto�ci numerycznych w formacie zewn�trznym */
#define	FLD_NUM_EXT_TAB	(BYTE) 5

/* tekst */
#define	FLD_STR					(BYTE) 6

/* tablica tekst�w */
#define	FLD_STR_TAB			(BYTE) 7

/* tekst d�ugi (mo�e by� wi�cej linii) */
#define	FLD_LIN					(BYTE) 8

/* tablica tekst�w d�ugich */
#define	FLD_LIN_TAB			(BYTE) 9

/*
	w wersji polskiej - ostatnie pole odpowiedzi (brak separatora - w ten spos�b nie sprawdzi si�
	zawarto�ci pola...
*/
#define FLD_UNK					(BYTE) 10

/* ostatni element - do cel�w kontrolnych */
#define	FLD_DEF_MAX			(BYTE) 11	



/*
	Separatory p�l (wersja polska) / terminatory (wersja w�oska):
	BYTe, NUMeric, STRing, LINe

	NUM - pole mo�e by� typu DWORD lub string zawieraj�cy warto�� liczbow�

	SEP_END dotyczy ramek odebranych i nie wyst�puje we w�oskim protokole 
	- bo tam s� w�a�ciwie termnatory a nie separatory	jak w wersji polskiej
*/

#define SEP_BYT			(BYTE) ';'
#define SEP_NUM			(BYTE) '/'
#define SEP_STR			(BYTE) 0x0D
#define SEP_LIN			(BYTE) 0xFF
#define SEP_ESC			ESC

#define SEP_BYT_IT	(BYTE) 0xFE
#define SEP_NUM_IT	(BYTE) 0x09
#define SEP_STR_IT	(BYTE) 0x0A
#define SEP_LIN_IT	(BYTE) 0xFF


/* taka warto�� d�ugo�ci oznacza, �e nie nale�y generowa� tego pola */
#define STR_EMPTY		-1

/*
	Wersja w�oska (w polskiej nie ma odpowiednika).
	Dobrze, �e cho� brak rabatu/narzutu jest tak samo kodowany we wszystkich rozkazach
	obu protoko��w 
*/
#define DISCOUNT_NONE	(BYTE) 0

#define DB_DEPARTMENT	(BYTE) 0
#define DB_HOT_KEYS		(BYTE) 1
#define DB_CASHIER		(BYTE) 2
#define DB_FORMS			(BYTE) 3
#define DB_DISCOUNT		(BYTE) 4
#define DB_PLU				(BYTE) 5
#define DB_BARCODE		(BYTE) 6

#define DB_REC_NO_REPLACEMENT	(BYTE) 0
#define DB_REC_REPLACE (BYTE) 1

#define	HW_TYPE_ECR				(BYTE) 0
#define HW_TYPE_BINGO			(BYTE) 1
#define HW_TYPE_THERMAL		(BYTE) 2

#define FSC_MODE_NON_FISCAL	(BYTE) 0
#define FSC_MODE_FISCAL			(BYTE) 1
#define FSC_MODE_EURO				(BYTE) 2


/*
	Wersja polska - rodzaje ramek zwracanych przez LBFSTRQ z parametrem Ps r�wnym 27
*/
#define LBFSTRQ27_DAILY_REPORT			(BYTE) 10
#define LBFSTRQ27_VAT_CHANGE				(BYTE) 11
#define LBFSTRQ27_RAM_CLEAR					(BYTE) 12
#define LBFSTRQ27_SALE_AFTER_CLEAR	(BYTE) 13
#define LBFSTRQ27_NO_MORE_RECORDS		(BYTE) 25

/*
	Wersja polska - LBTRSLN, rodzaje rabatu 
*/
#define LBTRSLN_NO_DISCNT			0
#define LBTRSLN_DISCNT_PERCNT	2
#define LBTRSLN_DISCNT_AMT		1
#define LBTRSLN_SURCH_PERCNT	4
#define LBTRSLN_SURCH_AMT			3


#define LBTRXEND1_MAX_FP_ITEMS	5
#define LBTRXEND1_MAX_ADD_LINES	3
#define LBTRXEND_MAX_ADD_LINES	5

/*----------------------------------------------------------------------------*/

/* Enumeracja: mo�liwe stany logowania sekwencji */
enum LOGGING_STATES
{
	LOGGING_INACTIVE	= 0,	/* wy��czone */
	LOGGING_ACTIVE		= 1,	/* w��czone  */
	LOGGING_ENUM_LAST	= 2		/* ostatni element - do cel�w kontrolnych */
};

enum LOG_OPERATION
{
	LOG_WRITE	= 0,
	LOG_READ	= 1
};

/*----------------------------------------------------------------------------*/

#define SNOOP_MAILSLOT_NAME "\\\\.\\mailslot\\ThSL_MailSlot"

/*----------------------------------------------------------------------------*/

/*
	Cash Register Status
*/
typedef struct
{
	BOOL	EUR;
	BOOL	FSK;
	BOOL	CMD;
	BOOL	PAR;
	BOOL	SUP;
} tI_CASH_REGISTER_STATUS;


typedef struct
{
	BOOL	FSK;
	BOOL	CMD;
	BOOL	PAR;
	BOOL	TRF;
} tCASH_REGISTER_STATUS;


/*
	Printer Status
*/
typedef struct
{
	BOOL	POW;
	BOOL	LVR;
	BOOL	PAP;
	BOOL	ERR;
} tI_PRINTER_STATUS;

typedef struct
{
	BOOL	ONL;
	BOOL	PE;
	BOOL	ERR;
} tPRINTER_STATUS;

/********************************* KODY ROZKAZ�W ******************************/

/*
	W�oskie
*/

#define LOGIN_CMD_STR			"*i"
#define LOGOUT_CMD_STR		"*q"

#define SETOPT_CMD_STR		"#y"
#define SETVAT_CMD_STR		"#v"
#define RECADD_CMD_STR		"#r"

#define TRSHDR_CMD_STR		"*t"
#define TRSLNE_CMD_STR		"*l"
#define TRSEND_CMD_STR		"*e"
#define TRSEXIT_CMD_STR		"*x"

#define GETSTS_CMD_STR		"$s"
#define GETTOT_CMD_STR		"$t"
#define RECGET_CMD_STR		"$r"

#define S_TRSITEM_CMD_STR	"*b"
#define S_TRSDSC_CMD_STR	"*s"

#define SBHDR_CMD_STR			"*H"
#define SBITEM_CMD_STR		"*B"
#define SBLINE_CMD_STR		"*S"
#define SBDSC_CMD_STR			"*C"
#define SBEND_CMD_STR			"*E"
#define SBEXIT_CMD_STR		"*X"

#define MESSAGE_SBHDR			(BYTE) 0
#define MESSAGE_SBITEM		(BYTE) 1
#define MESSAGE_SBLINE		(BYTE) 2
#define MESSAGE_SBDSC			(BYTE) 3
#define MESSAGE_SBEND			(BYTE) 4
#define MESSAGE_SBEXIT		(BYTE) 5

/*
	Polskie
*/
#define CMD_STR_START					"#$"

#define LBSETCK_CMD_STR				"$c"
#define LBDSP_CMD_STR					"$d"
#define LBSETPTU_CMD_STR			"$p"
#define LBSETHDR_CMD_STR			"$f"
#define LBFEED_CMD_STR				"#l"
#define LBSERM_CMD_STR				"#e"
#define LBTRSHDR_CMD_STR			"$h"
#define LBTRSLN_CMD_STR				"$l"
#define LBDEP_CMD_STR					"$d"
#define LBDEP_MODE_P					 6
#define LBDEP_MODE_STRP				 7
#define LBDEP_MODE_M					10
#define LBDEP_MODE_STRM				11
#define LBTREXITCAN_CMD_STR		"$e"
#define LBTREXIT_CMD_STR			LBTREXITCAN_CMD_STR

#define LBTRXEND_CMD_STR			"$x"
#define LBTRFORMPLAT_CMD_STR	"$b"
#define LBTRXEND1_CMD_STR			"$y"
#define LBCSHREP1_CMD_STR			"#f"
#define LBCSHREP2_CMD_STR			"#m"
#define LBTRSCARD_CMD_STR			"#g"
#define LBSTOCARD_CMD_STR			"#h"
#define LBINCCSH_CMD_STR			"#i"
#define LBDECCSH_CMD_STR			"#d"
#define LBCSHSTS_CMD_STR			"#t"
#define LBCSHREP_CMD_STR			"#k"
#define LBLOGIN_CMD_STR				"#p"
#define LBLOGOUT_CMD_STR			"#q"
#define LBFSKREP_CMD_STR			"#o"
#define LBDAYREP_CMD_STR			"#r"
#define LBDBREP_CMD_STR				"#b"
#define LBDBREPRS_CMD_STR			"$g"
#define LBSENDCK_CMD_STR			"#c"
#define LBFSTRQ_CMD_STR				"#s"
#define LBERNRQ_CMD_STR				"#n"
#define LBIDRQ_CMD_STR				"#v"
#define LBSNDMD_CMD_STR				"#a"
#define LBCASREP_CMD_STR			"#j"

#define O_LBSNDMD_CMD_STR			"#x"
#define O_LBOPAK_CMD_STR			"#w"
#define O_LBSTOCSH_CMD_STR		"#z"


/************* STRUKTURY ROZKAZOWE I ZWROTNE - PROTOKӣ W�OSKI ****************/

typedef struct
{
	char *						Name;
	tEXTERNAL_NUMERIC	Price;
	tEXTERNAL_NUMERIC	Amt;
	BYTE							Vat;
	BYTE							PrFxd;
	BYTE							CprMin;
	BYTE							CprMax;
	tEXTERNAL_NUMERIC	PrMin;
	tEXTERNAL_NUMERIC	PrMax;
} tDEPARTMENT_DB_RECORD;

typedef struct
{
	char *	Name;
	char *	Price;
	char *	Amt;
	BYTE		Vat;
	BYTE		PrFxd;
	BYTE		CprMin;
	BYTE		CprMax;
	char *	PrMin;
	char *	PrMax;
} tRECGET_DEPARTMENT_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef struct
{
	char *						Name;
	BYTE							Type;
	tEXTERNAL_NUMERIC	Amount;
} tFORMS_OF_PAYMENT_DB_RECORD;

typedef struct
{
	char *	Name;
	BYTE		Type;
	char *	Amount;
} tRECGET_FORMS_OF_PAYMENT_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef struct
{
	char *						Name;
	tEXTERNAL_NUMERIC	Value;
	BYTE							Type;
} tDISCOUNT_DB_RECORD;

typedef struct
{
	char *	Name;
	char *	Value;
	BYTE		Type;
} tRECGET_DISCOUNT_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef struct
{
	char *						Name;
	char *						Barcode;
	tEXTERNAL_NUMERIC	Price;
	tEXTERNAL_NUMERIC	Quantity;
	BYTE							Dpmt;
	BYTE							Vat;
	BYTE							PrFxd;
	BYTE							CprMin;
	BYTE							CprMax;
} tPLU_DB_RECORD;

typedef struct
{
	char *	Name;
	char *	Barcode;
	char *	Price;
	char *	Quantity;
	BYTE		Dpmt;
	BYTE		Vat;
	BYTE		PrFxd;
	BYTE		CprMin;
	BYTE		CprMax;
} tRECGET_PLU_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef struct
{
	char *		BcFmt;
} tBARCODE_FMT_DB_RECORD;

typedef tBARCODE_FMT_DB_RECORD tRECGET_BARCODE_FMT_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef struct
{
	char *							Name;
	char *							Password;
	tEXTERNAL_NUMERIC		CancCnt;
	tEXTERNAL_NUMERIC		Turnover;
	tEXTERNAL_NUMERIC		RetAmt;
	tEXTERNAL_NUMERIC		CancAmt;
	tEXTERNAL_NUMERIC		CashIn;
	tEXTERNAL_NUMERIC		CashOut;
	tEXTERNAL_NUMERIC		Fp1;
	tEXTERNAL_NUMERIC		Fp2;
	tEXTERNAL_NUMERIC		Fp3;
	tEXTERNAL_NUMERIC		Fp4;
	tEXTERNAL_NUMERIC		Tkngs;
	tEXTERNAL_NUMERIC		Dsc1;
	tEXTERNAL_NUMERIC		Dsc2;
	tEXTERNAL_NUMERIC		Dsc3;
	tEXTERNAL_NUMERIC		Dsc4;
	tEXTERNAL_NUMERIC		Dsc5;
	tEXTERNAL_NUMERIC		Dsc6;
	tEXTERNAL_NUMERIC		DscF;
	tEXTERNAL_NUMERIC		SrchF;
	tEXTERNAL_NUMERIC		RcptCnt;
	tEXTERNAL_NUMERIC		Time;
} tCASHIER_DB_RECORD;

typedef struct
{
	char *	Name;
	char *	Password;
	char *	CancCnt;
	char *	Turnover;
	char *	RetAmt;
	char *	CancAmt;
	char *	CashIn;
	char *	CashOut;
	char *	Fp1;
	char *	Fp2;
	char *	Fp3;
	char *	Fp4;
	char *	Tkngs;
	char *	Dsc1;
	char *	Dsc2;
	char *	Dsc3;
	char *	Dsc4;
	char *	Dsc5;
	char *	Dsc6;
	char *	DscF;
	char *	SrchF;
	char *	RcptCnt;
	char *	Time;
} tRECGET_CASHIER_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef union DB_RECORD
{
	tDEPARTMENT_DB_RECORD				Department;
	tFORMS_OF_PAYMENT_DB_RECORD	FormsOfPayment;
	tDISCOUNT_DB_RECORD					Discount;
	tPLU_DB_RECORD							PLU;
	tBARCODE_FMT_DB_RECORD			BarcodeFmt;
	tCASHIER_DB_RECORD					Cashier;
} tDB_RECORD;

typedef union RECGET_RECORD
{
	tRECGET_DEPARTMENT_RESPONSE				Department;
	tRECGET_FORMS_OF_PAYMENT_RESPONSE	FormsOfPayment;
	tRECGET_DISCOUNT_RESPONSE					Discount;
	tRECGET_PLU_RESPONSE							PLU;
	tRECGET_BARCODE_FMT_RESPONSE			BarcodeFmt;
	tRECGET_CASHIER_RESPONSE					Cashier;
} tRECGET_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE		Database;
	DWORD		RecNr;
} tRECGET_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	char *						Login;
} tLOGIN_PARAMS;

/*----------------------------------------------------------------------------*/

/*
	SETOPT -  WERSJA UPROSZCZONA !!!
*/
typedef struct
{
	BYTE				Option;
	BYTE				Flag;
} tSETOPT_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	tEXTERNAL_NUMERIC		VatA;
	tEXTERNAL_NUMERIC		VatB;
	tEXTERNAL_NUMERIC		VatC;
	tEXTERNAL_NUMERIC		VatD;
	tEXTERNAL_NUMERIC		VatE;
	tEXTERNAL_NUMERIC		VatF;
	tEXTERNAL_NUMERIC		VatG;
} tSETVAT_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	DWORD		NrOfHdrLines;
	char **	HdrLines;
	DWORD		NrOfHdrFlags;
	BYTE *	HdrFlags;
} tSETHDR_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	DWORD		NrOfAddHdrLines;
	char **	AddHdrLines;
	DWORD		NrOfAddHdrFlags;
	BYTE *	AddHdrFlags;
} tSETADDHDR_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	DWORD		NrOfAddFtrLines;
	char **	AddFtrLines;
	DWORD		NrOfAddFtrFlags;
	BYTE *	AddFtrFlags;
} tSETADDFTR_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	char *	OldPassword;
	char *	NewPassword;
} tSETPWD_PARAMS;

/*----------------------------------------------------------------------------*/
/*
	RECADD - zawiera uni� (DatabaseRecord)
*/
typedef struct
{
	BYTE				Database;
	BYTE				ModFlag;
	DWORD				RecNr;
	tDB_RECORD	DatabaseRecord;
} tRECADD_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE	GrId;
	BYTE	Flag;
} tGRSET_PARAMS ;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE	Lines;
	BYTE	LineWidth;
	BYTE	GrId;
} tGRSTART_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE		LineNo;
	char *	Data;
} tGRDATA_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	char *	FiscalCode;
} tTRSHDR_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE							Oper;
	BYTE							Flag;
	DWORD							Plu;
	char *						Name;
	tEXTERNAL_NUMERIC	Price;
	tEXTERNAL_NUMERIC	Quantity;
	BYTE							DscNo;
	tEXTERNAL_NUMERIC	DscAmt;
} tTRSLNE_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE							DscNo;
	tEXTERNAL_NUMERIC	DscAmt;
	char *						Name;
} tTRSDSC_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	tEXTERNAL_NUMERIC	Cash;
	BYTE							DscNo;
	tEXTERNAL_NUMERIC	DscAmt;
	tEXTERNAL_NUMERIC	Fp1;
	tEXTERNAL_NUMERIC	Fp2;
	tEXTERNAL_NUMERIC	Fp3;
	tEXTERNAL_NUMERIC	Fp4;
} tTRSEND_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE		HwType;
	BYTE		FscMode;
	BYTE		Tot;
	BYTE		F_Year;
	BYTE		F_Month;
	BYTE		F_Day;
	char *	Tin;
	char *	FmNr;
	char *	Password;
	char *	Version;
	DWORD		DbDep;
	DWORD		DbHk;
	DWORD		DbCsh;
	DWORD		DbFp;
	DWORD		DbDsc;
	DWORD		DbPlu;
	DWORD		DbBf;
} tGETSTS_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef struct
{
	char *	TotA;
	char *	TotB;
	char *	TotC;
	char *	TotD;
	char *	TotE;
	char *	TotF;
	char *	TotG;
} tGETTOT_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE								Flag;
	DWORD								PluDep;
	char *							Name;
	tEXTERNAL_NUMERIC		Price;
} tS_TRSITEM_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE								DscNo;
	tEXTERNAL_NUMERIC		DscAmt;
} tS_TRSDSC_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE	CshNo;
} tSBHDR_MESSAGE;

typedef struct
{
	char *	Barcode;
}	tSBITEM_MESSAGE;

typedef struct
{
	BYTE		Oper;
	BYTE		Flag;
	DWORD		PluDep;
	char *	Price;
	char *	Quantity;
} tSBLINE_MESSAGE;

typedef struct
{
	BYTE		DscNo;
	char *	DscAmt;
} tSBDSC_MESSAGE;

typedef struct
{
	char *	Total;
	char *	Cash;
	BYTE		DscNo;
	char *	DscAmt;
	char *	Fp1;
	char *	Fp2;
	char *	Fp3;
	char *	Fp4;
} tSBEND_MESSAGE;

/************* STRUKTURY ROZKAZOWE I ZWROTNE - PROTOKӣ POLSKI ****************/

typedef struct
{
	BYTE		Py;
	BYTE		Pm;
	BYTE		Pd;
	BYTE		Ph;
	BYTE		Pmn;
	BYTE		Ps;
	char *	CashRegNr;
	char *	Cashier;
} tLBSETCK_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE		Ps;
	char *	String;
} tLBDSP_PARAMS;

/*----------------------------------------------------------------------------*/

/*
	LBSETPTU - struktura dla generatora ramek, umo�liwiaj�ca realizacj� opcjonalnych 
	parametr�w: Py, Pm, Pd. CashRegNr i Cashier musz� by� OBA albo obecne albo nie (==NULL)

	(VatRates - EXTERNAL_NUMERIC ze wzgl�du na separator)
*/
typedef struct
{
	BYTE								Ps;
	DWORD								_Date;
	BYTE *							_DateTab;
	DWORD								Ps_2;
	tEXTERNAL_NUMERIC *	VatRates;
	char *							CashRegNr;
	char *							Cashier;
} tLBSETPTU_PARAMS_INT;

typedef struct
{
	BYTE								Ps;
	BYTE								_Date;
	BYTE								Py;
	BYTE								Pm;
	BYTE								Pd;
	tEXTERNAL_NUMERIC *	VatRates;
	char *							CashRegNr;
	char *							Cashier;
} tLBSETPTU_PARAMS;

/*----------------------------------------------------------------------------*/

/*
	LBSETHDR - struktura dla generatora ramek (do rozkazu z DLL-a parametry przekazywane s�
	wprost - bo s� tylko trzy)
*/
typedef struct
{
	BYTE		_dummy;
	char *	HdrString;
	char *	CashRegNr;
	char *	Cashier;
} tLBSETHDR_PARAMS_INT;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE		Ps;
} tLBFEED_PARAMS;

/*----------------------------------------------------------------------------*/

/*
	LBSERM - struktura dla generatora ramek (funkcja b�dzie otrzymywa� paramer bezpo�rednio - jako argument )
*/
typedef struct
{
	BYTE	Ps;
} tLBSERM_PARAMS_INT;

/*----------------------------------------------------------------------------*/

/*
	LBTRSHDR - do funkcji nie b�dziemy przekazywa� struktury (bo to tylko jeden bajt), natomiast potrzebna
	jest ona generatorowi ramek
*/
typedef struct
{
	BYTE	Pl;
} tLBTRSHDR_PARAMS_INT;

// Stara homologacja - z dodatkowymi liniami

typedef struct
{
	BYTE		Pl;
	BYTE		Pn;
	char *	Line1;
	char *	Line2;
	char *	Line3;
} tLBTRSHDR_A_PARAMS;

/*----------------------------------------------------------------------------*/

/*
	LBTRSLN - struktura dla generatora ramek (pole Ptu/Vat ma niestandardowy terminator )
*/
typedef struct 
{
	BYTE								Pi;
	BYTE								Pr;
	char *							Name;
	tEXTERNAL_NUMERIC		Quantity;
	char *							Vat;
	tEXTERNAL_NUMERIC		Price;
	tEXTERNAL_NUMERIC		Gross;
	tEXTERNAL_NUMERIC		Discount;
} tLBTRSLN_PARAMS_INT;

/*
	LBTRSLN - parametry
*/
typedef struct 
{
	BYTE								Pi;
	BYTE								Pr;
	char *							Name;
	tEXTERNAL_NUMERIC		Quantity;
	char								Vat;
	tEXTERNAL_NUMERIC		Price;
	tEXTERNAL_NUMERIC		Gross;
	tEXTERNAL_NUMERIC		Discount;
} tLBTRSLN_PARAMS;

/*----------------------------------------------------------------------------*/

/*
	LBDEP+, LBDEPSTR+, LBDEP-, LBDEPSTR-
	Te rozkazy maj� identyczn� konstrukcj� - wyb�r dokonywany jest parametrem Mode.
*/
typedef struct
{
	BYTE								Mode;
	tEXTERNAL_NUMERIC		Amount;
	char *							Number;
	tEXTERNAL_NUMERIC		Quantity;
} tLBDEP_PARAMS_INT;

typedef struct
{
	tEXTERNAL_NUMERIC		Amount;
	char *							Number;
	tEXTERNAL_NUMERIC		Quantity;
} tLBDEP_PARAMS;

/*----------------------------------------------------------------------------*/

/*
	LBTREXITCAN - struktura dla generatora ramek (poniewa� s� tylko dwa parametry - funkcja b�dzie je
	otrzymywa� wprost, jako argumenty)
*/
typedef struct
{
	BYTE		_dummy;
	char *	CashRegNr;
	char *	Cashier;
} tLBTREXITCAN_PARAMS_INT;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE								Ps;
	BYTE								Pr;
	char *							Code;
	tEXTERNAL_NUMERIC		PaymentIn;
	tEXTERNAL_NUMERIC		Total;
} tLBTREXIT_A_PARAMS;

typedef struct
{
	BYTE								Ps;
	BYTE								Pr;
	BYTE								Pn;
	BYTE								Pc;
	char *							Code;
	char *							Line1;
	char *							Line2;
	char *							Line3;
	tEXTERNAL_NUMERIC		PaymentIn;
	tEXTERNAL_NUMERIC		Total;
} tLBTREXIT_B_PARAMS;

typedef struct
{
	BYTE								Ps;
	BYTE								Pr;
	BYTE								Pn;
	BYTE								Pc;
	BYTE								Px;
	BYTE								Py;
	char *							Code;
	char *							Line1;
	char *							Line2;
	char *							Line3;
	tEXTERNAL_NUMERIC		PaymentIn;
	tEXTERNAL_NUMERIC		Total;
	tEXTERNAL_NUMERIC		Discount;
} tLBTREXIT_PARAMS;
/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE								Pn;
	BYTE								Pc;
	BYTE								Py;
	BYTE								Px;
	BYTE								Pg;
	BYTE								Pk;
	BYTE								Pz;
	BYTE								Pb;
	BYTE								Po1;
	BYTE								Po2;
	BYTE								Pr;
	char *							Code;
	DWORD								Pn_1;
	char **							Line;
	char *							CardName;
	char *							ChequeName;
	char *							CouponName;
	tEXTERNAL_NUMERIC		Total;
	tEXTERNAL_NUMERIC		Discount;
	tEXTERNAL_NUMERIC		Cash;
	tEXTERNAL_NUMERIC		Card;
	tEXTERNAL_NUMERIC		Cheque;
	tEXTERNAL_NUMERIC		Coupon;
	tEXTERNAL_NUMERIC		DepositTaken;
	tEXTERNAL_NUMERIC		DepositReturned;
	tEXTERNAL_NUMERIC		Rest;
} tO_LBTRXEND_PARAMS_INT;

typedef struct
{
	BYTE								Pn;
	BYTE								Pc;
	BYTE								Py;
	BYTE								Px;
	BYTE								Pg;
	BYTE								Pk;
	BYTE								Pz;
	BYTE								Pb;
	BYTE								Po1;
	BYTE								Po2;
	BYTE								Pr;
	char *							Code;
	char **							Line;
	char *							CardName;
	char *							ChequeName;
	char *							CouponName;
	tEXTERNAL_NUMERIC		Total;
	tEXTERNAL_NUMERIC		Discount;
	tEXTERNAL_NUMERIC		Cash;
	tEXTERNAL_NUMERIC		Card;
	tEXTERNAL_NUMERIC		Cheque;
	tEXTERNAL_NUMERIC		Coupon;
	tEXTERNAL_NUMERIC		DepositTaken;
	tEXTERNAL_NUMERIC		DepositReturned;
	tEXTERNAL_NUMERIC		Rest;
} tO_LBTRXEND_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE								Ps;
	BYTE								Pfx;
	tEXTERNAL_NUMERIC		Amount;
	char *							FormOfPaymentName;
} tLBTRFORMPLAT_PARAMS;

/*----------------------------------------------------------------------------*/

/*
	LBTRXEND1 - struktura dla generatora ramek ( wewn�trzna, ze wzgl�du na to, �e interpreter woli protok� w�oski;
	zewn�trzna, dost�pna dla u�ytkownika struktura	-> tLBTRXEND1_PARAMS )
*/
typedef struct
{
	BYTE									Pn;
	BYTE									Pc;
	BYTE									Py;
	BYTE									Pdsp;
	BYTE									Px;
	BYTE									Pkb;
	BYTE									Pkz;
	BYTE									Pns;
	BYTE									Pfn;
	BYTE									Pr;
	BYTE									Pg;
	DWORD									Pfn_1;				/* Pfn, tyle �e DWORD. Potrzebne dla generatora ramek */
	BYTE *								Pfx;
	char *								CashRegNr;
	char *								Cashier;
	char *								SystemNr;
	DWORD									Pn_1;					/* jak Pfn, tyle �e dotyczy Pn */
	char **								Line;
	DWORD									Pfn_2;				/* Pfn - po raz drugi */
	char **								FormOfPaymentName;
	DWORD									Pkb_1;				/* Pkb - pierwsza kopia */
	char **								DepositTakenNr;
	DWORD									Pkb_2;				/* Pkb - drugia kopia */
	tEXTERNAL_NUMERIC *		DepositTakenQuantity;
	DWORD									Pkz_1;
	char **								DepositReturnedNr;
	DWORD									Pkz_2;
	tEXTERNAL_NUMERIC *		DepositReturnedQuantity;
	tEXTERNAL_NUMERIC			Total;
	tEXTERNAL_NUMERIC			Dsp;
	tEXTERNAL_NUMERIC			Discount;
	tEXTERNAL_NUMERIC			Cash;
	DWORD									Pfn_3;
	tEXTERNAL_NUMERIC *		FormOfPaymentAmount;
	tEXTERNAL_NUMERIC			Rest;
	DWORD									Pkb_3;
	tEXTERNAL_NUMERIC *		DepositTakenAmount;
	DWORD									Pkz_3;
	tEXTERNAL_NUMERIC *		DepositReturnedAmount;
} tLBTRXEND1_PARAMS_INT;

/*
	LBTRXEND1 -  argumenty
*/
typedef struct
{
	BYTE									Pn;
	BYTE									Pc;
	BYTE									Py;
	BYTE									Pdsp;
	BYTE									Px;
	BYTE									Pkb;
	BYTE									Pkz;
	BYTE									Pns;
	BYTE									Pfn;
	BYTE									Pr;
	BYTE									Pg;
	BYTE *								Pfx;
	char *								CashRegNr;
	char *								Cashier;
	char *								SystemNr;
	char **								Line;
	char **								FormOfPaymentName;
	char **								DepositTakenNr;
	tEXTERNAL_NUMERIC *		DepositTakenQuantity;
	char **								DepositReturnedNr;
	tEXTERNAL_NUMERIC *		DepositReturnedQuantity;
	tEXTERNAL_NUMERIC			Total;
	tEXTERNAL_NUMERIC			Dsp;
	tEXTERNAL_NUMERIC			Discount;
	tEXTERNAL_NUMERIC			Cash;
	tEXTERNAL_NUMERIC *		FormOfPaymentAmount;
	tEXTERNAL_NUMERIC			Rest;
	tEXTERNAL_NUMERIC *		DepositTakenAmount;
	tEXTERNAL_NUMERIC *		DepositReturnedAmount;
} tLBTRXEND1_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE								Pk;
	BYTE								Pc;
	BYTE								Pb;
	char *							Shift;
	char *							Cashier;
	char *							CardName1;
	char *							CardName2;
	char *							CardName3;
	char *							CardName4;
	char *							CardName5;
	char *							CardName6;
	char *							CardName7;
	char *							CardName8;
	char *							ChequeName1;
	char *							ChequeName2;
	char *							ChequeName3;
	char *							ChequeName4;
	char *							CouponName1;
	char *							CouponName2;
	char *							CouponName3;
	char *							CouponName4;
	char *							Beginning;
	char *							End;
	tEXTERNAL_NUMERIC		Takings;
	tEXTERNAL_NUMERIC		SalesCash;
	tEXTERNAL_NUMERIC		Card1;
	tEXTERNAL_NUMERIC		Card2;
	tEXTERNAL_NUMERIC		Card3;
	tEXTERNAL_NUMERIC		Card4;
	tEXTERNAL_NUMERIC		Card5;
	tEXTERNAL_NUMERIC		Card6;
	tEXTERNAL_NUMERIC		Card7;
	tEXTERNAL_NUMERIC		Card8;
	tEXTERNAL_NUMERIC		Cheque1;
	tEXTERNAL_NUMERIC		Cheque2;
	tEXTERNAL_NUMERIC		Cheque3;
	tEXTERNAL_NUMERIC		Cheque4;
	tEXTERNAL_NUMERIC		Coupon1;
	tEXTERNAL_NUMERIC		Coupon2;
	tEXTERNAL_NUMERIC		Coupon3;
	tEXTERNAL_NUMERIC		Coupon4;
	tEXTERNAL_NUMERIC		PaymentIn;
	tEXTERNAL_NUMERIC		DepositTaken;
	tEXTERNAL_NUMERIC		PaymentOut;
	tEXTERNAL_NUMERIC		DepositReturned;
	tEXTERNAL_NUMERIC		Cash;
	char *							NrOfReceipts;
	char *							NrOfCancelledReceipts;
	char *							NrOfCancelledItems;
	char *							CashRegNr;
} tLBCSHREP1_PARAMS;


/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE									Pkb;
	BYTE									Pkz;
	BYTE									Pfn;
	BYTE									Pg;
	BYTE *								Pfx;
	char *								Shift;
	char *								CashRegNr;
	char *								Cashier;
	char *								Beginning;
	char *								End;
	char **								FormOfPaymentName;
	char **								DepositTakenName;
	char **								DepositReturnedName;
	tEXTERNAL_NUMERIC			Takings;
	tEXTERNAL_NUMERIC			SalesCash;
	tEXTERNAL_NUMERIC			PaymentIn;
	tEXTERNAL_NUMERIC			Expenditures;
	tEXTERNAL_NUMERIC			PaymentOut;
	tEXTERNAL_NUMERIC *		FormOfPaymentAmount;
	tEXTERNAL_NUMERIC			DepositTakenTotal;
	tEXTERNAL_NUMERIC *		DepositTakenAmount;
	tEXTERNAL_NUMERIC			DepositReturnedTotal;
	tEXTERNAL_NUMERIC *		DepositReturnedAmount;
	tEXTERNAL_NUMERIC			Cash;
	DWORD									NrOfReceipts;
	DWORD									NrOfCancelledReceipts;
	DWORD									NrOfCancelledItems;
} tLBCSHREP2_PARAMS;

typedef struct
{
	BYTE									Pkb;
	BYTE									Pkz;
	BYTE									Pfn;
	BYTE									Pg;
	DWORD									Pfn_1;
	BYTE *								Pfx;
	char *								Shift;
	char *								CashRegNr;
	char *								Cashier;
	char *								Beginning;
	char *								End;
	DWORD									Pfn_2;
	char **								FormOfPaymentName;
	DWORD									Pkb_1;
	char **								DepositTakenName;
	DWORD									Pkz_1;
	char **								DepositReturnedName;
	tEXTERNAL_NUMERIC			Takings;
	tEXTERNAL_NUMERIC			SalesCash;
	tEXTERNAL_NUMERIC			PaymentIn;
	tEXTERNAL_NUMERIC			Expenditures;
	tEXTERNAL_NUMERIC			PaymentOut;
	DWORD									Pfn_3;
	tEXTERNAL_NUMERIC *		FormOfPaymentAmount;
	tEXTERNAL_NUMERIC			DepositTakenTotal;
	DWORD									Pkb_2;
	tEXTERNAL_NUMERIC *		DepositTakenAmount;
	tEXTERNAL_NUMERIC			DepositReturnedTotal;
	DWORD									Pkz_2;
	tEXTERNAL_NUMERIC *		DepositReturnedAmount;
	tEXTERNAL_NUMERIC			Cash;
	DWORD									NrOfReceipts;
	DWORD									NrOfCancelledReceipts;
	DWORD									NrOfCancelledItems;
} tLBCSHREP2_PARAMS_INT;

/*----------------------------------------------------------------------------*/

/*
	Ramki LBTRSCARD i LBSTOCARD r�ni� si� jedynie kodem rozkazu, pola parametr�w s� takie same.
*/
typedef struct
{
	BYTE							Ps;
	BYTE							Pn;
	char *						CashRegNr;
	char *						CashierNr;
	char *						ReceiptNr;
	char *						Contract;
	char *						Terminal;
	char *						CardName;
	char *						CardNr;
	char *						DateMM;
	char *						DateYY;
	char *						AuthCode;
	tEXTERNAL_NUMERIC	Amount;
} tLBCARD_PARAMS;


/*----------------------------------------------------------------------------*/

/*
	Ramki LBINCCSH i LBDECCSH r�ni� si� jedynie kodem rozkazu, pola parametr�w s� takie same.
*/
typedef struct
{
	BYTE							_dummy;
	tEXTERNAL_NUMERIC	Payment;
	char *						CashRegNr;
	char *						Cashier;
} tLBCSH_PARAMS;


/*----------------------------------------------------------------------------*/

/*
	LBCSHSTS - struktura dla generatora ramek (do funkcji obs�uguj�cej LBCSHSTS parametry b�d�
	przekazywane wprost)
*/

typedef struct
{
	BYTE							_dummy;
	char *						CashRegNr;
	char *						Cashier;
} tLBCSHSTS_PARAMS_INT;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE		Ps;
	char *	Shift;
	char *	Cashier;
	char *	CashRegNr;
} tLBCSHREP_PARAMS;

/*----------------------------------------------------------------------------*/

/*
	Ramki LBLOGIN i LBLOGOUT r�ni� si� jedynie kodem rozkazu, pola parametr�w s� takie same.
*/
typedef struct
{
	BYTE		_dummy;
	char *	CashRegNr;
	char *	Cashier;
} tLBLOG_PARAMS;

/*----------------------------------------------------------------------------*/

/*
	LBFSKREP ma dwie, znacznie r�ni�ce si� od siebie, wersje - z podaniem dat lub z podaniem numer�w rekord�w
*/
typedef struct
{
	BYTE		Py1;
	BYTE		Pm1;
	BYTE		Pd1;
	BYTE		Py2;
	BYTE		Pm2;
	BYTE		Pd2;
	BYTE		Pt;
	char *	CashRegNr;
	char *	Cashier;
} tLBFSKREP_D_PARAMS;

typedef struct
{
	BYTE		Pt;
	DWORD		From;
	DWORD		To;
	char *	CashRegNr;
	char *	Cashier;
} tLBFSKREP_R_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE		Mode;
	DWORD		_Date;
	BYTE *	_DateTab;
	char *	CashRegNr;
	char *	Cashier;
} tLBDAYREP_PARAMS_INT;

typedef struct
{
	BYTE		_Date;
	BYTE		Py;
	BYTE		Pm;
	BYTE		Pd;
	char *	CashRegNr;
	char *	Cashier;
} tLBDAYREP_PARAMS;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE		Ps;
	char *	Name;
	char *	Vat;
	char *	CashRegNr;
	char *	Cashier;
} tLBDBREP_PARAMS;


typedef struct
{
	BYTE		Result;
} tLBDBREP_RESPONSE;


/*----------------------------------------------------------------------------*/

typedef struct
{
	char *	Name;
	char *	Vat;
} tLBDBREPRS_PARAMS_INT;

typedef struct
{
	BYTE		Pe;
	char *	Vat;
} tLBDBREPRS_RESPONSE;

/*----------------------------------------------------------------------------*/

/*
	LBSENDCK - struktura dla generatora ramek
*/
typedef struct
{
	BYTE _dummy;
} tLBSENDCK_PARAMS_INT;

typedef struct
{
	BYTE	Pyy;
	BYTE	Pmm;
	BYTE	Pdd;
	BYTE	Ph;
	BYTE	Pm;
	BYTE	_Ps;		/* sekundy - na sta�e 0 */
} tLBSENDCK_RESPONSE;

/*----------------------------------------------------------------------------*/

/*
	LBFSTRQ dla Ps == {23, 24, 27}, nie ma pola 'Nr'
*/

typedef struct
{
	BYTE		Ps;
} tLBFSTRQ_PARAMS;

/*
	LBFSTRQ dla Ps == 26, pole 'Nr' jest obecne
*/

typedef struct
{
	BYTE		Ps;
	DWORD		Nr;
} tLBFSTRQ26_PARAMS;


/*
	LBFSTRS1 - odpowied� na LBFSTRQ z parametrem 23
*/
typedef struct
{
	BYTE		Pe;
	BYTE		Pm;
	BYTE		Pt;
	BYTE		Px;
	BYTE		Pf;
	BYTE		Pz;
	BYTE		Pyy;
	BYTE		Pmm;
	BYTE		Pdd;
	char *	VatA;
	char *	VatB;
	char *	VatC;
	char *	VatD;
	char *	VatE;
	char *	VatF;
	char *	VatG;
	DWORD		NrOfReceipts;
	char *	TotA;
	char *	TotB;
	char *	TotC;
	char *	TotD;
	char *	TotE;
	char *	TotF;
	char *	TotG;
	char *	Cash;
	char *	SerialNr;
} tLBFSTRS1_RESPONSE;



/*
	LBFSTRQ1 - odpowied� na LBFSTRQ z parametrem 24
*/
typedef struct
{
	DWORD		Py;
	BYTE		Pm;
	BYTE		Pd;
	DWORD		DailyRecNr;
	DWORD		DailyRecRem;
	DWORD		Blocked;
	char *	AmountA;
	char *	AmountB;
	char *	AmountC;
	char *	AmountD;
	char *	AmountE;
	char *	AmountF;
	char *	AmountG;
} tLBFSTRQ1_RESPONSE;

/*
	LBFSTRQ25 - LBFSTRQ dla Ps == 25
*/
typedef struct
{
	BYTE	Ps;
	BYTE	Py;
	BYTE	Pm;
	BYTE	Pd;
	BYTE	Ph;
	BYTE	Pmin;
	BYTE	Psec;
} tLBFSTRQ25_PARAMS;

/*
	Odpowied� na LBFSTRQ z parametrem 27 -> rekord
*/
typedef struct
{
	DWORD		Year;			/* Rok w postaci pe�nej ! */
	BYTE		Month;
	BYTE		Day;
	BYTE		Hour;
	BYTE		Min;
	BYTE		Sec;
	DWORD		Num1;
	DWORD		Num2;
	DWORD		Num3;
	char *	Val1;
	char *	ValA;
	char *	ValB;
	char *	ValC;
	char *	ValD;
	char *	ValE;
	char *	ValF;
	char *	ValG;
} tLBFSTRQ27_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE		_dummy;
} tLBERNRQ_PARAMS_INT;

typedef struct
{
	BYTE		Pe;
} tLBERNRQ_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE		_dummy;
} tLBIDRQ_PARAMS_INT;

typedef struct
{
	char *		Type;
	char *		Version;
} tLBIDRQ_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE		Ps;
} tLBSNDMD_PARAMS_INT;


/******************* STRUKTURY - PROTOKӣ POLSKI (STARA HOM.) *****************/

/*
	LBFSTRS - odpowied� na LBFSTRQ
	- O_LBFSTRS_RESPONSE1...O_LBFSTRS_RESPONSE6 - numer oznacza liczb� zdefiniowanych
		stawek
	- O_LBFSTRS_RESPONSE - unia (a co, w sumie z g�ry nie wiadomo ile jest stawek -
		wi�c trzeba przygotowa� si� na najgorsze)
*/


typedef struct
{
	BYTE		Pe;
	BYTE		Pm;
	BYTE		Pt;
	BYTE		Px;
	BYTE		Pf;
	BYTE		Pz;
	BYTE		Pyy;
	BYTE		Pmm;
	BYTE		Pdd;
	char *	VatA;
	DWORD		NrOfReceipts;
	char *	TotA;
	char *	TotZ;
	char *	Cash;
	char *	SerialNr;
} tO_LBFSTRS_RESPONSE1;

typedef struct
{
	BYTE		Pe;
	BYTE		Pm;
	BYTE		Pt;
	BYTE		Px;
	BYTE		Pf;
	BYTE		Pz;
	BYTE		Pyy;
	BYTE		Pmm;
	BYTE		Pdd;
	char *	VatA;
	char *	VatB;
	DWORD		NrOfReceipts;
	char *	TotA;
	char *	TotB;
	char *	TotZ;
	char *	Cash;
	char *	SerialNr;
} tO_LBFSTRS_RESPONSE2;

typedef struct
{
	BYTE		Pe;
	BYTE		Pm;
	BYTE		Pt;
	BYTE		Px;
	BYTE		Pf;
	BYTE		Pz;
	BYTE		Pyy;
	BYTE		Pmm;
	BYTE		Pdd;
	char *	VatA;
	char *	VatB;
	char *	VatC;
	DWORD		NrOfReceipts;
	char *	TotA;
	char *	TotB;
	char *	TotC;
	char *	TotZ;
	char *	Cash;
	char *	SerialNr;
} tO_LBFSTRS_RESPONSE3;

typedef struct
{
	BYTE		Pe;
	BYTE		Pm;
	BYTE		Pt;
	BYTE		Px;
	BYTE		Pf;
	BYTE		Pz;
	BYTE		Pyy;
	BYTE		Pmm;
	BYTE		Pdd;
	char *	VatA;
	char *	VatB;
	char *	VatC;
	char *	VatD;
	DWORD		NrOfReceipts;
	char *	TotA;
	char *	TotB;
	char *	TotC;
	char *	TotD;
	char *	TotZ;
	char *	Cash;
	char *	SerialNr;
} tO_LBFSTRS_RESPONSE4;

typedef struct
{
	BYTE		Pe;
	BYTE		Pm;
	BYTE		Pt;
	BYTE		Px;
	BYTE		Pf;
	BYTE		Pz;
	BYTE		Pyy;
	BYTE		Pmm;
	BYTE		Pdd;
	char *	VatA;
	char *	VatB;
	char *	VatC;
	char *	VatD;
	char *	VatE;
	DWORD		NrOfReceipts;
	char *	TotA;
	char *	TotB;
	char *	TotC;
	char *	TotD;
	char *	TotE;
	char *	TotZ;
	char *	Cash;
	char *	SerialNr;
} tO_LBFSTRS_RESPONSE5;

typedef struct
{
	BYTE		Pe;
	BYTE		Pm;
	BYTE		Pt;
	BYTE		Px;
	BYTE		Pf;
	BYTE		Pz;
	BYTE		Pyy;
	BYTE		Pmm;
	BYTE		Pdd;
	char *	VatA;
	char *	VatB;
	char *	VatC;
	char *	VatD;
	char *	VatE;
	char *	VatF;
	DWORD		NrOfReceipts;
	char *	TotA;
	char *	TotB;
	char *	TotC;
	char *	TotD;
	char *	TotE;
	char *	TotF;
	char *	TotZ;
	char *	Cash;
	char *	SerialNr;
} tO_LBFSTRS_RESPONSE6;

typedef union
{
	tO_LBFSTRS_RESPONSE1 NrOfRatesEq1;
	tO_LBFSTRS_RESPONSE2 NrOfRatesEq2;
	tO_LBFSTRS_RESPONSE3 NrOfRatesEq3;
	tO_LBFSTRS_RESPONSE4 NrOfRatesEq4;
	tO_LBFSTRS_RESPONSE5 NrOfRatesEq5;
	tO_LBFSTRS_RESPONSE6 NrOfRatesEq6;
} tO_LBFSTRS_RESPONSE;

/*----------------------------------------------------------------------------*/

typedef struct
{
	BYTE _dummy;
} tO_LBTREXIT_PARAMS_INT;


typedef struct
{
	char *DepositReturned;
} t_O_LBOPAK_PARAMS;

typedef struct
{
	tEXTERNAL_NUMERIC	Returned;
} t_O_LBSTOCSH_PARAMS;


#endif // _THERMAL_SERVICE_LIBRARY