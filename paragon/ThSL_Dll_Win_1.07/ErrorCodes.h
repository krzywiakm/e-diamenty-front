/* Tab 2 */

/*
	ErrorCodes.h - kody b��d�w
*/

#ifndef _ERROR_CODES
#define _ERROR_CODES

/****************************** KODY B��D�W ***********************************/

/*
	Uwaga na:
	ERR_ACCESS_VIOLATION, ERR_PARAM_VALUE, ERR_PARAM_LEN, ERR_FIELD_TYPE
	Na m�odszym s�owie b�d� mia�y numer argumentu (liczony od zera), kt�ry
	spowodowa� ten b��d
	Zdefiniowana jest maska bitowa ERR_SPECIAL_MASK, s�u��ca do wykrywania tych
	b��d�w
*/

#define ERR_SPECIAL_MASK							 0xFFFF0000

/* wszystko w porz�dku */
#define SOK														 0

/* wyst�pi� b��d (kiedy nie jest konieczne bli�sze sprecyzowanie rodzaju) */
#define SERROR												-1

/* dost�p do nieprawid�owego obszaru pami�ci */
#define ERR_ACCESS_VIOLATION					0xC0000000

/* b��d allokacji pami�ci */
#define ERR_ALLOC											-2

/* b��d CreateFile(...) - w przypadku plik�w dyskowych */
#define ERR_CREATE_FILE								-3

/* b��d CreateFile(...) - w przypadku portu komunikacyjnego  */
#define ERR_CREATE_FILE_COM						-4

/* b��d funkcji ReadFile - dla pliku i dla portu, odpowiednio */
#define ERR_READ_FILE									-5
#define ERR_READ_FILE_COM							-6

/* b��d funkcji WriteFile (plik/port)*/
#define ERR_WRITE_FILE								-7
#define ERR_WRITE_FILE_COM						-8

/* b��d ustawiania parametr�w portu */
#define ERR_COMM_SETTINGS							-9	

/* port nie zosta� otwarty, a kto� pr�buje wys�a� lub odebra� dane */
#define ERR_PORT_NOT_OPEN							-10



/* zupe�ny brak odpowiedzi (w wyznaczonym czasie) */
#define ERR_NO_RESPONSE								-20

/*
	Za d�ugie oczekiwanie na odpowied� (na odbi�r ca�ej ramki)
	Mo�liwe przyczyny:
	- jakie� przerwy w transmisji
	- nie otrzymano znacznika ko�ca ramki w wyznaczonym czasie
*/
#define ERR_RESPONSE_TOUT							-21

/* w przypadku timeout-u u�ytkownik wybra� ponowne wys�anie ramki */
#define ERR_RESEND										-22

/* przekroczona liczba pr�b - protok� w�oski */
#define ERR_I_MAX_RETRIES							-23

/* nieprawid�owa warto�� sumy konrolnej */
#define ERR_CHECKSUM									-24

/* zasygnalizowano b��d wykonania rozkazu -> GetExtendedErrorCode() aby pobra� ten kod */
#define ERR_RETURN_CODE								-25

/* nieznana/nieprawid�owa ramka  */
#define ERR_UNKNOWN_FRAME							-26

/* nieznana (nierozpoznana) komenda */
#define ERR_UNKNOWN_CMD								-27

/* zape�niono bufor wej�ciowy - a nie by�o znacznika ko�ca ramki */
#define ERR_INPUT_BUF_OVRRUN					-28

/* nie jest mo�liwe wygenerowanie ramki (zbyt d�uga - bufor zbyt kr�tki) */
#define ERR_FRAME_TOO_LONG						-29



/* nieprawid�owy rodzaj pola */
#define ERR_FIELD_TYPE								0xD0000000

/* nieprawid�owa zawarto�� pola - niezgodna z rodzajem  (ODPOWIED�) */
#define ERR_FIELD_CONTENTS						-41

/* nieprawid�owa liczba p�l w ramce zwrotnej */
#define ERR_NUMBER_OF_FIELDS					-42

/* nieprawid�owe pole nag��wka */
#define ERR_FMT_HEADER								-43



/* nieprawid�owe pole zwrotnego kodu
	- b��du: IT, POL
	- bajtowego (?) znajduj�cego si� przed kodem rozkazu: POL
*/
#define ERR_FMT_RETURN_CODE						-50

/* nieprawid�owe pole (format) sumy kontrolnej */
#define ERR_FMT_CHECKSUM							-51

/* nieprawid�owe odpowiedzi na ENQ/DLE */
#define ERR_FMT_ENQ										-52
#define ERR_FMT_DLE										-53



/* nieprawid�owa warto�� parametru */
#define ERR_PARAM_VALUE								0xE0000000

/* nieprawid�owa d�ugo�� parametru (teksty) */
#define ERR_PARAM_LEN									0xF0000000

/*
	b��d parametr�w opcjonalnych (kiedy wymagane jest, aby wyst�powa�y albo
	wszystkie, albo �aden
*/
#define ERR_OPT_PARAMS								-60

/* nieprawid�owa warto�� rabatu/narzutu*/
#define ERR_DISCOUNT_VALUE						-61



/* nieprawid�owa linia w pliku z bazami danych - �le sformatowana */
#define ERR_FMT_DB_LINE								-200

/* nieprawid�owe pola w pliku z bazami danych */
#define ERR_FMT_DB_FIELD							-201
#define ERR_FMT_DB_FIELD_NUM					-202



#define ERR_NOTHING_TO_CANCEL					-300
#define ERR_CANT_CANCEL								-301
#define ERR_CANCEL										-302

/********************* KODY B��D�W ZWRACANE PRZEZ URZ�DZENIA ******************/


#define DEVERR_I_NOT_ALLOWED				254
#define DEVERR_I_VAT_NOT_DIFFERENT	93

#endif // _ERROR_CODES