/* Tab 2 */

/*
	CommonTexts.h - r�ne teksty wsp�lne
*/

#ifndef _COMMON_TEXTS
#define _COMMON_TEXTS

#define LANGUAGE_POLISH



#ifdef LANGUAGE_ENGLISH

#define ERRMSG_TITLE		"Error !"
#define WARNMSG_TITLE		"Warning !"
#define INFOMSG_TITLE		"Information..."

#define MSG_PROGRAMMING_COMPLETED	"Programming completed"
#define MSG_NOT_CONNECTED					"Not connected"
#define MSG_CONNECTED							"Connected"

#define FMTSTR_GET_ERROR_MESSAGE "( Command:%c%c  Return code:%d )"

#define STR_UNKNOWN						"???"

#define STR_HW_TYPE_ECR				"ECR"
#define STR_HW_TYPE_BINGO			"Bingo"
#define STR_HW_TYPE_THERMAL		"Thermal"

#define STR_FSC_MODE_NON_FISCAL	"Non fiscal"
#define STR_FSC_MODE_FISCAL			"Fiscal"
#define STR_FSC_MODE_EURO				"Euro"

#define TITLE_QUANTITY	"Quantity"
#define TITLE_DISCOUNT	"Discount"
#define TITLE_AMOUNT		"Amount"

#define ERRMSG_DLL_NOT_LOADED		"ThermalServiceLibrary.dll not loaded"
#define ERRMSG_FUNC_NOT_FOUND		"Function not found: "

#define ERRMSG_FREE_LIBRARY		"FreeLibrary failed"
#define ERRMSG_CREATE_FILE		"An error occured while opening the file"

#define ERRMSG_FMT_LINE_NR		"Line nr:%d"

#define ERRMSG_GET_STAT_CASH_REG	"An error occured while getting the cash register status"
#define ERRMSG_GET_STAT_PRINTER		"An error occured while getting the printer status"

#define ERRMSG_LOGIN		"An error occured while logging in"
#define ERRMSG_LOGOUT		"An error occured while logging out"
#define ERRMSG_SETOPT		"An error occured while setting the options"

#define ERRMSG_RECGET		"An error occured while getting the record"

#define ERRMSG_PROGRAMMING					"An error occured while programming"
#define ERRMSG_PROGRAMMING_NOTHING	"An error occured while programming\nNothing was sent..."

#define ERRMSG_PORT_OPEN						"An error occured while opening the port"

#define ERRMSG_TRANSACTION_START		"An error occured while starting the transaction"
#define ERRMSG_TRANSACTION_LINE			"An error ocurred while sending the transaction line"
#define ERRMSG_TRANSACTION_CANCEL		"An error occured while cancelling the previous transaction"
#define ERRMSG_TRANSACTION_END			"An error occured while finalizing the transaction"


#elif defined LANGUAGE_POLISH

#define ERRMSG_TITLE		"B��d !"
#define WARNMSG_TITLE		"Ostrze�enie !"
#define INFOMSG_TITLE		"Informacja..."
#define QUESTNMSG_TITLE	"Zapytanie"

#define	QUESTNMSG_TIMEOUT		"Urz�dzenie nie odpowiedzia�o w oczekiwanym czasie.\n\nCzy chcesz:\n- czeka� dalej na odpowied� (Tak)\n- wys�a� ramk� ponownie (Nie)\n- przerwa� (Anuluj)"

#define MSG_PROGRAMMING_COMPLETED	"Programming completed"
#define MSG_NOT_CONNECTED					"Not connected"
#define MSG_CONNECTED							"Connected"

#define FMTSTR_GET_ERROR_MESSAGE "( Rozkaz:%c%c  Kod b��du:%d )"

#define STR_UNKNOWN						"???"

#define STR_HW_TYPE_ECR				"ECR"
#define STR_HW_TYPE_BINGO			"Bingo"
#define STR_HW_TYPE_THERMAL		"Thermal"

#define STR_FSC_MODE_NON_FISCAL	"Non fiscal"
#define STR_FSC_MODE_FISCAL			"Fiscal"
#define STR_FSC_MODE_EURO				"Euro"

#define TITLE_QUANTITY	"Quantity"
#define TITLE_DISCOUNT	"Discount"
#define TITLE_AMOUNT		"Amount"

#define ERRMSG_DLL_NOT_LOADED		"Nie za�adowano ThermalServiceLibrary.dll"
#define ERRMSG_FUNC_NOT_FOUND		"Nie znaleziono funkcji: "

#define ERRMSG_FREE_LIBRARY		"FreeLibrary nie powiod�o si�"
#define ERRMSG_CREATE_FILE		"Wyst�pi� b��d podczas otwierania/tworzenia pliku"

#define ERRMSG_FMT_LINE_NR		"Nr linii:%d"

#define ERRMSG_GET_STAT_CASH_REG	"Wyst�pi� b��d podczas odczytywania statusu kasy"
#define ERRMSG_GET_STAT_PRINTER		"Wyst�pi� b��d podczas odczytywania statusu mechanizmu"

#define ERRMSG_PORT_OPEN						"Wyst�pi� b��d podczas otwierania portu"

#define ERRMSG_TRANSACTION_START		"Wyst�pi� b��d podczas rozpoczynania transakcji"
#define ERRMSG_TRANSACTION_LINE			"Wyst�pi� b��d podczas wysy�ania linii paragonu"
#define ERRMSG_TRANSACTION_CANCEL		"Wyst�pi� b��d podczas anulowania transakcji"
#define ERRMSG_TRANSACTION_END			"Wyst�pi� b��d podczas ko�czenia transakcji"

#define ERRMSG_UNRECOGNIZED_DEVICE_VERSION	"Nie uda�o si� rozpozna� wersji urz�dzenia"


#endif // LANGUAGE_...


#endif // _COMMON_TEXTS