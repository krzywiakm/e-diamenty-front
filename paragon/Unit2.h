//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "THERMALLIBLib_OCX.h"
#include <OleCtrls.hpp>
#include <Buttons.hpp>
#include <IniFiles.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
    TEdit *Edit1;
    TEdit *Edit2;
    TEdit *Edit3;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TEdit *Edit4;
    TLabel *Label4;
    TButton *Button1;
    TThermalLib *ThermalLib1;
    TBitBtn *BitBtn1;
    TBitBtn *BitBtn2;
    TLabel *Label5;
    TEdit *Edit5;
    TEdit *Edit6;
    TEdit *Edit7;
    TLabel *Label6;
    TLabel *Label7;
    TLabel *Label8;
    TEdit *Edit8;
    TLabel *Label9;
    TEdit *Edit9;
    TEdit *Edit10;
    TEdit *Edit11;
    TLabel *Label10;
    TLabel *Label11;
    TLabel *Label12;
    TBevel *Bevel1;
    TBevel *Bevel2;
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall BitBtn1Click(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
