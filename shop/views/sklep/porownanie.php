<? include '../../func.php'; ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Porównanie diamentów e-diamenty.pl – wybierz diament idealny dla siebie</title>
	<meta name="description" content="Porównanie diamentów - funkcja pozwalająca porównać najkorzystniejszą ofertę diamentów i dokonać zakupu brylantów w cenach hurtowych i diamentów inwestycyjnych" />
	<meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/component.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="main" class="compare">
    <!-- start /main header -->
    <div class="wrapper row">
        <div class="col-md-12">
            <div class="heading">
                <div class="row">
                    <div class="col-md-6 left">
                        <h1>Porównanie</h1>
                    </div>
                    <div class="col-md-6 right">
                        <a href="<?= BASE_URL ?>/views/sklep/katalog" title="Do katalogu produktów">Do katalogu produktów</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper row result">
        <div class="col-md-12">
            <p><span>6</span> <?= slownik(53) ?></p>
            <a href="#" title="usuń wszystkie" class="remove">usuń wszystkie</a>
        </div>
    </div>
    <!-- start /main header -->
    
    <!-- start / compare table -->
    <div class="wrapper row table">
        <div class="col-md-12">
            <div class="table-responsive">
                <div class="Table">
    				 <div class="TableHeading">
                         <div class="TableHead head-watch"><img src="<?= BASE_URL ?>/assets/images/icons/observ.png" alt="Obserwuj"></div>
                         <div class="TableHead head-compare"><img src="<?= BASE_URL ?>/assets/images/icons/compare.png" alt="Porównaj"></div>
                            <div data-orderBy="shape" class="TableHead head-shape"><?= slownik(55) ?>
                            </div>
                            <div data-orderBy="mass" class="TableHead head-mass"><?= slownik(56) ?>
                            </div>
                            <div data-orderBy="color" class="TableHead head-color"><?= slownik(57) ?>
                            </div>
                            <div data-orderBy="clarity" class="TableHead head-clarity"><?= slownik(58) ?>
                            </div>
                            <div data-orderBy="certificat" class="TableHead head-certificat"><?= slownik(59) ?>
                            </div>
                            <div data-orderBy="availability" class="TableHead head-availability"><?= slownik(60) ?>
                            </div>
                            <div data-orderBy="symmetry" class="TableHead head-symmetry"><?= slownik(61) ?>
                            </div>
                            <div data-orderBy="cut" class="TableHead head-cut"><?= slownik(62) ?>
                            </div>
                            <div data-orderBy="polishing" class="TableHead head-polishing"><?= slownik(63) ?>
                            </div>
                            <div data-orderBy="price" class="TableHead head-price"><?= slownik(64) ?>
                            </div>
                         <div data-orderBy="cart" class="TableHead head-cart"><img src="<?= BASE_URL ?>/assets/images/icons/add-to-cart-active.png" alt="Dodaj do koszyka"></div>
    				 </div>
                </div>
            </div>
        </div>
    </div>
    <!-- start / compare table -->
</section>
<!-- end / main  -->

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.comapred.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>   

<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>

</body>
</html>

