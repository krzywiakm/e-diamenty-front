<? include '../../func.php'; ?>
<? loggedOnly() ?>
<?
updateUserData($_SESSION["USER_ID"]);
$current_user = getUserData($_SESSION["USER_ID"]); 
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Twoje dane</title>
	<meta name="description" content="" />
	<meta name="robots" content="noindex, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link href="<?= BASE_URL ?>/assets/css/jquery.bxslider.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="main" class="register account-details">
    <div class="wrapper row">
        <div class="col-md-5 col-md-offset-1 form">
            <h2>Twoje dane</h2>
            <p style="color: green;"><?= printMsg() ?></p>
            <p style="color: red;"><?= printErrorMsg() ?></p>
            <form method="post">
                <div class="form-group">
                    <label>Imię</label>
                    <input type="text" name="name" value="<?= $current_user['Imie'] ?>" />
                </div>
                <div class="form-group">
                    <label>Nazwisko</label>
                    <input type="text" name="surname" value="<?= $current_user['Nazwisko'] ?>" />
                </div>
                <div class="form-group">
                    <label>E-mail</label>
                    <input class="disabled" type="email" name="Email" value="<?= $current_user['Email'] ?>" disabled />
                </div>
                <div class="form-group">
                    <label>Telefon</label>
                    <input type="text" name="telefon" value="<?= $current_user['Telefon'] ?>" />
                </div>
                <div class="form-group">
                    <label>Kraj</label>
                    <select name="kraj">
                        <option selected>Polska</option>
                        <option>Niemcy</option>
                        <option>Włochy</option>
                        <option>Anglia</option>
                    </select>
                </div>
                <div class=" row form-group">
                    <div class="col-md-6">
                        <label>Kod pocztowy</label>
                        <input type="text" name="kod" value="<?= $current_user['KodPocztowy'] ?>" />
                    </div>
                    <div class="col-md-6">
                        <label>Miasto</label>
                        <input type="text" name="miasto" value="<?= $current_user['Miejscowosc'] ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label>Ulica, nr</label>
                    <input type="text" name="ulica" value="<?= $current_user['Ulica'] ?>" />
                </div>
                <div class="form-group">
                    <label>Nazwa firmy</label>
                    <input type="text" name="nazwafirmy" value="<?= $current_user['NazwaFirmy'] ?>" />
                </div>
                <div class="form-group">
                    <label>NIP</label>
                    <input type="text" name="nip" value="<?= $current_user['NIP'] ?>" />
                </div>
                <div class="form-group">
                    <p><span class="required">*</span> pola wymagane</p>
                </div>
                
                <div class="form-group additional">
                    <input type="checkbox" name="chkDodatkowyAdres" id="add" />
                    <label for="add"><span></span>Adres dostawy, jeśli inny niż powyższy</label>
                </div>
                <!-- start / accordion form -->
                <div id="additional-form">
                    <h3>Adres dostawy</h3>
                    <div class="form-group">
                        <label>Imię i nazwisko <span class="required">*</span></label>
                        <input type="text" name="nazwa2" value="<?= $current_user['Nazwa2'] ?>" />
                    </div>
                    <div class="form-group">
                        <label>Kraj <span class="required">*</span></label>
                        <select name="krajplus">
                            <option selected>Polska</option>
                            <option>Niemcy</option>
                            <option>Włochy</option>
                            <option>Anglia</option>
                        </select>
                    </div>
                    <div class=" row form-group">
                        <div class="col-md-6">
                            <label>Kod pocztowy <span class="required">*</span></label>
                            <input type="text" name="kodplus" value="<?= $current_user['KodPocztowy2'] ?>" />
                        </div>
                        <div class="col-md-6">
                            <label>Miasto <span class="required">*</span></label>
                            <input type="text" name="miastoplus" value="<?= $current_user['Miejscowosc2'] ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Ulica, nr <span class="required">*</span></label>
                        <input type="text" name="ulicaplus" value="<?= $current_user['Ulica2'] ?>" />
                    </div>
                </div>
                <!-- end / accordion form -->
                <div class="form-group">
                    <input type="submit" name="updateData" value="Zapisz zmiany" />
                </div>
            </form>
        </div>
        
        <!-- start / slider  -->
        <div class="col-md-5 news">
            <? newest_products_slider(); ?>
        </div>
         <!-- end / slider  -->
    </div>
</section>
<!-- end / main  -->

<? footer(); ?>
<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script> 
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.registerAccordion.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script> 
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>  
 
</body>
</html>
