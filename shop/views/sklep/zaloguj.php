<? include '../../func.php'; ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Zaloguj się – e-diamenty.pl – największa baza diamentów i brylantów w Polsce</title>
	<meta name="description" content="Logowanie do e-diamenty.pl – po zalogowaniu klienci mają dostęp do historii zamówionych diamentów oraz do panelu klienta hurtowni diamentów inwestycyjnych" />
	<meta name="robots" content="noindex,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link href="<?= BASE_URL ?>/assets/css/jquery.bxslider.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<section id="main" class="login">
    <div class="wrapper row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1 form">
            <? if(auth() && ($_SESSION["BezRej"] != true) ) { $current_user = getUserData($_SESSION["USER_ID"]); ?>
            <h2>Witaj <?= $current_user['Imie']; ?>!</h2>
            <ul>
                <li><a style="margin-top:10px;" class="readmore" href="<?= BASE_URL ?>/views/sklep/twoje-dane" title="Twoje dane">Twoje dane</a></li>
                <li><a style="margin-top:10px;" class="readmore" href="<?= BASE_URL ?>/views/sklep/historia-zamowien" title="Historia zamówień">Historia zamówień</a></li>
                <li><a style="margin-top:10px;" class="readmore" href="<?= BASE_URL ?>/views/sklep/zmien-haslo" title="Zmiana hasła">Zmiana hasła</a></li>
                <li><form method="post"><input style="margin-top:10px; background: #fff;" class="readmore" type="submit" name="logout" value="WYLOGUJ"></form></li>
            </ul>
            <? } else { ?>
            <h1>Zaloguj</h1>
            <p style="color: green;"><?= printMsg() ?></p>
            <p style="color: red;"><?= printErrorMsg() ?></p>
            <form method="post">
                <div class="form-group">
                    <input type="text" name="email" placeholder="Email" />
                </div>
                <div class="form-group">
                    <input type="password" name="haslo" placeholder="Hasło" />
                </div>
                <div class="form-group">
                    <input type="checkbox" id="login" />
                    <label for="login"><span></span>Zapamiętaj mnie</label>
                </div>
                <div class="form-group">
                    <p><a href="<?= BASE_URL ?>/views/sklep/przypomnij-haslo">Nie pamiętasz hasła?</a></p>
                </div>
                <div class="form-group">
                    <input type="submit" name="loguj" value="Zaloguj" />
                </div>
            </form>
            <? } ?>
        </div>
        
        <!-- start / slider  -->
        <div class="col-md-5 col-sm-12 col-xs-12 news">
            <? newest_products_slider(); ?>
         </div>
         <!-- end / slider  -->
    </div>
</section>

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script> 
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.registerAccordion.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script> 

<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>  
 
</body>
</html>