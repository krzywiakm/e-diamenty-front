<? include '../../func.php'; ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Znajdź diament – Katalog - diamenty jubilerskie, brylanty, diamenty inwestycyjne</title>
	<meta name="description" content="Katalog zawiera pełną ofertę na naturalne diamenty jubilerskie, diamenty inwestycyjne, diamenty z certyfikatami oraz brylanty w doskonałych szlifach" />
	<meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="canonical" href="https:<?= BASE_URL ?>/sklep/katalog" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/tooltipster.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/themes/tooltipster-light.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/ion.rangeSlider.css" />
    <link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/ion.rangeSlider.skinNice.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/component.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]-->
    <script>
      (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2145518,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
      })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <script> window['_fs_debug'] = false; window['_fs_host'] = 'fullstory.com'; window['_fs_script'] = 'edge.fullstory.com/s/fs.js'; window['_fs_org'] = '108ATS'; window['_fs_namespace'] = 'FS'; (function(m,n,e,t,l,o,g,y){ if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;} g=m[e]=function(a,b,s){g.q?g.q.push([a,b,s]):g._api(a,b,s);};g.q=[]; o=n.createElement(t);o.async=1;o.crossOrigin='anonymous';o.src='https://'+_fs_script; y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y); g.identify=function(i,v,s){g(l,{uid:i},s);if(v)g(l,v,s)};g.setUserVars=function(v,s){g(l,v,s)};g.event=function(i,v,s){g('event',{n:i,p:v},s)}; g.anonymize=function(){g.identify(!!0)}; g.shutdown=function(){g("rec",!1)};g.restart=function(){g("rec",!0)}; g.log = function(a,b){g("log",[a,b])}; g.consent=function(a){g("consent",!arguments.length||a)}; g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)}; g.clearUserCookie=function(){}; g.setVars=function(n, p){g('setVars',[n,p]);}; g._w={};y='XMLHttpRequest';g._w[y]=m[y];y='fetch';g._w[y]=m[y]; if(m[y])m[y]=function(){return g._w[y].apply(this,arguments)}; g._v="1.3.0"; })(window,document,window['_fs_namespace'],'script','user'); </script>
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main -->
<section id="main" class="catalog">
    <!-- start / filtr -->
    <div class="wrapper row">
        <div class="col-xs-12">
            <h1 style="text-align: center;">Znajdź diament</h1>
        </div>
        <div class="col-md-12">
            <form id="filtr">
                <div id="basic-filtr">
                    <div class="row filtr1">
                        <div class="col-md-4 col-sm-12 col-xs-12 single">
                            <label>
                                <?= slownik(55) ?> <span>(Shape)</span>
                                <i class="info tooltip" title="<?= slownik(68) ?>"></i>
                            </label>
                            <ul class="choose-shape">
                                <li>
                                    <label for="s1" class="shape s1" title="<?= ksztalt(1); ?>"></label>
                                    <input id="s1" type="checkbox" name="sort_shape" value="s1" />
                                </li>
                                <li>
                                    <label for="s2" class="shape s2" title="<?= ksztalt(9); ?>"></label>
                                    <input id="s2" type="checkbox" name="sort_shape" value="s2" />
                                </li>
                                <li>
                                    <label for="s3" class="shape s3" title="<?= ksztalt(3); ?>"></label>
                                    <input id="s3" type="checkbox" name="sort_shape" value="s3" />
                                </li>
                                <li>
                                    <label for="s4" class="shape s4" title="<?= ksztalt(4); ?>"></label>
                                    <input id="s4" type="checkbox" name="sort_shape" value="s4" />
                                </li>
                                <li>
                                    <label for="s5" class="shape s5" title="<?= ksztalt(2); ?>"></label>
                                    <input id="s5" type="checkbox" name="sort_shape" value="s5" />
                                </li>
                                <li>
                                    <label for="s6" class="shape s6" title="<?= ksztalt(11); ?>"></label>
                                    <input id="s6" type="checkbox" name="sort_shape" value="s6" />
                                </li>
                                <li>
                                    <label for="s7" class="shape s7" title="<?= ksztalt(5); ?>"></label>
                                    <input id="s7" type="checkbox" name="sort_shape" value="s7" />
                                </li>
                                <li>
                                    <label for="s8" class="shape s8" title="<?= ksztalt(8); ?>"></label>
                                    <input id="s8" type="checkbox" name="sort_shape" value="s8" />
                                </li>
                                <li>
                                    <label for="s9" class="shape s9" title="<?= ksztalt(6); ?>"></label>
                                    <input id="s9" type="checkbox" name="sort_shape" value="s9" />
                                </li>
                                <li>
                                    <label for="s11" class="shape s11" title="<?= ksztalt(7); ?>"></label>
                                    <input id="s11" type="checkbox" name="sort_shape" value="s11" />
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 single">
                            <label>
                                <?= slownik(64) ?> <span>(PLN)</span>
                                <i class="info tooltip" title="<?= slownik(69) ?>"></i>
                            </label>
                            <input id="price" />
                            <input type="text" id="price_from" name="price_from" value="1" class="input-filtr" />
                            <input type="text" id="price_to" name="price_to" value="<?= getHighestPrice(); ?>" class="input-filtr" />
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 single">
                            <label>
                                <?= slownik(56) ?> <span>(Carat)</span>
                                <i class="info tooltip" title="<?= slownik(70) ?>"></i>
                            </label>
                            <input id="mass" />
                            <input type="text" id="mass_from" name="mass_from" value="0.01" class="input-filtr" />
                            <input type="text" id="mass_to" name="mass_to" value="<?= getHighestMass(); ?>" class="input-filtr" />
                        </div>
                    </div>
                    <div class="row filtr2">
                        <div class="col-md-4 col-sm-12 col-xs-12 filter-color single mobile-advanced-filters">
                            <label>
                                <?= slownik(57) ?> <span>(Color)</span>
                                <i class="info tooltip" title="<?= slownik(71) ?>"></i>
                            </label>
                            <input id="color" />
                            <input type="hidden" id="color_from" name="color_from" value="D" />
                            <input type="hidden" id="color_to" name="color_to" value="M" />
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 filter-clarity single mobile-advanced-filters">
                            <label>
                                <?= slownik(58) ?> <span>(Clarity)</span>
                                <i class="info tooltip" title="<?= slownik(72) ?>"></i>
                            </label>
                            <input id="clarity" />
                            <input type="hidden" id="clarity_from" name="clarity_from" value="IF" />
                            <input type="hidden" id="clarity_to" name="clarity_to" value="I3" />
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 filter-availability single mobile-advanced-filters">
                            <label>
                                <?= slownik(60) ?> <span>(Liczba dni)</span>
                                <i class="info tooltip" title="<?= slownik(73) ?>"></i>
                            </label>
                            <input id="availability" />
                            <input type="text" id="availability_from" name="availability_from" value="0" class="input-filtr" readonly />
                            <input type="text" id="availability_to" name="availability_to" value="30" class="input-filtr" readonly />
                        </div>
                    </div>
                </div>
                <div id="advanced-filtr" class="hide-advanced-filters">
                    <div class="row filtr3">
                        <div class="col-md-4 col-sm-12 col-xs-12 filter-cut single">
                            <label>
                                <?= slownik(62) ?>
                                <i class="info tooltip" title="<?= slownik(74) ?>"></i>
                            </label>
                            <input id="cut" />
                            <input type="hidden" id="cut_from" name="cut_from" value="1" />
                            <input type="hidden" id="cut_to" name="cut_to" value="4" />
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 filter-polishing single">
                            <label>
                                <?= slownik(65) ?>
                                <i class="info tooltip" title="<?= slownik(75) ?>"></i>
                            </label>
                            <input id="polishing" />
                            <input type="hidden" id="polishing_from" name="polishing_from" value="1" />
                            <input type="hidden" id="polishing_to" name="polishing_to" value="4" />
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 filter-symmetry single">
                            <label>
                                <?= slownik(61) ?>
                                <i class="info tooltip" title="<?= slownik(76) ?>"></i>
                            </label>
                            <input id="symmetry" />
                            <input type="hidden" id="symmetry_from" name="symmetry_from" value="1" />
                            <input type="hidden" id="symmetry_to" name="symmetry_to" value="4" />
                        </div>
                    </div>
                    <div class="row filtr4">
                        <div class="col-md-4 col-sm-12 col-xs-12 single">
                            <label>
                                <?= slownik(66) ?>
                                <i class="info tooltip" title="<?= slownik(77) ?>"></i>
                            </label>
                            <ul class="list">
                                <li>
                                    <input type="checkbox" name="certificate_gia" id="gia" value="true" /><label for="gia"><span></span>GIA</label>
                                </li>
                                <li>
                                    <input type="checkbox" name="certificate_igi" id="igi" value="true" /><label for="igi"><span></span>IGI</label>
                                </li>
                                <li>
                                    <input type="checkbox" name="certificate_hrd" id="hrd" value="true" /><label for="hrd"><span></span>HRD</label>
                                </li>
                                <li>
                                    <input type="checkbox" name="certificate_e_diamenty" id="ediamenty" value="true" /><label for="ediamenty"><span></span>ediamenty</label>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-8 col-sm-12 col-xs-12 single">
                            <label>
                                <?= slownik(67) ?>
                                <i class="info tooltip" title="<?= slownik(78) ?>"></i>
                            </label>
                            <ul class="choose always-show" style="display: block;">
                                <li>
                                    <label for="violet" class="color violet"></label>
                                    <input data-position="0" id="violet" type="checkbox" name="sort_colour" value="8" />
                                    <p class="color-caption"><?= slownik(38) ?></p>
                                </li>
                                <li>
                                    <label for="red" class="color red"></label>
                                    <input data-position="1" id="red" type="checkbox" name="sort_colour" value="4" />
                                    <p class="color-caption"><?= slownik(34) ?></p>
                                </li>
                                <li>
                                    <label for="purple" class="color purple"></label>
                                    <input data-position="2" id="purple" type="checkbox" name="sort_colour" value="6" />
                                    <p class="color-caption"><?= slownik(36) ?></p>
                                </li>
                                <li>
                                    <label for="pink" class="color pink"></label>
                                    <input data-position="3" id="pink" type="checkbox" name="sort_colour" value="2" />
                                    <p class="color-caption"><?= slownik(32) ?></p>
                                </li>
                                <li>
                                    <label for="orange" class="color orange"></label>
                                    <input data-position="4" id="orange" type="checkbox" name="sort_colour" value="7" />
                                    <p style="word-break: break-all;" class="color-caption"><?= slownik(37) ?></p>
                                </li>
                                <li>
                                    <label for="grey" class="color grey"></label>
                                    <input data-position="5" id="grey" type="checkbox" name="sort_colour" value="9" />
                                    <p class="color-caption"><?= slownik(39) ?></p>
                                </li>
                                <li>
                                    <label for="green" class="color green"></label>
                                    <input data-position="6" id="green" type="checkbox" name="sort_colour" value="5" />
                                    <p class="color-caption"><?= slownik(35) ?></p>
                                </li>
                                <li>
                                    <label for="cognac" class="color cognac"></label>
                                    <input data-position="7" id="cognac" type="checkbox" name="sort_colour" value="13" />
                                    <p class="color-caption"><?= slownik(43) ?></p>
                                </li>
                                <li>
                                    <label for="champagne" class="color champagne"></label>
                                    <input data-position="8" id="champagne" type="checkbox" name="sort_colour" value="12" />
                                    <p class="color-caption"><?= slownik(42) ?></p>
                                </li>
                                <li>
                                    <label for="chameleon" class="color chameleon"></label>
                                    <input data-position="9" id="chameleon" type="checkbox" name="sort_colour" value="14" />
                                    <p class="color-caption"><?= slownik(44) ?></p>
                                </li>
                                <li>
                                    <label for="yellow" class="color yellow"></label>
                                    <input data-position="10" id="yellow" type="checkbox" name="sort_colour" value="1" />
                                    <p class="color-caption"><?= slownik(31) ?></p>
                                </li>
                                <li>
                                    <label for="brown" class="color brown"></label>
                                    <input data-position="11" id="brown" type="checkbox" name="sort_colour" value="11" />
                                    <p class="color-caption"><?= slownik(41) ?></p>
                                </li>
                                <li>
                                    <label for="white" class="color white"></label>
                                    <input data-position="12" id="white" type="checkbox" name="sort_colour" value="15" />
                                    <p class="color-caption"><?= slownik(45) ?></p>
                                </li>
                                <li>
                                    <label for="blue" class="color blue"></label>
                                    <input data-position="13" id="blue" type="checkbox" name="sort_colour" value="3" />
                                    <p class="color-caption"><?= slownik(33) ?></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12 single">
                        <a href="#" title="Reset Filters" class="reset-filters"><?= slownik(54) ?></a>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-4 single">
                        <a href="#" title="Wyszukiwania zaawansowane" class="advanced-search down">Wyszukiwania zaawansowane</a>
                    </div>
                </div>
            </form>
            <span class="catalog-scroll-down"></span>
        </div>
    </div>
    <!-- end / filtr -->

    <!-- start / header compare -->
    <div class="wrapper row compare col-md-hide">
        <div class="col-md-3">
            <a href="<?= BASE_URL ?>/views/sklep/porownanie" title="Porównaj diamenty" class="comp">Porównaj diamenty (<span class="count"></span>)</a>
        </div>
        <div class="col-md-2 col-md-offset-7">
            <p><span><?= number_of_all_products(); ?></span> <?= slownik(53) ?></p>
        </div>
    </div>
    <!-- end / header compare -->

    <!-- end / table catalog -->
    <div class="wrapper row table">
        <div class="col-md-12">
            <div class="table-responsive">
                <!-- start / cube animate for search result -->
                <div class="spinner">
                     <div class="cube1"></div>
                     <div class="cube2"></div>
                </div>
                <div class="TableHeading">
                    <div class="TableHead head-watch" style="padding-top:15px;"><img src="<?= BASE_URL ?>/assets/images/icons/observ.png" alt="Obserwuj"></div>
                    <div class="TableHead head-compare" style="padding-top:15px;"><img src="<?= BASE_URL ?>/assets/images/icons/compare.png" alt="Porównaj"></div>
                    <div data-orderBy="shape" class="TableHead head-shape"><?= slownik(55) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="mass" class="TableHead head-mass"><?= slownik(56) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="color" class="TableHead head-color"><?= slownik(57) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="clarity" class="TableHead head-clarity"><?= slownik(58) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="certificat" class="TableHead head-certificat"><?= slownik(59) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="availability" class="TableHead head-availability"><?= slownik(60) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="symmetry" class="TableHead head-symmetry"><?= slownik(61) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="cut" class="TableHead head-cut"><?= slownik(62) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="polishing" class="TableHead head-polishing"><?= slownik(63) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="price" class="TableHead head-price"><?= slownik(64) ?><br>
                        <span data-direction="desc" class="arrow up"></span>
                        <span data-direction="asc" class="arrow down"></span>
                    </div>
                    <div data-orderBy="cart" class="TableHead head-cart" style="padding-top:17px;"><img src="<?= BASE_URL ?>/assets/images/icons/add-to-cart-active.png" alt="Dodaj do koszyka"></div>
                </div>
                <!-- end / cube animate for search result -->
                <div id="content-1">
                    <div class="Table">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end / table catalog -->
</section>
<!-- end / main -->

<? footer(); ?>

<script src="https://code.jquery.com/jquery-2.2.2.min.js" integrity="sha256-36cp2Co+/62rEAAYHLmRCPIych47CvdM+uTBJwSzWjI=" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?= BASE_URL ?>/assets/js/jquery.tooltipster.min.js"></script>
<script src="<?= BASE_URL ?>/assets/js/ion.rangeSlider.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.7/jquery.slimscroll.min.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.sliderRangeStorage.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.diamond.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>

<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>
<script>
    $('html, body').animate({
        scrollTop: $("#main").first().offset().top - 20
    }, 2000);
</script>

</body>
</html>
