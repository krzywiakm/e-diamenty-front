<? include '../../func.php'; ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="robots" content="noindex, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link href="<?= BASE_URL ?>/assets/css/jquery.bxslider.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<section id="main" class="login lost-password">
    <div class="wrapper row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1 form">
            <h2>Odzyskaj hasło</h2>
            <form>
                <div class="form-group">
                    <input type="text" placeholder="Imię" />
                </div>
                <div class="form-group">
                    <input type="text" placeholder="Nazwisko" />
                </div>
                <div class="form-group">
                    <input type="email" placeholder="E-mail" />
                </div>
                <div class="form-group">
                    <input type="submit" value="Odzyskaj" />
                </div>
            </form>
        </div>
        
        <!-- start / slider  -->
        <div class="col-md-5 col-sm-12 col-xs-12 news">
            <? newest_products_slider(); ?>
        </div>
         <!-- end / slider  -->
    </div>
</section>

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script> 
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.registerAccordion.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script> 

<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>  
 
</body>
</html>