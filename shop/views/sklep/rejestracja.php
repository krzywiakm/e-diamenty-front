<? include '../../func.php'; ?>
<? 
register(true);
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Rejestracja – sklep e-diamenty.pl – hurtownia brylantów hurtownia diamentów</title>
	<meta name="description" content="Rejestracja w e-diamenty.pl daje dostęp do funkcji: obserwowane diamenty inwestycyjne, porównywane diamenty jubilerskie, rabaty na zakup diamentów" />
	<meta name="robots" content="noindex,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link href="<?= BASE_URL ?>/assets/css/jquery.bxslider.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="main" class="register">
    <div class="wrapper row">
	    <div class="col-md-10 col-md-offset-1">
            <div style="color:red;margin-bottom:10px;font-size: 20px;"><?= printMsg(); ?></div>
        </div>
        <div class="col-md-5 col-md-offset-1 form">
            <h1><?= slownik(93) ?></h1>
            <p style="color:red; margin-bottom: 10px;"><?= printMsg(); ?></p>
            <p class="info"><?= slownik(94) ?></p>
            <form method="post">
                <div class="form-group">
                    <label>Imię</label>
                    <input type="text" name="name" value="<?= $_POST['name'] ?>" />
                </div>
                <div class="form-group">
                    <label>Nazwisko</label>
                    <input type="text" name="surname" value="<?= $_POST['surname'] ?>" />
                </div>
                <div class="form-group">
                    <label>E-mail <span class="required">*</span></label>
                    <input type="email" name="Email" value="<?= $_POST['Email'] ?>" />
                </div>
                <div class="form-group">
                    <label>Telefon</label>
                    <input type="text" name="telefon" value="<?= $_POST['telefon'] ?>" />
                </div>
                <div class="form-group">
                    <label>Kraj</label>
                    <select name="kraj">
                        <option>Polska</option>
                    </select>
                </div>
                <div class=" row form-group">
                    <div class="col-md-6">
                        <label>Kod pocztowy</label>
                        <input type="text" name="kod" value="<?= $_POST['kod'] ?>" />
                    </div>
                    <div class="col-md-6">
                        <label>Miasto</label>
                        <input type="text" name="miasto" value="<?= $_POST['miasto'] ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label>Ulica, nr</label>
                    <input type="text" name="street" value="<?= $_POST['street'] ?>" />
                </div>
                <div class="form-group">
                    <label>Nazwa firmy</label>
                    <input type="text" name="nazwafirmy" value="<?= $_POST['nazwafirmy'] ?>" />
                </div>
                <div class="form-group">
                    <label>NIP</label>
                    <input type="text" name="nip" value="<?= $_POST['nip'] ?>" />
                </div>
                <div class="form-group">
                    <p><span class="required">*</span> pola wymagane</p>
                </div>
                
                <div class="form-group additional">
                    <input type="checkbox" name="chkAdres_dostawy" id="add" />
                    <label for="add"><span></span><?= slownik(95) ?></label>
                </div>
                <!-- start / accordion form -->
                <div id="additional-form">
                    <h3>Adres dostawy</h3>
                    <div class="form-group">
                        <label>Imię i nazwisko</label>
                        <input type="text" name="imieplus" value="<?= $_POST['imieplus'] ?>" />
                    </div>
                    <div class="form-group">
                        <label>Kraj</label>
                        <select name="krajplus">
                            <option value="PL">Polska</option>
                            <option value="DE">Niemcy</option>
                            <option value="IT">Włochy</option>
                            <option value="EN">Anglia</option>
                        </select>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label>Kod pocztowy</label>
                            <input type="text" name="kodplus" value="<?= $_POST['kodplus'] ?>" />
                        </div>
                        <div class="col-md-6">
                            <label>Miasto</label>
                            <input type="text" name="miastoplus" value="<?= $_POST['miastoplus'] ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Ulica, nr</label>
                        <input type="text" name="ulicaplus" value="<?= $_POST['ulicaplus'] ?>" />
                    </div>
                </div>
                <!-- end / accordion form -->
                <hr />
                <div class="form-group rule custom-group">
                    <input type="checkbox" name="rule1" id="rule1" required />
                    <label for="rule1"><span></span><div class="label-content"><span class="asterisk">*</span>Oświadczam, że zapoznałem się z regulaminem sklepu i akceptuję ten regulamin</div></label>
                </div>
                <div class="form-group rule custom-group">
                    <input type="checkbox" name="rule3" id="rule3" required />
                    <label for="rule3"><span></span><div class="label-content"><span class="asterisk">*</span>Wyrażam zgodę na przetwarzanie moich danych osobowych przez DIAMOND INVESTMENT COMPANY T. KWIATKIEWICZ M. KWIATKIEWICZ SPÓŁKA JAWNA (prowadzącego portal e-DIAMENTY.PL) w rozumieniu Rozporządzenia Parlamentu Europejskiego i Rady UE 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE na potrzeby marketingu bezpośredniego towarów i usług. Oświadczam, że zapoznałem się z powyższą klauzulą informacyjną oraz <a href="https://e-diamenty.pl/views/informacje/polityka-prywatnosci" style="text-decoration: underline;">polityką prywatności</a></div></label>
                </div>
                <div class="form-group rule custom-group">
                    <input type="checkbox" name="rule4" id="rule4" />
                    <label for="rule4"><span></span><div class="label-content">Wyrażam zgodę na używanie przez DIAMOND INVESTMENT COMPANY T. KWIATKIEWICZ M. KWIATKIEWICZ SPÓŁKA JAWNA (prowadzącego portal e-DIAMENTY.PL) w kontaktach ze mną, telekomunikacyjnych urządzeń końcowych w celu prowadzenia marketingu bezpośredniego zgodnie z przepisami ustawy z dnia 16 lipca 2004 roku Prawo telekomunikacyjne w celu umówienia spotkania z doradcą oraz realizacji późniejszego kontaktu.</div></label>
                </div>
                <div class="form-group rule custom-group">
                    <input type="checkbox" name="rule2" id="rule2" />
                    <label for="rule2"><span></span><div class="label-content">Chcę otrzymywać newsletter od i informacje handlowe drogą elektroniczną od DIAMOND INVESTMENT COMPANY T. KWIATKIEWICZ M. KWIATKIEWICZ SPÓŁKA JAWNA (prowadzącego portal e-DIAMENTY.PL) na wskazany przeze mnie adres e-mail w rozumieniu art. 10 ust. 1 ustawy z dnia 18.07.2002 o świadczeniu usług drogą elektroniczną.</div></label>
                </div>
                <div class="form-group">
                    <input type="submit" name="rejestruj" value="Rejestracja" />
                </div>
            </form>
        </div>
        
        <!-- start / slider  -->
        <div class="col-md-5 news">
            <? newest_products_slider(); ?>
        </div>
         <!-- end / slider  -->
    </div>
</section>
<!-- end / main  -->

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script> 
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.registerAccordion.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script> 
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>  
 
</body>
</html>
