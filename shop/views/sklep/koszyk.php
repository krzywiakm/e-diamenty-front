<? include '../../func.php'; ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Koszyk – Twoje zamówione diamenty w realnych cenach hurtowych – e-diamenty.pl</title>
	<meta name="description" content="Koszyk sklepu e-diamenty.pl – w koszyku znajdują się wybrane przez klientów diamenty jubilerskie oraz diamenty inwestycyjne w realnych cenach hurtowych" />
	<meta name="robots" content="noindex,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/tooltipster.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/themes/tooltipster-light.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!-- todo: skrypt dla sklep_rejestracja.html-->
    <link href="<?= BASE_URL ?>/assets/css/jquery.bxslider.css" rel="stylesheet" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main -->
<section id="main" class="cart">
    <div class="wrapper row">
        <!-- start / heading  -->
        <div class="col-md-12 heading">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 left">
                    <h1>Koszyk</h1>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 right">
                    <a href="<?= BASE_URL ?>/views/sklep/katalog" title="Kontynuuj zakupy">Kontynuuj zakupy</a>
                </div>
            </div>
        </div>
        <!-- end / heading  -->
        
        <!-- start / table cart -->
        <div class="col-md-12 cart-table">
            <div class="table-responsive">
                <form id="cart" method="POST" action="#">
                    <table>
                        <thead>
                            <tr>
                                <th class="product-name">Diament</th>
                                <th class="product-availability">Dostępność</th>
                                <th class="product-quantity">Ilość</th>
                                <th class="product-price">Cena</th>
                                <th class="product-value">Wartość</th>
                                <th class="product-remove">Usuń</th>
                            </tr>
                        </thead>
                        <tbody class="cart-details"></tbody>
                    </table>
                </form>
            </div>
        </div>
        <!-- end / table cart -->
        
        <!-- start / subtotal -->
        <div class="row wrapper cart-subtotal">
            <div class="col-md-12">
                <div class="content">
                    <div class="row">
                        <div class="col-md-6 left">
                            <div class="row">
                                <div class="col-md-6 name">
                                    <h3>Czas realizacji  zamówienia<i class="info tooltip" title="<?= slownik(100) ?>"></i></h3>
                                </div>
                                <div class="col-md-5 detail">
                                    <span class="day"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 right">
                            <div class="row">
                                <div class="col-md-6 name">
                                    <h3>Wartość produktów w koszyku</h3>
                                </div>
                                <div class="col-md-6 detail">
                                    <span class="price"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- start / subtotal -->
        
        <? if(!auth() || ($_SESSION['BezRej'] == true) ) { ?>
        <!-- start / user area -->
        <div class="wrapper row user-area">
            <div class="col-md-4 login">
                <h4>Zaloguj</h4>
                <form method="post">
                    <div style="color: green;"><?= printMsg() ?></div>
                    <div style="color: red;"><?= printErrorMsg() ?></div>
                    <input type="text" placeholder="Email" name="email" />
                    <input type="password" placeholder="*****" name="haslo" />
                    <input type="checkbox" name="remember_me" id="remember_me" value="remember_me" /><label for="remember_me"><span></span>Zapamiętaj mnie</label>
                    <input type="submit" name="loguj" value="Zaloguj" />
                </form>
            </div>
            <div class="col-md-3 col-md-offset-1 buy-reg">
                <h4><?= slownik(101); ?></h4>
                <p><?= slownik(102); ?></p>
                <a href="<?= BASE_URL ?>/views/sklep/koszyk-krok1?bezrej=false" title="<?= slownik(101); ?>"><?= slownik(101); ?></a>
            </div>
            <div class="col-md-3 col-md-offset-1 buy-not-reg">
                <h4><?= slownik(103); ?></h4>
                <p><?= slownik(104); ?></p>
                <a href="<?= BASE_URL ?>/views/sklep/koszyk-krok1?bezrej=true" title="<?= slownik(103); ?>"><?= slownik(103); ?></a>
            </div>
        </div>
        <!-- end / user area -->
        <? } else { ?>
        <div class="wrapper row user-area">
            <div class="buy-not-reg">
                <a style="float: right;" href="<?= BASE_URL ?>/views/sklep/koszyk-krok1" title="<?= slownik(105); ?>"><?= slownik(105); ?></a>
            </div>
        </div>
        <? } ?>    
    </div>
    
    <!-- start / accordion -->
    <div class="wrapper row faq">
        <div class="col-md-12 heading">
            <h2><?= slownik(85) ?></h2>
        </div>
        <div class="col-md-12">
            <div class="accordion-group">
                <div class="accordion" id="section-1">
                    <h3><?= slownik(79) ?></h3><span></span>
                </div>
                <div class="accordion-content">
                    <p><?= slownik(80) ?></p>
                </div>
            </div>
            <div class="accordion-group">
                <div class="accordion" id="section-1">
                    <h3><?= slownik(81) ?></h3><span></span>
                </div>
                <div class="accordion-content">
                    <p>-<?= slownik(82) ?></p>
                </div>
            </div>
            <div class="accordion-group">
                <div class="accordion" id="section-1">
                    <h3><?= slownik(83) ?></h3><span></span>
                </div>
                <div class="accordion-content">
                    <p><?= slownik(84) ?></p>
                </div>
            </div>
        </div>
    </div>
    <!-- end / accordion -->
</section>
<!-- end / main -->

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= BASE_URL ?>/assets/js/jquery.tooltipster.min.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.accordion.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.accordion').accordion({
        //defaultOpen: 'section-1',
    });
  });
</script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.cart.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>  
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>   
</body>
</html>
