<? include '../../func.php'; ?>
<? 
if(auth()) 
{ 
    $current_user = getUserData($_SESSION["USER_ID"]); 
}
else 
{
    header('Location: ' . BASE_URL . '/views/sklep/koszyk');
    die();
}
session_start();
$session_id = md5(session_id().date("YmdHis"));
$zamowienie = proccessTransaction($_GET["id"], $session_id);
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <? if($_POST['purchase']) { ?>
    <script>
        sessionStorage.setItem('cart', "[]");
    </script>
    <? } ?>
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="main" class="cart steps">
    <!-- start / cart - step 3 -->
    <div class="wrapper row">
        <div class="col-md-12 step-3">
            <div id="step-container">
                <ul>
                    <li class="col-md-4 tab">1. nabywca/odbiorca</li>
                    <li class="col-md-4 tab">2. Przeysłka i Płatność</li>
                    <li class="col-md-4 tab active">3. podsumowanie</li>
                </ul>
                
                <div id="step-3" class="panel-container">
                    <div class="col-md-12 name">
                        <h2>Podsumowanie zamówienia</h2>
                        <p>Numer zamówienia: <span class="sku"><?= $zamowienie['Id']; ?></span></p>
                    </div>
                    <table class="cart-review">
                        <thead>
                            <tr>
                                <th class="product-name">Artykuł</th>
                                <th class="product-availability">Dostepność</th>
                                <th class="product-quantity">Ilość</th>
                                <th class="product-price">Wartość brutto</th>
                            </tr>
                        </thead>
                        <tbody>
                        <? foreach ($zamowienie['Products'] as $product) { ?>
                            <tr>
                                <td class="product-name"><a href="#" title="#"><?= getProductFullName($product['Id']); ?></a></td>
                                <td class="product-availability">
                                <? 
                                $dostepnosc = dostepnosc($product['RodzajZestawienia']);
                                if ($dostepnosc == 0) {
                                        print "Od ręki";
                                    }
                                    else {
                                        print $dostepnosc . ' dni';
                                    }
                                ?>
                                </td>
                                <td class="product-quantity"><?= $product['Qty']; ?></td>
                                <td class="product-price"><?= $product['Brutto']; ?> PLN </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>

                    <div class="row cart-subtotal">
                        <div class="col-md-12">
                            <div class="content">
                                <div class="row item">
                                    <div class="col-md-6 left">Czas realizacji zamówienia</div>
                                    <div class="col-md-6 right">
                                    <? 
                                    $dostepnosc = dostepnosc($zamowienie['Products'][0]['RodzajZestawienia']);
                                    if ($dostepnosc == 0) {
                                            print "Od ręki";
                                        }
                                        else {
                                            print $dostepnosc . ' dni';
                                        }
                                    ?>
                                    </div>
                                </div>
                                <div class="row item">
                                    <div class="col-md-6 left">Dostawa</div>
                                    <div class="col-md-6 right"><?= $zamowienie['KosztPrzesylki'] ?> PLN</div>
                                </div>
                                <div class="row item total">
                                    <div class="col-md-6 left">Suma</div>
                                    <div class="col-md-6 right"><?= $zamowienie['DoZaplaty'] ?> PLN </div>
                                </div>
                            </div>
                            <div class="row resume-detail">
                                <div class="col-md-12">
                                    <? if($zamowienie['Przesylka']){ ?>
                                        <div class="detail-group">
                                            <h3>Przesyłka</h3>
                                            <h3><?= $zamowienie['Przesylka']; ?></h3>
                                        </div>
                                    <? } ?>
                                    <? if($zamowienie['Zaplacone'] != 1){ ?>
                                        <div class="detail-group">
                                            <h3>Sposób płatności</h3>
                                        </div>
                                        <? if($zamowienie['Platnosc'] == 4){ ?>
                                            
                                            <div class="detail-group">
                                                <form action="https:<?= BASE_URL ?>/p24gate.php" method="post" class="form" style="display:inline-block;">
                                                 <input type="hidden" name="p24_session_id" value="<?= $session_id; ?>" />
                                                 <input type="hidden" name="p24_merchant_id" value="42182" />
                                                 <input type="hidden" name="p24_pos_id" value="42182" />
                                                 <input type="hidden" name="p24_amount" value="<?= $zamowienie['DoZaplaty']*100 ?>" />
                                                 <input type="hidden" name="p24_currency" value="PLN" />
                                                 <input type="hidden" name="p24_description" value="<?= $zamowienie['Id'] ?>" />
                                                 <input type="hidden" name="p24_client" value="<?= $current_user['Imie'] ?> <?= $current_user['Nazwisko'] ?>" />
                                                 <input type="hidden" name="p24_address" value="<?= $current_user['Ulica'] ?>" />
                                                 <input type="hidden" name="p24_zip" value="<?= $current_user['KodPocztowy'] ?>" />
                                                 <input type="hidden" name="p24_city" value="<?= $current_user['Miasto'] ?>" />
                                                 <input type="hidden" name="p24_country" value="PL" />
                                                 <input type="hidden" name="p24_email" value="<?= $_SESSION["USER_EMAIL"] ?>" />
                                                 <input type="hidden" name="p24_language" value="pl" />
                                                 <input type="hidden" name="p24_url_return" value="https:<?= BASE_URL ?>/views/sklep/dokonanie-platnosci" />
                                                 <input type="hidden" name="p24_url_status" value="https:<?= BASE_URL ?>/p24client.php" />
                                                 <input type="hidden" name="p24_api_version" value="3.2" />
                                                 <input type="hidden" name="p24_sign" value="<?= md5($session_id."|".'42182'."|". $zamowienie['DoZaplaty']*100 ."|".'PLN'."|".'2056a6cca85ad4f4') ?>" />
                                                 <input name="submit_send" value="ZAPŁAĆ ONLINE" type="submit" class="btn online-pay" />
                                                </form>
                                                <img style="margin-left:40px;" src="<?= BASE_URL ?>/assets/images/footer/przelewy24.png" alt="Przelewy24" />
                                            </div>
                                        <? } else if($zamowienie['Platnosc'] == 1) { ?>
                                            <?= renderPageContent('konto-bankowe'); ?>
                                            <div class="detail-group">
                                                <h3>Tytułem</h3>
                                                <p><?= $zamowienie['Id']; ?></p>
                                            </div>
                                        <? } else if($zamowienie['Platnosc'] == 2) { ?>
                                            <div class="detail-group">
                                                <h3>Pobranie</h3>
                                            </div>
                                        <? } else if($zamowienie['Platnosc'] == 3) { ?>
                                            <div class="detail-group">
                                                <h3>Gotówka przy odbiorze</h3>
                                            </div>
                                        <? } ?>
                                    <? } else { ?>
                                        <div class="detail-group">
                                            <h3>Zamówienie opłacone</h3>
                                        </div>
                                    <? } ?>
                                </div>
                            </div>
                            <div class="row resume-info">
                                <div class="col-md-12">
                                    <? if($zamowienie['Platnosc'] == 1) { ?>
                                        <p>Numer konta prześlemy również w mailu potwierdzającym zamówienie.</p>
                                    <? } ?>
                                    <p>Zamówienie zostanie wysłane po otrzymaniu płatności.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row actions">
                    <div class="col-md-12">
                        <div class="thanks">Dziękujemy za złożenie zamówienia</div>
                    </div>
                    <div class="col-md-6 continue"><a href="<?= BASE_URL ?>/views/sklep/katalog" title="Kontynuuj zakupy">Kontynuuj zakupy</a></div>
                </div>
            </div>
        </div>
    </div>
    <!-- end / cart - step 3 -->
    
    <!-- start / accordion -->
    <div class="wrapper row faq">
        <div class="col-md-12 heading">
            <h2><?= slownik(85) ?></h2>
        </div>
        <div class="col-md-12">
            <div class="accordion-group">
                <div class="accordion" id="section-1">
                    <h3><?= slownik(79) ?></h3><span></span>
                </div>
                <div class="accordion-content">
                    <p><?= slownik(80) ?></p>
                </div>
            </div>
            <div class="accordion-group">
                <div class="accordion" id="section-1">
                    <h3><?= slownik(81) ?></h3><span></span>
                </div>
                <div class="accordion-content">
                    <p>-<?= slownik(82) ?></p>
                </div>
            </div>
            <div class="accordion-group">
                <div class="accordion" id="section-1">
                    <h3><?= slownik(83) ?></h3><span></span>
                </div>
                <div class="accordion-content">
                    <p><?= slownik(84) ?></p>
                </div>
            </div>
        </div>
    </div>
    <!-- end / accordion -->
</section>
<!-- start / main  -->
<? if ($_POST['purchase']) { ?>
<script>
window.dataLayer = window.dataLayer || []
dataLayer.push({
   'transactionId': '<?= $zamowienie['Id'] ?>',
   'transactionTotal': <?= $zamowienie['DoZaplaty'] ?>,
   'transactionProducts': [
   <? 
   $numberOfProducts = count($zamowienie['Products']);
   $i = 1;
   ?>
   <? foreach ($zamowienie['Products'] as $product) { ?>
   {
       'sku': '<?= $product['Id']; ?>',
       'name': '<?= getProductFullName($product['Id']); ?>',
       'price': <?= $product['Brutto']; ?>,
       'quantity': <?= $product['Qty']; ?>
   }<? if ($numberOfProducts > 1 && $numberOfProducts != $i) {
       print ',';
   } $i++; } ?>]
});
</script>
<? } ?>
<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.accordion.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.accordion').accordion({
        //defaultOpen: 'section-1',
    });
  });
</script> 
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>  
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>   
</body>
</html>

