<? include '../../func.php'; ?>
<?
if(auth()) 
{ 
    $current_user = getUserData($_SESSION["USER_ID"]); 
}
else 
{
    header('Location: ' . BASE_URL . '/views/sklep/koszyk');
    die();
}
$delivery_options = getAllDeliveryOptions();
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Koszyk krok drugi - e-diamenty.pl</title>
	<meta name="description" content="" />
	<meta name="robots" content="noindex, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="main" class="cart steps">
    <!-- end / cart - step 1 -->
    <div class="wrapper row">
        <div class="col-md-9 step-2">
            <div id="step-container">
                <ul>
                    <li class="col-md-4 tab">1. nabywca/odbiorca</li>
                    <li class="col-md-4 tab active">2. Przeysłka i Płatność</li>
                    <li class="col-md-4 tab">3. podsumowanie</li>
                </ul>
                <div id="step-2" class="panel-container">
                    <form id="purchase-form" method="post" action="<?= BASE_URL ?>/views/sklep/podsumowanie">
                    <div class="user-detail">
                        <div class="col-md-4 detail">
                            <h2>Nabywca/odbiorca</h2>
                            <p class="user-name"><?= $current_user['Imie']; ?> <?= $current_user['Nazwisko']; ?></p>
                        </div>
                        <div class="col-md-4 detail">
                            <h2>Adres dostawy</h2>
                            <p class="street"><?= $current_user['Ulica']; ?></p>
                            <p class="address"><?= $current_user['Kod Pocztowy']; ?> <?= $current_user['Miejscowosc']; ?></p>
                        </div>
                        
                        <div class="col-md-12">
                            <span class="sep"></span>
                        </div>
                    </div>
                        <div class="col-md-12 shippment">
                            <h3>Wybierz rodzaj przesyłki</h3>
                            <div class="content">
                                <div class="row form-group form-country">
                                    <label class="col-md-3">Wybierz kraj dostawy</label>
                                    <select class="col-md-4">
                                        <option>Polska</option>
                                    </select>
                                </div>
                                <div class="row form-group form-shippment">
                                    <ul>
                                    <? foreach ($delivery_options as $option) { ?>
                                        <li class="shippment<?= $option['Id']; ?>">
                                            <input name="shippment" type="radio" value="<?= $option['Id']; ?>" id="shippment<?= $option['Id']; ?>" />
                                            <label for="shippment<?= $option['Id']; ?>">
                                                <span></span><i><?= $option['Nazwa']; ?><abbr><div style="display:inline;" class="shippment-cost"><?= $option['Koszt1']; ?></div> PLN</abbr></i>
                                            </label>
                                        </li>
                                    <? } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12 payment">
                            <h3>Sposób płatności</h3>
                            <div class="content">
                                <div class="row form-group form-payment">
                                    <ul>
                                        <li class="option-przelew">
                                            <input name="payment" type="radio" value="1" id="payment1" />
                                            <label for="payment1">
                                                <span></span><i>Przelew na konto</i>
                                            </label>
                                        </li>
                                        <li class="option-pobranie">
                                            <input name="payment" type="radio" value="2" id="payment2" />
                                            <label for="payment2">
                                                <span></span><i>Pobranie</i>
                                            </label>
                                        </li>
                                        <li class="option-gotowka">
                                            <input name="payment" type="radio" value="3" id="payment3"  />
                                            <label for="payment3">
                                                <span></span><i>Gotówka przy odbiorze</i>
                                            </label>
                                        </li>
                                        <li class="option-przelewy24">
                                            <input name="payment" type="radio" value="4" id="payment4"  />
                                            <label for="payment4">
                                                <span></span><i><img src="<?= BASE_URL ?>/assets/images/cart/przelewy24.png" alt="Przelewy24" /></i>
                                            </label>
                                        </li>
                                    </ul>
                                </div>   
                            </div>
                        </div>

                        <div class="col-md-12 invoice">
                            <h3>Paragon/Faktura</h3>
                            <div class="content">
                                <div class="row form-group form-invoice">
                                    <ul>
                                        <li>
                                            <input name="invoice" type="radio" value="invoice1" id="invoice1" />
                                            <label for="invoice1">
                                                <span></span><i>Faktura</i>
                                            </label>
                                        </li>
                                        <li>
                                            <input name="invoice" type="radio" value="invoice2" id="invoice2" checked="" />
                                            <label for="invoice2">
                                                <span></span><i>Paragon</i>
                                            </label>
                                        </li>
                                    </ul>
                                </div>  
                            </div>
                        </div>
                        <input type="hidden" name="purchase" value="kup">
                </div>
                <div class="row cart-comments">
                    <div class="col-md-12">
                        <h3>Komentarz do zamówienia</h3>
                        <textarea name="comment" rows="4"></textarea>
                    </div>
                </div>
                </form>
                <div class="row cart-total">
                    <div class="col-md-12">
                        <table>
                            <tr class="cart-delivery">
                                <td>Dostawa</td>
                                <td class="delivery-amount">0.00 PLN</td>
                            </tr>
                            <tr class="cart-total">
                                <td>Suma</td>
                                <td class="total-amount"></td>
                            </tr>
                    </table>
                    </div>
                </div>
                <div class="row actions">
                    <div class="col-md-6 continue"><a href="#" title="Kontynuuj zakupy">Kontynuuj zakupy</a></div>
                    <div class="col-md-6 nextstep"><a href="#" onclick="sendOrder(event)" title="Wyślij zamówienie">Wyślij zamówienie</a></div>
                </div>
            </div>
        </div>
        <!-- start / mini-cart -->
        <div class="col-md-3 mini-cart">
            <aside>
                <div class="aside-heading">
                    <h2>Koszyk</h2>
                    <div class="aside-heading-right">
                        <a href="<?= BASE_URL ?>/views/sklep/koszyk" title="Edytuj">Edytuj</a>
                    </div>
                </div>
                <div class="aside-content">
                    <div class="mini-cart-item">
                        <ul>
                        </ul>
                    </div>
                    
                    <div class="mini-cart-resume">
                        <ul>
                            <li>
                                <p>czas realizacji zamówienia</p>
                                <span class="day"></span>
                            </li>
                            <li>
                                <p>wartość produktów w koszyku</p>
                                <span class="resume-amount"></span>
                            </li>
                        </ul>
                    </div>
                    
                    <div class="mini-cart-total">
                        <p>Razem</p>
                        <span class="total-amount"></span>
                    </div>
                </div>
            </aside>
        </div>
        <!-- end / mini-cart -->
    </div>
    <!-- end / cart - step 2 -->
    
    <!-- start / accordion -->
    <div class="wrapper row faq">
        <div class="col-md-12 heading">
            <h2><?= slownik(85) ?></h2>
        </div>
        <div class="col-md-12">
            <div class="accordion-group">
                <div class="accordion" id="section-1">
                    <h3><?= slownik(79) ?></h3><span></span>
                </div>
                <div class="accordion-content">
                    <p><?= slownik(80) ?></p>
                </div>
            </div>
            <div class="accordion-group">
                <div class="accordion" id="section-1">
                    <h3><?= slownik(81) ?></h3><span></span>
                </div>
                <div class="accordion-content">
                    <p>-<?= slownik(82) ?></p>
                </div>
            </div>
            <div class="accordion-group">
                <div class="accordion" id="section-1">
                    <h3><?= slownik(83) ?></h3><span></span>
                </div>
                <div class="accordion-content">
                    <p><?= slownik(84) ?></p>
                </div>
            </div>
        </div>
    </div>
    <!-- end / accordion -->
</section>
<!-- end / main  -->

<? footer(); ?>



<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.accordion.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.accordion').accordion({
        //defaultOpen: 'section-1',
    });
  });
</script> 
<script>
    function sendOrder(event) {
        event.preventDefault();
        if ($('input[name="shippment"]:checked').val() != undefined && 
            $('input[name="payment"]:checked').val() != undefined) {
            document.getElementById('purchase-form').submit();
        }
        else {
            alert("Wybierz rodzaj przesyłki i sposób płatnośći");
        }
    }
</script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>  
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.cart.js"></script>   
</body>
</html>
