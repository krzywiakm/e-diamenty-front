<? include '../../func.php'; ?>
<? 
$register = register();
if(auth()  && ($_SESSION["BezRej"] != true) ) { 
    header('Location: ' . BASE_URL . '/views/sklep/koszyk-krok2');
    die();
} ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Koszyk krok pierwszy – zakup diamentów bez rejestracji w sklepie e-diamenty.pl</title>
	<meta name="description" content="Zakup diamentów na e-diamenty.pl dla niezarejestrowanych użytkowników hurtowni diamentów, jednorazowy zakup diamentu z certyfikatem autentyczności" />
	<meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="canonical" href="https://e-diamenty.pl/views/sklep/koszyk-krok1" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="main" class="cart steps">
    <!-- start / cart - step 1 -->
    <div class="wrapper row">
        <div class="col-xs-12">
            <h1 style="margin-bottom: 18px;">Koszyk krok pierwszy</h1>
        </div>
        <div class="col-md-9 step-1">
            <div id="step-container">
                <ul>
                    <li class="col-md-4 tab active">1. nabywca/odbiorca</li>
                    <li class="col-md-4 tab">2. Przeysłka i Płatność</li>
                    <li class="col-md-4 tab">3. podsumowanie</li>
                </ul>
                <div id="step-1" class="panel-container">
                    <div class="col-md-12 name">
                        <h1 style="color:red; margin-bottom:20px;"><?= printMsg(); ?></h1>
                    </div>
                    <div class="col-md-12 name">
                        <h2>Dane kupującego</h2>
                    </div>
                    <form id="cart-register" method="post">
                        <div class="col-md-6 left">
                            <div class="form-group">
                                <label>Imię <span class="required">*</span></label>
                                <input type="text" name="name" value="<?= $_POST['name'] ?>" />
                            </div>
                            <div class="form-group">
                                <label>Nazwisko <span class="required">*</span></label>
                                <input type="text" name="surname" value="<?= $_POST['surname'] ?>" />
                            </div>
                            <div class="form-group">
                                <label>Kraj <span class="required">*</span></label>
                                <select name="kraj">
                                    <option>Polska</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ulica<span class="required">*</span></label>
                                <input type="text" name="street" value="<?= $_POST['street'] ?>" />
                            </div>
                            <div class=" row form-group">
                                <div class="col-md-6">
                                    <label>Kod pocztowy <span class="required">*</span></label>
                                    <input type="text" name="kod" value="<?= $_POST['kod'] ?>" />
                                </div>
                                <div class="col-md-6">
                                    <label>Miasto <span class="required">*</span></label>
                                    <input type="text" name="miasto" value="<?= $_POST['miasto'] ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <p><span class="required">*</span> pola wymagane</p>
                            </div>
                        </div>
                        <div class="col-md-6 right">
                            <div class="form-group">
                                <label>E-mail<span class="required">*</span></label>
                                <input type="text" name="Email" value="<?= $_POST['Email'] ?>" />
                            </div>
                            <div class="form-group">
                                <label>Telefon<span class="required">*</span></label>
                                <input type="text" name="telefon" value="<?= $_POST['telefon'] ?>" />
                            </div>
                            <div style="padding-bottom: 43px;" class="form-group blank"></div>
                            <div class="form-group">
                                <label>Nazwa firmy</label>
                                <input type="text" name="nazwafirmy" value="<?= $_POST['nazwafirmy'] ?>" />
                            </div>
                            <div class="form-group">
                                <label>NIP</label>
                                <input type="text" name="nip" value="<?= $_POST['nip'] ?>" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group additional">
                                <input type="checkbox" id="add" />
                                <label for="add"><span></span><?= slownik(95) ?></label>
                            </div>
                            <!-- end / accordion form -->
                            <div id="additional-form">
                                <div class="form-group">
                                    <label>Imię i nazwisko <span class="required">*</span></label>
                                    <input type="text" name="imieplus" value="<?= $_POST['imieplus'] ?>" />
                                </div>
                                <div class="form-group">
                                    <label>Kraj <span class="required">*</span></label>
                                    <select name="krajplus">
                                        <option>Polska</option>
                                        <option>Niemcy</option>
                                        <option>Włochy</option>
                                        <option>Anglia</option>
                                    </select>
                                </div>
                                <div class=" row form-group">
                                    <div class="col-md-6">
                                        <label>Kod pocztowy <span class="required">*</span></label>
                                        <input type="text" name="kodplus" value="<?= $_POST['kodplus'] ?>" />
                                    </div>
                                    <div class="col-md-6">
                                        <label>Miasto <span class="required">*</span></label>
                                        <input type="text" name="miastoplus" value="<?= $_POST['miastoplus'] ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Ulica, nr <span class="required">*</span></label>
                                    <input type="text" name="ulicaplus" value="<?= $_POST['ulicaplus'] ?>" />
                                </div>
                            </div>
                            <!-- end / accordion form -->
                        </div>
                        
                        <div class="col-md-12">
                            <!-- <div class="form-group rule">
                                <input type="checkbox" name="rule1" id="rule1" />
                                <label for="rule1"><span></span><?= slownik(96) ?></label>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="rule2" id="rule2" />
                                <label for="rule2"><span></span><?= slownik(97) ?></label>
                            </div> -->

                            <div class="form-group rule custom-group">
                                <input type="checkbox" name="rule1" id="rule1" />
                                <label for="rule1"><span></span><div class="label-content"><span class="asterisk">*</span>Oświadczam, że zapoznałem się z regulaminem sklepu i akceptuję ten regulamin</div></label>
                            </div>
                            <div class="form-group rule custom-group">
                                <input type="checkbox" name="rule3" id="rule3" required />
                                <label for="rule3"><span></span><div class="label-content"><span class="asterisk">*</span>Wyrażam zgodę na przetwarzanie moich danych osobowych przez DIAMOND INVESTMENT COMPANY T. KWIATKIEWICZ M. KWIATKIEWICZ SPÓŁKA JAWNA (prowadzącego portal e-DIAMENTY.PL) w rozumieniu Rozporządzenia Parlamentu Europejskiego i Rady UE 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE na potrzeby marketingu bezpośredniego towarów i usług. Oświadczam, że zapoznałem się z powyższą klauzulą informacyjną oraz <a href="https://e-diamenty.pl/views/informacje/polityka-prywatnosci" style="text-decoration: underline;">polityką prywatności</a></div></label>
                            </div>
                            <div class="form-group rule custom-group">
                                <input type="checkbox" name="rule4" id="rule4" />
                                <label for="rule4"><span></span><div class="label-content">Wyrażam zgodę na używanie przez DIAMOND INVESTMENT COMPANY T. KWIATKIEWICZ M. KWIATKIEWICZ SPÓŁKA JAWNA (prowadzącego portal e-DIAMENTY.PL) w kontaktach ze mną, telekomunikacyjnych urządzeń końcowych w celu prowadzenia marketingu bezpośredniego zgodnie z przepisami ustawy z dnia 16 lipca 2004 roku Prawo telekomunikacyjne w celu umówienia spotkania z doradcą oraz realizacji późniejszego kontaktu.</div></label>
                            </div>
                            <div class="form-group rule custom-group">
                                <input type="checkbox" name="rule2" id="rule2" />
                                <label for="rule2"><span></span><div class="label-content">Chcę otrzymywać newsletter od i informacje handlowe drogą elektroniczną od DIAMOND INVESTMENT COMPANY T. KWIATKIEWICZ M. KWIATKIEWICZ SPÓŁKA JAWNA (prowadzącego portal e-DIAMENTY.PL) na wskazany przeze mnie adres e-mail w rozumieniu art. 10 ust. 1 ustawy z dnia 18.07.2002 o świadczeniu usług drogą elektroniczną.</div></label>
                            </div>

                        </div>
                        <input type="hidden" name="rejestruj" value="Dalej" />
                    </form>
                </div>
                <div class="row actions">
                    <div class="col-md-6 col-sm-6 col-xs-12 continue"><a href="#" title="Kontynuuj zakupy">Kontynuuj zakupy</a></div>
                    <div class="col-md-6 col-sm-6 col-xs-12 nextstep"><a href="#" onclick="event.preventDefault();document.getElementById('cart-register').submit();" title="Przejdź dalej">Przejdź dalej</a></div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mini-cart">
            <aside>
                <div class="aside-heading">
                    <h2>Koszyk</h2>
                    <div class="aside-heading-right">
                        <a href="<?= BASE_URL ?>/views/sklep/koszyk" title="Edytuj">Edytuj</a>
                    </div>
                </div>
                <div class="aside-content">
                    <div class="mini-cart-item">
                        <ul>
                        </ul>
                    </div>
                    
                    <div class="mini-cart-resume">
                        <ul>
                            <li>
                                <p>czas realizacji zamówienia</p>
                                <span class="day"></span>
                            </li>
                            <li>
                                <p>wartość produktów w koszyku</p>
                                <span class="resume-amount"></span>
                            </li>
                        </ul>
                    </div>
                    
                    <div class="mini-cart-total">
                        <p>Razem</p>
                        <span class="total-amount"></span>
                    </div>
                </div>
            </aside>
        </div>
    </div>
    <!-- end / cart - step 1 -->
    
    <!-- start / accordion -->
    <div class="wrapper row faq">
        <div class="col-md-12 heading">
            <h2><?= slownik(85) ?></h2>
        </div>
        <div class="col-md-12">
            <div class="accordion-group">
                <div class="accordion" id="section-1">
                    <h3><?= slownik(79) ?></h3><span></span>
                </div>
                <div class="accordion-content">
                    <p><?= slownik(80) ?></p>
                </div>
            </div>
            <div class="accordion-group">
                <div class="accordion" id="section-1">
                    <h3><?= slownik(81) ?></h3><span></span>
                </div>
                <div class="accordion-content">
                    <p>-<?= slownik(82) ?></p>
                </div>
            </div>
            <div class="accordion-group">
                <div class="accordion" id="section-1">
                    <h3><?= slownik(83) ?></h3><span></span>
                </div>
                <div class="accordion-content">
                    <p><?= slownik(84) ?></p>
                </div>
            </div>
        </div>
    </div>
    <!-- end / accordion -->
</section>
<!-- end / main  -->

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.accordion.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.accordion').accordion({
        //defaultOpen: 'section-1',
    });
  });
</script> 
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.registerAccordion.js"></script>
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script> 
<script src="<?= BASE_URL ?>/assets/js/jquery.cart.js"></script>  
</body>
</html>
