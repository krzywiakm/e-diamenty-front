<? include '../../func.php'; ?>
<? 
$cert_id = $_GET["cert-id"];
$product_id = $_GET["id"];

if ($product_id) {
    $product = getProduct($product_id);
    
}
else if($cert_id) {
    $product = getProductByCert($cert_id); 
    $product_id = $product['Id'];
}
else {
    header('Location: ' . BASE_URL . '/views/sklep/katalog');
    die();
}
if ($product['Id'] == null) {
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
    header('Location: ' . BASE_URL . '/404');
}
if ($product_id) {
    $gallery_links = getProductGalleryLinks($product_id);
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title><?= getProductFullName($product_id); ?> - naturalny diament, ceny hurtowe – e-diamenty.pl</title>
	<meta name="description" content="<?= shortProductDescription($product_id) ?>" />
	<? if($cert_id) { ?>
        <meta name="robots" content="noindex,follow">
    <? } else { ?>
        <meta name="robots" content="index, follow" />
    <? } ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="canonical" href="https:<?= BASE_URL ?>/sklep/produkt/<?= $product_id ?>" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/tooltipster.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/themes/tooltipster-light.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/component.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="main" class="product">
    <div class="wrapper row">
        <!-- start / product gallery  -->
        <div class="col-md-5 col-md-offset-1 images">
            <div class="thumbnail">
                <figure>
                    <img id="gallery-main-image" src="<?= getDiamondPhotoUrl($product['Ksztalt'], $product['Kolor']) ?>" alt="<?= ksztalt($product['Ksztalt']) . ' ' . formatMass($product['Masa']) ?> - e-diamenty.pl" />
                </figure>
                <div class="thumbnail-gallery">
                <?php for ($i = 0; $i < count($gallery_links); $i++) { ?>
                    <a href="#"><img src="<?= $gallery_links[$i] ?>" alt="gallery-thumbnail" /></a>
                <?php } ?>
                </div>
            </div>
        </div>
        <!-- end / product gallery  -->
        
        <!-- start / product detail  -->
        <div class="col-md-5 detail">
            <div class="row">
                <div class="col-md-6 summary">
                    <h1><?= getProductFullName($product_id); ?></h1>
                    <div class="amount">
                        <p><?= $product['Brutto']; ?> <span>PLN</span></p>
                    </div>
                    <form style="margin-bottom: 8px;">
                        <input type="checkbox" name="add_to_observe" id="add_to_observe" value="add_to_observe" data-id="<?=$product_id?>" /><label for="add_to_observe"><span></span>Dodaj do obserwowanych</label>
                        <input type="checkbox" name="add_to_compare" id="add_to_compare" value="add_to_compare" data-id="<?=$product_id?>" /><label for="add_to_compare"><span></span>Dodaj do porównania</label>
                    </form>
                    <a href="<?= BASE_URL ?>/views/sklep/porownanie">ZOBACZ PORÓWNANIE</a>
                </div>
                <div class="col-md-6 form">
                <? if(isProductAvailable($product_id)) { ?>
                    <? if ($product['Laboratorium'] == null) { ?>
                        <div id="addtocart">
                            <p>ilość</p>
                            <div>
                                <input data-changeby="minus" data-id="<?= $product['Id'] ?>" type="button" value="" class="qtyminus quantity-single-product" field="quantity" />
                                <input data-id="<?= $product['Id'] ?>" type="number" name="quantity" value="1" class="qty quantity-value" />
                                <input data-changeby="plus" data-id="<?= $product['Id'] ?>" type="button" value="" class="qtyplus quantity-single-product" field="quantity" />
                            </div>
                            <button class="md-trigger product-to-cart" data-id="<?= $product['Id'] ?>">Do koszyka</button>
                        </div>
                    <? } else { ?>
                        <div id="addtocart">
                            <p>ilość</p>
                            <div>
                                <input data-id="<?= $product['Id'] ?>" type="button" value="" class="qtyminus" field="quantity" />
                                <input data-id="<?= $product['Id'] ?>" type="number" name="quantity" value="1" class="qty" readonly="" />
                                <input data-id="<?= $product['Id'] ?>" type="button" value="" class="qtyplus" field="quantity" />
                            </div>
                            <button class="md-trigger product-to-cart" data-id="<?= $product['Id'] ?>">Do koszyka</button>
                        </div>
                    <? } ?>
                <? } else { ?>
                <h1 style="text-align: center;font-size: 20px;margin-top: 45px;">Produkt<br>niedostępny</h1>
                <? } ?>
                </div>
            </div>
            
            <div class="row delivery">
                <div class="col-md-12">
                    <div class="background">
                        <p><a href="<?= BASE_URL ?>/views/obsluga/koszty-wysylki"><?= slownik(86) ?></a></p>
                    </div>
                </div>
            </div>
            
            <div class="row meta">
                <div class="col-md-12">
                    <h2>Cechy produktu</h2>
                    <ul>
                        <li>
                            <span class="value">Kształt</span>
                            <span class="key"><?= ksztalt($product['Ksztalt']); ?></span>
                        </li>
                        <li>
                            <span class="value">Masa</span>
                            <span class="key"><?= formatMass($product['Masa']); ?></span>
                        </li>
                        <li>
                            <span class="value">Barwa</span>
                            <span class="key"><?= barwa($product['Barwa']); ?></span>
                        </li>
                        <li>
                            <span class="value">Czystość</span>
                            <span class="key"><?= czystosc($product['Czystosc']); ?></span>
                        </li>
                        <li>
                            <span class="value">Certyfikat</span>
                            <span class="key"><?= formatCert(certyfikaty($product['Laboratorium'])); ?></span>
                        </li>
                        <li>
                            <span class="value">Dostępność</span>
                            <span class="key"><? if(dostepnosc($product['RodzajZestawienia']) == 0) { 
                                print 'Od ręki'; 
                            } else { 
                                print dostepnosc($product['RodzajZestawienia']) . ' dni'; 
                            } ?></span>
                        </li>
                        <li>
                            <span class="value">Cena</span>
                            <span class="key"><?= $product['Brutto']; ?> PLN</span>
                        </li>
                        <li>
                            <span class="value">Szlif</span>
                            <span class="key"><?= szlif($product['Szlif']); ?></span>
                        </li>
                        <li>
                            <span class="value">Symetria</span>
                            <span class="key"><?= symetria($product['Symetria']); ?></span>
                        </li>
                        <li>
                            <span class="value">Polerowanie</span>
                            <span class="key"><?= polerowanie($product['Polerowanie']); ?></span>
                        </li>
                        <li>
                            <span class="value">Fluorescencja</span>
                            <span class="key"><?= fluorescencja($product['Fluorescencja']); ?></span>
                        </li>
                        <li>
                            <span class="value">Numer certyfikatu</span>
                            <span class="key"><?= $product['Symbol']; ?></span>
                        </li>
                    </ul>
                    <? if(formatCert(certyfikaty($product['Laboratorium'])) == 'GIA') { ?>
                    <a target="_blank" href="http://www.gia.edu/cs/Satellite?pagename=GST%2FDispatcher&childpagename=GIA%2FPage%2FReportCheck&c=Page&cid=1355954554547&reportno=<?=$product['Symbol']?>" title="Zobacz certyfikat" class="rmore">Zobacz certyfikat</a>
                    <?}?>
                    <? if(formatCert(certyfikaty($product['Laboratorium'])) == 'HRD') { ?>
                    <a target="_blank" href="https://my.hrdantwerp.com/?id=34&record_number=<?=$product['Symbol']?>" title="Zobacz certyfikat" class="rmore">Zobacz certyfikat</a>
                    <?}?>
                    <? if(formatCert(certyfikaty($product['Laboratorium'])) == 'IGI') { ?>
                    <a target="_blank" href="http://www.igiworldwide.com/verify.php?r=<?=$product['Symbol']?>" title="Zobacz certyfikat" class="rmore">Zobacz certyfikat</a>
                    <?}?>
                    <a href="#4c" title="Zobacz ten diament na skali jakosci 4C" class="rmore col-md-hide">Zobacz ten diament na skali jakosci 4C</a>
                
                    <button onclick="location.href='<?= BASE_URL ?>/views/kontakt?text=Zapytanie o produkt <?= getProductFullName($product_id); ?> certyfikat: <?=$product['Symbol']?> Wpisz treść swojego pytania:';" class="md-trigger" data-modal="question">Zapytaj o produkt</button>
                </div>
            </div>
         </div>
         <!-- end / product detail  -->
    </div>
    
    <!-- start / product description  -->
    <div id="4c" class="wrapper row">
        <div class="col-md-8 col-md-offset-2 description">
            <article class="col-md-hide">
                <h3><?= slownik(89) ?></h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="diamond-size">
                            <div class="size grosz">
                                <figure>
                                    <img src="<?= BASE_URL ?>/assets/images/product/size/grosz.png" alt="Grosz" />
                                    <figcaption>15,50 mm</figcaption>
                                </figure>
                            </div>
                            <div class="size size-1">
                                <figure>
                                    <img src="<?= BASE_URL ?>/assets/images/product/size/size-1.png" alt="Diament rozmiar 2ct +" />
                                    <figcaption>2 ct +<span>(8.1 mm)</span></figcaption>
                                </figure>
                            </div>
                            <div class="size size-2">
                                <figure>
                                    <img src="<?= BASE_URL ?>/assets/images/product/size/size-2.png" alt="Diament rozmiar 1.75 ct" />
                                    <figcaption>1.75 ct<span>(7.8 mm)</span></figcaption>
                                </figure>
                            </div>
                            <div class="size size-3">
                                <figure>
                                    <img src="<?= BASE_URL ?>/assets/images/product/size/size-3.png" alt="Diament rozmiar 1.5 ct" />
                                    <figcaption>1.5 ct +<span>(7.4 mm)</span></figcaption>
                                </figure>
                                <?
                                    $position_mass = intval(470 - ((floatval($product['Masa']) / 0.025) * 6.63));
                                    if ($position_mass < -175) {
                                       $position_mass = -175;
                                    }
                                ?>
                                <div style="left: <?= $position_mass ?>px;" id="indicator"><span><?= formatMass($product['Masa']); ?> ct</span></div>
                            </div>
                            <div class="size size-4">
                                <figure>
                                    <img src="<?= BASE_URL ?>/assets/images/product/size/size-4.png" alt="Diament rozmiar 1.25 ct" />
                                    <figcaption>1.25 ct<span>(6.9 mm)</span></figcaption>
                                </figure>
                            </div>
                            <div class="size size-5">
                                <figure>
                                    <img src="<?= BASE_URL ?>/assets/images/product/size/size-5.png" alt="Diament rozmiar 1 ct" />
                                    <figcaption>1 ct<span>(6.4 mm)</span></figcaption>
                                </figure>
                            </div>
                            <div class="size size-6">
                                <figure>
                                    <img src="<?= BASE_URL ?>/assets/images/product/size/size-6.png" alt="Diament rozmiar 0.75 ct" />
                                    <figcaption>0.75 ct<span>(5.8 mm)</span></figcaption>
                                </figure>
                            </div>
                            <div class="size size-6">
                                <figure>
                                    <img src="<?= BASE_URL ?>/assets/images/product/size/size-7.png" alt="Diament rozmiar 0.5 ct" />
                                    <figcaption>0.5 ct<span>(5.1 mm)</span></figcaption>
                                </figure>
                            </div>
                            <div class="size size-7">
                                <figure>
                                    <img src="<?= BASE_URL ?>/assets/images/product/size/size-8.png" alt="Diament rozmiar 0.25 ct" />
                                    <figcaption>0.25 ct<span>(4.1 mm)</span></figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            
            <article class="col-md-hide">
                <h3><?= slownik(90) ?></h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="diamond-color">
                            <div class="color color-1 <? if(barwa($product['Barwa']) == 'D') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>D</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/color/color-1.png" alt="Diament barwa D" />
                                </figure>
                            </div>
                            <div class="color color-2 <? if(barwa($product['Barwa']) == 'E') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>E</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/color/color-2.png" alt="Diament barwa E" />
                                </figure>
                            </div>
                            <div class="color color-3 <? if(barwa($product['Barwa']) == 'F') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>F</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/color/color-3.png" alt="Diament barwa F" />
                                </figure>
                            </div>
                            <div class="color color-4 <? if(barwa($product['Barwa']) == 'G') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>G</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/color/color-4.png" alt="Diament barwa G" />
                                </figure>
                            </div>
                            <div class="color color-5 <? if(barwa($product['Barwa']) == 'H') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>H</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/color/color-5.png" alt="Diament barwa H" />
                                </figure>
                            </div>
                            <div class="color color-6 <? if(barwa($product['Barwa']) == 'I') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>I</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/color/color-6.png" alt="Diament barwa I" />
                                </figure>
                            </div>
                            <div class="color color-7 <? if(barwa($product['Barwa']) == 'J') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>J</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/color/color-7.png" alt="Diament barwa J" />
                                </figure>
                            </div>
                            <div class="color color-8 <? if( (barwa($product['Barwa']) == 'K') || (barwa($product['Barwa']) == 'L') || (barwa($product['Barwa']) == 'M') ) { print 'active'; } ?>">
                                <figure>
                                    <figcaption>K-M</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/color/color-8.png" alt="Diament barwa K-M" />
                                </figure>
                            </div>
                            <? $ntoz = array('N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X','Y','Z'); ?>
                            <div class="color color-9 <? if(in_array(barwa($product['Barwa']), $ntoz)) { print 'active'; } ?>">
                                <figure>
                                    <figcaption>N-Z</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/color/color-9.png" alt="Diament barwa N-Z" />
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            
            <article class="col-md-hide">
                <h3><?= slownik(91) ?></h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="diamond-clarity">
                            <div class="clarity clarity-1 <? if(czystosc($product['Czystosc']) == 'IF') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>IF</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/clarity/clarity-1.png" alt="Diament czystość IF" />
                                </figure>
                            </div>
                            <div class="clarity clarity-2 <? if(czystosc($product['Czystosc']) == 'VVS1') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>VVS1</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/clarity/clarity-2.png" alt="Diament czystość VVS1" />
                                </figure>
                            </div>
                            <div class="clarity clarity-3 <? if(czystosc($product['Czystosc']) == 'VVS2') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>VVS2</figcaption>
                                    <span><img src="<?= BASE_URL ?>/assets/images/product/clarity/clarity-3.png" alt="Diament czystość VVS2" /></span>
                                </figure>
                            </div>
                            <div class="clarity clarity-4 <? if(czystosc($product['Czystosc']) == 'VS1') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>VS1</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/clarity/clarity-4.png" alt="Diament czystość VS1" />
                                </figure>
                            </div>
                            <div class="clarity clarity-5 <? if(czystosc($product['Czystosc']) == 'VS2') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>VS2</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/clarity/clarity-5.png" alt="Diament czystość VS2" />
                                </figure>
                            </div>
                            <div class="clarity clarity-6 <? if(czystosc($product['Czystosc']) == 'SI1') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>SI1</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/clarity/clarity-6.png" alt="Diament czystość SI1" />
                                </figure>
                            </div>
                            <div class="clarity clarity-7 <? if(czystosc($product['Czystosc']) == 'SI2') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>SI2</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/clarity/clarity-7.png" alt="Diament czystość SI2" />
                                </figure>
                            </div>
                            <div class="clarity clarity-8 <? if(czystosc($product['Czystosc']) == 'I1') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>I1</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/clarity/clarity-8.png" alt="Diament czystość I1" />
                                </figure>
                            </div>
                            <div class="clarity clarity-9 <? if(czystosc($product['Czystosc']) == 'I2') { print 'active'; } ?>">
                                <figure>
                                    <figcaption>I2</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/clarity/clarity-9.png" alt="Diament czystość I2" />
                                </figure>
                            </div>
                            <div class="clarity clarity-1 <? if(czystosc($product['Czystosc']) == 'I3') { print 'active'; } ?>0">
                                <figure>
                                    <figcaption>I3</figcaption>
                                    <img src="<?= BASE_URL ?>/assets/images/product/clarity/clarity-10.png" alt="Diament czystość I3" />
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <a href="<?= BASE_URL ?>/views/poradnik/o-diamentach" title="Zobacz poradnik" class="guide"><?= slownik(92) ?></a>
        </div>
    </div>
    <!-- start / product description  -->
</section>
<!-- end / main  -->

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= BASE_URL ?>/assets/js/jquery.tooltipster.min.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/classie.js"></script>
<script>
    var polyfilter_scriptpath = '/js/';
    (function($) {
        var counter = function(){
            var privateCounter = 1;

            function changeBy(val){
                privateCounter +=val;
            }

            return {
                setValue: function(value){
                    privateCounter = value;
                },

                increment: function() {
                    changeBy(1);
                },
                decrement: function() {
                    changeBy(-1);
                },
                value: function() {
                    return privateCounter;
                }
            }
        }
        window['item'+<?=$product_id?>] = counter();
    })();
</script>
<!-- Gallery -->
<script>
    jQuery('.thumbnail-gallery a').click(function(event) {
      event.preventDefault();
      var clickSrc = jQuery(this).find('img').attr('src');
      jQuery('#gallery-main-image').attr('src', clickSrc);
    });
</script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>  
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>   
</body>
</html>

