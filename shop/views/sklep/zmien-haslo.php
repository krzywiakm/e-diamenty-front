<? include '../../func.php'; ?>
<? 
loggedOnly();
changeUserPassword();
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Zmiana hasła</title>
	<meta name="description" content="" />
	<meta name="robots" content="noindex, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link href="<?= BASE_URL ?>/assets/css/jquery.bxslider.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<section id="main" class="login change-password">
    <div class="wrapper row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1 form">
            <h2>Zmiana hasła</h2>
            <p style="color:green;"><?= printMsg(); ?></p>
            <p style="color:red;"><?= printErrorMsg(); ?></p>
            <form action="" method="post">
                <div class="form-group">
                    <input type="password" name="starehaslo" placeholder="Podaj stare hasło" />
                    <span class="validate"></span>
                </div>
                <div class="form-group">
                    <input type="password" name="nowehaslo" placeholder="Nowe hasło" />
                    <span class="validate success"></span>
                </div>
                <div class="form-group">
                    <input type="password" name="nowehaslopowtorz" placeholder="Powtórz nowe hasło" />
                    <span class="validate warning"></span>
                </div>
                <div class="form-group">
                    <input type="submit" name="zmienhaslo" value="Zmień hasło" />
                </div>
            </form>
        </div>
        
        <!-- start / slider  -->
        <div class="col-md-5 col-sm-12 col-xs-12 news">
            <? newest_products_slider(); ?>
        </div>
         <!-- end / slider  -->
    </div>
</section>

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script> 
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.registerAccordion.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script> 

<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>  
 
</body>
</html>