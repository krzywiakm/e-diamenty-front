<? include '../../func.php'; ?>
<? loggedOnly() ?>
<? $orders = getCurrentUserOrders(); ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Historia zamówień</title>
	<meta name="description" content="" />
	<meta name="robots" content="noindex, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="main" class="login order-history">
    <!-- start / order history table -->
    <div class="wrapper row table">
        <div class="col-md-12">
            <h2>Historia zamówień</h2>
            <div class="table-responsive">
                <div class="Table">
    				 <div class="TableHeading">
    				     <div class="TableHead head-number">Numer</div>
                         <div class="TableHead head-date">Data</div>
                         <div class="TableHead head-time">Czas</div>
                         <div class="TableHead head-amount">Kwota</div>
                         <div class="TableHead head-check">Przyjęto</div>
                         <div class="TableHead head-shipment">Towar wysłany</div>
                         <div class="TableHead head-payment">Zapłacono</div>
                         <div class="TableHead head-detail"></div>
    				 </div>
                <? foreach ($orders as $order) { ?>
                     <div class="TableRow">
                        <div class="TableCell number"><?= $order['Id']; ?></div>
                        <div class="TableCell date"><?= $order['DataZam']; ?></div>
                        <div class="TableCell time"><?= $order['CzasZam']; ?></div>
                        <div class="TableCell amount"><?= $order['DoZaplaty']; ?> PLN</div>
                        <div class="TableCell check"><?= taknie($order['Przyjete']); ?></div>
                        <div class="TableCell shipment"><?= taknie($order['Wykonane']); ?></div>
                        <div class="TableCell payment"><?= taknie($order['Zaplacone']); ?></div>
                        <div class="TableCell detail"><a href="<?= BASE_URL ?>/views/sklep/podsumowanie?id=<?= $order['Id'] ?>">szczegóły</a></div>
                     </div>
                <? } ?>
                </div>
            </div>
            <span class="catalog-scroll-right">Przesuń w prawo</span>
        </div>
    </div>
    <!-- end / order history table -->
</section>
<!-- end / main  -->

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script> 
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script> 
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>  
 
</body>
</html>
