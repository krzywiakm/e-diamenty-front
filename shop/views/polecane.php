<? include '../func.php'; ?>
<? $page_name = basename(__FILE__, '.php'); ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Polecane diamenty – kup diament w okazyjne cenie hurtowej – e-diamenty.pl</title>
	<meta name="description" content="Polecane diamenty w realnych cenach hurtowych, promocyjna oferta na wybrane diamenty, rabat na diamenty jubilerskie, wyprzedaż naturalnych diamentów" />
	<meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="main" class="about">
    <!-- start / section 1  -->
    <div id="about-1">
        <div class="wrapper row">
            <div class="col-md-4 col-md-offset-2">
                <h1 style="font-size: 34px;">Polecane</h1>
            </div>
        </div>
    </div>
<? 
$newest_prod_count = count(getNewestProducts(12));

if($newest_prod_count >= 4){
    $newest_products = getNewestProducts(12);
}
else if(($newest_prod_count < 4) && ($newest_prod_count > 0)) {
    $rand_prod_num = 4 - $newest_prod_count;
    $newest_products = array_merge(getNewestProducts(),getRandomProducts($rand_prod_num));
}
else {
    $newest_products = getRandomProducts(9);
}
?>
<? if (count($newest_products) > 0) { ?>
<div id="home-products">
<!--     <div class="wrapper row">
        <div class="col-md-8 col-md-offset-2">
            <div class="table-row">
                <div class="table-cell" style="width:50%"><div class="border"></div></div>
                <div class="table-cell" style="padding: 0 50px"><h2><?= slownik(51) ?></h2></div>
                <div class="table-cell" style="width:50%"><div class="border"></div></div>
            </div>
        </div>
    </div> -->
    <div class="wrapper row">
    <? foreach ($newest_products as $product) { ?>
        <div class="col-md-3 col-sm-6 product">
            <article>
                <figure>
                    <img src="<?= getDiamondPhotoUrl($product['Ksztalt']) ?>" alt="<?= ksztalt($product['Ksztalt']) . ' ' . formatMass($product['Masa']) ?> - e-diamenty.pl" />
                </figure>
                <h3><?=  ksztalt($product['Ksztalt']) . ' ' . formatMass($product['Masa']) . 'ct ' . barwa($product['Barwa']) . '/' . czystosc($product['Czystosc']) ?></h3>
                <p><?= $product['Brutto'] ?> PLN</p>
                <a href="<?= BASE_URL ?>/views/sklep/produkt?id=<?= $product['Id'] ?>" title="Pokaż" class="readmore">Pokaż</a>
            </article>
        </div>
    <?}?>
    </div>
</div>
<?}?>
    
<? home_services(); ?>
</section>
<!-- start / main  -->

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>   
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>   
 
</body>
</html>
