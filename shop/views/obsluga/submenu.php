<!-- start / category  -->
<div class="category">
    <div class="wrapper row">
        <div class="col-md-10 col-md-offset-1">
            <ul id="o-nas-submenu">
                <li class="koszty-wysylki"><a href="<?= BASE_URL ?>/views/obsluga/koszty-wysylki">koszty wysyłki</a></li>
                <li class="zwroty"><a href="<?= BASE_URL ?>/views/obsluga/zwroty">zwroty</a></li>
                <li class="czas-dostawy"><a href="<?= BASE_URL ?>/views/obsluga/czas-dostawy">czas dostawy</a></li>
                <li class="formy-platnosci"><a href="<?= BASE_URL ?>/views/obsluga/formy-platnosci">formy płatności</a></li>
                <li class="gwarancja"><a href="<?= BASE_URL ?>/views/obsluga/gwarancja">gwarancja</a></li>
                <li class="certyfikaty"><a href="<?= BASE_URL ?>/views/obsluga/certyfikaty-diamentow">certyfikaty</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- end / category  -->