<? include '../func.php'; ?>
<? 
$post_id = $_GET["id"];
$slug = $_GET["slug"];
if ($post_id != null) {
    $post = getNewsPost($post_id);
}
else if ($slug != null) {
    $post = getNewsPostBySlug($slug);
}
if ($post['title'] == null) {
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
    header('Location: ' . BASE_URL . '/404');
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Aktualności e-diamenty.pl – <?= trim($post['title']); ?></title>
	<meta name="description" content="<?= trim($post['title']); ?> – najnowsze aktualności z rynku diamentów, branża diamentowa, ceny diamentów inwestycyjnych, handel diamentami" />
	<meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="canonical" href="https:<?= BASE_URL ?>/aktualnosci/<?= convertToSlug($post['title']); ?>" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="posts" class="single">
    <div class="posts-bar">
        <div class="wrapper row">
            <div class="col-md-12">
                <a href="<?= BASE_URL ?>/aktualnosci" title="Powrót do aktualności" class="prev">Powrót do aktualności</a>
            </div>
        </div>
    </div>
    <div class="wrapper row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-5 col-md-hide thumb">
                            <div class="thumbnail"><img src="<?= $post['img']; ?>" alt="<?= $post['title'] ?>" /></div>
                        </div>
                        <div class="col-md-7 col-sm-12 col-xs-12 content">
                            <div class="post-content">
                                <span class="date"><?= $post['date']; ?></span>
                                <h1><?= $post['title']; ?></h1>
                                <?= $post['body']; ?>
                                <div class="small-navigation">
                                    <? if ( $post['id']-1 > 0) { ?>
                                        <a rel="prev" href="<?= BASE_URL ?>/views/aktualnosc?id=<?= $post['id']-1; ?>" class="prev">Poprzedni</a>
                                    <? } ?>
                                    <? if ( $post['id']+1 <= getNumberOfAllPosts()) { ?>
                                        <a rel="next" href="<?= BASE_URL ?>/views/aktualnosc?id=<?= $post['id']+1; ?>" class="next">Następny</a> 
                                    <? } ?>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end / main  -->

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>   
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>   

</body>
</html>
