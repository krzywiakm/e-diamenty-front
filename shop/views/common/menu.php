<!-- start / menu -->
<div id="menu">
    <div class="wrapper row">
        <div class="col-md-12 logo">
            <a href="<?= BASE_URL ?>"><img src="<?= BASE_URL ?>/assets/images/e-diamenty-plogo.png" alt="Logo e-diamenty.pl" /></a>
        </div>
        <div class="col-md-8 col-md-offset-2 nav">
            <nav>
                <ul>
                    <li><a href="<?= BASE_URL ?>/sklep/katalog">Diamenty</a></li>
                    <li><a href="<?= BASE_URL ?>/views/o-nas">O nas</a></li>
                    <li><a href="<?= BASE_URL ?>/views/poradnik/o-diamentach">Poradnik</a></li>
                    <li><a href="<?= BASE_URL ?>/aktualnosci">Aktualności</a></li>
                    <li><a href="<?= BASE_URL ?>/views/kontakt">Kontakt</a></li>
                </ul>
            </nav> 
        </div>
    </div>
</div>
<!-- end / menu -->
