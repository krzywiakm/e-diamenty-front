<? 
$newest_prod_count = count(getNewestProducts());
if($newest_prod_count == 4){
    $newest_products = getNewestProducts();
}
else if(($newest_prod_count < 4) && ($newest_prod_count > 0)) {
    $rand_prod_num = 4 - $newest_prod_count;
    $newest_products = array_merge(getNewestProducts(),getRandomProducts($rand_prod_num));
}
else {
    $newest_products = getRandomProducts();
}
?>
<? if (count($newest_products) > 0) { ?>
<h2><?= slownik(51) ?></h2>
<ul class="row bxslider">
    <? foreach ($newest_products as $product) { ?>
        <li>
            <div class="col-md-5 image">
                <img src="<?= getDiamondPhotoUrl($product['Ksztalt']) ?>" alt="<?= ksztalt($product['Ksztalt']) . ' ' . formatMass($product['Masa']) ?> - e-diamenty.pl" />
            </div>
            <div class="col-md-7 desc">
                <h3><?=  ksztalt($product['Ksztalt']) . ' ' . formatMass($product['Masa']) . 'ct ' . barwa($product['Barwa']) . '/' . czystosc($product['Czystosc']) ?></h3>
                <p class="price"><?= $product['Brutto'] ?> PLN</p>
                <p><?= substr(shortProductDescription($product['Id']), 0, 120) . '...' ?></p>
                <a href="<?= BASE_URL . '/sklep/produkt/' . $product['Id'] ?>" title="zobacz diament">zobacz diament</a>
            </div>
        </li>
    <?}?>
</ul>
<?}?>