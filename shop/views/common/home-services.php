<section id="home-services">
    <div class="wrapper row">
        <h2 class="title">Obsługa klienta</h2>
        <div class="col-md-2 col-sm-4 col-xs-6 service">
            <a rel="nofollow" href="<?= BASE_URL ?>/views/obsluga/koszty-wysylki" title="więcej">
                <article>
                    <figure>
                        <img src="<?= BASE_URL ?>/assets/images/video/koszty-wysylki.jpg" alt="Koszty wysylki" />
                        <h3>Koszty wysyłki</h3>
                    </figure>
                    <p><?= getPageShortDescription('koszty-wysylki'); ?></p>
                    <div class="read-more" style="margin-left:10px;margin-bottom:10px;">więcej</div>
                </article>
            </a>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 service">
            <a rel="nofollow" href="<?= BASE_URL ?>/views/obsluga/zwroty" title="więcej">
                <article>
                    <figure>
                        <img src="<?= BASE_URL ?>/assets/images/video/zwroty.jpg" alt="Zwroty" />
                        <h3>Zwroty</h3>
                    </figure>
                    <p><?= getPageShortDescription('zwroty'); ?></p>
                    <div class="read-more" style="margin-left:10px;margin-bottom:10px;">więcej</div>
                </article>
            </a>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 service">
            <a rel="nofollow" href="<?= BASE_URL ?>/views/obsluga/czas-dostawy" title="więcej">
                <article>
                    <figure>
                        <img src="<?= BASE_URL ?>/assets/images/video/czas-dostawy.jpg" alt="Czas dostawy" />
                        <h3>Czas dostawy</h3>
                    </figure>
                    <p><?= getPageShortDescription('czas-dostawy'); ?></p>
                    <div class="read-more" style="margin-left:10px;margin-bottom:10px;">więcej</div>
                </article>
            </a>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 service">
            <a rel="nofollow" href="<?= BASE_URL ?>/views/obsluga/formy-platnosci" title="więcej">
                <article>
                    <figure>
                        <img src="<?= BASE_URL ?>/assets/images/video/formy-platnosci.jpg" alt="Formy platnosci" />
                        <h3>Formy płatności</h3>
                    </figure>
                    <p><?= getPageShortDescription('formy-platnosci'); ?></p>
                    <div class="read-more" style="margin-left:10px;margin-bottom:10px;">więcej</div>
                </article>
            </a>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 service">
            <a rel="nofollow" href="<?= BASE_URL ?>/views/obsluga/gwarancja" title="więcej">
                <article>
                    <figure>
                        <img src="<?= BASE_URL ?>/assets/images/video/gwarancja.jpg" alt="Gwarancja" />
                        <h3>Gwarancja</h3>
                    </figure>
                    <p><?= getPageShortDescription('gwarancja'); ?></p>
                    <div class="read-more" style="margin-left:10px;margin-bottom:10px;">więcej</div>
                </article>
            </a>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 service">
            <a rel="nofollow" href="<?= BASE_URL ?>/views/obsluga/certyfikaty-diamentow" title="więcej">
                <article>
                    <figure>
                        <img src="<?= BASE_URL ?>/assets/images/video/certyfikaty.jpg" alt="Certyfikaty" />
                        <h3>Certyfikaty</h3>
                    </figure>
                    <p><?= getPageShortDescription('certyfikaty-diamentow'); ?></p>
                   <div class="read-more" style="margin-left:10px;margin-bottom:10px;">więcej</div>
                </article>
            </a>
        </div>
    </div>
</section>