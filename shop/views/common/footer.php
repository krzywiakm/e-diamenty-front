<!-- start / footer  -->
<footer id="footer">
    <div class="wrapper row">
        <!-- start / top footer menu  -->
        <div id="top">
            <div class="col-md-3 col-md-12 col-xs-12 logo">
                <a href="<?= BASE_URL ?>"><img src="<?= BASE_URL ?>/assets/images/footer/e-diamenty-logo-f.png" alt="Logo e-diamenty.pl" /></a>
                <ul class="social">
                    <li><a rel="nofollow" target="_blank" href="https://www.pinterest.com/ediamenty/"><img src="<?= BASE_URL ?>/assets/images/footer/pin.png" alt="Pinterest - e-diamenty.pl" /></a></li>
                    <li><a rel="nofollow" target="_blank" href="https://www.instagram.com/e_diamenty.pl/"><img src="<?= BASE_URL ?>/assets/images/footer/ins.png" alt="Instagram - e-diamenty.pl" /></a></li>
                    <li><a rel="nofollow" target="_blank" href="https://www.linkedin.com/company/e-diamenty/"><img src="<?= BASE_URL ?>/assets/images/footer/in.png" alt="Linkedin - e-diamenty.pl" /></a></li>
                    <li><a rel="nofollow" target="_blank" href="https://www.facebook.com/ediamentypl/"><img src="<?= BASE_URL ?>/assets/images/footer/fb.png" alt="Facebook - e-diamenty.pl" /></a></li>
                </ul>
            </div>
            <div class="col-md-2 col-md-6 col-xs-6 menu">
                <h3>FIRMA</h3>
                <ul>
                    <li><a href="<?= BASE_URL ?>/views/sklep/katalog">Diamenty</a></li>
                    <li><a href="<?= BASE_URL ?>/bizuteria">Biżuteria</a></li>
                    <li><a href="<?= BASE_URL ?>/views/o-nas">O nas</a></li>
                    <li><a href="<?= BASE_URL ?>/views/kontakt">Kontakt</a></li>
                    <li><a href="<?= BASE_URL ?>/views/aktualnosci">Aktualności</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-md-6 col-xs-6 menu">
                <h3>INFORMACJE</h3>
                <ul>
                    <li><a href="<?= BASE_URL ?>/views/informacje/regulamin-sklepu">Regulamin</a></li>
                    <li><a href="<?= BASE_URL ?>/views/informacje/polityka-prywatnosci">Polityka prywatności</a></li>
                    <li><a href="<?= BASE_URL ?>/views/informacje/bezpieczenstwo-zakupow">Bezpieczeństwo zakupów</a></li>
                    <li><a href="<?= BASE_URL ?>/views/informacje/cookies">Cookies</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-md-6 col-xs-6 menu">
                <h3>OBSŁUGA KLIENTA</h3>
                <ul>
                    <li><a href="<?= BASE_URL ?>/views/obsluga/koszty-wysylki">Koszt wysyłki</a></li>
                    <li><a href="<?= BASE_URL ?>/views/obsluga/zwroty">Zwroty</a></li>
                    <li><a href="<?= BASE_URL ?>/views/obsluga/czas-dostawy">Czas dostawy</a></li>
                    <li><a href="<?= BASE_URL ?>/views/obsluga/formy-platnosci">Formy płatności</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-md-6 col-xs-6 menu">
                <h3>DODATKI</h3>
                <ul>
                    <li><a href="<?= BASE_URL ?>/views/obsluga/gwarancja">Gwarancja</a></li>
                    <li><a href="<?= BASE_URL ?>/views/obsluga/certyfikaty-diamentow">Certyfikaty</a></li>
                    <li><a href="<?= BASE_URL ?>/views/poradnik/o-diamentach">Poradnik</a></li>
                </ul>
            </div>
        </div>
        <!-- end / top footer menu  -->

        <!-- start / bottom footer menu  -->
        <div id="bottom">
            <div class="wrapper row">
                <div class="col-md-12">
                    <div class="bottom">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 left">
                                <a rel="nofollow" href="http://www.gia.edu/" target="_blank" title=""><img src="<?= BASE_URL ?>/assets/images/footer/gia.png" alt="GIA" /></a>
                                <a rel="nofollow" href="http://www.igiworldwide.com/" target="_blank" title=""><img style="height:30px" src="<?= BASE_URL ?>/assets/images/footer/gia1.png" alt="IGI" /></a>
                                <a rel="nofollow" href="http://www.hrdantwerp.com/" target="_blank" title=""><img src="<?= BASE_URL ?>/assets/images/footer/hrd-antwerp.png" alt="HRD Antwerp" /></a>
                                <a rel="nofollow" href="https://www.awdc.be/en" target="_blank" title="" style="margin-left: 8px;"><img src="<?= BASE_URL ?>/assets/images/dnaawdc.png" style="height: 56px;margin-top:6px;" alt="Antwerp World Diamond Centre" /></a>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 right">
                                <a rel="nofollow" href="http://www.przelewy24.pl/" target="_blank"><img src="<?= BASE_URL ?>/assets/images/footer/przelewy24.png" alt="Przelewy24" /></a>
                                <img src="<?= BASE_URL ?>/assets/images/footer/gepardy-biznesu.png" alt="Gepardy biznesu" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper row">
                <div class="col-md-12">
                    <div class="copyright">
                        <div class="row">
                            <div class="col-md-6 left">
                                Copyright 2016 &copy;. All rights reserved.
                            </div>
                            <div class="col-md-6 right">
                                Created by HESNA IMS
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end / bottom footer menu  -->
    </div>
</footer>
<!-- end / footer  -->
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NCWR5N"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NCWR5N');</script>
<!-- End Google Tag Manager -->
<script src="//code.tidio.co/wjbtq0mvw2yxs9cz8c0vzcpudkgrd3bb.js"></script>
