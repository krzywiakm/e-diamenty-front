<div class="wrapper row guide-category">
    <div class="col-md-2 col-sm-4 col-xs-6 item">
        <figure>
            <a href="#" title="#"><img src="<?= BASE_URL ?>/assets/images/guide-1.jpg" alt="guide-1" />
                <figcaption>czytaj więcej</figcaption>
            </a>
        </figure>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 item">
        <figure>
            <a href="#" title="#"><img src="<?= BASE_URL ?>/assets/images/guide-2.jpg" alt="guide-2" />
                <figcaption>czytaj więcej</figcaption>
            </a>
        </figure>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 item">
        <figure>
            <a href="#" title="#"><img src="<?= BASE_URL ?>/assets/images/guide-3.jpg" alt="guide-3" />
                <figcaption>czytaj więcej</figcaption>
            </a>
        </figure>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 item">
        <figure>
            <a href="#" title="#"><img src="<?= BASE_URL ?>/assets/images/guide-4.jpg" alt="guide-4" />
                <figcaption>czytaj więcej</figcaption>
            </a>
        </figure>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 item">
        <figure>
            <a href="#" title="#"><img src="<?= BASE_URL ?>/assets/images/guide-5.jpg" alt="guide-5" />
                <figcaption>czytaj więcej</figcaption>
            </a>
        </figure>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 item">
        <figure>
            <a href="#" title="#"><img src="<?= BASE_URL ?>/assets/images/guide-6.jpg" alt="guide-6" />
                <figcaption>czytaj więcej</figcaption>
            </a>
        </figure>
    </div>
</div>