<div id="nav">
    <!-- start / sticky & desktop menu -->
    <div class="wrapper static desktop">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 search">
                <form action="<?= BASE_URL ?>/views/sklep/produkt" method='get'>
                    <input type="text" name='cert-id' placeholder="Wpisz numer certyfikatu" />
                    <input type="submit" value="" />
                </form>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 col-xs-offset-2 account slidedown fix-after-translation">
            <? if(auth() && ($_SESSION["BezRej"] != true) ) { ?>
                <p><?= $_SESSION["USER_EMAIL"] ?></p>
                <ul class="choose">
                    <li><a href="<?= BASE_URL ?>/views/sklep/twoje-dane" title="Twoje dane">Twoje dane</a></li>
                    <li><a href="<?= BASE_URL ?>/views/sklep/historia-zamowien" title="Historia zamówień">Historia zamówień</a></li>
                    <li><a href="<?= BASE_URL ?>/views/sklep/zmien-haslo" title="Zmiana hasła">Zmiana hasła</a></li>
                    <li><form method="post"><input type="submit" name="logout" value="WYLOGUJ"></form></li>
                </ul>
            <? } else { ?>
                <a rel="nofollow" href="<?= BASE_URL ?>/views/sklep/zaloguj" title="Login" class="login">Logowanie</a>
                <a rel="nofollow" href="<?= BASE_URL ?>/views/sklep/rejestracja" title="Sign Up" class="signup">Rejestracja</a>
            <? } ?>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 watch">
                <a rel="nofollow" href="<?= BASE_URL ?>/views/sklep/obserwowane">Obserwowane <span>(0)</span></a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 cart slidedown">
                 <a rel="nofollow" href="<?= BASE_URL ?>/views/sklep/koszyk">Mój koszyk <span>(0)</span></a>
                 <div class="mini-cart choose">
                     <i id="slider-prev"></i>
                     <ul class="bx-cart">
                     </ul>
                     <i id="slider-next"></i>
                     <div class="mini-cart-subtotal">
                        <p>Łączna kwota</p>
                        <p class="amount"></p>
                     </div>
                     <a href="<?= BASE_URL ?>/views/sklep/koszyk" title="Do koszyka" class="go-cart">Do koszyka</a>
                 </div>
            </div>
        </div>
    </div>
    <!-- start / sticky & desktop menu -->

    <!-- start / sticky & mobile menu -->
    <div class="wrapper sticky mobile">
        <div class="row">
            <div class="col-md-1 col-sm-1 col-xs-1 logo sticky-logo">
                <a href="<?= BASE_URL ?>"><img src="<?= BASE_URL ?>/assets/images/icons/sticky-logo.png" alt="Logo e-diamenty.pl" /></a>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 menu sticky-menu">
                <nav id="cssmenu">
                    <ul>
                        <li><a href="<?= BASE_URL ?>/sklep/katalog" title="Katalog">Diamenty</a></li>
                        <li><a href="<?= BASE_URL ?>/bizuteria" title="Biżuteria">Biżuteria</a></li>
                        <li><a href="<?= BASE_URL ?>/views/o-nas" title="O nas">O nas</a></li>
                        <li><a href="<?= BASE_URL ?>/views/poradnik/o-diamentach" title="O diamentach">Poradnik</a></li>
                        <li><a href="<?= BASE_URL ?>/views/aktualnosci" title="Aktualności">Aktualnosci</a></li>
                        <li><a href="<?= BASE_URL ?>/views/kontakt" title="Kontakt">Kontakt</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 col-sm-offset-1 account sticky-account">
                <a href="#" id="mobile-login-menu-btn" title="Login" class="login"></a>
                <div class="mobile-login-menu">
                    <ul class="open">
                        <li><a rel="nofollow" href="<?= BASE_URL ?>/views/sklep/zaloguj" title="Login">Logowanie</a></li>
                        <li><a rel="nofollow" href="<?= BASE_URL ?>/views/sklep/rejestracja" title="Sign Up" class="signup">Rejestracja</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 watch sticky-watch">
                <a href="<?= BASE_URL ?>/views/sklep/obserwowane"><span class="badge">0</span></a>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 cart sticky-cart">
                 <a href="<?= BASE_URL ?>/views/sklep/koszyk"><span class="badge">0</span></a>
            </div>
        </div>
    </div>
    <!-- end / sticky & mobile menu -->
</div>
<style>
.mobile-login-menu {
    display:none;
    background: #fff;
    width: 115px;
    height: 100px;
    margin-top: 6px;
    margin-left: -15px;
    padding: 20px;
    cursor: pointer;
    font-size: 14px;
    text-decoration: none;
    color: #3d556d;border-left: 0!important;
    -webkit-box-shadow: 0px 0px 15px -6px rgba(196,196,196,1);
    -moz-box-shadow: 0px 0px 15px -6px rgba(196,196,196,1);
    box-shadow: 0px 0px 15px -6px rgba(196,196,196,1);
}

.mobile-login-menu ul li a {
    display: block;
    height: 40px;
    font-size: 14px !important;
}
</style>
