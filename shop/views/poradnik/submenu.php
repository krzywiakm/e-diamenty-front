<!-- start / category  -->
<div class="category">
    <div class="wrapper row">
        <div class="col-md-10 col-md-offset-1">
            <ul id="poradnik-submenu">
                <li class="o-diamentach"><a href="<?= BASE_URL ?>/views/poradnik/o-diamentach">O Diamentach</a></li>
                <li class="barwa"><a href="<?= BASE_URL ?>/views/poradnik/barwa">Barwa</a></li>
                <li class="czystosc"><a href="<?= BASE_URL ?>/views/poradnik/czystosc">Czystość</a></li>
                <li class="masa"><a href="<?= BASE_URL ?>/views/poradnik/masa">Masa</a></li>
                <li class="szlif"><a href="<?= BASE_URL ?>/views/poradnik/szlif">Szlif</a></li>
                <li class="diamenty-inwestycyjne"><a href="<?= BASE_URL ?>/views/poradnik/diamenty-inwestycyjne">Diamenty inwestycyjne</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- end / category  -->