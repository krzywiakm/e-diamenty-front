<div class="wrapper row guide-category">
    <div class="col-md-2 col-sm-4 col-xs-6 item">
        <figure>
            <a href="<?= BASE_URL ?>/views/poradnik/o-diamentach"><img src="<?= BASE_URL ?>/assets/images/guide-1.jpg" alt="Poradnik o diamentach" />
                <figcaption>czytaj więcej</figcaption>
                <span></span>
            </a>
        </figure>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 item">
        <figure>
            <a href="<?= BASE_URL ?>/views/poradnik/barwa"><img src="<?= BASE_URL ?>/assets/images/guide-2.jpg" alt="Poradnik barwa diamentów" />
                <figcaption>czytaj więcej</figcaption>
                <span></span>
            </a>
        </figure>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 item">
        <figure>
            <a href="<?= BASE_URL ?>/views/poradnik/czystosc"><img src="<?= BASE_URL ?>/assets/images/guide-3.jpg" alt="Poradnik czystość diamentów" />
                <figcaption>czytaj więcej</figcaption>
                <span></span>
            </a>
        </figure>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 item">
        <figure>
            <a href="<?= BASE_URL ?>/views/poradnik/masa"><img src="<?= BASE_URL ?>/assets/images/guide-4.jpg" alt="Poradnik masa diamentów" />
                <figcaption>czytaj więcej</figcaption>
                <span></span>
            </a>
        </figure>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 item">
        <figure>
            <a href="<?= BASE_URL ?>/views/poradnik/szlif"><img src="<?= BASE_URL ?>/assets/images/guide-5.jpg" alt="Poradnik szlif diamentów" />
                <figcaption>czytaj więcej</figcaption>
                <span></span>
            </a>
        </figure>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 item">
        <figure>
            <a href="<?= BASE_URL ?>/views/poradnik/diamenty-inwestycyjne"><img src="<?= BASE_URL ?>/assets/images/guide-6.jpg" alt="Poradnik diamenty inwestycyjne" />
                <figcaption>czytaj więcej</figcaption>
                <span></span>
            </a>
        </figure>
    </div>
</div>