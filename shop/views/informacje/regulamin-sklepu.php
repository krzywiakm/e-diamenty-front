<? include '../../func.php'; ?>
<? $page_name = basename(__FILE__, '.php'); ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Regulamin sklepu z diamentami – hurtowe ceny na diamenty – e-diamenty.pl</title>
	<meta name="description" content="Właścicielem, Sprzedawcą oraz Administratorem Sklepu e-diamenty.pl jest: DIAMOND INVESTMENT COMPANY T. KWIATKIEWICZ M. KWIATKIEWICZ SPÓŁKA JAWNA" />
	<meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />  
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]--> 
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="main">
<?= renderPageContent($page_name); ?>
</section>
<!-- end / main  -->

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.accordion.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.accordion').accordion({
        //defaultOpen: 'section-3',
    });
  });
</script> 
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>   
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>  
 
</body>
</html>

