<? include '../func.php'; ?>
<?
$news_page = $_GET["page"];
if (isset($_GET["page"])) {
    $news_page = $_GET["page"];
}
else {
    $news_page = 1;
}
$news_posts = getAllNewsPosts($news_page);
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <? if($news_page == 1){ ?>
        <title>Aktualności – branża diamentowa, diamenty inwestycyjne, ceny diamentów - e-diamenty.pl</title>
    	<meta name="description" content="Aktualności zawierajż informacje z branży diamentowej: rynek diamentów inwestycyjnych, hurtowe ceny diamentów, wzrost wartości diamentów, wydobycie diamentów" />
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="canonical" href="https://e-diamenty.pl/aktualnosci" />
    <? } else { ?>
        <title>Aktualności z branży diamentowej – strona <?= $news_page ?> - e-diamenty.pl</title>
        <meta name="description" content="Aktualności z branży diamentowej i rynku diamentów inwestycyjnych, informacje o hurtowych cenach diamentów i wydobyciu diamentów – strona <?= $news_page ?>" />
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="canonical" href="https://e-diamenty.pl/aktualnosci/strona/<?= $news_page ?>" />
    <? } ?>
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]-->
    <script> window['_fs_debug'] = false; window['_fs_host'] = 'fullstory.com'; window['_fs_script'] = 'edge.fullstory.com/s/fs.js'; window['_fs_org'] = '108ATS'; window['_fs_namespace'] = 'FS'; (function(m,n,e,t,l,o,g,y){ if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;} g=m[e]=function(a,b,s){g.q?g.q.push([a,b,s]):g._api(a,b,s);};g.q=[]; o=n.createElement(t);o.async=1;o.crossOrigin='anonymous';o.src='https://'+_fs_script; y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y); g.identify=function(i,v,s){g(l,{uid:i},s);if(v)g(l,v,s)};g.setUserVars=function(v,s){g(l,v,s)};g.event=function(i,v,s){g('event',{n:i,p:v},s)}; g.anonymize=function(){g.identify(!!0)}; g.shutdown=function(){g("rec",!1)};g.restart=function(){g("rec",!0)}; g.log = function(a,b){g("log",[a,b])}; g.consent=function(a){g("consent",!arguments.length||a)}; g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)}; g.clearUserCookie=function(){}; g.setVars=function(n, p){g('setVars',[n,p]);}; g._w={};y='XMLHttpRequest';g._w[y]=m[y];y='fetch';g._w[y]=m[y]; if(m[y])m[y]=function(){return g._w[y].apply(this,arguments)}; g._v="1.3.0"; })(window,document,window['_fs_namespace'],'script','user'); </script>
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="posts">
    <div class="wrapper row pagination">
        <div class="col-md-3 prev">
            <? if(ceil(getNumberOfAllPosts()/$news_page) >= $news_page) { ?>
                <a rel="prev" href="<?= BASE_URL ?>/aktualnosci/strona/<?= $news_page+1 ?>">poprzednie</a>
            <?}?>
        </div>
        <div class="col-md-6">
            <h1 class="title">Wszystkie aktualności</h1>
        </div>
        <div class="col-md-3 next">
            <? if($news_page > 1) { ?>
                <a rel="next" href="<?= BASE_URL ?>/aktualnosci/strona/<?= $news_page-1 ?>">następne</a>
            <?}?>
        </div>
    </div>
    <div class="wrapper row">
        <div class="col-md-12">
            <div class="row">
            <? $index = 0; ?>
            <? foreach ($news_posts as $post) { ?>
                <? if($index == 0 || $index == 1) { ?>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6 col-md-hide thumb">
                            <div class="thumbnail"><img src="<?= $post['img'] ?>" alt="<?= $post['title'] ?>" /></div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 content">
                            <div class="post-content">
                                <span class="date"><?= $post['date'] ?></span>
                                <h2><a href="<?= BASE_URL ?>/aktualnosci/<?= convertToSlug($post['title']); ?>"><?= $post['title'] ?></a></h2>
                                <p><?= mb_substr(trim($post['body']),0,180,'UTF-8'); ?></p>
                                <a rel="nofollow" href="<?= BASE_URL ?>/aktualnosci/<?= convertToSlug($post['title']); ?>" class="rmore">Zobacz więcej</a>
                            </div>
                        </div>
                    </div>
                </div>
                <? } else { ?>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12 content">
                            <div class="post-content">
                                <span class="date"><?= $post['date'] ?></span>
                                <h2><a href="<?= BASE_URL ?>/aktualnosci/<?= convertToSlug($post['title']); ?>"><?= $post['title'] ?></a></h2>
                                <p><?= mb_substr(trim($post['body']),0,180,'UTF-8'); ?></p>
                                <a rel="nofollow" href="<?= BASE_URL ?>/aktualnosci/<?= convertToSlug($post['title']); ?>" class="rmore">Zobacz więcej</a>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-hide thumb">
                            <div class="thumbnail"><img src="<?= $post['img'] ?>" alt="<?= $post['title'] ?>" /></div>
                        </div>
                    </div>
                </div>
                <? } ?>
                <? $index++; ?>
            <? } ?>
            </div>
        </div>
    </div>
</section>
<!-- end / main  -->

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>

</body>
</html>
