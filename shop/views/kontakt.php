<? include '../func.php'; ?>
<? $page_name = basename(__FILE__, '.php'); ?>
<? $text = $_GET["text"]; ?>
<?
if ($_POST['contact-form-submit']) {
    if(isset($_POST['email']) and $_POST['email'] != '' and isset($_POST['msg']) and $_POST['msg'] != '' ) {
        if(sendEmail($_POST['email'], 'Wiadomość od ' . $_POST['name'] . ' - formularz kontaktowy', $_POST['msg'])) {
            $succ_msg = "Wiadomość poprawnie wysłana.";
        }
        else {
            $error_msg = "Niestety nie udało się wysłać wiadomości. Spróbuj ponownie.";
        }
    }
    else {
        $error_msg = "Wypełnij wszystkie wymagane pola.";
    }
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
    <title>Kontakt – e-diamenty.pl – Hurtownia Diamond Investment Company Kwiatkiewicz</title>
	<meta name="description" content="Kontakt e-diamenty.pl - Diamond Investment Company Tomasz Kwiatkiewicz i Mikołaj Kwiatkiewicz wywodzą się z rodziny o 30 letniej tradycji w branży jubilerskiej" />
	<meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
        filter: none;
        }
    </style>
    <![endif]-->
    <script> window['_fs_debug'] = false; window['_fs_host'] = 'fullstory.com'; window['_fs_script'] = 'edge.fullstory.com/s/fs.js'; window['_fs_org'] = '108ATS'; window['_fs_namespace'] = 'FS'; (function(m,n,e,t,l,o,g,y){ if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;} g=m[e]=function(a,b,s){g.q?g.q.push([a,b,s]):g._api(a,b,s);};g.q=[]; o=n.createElement(t);o.async=1;o.crossOrigin='anonymous';o.src='https://'+_fs_script; y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y); g.identify=function(i,v,s){g(l,{uid:i},s);if(v)g(l,v,s)};g.setUserVars=function(v,s){g(l,v,s)};g.event=function(i,v,s){g('event',{n:i,p:v},s)}; g.anonymize=function(){g.identify(!!0)}; g.shutdown=function(){g("rec",!1)};g.restart=function(){g("rec",!0)}; g.log = function(a,b){g("log",[a,b])}; g.consent=function(a){g("consent",!arguments.length||a)}; g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)}; g.clearUserCookie=function(){}; g.setVars=function(n, p){g('setVars',[n,p]);}; g._w={};y='XMLHttpRequest';g._w[y]=m[y];y='fetch';g._w[y]=m[y]; if(m[y])m[y]=function(){return g._w[y].apply(this,arguments)}; g._v="1.3.0"; })(window,document,window['_fs_namespace'],'script','user'); </script>
</head>
<body>
<!-- start / header -->
<header id="header">
<? top_bar(); ?>
<? main_menu(); ?>
</header>
<!-- end / header -->

<!-- start / main  -->
<section id="main" class="contact">
    <div class="wrapper row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div style="padding-top: 30px;" class="col-md-6 col-sm-6 col-xs-12 detail">
                    <?= renderPageContent($page_name); ?>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form" style="display: none;">
                    <p style="color:green;"><?= $succ_msg ?></p>
                    <p style="color:red;"><?= $error_msg ?></p>
                    <h2>Formularz kontaktowy</h2>
                    <p>Jeżeli mają Państwo jakiekolwiek pytania prosimy o wypełnienie poniższego formularza. Pola oznaczone * są wymagane.</p>
                    <form action="" method="post">
                        <div class="form-group">
                            <label>Imię i nazwisko</label>
                            <input name="name" type="text" />
                        </div>
                        <div class="form-group">
                            <label>Adres email*</label>
                            <input name="email" type="text" />
                        </div>
                        <div class="form-group">
                            <label>Treść wiadomości*</label>
                            <textarea name="msg" rows="5"><?=$text?></textarea>
                        </div>
                        <div class="form-group">
                            <input name="contact-form-submit" type="submit" value="Wyślij" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end / main  -->

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>
<script>
    if('<?=$text?>' != '') {
        $('html, body').animate({
            scrollTop: $(".form").first().offset().top - 20
        }, 1000);
    }
</script>
</body>
</html>
