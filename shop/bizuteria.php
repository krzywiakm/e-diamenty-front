<? include 'func.php'; ?>
<? $page_name = basename(__FILE__, '.php'); ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Biżuteria na indywidualne zamówienie</title>
    <meta name="description"
          content="Jeżeli potrzebujesz oprawy do diamentu, możemy ją dla Ciebie wykonać w oparciu o Twój własny pomysł."/>
    <meta name="robots"
          content="index, follow"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon"
          type="image/png"
          href="/favicon.png"/>
    <link rel="stylesheet"
          type="text/css"
          href="<?= BASE_URL ?>/assets/css/normalize.css"/>
    <link rel="stylesheet"
          type="text/css"
          href="<?= BASE_URL ?>/assets/css/grid.css"/>
    <link rel="stylesheet"
          type="text/css"
          href="<?= BASE_URL ?>/assets/css/animate.css"/>
    <link rel="stylesheet"
          type="text/css"
          href="<?= BASE_URL ?>/style.css"/>
    <link rel="stylesheet"
          type="text/css"
          href="<?= BASE_URL ?>/assets/css/responsive.css"/>
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
            filter: none;
        }
    </style>
    <![endif]-->
    <style type="text/css">

        .contact-header {
            text-align: center;
        }

        main img {
            max-width: 100%;
            max-height: 240px;
            margin: 0 auto 1em;
            display: block;
        }

        .img-p-wrapper > div > h2 {
            display: none;
        }

        .jewellery-article-section {
            max-width: 764px;
            margin: 0 auto;
        }

        .jewellery-article-section h2 {
            text-transform: uppercase;
            font-weight: bold;
            margin-top: 1.5em;
            margin-bottom: 1em;
        }

        @media screen and (min-width: 768px) {
            main img {
                max-height: 300px;
                max-width: 300px;
                margin-right: 2.5em;
                margin-bottom: 0;
            }

            .img-p-wrapper > h2 {
                display: none;
            }

            .img-p-wrapper > div > h2 {
                display: block;
            }

            .jewellery-article-section {
                margin: 4em auto;
            }

            .jewellery-article-section h2 {
                margin-top: 0;
            }

            .img-p-wrapper {
                display: flex;
                align-items: center;
            }


            .jewellery-article-section:nth-child(2n) .img-p-wrapper {
                flex-direction: row-reverse;
            }

            .jewellery-article-section:nth-child(2n) .img-p-wrapper img {
                margin-left: 2.5em;
                margin-right: 0;
            }
        }
        @media screen and (min-width: 1280px) {
            main img {
                max-height: 410px;
                max-width: 410px;
                margin-right: 2.5em;
                margin-bottom: 0;
            }
        }
    </style>
</head>
<body>
<!-- start / header -->
<header id="header">
    <? top_bar(); ?>
    <? main_menu(); ?>
</header>
<!-- end / header -->

<main class="page">
    <div class="wrapper row content">

        <article class="col-md-8 col-md-offset-2">
            <header>
                <h1 class="title" style="margin-top: 50px;">Biżuteria na zamówienie</h1>
            </header>
            <h2 class="contact-header"><?= slownik(111) ?><br>
                <a class="header-phone-number" href="tel:<?= slownik(112) ?>" style="display: inline-block;font-weight: bold;font-size: 24px;color:rgb(61, 85, 109);line-height: 42px;"><?= slownik(112) ?></a>
            </h2>
            <section class="jewellery-article-section">
                <div class="img-p-wrapper">
                    <h2>Pomysł</h2>
                    <img src="<?= BASE_URL ?>/assets/images/jewellery1.png"
                         alt="">
                    <div>
                        <h2>Pomysł</h2>
                        <p>Jeżeli potrzebujesz oprawy do diamentu, możemy ją dla Ciebie wykonać w oparciu o Twój własny
                            pomysł.
                            Wystarczy, że prześlesz do nas szkic lub zdjęcie np. pierścionka i opowiesz jakie są Twoje
                            oczekiwania. Przeprowadzimy Cię przez cały proces - od pomysłu do gotowej biżuterii.</p>
                    </div>
                </div>
            </section>
            <section class="jewellery-article-section">
                <div class="img-p-wrapper">
                    <h2>Wizualizacja</h2>
                    <img src="<?= BASE_URL ?>/assets/images/jewellery2.png"
                         alt="">
                    <div>

                        <h2>Wizualizacja</h2>
                        <p>Po doprecyzowaniu szczegółów Twój pomysł trafia do jednego z naszych projektantów, który przy
                            pomocy
                            specjalistycznego oprogramowania CAD tworzy jego cyfrową wizualizację.
                            Dzięki temu masz możliwość zobaczyć jak dokładnie będzie wyglądać gotowy pierścionek czy
                            inna
                            biżuteria.</p>
                    </div>
                </div>
            </section>
            <section class="jewellery-article-section">
                <div class="img-p-wrapper">
                    <h2>Oprawa</h2>
                    <img src="<?= BASE_URL ?>/assets/images/jewellery3.jpg"
                         alt="">
                    <div>

                        <h2>Oprawa</h2>
                        <p>Po zaakceptowaniu przez Ciebie wizualizacji nasi złotnicy wykonują oprawę w wybranym przez Ciebie metalu,
                            a kiedy oprawa jest już gotowa, osadzają w nią Twój diament.</p>
                    </div>
                </div>
            </section>
            <img style="margin: 20px auto;display: block;"
                 src="<?= BASE_URL ?>/assets/images/jewellery4.png"
                 alt="">
        </article>
    </div>

</main>

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js"
        type="text/javascript"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>
<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>

<script>
  jQuery('#o-nas-submenu .<?= $page_name ?>').addClass('active');
</script>

</body>
</html>
