<? include 'func.php'; ?>
<? $news_posts = getAllNewsPosts(); ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8" />
    <title><?= slownik(108) ?></title>
    <meta name="description" content="<?= slownik(109) ?>" />
    <meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="RscPBSnvegK9TpIlF74GY_r0eEoH5CqcY0Tv5aScp0M" />
    <link rel="canonical" href="https:<?= BASE_URL ?>" />
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/grid.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/style.css" />
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/responsive.css" />
    <link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/ion.rangeSlider.css" />
    <link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/ion.rangeSlider.skinNice.css" />
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
            filter: none;
        }
    </style>
    <![endif]-->
    <style>
        .jewellery-banner {
            display: flex;
            border: 1px solid #ecf0f1;
            padding: 30px;
            text-align: center;
            float: unset;
            margin: 0 15px;
            max-width: 1192px;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            width: calc(100% - 30px);
        }
        @media screen and (min-width: 768px) {
            .jewellery-banner {
                height: 360px;
                flex-direction: row;
                font-size: 20px;
            }

            .jewellery-banner-see {
                height: 46px;
                width: 280px;
                max-width: unset !important;
                font-size: 18px !important;
                display: flex !important;
                justify-content: center;
                align-items: center;
            }
        }
        @media screen and (min-width: 1222px) {
            .jewellery-banner {
                margin: 0 auto;
                width: auto;
            }
        }

        .jewellery-banner:hover {
            border-color: #000c36;
        }
    </style>
    <!-- Hotjar Tracking Code for https://e-diamenty.pl/ -->
    <script>
      (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2145518,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
      })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <script> window['_fs_debug'] = false; window['_fs_host'] = 'fullstory.com'; window['_fs_script'] = 'edge.fullstory.com/s/fs.js'; window['_fs_org'] = '108ATS'; window['_fs_namespace'] = 'FS'; (function(m,n,e,t,l,o,g,y){ if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;} g=m[e]=function(a,b,s){g.q?g.q.push([a,b,s]):g._api(a,b,s);};g.q=[]; o=n.createElement(t);o.async=1;o.crossOrigin='anonymous';o.src='https://'+_fs_script; y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y); g.identify=function(i,v,s){g(l,{uid:i},s);if(v)g(l,v,s)};g.setUserVars=function(v,s){g(l,v,s)};g.event=function(i,v,s){g('event',{n:i,p:v},s)}; g.anonymize=function(){g.identify(!!0)}; g.shutdown=function(){g("rec",!1)};g.restart=function(){g("rec",!0)}; g.log = function(a,b){g("log",[a,b])}; g.consent=function(a){g("consent",!arguments.length||a)}; g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)}; g.clearUserCookie=function(){}; g.setVars=function(n, p){g('setVars',[n,p]);}; g._w={};y='XMLHttpRequest';g._w[y]=m[y];y='fetch';g._w[y]=m[y]; if(m[y])m[y]=function(){return g._w[y].apply(this,arguments)}; g._v="1.3.0"; })(window,document,window['_fs_namespace'],'script','user'); </script>
</head>
<body>
<!-- start / header -->
<header id="header" class="front-page">
    <? top_bar(); ?>
    <div id="menu" class="video">
        <div class="video-pattern">
            <video autoplay loop muted id="myVideo">
                <source src="<?= BASE_URL ?>/assets/video/diament.webm" type="video/webm" />
                <source src="<?= BASE_URL ?>/assets/video/diament.ogv" type="video/ogv" />
                <source src="<?= BASE_URL ?>/assets/video/diament.mp4" type="video/mp4" />
            </video>
            <div class="pattern"></div>
        </div>
        <div class="wrapper row video-row">
            <div class="col-md-12 logo">
                <a href=""><img src="<?= BASE_URL ?>/assets/images/e-diamenty-logo.png" alt="Logo e-diamenty.pl" /></a>
            </div>
            <div class="col-md-8 col-md-offset-2 nav video">
                <nav>
                    <ul>
                        <li><a href="<?= BASE_URL ?>/sklep/katalog">Diamenty</a></li>
                        <li><a href="<?= BASE_URL ?>/views/o-nas">O nas</a></li>
                        <li><a href="<?= BASE_URL ?>/views/poradnik/o-diamentach">Poradnik</a></li>
                        <li><a href="<?= BASE_URL ?>/aktualnosci">Aktualności</a></li>
                        <li><a href="<?= BASE_URL ?>/views/kontakt">Kontakt</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-6 col-md-offset-3 name video mobile-padding-fix">
                <h1 style="font-size: 20px;"><?= slownik(99) ?></h1>
                <h2><?= slownik(111) ?><a class="header-phone-number" href="tel:<?= slownik(112) ?>" style="display: inline-block;margin-left: 10px;font-weight: bold;font-size: 24px;color:white;"><?= slownik(112) ?></a>
                    <br><br><?= slownik(113) ?></h2>
                <a href="<?= BASE_URL ?>/views/o-nas" class="readmore col-md-hide"><?= slownik(87) ?></a>
                <a href="<?= BASE_URL ?>/views/sklep/katalog" class="readmore show-only-mobile"><?= slownik(88) ?></a>
            </div>
        </div>

        <!-- start / home - filtr -->
        <div class="home-filtr">
            <div class="wrapper row">
                <div class="col-md-4 col-md-offset-1 mass">
                    <label class="pre-search-label">MASA<span>(Carat)</span></label>
                    <input id="mass" />
                    <input type="text" id="mass_from" name="mass_from" value="0.01" class="input-filtr" />
                    <span style="color: #aaaaaa;">ct</span>
                    <span style="float: right;color: #aaaaaa;margin-top: 10px;margin-left: 6px;">ct</span>
                    <input type="text" id="mass_to" name="mass_to" value="<?= getHighestMass(); ?>" class="input-filtr" />
                </div>
                <div class="col-md-4 price">
                    <label class="pre-search-label">ZAKRES CEN<span>(PLN)</span></label>
                    <input id="price" />
                    <input type="text" id="price_from" name="price_from" value="1" class="input-filtr" />
                    <span style="color: #aaaaaa;">PLN</span>
                    <span style="float: right;color: #aaaaaa;margin-top: 10px;margin-left: 6px;">PLN</span>
                    <input type="text" id="price_to" name="price_to" value="<?= getHighestPrice(); ?>" class="input-filtr" />
                </div>
                <div class="col-md-2">
                    <a class="pre-search" href="<?= BASE_URL ?>/views/sklep/katalog"><?= slownik(52) ?></a>
                </div>
            </div>
        </div>
        <!-- end / home - filtr -->
    </div>
</header>
<!-- end / header -->

<?
$newest_prod_count = count(getNewestProducts());
if ($newest_prod_count > 4) {
    $newest_products = getNewestProducts();
}
else if($newest_prod_count == 4){
    $newest_products = getNewestProducts();
}
else if(($newest_prod_count < 4) && ($newest_prod_count > 0)) {
    $rand_prod_num = 4 - $newest_prod_count;
    $newest_products = array_merge(getNewestProducts(),getRandomProducts($rand_prod_num));
}
else {
    $newest_products = getRandomProducts();
}
?>
<? if (count($newest_products) > 0) { ?>
    <section id="home-products">
        <div class="wrapper row">
            <div class="col-md-8 col-md-offset-2">
                <div class="table-row">
                    <div class="table-cell" style="width:50%"><div class="border"></div></div>
                    <div class="table-cell" style="padding: 0 50px"><h2 style="white-space: pre-line;">BIŻUTERIA NA ZAMÓWIENIE</h2></div>
                    <div class="table-cell" style="width:50%"><div class="border"></div></div>
                </div>
            </div>
        </div>
        <div class="wrapper row">
            <div class="col-md-12 jewellery-banner">
                <img style="display: block; object-fit: contain; max-height: 100%;" src="assets/images/ring_banner.png">
                <div>
                    <h3>Potrzebujesz <strong>gotowego pierścionka</strong>, kolczyków, bransoletki?<br>
                        Zobacz naszą ofertę biżuterii wykonywanej
                        na indywidualne zamówienie.
                    </h3>
                    <div class="more-product">
                        <a class="jewellery-banner-see" href="<?= BASE_URL ?>/bizuteria">Zobacz</a>
                    </div>
                </div>
            </div>

        </div>
    </section>
<?}?>

<section id="home-about">
    <div class="wrapper row">
        <div class="col-md-10 col-md-offset-1">
            <article>
                <header>
                    <h2 class="title">O nas</h2>
                </header>
                <p><?= renderPageContent('o-nas-glowna'); ?></p>
            </article>
        </div>
    </div>
</section>

<? home_services(); ?>

<section id="home-posts">
    <div class="wrapper row">
        <h2 class="title">Aktualności</h2>
        <div class="col-md-12">
            <div class="row">
                <? $index = 0; ?>
                <? foreach ($news_posts as $post) { ?>
                    <? if($index == 0 || $index == 1) { ?>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6 col-md-hide thumb">
                                    <div class="thumbnail"><img src="<?= $post['img'] ?>" alt="<?= $post['title'] ?>" /></div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 content">
                                    <div class="post-content">
                                        <span class="date"><?= $post['date'] ?></span>
                                        <h2><a href="<?= BASE_URL ?>/aktualnosci/<?= convertToSlug($post['title']); ?>"><?= $post['title'] ?></a></h2>
                                        <p><?= mb_substr(trim($post['body']),0,180,'UTF-8'); ?></p>
                                        <a href="<?= BASE_URL ?>/aktualnosci/<?= convertToSlug($post['title']); ?>" class="rmore">Zobacz więcej</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? } else { ?>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12 content">
                                    <div class="post-content">
                                        <span class="date"><?= $post['date'] ?></span>
                                        <h2><a href="<?= BASE_URL ?>/aktualnosci/<?= convertToSlug($post['title']); ?>"><?= $post['title'] ?></a></h2>
                                        <p><?= mb_substr(trim($post['body']),0,180,'UTF-8'); ?></p>
                                        <a href="<?= BASE_URL ?>/aktualnosci/<?= convertToSlug($post['title']); ?>" class="rmore">Zobacz więcej</a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-hide thumb">
                                    <div class="thumbnail"><img src="<?= $post['img'] ?>" alt="<?= $post['title'] ?>" /></div>
                                </div>
                            </div>
                        </div>
                    <? } ?>
                    <? $index++; ?>
                <? } ?>
            </div>
            <div class="row">
                <div class="col-md-12 more">
                    <a href="<?= BASE_URL ?>/aktualnosci">wszystkie aktualności</a>
                </div>
            </div>

        </div>
    </div>
</section>

<a id="zloto-inwestycyjne-banner" href="https://zlotoinwestycyjne.pl" target="_blank" rel="noopener noreferrer">
    <div id="zloto-banner-img"></div>
</a>

<? footer(); ?>

<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= BASE_URL ?>/assets/js/jquery.tooltipster.min.js"></script>
<script src="<?= BASE_URL ?>/assets/js/ion.rangeSlider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.sliderRangeStorage.js"></script>
<script src="<?= BASE_URL ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?= BASE_URL ?>/assets/js/script.js"></script>

<script src="<?= BASE_URL ?>/assets/js/whcookies.js"></script>

<script>
  localStorage.removeItem('priceTo');
  localStorage.removeItem('priceFrom');
  localStorage.removeItem('massTo');
  localStorage.removeItem('massFrom');
</script>
</body>
</html>
