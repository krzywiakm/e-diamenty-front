Palma wskazująca złoża diamentów?
20/06/2016
//e-diamenty.pl/content/uploads/2016/01/18.jpg
<p>Geolodzy odkryli palmę, która wskazuje na lokalizację złóż diamentów. Rosnąca w Liberii roślina Pandanus candelabrum występuje wyłącznie tam, gdzie w kolumnach kimberlitowych znajdują się diamenty.</p>
<p>Diamenty powstają setki kilometrów pod powierzchnią Ziemi i są wynoszone przez erupcje wulkaniczne m.in. w kominach kimberlitowych. W Afryce Zachodniej wiele ludzi zajmuje się poszukiwaniem diamentów w osadach rzecznych. Trafiają tam kamienie wypłukane z kominów kimberlitowych. Poszukiwanie takich kominów w dżungli jest bardzo trudne.</p>
<p>W Liberii w glebie nad kominem znaleziono cztery diamenty. Najważniejsze było jednak odkrycie, że znajduje się tam roślina, która prawdopodobnie występuje wyłącznie przy kominach kimberlitowych. We współpracy z naukowcami z Królewskiego Ogrodu Botanicznego w Kent oraz Missouri Botanical Garden zidentyfikowano roślinę jako Pandanus candelabrum. Wiadomo, że występuje ona od Kamerunu po Senegal.</p>
<p>Jeśli spostrzeżenia co do rośliny okażą się prawdziwe, znacznie łatwiej będzie wyszukiwać kominy kimberlitowe. Dla krajów Afryki Zachodniej może być to spora szansa.</p>
 