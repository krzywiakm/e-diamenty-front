Lesedi la Rona – za droga na sprzedaż?
13/07/2016
//e-diamenty.pl/content/uploads/2016/01/16.jpg
<p>Aukcja drugiego największego wydobytego jak do tej pory diamentu zakończyła się fiaskiem. Sprzedający nie zgodził się na zaproponowane na aukcji 61 milionów dolarów.</p>
<p>Wydobyty w 2015 roku w Botswanie diament Lesedi la Rona jest wielkości piłki tenisowej. Liczy sobie około trzech miliardów lat. Właściciel kamienia wyznaczył cenę minimalną, za jaką jest w stanie oddać diament, na 70 milionów dolarów.</p>
<p>Nie udało się jednak znaleźć kupca, który chciałby tyle zapłacić. Zaproponowano 61 milionów dolarów, ale kwota ta nie usatysfakcjonowała sprzedającego.</p>
<p>Możliwe, że Lesedi la Rona zostanie sprzedany prywatnemu nabywcy, z wyłączeniem publicznej aukcji, albo wypożyczony do muzeum.</p>
 