Niezwykle rzadki fioletowy diament
07/06/2016
//e-diamenty.pl/content/uploads/2016/01/14.jpg
<p>Kolorowe diamenty są rzadkością w naturze, natomiast barwa ma znaczenie. Niezwykle rzadki fioletowy kamień to największy tego typu diament znaleziony w kopalni Argyle w Australii należącej do Rio Tonto. 2,83-karatowy diament zwany The Argyle Violet jest wart miliony!</p>
<p>Jak twierdzi Josephine Johnson menedżer Argyle Pink Diamonds – Jesteśmy bardzo podekscytowani ogłoszeniem tego historycznego wydobycia. Ten niezwykły, fioletowy diament pobudzi wyobraźnię największych na świecie kolekcjonerów i koneserów diamentów.</p>
<p>Diament wypolerowany został przez australijskiego mistrza Richarda How Kim Kam z 9,17-karatowego kamienia do 2,83-karatowego diamentu o kształcie owalnym.</p>
 