<h1>Kontakt</h1>
<p><span>Telefon</span><br /><strong><a href="tel:+48571207208">+48 571 207 208</a></strong></p>
<p><span>email dla klientów</span><br /><strong><a href="mailto:kontakt@e-diamenty.pl">kontakt@e-diamenty.pl</a></strong></p>
<address>
Adres siedziby<br />
e-diamenty.pl<br />
ul. Świerzawska 1<br />
60-321 Poznań
</address>
<br />
<p>Jesteśmy do Państwa dyspozycji<br /><strong>od poniedziałku do piątku w godz. 8.00 - 16.00</strong></p>
<p>W przypadku chęci obejrzenia lub odbioru osobistego kamieni uprzejmie prosimy o wcześniejszy kontakt</p>
<br /><br />
<h2>Dane firmy</h2>
<p>Portal <a href="http://www.e-diamenty.pl">www.e-diamenty.pl</a> prowadzony jest przez</p>
<address class="investment">
DIAMOND INVESTMENT COMPANY<br />
T. KWIATKIEWICZ M. KWIATKIEWICZ SPÓŁKA JAWNA<br />
ul. Świerzawska 1<br />
60-321 Poznań<br />
NIP: 7792389565<br />
REGON: 301692462<br />
KRS: 0000380507
</address>

<p><span>Nasze konto bankowe</span><br /><strong>ING Bank Śląski: 14 1050 1520 1000 0090 3132 8553&#8203;</strong></p>


