<div class="wrapper row content">
    <div class="col-md-8 col-md-offset-2">
        <article>
            <header>
               <div class="video-container" data-embed-url="https://www.youtube.com/embed/YS7i2EpeLrI">
                    <img class="video-overlay" src="<?= BASE_URL ?>/assets/images/certyfikaty-diamentow-video.jpg" alt="certyfikaty-diamentow-video" class="aligncenter" />
                </div>
                <h1 class="title">Certyfikaty diamentów</h1>
            </header>

            <p>Wszystkie oferowane przez nas diamenty posiadają certyfikat autentyczności poświadczający autentyczność oraz jakość danego kamienia. </p>
            <p>Nasze diamenty certyfikowane są wyłącznie w najbardziej wiarygodnych światowych laboratoriach  gemmologicznych:</p>
            <ul>
                <li>GIA - Gemological Institute Of America</li>
                <li>IGI - International Gemological Institute</li>
                <li>HRD Antwerp</li>
            </ul>
            <p>Natomiast w przypadku diamentów, dla których koszt międzynarodowego certyfikatu jest zbyt wysoki, jakość kamienia poświadczamy firmowym certyfikatem autentyczności e-diamenty.pl W takim przypadku ocena jakości przeprowadzona jest zgodnie z obowiązującymi przepisami światowej rady ds diamentów i według standardów GIA - Gemological Institute of America przez naszego certyfikowanego eksperta - GIA Diamonds Graduate.</p>
            <p>Sprzedawane przez nas diamenty są naturalne i pochodzą ze źródeł legalnych, nie zaangażowanych w finansowanie konfliktów, w zgodzie z Rezolucjami ONZ.<br><br>
Sprzedawane przez nas diamenty są wolne od wad fizycznych i prawnych, oraz zostały legalnie wprowadzone na rynek Unii Europejskiej.</p>
            <br />
<h2>JAK ZWERYFIKOWAĆ AUTENTYCZNOŚCI ZAKUPIONYCH DIAMENTÓW?</h2>

            <p><a target="_blank" href="http://www.ptgem.org.pl/index.php?id=8">TUTAJ </a> znajdą Państwo ogólnokrajową listę Rzeczoznawców Diamentów Polskiego Towarzystwa Gemmologicznego, lub <a target="_blank" href="http://rzeczoznawcy.srj.org.pl/">TUTAJ </a> listę Rzeczoznawców Stowarzyszenie Rzeczoznawców Jubilerskich, u których w razie jakichkolwiek wątpliwości możecie zweryfikować autentyczność oraz jakość zakupionych u nas diamentów.</p>
            
        </article>
    </div>
</div>