<div class="wrapper row content">
    <div class="col-md-8 col-md-offset-2">
        <article>
            <h1 class="title">Bezpieczeństwo zakupów</h1>
			<h2>Bezpieczeństwo transakcji</h2>
			<p>Nad bezpieczeństwem płatności kartami kredytowymi czuwa firma Przelewy24. – największa na polskim rynku firma obsługująca płatności elektroniczne realizowane w sklepach internetowych. System autoryzacji kart płatniczych oraz przelewów bankowych w Przelewy24. gwarantuje klientom sklepów internetowych najwyższy poziom bezpieczeństwa.
			Bezpieczeństwo transakcji kartami zapewniane jest przez zastosowanie skutecznych zabezpieczeń transakcji – 3D Secure (Verified by VISA oraz MasterCard Secure Code), SSL 128 bitów (Secure Sockets Layer). System zapewnia wysokie bezpieczeństwo również dzięki rozwiązaniu, w którym numery kart płatniczych nigdy nie trafiają do sklepu internetowego i co równie ważne, nigdzie nie są one przechowywane.</p>
			<p>Klient naszego sklepu ma gwarancję, że numer jego karty zna tylko on i jego bank. Bezpieczeństwo transakcji dokonywanych za pomocą przelewów bankowych jest zapewnianie przez zastosowanie 128 bitowego protokołu SSL, jak również przez procedury identyfikacji klienta w banku (login, hasło, hasło jednorazowe). Dokonując przelewu, Klient loguje się bezpiecznie bezpośrednio na stronie swojego banku. Informacje o jego profilu posiada tylko i wyłącznie jego bank.*</p>
			<p>Pracownicy e-diamenty.pl dysponują jedynie danymi teleadresowymi i adresem, pod który zakupione produkty mają zostać wysłane i na żadnym etapie realizacji zamówienia nie maja dostępu do informacji związanych z numerami kart kredytowych oraz danymi z nimi związanymi.</p>

			<p>*Informacje firmy Przelewy24</p>
        </article>
    </div>
</div>