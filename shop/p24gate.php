<? include 'func.php'; ?>
<? 
if(auth()) 
{ 
    $current_user = getUserData($_SESSION["USER_ID"]); 
}
else 
{
    header('Location: ' . BASE_URL . '/views/sklep/koszyk');
    die();
}
if ($_POST["p24_merchant_id"] != "42182") {
    header('Location: ' . BASE_URL . '/views/sklep/koszyk');
    die();
}
$transactionId = (int) $_POST["p24_description"];
$sessionId = $_POST["p24_session_id"];
setTransactionP24sessionId($transactionId, $sessionId);
?>
<form style="display: none;" id="hiddenP24Form" action="https://secure.przelewy24.pl/trnDirect" method="post">
<?php
    foreach ($_POST as $a => $b) {
        echo '<input type="hidden" name="'.htmlentities($a).'" value="'.htmlentities($b).'">';
    }
?>
</form>
<script type="text/javascript">
    document.getElementById('hiddenP24Form').submit();
</script>