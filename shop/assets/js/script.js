(function($) {
  var BASE_URL = '//e-diamenty.pl';

  var add_to_cart_modal = ' <div style="display:none;" class="md-modal" id="md-addtocart">' +
                      '<div class="md-content">' +
                                '<div class="md-close"></div>' +
                        '<h3>produkt dodany do koszyka </h3>' +
                      '</div>' +
                    '</div>';
  $('body').append(add_to_cart_modal);


  if(sessionStorage.length == 0){
      sessionStorage.setItem('observed', JSON.stringify([]));
      sessionStorage.setItem('compared', JSON.stringify([]));
      sessionStorage.setItem('cart', JSON.stringify([]));
  }

  var cart_init = false;
  function reloadCart() {
    var cart = decodeURIComponent($.param( {ids: (sessionStorage.getItem('cart')).replace(/]|\[/g,''), cart: true } ));
    $.get( BASE_URL + '/webservice.php?' + cart, function( data ) {
        var mini_cart_list = $('#nav .mini-cart ul.bx-cart');
        mini_cart_list.html('');
        var subtotal = 0;
        jQuery.each(data, function(k,v){
            var price = Number(v.price);
            var qty = 1;
            if (sessionStorage.getItem('item'+v.id) != null && sessionStorage.getItem('item'+v.id) != undefined) {
              qty = parseInt(sessionStorage.getItem('item'+v.id));
            }
            subtotal += price*qty;
            var item = '<li style="min-width:100% !important;">' +
                            '<div class="mini-cart-product-item"> ' +
                            '<a href="' + v.url + '"><img style="width:25px;height:25px;" src="' + v.image + '" alt="product" class="alignleft" /></a> ' +
                                '<h2>' +
                                    '<a href="' + v.url + '" title="#">' + v.shape + ' ' + v.mass + 'ct ' + v.color + '/' + v.clarity + '</a>' +
                                '</h2> ' +
                                '<p class="qty">Ilość: <span class="qtyitem' + v.id + '">' + qty + '</span></p>' +
                                '<p class="amount">' + price  + ' PLN</p>' +
                            '</div> ' +
                        '</li> '
            mini_cart_list.append(item);
        });

        $('.mini-cart-subtotal .amount').text((Math.round(subtotal * 100) / 100) + ' PLN');
        if (cart_init == false) {
          $('.bx-cart').bxSlider({
              mode: 'vertical',
              controls: true,
              pager: false,
              auto: false,
              nextSelector: '#slider-next',
              prevSelector: '#slider-prev',
              minSlides: 2,
              maxSlides: 2,
              adaptiveHeight: true
          });
        }
        cart_init = true;
    }, 'json');
    $('#nav .cart span').html("("+ (JSON.parse(sessionStorage.getItem('cart'))).length +")");
  }
  reloadCart();

  // empty cart
  function emptyCart() {
      sessionStorage.setItem('cart', "[]");
  }

  // cart
  $(document).on("click", "a.add-product-to-cart", function (e) {
      e.preventDefault();
      console.log('cart', JSON.parse(sessionStorage.getItem('cart')));
      var id = $(this).data('id');
      var cart = JSON.parse(sessionStorage.getItem('cart'));

      if($.inArray(id, cart) ==  -1){
          cart.push(id)
          $(this).addClass('active');
          $('#md-addtocart').show();
          $("#md-addtocart").css("visibility","visible");
          setTimeout(function(){
            $('#md-addtocart').fadeOut();
          }, 1000);
      } else{
          cart.splice( $.inArray(id, cart), 1 );
          $(this).removeClass('active');
      }

      sessionStorage.setItem('cart', JSON.stringify(cart));
      $('#nav .cart span').html(""+ (JSON.parse(sessionStorage.getItem('cart'))).length +"");
      reloadCart();
      console.log('cart', JSON.parse(sessionStorage.getItem('cart')));
  })

  // cart
  $(document).on("click", "button.product-to-cart", function (e) {
      var id = $(this).data('id');
      var cart = JSON.parse(sessionStorage.getItem('cart'));

      if($.inArray(id, cart) ==  -1){
          cart.push(id)
          $(this).addClass('active');
          sessionStorage.setItem('item'+id, parseInt(window['item'+id].value()));
          $('#md-addtocart').show();
          $("#md-addtocart").css("visibility","visible");
          setTimeout(function(){
            $('#md-addtocart').fadeOut();
          }, 1000);
      } else{
          cart.splice( $.inArray(id, cart), 1 );
          $(this).removeClass('active');
      }

      sessionStorage.setItem('cart', JSON.stringify(cart));
      $('#nav .cart span').html(""+ (JSON.parse(sessionStorage.getItem('cart'))).length +"");
      reloadCart();
      console.log('cart', JSON.parse(sessionStorage.getItem('cart')));
  })

  $.fn.menumaker = function(options) {
      
      var cssmenu = $(this), settings = $.extend({
        title: "",
        format: "dropdown",
        breakpoint: 992,
        sticky: false
      }, options);

      return this.each(function() {
        cssmenu.find('li ul').parent().addClass('has-sub');
        if (settings.format != 'select') {
          cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
          $(this).find("#menu-button").on('click', function(){
            $(this).toggleClass('menu-opened');
            var mainmenu = $(this).next('ul');
            if (mainmenu.hasClass('open')) { 
              mainmenu.hide().removeClass('open');
            }
            else {
              mainmenu.show().addClass('open');
              if (settings.format === "dropdown") {
                mainmenu.find('ul').show();
              }
            }
          });

          multiTg = function() {
            cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
            cssmenu.find('.submenu-button').on('click', function() {
              $(this).toggleClass('submenu-opened');
              if ($(this).siblings('ul').hasClass('open')) {
                $(this).siblings('ul').removeClass('open').hide();
              }
              else {
                $(this).siblings('ul').addClass('open').show();
              }
            });
          };

          if (settings.format === 'multitoggle') multiTg();
          else cssmenu.addClass('dropdown');
        }

        else if (settings.format === 'select')
        {
          cssmenu.append('<select style="width: 100%"/>').addClass('select-list');
          var selectList = cssmenu.find('select');
          selectList.append('<option>' + settings.title + '</option>', {
                                                         "selected": "selected",
                                                         "value": ""});
          cssmenu.find('a').each(function() {
            var element = $(this), indentation = "";
            for (i = 1; i < element.parents('ul').length; i++)
            {
              indentation += '-';
            }
            selectList.append('<option value="' + $(this).attr('href') + '">' + indentation + element.text() + '</option');
          });
          selectList.on('change', function() {
            window.location = $(this).find("option:selected").val();
          });
        }

        if (settings.sticky === true) cssmenu.css('position', 'fixed');

        resizeFix = function() {
          if ($(window).width() > settings.breakpoint) {
            cssmenu.find('ul').show();
            cssmenu.removeClass('small-screen');
            if (settings.format === 'select') {
              cssmenu.find('select').hide();
            }
            else {
              cssmenu.find("#menu-button").removeClass("menu-opened");
            }
          }

          if ($(window).width() <= settings.breakpoint && !cssmenu.hasClass("small-screen")) {
            cssmenu.find('ul').hide().removeClass('open');
            cssmenu.addClass('small-screen');
            if (settings.format === 'select') {
              cssmenu.find('select').show();
            }
          }
        };
        resizeFix();
        return $(window).on('resize', resizeFix);

      });
  };

    var QueryString = function () {
      // This function is anonymous, is executed immediately and 
      // the return value is assigned to QueryString!
      var query_string = {};
      var query = window.location.search.substring(1);
      var vars = query.split("&");
      for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
            // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
          query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
          var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
          query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
          query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
      } 
      return query_string;
    }();

    $(document).ready(function(){

        $('.choose').not('.always-show').css('display', 'none');

        $('.slidedown').on({
            mouseover: function(){
                $(this).find('.choose').stop().slideDown();
            },

            mouseleave: function(){
                $(this).find('.choose').stop().slideUp();
            }
        });


        // responsive menu 
        $(window).load(function() {
            $("#cssmenu").menumaker({
                title: "",
                format: "dropdown"
            });
        });
        
        // add class to scroll
        $(window).on('scroll load', function() {
            var totalScroll = $(window).scrollTop();
            var headerHeight = $('.sticky').height();
            if (totalScroll >= 125) {
                $('.sticky').addClass('scroll');
                $('.static').addClass('noscroll');
            } else {
                $('.sticky').removeClass('scroll');
                $('.static').removeClass('noscroll');
            }
        });
        
      // start tooltip
      if($('.tooltip').length) {
        $('.tooltip').tooltipster({
          trigger: 'click',
              position: 'right',
              maxWidth: 350,
              theme: 'tooltipster-light',
              interactive: true,
              animation: 'grow',
              touchDevices: true
        });
      }
        
    	$(window).keypress(function() {
        if ($('.tooltip').length) {
          $('.tooltip').tooltipster('hide');
        }
    	});

      $(".video-overlay").click(function(){
        var embed_url = $( this ).parent().data('embed-url');
        $( ".video-container" ).append( '<iframe class="video-embed" width="560" height="380" src="' + embed_url + '?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>' );
        $( this ).hide();
      });

      if (QueryString.playvideo == 'true') {
        $('html, body').animate({
            scrollTop: $(".video-overlay").first().offset().top - 100
        }, 2000);
        $(".video-overlay").first().trigger( "click" );
      }

    });

    //observed
    $(document).on("change", "#add_to_observe", function () {
        var id = $(this).data('id');
        var observed = JSON.parse(sessionStorage.getItem('observed'));

        if($(this).is(":checked") ){
            observed.push(id);
        } else{
            observed.splice( $.inArray(id, observed), 1 );
        }

        sessionStorage.setItem('observed', JSON.stringify(observed));
        $('#nav .watch span').html(""+ (JSON.parse(sessionStorage.getItem('observed'))).length +"");

        console.log('observed', JSON.parse(sessionStorage.getItem('observed')));
    });


    //comparison
    $(document).on("change", "#add_to_compare", function () {
        var id = $(this).data('id');
        var compared = JSON.parse(sessionStorage.getItem('compared'));

        if($(this).is(":checked")){
            compared.push(id)
        } else{
            compared.splice( $.inArray(id, compared), 1 );
        }

        sessionStorage.setItem('compared', JSON.stringify(compared));
        $('.compare .comp span').html(""+ (JSON.parse(sessionStorage.getItem('compared'))).length +"");

        console.log('compared', JSON.parse(sessionStorage.getItem('compared')));
    });

    $(document).on("click", ".quantity-single-product", function (e) {
      console.log('click');

        $('.quantity-message').html();

        var id = $(this).data('id');
        var changeBy = $(this).data('changeby');

        var item = window['item'+id];
        console.log(changeBy);

        if(changeBy == "plus") {
            item.increment();
            $('.quantity-value[data-id='+id+']').val(item.value());
        } 
        else {
            if(item.value() == 0){
                return true
            } else{
              if (item.value() > 1) {
                item.decrement();
                $('.quantity-value[data-id='+id+']').val(item.value());
              }
            }
        }

    });
    $(document).on("change", ".quantity-value", function () {
      if ( parseInt($(this).val()) < 1 ) {
          $(this).val(1);
      }
      var id = $(this).data('id');
      window['item'+id].setValue(parseInt($(this).val()));
    });
    
    $( "#mobile-login-menu-btn" ).click(function() {
      $('.mobile-login-menu').toggle();
    });
    
})(jQuery);

$(window).on('scroll', function(e){
    let scroll = $(window).scrollTop();
    if(scroll > 200){
        $(".catalog-scroll-down").css("display", "none")
    } else{
        $(".catalog-scroll-down").css("display", "block")
    }
});
