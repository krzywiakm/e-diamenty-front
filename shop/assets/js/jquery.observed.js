$(document).ready(function () {
    var BASE_URL = '//e-diamenty.pl';

    $('.result p span').html((JSON.parse(sessionStorage.getItem('observed'))).length);
    $('#nav .watch span').html("("+ (JSON.parse(sessionStorage.getItem('observed'))).length +")");
    $('#nav .cart span').html("("+ (JSON.parse(sessionStorage.getItem('cart'))).length +")");

    //observed
    $(document).on("click", ".Table .remove a", function (e) {
        e.preventDefault();
        var id = $(this).parents('.TableRow').data('id');
        var tableRow = $(this).parents('.TableRow');
        var observed = JSON.parse(sessionStorage.getItem('observed'));

        observed.splice( $.inArray(id, observed), 1 );
        tableRow.remove();


        sessionStorage.setItem('observed', JSON.stringify(observed));

        $('#nav .watch span').html("("+ (JSON.parse(sessionStorage.getItem('observed'))).length +")");
        $('.result p span').html((JSON.parse(sessionStorage.getItem('observed'))).length);
    });


     $(document).on("click", ".result a.remove", function (e) {
         e.preventDefault();
         sessionStorage.setItem('observed', JSON.stringify([]));
         $('#nav .watch span').html("("+ (JSON.parse(sessionStorage.getItem('observed'))).length +")");
         $('.result p span').html((JSON.parse(sessionStorage.getItem('observed'))).length);
         $('.TableRow').remove();
    });

    //observed
    $(document).on("change", ".Table .watch input", function () {
        var id = $(this).parents('.TableRow').data('id');
        var tableRow = $(this).parents('.TableRow');
        var observed = JSON.parse(sessionStorage.getItem('observed'));

        if($(this).is(":checked") ){
            observed.push(id);
        } else{
            observed.splice( $.inArray(id, observed), 1 );
            tableRow.remove();
        }

        sessionStorage.setItem('observed', JSON.stringify(observed));
        $('#nav .watch span').html(""+ (JSON.parse(sessionStorage.getItem('observed'))).length +"");
    })


    //comparison
    $(document).on("change", ".Table .comparison input", function () {
        var id = $(this).parents('.TableRow').data('id');
        var compared = JSON.parse(sessionStorage.getItem('compared'));

        if($(this).is(":checked")){
            compared.push(id)
        } else{
            compared.splice( $.inArray(id, compared), 1 );
        }

        sessionStorage.setItem('compared', JSON.stringify(compared));
        $('.compare .comp span').html(""+ (JSON.parse(sessionStorage.getItem('compared'))).length +"");
    })

    // cart

    $(document).on("click", ".Table .cart a", function (e) {
        e.preventDefault();
        var id = $(this).parents('.TableRow').data('id');
        var cart = JSON.parse(sessionStorage.getItem('cart'));

        if($.inArray(id, cart) ==  -1){
            cart.push(id)
        } else{
            cart.splice( $.inArray(id, cart), 1 );
        }

        sessionStorage.setItem('cart', JSON.stringify(cart));
        $('#nav .cart span').html("("+ (JSON.parse(sessionStorage.getItem('cart'))).length +")");

        console.log('cart', JSON.parse(sessionStorage.getItem('cart')));
    });


    var s;
    var records = 20;
    var fetchResults = {

        sortData: {
            records: records,
            offset: 0,
            direction: "asc",
            orderBy: "shape"
        },


        init: function(){
            s = this.sortData;
            this.load();
            this.onArrow();
        },

        load: function(){
            console.log( "LOAD") ;
            fetchResults.viewResults();
            s.offset = s.offset + s.records;
        },

        onArrow: function(){
            $('.arrow').on('click', function(){
                console.log( "ARROW") ;

                var $this = $(this);

                $.when($('.TableRow').remove()).then( function(){
                    s.orderBy = $this.parent().attr('data-orderBy');
                    s.direction = $this.data('direction');
                    s.records = s.offset == 0 ? records : s.offset;
                    s.offset = 0;
                });

                fetchResults.load();

                s.offset = s.records;
                s.records = records;
            })
        },

        viewResults: function () {

            var observed = decodeURIComponent($.param( {ids: (sessionStorage.getItem('observed')).replace(/]|\[/g,'') } ));

            var filterParams = observed+'&records='+s.records+'&offset='+s.offset+'&direction='+s.direction+'&orderBy='+s.orderBy;

            var table = $('.Table');
            console.log(filterParams);
            $.get( BASE_URL + '/webservice.php?' + filterParams, function(data){

                jQuery.each(data, function(k,v){

                    var tmpTr = $('<div data-id="'+v.id+'" class="TableRow"/>');

                    tmpTr.append('<div class="TableCell watch"><input type="checkbox" id="watch' + v.id + '" /><label for="watch' + v.id + '"><span></span></label></div>' +
                        '<div class="TableCell comparison"><input type="checkbox" id="comp' + v.id + '" value="' + v.id + '"><label for="comp' + v.id + '"><span></span></label></div>'
                    );

                    tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell shape"><img style="width:27px;height:27px;" src="'+v.image+'" alt="'+v.shape+'"/>' + v.shape + '</div>');

                    jQuery.each(v, function(kk,vv){
                    switch (kk){
                        case 'url':
                            break;
                        case 'shortDesc':
                            break;
                        case 'availability':
                            if (vv == 0) { 
                                tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '"> Od ręki </div>') 
                            }
                            else if(vv == 'Brak danych') {
                                tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '"> Brak danych </div>') 
                            }
                            else {
                                tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '">' + vv + ' dni </div>')
                            }
                            break;
                        default:
                            return kk == 'id'|| kk == "shape" || kk == "image" ? '' :  tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell '+kk+'">'+vv+'</div>');
                    }
                    });
                    tmpTr.append('<div class="TableCell cart"><a href="#" data-id="'+v.id+'" title="Dodaj do koszyka">Dodaj do koszyka</a></div>')

                    table.append(tmpTr);
                })
            }, 'json').done(function(){

                var observed = JSON.parse(sessionStorage.getItem('observed'));
                var compared = JSON.parse(sessionStorage.getItem('compared'));


                $.each(observed, function (k, v) {
                    $('.Table').find('.TableRow[data-id="' + v + '"]').find('input[id="watch' + v + '"]').prop('checked', true);
                });

                $.each(compared, function (k, v) {
                    $('.Table').find('.TableRow[data-id="' + v + '"]').find('input[id="comp' + v + '"]').prop('checked', true);
                });
            })
        }
    }

    fetchResults.init();


});


