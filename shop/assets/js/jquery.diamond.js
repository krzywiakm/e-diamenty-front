$(document).ready(function () {
    var BASE_URL = '//e-diamenty.pl';

    // Barwa - color
    var $rangeColor = $("#color");
    if ($rangeColor.length) {
        $rangeColor.ionRangeSlider({
            type: "double",
            grid: true,
            values: ["D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
            onChange: function (data) {
                localStorage.setItem('serviceLock', 'true');
            },
            onFinish: function (data) {
                localStorage.setItem('serviceLock', 'false');
                $('#filtr').trigger('change');
            }
        });

        $rangeColor.on("change", function(){
            var $this = $(this),
                from = $('.filter-color .irs-from').html(),
                to = $('.filter-color .irs-to').html();

            $('#color_from').val(from);
            $('#color_to').val(to);
        });
    }

    // Czystość - clarity
    var $rangeClarity = $("#clarity");

    if ($rangeClarity.length) {

        $rangeClarity.ionRangeSlider({
            type: "double",
            grid: true,
            values: ["IF", "VVS1", "VVS2", "VS1", "VS2", "SI1", "SI2", "I1", "I2", "I3"],
            onChange: function (data) {
                localStorage.setItem('serviceLock', 'true');
            },
            onFinish: function (data) {
                localStorage.setItem('serviceLock', 'false');
                $('#filtr').trigger('change');
            }
        });

        $rangeClarity.on("change", function(){
            var $this = $(this),
                from = $('.filter-clarity .irs-from').html(),
                to = $('.filter-clarity .irs-to').html();

            $('#clarity_from').val(from);
            $('#clarity_to').val(to);
        });
    }

    // Dostępność - Liczba dni
    var $rangeAvailability = $("#availability");

    $rangeAvailability.ionRangeSlider({
        type: "double",
        min: 0,
        max: 30,
        from: 0,
        to: 30,
        onChange: function (data) {
            localStorage.setItem('serviceLock', 'true');
        },
        onFinish: function (data) {
            localStorage.setItem('serviceLock', 'false');
            $('#filtr').trigger('change');
        }
    });

    $rangeAvailability.on("change", function () {
        var $this = $(this),
            from = $this.data("from"),
            to = $this.data("to");

        $('#availability_from').val(from);
        $('#availability_to').val(to);
    });

    // Szlif
    var $rangeCut = $("#cut");
    $rangeCut.ionRangeSlider({
        type: "double",
        grid: true,
        force_edges: true,
        values: ["DOSKONAŁY", "BARDZO DOBRY", "DOBRY", "ZADOWALAJĄCY"],
        onChange: function (data) {
            localStorage.setItem('serviceLock', 'true');
        },
        onFinish: function (data) {
            localStorage.setItem('serviceLock', 'false');
            $('#filtr').trigger('change');
        }
    });
    $rangeCut.on("change", function () {
        var $this = $(this),
            from = $this.data("from") + 1,
            to = $this.data("to") + 1;

        $('#cut_from').val(from);
        $('#cut_to').val(to);
    });


    // Polerowanie
    var $rangePolishing = $("#polishing");
    $rangePolishing.ionRangeSlider({
        type: "double",
        grid: true,
        force_edges: true,
        values: ["DOSKONAŁY", "BARDZO DOBRY", "DOBRY", "ZADOWALAJĄCY"],
        onChange: function (data) {
            localStorage.setItem('serviceLock', 'true');
        },
        onFinish: function (data) {
            localStorage.setItem('serviceLock', 'false');
            $('#filtr').trigger('change');
        }
    });

    $rangePolishing.on("change", function () {
        var $this = $(this),
            from = $this.data("from") + 1,
            to = $this.data("to") + 1;

        $('#polishing_from').val(from);
        $('#polishing_to').val(to);
    });

    // Symetria
    var $rangeSymmetry = $("#symmetry");
    $rangeSymmetry.ionRangeSlider({
        type: "double",
        grid: true,
        force_edges: true,
        values: ["DOSKONAŁY", "BARDZO DOBRY", "DOBRY", "ZADOWALAJĄCY"],
        onChange: function (data) {
            localStorage.setItem('serviceLock', 'true');
        },
        onFinish: function (data) {
            localStorage.setItem('serviceLock', 'false');
            $('#filtr').trigger('change');
        }
    });

    $rangeSymmetry.on("change", function () {
        var $this = $(this),
            from = $this.data("from") + 1,
            to = $this.data("to") + 1;

        $('#symmetry_from').val(from);
        $('#symmetry_to').val(to);
    });

    $('a.advanced-search').click( function(e){
        e.preventDefault();
        $(this).toggleClass('up down');
        $('#advanced-filtr').toggle(1000, function(){
            $(this).toggleClass('hide-advanced-filters show-advanced-filters')
        })
        if ($(window).width() < 768) {
            $('.mobile-advanced-filters').toggle(1000, function(){})
        }
    });

    if ($(window).width() < 768) {
        $('.mobile-advanced-filters').hide();
    }

    $('input[name="sort_shape"]').on('change', function() {
        var $this = $(this);
        if ($this.is(':checked')) {
            $this.parent('li').css('opacity', 1);
        } else {
            $this.parent('li').removeAttr('style');
        }
    });

    $('input[name="sort_colour"]').on('change', function() {
        var $this = $(this);
        var position = $this.data('position')*34 ;
        if ($this.is(':checked')) {
            $this.siblings('label').css('background-position', -position + 'px 0px');
        } else {
            $this.siblings('label').removeAttr('style');
        }
    });

    if(sessionStorage.length == 0){
        sessionStorage.setItem('observed', JSON.stringify([]));
        sessionStorage.setItem('compared', JSON.stringify([]));
        sessionStorage.setItem('cart', JSON.stringify([]));
    }

    // console.log(sessionStorage);

    $('.compare .comp span').html(""+ (JSON.parse(sessionStorage.getItem('compared'))).length +"");
    $('#nav .watch span').html(""+ (JSON.parse(sessionStorage.getItem('observed'))).length +"");
    $('#nav .cart span').html(""+ (JSON.parse(sessionStorage.getItem('cart'))).length +"");


    //observed
    $(document).on("change", ".Table .watch input", function () {
        var id = $(this).parents('.TableRow').data('id');
        var observed = JSON.parse(sessionStorage.getItem('observed'));

        if($(this).is(":checked") ){
            observed.push(id);
        } else{
            observed.splice( $.inArray(id, observed), 1 );
        }

        sessionStorage.setItem('observed', JSON.stringify(observed));
        $('#nav .watch span').html(""+ (JSON.parse(sessionStorage.getItem('observed'))).length +"");

        // console.log('observed', JSON.parse(sessionStorage.getItem('observed')));
    })


    //comparison
    $(document).on("change", ".Table .comparison input", function () {
        var id = $(this).parents('.TableRow').data('id');
        var compared = JSON.parse(sessionStorage.getItem('compared'));

        if($(this).is(":checked")){
            compared.push(id)
        } else{
            compared.splice( $.inArray(id, compared), 1 );
        }

        sessionStorage.setItem('compared', JSON.stringify(compared));
        $('.compare .comp span').html(""+ (JSON.parse(sessionStorage.getItem('compared'))).length +"");

        // console.log('compared', JSON.parse(sessionStorage.getItem('compared')));
    })


    // cart
    $(document).on("click", ".Table .cart a", function (e) {
        e.preventDefault();
        var id = $(this).parents('.TableRow').data('id');
        var cart = JSON.parse(sessionStorage.getItem('cart'));

        if($.inArray(id, cart) ==  -1){
            cart.push(id)
            $(this).addClass('active');
            $('#md-addtocart').show();
            $("#md-addtocart").css("visibility","visible");
            setTimeout(function(){
                $('#md-addtocart').fadeOut();
            }, 1000);
        } else{
            cart.splice( $.inArray(id, cart), 1 );
            $(this).removeClass('active');
        }

        sessionStorage.setItem('cart', JSON.stringify(cart));
        $('#nav .cart span').html(""+ (JSON.parse(sessionStorage.getItem('cart'))).length +"");

        // console.log('cart', JSON.parse(sessionStorage.getItem('cart')));
    })

    var s;
    var records = 700;
    localStorage.setItem('serviceLock', 'false');
    var fetchResults = {

        sortData: {
            records: records,
            offset: 0,
            direction: "asc",
            orderBy: "price"
        },


        init: function(){
            s = this.sortData;
            this.onChangeFilters();
            this.onScroll();
            this.onClear();
            this.onArrow();
        },

        load: function(){
            fetchResults.viewResults();
            s.offset = s.offset + s.records;
            $("#content-1").slimScroll({
                height: '400px',
                alwaysVisible: true,
                allowPageScroll: false
            });

        },

        ignoreChanges: false,

        shapesInitialized: false,
        initializeShapeFiltersWorkaround: function () {
            if (fetchResults.shapesInitialized) {
                return;
            }

            const shapes = $('#filtr input[type="checkbox"]:checked').toArray();
            shapes.forEach(s => {
                s.parentElement.style.opacity = 1;
            });
            fetchResults.shapesInitialized = true;
        },

        onChangeFilters: function(){
            $('#filtr').on('change', function(e){
                if (fetchResults.ignoreChanges) {
                    return;
                }

                fetchResults.initializeShapeFiltersWorkaround();

                var target = e.target.name;

                if (target == 'sort_shape' || target == 'sort_colour' || target == 'certificate_gia' || target == 'certificate_igi' || target == 'certificate_hrd' || target == 'certificate_e_diamenty') {
                    localStorage.setItem('serviceLock', 'false');
                }
                $.when($('.TableRow').remove()).then( function(){
                    s.offset = 0;
                    $(".slimScrollBar,.slimScrollRail").remove();
                    $(".slimScrollDiv").contents().unwrap();
                });

                fetchResults.load();
            })
        },

        onScroll: function(){

            $("#content-1").slimScroll().bind('slimscroll', function(e, pos){
                // console.log("Reached " + pos);
                localStorage.setItem('serviceLock', 'false');

                if(pos == "bottom"){
                    fetchResults.viewResults();
                    s.offset = s.offset + s.records;
                }
            });

        },

        onArrow: function(){
            $('.arrow').on('click', function(){
                // console.log( "ARROW") ;
                localStorage.setItem('serviceLock', 'false');
                var $this = $(this);

                $.when($('.TableRow').remove()).then( function(){
                    s.orderBy = $this.parent().attr('data-orderBy');
                    s.direction = $this.data('direction');
                    s.records = s.offset == 0 ? records : s.offset;
                    s.offset = 0;
                });

                fetchResults.load();

                s.offset = s.records;
                s.records = records;
            })
        },

        onClear: function(){
            $('a.reset-filters').click(function(e){
                fetchResults.ignoreChanges = true;
                e.preventDefault();
                localStorage.setItem('serviceLock', 'false');
                console.log( "CLEAR") ;
                const shapes = $('#filtr input[type="checkbox"]:checked').toArray();
                shapes.forEach(s => {
                    s.checked = false;
                    s.parentElement.removeAttribute('style');
                });
                $rangePrice.data("ionRangeSlider").reset();
                $rangeMass.data("ionRangeSlider").reset();
                $rangeColor.data("ionRangeSlider").reset();
                $rangeClarity.data("ionRangeSlider").reset();
                $rangeAvailability.data("ionRangeSlider").reset();
                $rangeCut.data("ionRangeSlider").reset();
                $rangePolishing.data("ionRangeSlider").reset();
                $rangeSymmetry.data("ionRangeSlider").reset();

                if (localStorage.getItem('massMin') != null && localStorage.getItem('massMax') != null &&
                    localStorage.getItem('priceMin') != null && localStorage.getItem('priceMax') != null) {

                    var massParamInit = new function() {
                        this.name = 'mass';
                        this.divider = 100;
                        //database value, number format
                        this.min = Number(localStorage.getItem('massMin'));
                        this.max = Number(localStorage.getItem('massMax'));
                    };

                    var priceParamInit = new function() {
                        this.name = 'price';
                        this.divider = 1;
                        //database value, number format
                        this.min = Number(localStorage.getItem('priceMin'));
                        this.max = Number(localStorage.getItem('priceMax'));
                    };



                    function Grid(paramInit){

                        this.name = paramInit.name;
                        this.divider = paramInit.divider;
                        this.min = paramInit.min;
                        this.max = paramInit.max;
                        this.maxO = paramInit.max * paramInit.divider;
                        this.minO = paramInit.min * paramInit.divider;
                        this.values = []
                    }

                    Grid.prototype.recursiveGenerateGrid = function (digits) {

                        var self = this;
                        var compare = digits;
                        var roundtoOne = Number(1 + '0'.repeat(compare - 1));
                        var min = Math.ceil(this.minO/roundtoOne)*roundtoOne;
                        var step;

                        switch(this.name){
                            case "mass": step = calculateStepforMass(digits, compare); break;
                            case "price": step = calculateStepforPrice(digits, compare); break;
                        }

                        min = min < step ? step : min;
                        for (i = min; i <= this.maxO; i += step) {
                            compare = i.toString().length;
                            if (digits != compare) {
                                min = i;
                                return this.recursiveGenerateGrid(compare) ;
                            }
                            this.values.push(i/this.divider)
                        }
                    };

                    Grid.prototype.getValues = function(){
                        this.values.indexOf(this.min) < 0 ? this.values.unshift(this.min)  : "";
                        this.values.indexOf(this.max) < 0 ? this.values.push(this.max) : "";
                        return this.values
                    };

                    function calculateStepforMass(digits, compare){
                        return digits % 2 == 0 ? (digits < 3 ? Number(5 + '0'.repeat(compare - 1)/2) :  Number(1 + '0'.repeat(compare - 2)) ) : (digits < 4 ? Number(1 + '0'.repeat(compare - 1)) : Number(1 + '0'.repeat(compare - 2))) ;
                    }

                    function calculateStepforPrice(digits, compare){
                        return digits % 2 == 0 ? (digits < 3 ? Number(5 + '0'.repeat(compare - 1)) :  (digits < 8 ) ? Number(5 + '0'.repeat(compare - 2)) : Number(5 + '0'.repeat(compare - 3)) ) : (digits < 4 ? Number(1 + '0'.repeat(compare - 1)) : Number(1 + '0'.repeat(compare - 2)) ) ;
                    }

                    var mass = new Grid(massParamInit);
                    mass.recursiveGenerateGrid(mass.minO.toString().length);

                    var price = new Grid(priceParamInit);
                    price.recursiveGenerateGrid(price.minO.toString().length);

                    var priceValues = price.getValues();
                    var massValues = mass.getValues();

                    $rangePrice.data("ionRangeSlider").update({
                        from: priceValues.indexOf(Number(localStorage.getItem('priceMin'))),
                        to: priceValues.indexOf(Number(localStorage.getItem('priceMax')))
                    });

                    $rangeMass.data("ionRangeSlider").update({
                        from: massValues.indexOf(Number(localStorage.getItem('massMin'))),
                        to: massValues.indexOf(Number(localStorage.getItem('massMax')))
                    });
                }

                $.when($('.TableRow').remove()).then( function(){
                    s.offset = 0;
                    s.direction = "asc";
                    s.orderBy = "price";
                    s.records = records;
                });

                $(".slimScrollBar,.slimScrollRail").remove();
                $(".slimScrollDiv").contents().unwrap();

                fetchResults.load();
                fetchResults.ignoreChanges = false;
            })
        },

        viewResults: function () {
            if(localStorage.getItem('serviceLock') == 'false'){
                localStorage.setItem('serviceLock', 'true');

            var filterParams = $('#filtr').serialize() + '&records=' + s.records + '&offset=' + s.offset + '&direction=' + s.direction + '&orderBy=' + s.orderBy;
            var table = $('.Table');
            $.get( BASE_URL + '/webservice.php?' + filterParams, function (data) {
                $('.spinner').show();
                console.log(filterParams);

               //  var dataLength = 0;
               //  jQuery.each(data, function (k, v) { dataLength++; });
               //  console.log(dataLength);
               //  console.log(s.records);
               // if(dataLength < s.records/2){
               //   console.log("test");
               // };

                jQuery.each(data, function (k, v) {

                    var tmpTr = $('<div data-id="' + v.id + '" class="TableRow">');
                    tmpTr.append('<div class="TableCell watch"><input type="checkbox" id="watch' + v.id + '" /><label for="watch' + v.id + '"><span></span></label></div>' +
                        '<div class="TableCell comparison"><input type="checkbox" id="comp' + v.id + '" value="' + v.id + '"><label for="comp' + v.id + '"><span></span></label></div>'
                    );

                    tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell shape">' + '<a style="display:block;" href="' + v.url +'" alt="">' + '<img style="width:27px;height:27px;" src="'+v.image+'" alt="'+v.shape+'"/> ' + v.shape + '</a>' + '</div>');

                    jQuery.each(v, function (kk, vv) {
                        switch (kk){
                            case 'id':
                            case 'image':
                            case 'shape':
                                break;
                            case 'url':
                                break;
                            case 'shortDesc':
                                break;
                            case 'price':
                                tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '">' + vv + ' <span class="price-currency">PLN</span></div>')
                                break;
                            case 'availability':
                                if (vv == 0) {
                                    tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '"> Od ręki </div>')
                                }
                                else if(vv == 'Brak danych') {
                                    tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '"> Brak danych </div>')
                                }
                                else {
                                    tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '">' + vv + ' dni </div>')
                                }
                                break;
                            case 'mass':
                                tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '">' + vv + '</div>');
                                break;
                            case 'color':
                                tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '">' + vv + '</div>');
                                break;
                            case 'clarity':
                                tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '">' + vv + '</div>');
                                break;
                            case 'certificat':
                                tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '">' + vv + '</div>');
                                break;
                            case 'symmetry':
                                tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '">' + vv + '</div>');
                                break;
                            case 'cut':
                                tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '">' + vv + '</div>');
                                break;
                            case 'polishing':
                                tmpTr.append('<div style="cursor:pointer;" onclick="location.href=\'' + v.url + '\';" class="TableCell ' + kk + '">' + vv + '</div>');
                                break;
                            default:
                                break;

                        }
                    });

                    tmpTr.append('<div class="TableCell cart"><a href="#" data-id="' + v.id + '" title="Dodaj do koszyka">Dodaj do koszyka</a></div>')

                    table.append(tmpTr);
                })
            }, 'json').done(function () {

                var observed = JSON.parse(sessionStorage.getItem('observed'));
                var compared = JSON.parse(sessionStorage.getItem('compared'));


                $.each(observed, function (k, v) {
                    $('.Table').find('.TableRow[data-id="' + v + '"]').find('input[id="watch' + v + '"]').prop('checked', true);
                });

                $.each(compared, function (k, v) {
                    $('.Table').find('.TableRow[data-id="' + v + '"]').find('input[id="comp' + v + '"]').prop('checked', true);
                });

                setTimeout(function () {
                     $('.spinner').hide();
                     $('.TableRow').show();
                }, 500);

                // localStorage.setItem('serviceLock', 'false');
            })

            }
        }
    };

    fetchResults.init();
    setTimeout(function () {
        console.log('initload');
        fetchResults.load();
        fetchResults.initializeShapeFiltersWorkaround();
    }, 500);

});




