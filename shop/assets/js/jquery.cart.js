$(document).ready(function () {
    var BASE_URL = '//e-diamenty.pl';

    console.log('cart', JSON.parse(sessionStorage.getItem('cart')));
    $('.result p span').html((JSON.parse(sessionStorage.getItem('observed'))).length);
    $('#nav .watch span').html("("+ (JSON.parse(sessionStorage.getItem('observed'))).length +")");
    $('#nav .cart span').html("("+ (JSON.parse(sessionStorage.getItem('cart'))).length +")");


    console.log('Storage', sessionStorage,sessionStorage.getItem('cart'));
    console.log('cart1', (JSON.parse(sessionStorage.getItem('cart'))));
    items_qty = {}
    items_ids = JSON.parse(sessionStorage.getItem('cart'));
    jQuery.each(items_ids, function(k,v){
        console.log(v);
        console.log(sessionStorage.getItem('item'+v));
        if (sessionStorage.getItem('item'+v) == null) {
            sessionStorage.setItem('item'+v, 1);
        }
        items_qty[items_ids[k]] = sessionStorage.getItem('item'+v);
    });

    var cart = decodeURIComponent($.param( {ids: (sessionStorage.getItem('cart')).replace(/]|\[/g,''), qty: JSON.stringify(items_qty), cart: true } ));

    var filterParams = cart;
    var totalAmonut = 0;
    var totalDelivery = 0;

    $.get( BASE_URL + '/webservice.php?' + cart, function( data ) {

        var table = $('.cart-details');
        jQuery.each(data, function(k,v){
            if (sessionStorage.getItem('item'+v.id) == null) {
                window['item'+v.id] = counter();
                sessionStorage.setItem('item'+v.id, parseInt(window['item'+v.id].value()));
            }
            else {
                window['item'+v.id] = counter();
                window['item'+v.id].setValue(parseInt(sessionStorage.getItem('item'+v.id)));
            }
            var qty = 1;
            if (sessionStorage.getItem('item'+v.id) != null && sessionStorage.getItem('item'+v.id) != undefined) {
              qty = parseInt(sessionStorage.getItem('item'+v.id));
            }
            var price = Number(v.price);
            if (v.availability == 0) {
                v.availability = "Od ręki";
            }
            else {
                v.availability = v.availability + ' dni';
            }

            product_quantity = '<div>'+
                            '<p data-id="'+v.id+'" class="quantity-message"></p>'+
                            '<input data-changeby ="minus" data-id="'+v.id+'" type="button" value="" class="qtyminus quantity" field="quantity" /> ' +
                            '<input data-id="'+v.id+'" type="number" name="quantity" value="'+window['item'+v.id].value()+'" class="qty quantity-value" /> ' +
                            '<input data-changeby="plus" data-id="'+v.id+'" type="button" value="" class="qtyplus quantity" field="quantity" /> ' +
                        '</div>';

            if (v.certificat != 'e-diamenty.pl') {
                product_quantity = '<div>'+
                                '<input type="button" value="" class="qtyminus quantity" field="quantity" /> ' +
                                '<input type="number" name="quantity" value="1" class="qty quantity-value" readonly="" /> ' +
                                '<input type="button" value="" class="qtyplus quantity" field="quantity" /> ' +
                            '</div>';
            }

            var tmpTr = $('<tr data-id="'+v.id+'" class="TableRow"/>');
            tmpTr.append('<td class="product-name">' +
                            '<a href="' + v.url + '"><img style="width:27px;height:27px;" src="' + v.image + '" alt="product" class="alignleft" /></a>' +
                            '<div class="product-detail"> ' +
                                '<h2>' +
                                    '<a href="' + v.url + '" title="#">' + v.shape + ' ' + v.mass + 'ct ' + v.color + '/' + v.clarity + '</a>' +
                                '</h2> ' +
                                '<p>' + v.shortDesc + '</p>' +
                            '</div> ' +
                        '</td> ' +
                        '<td class="product-availability">'+ v.availability + '</td> ' +
                        '<td class="product-quantity"> ' +
                        product_quantity +
                        '</td> ' +
                        '<td data-id="'+v.id+'" class="product-price">' + price.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }) + ' </td> ' +
                        '<td data-id="'+v.id+'" class="product-value product-value-total"> ' + (price * window['item'+v.id].value()).toLocaleString("pl-PL", { style: "currency", currency: "PLN" }) + '</td> ' +
                        '<td class="product-remove"> <a href="#" title="Usuń produkt">X</a> ' + '</td>');
            table.append(tmpTr);

            var mini_cart_list_on_page = $('.mini-cart-item ul');
            var mini_item = '<li style="min-width:100% !important;">' +
                            '<div class="mini-cart-product-item"> ' +
                            '<img src="' + v.image + '" alt="product" class="alignleft" /> ' +
                                '<h2>' +
                                    '<a href="#" title="#">' + v.shape + ' ' + v.mass + 'ct ' + v.color + '/' + v.clarity + '</a>' +
                                '</h2> ' +
                                '<p class="qty">Ilość: <span style="font-size:14px;" class="qtyitem' + v.id + '">' + qty + '</span></p>' +
                                '<p class="amount">' + price  + ' PLN</p>' +
                            '</div> ' +
                        '</li> '
            if (mini_cart_list_on_page) {
                mini_cart_list_on_page.append(mini_item);
            }

            
            totalAmonut += price * parseInt(window['item'+v.id].value());
            if (v.availability == 'Od ręki') {
                totalDelivery = v.availability;
            }
            else {
                totalDelivery = totalDelivery > Number(v.availability) ? totalDelivery : v.availability;
                totalDelivery = totalDelivery + ' dni';
            }
            
        });
        totalAmonut = parseFloat(totalAmonut.toFixed(2));

        if (totalAmonut > 5000) {
            if ($('.shippment2').length > 0) {
                $('.shippment2').hide();
                $('.option-pobranie').hide();
            }
        }

        console.log('here:');
        console.log(totalAmonut);
        if (totalAmonut >= 5000) {
            $('.shippment1').find(".shippment-cost").text('0');
            console.log('gooo');
        }

        $('.price').html(totalAmonut + ' PLN');
        $('.day').html(totalDelivery);
        $('.mini-cart .mini-cart-total .total-amount').text(totalAmonut + ' PLN');
        $('.mini-cart .mini-cart-resume .resume-amount').text(totalAmonut + ' PLN');
        $('.cart-total .cart-total .total-amount').text(totalAmonut + ' PLN');

    }, 'json');

    $(document).on("click", ".quantity", function (e) {

        $('.quantity-message').html();

        var id = $(this).data('id');
        var price =   Number($('.product-price[data-id='+id+']').text().replace(/[^0-9\.]+/g,""))/100;
        var changeBy = $(this).data('changeby');

        var item = window['item'+id];

        if(changeBy == "plus"){

            item.increment();

            $('.quantity-value[data-id='+id+']').val(item.value());
            $('.product-value[data-id='+id+']').html((item.value() * price).toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));
            totalAmonut+=Number(price);
            $('.price').html(totalAmonut.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));
            $('.mini-cart-subtotal .amount').text(totalAmonut.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));
            sessionStorage.setItem('item'+id, item.value());
            $('.qtyitem'+id).text(item.value());

        } else{

            if(item.value() == 0){
                return true

            } else{
                if (item.value() > 1) {
                    item.decrement();

                    $('.quantity-value[data-id='+id+']').val(item.value());
                    $('.product-value[data-id='+id+']').html((item.value() * price).toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));
                    totalAmonut-=price;
                    $('.price').html(totalAmonut.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));
                    $('.mini-cart-subtotal .amount').text(totalAmonut.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));
                    sessionStorage.setItem('item'+id, item.value());
                    $('.qtyitem'+id).text(item.value());
                }
            }

        }

    });

    $(document).on("change", ".quantity-value", function () {
        if ( parseInt($(this).val()) < 1 ) {
            $(this).val(1);
        }
        var id = $(this).data('id');
        var item = window['item'+id];
        var price =   Number($('.product-price[data-id='+id+']').text().replace(/[^0-9\.]+/g,""))/100;
        item.setValue(parseInt($(this).val()));

        $('.quantity-value[data-id='+id+']').val(item.value());
        $('.product-value[data-id='+id+']').html((item.value() * price).toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));
        totalAmonut = Number(item.value()) * Number(price);
        $('.price').html(totalAmonut.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));
        $('.mini-cart-subtotal .amount').text(totalAmonut.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));
        sessionStorage.setItem('item'+id, item.value());
        $('.qtyitem'+id).text(item.value());
    });

    var counter = function(){
        var privateCounter = 1;

        function changeBy(val){
            privateCounter +=val;
        }

        return {
            setValue: function(value){
                privateCounter = value;
            },

            increment: function() {
                changeBy(1);
            },
            decrement: function() {
                changeBy(-1);
            },
            value: function() {
                return privateCounter;
            }
        }
    }


    $(document).on("click", ".product-remove a", function (e) {
        e.preventDefault();
        var id = $(this).parents('.TableRow').data('id');
        var tableRow = $(this).parents('.TableRow');
        var cart = JSON.parse(sessionStorage.getItem('cart'));
        var delivery = Number(tableRow.find('.product-availability').text());
        var total = Number(tableRow.find('.product-value-total').text().replace(/[^0-9\.]+/g,""))/100;

        totalAmonut -=total;

        console.log(delivery, totalDelivery, totalAmonut, Number(totalAmonut));

        $('.price').html(totalAmonut.toLocaleString("pl-PL", { style: "currency", currency: "PLN" }));

        cart.splice( $.inArray(id, cart), 1 );
        tableRow.remove();
        window['item'+id].setValue(1);

        if(totalDelivery == delivery){
            $('.product-availability').each(function(){
                totalDelivery = totalDelivery > Number($(this).text()) ? totalDelivery : Number($(this).text());
            });
        }

        $('.day').html(totalDelivery);

        sessionStorage.setItem('cart', JSON.stringify(cart));
        console.log('cart', JSON.parse(sessionStorage.getItem('cart')));

        $('#nav .cart span').html("("+ (JSON.parse(sessionStorage.getItem('cart'))).length +")");
    });

    $('input[type=radio][name=shippment]').change(function() {
        option = $(this).val();
        shippment_cost = Number($(this).siblings("label").find(".shippment-cost").text());
        current_total = Number($('.mini-cart .mini-cart-resume .resume-amount').text().replace(" PLN", ""));
        if (current_total < 5000) {
            totalAmonut = (current_total + shippment_cost).toFixed(2);
            $('.cart-delivery .delivery-amount').text(shippment_cost + ' PLN');
        }
        else {
            $('.cart-delivery .delivery-amount').text('0 PLN');
        }

        $('.mini-cart .mini-cart-total .total-amount').text(totalAmonut + ' PLN');
        $('.cart-total .cart-total .total-amount').text(totalAmonut + ' PLN');

        if (option == 1) {
            $('.option-pobranie').hide();
            $('.option-gotowka').hide();
            $('.option-przelew').show();
            $('.option-przelewy24').show();
            $('#payment2').prop('checked', false);
        }
        else if (option == 2) {
            $('.option-pobranie').show();
            $('.option-gotowka').hide();
            $('.option-przelew').hide();
            $('.option-przelewy24').hide();
            $('#payment2').prop('checked', true);
        }
        else if (option == 3) {
            $('.option-pobranie').hide();
            $('.option-gotowka').show();
            $('.option-przelew').show();
            $('.option-przelewy24').show();
            $('#payment2').prop('checked', false);
        }
        else {
            $('.option-pobranie').show();
            $('.option-gotowka').show();
            $('.option-przelew').show();
            $('.option-przelewy24').show();
            $('#payment2').prop('checked', false);
        }
    });

});