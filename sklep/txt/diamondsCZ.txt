<h1>4C - charakteristika diamantu</h1>

        <h2>V�ha (Carat)</h2>
<p>
V�ha diamant� se ud�v� v kar�tech. Metrick� kar�t (Carat) je definov�n jako 0,2 g. Pro men�� diamanty se pou��v� tak� bodov� syst�m, jim� d�l� 1 kar�t na 100 bod�. 10-bodov� briliant bude m�t 1/10 kar�tu, ozna�enou zpravidla jako 0,10 ct. </p>
<h2>Ryzost(Clarity)</h2>
<p>
U v�ech diamant� lze zaznamenat stopy jejich vzniku a v�voje po dobu mnoha stolet�. Mikroskopick� inkluze jsou neodd�liteln� vlastnost drahokam�. T��da �istoty diamantu ozna�uje velikost, po�et a um�st�n� vad v kameni, je� lze vid�t jen p�i 10-n�sobn�m zv�t�en�. Diamanty nemaj�c� ��dn� p��m�si ani kazy jsou dokonal�, ale v p��rod� se vyskytuj� neuv��iteln� vz�cn�. Za velmi vz�cn� jsou v�ak uzn�v�ny tak� kameny s okem neviditeln�mi ne�istotami. </p><p>
<img src="images/diamenty_czystosc_910x250.png">
</p>

<h2>Barva (Colour)</h2>
<p>

V�t�ina diamant� m� jemn� �lut� nebo bronzov� n�dechy. Barva diamant� se hodnot� na stupnici od D (bezbarv�) po Z (z�eteln� �lut� nebo nahn�dl�) a tento syst�m hodnocen� zabarven� diamant� se vykl�d� za pou�it� kontroln� sady p��rodn�ch diamant�. ��m bezbarv�j�� je diamant, t�m vy��� je jeho hodnota. V�jimkou jsou kameny s intenzivn�j��m zabarven�m: r��ov�m, modr�m nebo �lut�m. Diamanty s neobvykl�m nebo velmi siln�m zabarven�m b�vaj� ozna�ov�ny jako "m�dn�" (fancy) a v jejich p��pad� nedo�lo k um�l� zm�n� barvy fyzik�ln�m procesem. </p><p>
<img src="images/diamenty_barwa_910x250.png"></p>

<h2>V�brus(Cut)</h2>
<p>

Briliant je diamant s briliantov�m, jinak �e�eno kulat�m v�brusem. Diamanty s dal��mi v�brusy jsou pojmenov�ny podle sv�ho tvaru, a nej�ast�ji pou��van� v�brusy jsou: v�brus &#8222;princes&#8220;, v�brus ov�l, v�brus kapka - slza, v�brus srdce, v�brus mark�za, smaragdov� v�brus. V�brus diamantu m� rozhoduj�c� vliv na jeho lesk. M�l by z kamene dostat co nejlep�� estetick� p�ednosti. Pln� v�brus se skl�d� z 58 stran (56 faset, tabule a kalet). �patn� v�brus m��e zp�sobit sn��en� hodnoty briliantu a� o polovinu. Hodnocen� kvality brusu diamant� je nejt쾹� a z�rove� nejd�le�it�j�� sou��st� procesu stanoven� ceny kamene, proto�e pr�v� v�brus ur�uje kr�su diamantu. <p>
<img src="images/diamenty_rodzaje_910x720(CZ).png"> </p>

