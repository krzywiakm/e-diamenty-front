<h1>Returns</h1>
<h3>Wthin 30 days of the date of purchase our customers have unconditional right to return the jewellery and receive their payment back.</h3> 
<p>In case customers do not like the goods they shall return it to us. (Return can be accepted only if the product was not used and the tag attached to the jewellery is not damaged. Along with jewellery customers have to return a certificate, receipt and any packaging in which they received the goods.)<br><br>
In case of custom made jewellery or jewellery was picked up personally, any returns are not allowed! In order to complete a custom the order, 30% prepayment is required. Prepayment is treated as a down payment and is not subject to return in case customer resigns from the purchase!</p>

<h3>Return / warranty / exchange form </h3>  
<p>If you need to make a complaint, return or exchange the product, please print and fill out the form below and send to us with the product.</p><p>    <a href="http://www.staviori.pl/images/return_form.pdf" target=_blank> <img src="http://www.staviori.pl/images/pdf2.png" align=right></a></p>
