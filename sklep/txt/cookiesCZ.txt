<h1>Politika sobour� <br> &#8222;cookies&#8221;          </h1>
    <div><img src="graf/linia_ciemna_jasna2.png" width="486" height="9" style="margin-top:10px; margin-bottom:10px;" /></div>
    <br />
    <p>1. Co je &#8222;cookies&#8221;?
<br>
&#8222;Cookie&#8221; je informace zaslan� prost�ednictv�m internetov�ho serveru, ulo�en� v prohl��e�i, tak �e je mo�n� ji v budoucnu op�tovn� p�e��st. Jde hlavn� o textov� soubory, slou��c� k prohl��en� internetov�ch str�nek. Mohou pom�hat internetov�mu prohl��e�i rychleji uspo��dat obsah, kter� bude l�pe odpov�dat Va�im preferovan�m z�jm�m.  Tyto soubory obvykle obsahuj� n�zev dom�ny, ze kter� soubor cookie p�i�el; &#8222;�ivotnost&#8220; souboru cookie; a hodnotu, obvykle n�hodn� generovan� jedine�n� ��slo.  <br />
2. K �emu slou�� &#8222;cookies&#8221;?<br>

Souboury &#8222;cookies&#8221;  slou�� k personalizaci str�nek a zapamatov�v�n� preferenc� n�v�t�vn�k�, optim�ln� prohl��en� web�. D�ky tomu lze tvo�it statistiky, analyzovat chov�n� n�v�t�vn�k� str�nek, sledovat chov�n� lid� na internetov�ch str�nk�ch a prezentovat behavior�ln� reklamu. D�le pom�haj� s ��zen�m n�kupn�ch ko��k� v e-shopech.  
<br />
3. Kter� druhy sobour� &#8222;cookies&#8221; se pou��vaj�?
<br>
Na�e str�nky vyu��vaj� dva typy cookies : do�asn� soubory cookie a trval� soubory cookie. Ty prvn� z�st�vaj� v souboru cookie Va�eho prohl��e�e do okam�iku, kdy opust�te internetovou str�nku. Weby je pou��vaj� k ukl�d�n� do�asn�ch informac�, nap��klad polo�ek v n�kupn�m ko��ku. Ty druh� z�st�vaj� v souboru cookie Va�eho prohl��e�e mnohem d�le, co� z�vis� na �ivotnosti p��slu�n�ho souboru cookie. Weby je pou��vaj� k ukl�d�n� informac�, jako nap��klad p�ihla�ovac�ho jm�na a hesla, tak�e se u�ivatel nemus� poka�d� p�ihla�ovat p�i n�v�t�v� ur�it�ho webu. Na soubory pou��van� partnery internetov�ho administr�tora, zejm�na u�ivately dane internetov� str�nky, se vztahuje jejich vlastn� politika ochrany soukrom�.


<br />
4. Obsahuj� &#8222;cookies&#8221; osobn� �daje?
<br>
Osobn� �daje shroma��ovan� pomoc� soubor� &#8222;cookies&#8221; mohou b�t sb�r�ny pouze za ��elem pln�n� ur�it�ch funkc� z�kazn�k�m. Takov� data jsou �ifrov�na, abychom p�ede�li neopr�vn�n�mu p��stupu  t�et�ch osob. 
<br />
5. Odstra�ov�n� &#8222;cookies&#8221; 
<br>
V�t�ina internetov�ch prohl��e�� automaticky povoluje cookies. Sem pat�� v�t�ina po��ta��, tablet� a mobiln�ch telefon�. Pokud si p�ejete nastaven� cookies zm�nit, nap��klad automaticky je blokovat nebo poka�d� je ukl�dat do po��ta�e u�ivatele, m��ete toho dos�hnout zm�nou nastaven� va�eho internetov�ho prohl��e�e, proto�e v�t�ina internetov�ch prohl��e�� umo��uje spravovat cookies nebo odstra�ovat soubory cookies prost�ednictv�m p��stupu v nastaven�. Podrobn�j�� informace naleznete v n�pov�d� ve va�em prohl��e�i. Pokud v�ak v�echny soubory cookie zak�ete, nebudete mo�n� moci vyu��vat ve�ker� interaktivn� funkce dan�ch internetov�ch str�nek.
<br />
