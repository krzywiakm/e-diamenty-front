<h1>V�m�na</h1>

<h3>Prst�nku na jinou velikost</h3>

<p>Na�im z�kazn�k�m zaru�ujeme mo�nost bezplatn� v�m�ny velikosti zakoupen�ho prst�nku do 30 dn� od jeho n�kupu, proto se p�i koupi z�snubn�ho prst�nku nemus�te tr�pit, zda zvolen� velikost bude spr�vn�.<br><br>
A� se uk�e, �e se prst�nek nehod�, ode�lete jej k n�m, abychom jej vym�nili na produkt se spr�vnou velikost�. V takov�m p��pad� se plat� jenom za zp�tnou z�silku k n�m. <br><br>

M��e se st�t, �e prst�nek u� bude pou�it nebo nebudeme m�t k dispozici po�adovanou velikost, tehdy existuje mo�nost zm�ny velikosti u na�eho odborn�ho zlatn�ka. Je to podobn� situace jako v p��pad� produkt� s nestandardn� velikost�. Prvn� zm�na je zdarma, a� na velikosti nad 18. Tehdy cena za zm�nu velikosti je stanovena individu�ln� pro jednotliv� modely.<br><br>
Upozorn�n�! V p��pad�vyroben�ch na individu�ln� objedn�vku nen� mo�nost k v�m�n� velikost a tehdy jen existuje mo�nost zm�ny velikosti u na�eho odborn�ho zlatn�ka. Av�ak pouze v p��pad�, �e konstrukce prstence umo��uje zm�nu velikosti!

<h3>�perky</h3>
<p>Na�im z�kazn�k�m zaru�ujeme mo�nost bezplatn� v�m�ny �perky zakoupen� do 30 dn� od jeho n�kupu. <br>
Kdy� se z�kazn�kovi n�jak� produkt nel�b�, m��e jej odeslat zp�t. V takov�m p��pad� je kupuj�c� povinen zaplatit za zp�tnou z�silku. (Podm�nkou v�m�ny �perk� je nepo�kozen� visa�ka v�robku, nepo�kozen� �perk- vinou nedbal�ho zach�zen�, jako�to nepo�kozen� certifik�t� a dokladu o koupi �perku v�etn� obal� , ve nich� klient �perky obdr�el.)<br>
V p��pad� �perk� vyroben�ch na individu�ln� objedn�vku nen� mo�nost v�m�na �perku ! 

<h3>Formul�� v�m�ny / reklamace / vr�cen� zbo�� </h3>  
<p>V p��pad� nutnosti vr�cen�, v�m�ny nebo po�kozen� zbo�� , pros�me o jeho odesl�n� spolu s vyti�t�n�m a vypln�n�m formul��em. </p><p>    <a href="http://www.staviori.pl/images/formular_vraceni.pdf" target=_blank> <img src="http://www.staviori.pl/images/pdf3.png" align=right></a></p>