<h3>Your Consultants</h3>

<p>On the map you will find the best Infinity Consultants who are always ready to assist you. They will present you the catalogues, advice you on jewelry, take your order and give you your products as they get them or sign you in if you would like to join Infinity.
<b>IMPORTANT!</b> Consultants do not have jewelry on them, you may order and pick up chosen products from them.</p>	