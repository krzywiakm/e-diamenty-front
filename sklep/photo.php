<!doctype html>
<html>
    <head>
    <meta charset="utf-8">
    <title>Staviori</title>
    <meta name="robots" content="noindex,follow">
    <meta name="description" content="description">
    <meta name="keywords" content="keywords">
    <link href="font/fonts.css" rel="stylesheet" />
    <link href="css/zdjecia.css" rel="stylesheet" />
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script>
    (function(d){function h(j){d(j).each(function(k){d("<img/>")[0].src=this})}function c(j){window.open(j.data.link,j.data.target);j.stopPropagation()}var f=0;var a,g,e;function i(k,l){clearInterval(e);prevThumb=l.data("prevThumb");var m=l.data("thumbnailArr");var o=l.data("linkArr");var n=l.data("targetArr");var j=l.data("bigImageArr");prevThumb.removeClass("active");a=m[k].addClass("active");l.data("prevThumb",a);d.backstretch(j[k],{speed:500},function(){var p=d("#backstretch");if(o[f]){p.css("cursor","pointer");p.bind("click",{link:o[k],target:n[k]},function(q){c(q)})}else{p.css("cursor","default");p.unbind("click")}if(l.data("slideShow")&&!l.data("mouseover")){clearInterval(e);e=setInterval(function(){b(l)},l.data("slideShowDelay"))}});f=k}function b(k){var j=f;j++;if(j>g-1){j=0}i(j,k)}d.fn.extend({fullscreenGallery:function(l){var A={containerClass:"galleryContainer",thumbContainer:"thumbContainer",thumbParentContainer:"thumbParentContainer",thumbNum:6,thumbPadding:0,thumbAutoHide:true,easeInType:"bounceInRight",slideShow:true,slideShowDelay:4000};if(l){d.extend(A,l)}var w=this;var s=w.parent();var q=w.find(".bigImages");var j=w.find(".thumbnails");g=q.find("ul li").size();j.wrap('<div class="'+A.thumbContainer+'"/>');var p=j.parent();p.wrap('<div class="'+A.thumbParentContainer+'"/>');var B=p.parent();var v=[];var k=[];var u=[];var C=[];q.find("li").each(function(D){d(this).hide();v[D]=d(this).find("img").attr("src");if(d(this).find("a").size()>0){u[D]=d(this).find("a").attr("href");C[D]=d(this).find("a").attr("target")||"_self"}});w.data("bigImageArr",v);w.data("linkArr",u);w.data("targetArr",C);w.data("mouseover",false);h(v);d.backstretch(v[0],{speed:500},function(){var D=d("#backstretch");if(u[f]){D.css("cursor","pointer");D.bind("click",{link:u[f],target:C[f]},function(E){c(E)})}D.mouseout(function(E){if(A.thumbAutoHide){B.stop().animate({opacity:0},300)}w.data("mouseover",false);if(A.slideShow){clearInterval(e);e=setInterval(function(){b(w)},w.data("slideShowDelay"))}});D.mouseover(function(E){if(A.slideShow){clearInterval(e)}B.stop().animate({opacity:1},300);w.data("mouseover",true)});if(A.slideShow&&!w.data("mouseover")){e=setInterval(function(){b(w)},A.slideShowDelay)}w.data("slideShow",A.slideShow);w.data("slideShowDelay",A.slideShowDelay)});a=j.find("li").first().addClass("active");w.data("prevThumb",a);var n,m,x=A.thumbPadding;j.find("li").each(function(D){d(this).attr("rel",D);k[D]=d(this);if(D==0){n=d(this).outerWidth(true);m=d(this).outerHeight(true)+10}d(this).css({cursor:"pointer",position:"absolute",left:(n+x)*D});d(this).bind("click",function(F){var E=d(this).attr("rel");w.data("prevThumb",a);i(E,w);F.stopPropagation()})});w.data("thumbnailArr",k);B.css({width:(n+x)*A.thumbNum+20*2+"px",height:m+"px",left:"50%",marginLeft:-n*A.thumbNum/2+20+"px"});p.css({width:(n+x)*A.thumbNum+"px",height:m+"px",overflow:"hidden"});B.mouseover(function(D){B.stop().animate({opacity:1},300)});var t=d('<div class="nextArrow"></div><div class="prevArrow"></div>');B.append(t);var y=d(".nextArrow",B);var r=d(".prevArrow",B);var o=0;var z=parseInt(p.css("width"));y.click(function(E){var D=o;D++;var F=-parseInt(p.css("width"))*D;if(F<z-(n+x)*g){F=z-(n+x)*g;D=Math.ceil((n+x)*g/z)-1}o=D;j.animate({left:F+"px"},300)});r.click(function(E){var D=o;D--;var F=-parseInt(p.css("width"))*D;if(F>0){F=0;D=0}j.animate({left:F+"px"},300);o=D});return this}});d.backstretch=function(j,y,x){var t={centeredX:true,centeredY:true,speed:0},m=d("#backstretch"),o=m.data("settings")||t,p=m.data("settings"),k=("onorientationchange" in window)?d(document):d(window),n,l,s,r,w,v;if(y&&typeof y=="object"){d.extend(o,y)}if(y&&typeof y=="function"){x=y}d(document).ready(u);return this;function u(){if(j){var z;if(m.length==0){m=d("<div />").attr("id","backstretch").css({left:0,top:0,position:"fixed",overflow:"hidden",zIndex:-999999,margin:0,padding:0,height:"100%",width:"100%"})}else{m.find("img").addClass("deleteable")}z=d("<img />").css({position:"absolute",display:"none",margin:0,padding:0,border:"none",zIndex:-999999}).bind("load",function(D){var C=d(this),B,A;C.css({width:"auto",height:"auto"});B=this.width||d(D.target).width();A=this.height||d(D.target).height();n=B/A;q(function(){C.fadeIn(o.speed,function(){m.find(".deleteable").remove();if(typeof x=="function"){x()}})})}).appendTo(m);if(d("body #backstretch").length==0){d("body").append(m)}m.data("settings",o);z.attr("src",j);d(window).resize(q)}}function q(z){try{v={left:0,top:0};s=k.width();r=s/n;if(r>=k.height()){w=(r-k.height())/2;if(o.centeredY){d.extend(v,{top:"-"+w+"px"})}}else{r=k.height();s=r*n;w=(s-k.width())/2;if(o.centeredX){d.extend(v,{left:"-"+w+"px"})}}d("#backstretch, #backstretch img:not(.deleteable)").width(s).height(r).filter("img").css(v)}catch(A){}if(typeof z=="function"){z()}}}})(jQuery);
    </script>
    <script>
		$(document).ready(function() {
			$('#gallery').fullscreenGallery();
					});
    </script>
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    </head>

    <body>
    <div id="gallery">
      <div class="bigImages">
        <ul>
<?
$dir = 'slider/images/slides/';
foreach(glob($dir.'*.jpg') as $file) 
    print "<li><img src=\"$file\"></li>\n";
?>
        </ul>
      </div>
      <div class="thumbnails">
        <ul>
<?
$dir = 'slider/images/slides/thumbs/';
foreach(glob($dir.'*.jpg') as $file) 
    print "<li><img src=\"$file\"></li>\n";
?>
        </ul>
      </div>
    </div>
</body>
</html>
