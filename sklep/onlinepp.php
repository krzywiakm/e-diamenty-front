<?
// STEP 1: read POST data
 
// Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
// Instead, read raw POST data from the input stream.
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$myPost = array();
foreach ($raw_post_array as $keyval) 
    {
    $keyval = explode ('=', $keyval);
    if (count($keyval) == 2)
        $myPost[$keyval[0]] = urldecode($keyval[1]);
    }
// read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) 
    {
    $get_magic_quotes_exists = true;
    }
foreach ($myPost as $key => $value) 
    {
    if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) 
        {
        $value = urlencode(stripslashes($value));
        } 
    else 
        {
        $value = urlencode($value);
        }
    $req .= "&$key=$value";
    }
 
// STEP 2: POST IPN data back to PayPal to validate
 
$ch = curl_init('https://www.paypal.com/cgi-bin/webscr');
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
 
// In wamp-like environments that do not come bundled with root authority certificates,
// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set
// the directory path of the certificate as shown below:
// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
if( !($res = curl_exec($ch)) ) 
    {
    // error_log("Got " . curl_error($ch) . " when processing IPN data");
    $files = fopen("platpp.dat", "a");
    flock($files, 2);
    fputs($files, "Got " . curl_error($ch) . " when processing IPN data\n\n");
    flock($files, 3);
    fclose($files);
        
    curl_close($ch);
    exit;
    }
curl_close($ch);
 
// STEP 3: Inspect IPN validation result and act accordingly
 
if (strcmp ($res, "VERIFIED") == 0) 
    {
    $files = fopen("platpp.dat", "a");
    flock($files, 2);
    fputs($files, $res."\n");
    foreach($_POST as $key => $value)
        fputs($files, $key." = ". $value."\n");
    fputs($files,"\n");
    flock($files, 3);
    fclose($files);
    // The IPN is verified, process it:
    // check whether the payment_status is Completed
    // check that txn_id has not been previously processed
    // check that receiver_email is your Primary PayPal email
    // check that payment_amount/payment_currency are correct
    // process the notification
     
    // assign posted variables to local variables
    $item_name = $_POST['item_name'];
    $item_number = $_POST['item_number'];
    $payment_status = $_POST['payment_status'];
    $payment_amount = $_POST['mc_gross'];
    $payment_currency = $_POST['mc_currency'];
    $txn_id = $_POST['txn_id'];
    $receiver_email = $_POST['receiver_email'];
    $payer_email = $_POST['payer_email'];
     
    // IPN message values depend upon the type of notification sent.
    // To loop through the &_POST array and print the NV pairs to the screen:
    /*foreach($_POST as $key => $value) 
        {
        echo $key." = ". $value."<br>";
        }*/
    $nrzam=$item_name;
    $amount=$payment_amount;
            
    if(strcmp ($payment_status, "Completed") == 0)
        {
        include "../katalog/func2.php";
        
        $link = dblogin();

      	$query = "SELECT * FROM ZAMOWIENIA WHERE Id='" . $nrzam . "'";
      	$result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
      	$zamline = mysql_fetch_array($result, MYSQL_ASSOC);
      	
      	$query = "SELECT * FROM USERS WHERE Id='" . $zamline[IdUser] . "'";
      	$result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
      	$userline = mysql_fetch_array($result, MYSQL_ASSOC);
      	
      	$query = "SELECT * from KUPONY where Id='$zamline[Kupon]'";
          $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
      	  $kuponline = mysql_fetch_array($result, MYSQL_ASSOC);

      	$query = "SELECT Symbol, Rozmiar, Netto, Brutto, Ilosc FROM PRODUKTYZAM WHERE IdZam='" . $zamline[Id] . "'";
      	$result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
        $ile = mysql_num_rows($result);

        
        if($ile < 1)
        	{

        	}
        else
            {
          	$wartnetto=0.0;
     	 	$wartbrutto=0.0;
      		$sumailosc=0;
      		
      		$prodlst="";
      		
      		while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
        		{
        		$wbrt=$line[Brutto]*$line[Ilosc];
            	$wnet=$line[Netto]*$line[Ilosc];

     		   	$wartnetto+=$wnet;
       		 	$wartbrutto+=$wbrt;
        		$sumailosc+=$line[Ilosc];
        		
        		$prodlst.=$line[Symbol].($line[Rozmiar]?" (".$line[Rozmiar].") - ":" - "). waluta($wbrt,$_GET[CURR])."\n";
            	}

	
                $jakirabat=0;
            			        
	

	        $rabat = $jakirabat*$wartbrutto/100;
	        $strrabat = $jakirabat."%";
	        $rabat2 = $jakirabat;
	        
	        if($kuponline)
                {
                switch($kuponline[Rodzaj])
                    {
                    case 0:  
                            $kwotaprom= -$kuponline[KwotProc];
                            //$wartbrutto+=$kwotaprom;
                            $rabat=-$kwotaprom;
                            $rabat2=$rabat*100/$wartbrutto;
                            break;
                    case 1: 
                            $kwotaprom= -$kuponline[KwotProc]*$wartbrutto/100;
                            //$wartbrutto+=$kwotaprom;
                            $rabat2=$kuponline[KwotProc];
                            $rabat=-$kwotaprom;
                            break;
                    }
                }
        	
	        $rabat = round($rabat,2);        
                
            $queryPrzesyl="SELECT * FROM PRZESYLKI WHERE Nazwa='" . $zamline[Przesylka] . "'";
            $resultPrzesyl = mysql_query ($queryPrzesyl) or die ("Zapytanie zakonczone niepowodzeniem");	
            $linePrzesyl = mysql_fetch_array($resultPrzesyl, MYSQL_ASSOC);
            
            $przesylka=$zamline[Przesylka];
            
        	$kosztprzesylki=$linePrzesyl[Koszt1];
        	if($wartbrutto>$linePrzesyl[Prog1])
        	    $kosztprzesylki=$linePrzesyl[Koszt2];
        	if($wartbrutto>$linePrzesyl[Prog2])
        	    $kosztprzesylki=$linePrzesyl[Koszt3];
        	if($wartbrutto>$linePrzesyl[Prog3])
        	    $kosztprzesylki=$linePrzesyl[Koszt4];

        	//if($userline[DarmowaPrzesylka])
	          //  $kosztprzesylki=0.00;		
	            
            $kosztprzesylki = $zamline[KosztPrzesylki];
	
	        $dozaplaty = $wartbrutto  + $zamline[KosztPrzesylki] - $rabat;
	        if($dozaplaty<$zamline[KosztPrzesylki])
	            $dozaplaty=$zamline[KosztPrzesylki];
            
            $dozaplatykurs="1";        
            if($userline[Waluta]!="PLN")
                $dozaplatykurs=kurs($userline[Waluta]); 
                
            $komentarz="";
            
            if($dozaplaty==$amount && strcmp ($receiver_email, "kontakt@staviori.pl") == 0)
                {
                if($zamline[Zatwierdzone])
                    {
                    $query = "UPDATE ZAMOWIENIA SET Przedplata=1 WHERE Id='" . $zamline[Id] . "'";
                    $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
                    }
                else
                    {
                    $faktura="";
                    if($userline[NazwaFirmy])
                        $faktura=" Faktura=1,";
        
                    $query = "UPDATE ZAMOWIENIA SET Przedplata=1, Zatwierdzone=1,$faktura KwotaBrutto='".$wartbrutto."', KwotaNetto='".$wartnetto."', DataZam='".currdate()."', CzasZam='".currtime()."', Rabat='".$rabat."', Rabat2='".$rabat2."', Przesylka='".$przesylka."', DoZaplaty='".$dozaplaty."', Waluta='".$userline[Waluta]."', Kurs='".$dozaplatykurs."' WHERE Id='" . $zamline[Id] . "'";
          		    $result = mysql_query ($query) or die ("Zapytanie zakonczone niepowodzeniem");
          		    
          		    $lang=$userline[Lang];
          		    if($lang=="PL") $lang="";    
                	$myFile = "../common/mail6".$lang.".txt"; 	
                	$fh = fopen($myFile, 'r');
                	$wiadomosc = fread($fh, filesize($myFile));
                	fclose($fh);

                    $wiadomosc = str_replace("[IMIE]", $imie, $wiadomosc);        
                    $wiadomosc = str_replace("[NAZWISKO]", $nazwisko, $wiadomosc);
                    $wiadomosc = str_replace("[NUMERZAMOWIENIA]", $zamline[Id], $wiadomosc);
                    $wiadomosc = str_replace("[KWOTA]", waluta($dozaplaty,$_GET[CURR]), $wiadomosc);
                    $wiadomosc = str_replace("[KOSZTWYSYLKI]", waluta($kosztprzesylki,$_GET[CURR]), $wiadomosc);
                    $wiadomosc = str_replace("[LISTAPRODUKTOW]", $prodlst, $wiadomosc);
            
                    mail($userline[Email],"[staviori] ".slownik(351,$lang)."",$wiadomosc,"Content-type: text/plain; charset=iso-8859-2\r\nFrom: kontakt@staviori.pl");
          		    }
                }	

            //print "<p class=ok align=center>".slownik(120)."</p>";

            //powiadomienie
            $wiadomosc = "Nowe zamówienie od: ".$userline[Login]." ".$userline[Imie]." ".$userline[Nazwisko];
            mail("kontakt@staviori.pl","[staviori] nowe zamówienie (".$zamline[Id].")",
            			$wiadomosc,"Content-type: text/plain; charset=iso-8859-2\r\nFrom: ".$userline[Email]);
      		}
        mysql_close($link);
        }    
    } 
else if (strcmp ($res, "INVALID") == 0) 
    {
    // IPN invalid, log for manual investigation
    //echo "The response from IPN was: <b>" .$res ."</b>";
    $files = fopen("platpp.dat", "a");
    flock($files, 2);
    fputs($files, $res."\n");
    foreach($_POST as $key => $value)
        fputs($files, $key." = ". $value."\n");
    fputs($files,"\n");
    flock($files, 3);
    fclose($files);
    }


?>
